<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
 
 
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${reportName}</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Raporlar </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${reportName} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								Filtre Ayarları
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">	
								<div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Export
									</button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1" id="mnuExport">
										<a class="dropdown-item" href="javascript:void(0);" export-type="xml">
											<i class="fa fa-file-code"></i> XML
										</a>
										<a class="dropdown-item" href="javascript:void(0);" export-type="pdf"> 
											<i class="fa fa-file-pdf"></i> PDF
										</a>
										<a class="dropdown-item" href="javascript:void(0);" export-type="excel">
											<i class="fa fa-file-excel"></i> EXCEL
										</a>									
									</div>
								</div>								
								<spring:url value="/bildirim/bildirimara?type=pdf" var="pdfURL"/>
								<spring:url value="/bildirim/bildirimara?type=xls" var="xlsURL"/>																					
							</div>
						</div>
					</div>
					<div class="kt-portlet__body">
					
						<div class="form-group m-form__group">
					    	<label for="bildirim_no">Tarih Aralığı</label>
							<input type="text"  id="sdate"  style="width:100%;">
							<div>&nbsp;</div>
							<input type="text" id="fdate"  style="width:100%">			    	
						</div>
						<div class="form-group m-form__group">
					    	<label for="fk_agent">Kullanıcı</label>
							<input id="fk_agent" value="0" style="width: 100%;" />	    	
						</div>			
																							
						<div class="form-group m-form__group">
							<button class="btn btn-danger btn-sm float-left" id="btnClear">
								<i class="fa fa-times"></i> 
								Temizle
							</button>						
							<button class="btn btn-brand btn-sm float-right" id="btnReport">
								<i class="fa fa-search"></i> 
								Rapor Al
							</button>
						</div>						
					</div>
				</div>				
			</div>	
			<div class="col-md-9">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${reportName}
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div id=reportGrid></div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>
<script type="text/x-kendo-template" id="tarihColTemplate">
	<div>
		#=tarih#
	</div>
</script>
<script>

var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;
$(function () {
    var token = $("input[name='_csrf']").val();
    var header = "X-CSRF-TOKEN";
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, _csrf_token);
    });
});
$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});

$("#sdate").kendoDatePicker({
	format:'yyyy-MM-dd',
	culture:"tr-TR",
});
$("#fdate").kendoDatePicker({
	format:'yyyy-MM-dd',
	culture:"tr-TR",
});


$("#btnClear").unbind().click(function(){
	location.reload();
})



var crudServiceBaseUrl="/rapor/raporlog";
reportGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "tarih"            	:   { editable: false, nullable: true },
                "rapor"            	:   { editable: false, nullable: true },
                "kullanici"         :   { editable: false, nullable: true },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});

function reportDSFilter(sdate,fdate,fk_agent){
	reportGridDataSource.filter([
            {
                field: "sdate",
                operator: "eq",
                value: sdate
            },                                                 
            {
                field: "fdate",
                operator: "eq",
                value: fdate
            },                                                 
            {
                field: "fk_agent",
                operator: "eq",
                value: fk_agent
            }                                                
        ]
    );

}
var reportGrid = $("#reportGrid").kendoGrid({
    "columns": [
		{
            "field": "tarih",
            "filterable": false, "groupable": false, "title": "İşem Tarihi",
			template:kendo.template($("#tarihColTemplate").html()),
			"width":150
        },
		{
            "field": "rapor",
            "filterable": false, "groupable": true, "title": "Rapor",
        },
		{
            "field": "kullanici",
            "filterable": false, "groupable": false, "title": "Kullanıcı",
        }
                   
    ],
    "autoBind": false,
    "dataSource":reportGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": true,
    "navigatable": false,
    "editable": "false",
    "pageable": {"alwaysVisible": true, "responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [10,20,30,50]},
    "height":800,

});
var grid = $("#reportGrid").data("kendoGrid");
$("#btnReport").unbind().click(function(){
	var sdate=$("#sdate").val();
	var fdate=$("#fdate").val();
	var fk_agent=$("#fk_agent").data("kendoDropDownList").value();

	
	if(sdate=="" || fdate=="" || fk_agent=="" || fk_agent=="0"){
		alertify.error("Tarih Aralığı Ve Kullanıcı Girilmeden Rapor Alınamaz");
		return false;
	}
	reportDSFilter(sdate,fdate,fk_agent);
});



/*****
 * 
 * tarih aralığı validasyonları
 * 
 * ***/
 var tAraligi=Settings.raporTarihAraligi;
var todayDate = kendo.toString(kendo.parseDate(new Date()), 'yyyy-MM-dd');
$("#sdate").data("kendoDatePicker").value(todayDate);
$("#fdate").data("kendoDatePicker").value(todayDate);
function startChange() {
	var startDate = start.value(),
	endDate = end.value();
	startDate = new Date(startDate);
	startDate.setDate(startDate.getDate());
	var edate= new Date(startDate);
	edate.setDate(edate.getDate()+tAraligi);
	end.min(startDate);
	end.max(edate);
}

function endChange() {
	var endDate = end.value(),
		startDate = start.value();
	if (!startDate) {
		endDate = new Date(endDate);
		endDate.setDate(endDate.getDate());
		var edate= new Date(endDate);
		edate.setDate(edate.getDate()-tAraligi);
		start.max(endDate);
		start.min(edate)
	
	}
}

var start = $("#sdate").kendoDatePicker({
	change: startChange,
	format:'yyyy-MM-dd',
	culture:"tr-TR",
}).data("kendoDatePicker");

var end = $("#fdate").kendoDatePicker({
	change: endChange,
	format:'yyyy-MM-dd',
	culture:"tr-TR",
}).data("kendoDatePicker");

start.max(end.value());
end.min(start.value());
$("#sdate,#fdate").keypress(function(){
	return false;
});



var crudServiceBaseUrl="/rapor/getUsers";
usersDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"            	:   { editable: false, nullable: true }
            }
        },
    },
    "serverFiltering": true,
    "autoSync": true,
});
$("#fk_agent").kendoDropDownList({
	filter: "startswith",
	dataTextField: "adi",
	dataValueField: "id",
	minLength: 3,
	dataSource:usersDataSource,
	value: "0"
});
</script>

<jsp:include page="Footer.jsp" />
 
 
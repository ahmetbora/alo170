<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../BILDIRIM/header.jsp" />
 
 
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					KURUM / ŞUBE / HAVUZ İSTATİSTİK RAPORU</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Raporlar </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						KURUM / ŞUBE / HAVUZ İSTATİSTİK RAPORU </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar d-none">
				<div class="kt-subheader__wrapper">
					<a href="#" class="btn kt-subheader__btn-primary">
						Actions &nbsp;
	
						<!--<i class="flaticon2-calendar-1"></i>-->
					</a>
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
						<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000" />
								</g>
							</svg>
	
							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
	
							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Add anything or jump to:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Order</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Ticket</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Goal</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support Case</span>
										<span class="kt-nav__link-badge">
											<span class="kt-badge kt-badge--success">5</span>
										</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
									<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
								</li>
							</ul>
	
							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-lg-3 col-md-12">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Filtre
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="form-group">
							<label>Tarih Aralığı :</label>
							<input id="sdate" class="pb-1" style="width: 100%" />
							<input id="fdate" class="pt-1" style="width: 100%" />
						</div>
						<div class="form-group">
							<label>Kurum :</label>
							<select class="form-control zorunlu" name="tip_level1" id="tip_level1" placeholder="Kurum" >
								<option value="">--Kurumlar--</option>
							 <c:forEach items="${kurumlar}" var="n">
								 <option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
							 </c:forEach>													
						 </select>
						</div>
						<div class="form-group">
							<label>Şehir :</label>
							<select class="form-control zorunlu" name="fk_il" id="fk_il" placeholder="Şehir">
								<option value="">--Şehir--</option>
							 <c:forEach items="${iller}" var="n">					
								 <option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
							 </c:forEach>							   
						</select>	
						</div>
						<div class="form-group">
							<label>Şube :</label>
							<select id="fk_sube" name="fk_sube" placeholder="Şube" class="form-control zorunlu">
								<option value="">--Şubeler--</option>								   			
							</select>  	
						</div>
						<div class="form-group">
							<label>Havuz :</label>
							<select class="form-control zorunlu" name="fk_havuz" id="fk_havuz" placeholder="Havuzlar">
								<option value="">--Havuzlar--</option>
							</select>		
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9 col-md-12" id="reportCont">
				<div class="kt-portlet kt-portlet--responsive-mobile">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<span class="kt-portlet__head-icon">
								<i class="flaticon-technology kt-font-success"></i>
							</span>
							<h3 class="kt-portlet__head-title kt-font-brand">
								Custom Title &amp; Toolbar
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
								<button type="button" class="btn btn-secondary"><i class="la la-file-text-o"></i></button>
								<button type="button" class="btn btn-secondary"><i class="la la-floppy-o"></i></button>
								<button type="button" class="btn btn-secondary"><i class="la la-header"></i></button>
								<div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Dropdown
									</button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
										<a class="dropdown-item" href="#">Dropdown link</a>
										<a class="dropdown-item" href="#">Dropdown link</a>
										<a class="dropdown-item" href="#">Dropdown link</a>
										<a class="dropdown-item" href="#">Dropdown link</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script>
var xhr=null;
	alertify.set({ labels: {
		ok     : "Tamam",
		cancel : "Vazgeç"
	}});

	$('#tip_level1,#fk_il').select2();

	function getSubeler(){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	$.ajax({
		type: "POST",
		url: '/bildirim/getSubeler',
		data:
				{
					tip_level1: $('#tip_level1 option:selected').val(),
					fk_il: $('#fk_il option:selected').val()
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_sube').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}
function getHavuzlar(){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	$.ajax({
		type: "POST",
		url: '/bildirim/getHavuzlar',
		data:
				{
					fk_sube: $('#fk_sube option:selected').val()
					
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_havuz').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});			
}
$('#fk_il').change(function(){
	getSubeler();
});
$('#tip_level1').change(function(){
	getSubeler();
});
$('#fk_sube').change(function(){
	getHavuzlar();
});


var tAraligi=Settings.raporTarihAraligi;
var todayDate = kendo.toString(kendo.parseDate(new Date()), 'yyyy-MM-dd');
$("#sdate").data("kendoDatePicker").value(todayDate);
$("#fdate").data("kendoDatePicker").value(todayDate);
function startChange() {
	var startDate = start.value(),
	endDate = end.value();
	startDate = new Date(startDate);
	startDate.setDate(startDate.getDate());
	var edate= new Date(startDate);
	edate.setDate(edate.getDate()+tAraligi);
	end.min(startDate);
	end.max(edate);
}

function endChange() {
	var endDate = end.value(),
		startDate = start.value();
	if (!startDate) {
		endDate = new Date(endDate);
		endDate.setDate(endDate.getDate());
		var edate= new Date(endDate);
		edate.setDate(edate.getDate()-tAraligi);
		start.max(endDate);
		start.min(edate)
	
	}
}

var start = $("#sdate").kendoDatePicker({
	change: startChange,
	format:'yyyy-MM-dd',
	culture:"tr-TR",
}).data("kendoDatePicker");

var end = $("#fdate").kendoDatePicker({
	change: endChange,
	format:'yyyy-MM-dd',
	culture:"tr-TR",
}).data("kendoDatePicker");

start.max(end.value());
end.min(start.value());
$("#sdate,#fdate").keypress(function(){
	return false;
});
</script>
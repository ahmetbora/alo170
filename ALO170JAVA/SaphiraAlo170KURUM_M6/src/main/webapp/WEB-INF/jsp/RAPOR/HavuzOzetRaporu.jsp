<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
 
 
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${reportName}</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Raporlar </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${reportName} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${reportName}
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div id=reportGrid></div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>

<script type="text/x-kendo-template" id="reportGridToolbar">
    <button class="btn btn-brand btn-sm " id="btnUstuneAl">Üzerime Al</button>
</script>

<script>

var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;
$(function () {
    var token = $("input[name='_csrf']").val();
    var header = "X-CSRF-TOKEN";
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, _csrf_token);
    });
});
$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});

$("#btnClear").unbind().click(function(){
	location.reload();
})



var crudServiceBaseUrl="/rapor/havuz-ozet-raporu";
reportGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "havuz"            	:   { editable: false, nullable: true },
                "toplam"            :   { editable: false, nullable: true },
                "ucgun"            	:   { editable: false, nullable: true },
                "ongun"            	:   { editable: false, nullable: true },
                "biray"            	:   { editable: false, nullable: true },
                "ikiay"            	:   { editable: false, nullable: true },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});


var reportGrid = $("#reportGrid").kendoGrid({
    "columns": [
        {
            "field": "havuz",
            "filterable": false, "groupable": false, "title": "Havuz","width":"50%"
        },

        {
            "field": "toplam",
            "filterable": false, "groupable": false, "title": "Toplam"
        },

        {
            "field": "ucgun",
            "filterable": false, "groupable": false, "title": "3 GÜN"
        },

        {
            "field": "ongun",
            "filterable": false, "groupable": false, "title": "10 GÜN"
        },

        {
            "field": "biray",
            "filterable": false, "groupable": false, "title": "1 AY"
        },

        {
            "field": "ikiay",
            "filterable": false, "groupable": false, "title": "2 AY"
        },
                   
    ],
    "autoBind": true,
    "dataSource":reportGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": false,
    "navigatable": false,
    "editable": "false",

    "pageable": {"alwaysVisible": true, "responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [10,20,30,50]},
    "height":800,
    "toolbar": kendo.template($("#reportGridToolbar").html())
});
var grid = $("#reportGrid").data("kendoGrid");





</script>

<jsp:include page="Footer.jsp" />
 
 
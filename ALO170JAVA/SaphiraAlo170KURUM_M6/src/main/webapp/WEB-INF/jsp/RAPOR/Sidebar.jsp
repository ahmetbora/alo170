<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__section ">
                <h4 class="kt-menu__section-text">ALO 170 RAPORLARI</h4>
                <i class="kt-menu__section-icon flaticon-more-v2"></i>
            </li>
            <li class="kt-menu__item " aria-haspopup="true" data-menu="kshistatistikraporu">
                <a href="/rapor/kshistatistikraporu" class="kt-menu__link ">
                   <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Kurum / Şube / Havuz İst.</span>
                </a>
            </li>								
            <li class="kt-menu__item " aria-haspopup="true" data-menu="kurumkullaniciistatistikraporu">
                <a href="/rapor/kurumkullaniciistatistikraporu" class="kt-menu__link ">
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Kurum Kullanıcı İstatistik</span>
                </a>
            </li>															

            <li class="kt-menu__item " aria-haspopup="true" data-menu="bildirimdetayraporu">
                <a href="/rapor/bildirimdetayraporu" class="kt-menu__link ">
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Bildirim Detay Raporu</span>
                </a>
            </li>		
            <li class="kt-menu__item " data-menu="mu-aktivite-rapor">
                <a href="/rapor/mu-aktivite-rapor" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Mevzuat Uzmanı Aktivite Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="mu-sure-rapor">
                <a href="/rapor/mu-sure-rapor" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Mevzuat Uzmanı İşlem Süresi Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="ky-sure-rapor">
                <a href="/rapor/ky-sure-rapor" class="kt-menu__link ">
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Kurum Yetkilisi İşlem Süresi Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="havuz-bildirim-sure">
                <a href="/rapor/havuz-bildirim-sure" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Havuz Bildirim Süre Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="ham-data-raporu">
                <a href="/rapor/ham-data-raporu" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Ham Data Raporu</span>
                </a>
            </li>

            <li class="kt-menu__item " data-menu="sektor-raporu">
                <a href="/rapor/sektor-raporu" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Sektör Bazında</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="sektor-detay-raporu">
                <a href="/rapor/sektor-detay-raporu" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Sektör Bazında İhbar / Şikayet</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="ile-gore-bildirim-raporu">
                <a href="/rapor/ile-gore-bildirim-raporu" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">İl Bazında</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="konulara-gore-bildirim-raporu">
                <a href="/rapor/konulara-gore-bildirim-raporu" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Konulara Göre</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="havuz-ozet-raporu">
                <a href="/rapor/havuz-ozet-raporu" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Havuz Açık Görevler Özet</span>
                </a>
            </li>
    
            <li class="kt-menu__item " data-menu="pbxreports">
                <a href="/rapor/pbxreports" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Çağrı Merkezi Raporları</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="sgk-bilgilendirme">
                <a href="/rapor/sgk-bilgilendirme" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">WebChat Kurum Bilgilendirmeleri</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="ivr-agent">
                <a href="/rapor/ivr-agent" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Agent Bazlı IVR Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="taseron-havuzu-rapor">
                <a href="/rapor/taseron-havuzu-rapor" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Taşeron Havuzu Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="ses-kayit">
                <a href="/rapor/ses-kayit" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Ses Kayıt Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="kko-olcum-yeni">
                <a href="/rapor/kko-olcum-yeni" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">YENİ Kalite Güvence Kriterleri Ölçüm Sonuçları</span>
                </a>
            </li>
            <li class="kt-menu__item d-none" data-menu="kko-olcum-out">
                <a href="/rapor/kko-olcum-out" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Dış Arama KKO Ölçüm Sonuçları</span>
                </a>
            </li>		
            <li class="kt-menu__item " data-menu="kurum-kapanma-istatistik-raporu">
                <a href="/rapor/kurum-kapanma-istatistik-raporu" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Kurumlar Bazında Bildirim İstatistikleri</span>
                </a>
            </li>
 
    
            <li class="kt-menu__item " data-menu="islem-yapilmamis-kbb">
                <a href="/rapor/islem-yapilmamis-kbb" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">İşlem Yapılmamış Bildirimler - Kurum Birimleri Bazında</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="birimlere-gore-kullanici-sayisi">
                <a href="/rapor/birimlere-gore-kullanici-sayisi" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Birimlere Göre Kullanıcı Sayıları</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="il-kurum-bildirim-dagilimi">
                <a href="/rapor/il-kurum-bildirim-dagilimi" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">İl Ve Kurum Bazlı Bildirim Dağılımı</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="kurum-birim-bildirim-dagilimi">
                <a href="/rapor/kurum-birim-bildirim-dagilimi" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Kurum Birimlerine Göre Bildirim Dağılımı</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="arama-nedeni-kurum-bildirim-dagilimi">
                <a href="/rapor/arama-nedeni-kurum-bildirim-dagilimi" class="kt-menu__link ">
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Arama Nedeni Kurum Bildirim Dağılımı</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="kurum-bazinda-zamaninda-kapanan">
                <a href="/rapor/kurum-bazinda-zamaninda-kapanan" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span
                    class="kt-menu__link-text">Kurumlar Bazında Zamanında kapanan Bildirimler</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="ivr-rapor">
                <a href="/rapor/ivr-rapor" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">IVR Anket Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="sms-rapor">
                <a href="/rapor/sms-rapor" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Sms / Email Raporu</span>
                </a>
            </li>
            <li class="kt-menu__item d-none" data-menu="kko-olcum">
                <a href="/rapor/kko-olcum" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Kalite Güvence Kriterleri Ölçüm Sonuçları</span>
                </a>
            </li>		
            <li class="kt-menu__item " data-menu="periyodik-rapor">
                <a href="/rapor/periyodik-rapor" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Periyodik Rapor</span>
                </a>
            </li>
            <li class="kt-menu__item d-none" data-menu="ppt-report">
                <a href="/rapor/ppt-report" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">İstatistik Raporları - Powerpoint</span>
                </a>
            </li>
            <li class="kt-menu__item " data-menu="raporlog">
                <a href="/rapor/raporlog" class="kt-menu__link "> 
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>
                    <span class="kt-menu__link-text">Rapor Logları</span>
                </a>
            </li> 
            <li class="kt-menu__item " aria-haspopup="true" data-menu="geo-rapor">
                <a href="/rapor/geo-rapor" class="kt-menu__link ">
                     <i class="fa fa-angle-double-right kt-menu__link-icon"></i>									
                    <span class="kt-menu__link-text">GEO Rapor</span>
                </a>
            </li> 	
            <li class="kt-menu__item d-none" aria-haspopup="true" data-menu="izin-talep-rapor">
                <a href="/rapor/izin-talep-rapor" class="kt-menu__link ">
                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                    <span class="kt-menu__link-text">Personel İzin Talebi Oluştur</span>
                </a>
            </li>																						
        </ul>
    </div>
</div>
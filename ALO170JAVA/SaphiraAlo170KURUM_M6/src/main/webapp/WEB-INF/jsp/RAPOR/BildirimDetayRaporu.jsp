<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
 
 <style>
	 #bildirimGrid td{
		 font-weight: 400 !important;
	 }
 </style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${reportName} </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						İstatistik Raporları </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${reportName} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<!-- rapor filtre start-->
			<div class="col-md-3">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								Filtre Ayarları
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">	
								<div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Export
									</button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1" id="mnuExport">
										<a class="dropdown-item" href="javascript:void(0);" export-type="xml">
											<i class="fa fa-file-code"></i> XML
										</a>
										<a class="dropdown-item" href="javascript:void(0);" export-type="pdf"> 
											<i class="fa fa-file-pdf"></i> PDF
										</a>
										<a class="dropdown-item" href="javascript:void(0);" export-type="excel">
											<i class="fa fa-file-excel"></i> EXCEL
										</a>									
									</div>
								</div>								
								<spring:url value="/bildirim/bildirimara?type=pdf" var="pdfURL"/>
								<spring:url value="/bildirim/bildirimara?type=xls" var="xlsURL"/>																					
							</div>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="form-group m-form__group">
					    	<label for="sdate">Tarih Aralığı</label>
							<input type="text"  id="sdate"  style="width:100%;">
							<div>&nbsp;</div>
							<input type="text" id="fdate"  style="width:100%">			    	
						</div>
		
						<div class="form-group m-form__group">
					    	<label for="fk_il">Şehir</label>
		                    <select name="iller" id="fk_il" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Seciniz ---</option>
		                    	<c:forEach items="${iller}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
		                    </select>	    	
						</div>	
						<div class="form-group m-form__group">
					    	<label for="fk_ilce">İlçe</label>
		                    <select name="fk_ilce" id="fk_ilce" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Seciniz ---</option>
		                    </select>	    	
						</div>	
						<div class="form-group m-form__group">
					    	<label for="fk_gorev_statu">Statü</label>
		                    <select name="fk_gorev_statu" id="fk_gorev_statu" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Seciniz ---</option>
		                    	<c:forEach items="${taskStatu}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
		                    </select>	    	
						</div>	
						<div class="form-group m-form__group">
					    	<label for="arama_kanali_tipi">Havuz Durumu</label>
		                    <select name="fk_havuz" id="fk_havuz" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Tümü ---</option>
		                    	<option value='1'>Kuruma Yönlendirilmiş</option>
		                    </select>	    	
						</div>	
						<div class="form-group m-form__group">
					    	<label for="bildirim_no">Arama Kanalı Tipi</label>
		                    <select name="arama_kanali_tipi" id="arama_kanali_tipi" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Seciniz ---</option>
		                    	<c:forEach items="${aramakanalitipi}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
		                    </select>	    	
						</div>	
						<div class="form-group m-form__group">
					    	<label for="fk_aramakanali">Arama Kanalı</label>
							<select class="form-control" id="fk_aramakanali">
								<option value='0'>--- Seciniz ---</option>
							</select>		    	
						</div>																							
						<div class="form-group m-form__group">
					    	<label for="fk_aramasebebi">Arama Sebebi</label>
							<select class="form-control" id="fk_aramasebebi">
								<option value='0'>--- Seciniz ---</option>
		                    	<c:forEach items="${aramaSebebi}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>								
							</select>		    	
						</div>																							
						<div class="form-group m-form__group">
					    	<label for="tip_level1">Kurum</label>
							<select class="form-control" id="tip_level1">
								<option value='0'>--- Seciniz ---</option>
		                    	<c:forEach items="${kurumlar}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
							</select>		    	
						</div>																							
						<div class="form-group m-form__group">
					    	<label for="tip_level2">Alt Kurum</label>
							<select class="form-control" id="tip_level2">
								<option value='0'>--- Seciniz ---</option>
							</select>		    	
						</div>																							
						<div class="form-group m-form__group">
					    	<label for="tip_level3">Arama Konusu</label>
							<select class="form-control" id="tip_level3">
								<option value='0'>--- Seciniz ---</option>
							</select>		    	
						</div>																							
						<div class="form-group m-form__group">
							<button class="btn btn-danger btn-sm float-left" id="btnClear">
								<i class="fa fa-times"></i> 
								Temizle
							</button>						
							<button class="btn btn-brand btn-sm float-right" id="btnReport">
								<i class="fa fa-search"></i> 
								Rapor Al
							</button>
						</div>						
					</div>
				</div>				
			</div>
			<!-- rapor filtre end-->	
			
			<!-- rapor grid start-->
			<div class="col-md-9">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${reportName}
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div id="bildirimGrid"></div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>

<script type="text/x-kendo-template" id="reportGridToolbar">

</script>
<script type="text/x-kendo-template" id="yetkiliColTemplate">
	# if(fk_agent!="0" && fk_gorev_statu!="0"){#
		<span class="text-info kt-font-bold">#=agent#</span>
	#}else{#
		<span class="kt-badge kt-badge--danger kt-badge--inline">#=agent#</span>
	#}#
</script>
<script type="text/x-kendo-template" id="sureColTemplate">
	# if(sure>360){#
		<span class="kt-badge kt-badge--danger kt-badge--inline">#=sure#</span>
	#}else if(sure>100){#
		<span class="kt-badge kt-badge--danger kt-badge--inline">#=sure#</span>
	#}else{#
		<span class="kt-badge kt-badge--success kt-badge--inline">#=sure#</span>
	#}#	
</script>
<script type="text/x-kendo-template" id="statuColTemplate">
	<span class="kt-badge kt-badge--inline text-white #=statuClass#">#=statu#</span>
</script>
<script>
$("#tip_level1").select2({ placeholder: "Seçiniz", minimumResultsForSearch: 1 / 0 });
$("#fk_il").select2({ placeholder: "Seçiniz", minimumResultsForSearch: 1 / 0 });
var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;
$(function () {
    var token = $("input[name='_csrf']").val();
    var header = "X-CSRF-TOKEN";
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, _csrf_token);
    });
});
$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});
var todayDate = kendo.toString(kendo.parseDate(new Date()), 'yyyy-MM-dd');
$("#sdate").kendoDatePicker({
	format:'yyyy-MM-dd',
	culture:"tr-TR",
});
$("#fdate").kendoDatePicker({
	format:'yyyy-MM-dd',
	culture:"tr-TR",
});

$("#sdate").data("kendoDatePicker").value("${sdate}");
$("#fdate").data("kendoDatePicker").value("${fdate}");
function getIlceler(fk_il){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	
	$.ajax({
		type: "POST",
		url: '/lookup/getIlceler',
		data:
				{
					fk_il: fk_il
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_ilce').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}
function getAramaKanali(arama_kanali_tipi){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	
	$.ajax({
		type: "POST",
		url: '/lookup/getAramaKanali',
		data:
				{
					arama_kanali_tipi: arama_kanali_tipi
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_aramakanali').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}


function AltKurumlar(){
	var val=$("#fk_aramasebebi option:selected").val();
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	$.ajax({
		type: "POST",
		url: '/bildirim/altKurumlar',
		data:
				{
					id: $('#tip_level1 option:selected').val()
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {			
			$('#tip_level2').html(jqXHR.responseText);
			$('#tip_level3').html('<option value="">--Seçiniz--</option>');								
			//$(".tip_level3").addClass("d-none");

		},
		error: function (request, status, error) {
			console.log(status);
		}
	});

}
function aramaKonusu(){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	$.ajax({
		type: "POST",
		url: '/bildirim/aramaKonulari',
		data:
				{
					id: $('#tip_level2 option:selected').val()
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$("#tip_level3").select2("destroy");					
			$('#tip_level3').html(jqXHR.responseText);					
			$(".tip_level3").removeClass("d-none");
			$("#tip_level3").select2();
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
	
	var val=$("#fk_aramasebebi option:selected").val();
}

$('#tip_level2').change(function(){
	$("#tip_level3").select2();
	aramaKonusu();
});
$('#tip_level1').change(function(){
	AltKurumlar();
}); 

$('#fk_il').change(function(){
	fk_il=$('#fk_il option:selected').val()
	getIlceler(fk_il);
});
$('#arama_kanali_tipi').change(function(){
	arama_kanali_tipi=$('#arama_kanali_tipi option:selected').val()
	getAramaKanali(arama_kanali_tipi);
});

$("#btnClear").unbind().click(function(){
	location.reload();
})
var crudServiceBaseUrl="/rapor/bildirimdetayraporu";
bildirimGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            			:   { editable: false, nullable: true },
                "vatandas"            		:   { editable: false, nullable: true },
                "tarih"            			:   { editable: false, nullable: true },
                "agent"            			:   { editable: false, nullable: true },
                "konu"          			:   { editable: false, nullable: true },
                "il"          				:   { editable: false, nullable: true },
                "statu"          			:   { editable: false, nullable: true },
                "sure"          			:   { editable: false, nullable: true },
				"fk_agent"					:	{ editable: false, nullable: true },
				"statuClass"				:	{ editable: false, nullable: true },

            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});


function bildirimGridDsFilter(sdate,fdate,tip_level1,fk_il,fk_ilce,fk_havuz,fk_gorev_statu,arama_kanali_tipi,fk_aramakanali,fk_aramasebebi,tip_level2,tip_level3){
	bildirimGridDataSource.filter([
            {
                field: "sdate",
                operator: "eq",
                value: sdate
            },                                                 
            {
                field: "fdate",
                operator: "eq",
                value: fdate
            },                                                                                                 
            {
                field: "fk_il",
                operator: "eq",
                value: fk_il
            },                                                 
            {
                field: "fk_ilce",
                operator: "eq",
                value: fk_ilce
            },                                                                                               
            {
                field: "fk_gorev_statu",
                operator: "eq",
                value: fk_gorev_statu
            },                                                
			{
                field: "fk_havuz",
                operator: "eq",
                value: fk_havuz
            }, 
            {
                field: "arama_kanali_tipi",
                operator: "eq",
                value: arama_kanali_tipi
            },                                                
            {
                field: "fk_aramakanali",
                operator: "eq",
                value: fk_aramakanali
            },                                                
            {
                field: "fk_aramasebebi",
                operator: "eq",
                value: fk_aramasebebi
            },    
			{
                field: "tip_level1",
                operator: "eq",
                value: tip_level1
            },                                                 
            {
                field: "tip_level2",
                operator: "eq",
                value: tip_level2
            },                                                 
            {
                field: "tip_level3",
                operator: "eq",
                value: tip_level3
            },                                             
        ]
    );
}

var bildirimGrid = $("#bildirimGrid").kendoGrid({
    "columns": [

        {
            "field": "id",
            "filterable": false, "groupable": false, "title": "No","width":60
        },
        {
            "field": "vatandas",
            "filterable": false, "groupable": false, "title": "Vatandaş",width:200
        },
        {
            "field": "tarih",
            "filterable": false, "groupable": false, "title": "Tarih"
        },
        {
            "field": "agent",
            "filterable": false, "groupable": false, "title": "Yetkili",
			template:kendo.template($("#yetkiliColTemplate").html())
        },
        {
            "field": "konu",
            "filterable": false, "groupable": false, "title": "Konu",width:200
        },
        {
            "field": "il",
            "filterable": false, "groupable": true, "title": "Şehir",width:100
        },
        {
            "field": "statu",
            "filterable": false, "groupable": true, "title": "Durum",
			template:kendo.template($("#statuColTemplate").html())
        },
        {
            "field": "sure",
            "filterable": false, "groupable": false, "title": "Süre","width":75,
			template:kendo.template($("#sureColTemplate").html())
        },
           
    ],
    "autoBind": false,
    "dataSource":bildirimGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": true,
    "groupable": true,
    "navigatable": false,
    "editable": "false",
    "pageable": {"alwaysVisible": true, "responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [10,20,30,50]},
    "height":800,

});

var grid = $("#bildirimGrid").data("kendoGrid");


$("#btnReport").unbind().click(function(){

	var sdate=$("#sdate").val();
	var fdate=$("#fdate").val();
	var tip_level1=$("#tip_level1 option:selected").val();
	var fk_ilce=$("#fk_sube option:selected").val();
	var fk_havuz=$("#fk_havuz option:selected").val();
	var fk_il=$("#fk_il option:selected").val();
	var fk_gorev_statu=$("#fk_gorev_statu option:selected").val();
	var arama_kanali_tipi=$("#arama_kanali_tipi option:selected").val();
	var fk_aramakanali=$("#fk_aramakanali option:selected").val();
	var fk_aramasebebi=$("#fk_aramasebebi option:selected").val();
	var tip_level2=$("#tip_level2 option:selected").val();
	var tip_level3=$("#tip_level3 option:selected").val();
	if(sdate=="" || fdate==""){
		alertify.error("Tarih Aralığı Alanları Girilmeden Rapor Alınamaz");
		return false;
	}
	bildirimGridDsFilter(sdate,fdate,tip_level1,fk_il,fk_ilce,fk_havuz,fk_gorev_statu,arama_kanali_tipi,fk_aramakanali,fk_aramasebebi,tip_level2,tip_level3);

});




/*****
 * 
 * tarih aralığı validasyonları
 * 
 * ***/
 var tAraligi=Settings.raporTarihAraligi;
var todayDate = kendo.toString(kendo.parseDate(new Date()), 'yyyy-MM-dd');
$("#sdate").data("kendoDatePicker").value(todayDate);
$("#fdate").data("kendoDatePicker").value(todayDate);
function startChange() {
	var startDate = start.value(),
	endDate = end.value();
	startDate = new Date(startDate);
	startDate.setDate(startDate.getDate());
	var edate= new Date(startDate);
	edate.setDate(edate.getDate()+tAraligi);
	end.min(startDate);
	end.max(edate);
}

function endChange() {
	var endDate = end.value(),
		startDate = start.value();
	if (!startDate) {
		endDate = new Date(endDate);
		endDate.setDate(endDate.getDate());
		var edate= new Date(endDate);
		edate.setDate(edate.getDate()-tAraligi);
		start.max(endDate);
		start.min(edate)
	
	}
}

var start = $("#sdate").kendoDatePicker({
	change: startChange,
	format:'yyyy-MM-dd',
	culture:"tr-TR",
}).data("kendoDatePicker");

var end = $("#fdate").kendoDatePicker({
	change: endChange,
	format:'yyyy-MM-dd',
	culture:"tr-TR",
}).data("kendoDatePicker");

start.max(end.value());
end.min(start.value());
$("#sdate,#fdate").keypress(function(){
	return false;
});
</script>

<jsp:include page="Footer.jsp" />
 
 
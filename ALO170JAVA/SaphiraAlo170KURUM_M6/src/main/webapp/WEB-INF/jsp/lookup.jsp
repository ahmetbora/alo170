<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<option value='0'>-- Seciniz --</option>
<c:forEach items="${l}" var="n">
<option data-ext="${n.id}" value="${n.id}" <c:if test="${n.id eq selectedId}"> selected </c:if>>${n.adi}</option>
</c:forEach>
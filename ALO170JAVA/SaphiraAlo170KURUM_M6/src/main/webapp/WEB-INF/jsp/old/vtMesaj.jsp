<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="form-group m-form__group row">
	<label class="col-xl-2 col-lg-2 col-form-label ">Kullanıcı:</label>
	<div class="col-xl-10 col-lg-10">
		<input id="mesaj_user" style="width:100%"/>
	</div>
</div>

<div class="form-group m-form__group row">
	<label class="col-xl-2 col-lg-2 col-form-label">Mesaj:</label>
	<div class="col-xl-10 col-lg-10">
		<textarea class="form-control " name="adres" id="mesaj_text" style="min-height: 40px; text-transform:uppercase" placeholder="Mesaj" rows="5"></textarea>
	</div>
</div>
<div class="form-group m-form__group row">
	<label class="col-sm-2 control-label">&nbsp;</label>
	<div class=" col-sm-8">
		<button class="btn btn-success" id="btn_mesaj_gonder">Mesajı Gönder</button>
	</div>
</div>

<hr>
<div id="vtMesajDetay" class="d-none"></div>
<div id="vtMesajGrid"></div>


<script type="text/x-kendo-template" id="vtMesajToolbar">
    <div class="toolbar">
		<div class="col-md-7 col-sm-12 float-left text-left m--font-info">
			<h4 id="grid_title"></h4>
		</div>
        <div class="col-md-4 col-sm-12 float-right text-right">
            <button class="btn btn-success btn_mesaj_tipi" data-tip="gelen"><span class="k-icon k-i-download"></span>Gelen Mesajlar</button>
            <button class="btn btn-primary btn_mesaj_tipi" data-tip="giden"><span class="k-icon k-i-upload"></span>Gönderilen Mesajlar</button>
        </div>
    </div>
</script>
<script id="vtMesajGridislemlerTemplate" type="text/x-kendo-template">
        <a href="javascript:void(0)" class="btn btn-sm btn-info" onclick="vtMesajDetay('#: id #');">
        <i class="fa fa-search-plus"></i> Detay
        </a>
</script>	
<script id="vtMesajGridMesajDurumTemplate" type="text/x-kendo-template">
#  if (isReplied == '1' ) { #
	<span class="m-badge m-badge--success m-badge--wide" style="width:100px">Cevaplanmış</span>
# } else { #
	# if(isActive=='0') { #
		<span class="m-badge m-badge--danger m-badge--wide" style="width:100px">Okunmamış</span>
	# } else { #
		<span class="m-badge m-badge--metal m-badge--wide" style="width:100px">Okunmuş</span>
	# } #
# } #

</script>	

<script>
$(document).ready(function() {


	var dataSource = new kendo.data.DataSource({
        dataType: 'json',
        serverFiltering: true,
        transport: {
            read: function (options) {
                var filters=options.data.filter.filters;
                var term="";
                if(filters.length>0){
					term=filters[0].value;
                }
                
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    data: JSON.stringify({
                        term: term,
                    }),
                    url: "/bildirim/vtMesajKullaniciListe",
                    contentType: "application/json; charset=utf-8",
                    beforeSend: function(req) {
                        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
                    },
                    success: function (result) {
                        //result.data.unshift({ Key: "0", Value: adi });
                        options.success(result.data);
                        //console.log(result.data);
                    }
                });
            }
        }
    });




    
    $("#mesaj_user").kendoComboBox({
    	placeholder: "Kullanıcı Seç",
        dataTextField: "adi",
        dataValueField: "id",
        filter: "contains",
        autoBind: true,
        minLength: 3,
        dataSource: dataSource,
        //this is done in the dataSource already: placeholder:placeholderText,
        index: 0
    }); 


    var crudServiceBaseUrl="/bildirim/vtMesajListeJSON";
    vtMesajDataSource = new kendo.data.DataSource({
        transport: {
            read:  {
                type: "POST",
                url: crudServiceBaseUrl,
                contentType: "application/json; charset=utf-8",            
    			dataType: 'json',
                beforeSend: function(req) {
                    req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
                }
            },
            
            parameterMap: function(options, operation) {
                return kendo.stringify(options);
                if (operation !== "read" && options.models) {
                    //return {models: kendo.stringify(options.models)};
                }
            }
        },
        batch: true,
        pageSize: 30,
        schema: {
            data:'data', 
            model: {
                id: "id",
                fields: {
                    "id"            :   { editable: false, nullable: true },
                    "kullanici"		:   { editable:false,type: "string" },
                    "tarih"			:   { editable:false,type: "string" },
                    "isActive"		:	{editable:false,type:"string"},
                    "isReplied"		:	{editable:false,type:"string"},
                }
            },
            "total": "total"
        },
        "serverFiltering": true,
        "serverSorting": true,
        "autoSync": true,
        "serverPaging": true
    });

    function vtMesajDsFilter(tip){
    	vtMesajDataSource.filter([
                {
                    field: "tip",
                    operator: "eq",
                    value: tip
                }
            ]
        );
        var title="";
        if(tip=="gelen"){
			title="GELEN MESAJLAR";
        }
        if(tip=="giden"){
			title="GÖNDERİLEN MESAJLAR";
        }
        $("#grid_title").html(title);
    }

    var vtMesajGrid = $("#vtMesajGrid").kendoGrid({
        "columns": [
            {
                "field": "kullanici",
                "filterable": false, "groupable": false, "title": "Kullanıcı",
            },
            {
                "field": "tarih",
                "filterable": false, "groupable": false, "title": "Tarih",
            },
            
            {
                template:kendo.template($("#vtMesajGridMesajDurumTemplate").html())
            },                
            {
                width:130,
                template:kendo.template($("#vtMesajGridislemlerTemplate").html())
            },   
                       
        ],
        "autoBind": false,
        "dataSource":vtMesajDataSource,
        "scrollable": true,
        "sortable": false,
        "persistSelection": false,
        "filterable": false,
        "reorderable": true,
        "resizable": false,
        "columnMenu": false,
        "groupable": false,
        // "rowTemplate": kendo.template($('#row-template').html()),
        "navigatable": false,
        "editable": "false",
        "selectable": "true",
        "pageable": {"alwaysVisible": true, "pageSize": 20,"responsive": false,"input": true,"refresh": true, "info":true,"pageSizes": [30, 50]},
        "height":800,
        "toolbar": kendo.template($("#vtMesajToolbar").html())
    });

    vtMesajDsFilter("gelen");

    $(".btn_mesaj_tipi").click(function(){
		var tip=$(this).attr("data-tip");
		vtMesajDsFilter(tip);
		
	});



    $("#btn_mesaj_gonder").click(function() {
    	var user= $("#mesaj_user").data('kendoComboBox').dataItem();
    	var mesaj=$("#mesaj_text").val();
    	if(mesaj==""){
    		alertify.error("Mesaj Yazınız.");
    		return false;
    	}
    	if(user.id<1){
    		alertify.error("Mesaj Gönderilecek Kullanıcıyı Seçin.");
    		return false;	
    	}
    	$.ajax({
    		type 	: 'POST',
    		dataType: 'json',
    		url		: '/bildirim/vtMesajGonder',
    		data	: "alici="+user.id+"&mesaj="+mesaj,
    		success : function(e){
    			if(e.sonuc=="1"){
    				alertify.success("Mesaj Gönderildi");
    				$("#mesaj_text").val('');
    				vtMesajDsFilter("gelen");
    			}else{
    				alertify.error("Mesaj Gönderilemedi");
    			}
    		},
    	    error: function (request, status, error) {

    	    }
    	});
    });



    
});



$('.nav-tabs a').on('shown.bs.tab', function(event){
  var tab = $(event.target).attr("href");
  	if(tab=="#mesajlar"){
  	   	var vtMesajGrid = $("#vtMesajGrid").data("kendoGrid");
  	   	vtMesajGrid.refresh();
	}
});
function vtMesajDetay(id){
	$.ajax({
		type 	: 'POST',
		url		: '/bildirim/vtMesajDetay',
		data	: "mesajId="+id,
		success : function(e){
			$("#vtMesajDetay").html(e);
			$("#vtMesajDetay").removeClass("d-none");
			
		},
	    error: function (request, status, error) {

	    }
	});	
}




</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="newline" value="
"/>
<jsp:include page="header.jsp" />
<div class="m-content">

	<div class="row">
        
		<div class="col-md-9">
						
			<div class="m-portlet m-portlet--mobile">
        		<div class="m-portlet__body">
			
					<div class="row">
						<div class="col-md-12">
							<table class="table">
								<tr>VATANDA� B�LG�LER� </tr>
							</table>
						</div>	

						<c:choose>
						    <c:when test="${VatandasBilgisiGoster=='true'}">
						     <c:choose>
						    <c:when test="${task.isOnayBekle==1}">
						    	<div class="col-md-12">
									<div class="m-alert m-alert--icon m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
										<div class="m-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="m-alert__text">
											<strong>Dikkat!</strong> Vatanda� bilgilerini g�r�nt�lemek i�in yetkiniz bulunmamaktad�r.
										</div>
									</div>	
								</div>						    	
						    </c:when>  
						    <c:otherwise>
								<div class="col-md-6 col-sm-12">
									<table class="table">
										<tr>
											<td style="width:20%;">Ad� Soyad�</td>
											<td><strong>${kart.kartadi}</strong></td>
										</tr>
										<tr>
											<td>T.C. Kimlik</td>
											<td><strong>${kart.tckimlik}</strong></td>
										</tr>
										<tr>
											<td>Telefon</td>
											<td><strong>${kart.tel1}</strong></td>
										</tr>
										
										<tr>
											<td>GSM</td>
											<td><strong>${kart.gsm}</strong></td>
										</tr>
										
										<tr>
											<td>�lke / �ehir</td>
											<td><strong>${kart.ulkeadi} ${kart.iladi} ${kart.ilceadi}</strong></td>
										</tr>
										<tr>
											<td>Cinsiyet</td>
											<td><strong>${kart.cinsiyet}</strong></td>
										</tr>							
									</table>
								</div>
								
								<div class="col-md-6 col-sm-12">
								<table class="table">
										<tr>
											<td style="width:20%;">EPosta</td>
											<td><strong>${kart.email}</strong></td>
										</tr>
										<tr>
											<td>Fax</td>
											<td><strong>${kart.fax}</strong></td>
										</tr>
										<tr>
											<td>E�itim Durumu</td>
											<td><strong>${kart.edurum}</strong></td>
										</tr>
										
										<tr>
											<td>Do�um Tarihi</td>
											<td><strong>
											
											<c:choose>
											       <c:when test="${kart.dogumtarihi.equals('1900-01-01')}">
											              -
											       </c:when>
											       <c:otherwise>
											              ${kart.dogumtarihi}
											       </c:otherwise>
											  </c:choose>`									  
											</strong>
											</td>
										</tr>
										
										<tr>
											<td>SSK/Ba�kur/Emekli Sand�k No</td>
											<td> ${kart.getSskNo()}</td>
										</tr>
																	
									</table>
								</div>							    
						    </c:otherwise>
						    </c:choose>
					       
						    </c:when>    
						    <c:otherwise>
						    	<div class="col-md-12">
									<div class="m-alert m-alert--icon m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
										<div class="m-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="m-alert__text">
											<strong>Dikkat!</strong> Vatanda� bilgilerini g�r�nt�lemek i�in yetkiniz bulunmamaktad�r.
										</div>
									</div>	
								</div>							        
						    </c:otherwise>
						</c:choose>										
					</div>
				</div>
				</div>
		
			<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<div class="col-md-12">
					<h4 style="color:navy;">${task.id} numarali bildirim<c:if test="${task.havuzAdi ne ''}">, <span class="m--font-warning">${task.havuzAdi }</span> havuzunda</c:if>
					</h4>
					<table class="table">
						<tr>Bildirim T�r� : ${task.konu1} / ${task.konu2} / ${task.konu3}</tr> </br>
						<tr>�ube : </tr> </br>
						<tr>Havuz :  </tr> </br>

					</table>
				</div>
				<hr>
				<div class="col-md-12">
					<span class="text-danger">VT A�IKLAMASI</span>
					<c:if test="${task.isEnc == 1}"><i class="la la-key"></i></c:if> <br>
					<span class="bild_aciklama">${fn:replace(task.aciklama, newline , '<br>')}</span>
				</div>
			</div>
		</div>
			
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<div class="col-md-12">
					<a href="#" onclick="" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Bildirim Sil</a>&nbsp;
					<a href="#" onclick="" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Bildirimi �st�ne Al</a>&nbsp;
					<a href="#" onclick="AciklamaEkle();" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> A��klama Ekle</a>&nbsp;
					<a href="#" onclick="" class="btn btn-warning btn-sm"><i class="fa fa-reply"></i> Y�nlendir</a>&nbsp;
					
					<a href="#" onclick="" class="btn btn-primary btn-sm pull-right"><i class="fa fa-sync"></i> Yeniden A�</a>&nbsp;
					<a href="#" onclick="" class="btn btn-danger btn-sm pull-right"><i class="fa fa-times"></i> Ba�vuru �ptali</a>&nbsp;
				</div>
			</div>
		</div>
			

			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
				
			<div class="m-timeline-3">
				<div class="m-timeline-3__items">
					<c:forEach items="${detay}" var="d" >
              	<div class="m-timeline-3__item m-timeline-3__item--${d.cls} clear-fix">
					<span class="m-timeline-3__item-time">${d.tarih}<br>${d.saat}</span>
					<div class="m-timeline-3__item-desc">
						<span class="m-timeline-3__item-text-baslik">
							${d.agent} / 
							<c:if test="${d.ext ne ''}">${d.ext} -</c:if>
							<c:if test="${d.lokasyon ne ''}">${d.lokasyon} /</c:if>
							 ${d.rol_adi} <span class="text-danger"> 
							<c:if test="${d.islemtipi ne ''}"> / ${d.islemtipi}</c:if>
							 </span>
						</span><br>
						<span class="m-timeline-3__item-text">
						<c:if test="${d.isEnc == 1}"><i class="la la-key"></i></c:if> <br>
							${fn:replace(d.aciklama, newline , '<br>')}
						</span><br>
						<span style="color: red;">${d.cryp_aciklama }</span>
						<span>${d.id }</span>
					</div>
				</div>
                </c:forEach>
				</div>
			</div>
			</div>
            
		</div>
						
		</div>
			
		<div class="col-md-3">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<h2># ${task.id}</h2>
					<span>Bildirim Konusu</span>
					<h5>${task.konu1} / ${task.konu2}</h5>
					
					<span>Kayd� Olu�turan</span>
					<h5>${task.kaydi_acan}</h5>		
					
					<span>Kay�t Olu�turulma Tarihi</span>
					<h5>${task.tarih} ${task.saat}</h5>							
				</div>
			</div>
			
<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
												<i class="flaticon-exclamation-1"></i>
												<span></span>
											</div>
											<div class="m-alert__text">
												<strong>KVKK</strong> Kanun gere�i bildirim g�r�nt�lemeleriniz loglanmaktad�r.
											</div>

										</div>				
			
			<div class="m-portlet m-portlet--mobile">
				CDR BILGILERI
			</div>
			
			</div>
                

    </div>

</div>

<div class="modal fade" id="pnlBildirimDetay">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="fa fa-times" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="bildirimdetaybody">

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
function AciklamaEkle(){
    
        $('#modalLabel').html('Aciklama Ekle');
        $('#pnlBildirimDetay').modal('show');
        $('#bildirimdetaybody').html(ret);

    
}

</script>
<style>
	.bild_aciklama{
		font-size: 14pt !important;
		font-weight: 400 !important;
		line-height: 1.5em !important;
	}

	.m-timeline-3__item-text-baslik{
		font-size: 12pt !important;
		font-weight: 900 !important;
	}

	.m-timeline-3__item-time{
		font-size: 10pt !important;
		font-weight: 900 !important;
	}
</style>


<jsp:include page="footer.jsp" />
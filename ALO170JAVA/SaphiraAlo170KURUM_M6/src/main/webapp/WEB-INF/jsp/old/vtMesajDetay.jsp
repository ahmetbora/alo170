<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show col-md-12" role="alert">
	<div class="m-alert__icon" style="padding:2px!important;">
	</div>
	<div class="m-alert__text">
		<strong>Gönderen</strong> ${mesaj.getGonderen()} / <strong>Alıcı</strong> ${mesaj.geAlici()} / <strong>Tarih</strong> ${mesaj.gettarih()}
		<hr>
		<div style="color:#000!important">
			${mesaj.getmesaj() }
		</div>
		
		<c:if test="${mesaj.getfk_gonderen()!=userId}">
		<div class="mt-3">
			<span class="text-danger">Cevapla :</span>
			<textarea id="txt_mesaj_cevapla" class="form-control"></textarea>
			<br>
			<button class="btn btn-info pull-right" id="btn_cevapla">Cevapla</button>
			<div class="clearfix"></div>		
		</div>
		</c:if>
	</div>

		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>

	
</div>
<script>
$(document).ready(function() {
$("#btn_cevapla").click(function() {

	var mesaj=$("#txt_mesaj_cevapla").val();
	if(mesaj==""){
		alertify.error("Mesaj Yazınız.");
		return false;
	}
	$.ajax({
		type 	: 'POST',
		dataType: 'json',
		url		: '/bildirim/vtMesajGonder',
		data	: "alici=${mesaj.getfk_gonderen()}&mesaj="+mesaj+"&parentId=${mesaj.getid()}",
		success : function(e){
			if(e.sonuc=="1"){
				alertify.success("Mesaj Gönderildi");
				$("#txt_mesaj_cevapla").val('');
				vtMesajDsFilter("gelen");
				$("#vtMesajDetay .m-alert").remove();
			}else{
				alertify.error("Mesaj Gönderilemedi");
			}
		},
	    error: function (request, status, error) {

	    }
	});
});

function vtMesajDsFilter(tip){
	vtMesajDataSource.filter([
            {
                field: "tip",
                operator: "eq",
                value: tip
            }
        ]
    );
    var title="";
    if(tip=="gelen"){
		title="GELEN MESAJLAR";
    }
    if(tip=="giden"){
		title="GÖNDERİLEN MESAJLAR";
    }
    $("#grid_title").html(title);
}


});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <style>
    #bildirimScriptGrid tr[role=row] {
    cursor:pointer;
    }
    </style>
		<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered p-0" >
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							BİLDİRİM SCRİPT LİSTESİ
						</h3>
					</div>
				</div>
			</div>		
			<div class="m-portlet__body" style="height:auto!important">
				<div id="script_oku" class="d-none">
					<button class="btn btn-warning btn-block btn_script_ekle">SCRİPTİ EKLE</button><br>
					<div class=" text-danger" style="font-size:11pt" id="script_kirilim"></div>
					<div class="" id="script_icerik"></div>		
					<div class="text-danger" id="script_aciklama"  style="font-size:11pt"></div>	
					<div id="script_icerigi"></div>
				</div>
				<div id="bildirimScriptGrid">
				
				</div>
			</div>
		</div>	
<script>
$(document).ready(function(){
	initBildirimScriptGrid();

});



function bildirimScriptsetDsFilter(term){
	bildirimScriptDataSource.filter([
            {
                field: "term",
                operator: "eq",
                value: term
            },
            {
                field: "filtre",
                operator: "eq",
                value: ""
            }
        ]
    );
}



function initBildirimScriptGrid(){
    var crudServiceBaseUrl="/bildirim/bildirimScriptListeJSON";
    bildirimScriptDataSource = new kendo.data.DataSource({
        transport: {
            read:  {
                type: "POST",
                url: crudServiceBaseUrl,
                contentType: "application/json; charset=utf-8",            
    			dataType: 'json',
                beforeSend: function(req) {
                    req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
                }
            },
            
            parameterMap: function(options, operation) {
                return kendo.stringify(options);
                if (operation !== "read" && options.models) {
                    //return {models: kendo.stringify(options.models)};
                }
            }
        },
        batch: true,
        pageSize: 30,
        schema: {
            data:'data', 
            model: {
                id: "id",
                fields: {
                    "id"            :   { editable: false, nullable: true },
                    "script"		:   { editable:false,type: "string" },
                    "aciklama"		:   { editable:false,type: "string" },
                    "skAdi"			:   { editable:false,type: "string" },
                }
            },
            "total": "total"
        },
        "serverFiltering": true,
        "serverSorting": true,
        "autoSync": true,
        "serverPaging": true
    });


    var bildirimScriptGrid = $("#bildirimScriptGrid").kendoGrid({
        "columns": [
            {
                "field": "skAdi",
                "filterable": false, "groupable": false, "title": "Script Konusu",
             
            }         
        ],
 
        "dataSource":bildirimScriptDataSource,
        "scrollable": true,
        "sortable": true,
        "persistSelection": false,
        "filterable": false,
        "reorderable": true,
        "resizable": false,
        "columnMenu": false,
        "groupable": false,
        // "rowTemplate": kendo.template($('#row-template').html()),
        "navigatable": false,
        "editable": "false",
        "selectable": "true",
        "pageable": {"alwaysVisible": true, "pageSize": 20,"responsive": false,"input": true,"refresh": true, "info":false,"pageSizes": [30, 50]},
        "height":800,
        change: BildirimScriptOnChange
    });


    function BildirimScriptOnChange(e) {
        var rows = e.sender.select();
        rows.each(function(e) {
            var grid = $("#bildirimScriptGrid").data("kendoGrid");
            var dataItem = grid.dataItem(this);
            var kirilim="";

			if(dataItem.tipLevel3=="0"){
				kirilim=dataItem.knAdi+" -> "+dataItem.konu+" -> "+dataItem.altKonu+" -> (KONUYU SEÇİN)";
			}else{
				kirilim=dataItem.knAdi+" -> "+dataItem.konu+" -> "+dataItem.altKonu+" -> "+dataItem.altKonuDetay;
			}
        	$("#script_kirilim").html("<strong>KIRILIM : "+kirilim+"</strong>");
        	
        	$("#script_icerik").html(nl2br(dataItem.script));
        	$("#script_aciklama").html(dataItem.aciklama);
        	$("#script_icerigi").html(dataItem.script);
        	$("#script_oku").removeClass("d-none");
           // console.log(dataItem);
        })
    }

    function nl2br (str, is_xhtml) {
        if (typeof str === 'undefined' || str === null) {
            return '';
        }
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }

    $(".btn_script_ekle").click(function(){
    	$("#aciklama").val($("#script_icerigi").html());
    });

}
$('.nav-tabs a').on('shown.bs.tab', function(event){
	  var tab = $(event.target).attr("href");
	  	if(tab=="#bildirim_scriptleri"){
	        var bildirimScriptGrid = $("#bildirimScriptGrid").data("kendoGrid");
	        bildirimScriptGrid.refresh();
		}
});
</script>
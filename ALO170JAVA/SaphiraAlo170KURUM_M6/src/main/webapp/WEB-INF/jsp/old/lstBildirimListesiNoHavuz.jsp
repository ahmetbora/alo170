<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="header.jsp" />

<style>
.m-portlet__head{
	padding:2rem 1.2rem!important;
}
.k-grid td {
    padding: .929em 1.286em;
    padding-right: .286em!important;
    padding-left: .286em!important;
	font-size: 13px!important;
    }
.k-filter-row th, .k-grid-header th.k-header {
	padding-top: .286em!important;
	padding-bottom: .286em!important;
    padding-right: .286em!important;
    padding-left: .286em!important;
}   
.k-grid-toolbar{
	padding-top:8px!important;
	padding-bottom:8px!important;
}
.m-portlet__body{
padding:3px!important;
}
</style>
<div class="m-content">

	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation m--font-brand"></i>
		</div>
		<div class="m-alert__text">
			<strong>İŞLEM YAPILMAMIŞ BİLDİRİMLER</strong> Bu Ekranda işlem yapılmamış bildirimler listelenir.
		</div>
	</div>

	<div class="m-portlet m-portlet--responsive-tablet-and-mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">

					<h3 class="m-portlet__head-text">
						Bildirim Havuzu
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<div id=bildirimGrid></div>
		</div>
	</div>
</div>
<script type="text/x-kendo-template" id="bildirimGridToolbar">
    <div class="toolbar">
		<div class="col-md-7 col-sm-12 float-left text-left m--font-info">
			<h4 id="grid_title"></h4>
		</div>
        <div class="col-md-4 col-sm-12 float-right text-right">
            <button class="btn btn-brand btn-sm " id="btnUstuneAl">Üzerime Al</button>
        </div>
    </div>
</script>
<script id="vatandasTemplate" type="text/x-kendo-template">
	# if(isOnayBekle=='0') { #
		#: basvuruSahibi #
	# } else { #
		***** *****
	# } #
</script>

<script id="yapacakAgentTemplate" type="text/x-kendo-template">
	# if(fk_agent=='0') { #
		<span class="m-badge m-badge--danger m-badge--wide">Atanmamış</span>
	# } else { #
		#: isiYapacakAgent #
	# } #
</script>
<script id="konuTemplate" type="text/x-kendo-template">
	#: konu1 # / #: konu2 # / <strong>#: konu3 # </strong>
</script>
<script id="statuTemplate" type="text/x-kendo-template">
	# if(fkGorevStatu=="20"){ #
		<span class="m-badge m-badge--danger m-badge--wide">Yeniden Açılmış</span>
	# } else { #
		<span class="m-badge m-badge--#: cls # m-badge--wide">#: statu #</span>
	# } #
</script>	
<script id="tarihTemplate" type="text/x-kendo-template">
	#: tarih # #: saat #
</script>
<script id="chkTemplate" type="text/x-kendo-template">
<input type="checkbox" id="#: id #" class="k-checkbox chkTask">
<label class="k-checkbox-label" for="#: id #"></label>
</script>
<script id="detayTemplate" type="text/x-kendo-template">
<a class="btn btn-info btn-sm " id="btnDetay" href="/bildirim/bildirimdetay/#: id #">Detay</a>
</script>

<script>

var crudServiceBaseUrl="/bildirim/BildirimListesiNoHavuz";
bildirimGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "tarih"				:   { editable:false,type: "string" },
                "basvuruSahibi"		:   { editable:false,type: "string" },
                "isiYapacakAgent"	:   { editable:false,type: "string" },
                "konuFull"			:   { editable:false,type: "string" },
                "statu"				:   { editable:false,type: "string" },
                "ilAdi"				:   { editable:false,type: "string" },
                "lokasyon"			:   { editable:false,type: "string" },
                "kaydiAcanAgent"	:   { editable:false,type: "string" },
                "isOnayBekle"		:   { editable:false,type: "string" },
                "saat"				:	{ editable:false,type: "string" },
                "fkGorevStatu"		:	{ editable:false,type: "number" },
                
                
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});

function bildirimGridDsFilter(){
	bildirimGridDataSource.filter([
            {
                field: "tip",
                operator: "eq",
                value: ""
            }
        ]
    );

}

var bildirimGrid = $("#bildirimGrid").kendoGrid({
    "columns": [
    	{ 
        	title:"#",
        	template:kendo.template($("#chkTemplate").html()),
			width:40
        },
        {
            "field": "id",
            "filterable": false, "groupable": false, "title": "#",width:85
        },
        {
            template:kendo.template($("#tarihTemplate").html()),
            width:120,
            title:"Tarih"
        },
        {
            template:kendo.template($("#vatandasTemplate").html()),
            title:"Başvuru Sahibi",
            width:200
            
        },  
        {
            template:kendo.template($("#yapacakAgentTemplate").html()),
            title:"İşi Yapacak Agent",
            width:175
        },  
        {
            template:kendo.template($("#konuTemplate").html()),
            title:"Konu",
         
        },  
        {
            template:kendo.template($("#statuTemplate").html()),
            title:"Görev Durumu",
            width:140
        },  
        {
            "field": "ilAdi",
            "filterable": false, "groupable": false, "title": "Şehir",
            width:110
        },
        {
            "field": "lokasyon",
            "filterable": false, "groupable": false, "title": "Lokasyon",
            width:90
                
        },
        {
            "field": "kaydiAcanAgent",
            "filterable": false, "groupable": false, "title": "Kaydı Açan",
            width:200
        },
        {
            template:kendo.template($("#detayTemplate").html()),
            width:75
        }   
                   
    ],
    "autoBind": true,
    "dataSource":bildirimGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": false,
    "navigatable": false,
    "editable": "false",

    "pageable": {"alwaysVisible": true, "pageSize": 20,"responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [30, 50]},
    "height":800,
    "toolbar": kendo.template($("#bildirimGridToolbar").html())
});

var grid = $("#bildirimGrid").data("kendoGrid");

grid.table.on("click", ".k-checkbox" , selectRow);


var checkedIds = {};
function selectRow() {
  var checked = this.checked,
      row = $(this).closest("tr"),
      grid = $("#bildirimGrid").data("kendoGrid"),
      dataItem = grid.dataItem(row);

  checkedIds[dataItem.id] = checked;
  if (checked) {
    //-select the row
    row.addClass("k-state-selected");
  } else {
    //-remove selection
    row.removeClass("k-state-selected");
  }
  console.log(checkedIds);
}

bildirimGrid.find("#btnUstuneAl").on("click",function(e){
    e.preventDefault();
    var checked = [];
    for(var i in checkedIds){
      if(checkedIds[i]){
        checked.push(i);
      }
    }
	
    if(checked.length<1){
		alertify.error("Üzerinize almak istediğiniz bildirim(leri) seçiniz.");
		return false;
    }

	$.ajax({
		type 	: 'POST',
		dataType: 'json',
		url		: '/bildirim/uzerimeAl',
		data	: "ids="+checked,
        beforeSend: function(req) {
            req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
        },
		success : function(e){
			if(e.sonuc=="1"){
				alertify.success("İşlem Başarılı");
			}else{
				alertify.error("Bildirimler Atanamadı.");
			}
			bildirimGridDsFilter();
		},
		
	    error: function (request, status, error) {

	    }
	});
  
});
</script>

<jsp:include page="footer.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<input type="hidden" name="uid" id="uid" value="${uid}">
	<input type="hidden" name="cname" id="cname" value="${que}">
	<input type="hidden" name="cid" id="cid" value="${cid}">
	<input type="hidden" id="fk_sonuckod" name="fk_sonuckod" value="2">
	<input type="hidden" id="fk_kart" name="fk_kart" value="0">
	<input type="hidden" id="firma_bilgi" name="firma_bilgi" value="0">
	<input type="hidden" id="kullanici_id" value="${userID}">
	

	<!--begin: Form Wizard-->
	<div class="m-wizard m-wizard--1 m-wizard--success" id="m_wizard">

		<div class="firma_bilgileri d-none">firma bilgileri girilecek</div>
		
		<!-- mernis loading -->
		<div class="row">
			<div class="col-md-12">
				<div class="m-alert m-alert--outline m-alert--outline-2x alert alert-warning fade show text-center d-none" role="alert" id="mernisload">
					MERNİS SORGUSU YAPILIYOR LÜTFEN BEKLEYİN...
				</div>
				<div id="mernis_content"></div>
			</div>
		</div>		
		<!-- mernis bitti-->
				
		<!-- tc dogrulama loading -->
		<div class="row">
			<div class="col-md-12">
				<div class="m-alert m-alert--outline m-alert--outline-2x alert alert-warning fade show text-center d-none" role="alert" id="dogrulamaload">
					KİMLİK DOĞRULAMA SORUSU GELİYOR LÜTFEN BEKLEYİN...
				</div>
				<div id="dogrulama_content"></div>
			</div>
		</div>		
		<!-- tc dogrulama bitti-->
		
		
		<div class="d-none" id="statu_div"></div>
		
		<!-- özel numaralar kontrolü -->
		<c:if test="${ozelNumraKontrol}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>PRİZMA EMYS AR-GE BİRİMİNDEN ARANMAKTASINIZ. TEST İÇİN ARAMA YAPILDI!</strong>
				</div>
			</div>
		</div>
		</c:if>
		<!-- özel numaralar kontrolü bitti -->
		
		<!-- IVR dan gelen tuşlama kontrolü -->
		<c:if test="${ivrtc[0].menu=='1'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>RAPOR ÜCRETİ, ÇEYİZ  VE EMZİRME ÖDENEKLERİ KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>
		<c:if test="${ivrtc[0].menu=='2'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>SİGORTALILIK HİZMET SÜRESİ KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='3'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>EMEKLİLİK İŞLEMLERİ KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>			
		<c:if test="${ivrtc[0].menu=='4'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>SAĞLIK AKTİVASYONU KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='5'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>İHBAR VE ŞİKAYET KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='6'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>YABANCI ÇALIŞMA İZİNLERİ KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='7'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>DİĞER İŞLEMLER KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='0'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>MADEN VE İŞ KAZALARI KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>	
		<!-- IVR dan gelen tuşlama kontrolü bitti -->
		
													
		<!--begin: Message container -->
		<div class="m-portlet__padding-x">

			<!-- Here you can put a message or alert -->
		</div>

		<!--end: Message container -->

		<!--begin: Form Wizard Head -->
		<div class="m-wizard__head m-portlet__padding-x">

			<!--begin: Form Wizard Progress -->
			<div class="m-wizard__progress">
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>

			<!--end: Form Wizard Progress -->

			<!--begin: Form Wizard Nav -->
			<div class="m-wizard__nav">
				<div class="m-wizard__steps">
					<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
						<div class="m-wizard__step-info">
							<a href="#" class="m-wizard__step-number"  data-step="1">
								<span><span>1</span></span>
							</a>
							<div class="m-wizard__step-line">
								<span></span>
							</div>
							<div class="m-wizard__step-label">
								Vatandaş Bilgileri
							</div>
						</div>
					</div>
					<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
						<div class="m-wizard__step-info">
							<a href="#" class="m-wizard__step-number" data-step="2">
								<span><span>2</span></span>
							</a>
							<div class="m-wizard__step-line">
								<span></span>
							</div>
							<div class="m-wizard__step-label">
								Diğer Bilgiler
							</div>
						</div>
					</div>
					<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
						<div class="m-wizard__step-info">
							<a href="#" class="m-wizard__step-number" data-step="3">
								<span><span>3</span></span>
							</a>
							<div class="m-wizard__step-line">
								<span></span>
							</div>
							<div class="m-wizard__step-label">
								Başvuru Detayları
							</div>
						</div>
					</div>
					<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4">
						<div class="m-wizard__step-info">
							<a href="#" class="m-wizard__step-number" data-step="4">
								<span><span>4</span></span>
							</a>
							<div class="m-wizard__step-line">
								<span></span>
							</div>
							<div class="m-wizard__step-label">
								Firma Bilgileri
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end: Form Wizard Nav -->
		</div>

		<!--end: Form Wizard Head -->

		<!--begin: Form Wizard Form-->
		<div class="m-wizard__form">
			<form class="m-form m-form--fit m-form--label-align-right " id="m_form" autocomplete="off">

				<!--begin: Form Body -->
				<div class="m-portlet__body">

					<!--begin: Form Wizard Step 1-->
					<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
						<div class="row">
							<div class="col-xl-12">
								<div class="m-form__section m-form__section--first">
									<div class="m-form__heading">
										<h3 class="m-form__heading-title">Kişisel Bilgi Seçenekleri</h3>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-xl-2 col-lg-2 col-form-label "><span class="m--font-danger">Kişisel Bilgi Seçeneği:</span></label>
										<div class="col-xl-10 col-lg-10">
											<select name="vbilgi" class="form-control m-input" id="vbilgi">
												<option value="1" selected>Vatandaş Bilgilerini Vermek İstiyor.</option>
												<option value="0" >Vatandaş Bilgilerini Vermek İstemiyor.</option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-xl-2 col-lg-2 col-form-label ">Bilgi Gizliliği:</label>
										<div class="col-xl-10 col-lg-10">
											<select name="isOnayBekle" class="form-control m-input" id="isOnayBekle">
												<option value="1">Vatandaş Bilgileri Gizli Kalsın</option>
												<option value="0" selected>Vatandaş Bilgileri Görüntülenebilsin</option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-xl-2 col-lg-2 col-form-label">Bilgi Güncelle:</label>
										<div class="col-xl-10 col-lg-10">
											<select name="kart_update" class="form-control m-input" id="kart_update">
												<option value="0" selected>Vatandaş Bilgileri Olduğu Gibi Kalsın</option>
												<option value="1">Vatandaş Bilgilerini Güncelle</option>
											</select>
										</div>
									</div>

									<div class="m-form__heading">
										<h3 class="m-form__heading-title">Vatandaş Bilgisi</h3>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Adı:</label>
										<div class="col-xl-4 col-lg-4">
											<input type="text" name="adi" id="adi" class="form-control m-input zorunlu" placeholder="">
										</div>

										<label class="col-xl-2 col-lg-2 col-form-label">Ülke:</label>
										<div class="col-xl-4 col-lg-4">
											<select class="form-control m-input" name="ulkeler" id="ulkeler">
												<option value='0'>--- Seciniz ---</option>
												<c:forEach items="${ulkeler}" var="n">
													<option data-id="${n.id}" value="${n.id}" ${n.adi=="Türkiye" ? "selected" : ""}>${n.adi}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Soyadı:</label>
										<div class="col-xl-4 col-lg-4">
											<input type="text" name="soyadi" id="soyadi" class="form-control m-input zorunlu" placeholder="">
										</div>

										<label class="col-xl-2 col-lg-2 col-form-label">il:</label>
										<div class="col-xl-4 col-lg-4">
											<select class="form-control m-input" name="iller" id="iller">
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Doğum Tarihi:</label>
										<div class="col-xl-4 col-lg-4">
											<input type="text" name="dt" id="dt" class="form-control m-input zorunlu" placeholder="">
										</div>

										<label class="col-xl-2 col-lg-2 col-form-label">İlçe:</label>
										<div class="col-xl-4 col-lg-4">
											<select class="form-control m-input" name="ilceler" id="ilceler">
											</select>
										</div>
									</div>


									<div class="form-group m-form__group row">
										<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> T.C. Kimlik No:</label>
										<div class="col-xl-10 col-lg-10">
											<div class="input-group">
												<input type="text" class="form-control zorunlu" maxlength="11" autocomplete="off" placeholder="T.C. Kimlik No" name="tckimlik" id="tckimlik">
												<div class="input-group-append">
													<button class="btn btn-primary" type="button" id="copyButton" >
														<i class="fa fa-copy"></i>  T.C. Kopyala
													</button>
												</div>
											</div>
											<div id="tcislemler" class="mt-1">
												<a class="btn btn-success btn-md text-white" id="tchange"><i class="fa fa-cog"></i> TC Değiştir</a>
												<a href="javascript:void(0)"  class="btn btn-primary btn-md" id="btnMernis"><i class="fa fa-user"></i> MERNİS</a>
												<a href="javascript:void(0)"  class="btn btn-danger btn-md" id="btnDogrula"><i class="fa 	fa-exclamation-triangle"></i> KİMLİK DOĞRULAMA</a>
											</div>
										</div>
									</div>

								</div>

							</div>
						</div>
					</div>

					<!--end: Form Wizard Step 1-->

					<!--begin: Form Wizard Step 2-->
					<div class="m-wizard__form-step" id="m_wizard_form_step_2">
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Telefon:</label>
							<div class="col-xl-4 col-lg-4">
								<input type="text" name="tel1" id="tel1" class="form-control m-input" placeholder="" value="${cid }">
							</div>

							<label class="col-xl-2 col-lg-2 col-form-label">SSK/Bağkur/Emekli Sandık No:</label>
							<div class="col-xl-4 col-lg-4">
								<input type="text" class="form-control " name="sskno" id="sskno" autocomplete="off" placeholder="SSK/Bağkur/Emekli Sandık No" value="">
							</div>
						</div>
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">GSM:</label>
							<div class="col-xl-4 col-lg-4">
								<input type="text" class="form-control " name="gsm" id="gsm" autocomplete="off" placeholder="GSM / Cep Telefonu" value="">
							</div>

							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger d-none" id="email_zorunlu">*</span> E-Posta:</label>
							<div class="col-xl-4 col-lg-4">
								<input type="text" class="form-control " name="email" id="email" autocomplete="off" placeholder="E-Posta" value="">
							</div>
						</div>		
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Faks:</label>
							<div class="col-xl-4 col-lg-4">
								<input type="text" class="form-control " name="fax" id="fax"  autocomplete="off" placeholder="Faks" value="">	
							</div>

							<label class="col-xl-2 col-lg-2 col-form-label">Cinsiyet:</label>
							<div class="col-xl-4 col-lg-4">
							   	<select class="form-control" name="fk_cinsiyet" id="fk_cinsiyet">
									<option value='0'>--- Seciniz ---</option>
									<c:forEach items="${cinsiyet}" var="n">
										<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
									</c:forEach>										   	
							   	</select>	
							</div>
						</div>		
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Eğitim Durumu:</label>
							<div class="col-xl-4 col-lg-4">
							   	<select class="form-control" name="fk_egitimdurumu" id="fk_egitimdurumu">
									<option value='0'>--- Seciniz ---</option>
									<c:forEach items="${egitimdurumu}" var="n">
										<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
									</c:forEach>											   		
							   	</select>	
							</div>

						</div>		
						
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Geri Dönüş Kanalı:</label>
							<div class="col-xl-10 col-lg-10">
							   	<select class="form-control" name="fk_geri_donus_kanali" id="fk_geri_donus_kanali">
									<option value='0'>--- Seciniz ---</option>
									<c:forEach items="${geridonuskanali}" var="n">
										<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
									</c:forEach>											   										
							   	</select>	
							</div>

						</div>	
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Adres:</label>
							<div class="col-xl-10 col-lg-10">
								<textarea class="form-control " name="adres" id="adres" style="min-height: 40px; text-transform:uppercase" placeholder="Adres" rows="5"></textarea>
							</div>

						</div>		
						
						<div class="form-group m-form__group row " style="padding-top:0;padding-bottom:0">
							<label class="col-xl-2 col-lg-2 col-form-label">&nbsp;</label>
							<div class="col-xl-10 col-lg-10">
								<label class="m-checkbox">
									<input type="checkbox"> Özel Liste
									<span></span>
								</label>
							</div>

						</div>	
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">Güvenlik Sorusu:</span></label>
							<div class="col-xl-10 col-lg-10">
							   	<select class="form-control" name="fk_gsoru" id="fk_gsoru">
									<option value='0'>--- Seciniz ---</option>
									<c:forEach items="${guvenliksorusu}" var="n">
										<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
									</c:forEach>											   		
							   	</select>
							</div>

						</div>	
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">Cevap:</span></label>
							<div class="col-xl-10 col-lg-10">
								<textarea class="form-control text-danger" name="gsoru_cevap" id="gsoru_cevap" style="min-height: 40px;" placeholder="Cevap" ></textarea>
							</div>

						 </div>																																																																			
					</div>

					<!--end: Form Wizard Step 2-->

					<!--begin: Form Wizard Step 3-->
					<div class="m-wizard__form-step" id="m_wizard_form_step_3">
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Arayan Numara:</label>
							<div class="col-xl-10 col-lg-10">
								<input type="text" id="arayan_numara" name="arayan_numara" class="form-control zorunlu " placeholder="Arayan Numara"  value="${cid }">
							</div>
						 </div>
						 
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Arama Kanalı Türleri:</label>
							<div class="col-xl-10 col-lg-10">
						   		<select id="arama_kanali_tipi" name="arama_kanali_tipi" placeholder="Arama Kanalı Türü" class="form-control zorunlu">
						   			<option value="">Seçiniz</option>
									<c:forEach items="${aramakanalitipi}" var="n">					
										<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
									</c:forEach>									   			
						   		</select>											
							</div>
						 </div>	
						 
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Arama Kanalı:</label>
							<div class="col-xl-10 col-lg-10">
							   	<select class="form-control zorunlu" name="fk_aramakanali" id="fk_aramakanali" placeholder="Arama Kanalı">
							   			<option value="">--Seçiniz--</option>
							   	</select>												
							</div>
						 </div>	
						 
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Arama Sebebi:</label>
							<div class="col-xl-10 col-lg-10">
								<select class="form-control zorunlu" name="fk_aramasebebi" id="fk_aramasebebi" placeholder="Arama Sebebi">
							   		<option value="">--Seçiniz--</option>
									<c:forEach items="${aramasebebi}" var="n">
										<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
									</c:forEach>										   												   
							   	</select>
							</div>
						 </div>	
						 
						<div class="form-group m-form__group row d-none">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Bildirim Tipi:</label>
							<div class="col-xl-10 col-lg-10">
							   	<select class="form-control" name="fk_tip" id="fk_tip" placeholder="Bildirim Tipi">
							   		
							   	</select>											
							</div>
						 </div>	
						 
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Kurum:</label>
							<div class="col-xl-10 col-lg-10">
								<select class="form-control zorunlu" name="tip_level1" id="tip_level1" placeholder="Kurum" >
							   		<option value="">--Seçiniz--</option>
									<c:forEach items="${kurum}" var="n">
										<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
									</c:forEach>													
								</select>											
							</div>
						 </div>	
						 
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Alt Kurum:</label>
							<div class="col-xl-10 col-lg-10">
								<select class="form-control zorunlu" name="tip_level2" id="tip_level2" placeholder="Alt Kurum" >
								</select>											
							</div>
						 </div>		
						 
						<div class="form-group m-form__group row d-none tip_level3">
							<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Arama Konusu:</label>
							<div class="col-xl-10 col-lg-10">
								<select class="form-control zorunlu" name="tip_level3" id="tip_level3" placeholder="Başvuru Açıklaması" >
								</select>											
							</div>
						 </div>		
						 
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Başvuru Açıklaması:</label>
							<div class="col-xl-10 col-lg-10">
								<textarea class="form-control" name="aciklama" id="aciklama" style="min-height: 150px;" placeholder="Başvuru Açıklaması" ></textarea>										
							</div>
						</div>										 								 								 									 									 									 									 									 										
					</div>

					<!--end: Form Wizard Step 3-->

					<!--begin: Form Wizard Step 4-->
					<div class="m-wizard__form-step" id="m_wizard_form_step_4">
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Kurum / Kişi Adı:</label>
							<div class="col-xl-10 col-lg-10">
								<input type="text" class="form-control" name="f_adi" id="f_adi"  placeholder="Firma Adı">																		
							</div>
						</div>	
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Sektör:</label>
							<div class="col-xl-10 col-lg-10">
							   	<select class="form-control " name="fk_proje" id="fk_proje" placeholder="Sektör" style="width:100%">
							   		<option value="">--Seçiniz--</option>
									<c:forEach items="${sektorler}" var="n">
										<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
									</c:forEach>											
							   	</select>																		
							</div>
						</div>
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Adres:</label>
							<div class="col-xl-10 col-lg-10">
								<textarea class="form-control" name="f_adres" id="f_adres" rows="3" placeholder="Firma Adresi"></textarea>																		
							</div>
						</div>
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Telefon:</label>
							<div class="col-xl-10 col-lg-10">
								<input type="text" class="form-control phone" name="f_tel" id="f_tel"  placeholder="Firma Telefonu">																		
							</div>
						</div>
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">İl:</label>
							<div class="col-xl-10 col-lg-10">
							   	<select class="form-control" name="f_fk_il" id="f_fk_il" placeholder="Firma İl" style="width:100%">
									<option value='0'>--- Seciniz ---</option>
									<c:forEach items="${iller}" var="n">
										<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
									</c:forEach>													
							   	</select>	
							   																	
							</div>
						</div>
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">İlçe:</label>
							<div class="col-xl-10 col-lg-10">
							   	<select class="form-control" name="f_fk_ilce" id="f_fk_ilce"  placeholder="Firma İlçe">
							   	
							   	</select>																	
							</div>
						</div>
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">NACE Kodu:</label>
							<div class="col-xl-10 col-lg-10">
								<input type="text" class="form-control" name="nace" id="nace" placeholder="NACE Kodu">																		
							</div>
						</div>
						
						<div class="form-group m-form__group row">
							<label class="col-xl-2 col-lg-2 col-form-label">Sigortasız Çalışma Süresi(Ay):</label>
							<div class="col-xl-10 col-lg-10">
								<input type="text" class="form-control" name="scs" id="scs" placeholder="Sigortasız Çalışma Süresi(Ay)" >																		
							</div>
						</div>																																																															
					</div>

					<!--end: Form Wizard Step 4-->
				</div>

				<!--end: Form Body -->

				<!--begin: Form Actions -->
				<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
					<div class="m-form__actions m-form__actions form_islemler">

							<button id="" type="button" class="btn btn-md btn-primary BildirimFormDBBTN float-left" data-kod="1" data-kodd="kaydet" >Mevzuat Uzmanına Aktar</button>
							<button id="" type="button" class="btn btn-primary BildirimFormDBBTN btn-md float-left ml-2" data-kod="2" data-kodd="kaydetkapat">Kaydet Ve Kapat</button>
							<button type="reset" id="BildirimFormResetBTN" class="btn btn-warning btn-md float-left ml-4" style="margin-left:30px">iptal</button>	
							<button id="" type="button" class="btn btn-danger btn_karaliste pull-right btn-md float-left ml-2 mr-2" style="margin-right:30px" data-kod="karaliste">Kara Liste</button>
							<button id="" type="button" class="btn btn-metal btn_belirsiz_cagri pull-right btn-md float-left" style="margin-right:30px" data-kod="belirsiz">Belirsiz Çağrı</button>

							<button class="btn btn-outline-info m-btn m-btn--custom m-btn--icon btn-md float-right" id="btn_next" style="float:right!important">
								<span>
									<span>İlerle </span>&nbsp;&nbsp;
									<i class="la la-arrow-right"></i>
								</span>
							</button>
							<button class="btn btn-outline-info m-btn m-btn--custom m-btn--icon  btn-md float-right" data-wizard-action="prev" style="margin-right:50px">
								<span>
									<i class="la la-arrow-left"></i>&nbsp;&nbsp;
									<span>Önceki</span>
								</span>
							</button>	
																																				
					</div>
				</div>

				<!--end: Form Actions -->
			</form>
		</div>

		<!--end: Form Wizard Form-->
	</div>

	<!--end: Form Wizard-->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="m-widget3">
<c:forEach items="${bilgibankasiListe}" var="l">
	<div class="m-widget3__item">
		<div class="m-widget3__header">
			<div class="m-widget3__info">
				<span class="m-widget3__username">
					${l.baslik}
				</span>
			</div>
			<span class="m-widget3__status m--font-info">
				<a class="btn btn-sm btn-info" href="javascript:void(0);"><i class="fa fa-search"></i> Detay</a>
			</span>
		</div>
	</div>	
</c:forEach>
</div>
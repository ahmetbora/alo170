<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<style>
.wsLink:hover .m-nav__link-text {
color:#34bfa3!important;
}
.wsLink:hover{
color:#34bfa3!important;
    border-radius: 2rem;
}
.aktif{
background:#eaeaea!important;
color:#34bfa3!important;
border-radius: 2rem;
}
.aktif span{
color:#34bfa3!important;
}
</style>
		<div class="m-portlet m-portlet--responsive-mobile">
			<div class="m-portlet__body">
	            <div class="details">
	                <div class="number">
	                    <h2 id="sayac_basla" class="m--font-primary"></h2>
	                </div>
	                <div class="desc"><h2 class="m--font-primary">Görüşme Süreniz</h2></div>
	            </div>	            		
			</div>
		</div>
		<c:if test="${webServisGoster==true}">
		<div class="m-portlet m-portlet--responsive-mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<i class="flaticon-technology m--font-brand"></i>
						</span>
						<h3 class="m-portlet__head-text m--font-brand">
							WEB SERVİSLERİ
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					
				</div>
			</div>
			<div class="m-portlet__body">
				<ul class="m-nav m-nav--active-bg" id="m_nav" role="tablist">

					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_1"   aria-expanded=" true">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text">4A SERVİSLERİ</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_1" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="raporBilgileriSorgula" id="raporBilgileriSorgula" data-text="Rapor Bilgileri Sorgula">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">RAPOR BİLGİLERİ SORGULA</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="sigortaliHizmetDokumu" id="sigortaliHizmetDokumu" data-text="4A Hizmet Bilgisi">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A HİZMET BİLGİSİ</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="tescilKaydi4a" data-text="4A Tescil Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A TESCİL KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="emekliKaydi4a" data-text="4A Emekli Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A EMEKLİ KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="hizmetBorclanma4A" data-text="4A Hizmet Borçlanma">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A HİZMET BORÇLANMALARI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="tescilKaydi4aDetay" data-text="4A Detaylı Tescil Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A DETAYLI TESCİL KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="tumTescilVeKimlikBilgileriniSorgula4A" data-text="Tüm Tescil Bilgilerini Sorgula">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">TÜM SİCİL BİLGİLERİNİ SORGULA</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="sigortaliAyrilisSorgula4A" data-text="4A Sigortalı Ayrılış Sorgula">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A SİGORTALI AYRILIŞ SORGULA</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="sigortaliTescilSorgula4A" data-text="4A Sigortalı İşe Giriş Sorgula">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A SİGORTALI İŞE GİRİŞ SORGULA</span>
								</a>
							</li>
																																																																	
						</ul>
					</li>


					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_2"   aria-expanded=" false">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text">4B SERVİSLERİ</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_2" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Borç Durumu"  data-ws="borcDurumu4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B BORÇ DURUMU</span>
								</a>
							</li>		
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Tescil Kaydı"  data-ws="tescilKaydi4b">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B TESCİL KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Basamak Listesi"  data-ws="basamakListesi4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B BASAMAK LİSTESİ</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Hizmet Bilgisi"  data-ws="hizmetBilgisi4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B HİZMET BİLGİSİ</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Ödeme Dökümü"  data-ws="odemeDokumu4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B ÖDEME DÖKÜMÜ</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Emekli Kaydı"  data-ws="emekliKaydi4b">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B EMEKLİ KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B - 6736 Yapılandırma"  data-ws="yapilandirmaBorcBilgisi4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B - 6736 YAPILANDIRMA</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B - 7020 Yapılandırma"  data-ws="yapilandirma7020BorcBilgisi4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B - 7020 YAPILANDIRMA</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Hizmet Borçlanmaları"  data-ws="hizmetBorclanma4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B HİZMET BORÇLANMALARI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Hizmet Bilgisi" data-ws="sigortaliHizmetDokumu4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text text-danger">4B Hizmet Dökümü (TEST)</span>
								</a>
							</li>																																																																
						</ul>
					</li>
					
					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_3"   aria-expanded=" false">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text">4C SERVİSLERİ</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_3" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="tescilKaydi4c" data-text="4C Tescil Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4C TESCİL KAYDI</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="emekliKaydi4c" data-text="4C Emekli Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4C EMEKLİ KAYDI</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="hizmetBorclanma4C" data-text="4C Hizmet Borçlanmaları">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4C HİZMET BORÇLANMALARI</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="sigortaliHizmetDokumu4C" data-text="4C Hizmet Dökümü">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4C HİZMET DÖKÜMÜ</span>
								</a>
							</li>																								
						</ul>
					</li>
					
					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_1"   aria-expanded=" false">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text">ORTAK SERVİSLER</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_4" role="tabpanel" aria-labelledby="m_nav_link_4" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Sağlık Aktivasyonu Sorgulama"  data-ws="spas">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">SAĞLIK AKTİVASYONU SORGULAMA</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Emekli Ödeme Sorgulama"  data-ws="odemedurumbilgisiemekli">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">EMEKLİ ÖDEME SORGULAMA (EÖS)</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Emekli Kesinti Sorgulama"  data-ws="kesintibilgisiemekli">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">EMEKLİ KESİNTİ SORGULAMA (EKS)</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Merkezi Ödeme Sorgulama"  data-ws="odemedurumbilgisi">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">MERKEZİ ÖDEME SORGULAMA (MÖS)</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Emekli Hareket Sorgulama"   data-ws="emeklihareket">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">EMEKLİ HAREKET SORGULAMA (EHS)</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Evrak Sorgulama"   data-ws="evraksorgulama">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">EVRAK TAKİP (DYS) SORGULAMA</span>
								</a>
							</li>																																						
						</ul>
					</li>
					
					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_1"   aria-expanded=" false">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text">KULLANILAN DİĞER SERVİSLER</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_5" role="tabpanel" aria-labelledby="m_nav_link_5" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="https://app2.csgb.gov.tr/yabancilar/faces/login" class="m-nav__link" target="_blank">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">YABANCILAR ÇALIŞMA İZİNLERİ DAİRE BAŞKANLIĞI</span>
								</a>
							</li>			
							<li class="m-nav__item">
								<a href="http://cc.iskur.gov.tr/CallCenter.aspx" class="m-nav__link" target="_blank">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">İŞKUR ÇAĞRI MERKEZİ İŞLEMLERİ</span>
								</a>
							</li>								
						</ul>
					</li>					
				</ul>
						
			</div>
		</div>	
		</c:if>
		
		
		
				<div class="d-none">											
			<div class="m-portlet">
				<div class="m-portlet__head alert alert-info">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text text-white">
								4A SERVİSLERİ
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body" style="padding-top: 0px !important;">
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> RAPOR BİLGİLERİNİ SORGULA</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4A HİZMET BİLGİSİ</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4A TESCİL KAYDI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4A EMEKLİ KAYDI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4A HİZMET BORÇLANMALARI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4A DETAYLI TESCİL KAYDI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> TÜM SİCİL BİLGİLERİNİ SORGULA</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4A SİGORTALI AYRILIŞ SORGULA</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4A SİGORTALI İŞE GİRİŞ SORGULA</a>
				</div>
			</div>

			<div class="m-portlet">
				<div class="m-portlet__head alert alert-info">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text text-white">
								4B SERVİSLERİ
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body" style="padding-top: 0px !important;">
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B BORÇ DURUMU</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B TESCİL KAYDI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B BASAMAK LİSTESİ</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B HİZMET BİLGİSİ</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B ÖDEME DÖKÜMÜ</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B EMEKLİ KAYDI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B - 6736 YAPILANDIRMA</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B - 7020 YAPILANDIRMA</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B HİZMET BORÇLANMALARI</a>
					<a href="#" onclick="" class="btn btn-danger btn-block"><i class="fa fa-chevron-circle-right"></i> 4B Hizmet Dökümü (TEST)</a>
				</div>
			</div>

			<div class="m-portlet">
				<div class="m-portlet__head alert alert-info">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text text-white">
								4C SERVİSLERİ
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body" style="padding-top: 0px !important;">
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B TESCİL KAYDI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B EMEKLİ KAYDI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B HİZMET BORÇLANMALARI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> 4B HİZMET DÖKÜMÜ</a>

				</div>
			</div>

			<div class="m-portlet">
				<div class="m-portlet__head alert alert-info">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text text-white">
								ORTAK SERVİSLER
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body" style="padding-top: 0px !important;">
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i>	SAĞLIK AKTİVASYONU SORGULAMA</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> EMEKLİ ÖDEME SORGULAMA (EÖS)</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> EMEKLİ KESİNTİ SORGULAMA (EKS)</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> MERKEZİ ÖDEME SORGULAMA (MÖS)</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md"><i class="fa fa-chevron-circle-right"></i> EMEKLİ HAREKET SORGULAMA (EHS)</a>

				</div>
			</div>

			<div class="m-portlet">
				<div class="m-portlet__head alert alert-info">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text text-white">
								KULLANILAN DİĞER SERVİSLER
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body" style="padding-top: 0px !important;">
					<a href="#" onclick="" class="btn btn-success btn-block btn-md">YABANCILAR ÇALIŞMA İZİNLERİ DAİRE BAŞKANLIĞI</a>
					<a href="#" onclick="" class="btn btn-success btn-block btn-md">İŞKUR ÇAĞRI MERKEZİ İŞLEMLERİ</a>


				</div>
			</div>
			</div>
			<script>
				$(".wsLink").click(function(){
					var tckn=$("#tckimlik").val();
					if(tckn.length!=11){
						alertify.error("T.C. Kimlik Numarası Hatalı.");
						return false;
					}
					var datatext=$(this).attr("data-text");
					$("li#webservis a").html(datatext);
					$("li#webservis").removeClass("hidden");
					$("#webservis_sonuc").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
					$(".aktif").removeClass("aktif");
					$('.nav-tabs a[href="#webserviscont"]').tab('show');
					$(this).addClass("aktif");
					
					if($(this).attr("data-ws")=="odemedurumbilgisiemekli"){
						$.ajax({

							type 	: 'POST',
							url		: '//'+location.host+'/bildirim/bildirim/ws-odemebilgiemekli',
							data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
							success : function(e){
								$("#webservis_sonuc").html(e);
								$("#tcemekli").val(tckn);
								//$("#wssonuc").html(e);
								$("#btn_go").click(function(){
									if($("#tc").val()==""){
										alertify.error("Hatalı T.C.");
										return false;
									}
									if($("#ay").val()==""){
									alertify.error("Dönem Ay Bilgisi Girin");
									return false;
								}
									if($("#yil").val()==""){
									alertify.error("Dönem Yıl Bilgisi Girin");
									return false;
								}
									var tckn=$("#tcemekli").val();
									var ay=$("#ay").val();
									var yil=$("#yil").val();

									$("#btn_go").attr("disabled",true);
									$("#sonuc_emekli").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
									$.ajax({
										type	: 'POST',
										url		: 'http://172.30.10.253/tools/odemedurumbilgisiemekli.php',
										data	: "tckn="+tckn+"&ay="+ay+"&yil="+yil+"&fk_agent=${userID}&ip=${ipAdres}",
										success : function(r){
											$("#sonuc_emekli").html(r);
											$("#btn_go").attr("disabled",false);
										}
									});

								});

							}
						});
					} else if($(this).attr("data-ws")=="spas"){

						$.ajax({

							type 	: 'POST',
							url		: '//'+location.host+'/bildirim/bildirim/ws-spas',
							data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
							success : function(e){
								$("#webservis_sonuc").html(e);
								$("#tcemekli").val(tckn);
								//$("#wssonuc").html(e);
								$("#btn_go").click(function(){
									if($("#tc").val()==""){
										alertify.error("Hatalı T.C.");
										return false;
									}
								/*	if($("#spastarih").val()==""){
									alertify.error("Tarih Seçin");
									return false;
								}*/
									var tckn=$("#tcemekli").val();
									var spastarih=$("#spastarih").val();

									$("#btn_go").attr("disabled",true);
									$("#sonuc_saglik").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
									$.ajax({
										type	: 'POST',
										url		: 'http://172.30.10.253/tools/spas.php',
										data	: "tckn="+tckn+"&tarih="+spastarih+"&fk_agent=${userID}&ip=${ipAdres}",
										success : function(r){
											$("#sonuc_saglik").html(r);
											$("#btn_go").attr("disabled",false);
										}
									});

								});

							}
						});
					} else 	if($(this).attr("data-ws")=="kesintibilgisiemekli"){

						$.ajax({

							type 	: 'POST',
							url		: '//'+location.host+'/bildirim/bildirim/ws-kesintiemekli',
							data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
							success : function(e){
								$("#webservis_sonuc").html(e);
								$("#tcemekli").val(tckn);
								//$("#wssonuc").html(e);
								$("#btn_go").click(function(){
									if($("#tc").val()==""){
										alertify.error("Hatalı T.C.");
										return false;
									}
									if($("#ay").val()==""){
									alertify.error("Dönem Ay Bilgisi Girin");
									return false;
								}
									if($("#yil").val()==""){
									alertify.error("Dönem Yıl Bilgisi Girin");
									return false;
								}
									if($("#kisitip").val()==""){
										alertify.error("Emekli türünü seçin");
										return false;
									}
									var tckn=$("#tcemekli").val();
									var ay=$("#ay").val();
									var yil=$("#yil").val();
									var kisitip=$("#kisitip").val();

									$("#btn_go").attr("disabled",true);
									$("#sonuc_kesinti").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
									$.ajax({
										type	: 'POST',
										url		: 'http://172.30.10.253/tools/kesintibilgisiemekli.php',
										data	: "tckn="+tckn+"&ay="+ay+"&yil="+yil+"&kisitip="+kisitip+"&fk_agent=${userID}&ip=${ipAdres}",
										success : function(r){
											$("#sonuc_kesinti").html(r);
											$("#btn_go").attr("disabled",false);
										}
									});

								});

							}
						});
					} else if($(this).attr("data-ws")=="evraksorgulama"){

						$.ajax({

							type 	: 'POST',
							url		: '//'+location.host+'/bildirim/bildirim/ws-evraksorgulama',
							data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
							success : function(e){
								$("#webservis_sonuc").html(e);
								$("#tcevrak").val(tckn);
								//$("#wssonuc").html(e);
								$("#btn_go").click(function(){
									if($("#tcevrak").val()==""){
										alertify.error("Hatalı T.C.");
										return false;
									}

									if($("#varideyil").val()==""){
									alertify.error("Varide / Evrak Yıl Bilgisi Girin");
									return false;
								}
									var tckn=$("#tcevrak").val();
									var varideno=$("#varideno").val();
									var varideyil=$("#varideyil").val();

									$("#btn_go").attr("disabled",true);
									$("#sonuc_evrak").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
									$.ajax({
										type	: 'POST',
										url		: 'http://172.30.10.253/tools/EvrakDetaySorgusu.php',
										data	: "tckn="+tckn+"&varideno="+varideno+"&varideyil="+varideyil+"&fk_agent=${userID}&ip=${ipAdres}",
										success : function(r){
											$("#sonuc_evrak").html(r);
											$("#btn_go").attr("disabled",false);
										}
									});

								});

							}
						});
					}else{
						$.ajax({

							type 	: 'POST',
							url		: 'http://172.30.10.253/tools/' +$(this).attr('data-ws')+ '.php',
							data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
							success : function(e){
								$("#webservis_sonuc").html(e);
								if($("#rapor_sonuc").val()=="1"){

									$.ajax({

										type 	: 'POST',
										url		: 'http://172.30.10.253/tools/odemedurumbilgisi.php',
										data	: "tckn="+$("#tckimlik").val(),
										success : function(e){

											$("#odemesonucrapor").html(e);


										}
									});
								}
								//$("#wssonuc").html(e);

							}
						});
					}					
					return false;
				});
			</script>
		
<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<jsp:include page="header.jsp" />
		
<script src="<c:url value="/tools/default/custom/crud/forms/widgets/bootstrap-datetimepicker.js"/>" type="text/javascript"></script>


                 
	<div class="row" style="padding-top: 10px;">
        <div class="col-md-3">
        
        <div class="m-portlet">					
			<div class="m-portlet__body">
				<div class="form-group m-form__group">
					<strong>B�LD�R�M SORGULAMA</strong> <hr/>
					<div class="row m-stack__items--right" style="padding-left: 3px;">
						<a href="#" onclick="RaporAl()" class="btn btn-brand btn-block"><i class="fa fa-search"></i> Rapor Al</a>
					</div>
				</div> 

            </div>
        </div>
        
        <div class="m-portlet">
        	<div class="m-portlet__body">
        		<div class="form-group m-form__group">
					<label for="statu">Stat�</label>
				            <form:select path="statu" id="statu" class="form-control m-select2">
							  <form:option value="" label="--- T�m� ---" />
							  <form:options items="${statu}" itemValue="id" itemLabel="adi" />
	      				    </form:select>
			    </div> 
		    	<div class="form-group m-form__group">
			    	<label for="adi">�sim</label>
						<input type="text" name="adi" id="adi" value=""
						class="form-control  m-input" placeholder="ADI"/>
				</div> 
				<div class="form-group m-form__group">
		    	<label for="soyadi">Soyisim</label>
					<input type="text" name="soyadi" id="soyadi" value=""
					class="form-control  m-input" placeholder="SOYADI"/>
				</div> 
					    <div class="form-group m-form__group">
		    	<label for="tel">Telefon</label>
					<input type="text" name="tel" id="tel" value=""
					class="form-control  m-input" placeholder="TELEFON"/>
				</div> 
	    		<div class="form-group m-form__group">
		    	<label for="tc">T.C. Kimlik</label>
					<input type="text" name="tc" id="tc" value=""
					class="form-control  m-input" placeholder="T.C. K�ML�K"/>
				</div>
				<div class="form-group m-form__group">
		    	<label for="bildirim_no">Bildirim No</label>
					<input type="text" name="bildirim_no" id="bildirim_no" value=""
					class="form-control  m-input" placeholder="B�LD�R�M NO"/>
				</div>
				<div class="form-group m-form__group">
					<a href="#" onclick="Excel()" class="btn btn-outline-info btn-sm"><i class="la la-file-code-o"></i>XML</a>&nbsp;
					<a href="/bildirim/bildirimara?type=pdf" onclick="" class="btn btn-outline-info btn-sm"><i class="la la-file-pdf-o"></i>PDF</a>&nbsp;
					<a href="#" onclick="ExcelAl()" class="btn btn-outline-info btn-sm"><i class="la la-file-excel-o"></i>XLS</a>
					<spring:url value="/bildirim/bildirimara?type=pdf" var="pdfURL"/>
					<spring:url value="/bildirim/bildirimara?type=xls" var="xlsURL"/>
					
				</div>
        	</div>
        </div>
 
    	</div>
  
               <div class="col-md-9">
                 <div class="m-portlet">				
					<div class="m-portlet__body" id="bildirimara">
					
					</div>
	  			</div>
                </div>         

</div>





<script>
$(document).ready(function(){

	$("#statu").select2({ placeholder: "--- T�m� ---", minimumResultsForSearch: 1 / 0 });

});

function RaporAl() {
	$('#bildirimara').html("");
	var data = {
			
				"statu" : $('#statu').val()
				,"adi" : $('#adi').val()
				, "soyadi" : $('#soyadi').val()
				, "tc" : $('#tc').val()
				, "tel" : $('#tel').val()
				, "bildirim_no" : $('#bildirim_no').val()

	};
	$.post("/bildirim/bildirimara?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#bildirimara').html(data);
		});


}

function Excel(){
	var blob = new Blob([result], {type:'text/html'});
    var url  = window.URL.createObjectURL(blob);
    var link = document.createElement('a');
    document.body.appendChild(link);
    link.href = url;
    link.download="test.xls";
    link.click();
}

function ExcelAl(){
	var data = {
			
			"statu" : $('#statu').val()
			,"adi" : $('#adi').val()
			, "soyadi" : $('#soyadi').val()
			, "tc" : $('#tc').val()
			, "tel" : $('#tel').val()
			, "bildirim_no" : $('#bildirim_no').val()
			, "type" : 'xls'

};

	
	$.post("/bildirim/export?${_csrf.parameterName}=${_csrf.token}", data, function(result){
		
	});
}
</script>
<jsp:include page="footer.jsp" />
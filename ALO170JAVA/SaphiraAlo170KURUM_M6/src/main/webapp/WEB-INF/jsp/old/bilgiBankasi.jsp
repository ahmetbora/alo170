<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
		<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered p-0" >
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							SORU LİSTESİ
						</h3>
					</div>
				</div>
			</div>		
			<div class="m-portlet__body" style="height:auto!important">
				<div class="form-group m-form__group">					
					<div class="input-group">
						<div class="input-group-prepend">
							<button class="btn btn-danger" type="button" id="sss_temizle" onclick="filtreKaldir();">Temizle</button>
						</div>
						<input type="text" class="form-control" placeholder="Bilgi Bankası Arama..." id="sss_term">
						<div class="input-group-append">
							<button class="btn btn-primary" type="button" id="sss_ara" onclick="sssAra()">Ara</button>					
						</div>
					</div>
				</div>	
				<div id="grid">
				</div>
				<div id="bilgi_bankasi_detay">
				</div>	
         
			</div>
		</div>	
<script id="islemlerTemplate" type="text/x-kendo-template">
        <a href="javascript:void(0)" class="btn btn-sm btn-info" onclick="sssDetay('#: id #');">
        <i class="fa fa-search-plus"></i> Detay
        </a>
</script>	
<script>

$(document).ready(function(){
	initGrid();
});



function sssDetay(id){
	$.ajax({
		type 	: 'POST',
		url		: '/bildirim/bilgiBankasiDetay',
		data	: "sssId="+id,
		success : function(e){
			$("#bilgi_bankasi_detay").html(e);
			$("#grid").hide();
			$("#bilgi_bankasi_detay").show();
			
		},
	    error: function (request, status, error) {

	    }
	});	
}

function sssAra (){
     var term=$("#sss_term").val();
     setDsFilter(term);
     dataSource.read();
 }

function filtreKaldir(){
    $("#sss_term").val('');
    var val=$("#sss_term").val();
    $("#grid").data("kendoGrid").dataSource.filter([]);
    setDsFilter(val);	
}

 function setDsFilter(term){
     dataSource.filter([
             {
                 field: "term",
                 operator: "eq",
                 value: term
             },
             {
                 field: "filtre",
                 operator: "eq",
                 value: ""
             }
         ]
     );
 }

 function initGrid(){
	    var crudServiceBaseUrl="/bildirim/bilgiBankasiListeJSON";
	    dataSource = new kendo.data.DataSource({
	        transport: {
	            read:  {
	                type: "POST",
	                url: crudServiceBaseUrl,
	                contentType: "application/json; charset=utf-8",            
	    			dataType: 'json',
	                beforeSend: function(req) {
	                    req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	                }
	            },
	            
	            parameterMap: function(options, operation) {
	                return kendo.stringify(options);
	                if (operation !== "read" && options.models) {
	                    //return {models: kendo.stringify(options.models)};
	                }
	            }
	        },
	        batch: true,
	        pageSize: 30,
	        schema: {
	            data:'data', 
	            model: {
	                id: "id",
	                fields: {
	                    "id"            :   { editable: false, nullable: true },
	                    "baslik"		:   { editable:false,type: "string" },
	                    "aciklama"		:   { editable:false,type: "string" },
	                }
	            },
	            "total": "total"
	        },
	        "serverFiltering": true,
	        "serverSorting": true,
	        "autoSync": true,
	        "serverPaging": true
	    });


	    var grid = $("#grid").kendoGrid({
	        "columns": [
	            {
	                "field": "baslik",
	                "filterable": false, "groupable": false, "title": "Konu",
	             
	            },
	            {
	                width:130,
	                template:kendo.template($("#islemlerTemplate").html())
	            },            
	        ],
	 
	        "dataSource":dataSource,
	        "scrollable": true,
	        "sortable": true,
	        "persistSelection": false,
	        "filterable": false,
	        "reorderable": true,
	        "resizable": false,
	        "columnMenu": false,
	        "groupable": false,
	        // "rowTemplate": kendo.template($('#row-template').html()),
	        "navigatable": false,
	        "editable": "false",
	        "selectable": "true",
	        "pageable": {"alwaysVisible": true, "pageSize": 20,"responsive": false,"input": true,"refresh": true, "info":true,"pageSizes": [30, 50, 75, 100]},
	        "height":800
	    });


 }


 $('.nav-tabs a').on('shown.bs.tab', function(event){
	  var tab = $(event.target).attr("href");
	  	if(tab=="#bilgibankasi"){
	  	     var grid = $("#grid").data("kendoGrid");
	  	     grid.refresh();
		}
});

</script>
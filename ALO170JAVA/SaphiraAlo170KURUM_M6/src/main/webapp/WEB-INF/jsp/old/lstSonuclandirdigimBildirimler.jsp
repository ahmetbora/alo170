<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<jsp:include page="header.jsp" />
<div class="m-content">

<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
							<div class="m-alert__icon">
								<i class="flaticon-exclamation m--font-brand"></i>
							</div>
							<div class="m-alert__text">
								<strong>SONU�LANDIRDI�IM B�LD�R�MLER</strong> Bu ekranda son 7 g�n i�inde sonu�land�rd���n�z bildirimlerin listesine eri�ebilirsiniz.
							</div>
						</div>


    <div class="m-portlet m-portlet--mobile">


        <div class="m-portlet__body">

                <table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>TARIH SAAT</th>
                        <th>BA�VURU SAH�B�</th>
                        <th>��� YAPACAK</th>
                        <th>KONU</th>
                        <th>G�REV DURUMU</th>
                        <th>�EH�R</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${listTask}" var="l" >
                        <tr>
                            <td><a href="/bildirim/bildirimdetay/${l.id}/${l.ukey}">${l.id }</a></td>
                            <td>${l.tarih }</td>
                            <td>${l.basvuruSahibi }</td>
                            <td>${l.isiYapacakAgent }</td>
                            <td>
                            ${l.konu1 } ${l.konu2 } ${l.konu3 }                                                        
                            </td>
                            <td>
                                <span class="m-badge  m-badge--${l.cls } m-badge--wide"> 
                                ${l.statu }</span>


                            </td>
                            <td>${l.ilAdi } ${l.ilceAdi} </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

        </div>



    </div>

</div>
<script>
    $(document).ready(function(){
        $('#liste').DataTable();
    })
</script>


<jsp:include page="footer.jsp" />
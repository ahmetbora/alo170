<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>

<jsp:include page="header.jsp" />






<div class="m-content">

<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
							<div class="m-alert__icon">
								<i class="flaticon-exclamation m--font-brand"></i>
							</div>
							<div class="m-alert__text">
								<strong>HAVUZDAK� B�LD�R�M L�STES�</strong> Bu ekranda �ye oldu�unuz havuzlardaki bildirimler listelenir. Atanmam�� bildirimler i�inden �zerinize bildirim alabilirsiniz.
							</div>
						</div>


    <div class="m-portlet m-portlet--mobile">
				    <div class="">
				<div class="d-flex align-items-center">
					<div class="mr-auto" style="padding-left: 35px; padding-top: 13px;">
						<h3 class="m-subheader__title ">Bildirimlerim</h3>
					</div>
					<div class="m-form__actions">
						<a href="/" class="btn btn-brand"><span>�zerime Al</span></a>
					</div>
				</div>
			</div>

        <div class="m-portlet__body">
				
                <table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                    	<th>#</th>
                        <th>#</th>
                        <th>TARIH</th>
                        <th>BA�VURU SAH�B�</th>
                        <th>��� YAPACAK</th>
                        <th>KONU</th>
                        <th>HAVUZ</th>
                        <th>SURE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${listTask}" var="l" >
                        <tr>
	                   	     <td class="text-center"  style="width: 5px;">
                                 	  	<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
								<input type="checkbox" name="chk" id="chk" value="0"/>
							  	<span></span>
							  	</label>
							 </td>
                            <td><a href="/bildirim/bildirimdetay/${l.id}/${l.ukey}">${l.id }</a></td>
                            <td>${l.tarih }</td>
                            <td>${l.basvuruSahibi }</td>
                            <td>${l.isiYapacakAgent }
                            <c:if test="${l.isiYapacakAgent == ''}">
								<span class="m-badge  m-badge--danger m-badge--wide">
									Atanmam��
                                </span>
							</c:if>
                            </td>
                            <td>
                            ${l.konu1 } ${l.konu2 } ${l.konu3 }                                                        
                            </td>
                            <td>${l.konu1 } ${l.konu2 } ${l.konu3 }</td>
                            <td>
                            <span class="m-badge  m-badge--info m-badge--wide">
									1 Saat
	                        </span></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

        </div>



    </div>

</div>
<script>
    $(document).ready(function(){
        $('#liste').DataTable();
    })
</script>


<jsp:include page="footer.jsp" />
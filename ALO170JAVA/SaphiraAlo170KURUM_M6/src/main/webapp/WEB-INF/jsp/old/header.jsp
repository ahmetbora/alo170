<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>CSGB - Alo170</title>
    <meta name="description" content="Spinners examples">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>

		<!--end::Web font -->
		<!--begin::Base Styles -->
		<link href="/assets/saphira/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="/assets/saphira/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    	<link rel="shortcut icon" href="<c:url value="/assets/demo/default/media/img/logo/favicon.ico"/>" />	
    	<c:forEach items="${css}" var="c">
    	<link href="${c}" rel="stylesheet">
    	</c:forEach>
    
		<script src="/assets/saphira/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="/assets/saphira/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    	<script src="<c:url value="/assets/vendors/custom/jquery-ui/jquery-ui.bundle.js"/>" type="text/javascript"></script>
    	<script src="<c:url value="/tools/default/custom/components/portlets/draggable.js"/>" type="text/javascript"></script>
    	
    	<c:forEach items="${js}" var="j">
    	<script src="${j}" type="text/javascript"></script>
    	</c:forEach>
	


</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-dark ">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
  							<h2><font color="#716dc7">SAPHIRA</font></h2>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">

                            <!-- BEGIN: Left Aside Minimize Toggle -->
                            <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>

                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>

                <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                    <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
                        <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                            
                    <!--        <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link"><i
                                        class="m-menu__link-icon flaticon-paper-plane"></i><span class="m-menu__link-title"> <span class="m-menu__link-wrap"> <span class="m-menu__link-text">${sessionScope.User_NAME }</span> <span class="m-menu__link-badge"></span>
												</span></span></a>
                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left"><span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item  m-menu__item--submenu" m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
                                        <a href="/logout" class="btn m-btn--block btn-outline-brand m-btn m-btn--custom m-btn--outline-2x"><i class="la la-power-off"></i> ��k��</a>

                                        </li>
                                    </ul>
                                </div>
                            </li>
                     -->        
                        </ul>
                    </div>

                    <!-- END: Horizontal Menu -->

                    <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-nav__link-text"><i class="flaticon-paper-plane"></i> ${sessionScope.User_NAME }</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: url(/assets/app/media/img/misc/quick_actions_bg.jpg); background-size: cover;">
														<span class="m-dropdown__header-subtitle">${sessionScope.User_NAME }</span>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__item m-nav__item--active">
																	<a href="/logout" class="m-nav__link m-nav__link--active">
																		<span class="m-nav__link-icon"><i class="la la-power-off"></i></span>
																		<span class="m-nav__link-title m-topbar__language-text m-nav__link-text">��k��</span>
																	</a>
																</li>
															
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
                                

                                

                            </ul>
                        </div>
                    </div>

                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>

    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

            <!-- BEGIN: Aside Menu -->
            <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="/bildirim/KullaniciHavuzBildirimListesi" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-open-box"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Bildirim Havuzu</span>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="/bildirim/BildirimListesiNoHavuz" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-open-box"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Bildirim Y�netimi</span>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="/bildirim/UzerimdekiBildirimListesi" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-folder"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap"> <span class="m-menu__link-text">�zerimdeki Bildirimler</span>
                                <span class="m-menu__link-badge"><span class="m-badge m-badge--danger" id="uzerimdekiAdet"></span></span>
                                </span>
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="/bildirim/SonuclandirdigimBildirimListesi" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-interface-5"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Sonu�land�rd���m Bildirimler</span>
                                </span>
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="/bildirim/GuncellenenBildirimListesi" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-refresh"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap"> <span class="m-menu__link-text">G�ncellenen Bildirimler</span>
                                </span>
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="/bildirim/bildirimsorgulama" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-search"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Bildirim Sorgulama</span>
                                </span>
                            </span>
                        </a>
                    </li>

                    

                    <li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--expanded" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-layers"></i><span
                                class="m-menu__link-text">�statistik Raporlar�</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">Base</span></span></li>
                                <li class="m-menu__item " aria-haspopup="true"><a href="/bildirim/kshistatistikraporu" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Kurum / �ube / Havuz �st. Raporu</span></a></li>
                                
                            </ul>
                        </div>
                    </li>



                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-list-1"></i><span class="m-menu__link-text">Raporlar</span><i
                                class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">

                                <li class="m-menu__item " aria-haspopup="true"><a href="/bildirim/bildirimdetayraporu" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">Bildirim Detay Raporu</span>
                                    </a>
                                </li>
                                
                                <li class="m-menu__item " aria-haspopup="true"><a href="/bildirim/bildirimsureraporu" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">Havuz Bildirim S�re Raporu</span>
                                    </a>
                                </li>
                                
                                <li class="m-menu__item " aria-haspopup="true"><a href="/bildirim/kurumyetkilisisureraporu" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">Kurum Yetkilisi ��lem S�resi Raporu</span>
                                    </a>
                                </li>


                                <li class="m-menu__item " aria-haspopup="true"><a href="../../components/icons/socicons.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">Sektor Baz�nda</span>
                                    </a>
                                </li>

								<li class="m-menu__item " aria-haspopup="true"><a href="../../components/icons/socicons.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">�l Baz�nda</span>
                                    </a>
                                </li>
                                
                                <li class="m-menu__item " aria-haspopup="true"><a href="/bildirim/konularagorebildirimraporu" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">Konulara G�re</span>
                                    </a>
                                </li>

                                <li class="m-menu__item " aria-haspopup="true"><a href="../../components/icons/socicons.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">Havuz A��k G�revler �zet</span>
                                    </a>
                                </li>
                                
                                <li class="m-menu__item " aria-haspopup="true"><a href="/bildirim/suresigecenbasvurular" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                        <span class="m-menu__link-text">S�resi Ge�en Ba�vurular</span>
                                    </a>
                                </li>


                            </ul>
                        </div>
                    </li>
                    
                     <li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--expanded" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-cogwheel"></i><span
                                class="m-menu__link-text">Ayarlar</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">Base</span></span></li>
                                <li class="m-menu__item " aria-haspopup="true"><a href="/ayarlar/tanimlar" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Tanimlar</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true"><a href="/ayarlar/kullanici/liste" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Kullan�c�lar</span></a></li>
                                <li class="m-menu__item " aria-haspopup="true"><a href="/ayarlar/yetki" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Yetki Tan�mlamalar�</span></a></li>
                                
                            </ul>
                        </div>
                    </li>
                    
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="/website/icerik/list" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-globe"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap"> <span class="m-menu__link-text">WEB</span>
                                </span>
                            </span>
                        </a>
                    </li>
                    
                </ul>
            </div>

            <!-- END: Aside Menu -->
        </div>

        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

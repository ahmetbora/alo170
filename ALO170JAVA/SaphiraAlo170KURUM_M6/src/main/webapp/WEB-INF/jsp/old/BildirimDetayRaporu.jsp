<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<jsp:include page="header.jsp" />
		
<script src="<c:url value="/tools/default/custom/crud/forms/widgets/bootstrap-datepicker.js"/>" type="text/javascript"></script>


                 
	<div class="row" style="padding-top: 10px;">
        <div class="col-md-3">
        
        <div class="m-portlet">					
				<div class="m-portlet__body">
				<div class="form-group m-form__group">
				<strong>B�LD�R�M RAPORLARI</strong> <hr/>
					<div class="row m-stack__items--right" style="padding-left: 3px;">
						<a href="#" onclick="BildirimDetayAl()" class="btn btn-brand btn-block"><i class="fa fa-search"></i> Rapor Al</a>
					</div>
				</div> 
		
            </div>
        </div>
        
        <div class="m-portlet">
 			<div class="m-portlet__body">
 			<div class="form-group m-form__group">
								<label for="s_date">Tarih Aral���</label>
								<div class="input-group date">
									<input type="text" class="form-control m-input" id="ilkTAR" name="ilkTAR" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group m-form__group">
								<div class="input-group date">
									<input type="text" class="form-control m-input" id="sonTAR" name="sonTAR" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
							</div>
					    	
					    	<div class="form-group m-form__group">
					    	<label for="iladi">�l</label>
		                    <select name="iller" id="iller" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Seciniz ---</option>
		                    	<c:forEach items="${iller}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
		                    </select>
							</div> 
							<div class="form-group m-form__group">
					    	<label for="ilce">�l�e</label>
		                    <select name="ilceler" id="ilceler" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Seciniz ---</option>

		                    </select>
							</div> 
 	 					    <div class="form-group m-form__group">
					    	<label for="statu">Stat�</label>
				            <form:select path="statu" id="statu" class="form-control m-select2">
							  <form:option value="0" label="--- Se�iniz ---" />
							  <form:options items="${statu}" itemValue="id" itemLabel="adi" />
	      				    </form:select>
							</div> 
				    		<div class="form-group m-form__group">
					    		<label for="aramakanalitipi">Arama Kanal� Tipi</label>
								<form:select path="aramakanalitipi" id="aramakanalitipi" class="form-control m-select2">
								  <form:option value="0" label="--- Se�iniz ---" />
								  <form:options items="${aramakanalitipi}" itemValue="id" itemLabel="adi" />
		      				    </form:select>
							</div>
							<div class="form-group m-form__group">
								<label for="fk_aramakanali">Arama Kanal�</label>
				                <select name="fk_aramakanali" id="fk_aramakanali" class="form-control m-select2" style="width:100%">
		                    		<option value='0'>--- Seciniz ---</option>
		                    	</select>
							</div>
							<div class="form-group m-form__group">
									<label for="adi">Arama Sebebi</label>
					                    <select name="fk_aramasebebi" id="fk_aramasebebi" class="form-control m-select2" style="width:100%">
					                    
					                    	<option value='0'>-- Se�iniz --</option>
					                     	<c:forEach items="${aramasebebi}" var="n">
					                    	<option data-ext="${n.id}" ${list.fk_konu == n.id ? 'selected' : ' '} value="${n.id}">${n.adi}</option>
					                    	</c:forEach>
					                   	
					                    </select>
								</div>
							<div class="form-group m-form__group">
							<label for="kurum">Kurum</label>
			                 <select name="tip_level1" id="tip_level1" class="form-control m-select2" style="width:100%">
			                 	<option value='0'>--- Se�iniz ---</option>
			                 	<c:forEach items="${kurumlar}" var="n">
			                 	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
			                 	</c:forEach>
			                 </select>
							</div>
							<div class="form-group m-form__group">
								<a href="#" onclick="" class="btn btn-outline-info btn-sm"><i class="la la-file-code-o"></i>XML</a>&nbsp;
								<a href="#" onclick="" class="btn btn-outline-info btn-sm"><i class="la la-file-pdf-o"></i>PDF</a>&nbsp;
								<a href="#" onclick="" class="btn btn-outline-info btn-sm"><i class="la la-file-excel-o"></i>XLS</a>
							</div> 
 			</div>
 		</div>
 		
 		
         
         
 </div>
               <div class="col-md-9">
                 <div class="m-portlet">				
					<div class="m-portlet__body" id="bildirimdetayara">
					
					</div>
	  			</div>
                </div>         

</div>





<script>
var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;

$(document).ready(function(){

			$(function () {
			    var token = $("input[name='_csrf']").val();
			    var header = "X-CSRF-TOKEN";
			    $(document).ajaxSend(function(e, xhr, options) {
			        xhr.setRequestHeader(header, _csrf_token);
			    });
			});
			
	
	$.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

	$("#iller").change(function() {
		
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		
		var headers = {};
		headers[csrfHeader] = csrfToken;
		
	    $.ajax({
	        type: 'POST',
	        url: '/bildirim/ilceDetay',
	        data: 
	        {
                id: $('#iller').val()
                
            },
	        success: function(response)
	        {
	        	$("#ilceler").html(response);
	        }
	    });

	});

	$("#aramakanalitipi").change(function() {
		
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		
		var headers = {};
		headers[csrfHeader] = csrfToken;
		
	    $.ajax({
	        type: 'POST',
	        url: '/bildirim/ilceDetay',
	        data: 
	        {
                id: $('#aramakanalitipi').val()
                
            },
	        success: function(response)
	        {
	        	$("#aramakanali").html(response);
	        }
	    });

	});

	$("#fk_gorev_statu").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#iller").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#ilceler").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#statu").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#aramakanalitipi").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#fk_aramakanali").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#tip_level1").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#fk_aramasebebi").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	
	$('#ilkTAR').datetimepicker({
		
		todayHighlight: !0,
        autoclose: !0 ,
        gotoCurrent: true 
				});

	$('#sonTAR').datetimepicker();





});



function BildirimDetayAl() {
	$('#bildirimdetayara').html("");
	var data = {
			
				"ilkTAR" : $('#ilkTAR').val()
				, "sonTAR" : $('#sonTAR').val()
				, "iller" : $('#iller').val()
				, "ilceler" : $('#ilceler').val()
				, "statu" : $('#statu').val()
				, "aramakanalitipi" : $('#aramakanalitipi').val()
				, "asebebi" : $('#asebebi').val()
				, "tip_level1" : $('#tip_level1').val()
				, "altkurum" : $('#altkurum').val()
				, "${_csrf.parameterName}" : "${_csrf.token}"

	};
	$.post("/bildirim/bildirimdetaysorgulama?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#bildirimdetayara').html(data);
		});



}

</script>
<jsp:include page="footer.jsp" />
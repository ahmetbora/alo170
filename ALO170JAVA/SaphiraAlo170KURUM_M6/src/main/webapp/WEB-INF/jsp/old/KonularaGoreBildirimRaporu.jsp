<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<jsp:include page="header.jsp" />
		
<script src="<c:url value="/tools/default/custom/crud/forms/widgets/bootstrap-datepicker.js"/>" type="text/javascript"></script>


                 
	<div class="row" style="padding-top: 10px;">
        <div class="col-md-3">
        
        <div class="m-portlet">					
				<div class="m-portlet__body">
				<div class="form-group m-form__group">
				<strong>KONULARA GORE B�LD�R�M RAPORLARI</strong><hr/>
					<div class="row m-stack__items--right" style="padding-left: 3px;">
						<a href="#" onclick="Filtrele()" class="btn btn-brand btn-block"><i class="fa fa-search"></i> Rapor Al</a>
					</div>
				</div> 
			
			</div>
        </div>
        
        <div class="m-portlet">
        	<div class="m-portlet__body">
        		<div class="form-group m-form__group">
						<label for="statu">Stat�</label>
				            <form:select path="statu" id="statu" class="form-control m-select2">
							  <form:option value="" label="--- T�m� ---" />
							  <form:options items="${statu}" itemValue="id" itemLabel="adi" />
	      				    </form:select>
			    	</div> 
			    	
			    	<div class="form-group m-form__group">
								<label for="s_date">Tarih Aral���</label>
								<div class="input-group date">
									<input type="text" class="form-control m-input" id="ilkTAR" name="ilkTAR" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group m-form__group">
								<div class="input-group date">
									<input type="text" class="form-control m-input" id="sonTAR" name="sonTAR" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
							</div>
					    	
							<div class="form-group m-form__group">
							<label for="kurum">Kurum</label>
			                 <select name="tip_level1" id="tip_level1" class="form-control m-select2" style="width:100%">
			                 	<option value='0'>--- Se�iniz ---</option>
			                 	<c:forEach items="${kurumlar}" var="n">
			                 	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
			                 	</c:forEach>
			                 </select>
							</div>
        	</div>
        </div>
        
	</div>
         
         
         
               <div class="col-md-9">
                 <div class="m-portlet">				
					<div class="m-portlet__body" id="konularagorebildirimara">
					
					</div>
	  			</div>
                </div>         

</div>





<script>
$(document).ready(function(){

	$("#statu").select2({ placeholder: "--- T�m� ---", minimumResultsForSearch: 1 / 0 });
	$("#tip_level1").select2({ placeholder: "--- T�m� ---", minimumResultsForSearch: 1 / 0 });

	$('#ilkTAR').datetimepicker({
		
		todayHighlight: !0,
        autoclose: !0 ,
        gotoCurrent: true 
				});

	$('#sonTAR').datetimepicker();
});

function Filtrele() {
	$('#konularagorebildirimara').html("");
	var data = {
			
				"ilkTAR" : $('#ilkTAR').val()
				, "sonTAR" : $('#sonTAR').val()
				, "statu" : $('#statu').val()
				, "tip_level1" : $('#tip_level1').val()
				

	};
	$.post("/bildirim/konularagorebildirimraporu?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#konularagorebildirimara').html(data);
		});


}
</script>
<jsp:include page="footer.jsp" />
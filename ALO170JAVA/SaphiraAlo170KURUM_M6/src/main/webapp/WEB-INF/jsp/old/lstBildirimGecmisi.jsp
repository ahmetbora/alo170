<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>

				
<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
    <thead>
    <tr>

        <th>#</th>
        <th>TARIH</th>
        <th>BA�VURU SAH�B�</th>
        <th>��� YAPACAK</th>
        <th>KONU</th>
        <th>HAVUZ</th>
        <th>SURE</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${listTask}" var="l" >
        <tr>

            <td><a href="/bildirim/bildirimdetay/${l.id}/${l.ukey}">${l.id }</a></td>
            <td>${l.tarih }</td>
            <td>${l.basvuruSahibi }</td>
            <td>${l.isiYapacakAgent }
            <c:if test="${l.isiYapacakAgent == ''}">
			<span class="m-badge  m-badge--danger m-badge--wide">Atanmam��</span>
			</c:if>
            </td>
            <td>
            ${l.konu1 } ${l.konu2 } ${l.konu3 }                                                        
            </td>
            <td>${l.konu1 } ${l.konu2 } ${l.konu3 }</td>
            <td>
            <span class="m-badge  m-badge--info m-badge--wide">1 Saat</span>
            </td>
        </tr>
           </c:forEach>
    </tbody>
</table>

<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<jsp:include page="header.jsp" />
		
<script src="<c:url value="/tools/default/custom/crud/forms/widgets/bootstrap-datepicker.js"/>" type="text/javascript"></script>


                 
	<div class="row" style="padding-top: 10px;">
			    <div class="col-md-4">
            	<div class="m-portlet">
					<div class="m-portlet__body">
						<div class="form-group m-form__group">
							<strong>KURUM / �UBE / HAVUZ �STAT�ST�K RAPORU</strong><hr/>
							<div class="row m-stack__items--right" style="padding-left: 3px;">
								<a href="#" onclick="IstatistikAra()" class="btn btn-brand btn-block"><i class="fa fa-search"></i> Rapor Al</a>
							</div>
						</div>
					</div>
				</div>
			</div>
        <div class="col-md-4">
        	
        <div class="m-portlet">
        					
			<div class="m-portlet__body">
				
					<div class="form-group m-form__group">
						<strong>�lk Tarih</strong><hr/>
						<div class="input-group date">
							<input type="text" class="form-control m-input" id="ilkTAR" name="ilkTAR" />
							<div class="input-group-append">
								<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
								</span>
							</div>
						</div>
					</div>
				</div>
        </div>
       </div>
        <div class="col-md-4">
            <div class="m-portlet">
			<div class="m-portlet__body">
					<div class="form-group m-form__group">
						<strong>Son Tarih</strong><hr/>
							<div class="form-group m-form__group">
								<div class="input-group date">
									<input type="text" class="form-control m-input" id="sonTAR" name="sonTAR" />
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>

         
         
         
         
               <div class="col-md-12">
                 <div class="m-portlet">				
					<div class="m-portlet__body" id="istatistikara">
					
					</div>
	  			</div>
                </div>         

</div>





<script>
$(document).ready(function(){

	$("#fk_gorev_statu").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });

	$('#ilkTAR').datetimepicker({
		
		todayHighlight: !0,
        autoclose: !0 ,
        gotoCurrent: true 
				});

	$('#sonTAR').datetimepicker();
});

function IstatistikAra() {
	$('#istatistikara').html("");
	var data = {
			
				"ilkTAR" : $('#ilkTAR').val()
				, "sonTAR" : $('#sonTAR').val()

	};
	$.post("/bildirim/kshistatistikraporu?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#istatistikara').html(data);
		});


}
</script>
<jsp:include page="footer.jsp" />
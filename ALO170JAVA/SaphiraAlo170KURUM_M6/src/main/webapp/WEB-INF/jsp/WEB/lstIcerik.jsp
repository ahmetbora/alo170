<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Web Sitesi İçerik</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Web Sitesi İçerik </a>
				
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__body p-3">
									
						<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
							<thead>
							<tr>
								<th>BASLIK</th>
								<th>KISA ACIKLAMA</th>
								<th>URL</th>
								<th class="text-right">ISLEMLER</th>
							</tr>
							</thead>
							<tbody>
							<c:forEach items="${listTask}" var="l" >
								<tr>
									<td>${l.baslik }</td>
									<td>${l.kisa_aciklama }</td>
									<td>${l.url }</td>
									<td class="text-right"> 
									<a href="/website/icerik/form/${l.id }" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill">
									<i class="la la-search"></i>
									</a>
									</td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>
<script>
    $(document).ready(function(){
        $('#liste').DataTable();
    })
</script>
<jsp:include page="footer.jsp" />
 
 
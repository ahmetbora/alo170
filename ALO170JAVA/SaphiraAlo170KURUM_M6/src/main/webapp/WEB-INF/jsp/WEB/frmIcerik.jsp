<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Web Sitesi İçerik</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Web Sitesi İçerik </a>
				
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="kt-portlet">
					<form:form id="webForm" class="m-form m-form--fit m-form--label-align-right"  modelAttribute="webForm" method="POST" action="/website/icerik/save"  >
					<div class="kt-portlet__body p-3">
						<div class="row">
							<div class="col-12">
									<form:input type="hidden" name="id" path="id" value="${webForm.id }" />	
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12" for="baslik">Baslik</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<form:input type="text" name="baslik" path="baslik" value="${webForm.baslik }" placeholder="İçerik Başlığı" class="form-control  m-input"/>
										</div>						
									</div>
			 
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kisa Açıklama</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<form:textarea path="kisa_aciklama" name="kisa_aciklama" class="form-control" value="${webForm.kisa_aciklama }" data-provide="summernote"  rows="10"/>
										</div>
									</div>
			 
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">İçerik</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<form:textarea path="icerik" name="icerik" class="form-control" value="${webForm.icerik }" data-provide="summernote" rows="20"/>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12" for="resim">İçerik Resim</label>
										<div class="col-lg-9 col-md-9 col-sm-12" class="col-10">
											<form:input type="text" name="resim" path="resim" value="${webForm.resim }" placeholder="Resim Adı" class="form-control  m-input"/>
										</div>						
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12" for="url">URL</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<form:input type="text" name="url" path="url" value="${webForm.url }" placeholder="İçerik URL'i" class="form-control  m-input"/>
										</div>						
									</div>
			
							</div>
			
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="row align-items-center">
							<div class="col-lg-12">
								<button type="submit" onclick="IcerikEkle()" class="btn btn-brand">Kaydet</button>
								<button type="reset" class="btn btn-secondary">İptal</button>
							</div>
						</div>
					</div>
					</form:form>
				</div>			
			</div>
		</div>
	</div>

</div>
<script>
    $(document).ready(function(){
        $('#kisa_aciklama').summernote();
        $('#icerik').summernote();
    })
    
    function IcerikEkle() {
	var data = {
				 "id" : $('#id').val()
				,"baslik" : $('#baslik').val()
				,"kisa_aciklama" : $('#kisa_aciklama').val()
				, "icerik" : $('#icerik').val()
				, "resim" : $('#resim').val()
				, "url" : $('#url').val()

	};

	$.post("/website/icerik/save?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		
		});


}
</script>
<jsp:include page="footer.jsp" />
 
 
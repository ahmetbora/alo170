<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${yetki.bildirim_AciklamaEkle == 'true'}">
    <a href="javascript:void(0);"  class="btn btn-success btn-sm" id="btn_aciklamaEkle"><i class="fa fa-edit"></i> Açıklama Ekle</a>&nbsp;
</c:if>

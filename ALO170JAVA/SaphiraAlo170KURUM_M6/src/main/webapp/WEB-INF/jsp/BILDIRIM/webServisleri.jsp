<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<style>
.wsLink:hover .m-nav__link-text {
color:#34bfa3!important;
}
.wsLink:hover{
color:#34bfa3!important;
    border-radius: 2rem;
}
.aktif{
background:#eaeaea!important;
color:#34bfa3!important;
border-radius: 2rem;
}
.aktif span{
color:#34bfa3!important;
}
</style>
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						WEB SERVİSLERİ					
					</h3>
				</div>			
			</div>
			<div class="kt-portlet__body">
				<div class="kt-section">
					<div class="kt-section">
						<div class="kt-section__content kt-section__content--border kt-section__content--fit">
							<ul class="kt-nav">
								<li class="kt-nav__item kt-nav__item--active">
									<a href="#" class="kt-nav__link wsLink">
										<i class="kt-nav__link-icon flaticon2-expand"></i>
										<span class="kt-nav__link-title">
											<span class="kt-nav__link-text kt-font-info">4A SERVİSLERİ</span>
									</span></a>
									<ul class="kt-nav__sub">
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="raporBilgileriSorgula" id="raporBilgileriSorgula" data-text="Rapor Bilgileri Sorgula">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">RAPOR BİLGİLERİ SORGULA</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="sigortaliHizmetDokumu" id="sigortaliHizmetDokumu" data-text="4A Hizmet Bilgisi">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4A HİZMET BİLGİSİ</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="tescilKaydi4a" data-text="4A Tescil Kaydı">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4A TESCİL KAYDI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="emekliKaydi4a" data-text="4A Emekli Kaydı">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4A EMEKLİ KAYDI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="hizmetBorclanma4A" data-text="4A Hizmet Borçlanma">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4A HİZMET BORÇLANMALARI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="tescilKaydi4aDetay" data-text="4A Detaylı Tescil Kaydı">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4A DETAYLI TESCİL KAYDI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="tumTescilVeKimlikBilgileriniSorgula4A" data-text="Tüm Tescil Bilgilerini Sorgula">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">TÜM SİCİL BİLGİLERİNİ SORGULA</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="sigortaliAyrilisSorgula4A" data-text="4A Sigortalı Ayrılış Sorgula">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4A SİGORTALI AYRILIŞ SORGULA</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="sigortaliTescilSorgula4A" data-text="4A Sigortalı İşe Giriş Sorgula">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4A SİGORTALI İŞE GİRİŞ SORGULA</span>
											</a>
										</li>																																																																															
									</ul>
								</li>
								<li class="kt-nav__item kt-nav__item--active">
									<a href="#" class="kt-nav__link wsLink">
										<i class="kt-nav__link-icon flaticon2-expand"></i>
										<span class="kt-nav__link-title">
											<span class="kt-nav__link-text kt-font-info">4B SERVİSLERİ</span>
									</span></a>
									<ul class="kt-nav__sub">
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B Borç Durumu"  data-ws="borcDurumu4B">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B BORÇ DURUMU</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B Tescil Kaydı"  data-ws="tescilKaydi4b">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B TESCİL KAYDI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B Basamak Listesi"  data-ws="basamakListesi4B">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B BASAMAK LİSTESİ</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B Hizmet Bilgisi"  data-ws="hizmetBilgisi4B">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B HİZMET BİLGİSİ</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B Ödeme Dökümü"  data-ws="odemeDokumu4B">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B ÖDEME DÖKÜMÜ</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B Emekli Kaydı"  data-ws="emekliKaydi4b">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B EMEKLİ KAYDI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B - 6736 Yapılandırma"  data-ws="yapilandirmaBorcBilgisi4B">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B - 6736 YAPILANDIRMA</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B - 7020 Yapılandırma"  data-ws="yapilandirma7020BorcBilgisi4B">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B - 7020 YAPILANDIRMA</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B Hizmet Borçlanmaları"  data-ws="hizmetBorclanma4B">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4B HİZMET BORÇLANMALARI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-text="4B Hizmet Bilgisi" data-ws="sigortaliHizmetDokumu4B">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text kt-font-danger">4B Hizmet Dökümü (TEST)</span>
											</a>
										</li>																																																																																									
									</ul>
								</li>
								<li class="kt-nav__item kt-nav__item--active">
									<a href="#" class="kt-nav__link wsLink">
										<i class="kt-nav__link-icon flaticon2-expand"></i>
										<span class="kt-nav__link-title">
											<span class="kt-nav__link-text kt-font-info">4C SERVİSLERİ</span>
									</span></a>
									<ul class="kt-nav__sub">
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="tescilKaydi4c" data-text="4C Tescil Kaydı">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4C TESCİL KAYDI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="emekliKaydi4c" data-text="4C Emekli Kaydı">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4C EMEKLİ KAYDI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="hizmetBorclanma4C" data-text="4C Hizmet Borçlanmaları">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4C HİZMET BORÇLANMALARI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="#" class="kt-nav__link wsLink" data-ws="sigortaliHizmetDokumu4C" data-text="4C Hizmet Dökümü">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">4C HİZMET DÖKÜMÜ</span>
											</a>
										</li>																													
									</ul>
								</li>
								<li class="kt-nav__item kt-nav__item--active">
									<a href="javascript:void(0);" class="kt-nav__link wsLink">
										<i class="kt-nav__link-icon flaticon2-expand"></i>
										<span class="kt-nav__link-title">
											<span class="kt-nav__link-text kt-font-info">ORTAK SERVİSLER</span>
									</span></a>
									<ul class="kt-nav__sub">
										<li class="kt-nav__item">
											<a href="javascript:void(0);" class="kt-nav__link wsLink" data-text="Sağlık Aktivasyonu Sorgulama"  data-ws="spas">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">SAĞLIK AKTİVASYONU SORGULAMA</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="javascript:void(0);" class="kt-nav__link wsLink" data-text="Emekli Ödeme Sorgulama"  data-ws="odemedurumbilgisiemekli">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">EMEKLİ ÖDEME SORGULAMA (EÖS)</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="javascript:void(0);" class="kt-nav__link wsLink" data-text="Emekli Kesinti Sorgulama"  data-ws="kesintibilgisiemekli">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">EMEKLİ KESİNTİ SORGULAMA (EKS)</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="javascript:void(0);" class="kt-nav__link wsLink" data-text="Merkezi Ödeme Sorgulama"  data-ws="odemedurumbilgisi">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">MERKEZİ ÖDEME SORGULAMA (MÖS)</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="javascript:void(0);" class="kt-nav__link wsLink" data-text="Emekli Hareket Sorgulama"   data-ws="emeklihareket">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">EMEKLİ HAREKET SORGULAMA (EHS)</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="javascript:void(0);" class="kt-nav__link wsLink" data-text="Evrak Sorgulama"   data-ws="evraksorgulama">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">EVRAK TAKİP (DYS) SORGULAMA</span>
											</a>
										</li>																																																	
									</ul>
								</li>
								<li class="kt-nav__item kt-nav__item--active">
									<a href="javascript:void(0);" class="kt-nav__link wsLink">
										<i class="kt-nav__link-icon flaticon2-expand"></i>
										<span class="kt-nav__link-title">
											<span class="kt-nav__link-text kt-font-info">KULLANILAN DİĞER SERVİSLER</span>
									</span>
									</a>
									<ul class="kt-nav__sub">
										<li class="kt-nav__item">
											<a href="https://app2.csgb.gov.tr/yabancilar/faces/login" class="kt-nav__link wsLink" target="_blank">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">YABANCILAR ÇALIŞMA İZİNLERİ DAİRE BAŞKANLIĞI</span>
											</a>
										</li>
										<li class="kt-nav__item">
											<a href="http://cc.iskur.gov.tr/CallCenter.aspx" class="kt-nav__link wsLink" target="_blank">
												<span class="kt-nav__link-bullet kt-nav__link-bullet--line"><span></span></span>
												<span class="kt-nav__link-text">İŞKUR ÇAĞRI MERKEZİ İŞLEMLERİ</span>
											</a>
										</li>									
									</ul>
								</li>																														
							</ul>
						</div>
					</div>
				</div>				
			</div>
		</div>
		<c:if test="${webServisGoster==true}">
		<div class="m-portlet m-portlet--responsive-mobile d-none">

			<div class="m-portlet__body ">
				<ul class="m-nav m-nav--active-bg" id="m_nav" role="tablist">

					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_1"   aria-expanded=" true">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text kt-font-info">4A SERVİSLERİ</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_1" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="raporBilgileriSorgula" id="raporBilgileriSorgula" data-text="Rapor Bilgileri Sorgula">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">RAPOR BİLGİLERİ SORGULA</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="sigortaliHizmetDokumu" id="sigortaliHizmetDokumu" data-text="4A Hizmet Bilgisi">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A HİZMET BİLGİSİ</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="tescilKaydi4a" data-text="4A Tescil Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A TESCİL KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="emekliKaydi4a" data-text="4A Emekli Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A EMEKLİ KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="hizmetBorclanma4A" data-text="4A Hizmet Borçlanma">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A HİZMET BORÇLANMALARI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="tescilKaydi4aDetay" data-text="4A Detaylı Tescil Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A DETAYLI TESCİL KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="tumTescilVeKimlikBilgileriniSorgula4A" data-text="Tüm Tescil Bilgilerini Sorgula">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">TÜM SİCİL BİLGİLERİNİ SORGULA</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="sigortaliAyrilisSorgula4A" data-text="4A Sigortalı Ayrılış Sorgula">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A SİGORTALI AYRILIŞ SORGULA</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="sigortaliTescilSorgula4A" data-text="4A Sigortalı İşe Giriş Sorgula">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4A SİGORTALI İŞE GİRİŞ SORGULA</span>
								</a>
							</li>
																																																																	
						</ul>
					</li>


					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_2"   aria-expanded=" false">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text kt-font-info">4B SERVİSLERİ</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_2" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Borç Durumu"  data-ws="borcDurumu4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B BORÇ DURUMU</span>
								</a>
							</li>		
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Tescil Kaydı"  data-ws="tescilKaydi4b">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B TESCİL KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Basamak Listesi"  data-ws="basamakListesi4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B BASAMAK LİSTESİ</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Hizmet Bilgisi"  data-ws="hizmetBilgisi4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B HİZMET BİLGİSİ</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Ödeme Dökümü"  data-ws="odemeDokumu4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B ÖDEME DÖKÜMÜ</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Emekli Kaydı"  data-ws="emekliKaydi4b">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B EMEKLİ KAYDI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B - 6736 Yapılandırma"  data-ws="yapilandirmaBorcBilgisi4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B - 6736 YAPILANDIRMA</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B - 7020 Yapılandırma"  data-ws="yapilandirma7020BorcBilgisi4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B - 7020 YAPILANDIRMA</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Hizmet Borçlanmaları"  data-ws="hizmetBorclanma4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4B HİZMET BORÇLANMALARI</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="4B Hizmet Bilgisi" data-ws="sigortaliHizmetDokumu4B">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text text-danger">4B Hizmet Dökümü (TEST)</span>
								</a>
							</li>																																																																
						</ul>
					</li>
					
					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_3"   aria-expanded=" false">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text kt-font-info">4C SERVİSLERİ</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_3" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="tescilKaydi4c" data-text="4C Tescil Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4C TESCİL KAYDI</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="emekliKaydi4c" data-text="4C Emekli Kaydı">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4C EMEKLİ KAYDI</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="hizmetBorclanma4C" data-text="4C Hizmet Borçlanmaları">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4C HİZMET BORÇLANMALARI</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-ws="sigortaliHizmetDokumu4C" data-text="4C Hizmet Dökümü">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">4C HİZMET DÖKÜMÜ</span>
								</a>
							</li>																								
						</ul>
					</li>
					
					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_1"   aria-expanded=" false">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text kt-font-info">ORTAK SERVİSLER</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_4" role="tabpanel" aria-labelledby="m_nav_link_4" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Sağlık Aktivasyonu Sorgulama"  data-ws="spas">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">SAĞLIK AKTİVASYONU SORGULAMA</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Emekli Ödeme Sorgulama"  data-ws="odemedurumbilgisiemekli">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">EMEKLİ ÖDEME SORGULAMA (EÖS)</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Emekli Kesinti Sorgulama"  data-ws="kesintibilgisiemekli">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">EMEKLİ KESİNTİ SORGULAMA (EKS)</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Merkezi Ödeme Sorgulama"  data-ws="odemedurumbilgisi">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">MERKEZİ ÖDEME SORGULAMA (MÖS)</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Emekli Hareket Sorgulama"   data-ws="emeklihareket">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">EMEKLİ HAREKET SORGULAMA (EHS)</span>
								</a>
							</li>	
							<li class="m-nav__item">
								<a href="javascript:void(0);" class="m-nav__link wsLink" data-text="Evrak Sorgulama"   data-ws="evraksorgulama">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">EVRAK TAKİP (DYS) SORGULAMA</span>
								</a>
							</li>																																						
						</ul>
					</li>
					
					<li class="m-nav__item m-nav__item--active">
						<a class="m-nav__link" role="tab" id="m_nav_link_1"   aria-expanded=" false">
							<i class="m-nav__link-icon flaticon-interface-9"></i>
							<span class="m-nav__link-title">
								<span class="m-nav__link-wrap">
									<span class="m-nav__link-text kt-font-info">KULLANILAN DİĞER SERVİSLER</span>
									<span class="m-nav__link-badge d-none">
										<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
									</span>
								</span>
							</span>
							<span class="m-nav__link-arrow"></span>
						</a>
						<ul class="m-nav__sub collapse show" id="m_nav_sub_5" role="tabpanel" aria-labelledby="m_nav_link_5" data-parent="#m_nav">
							<li class="m-nav__item">
								<a href="https://app2.csgb.gov.tr/yabancilar/faces/login" class="m-nav__link" target="_blank">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">YABANCILAR ÇALIŞMA İZİNLERİ DAİRE BAŞKANLIĞI</span>
								</a>
							</li>			
							<li class="m-nav__item">
								<a href="http://cc.iskur.gov.tr/CallCenter.aspx" class="m-nav__link" target="_blank">
									<span class="m-nav__link-bullet m-nav__link-bullet--line">
										<i class="la la-angle-double-right"></i>
									</span>
									<span class="m-nav__link-text">İŞKUR ÇAĞRI MERKEZİ İŞLEMLERİ</span>
								</a>
							</li>								
						</ul>
					</li>					
				</ul>
						
			</div>
		</div>	
		</c:if>

			<script>
			
			</script>
		
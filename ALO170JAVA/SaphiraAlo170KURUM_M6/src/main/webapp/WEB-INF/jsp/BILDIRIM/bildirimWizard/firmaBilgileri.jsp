<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="kt-form__section kt-form__section--first">
	<div class="kt-wizard-v3__form">
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Kurum / Kişi Adı:</label>
			<div class="col-xl-10 col-lg-10">
				<input type="text" class="form-control" name="f_adi" id="f_adi"  placeholder="Firma Adı">																		
			</div>
		</div>	
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Sektör:</label>
			<div class="col-xl-10 col-lg-10">
			   	<select class="form-control " name="fk_proje" id="fk_proje" placeholder="Sektör" style="width:100%">
			   		<option value="">--Seçiniz--</option>
					<c:forEach items="${sektorler}" var="n">
						<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
					</c:forEach>											
			   	</select>																		
			</div>
		</div>
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Adres:</label>
			<div class="col-xl-10 col-lg-10">
				<textarea class="form-control" name="f_adres" id="f_adres" rows="3" placeholder="Firma Adresi"></textarea>																		
			</div>
		</div>
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Telefon:</label>
			<div class="col-xl-10 col-lg-10">
				<input type="text" class="form-control phone" name="f_tel" id="f_tel"  placeholder="Firma Telefonu">																		
			</div>
		</div>
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">İl:</label>
			<div class="col-xl-10 col-lg-10">
			   	<select class="form-control" name="f_fk_il" id="f_fk_il" placeholder="Firma İl" style="width:100%">
					<option value='0'>--- Seciniz ---</option>
					<c:forEach items="${iller}" var="n">
						<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
					</c:forEach>													
			   	</select>	
			   																	
			</div>
		</div>
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">İlçe:</label>
			<div class="col-xl-10 col-lg-10">
			   	<select class="form-control" name="f_fk_ilce" id="f_fk_ilce"  placeholder="Firma İlçe">
			   	
			   	</select>																	
			</div>
		</div>
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">NACE Kodu:</label>
			<div class="col-xl-10 col-lg-10">
				<input type="text" class="form-control" name="nace" id="nace" placeholder="NACE Kodu">																		
			</div>
		</div>
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Sigortasız Çalışma Süresi(Ay):</label>
			<div class="col-xl-10 col-lg-10">
				<input type="text" class="form-control" name="scs" id="scs" placeholder="Sigortasız Çalışma Süresi(Ay)" >																		
			</div>
		</div>								
	</div>
</div>
		
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${(yetki.bildirim_BildirimKapat == 'true') and (task.fkGorevStatu<99) and (task.fk_agent == userID)}">
    <a href="javascript:void(0);" class="btn btn-danger btn-sm pull-right kt-margin-l-5" id="btn_kapat"><i class="fa fa-check"></i> Bildirimi Kapat</a>&nbsp;	
</c:if>
														  

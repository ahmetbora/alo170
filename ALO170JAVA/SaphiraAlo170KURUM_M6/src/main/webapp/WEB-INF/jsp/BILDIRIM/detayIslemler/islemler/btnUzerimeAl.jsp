<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <%-- üst düzey yetkilendirme kontrolü kendi havuzunda olmayan bildirimleri uzerine alma yetkisi varmı --%>
    <c:when test="${yetki.bildirim_UzerimeAlAll == 'true'}">
        <%-- bildirim kendi ustunde değil ise ve kapanmamış ise ve bir havuza yonlendirilmiş ise --%>
        <c:if test="${(task.fk_agent != userID) and (task.fkGorevStatu<99) and (task.fk_agent==0)}">
            <a href="javascript:void(0);" onclick="" class="btn btn-primary btn-sm" id="btn_uzerime_al"><i class="fa fa-plus"></i> Bildirimi Üstüne Al</a>&nbsp;
        </c:if>
    </c:when>    
    <c:otherwise>
        <%-- üst düzey yetkiye sahip değilse --%>
        <c:if test="${(yetki.bildirim_UzerimeAl == 'true') and (task.fk_agent != userID) and (task.fkGorevStatu<99) and (task.fk_agent==0)}">
            <a href="javascript:void(0);" onclick="" class="btn btn-primary btn-sm" id="btn_uzerime_al"><i class="fa fa-plus"></i> Bildirimi Üstüne Al</a>&nbsp;
        </c:if>
    </c:otherwise>
</c:choose>
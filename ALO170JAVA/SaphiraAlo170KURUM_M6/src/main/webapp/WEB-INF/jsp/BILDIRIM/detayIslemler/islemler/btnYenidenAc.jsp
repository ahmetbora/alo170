<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${(yetki.bildirim_YenidenAc == 'true') and (task.fkGorevStatu==99)}">
    <a href="javascript:void(0);" class="btn btn-primary btn-sm pull-right kt-margin-l-5" id="btn_yeniden_ac"><i class="fa fa-sync"></i> Yeniden Aç</a>&nbsp;
</c:if>

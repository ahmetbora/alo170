<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<input type="hidden" name="uid" id="uid" value="${uid}">
	<input type="hidden" name="cname" id="cname" value="${que}">
	<input type="hidden" name="cid" id="cid" value="${cid}">
	<input type="hidden" id="fk_sonuckod" name="fk_sonuckod" value="2">
	<input type="hidden" id="fk_kart" name="fk_kart" value="0">
	<input type="hidden" id="firma_bilgi" name="firma_bilgi" value="0">
	<input type="hidden" id="kullanici_id" value="${userID}">
		<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
		<div class="firma_bilgileri d-none">firma bilgileri girilecek</div>
				<!-- mernis loading -->
		<div class="row">
			<div class="col-md-12">
				<div class="m-alert m-alert--outline m-alert--outline-2x alert alert-warning fade show text-center d-none" role="alert" id="mernisload">
					MERNİS SORGUSU YAPILIYOR LÜTFEN BEKLEYİN...
				</div>
				<div id="mernis_content"></div>
			</div>
		</div>		
		<!-- mernis bitti-->
				
		<!-- tc dogrulama loading -->
		<div class="row">
			<div class="col-md-12">
				<div class="m-alert m-alert--outline m-alert--outline-2x alert alert-warning fade show text-center d-none" role="alert" id="dogrulamaload">
					KİMLİK DOĞRULAMA SORUSU GELİYOR LÜTFEN BEKLEYİN...
				</div>
				<div id="dogrulama_content"></div>
			</div>
		</div>		
		<!-- tc dogrulama bitti-->
		
		
		<div class="d-none" id="statu_div"></div>
		
		<!-- özel numaralar kontrolü -->
		<c:if test="${ozelNumraKontrol}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>PRİZMA EMYS AR-GE BİRİMİNDEN ARANMAKTASINIZ. TEST İÇİN ARAMA YAPILDI!</strong>
				</div>
			</div>
		</div>
		</c:if>
		<!-- özel numaralar kontrolü bitti -->
		
		<!-- IVR dan gelen tuşlama kontrolü -->
		<c:if test="${ivrtc[0].menu=='1'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>RAPOR ÜCRETİ, ÇEYİZ  VE EMZİRME ÖDENEKLERİ KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>
		<c:if test="${ivrtc[0].menu=='2'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>SİGORTALILIK HİZMET SÜRESİ KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='3'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>EMEKLİLİK İŞLEMLERİ KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>			
		<c:if test="${ivrtc[0].menu=='4'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>SAĞLIK AKTİVASYONU KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='5'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>İHBAR VE ŞİKAYET KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='6'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>YABANCI ÇALIŞMA İZİNLERİ KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='7'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>DİĞER İŞLEMLER KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>		
		<c:if test="${ivrtc[0].menu=='0'}">
		<div class="row">
			<div class="col-md-12">		
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air text-center" role="alert">
					<strong>MADEN VE İŞ KAZALARI KONUSU SEÇİLMİŞTİR!</strong>
				</div>
			</div>
		</div>			
		</c:if>	
		<!-- IVR dan gelen tuşlama kontrolü bitti -->
		<div class="kt-grid__item">
	
			<!--begin: Form Wizard Nav -->
			<div class="kt-wizard-v3__nav">
				<div class="kt-wizard-v3__nav-items">
					<a class="kt-wizard-v3__nav-item" href="javascript:void(0);" data-ktwizard-type="step" data-ktwizard-state="current">
						<div class="kt-wizard-v3__nav-body">
							<div class="kt-wizard-v3__nav-label">
								<span>1</span> Vatandaş Bilgileri
							</div>
							<div class="kt-wizard-v3__nav-bar"></div>
						</div>
					</a>
					<a class="kt-wizard-v3__nav-item" href="javascript:void(0);" data-ktwizard-type="step">
						<div class="kt-wizard-v3__nav-body">
							<div class="kt-wizard-v3__nav-label">
								<span>2</span> Diğer Bilgiler
							</div>
							<div class="kt-wizard-v3__nav-bar"></div>
						</div>
					</a>
					<a class="kt-wizard-v3__nav-item" href="javascript:void(0);" data-ktwizard-type="step">
						<div class="kt-wizard-v3__nav-body">
							<div class="kt-wizard-v3__nav-label">
								<span>3</span>	Başvuru Detayları
							</div>
							<div class="kt-wizard-v3__nav-bar"></div>
						</div>
					</a>
					<a class="kt-wizard-v3__nav-item" href="javascript:void(0);" data-ktwizard-type="step">
						<div class="kt-wizard-v3__nav-body">
							<div class="kt-wizard-v3__nav-label">
								<span>4</span>Firma Bilgileri
							</div>
							<div class="kt-wizard-v3__nav-bar"></div>
						</div>
					</a>
				
				</div>
			</div>
			<!--end: Form Wizard Nav -->
		</div>
		<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
	
			<!--begin: Form Wizard Form-->
			<form class="kt-form" id="kt_form">
	
				<!--begin: Form Wizard Step 1-->
				<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
					<jsp:include page="bildirimWizard/vatandas.jsp" />
				</div>
				<!--end: Form Wizard Step 1-->
	
				<!--begin: Form Wizard Step 2-->
				<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
					<jsp:include page="bildirimWizard/digerBilgiler.jsp" />
				</div>
				<!--end: Form Wizard Step 2-->
	
				<!--begin: Form Wizard Step 3-->
				<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
					<jsp:include page="bildirimWizard/basvuruDetaylari.jsp" />
				</div>
				<!--end: Form Wizard Step 3-->
	
				<!--begin: Form Wizard Step 4-->
				<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
					<jsp:include page="bildirimWizard/firmaBilgileri.jsp" />
				</div>
				<!--end: Form Wizard Step 4-->									
	
	
				<!--begin: Form Actions -->
							<button  type="button" class="btn btn-md btn-primary BildirimFormDBBTN float-left" data-kod="1" data-kodd="kaydet" >Mevzuat Uzmanına Aktar</button>
							<button  type="button" class="btn btn-primary BildirimFormDBBTN btn-md float-left ml-2" data-kod="2" data-kodd="kaydetkapat">Kaydet Ve Kapat</button>
							<button  type="reset"  class="btn btn-warning btn-md float-left ml-4" style="margin-left:30px">iptal</button>	
							<button  type="button" class="btn btn-danger btn_karaliste pull-right btn-md float-left ml-2 mr-2" style="margin-right:30px" data-kod="karaliste">Kara Liste</button>
							<button  type="button" class="btn btn-secondary btn-hover-brand btn_belirsiz_cagri pull-right btn-md float-left" style="margin-right:30px" data-kod="belirsiz">Belirsiz Çağrı</button>

							<button class="btn btn-outline-info m-btn m-btn--custom m-btn--icon btn-md float-right" id="btn_next" style="float:right!important">
								<span>
									<span>İlerle </span>&nbsp;&nbsp;
									<i class="la la-arrow-right"></i>
								</span>
							</button>
							<button class="btn btn-outline-info m-btn m-btn--custom m-btn--icon  btn-md float-right" data-ktwizard-type="action-prev" style="margin-right:50px">
								<span>
									<i class="la la-arrow-left"></i>&nbsp;&nbsp;
									<span>Önceki</span>
								</span>
							</button>	
	
				<!--end: Form Actions -->
			</form>
	
			<!--end: Form Wizard Form-->
		</div>
	</div>

	
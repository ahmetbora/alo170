<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="modal-header" id="moda_header">
    <h5 class="modal-title" id="modalLabel">Bildirim Kapatma İşlemi</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body" id="modal_body">
    <form>
		<div class="form-group row">
            <div class="col-md-12">
                <label for="message-text" class="form-control-label">Sonlanma Bilgisi:</label>
			   	<select class="form-control zorunlu" name="fk_taskb_kapanma_statu" id="fk_taskb_kapanma_statu" placeholder="Sonlanma Bilgisi">
			   			<option value="">--Seçiniz--</option>
						<c:forEach items="${statu}" var="n">					
							<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
						</c:forEach>
			   	</select>	                
            </div>
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Açıklama:</label>
            <textarea class="form-control" id="txtAciklama" rows="5"></textarea>
        </div>
    </form>
</div>
<div class="modal-footer" id="modal_footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
    <button type="button" class="btn btn-primary" id="btn_modal_kaydet">Kaydet</button>
</div>
<script>
var token = '${_csrf.token}';
var header ='${_csrf.headerName}';
$('#pnlBildirimDetay').on('hidden.bs.modal', function (e) {
  $("#modalContent").html('');
  $(this).off('hidden.bs.modal');
})
var xhr=null;
$("#btn_modal_kaydet").unbind().click(function(){
    var aciklama=$("#txtAciklama").val();
    var fk_taskb_kapanma_statu=$("#fk_taskb_kapanma_statu option:selected").val();
    var id="${id}";
	if (xhr) {
		xhr.abort();
	}    
    if(fk_taskb_kapanma_statu=="" || fk_taskb_kapanma_statu=="0"){
        alertify.error("Sonlanma Bilgisini Seçiniz!");
        $("#fk_taskb_kapanma_statu").focus();
        return false;
	}
    if(aciklama==""){
        alertify.error("Açıklama Giriniz.");
        return false;
    }
	xhr=$.ajax({
		type	: 'POST',
		url		: '/bildirim/kapatKaydet',
        data:
                {
                    aciklama    : aciklama,
                    id          : id,
                    fk_taskb_kapanma_statu:fk_taskb_kapanma_statu
                },        
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
		success : function(r){
            $('#pnlBildirimDetay').modal("hide");
            alertify.set({ labels: {
                ok     : "Tamam",
                cancel : "Vazgeç"
            } });              
           	alertify.alert("Bildirim Güncellendi.",function(ex){
                  // setTimeout(function(){ location.reload(); }, 750);
			});            
		},
        error: function (request, status, error) {
            xhr.abort();
            alertify.set({ labels: {
                ok     : "Tamam",
                cancel : "Vazgeç"
            } });            
           	alertify.alert(request.responseJSON.message,function(ex){   
			});
        }        
	});	
});
</script>
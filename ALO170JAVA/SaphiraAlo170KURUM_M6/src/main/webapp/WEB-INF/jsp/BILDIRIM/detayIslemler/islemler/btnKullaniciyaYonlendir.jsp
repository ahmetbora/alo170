<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <c:when test="${yetki.bildirim_KullaniciyaYonlendirAll=='true'}">
        <c:if test="${(task.fkHavuz>0) and (task.fkGorevStatu<99)}">
            <a href="javascript:void(0);" class="btn btn-warning btn-sm" id="btn_yonlendir_user"><i class="fa fa-reply"></i> Kullanıcıya Yönlendir</a>&nbsp;
        </c:if>
    </c:when>
    <c:otherwise>
       <c:if test="${(yetki.bildirim_KullaniciyaYonlendir=='true') and (task.fk_agent == userID) and (task.fkGorevStatu<99)}">
        <a href="javascript:void(0);" class="btn btn-warning btn-sm" id="btn_yonlendir_user"><i class="fa fa-reply"></i> Kullanıcıya Yönlendir</a>&nbsp;
       </c:if>
    </c:otherwise>
</c:choose>
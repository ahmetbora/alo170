<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                        <th>NO</th>
                        <th>VATANDA�</th>
                        <th>TAR�H</th>
                        <th>YETK�L�</th>
                        <th>KONU</th>
                        <th>DURUM</th>
                        <th>�EH�R</th>
                        <th>S�RE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${filtre}" var="l" >
                        <tr>
                            <td><a href="/bildirim/bildirimdetay/${l.id}/${l.ukey}">${l.id }</a></td>
                            <td>${l.basvuruSahibi }</td>
                            <td>${l.tarih } ${l.saat }</td>
                            <td>${l.isiYapacakAgent }</td>
                            <td>
                            <c:if test="${l.konu1 ne ''}">${l.konu1 } /</c:if>
                            <c:if test="${l.konu2 ne ''}">${l.konu2 } /</c:if>
                            <c:if test="${l.konu3 ne ''}">${l.konu3 }</c:if>
                                                                    
                            </td>
                            <td>
                                <span class="m-badge  m-badge--${l.cls } m-badge--wide"> 
                                ${l.statu }</span>
                            </td>
                            <td>${l.ilAdi }</td>
                            <td>
                                <span class="m-badge  m-badge--${l.cls } m-badge--wide"> 
                               </span>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
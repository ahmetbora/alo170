<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach items="${css}" var="c">
<link href="${c}" rel="stylesheet">
</c:forEach>
<c:forEach items="${js}" var="j">
<script src="${j}" type="text/javascript"></script>
</c:forEach>
<div class="modal-header" id="moda_header">
    <h5 class="modal-title" id="modalLabel">Kullanıcıya Yönlendir</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body" id="modal_body">
    <form>
		<div class="form-group row">
            <div class="col-md-12">
                <label for="message-text" class="form-control-label">Kullanıcılar:</label>
			   	<select class="form-control zorunlu" name="fk_user" id="fk_user" placeholder="Kullanıcılar">
			   			<option value="">--Seçiniz--</option>
						<c:forEach items="${kullanicilar}" var="n">					
							<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
						</c:forEach>
			   	</select>	                
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label for="message-text" class="form-control-label">Açıklama:</label>
				<textarea class="form-control" id="txtAciklama" placeholder="Açıklama" rows="5"></textarea>
            </div>
        </div>


    </form>
</div>
<div class="modal-footer" id="modal_footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
    <button type="button" class="btn btn-primary" id="btn_modal_kaydet">Güncelle</button>
</div>

<script>
alertify.set({ labels: {
    ok     : "Tamam",
    cancel : "Vazgeç"
} });
$('#pnlBildirimDetay').on('hidden.bs.modal', function (e) {
  $("#modalContent").html('');
  $(this).off('hidden.bs.modal');
})

$("#btn_modal_kaydet").unbind().click(function(){
	var fk_user=$("#fk_user option:selected").val();
	var aciklama=$("#txtAciklama").val();
    var id="${id}";
    var token = '${_csrf.token}';
    var header ='${_csrf.headerName}';

	if(fk_user=="" || fk_user==0){
		alertify.error("Kullanıcı Seçin");
		return false;
	}
			
	$.ajax({
		type	: 'POST',
		url		: '/bildirim/kullaniciyaYonlendirKaydet',
        data:
                {
					fk_user			: fk_user,
					id				: id,
					aciklama		: aciklama
                   
                },        
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
		success : function(r){
            $('#pnlBildirimDetay').modal("hide");
           	alertify.alert("Bildirim Yönlendirildi",function(ex){
                   location.reload();
			});            
		},
        error: function (request, status, error) {
            xhr.abort();
           	alertify.alert(request.responseJSON.message,function(ex){   
			});
        }        
	});	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${(yetki.bildirim_BasvuruIptali == 'true') and (task.fkGorevStatu!=99) and (task.fkGorevStatu!=6)}">
    <a href="javascript:void(0);" class="btn btn-dark btn-sm pull-right" id="btn_iptal"><i class="fa fa-times"></i> Başvuru İptali</a>&nbsp;
</c:if>

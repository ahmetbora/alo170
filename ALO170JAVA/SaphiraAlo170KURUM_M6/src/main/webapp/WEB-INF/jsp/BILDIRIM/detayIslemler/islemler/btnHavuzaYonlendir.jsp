<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:choose>
    <c:when test="${yetki.bildirim_HavuzaYonlendirAll=='true'}">
        <a href="javascript:void(0);" class="btn btn-warning btn-sm" id="btn_yonlendir"><i class="fa fa-reply"></i> Havuza Yönlendir</a>&nbsp;
    </c:when>
    <c:otherwise>
       <c:if test="${(yetki.bildirim_HavuzaYonlendir=='true') and (task.fk_agent == userID)}">
            <a href="javascript:void(0);" class="btn btn-warning btn-sm" id="btn_yonlendir"><i class="fa fa-reply"></i> Havuza Yönlendir</a>&nbsp;
       </c:if>
    </c:otherwise>
</c:choose>
														  									
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="kt-form__section kt-form__section--first">
	<div class="kt-wizard-v3__form">
		<div class="row">
			<div class="col-xl-12">
				<div class="m-form__section m-form__section--first">
					<div class="m-form__heading">
						<h3 class="m-form__heading-title">Kişisel Bilgi Seçenekleri</h3>
					</div>
				
					<div class="form-group m-form__group row">
						<label class="col-xl-2 col-lg-2 col-form-label "><span class="kt-font-danger">Kişisel Bilgi Seçeneği:</span></label>
						<div class="col-xl-10 col-lg-10">
							<select name="vbilgi" class="form-control m-input" id="vbilgi">
								<option value="1" selected>Vatandaş Bilgilerini Vermek İstiyor.</option>
								<option value="0" >Vatandaş Bilgilerini Vermek İstemiyor.</option>
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-xl-2 col-lg-2 col-form-label ">Bilgi Gizliliği:</label>
						<div class="col-xl-10 col-lg-10">
							<select name="isOnayBekle" class="form-control m-input" id="isOnayBekle">
								<option value="1">Vatandaş Bilgileri Gizli Kalsın</option>
								<option value="0" selected>Vatandaş Bilgileri Görüntülenebilsin</option>
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-xl-2 col-lg-2 col-form-label">Bilgi Güncelle:</label>
						<div class="col-xl-10 col-lg-10">
							<select name="kart_update" class="form-control m-input" id="kart_update">
								<option value="0" selected>Vatandaş Bilgileri Olduğu Gibi Kalsın</option>
								<option value="1">Vatandaş Bilgilerini Güncelle</option>
							</select>
						</div>
					</div>

					<div class="m-form__heading">
						<h3 class="m-form__heading-title">Vatandaş Bilgisi</h3>
					</div>

					<div class="form-group m-form__group row">
						<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Adı:</label>
						<div class="col-xl-4 col-lg-4">
							<input type="text" name="adi" id="adi" class="form-control m-input zorunlu" placeholder="">
						</div>

						<label class="col-xl-1 col-lg-1 col-form-label">Ülke:</label>
						<div class="col-xl-5 col-lg-5">
							<select class="form-control m-input" name="ulkeler" id="ulkeler">
								<option value='0'>--- Seciniz ---</option>
								<c:forEach items="${ulkeler}" var="n">
									<option data-id="${n.id}" value="${n.id}" ${n.adi=="Türkiye" ? "selected" : ""}>${n.adi}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Soyadı:</label>
						<div class="col-xl-4 col-lg-4">
							<input type="text" name="soyadi" id="soyadi" class="form-control m-input zorunlu" placeholder="">
						</div>

						<label class="col-xl-1 col-lg-1 col-form-label">il:</label>
						<div class="col-xl-5 col-lg-5">
							<select class="form-control m-input" name="iller" id="iller">
							</select>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Doğum Tarihi:</label>
						<div class="col-xl-4 col-lg-4">
							<input type="text" name="dt" id="dt" class="form-control m-input zorunlu" placeholder="">
						</div>

						<label class="col-xl-1 col-lg-1 col-form-label">İlçe:</label>
						<div class="col-xl-5 col-lg-5">
							<select class="form-control m-input" name="ilceler" id="ilceler">
							</select>
						</div>
					</div>


					<div class="form-group m-form__group row">
						<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> T.C. Kimlik No:</label>
						<div class="col-xl-10 col-lg-10">
							<div class="input-group">
								<input type="text" class="form-control zorunlu" maxlength="11" autocomplete="off" placeholder="T.C. Kimlik No" name="tckimlik" id="tckimlik">
								<div class="input-group-append">
									<button class="btn btn-primary" type="button" id="copyButton" >
										<i class="fa fa-copy"></i>  T.C. Kopyala
									</button>
								</div>
							</div>
							<div id="tcislemler" class="mt-1">
								<a class="btn btn-success btn-md text-white" id="tchange"><i class="fa fa-cog"></i> TC Değiştir</a>
								<a href="javascript:void(0)"  class="btn btn-primary btn-md" id="btnMernis"><i class="fa fa-user"></i> MERNİS</a>
								<a href="javascript:void(0)"  class="btn btn-danger btn-md" id="btnDogrula"><i class="fa 	fa-exclamation-triangle"></i> KİMLİK DOĞRULAMA</a>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>		
	</div>
</div>						
						
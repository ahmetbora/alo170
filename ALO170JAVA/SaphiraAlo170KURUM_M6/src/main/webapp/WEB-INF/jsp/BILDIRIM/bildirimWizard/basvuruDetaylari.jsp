<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="kt-form__section kt-form__section--first">
	<div class="kt-wizard-v3__form">
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Arayan Numara:</label>
			<div class="col-xl-10 col-lg-10">
				<input type="text" id="arayan_numara" name="arayan_numara" class="form-control zorunlu " placeholder="Arayan Numara"  value="${cid }">
			</div>
		 </div>
		 
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Arama Kanalı Türleri:</label>
			<div class="col-xl-10 col-lg-10">
		   		<select id="arama_kanali_tipi" name="arama_kanali_tipi" placeholder="Arama Kanalı Türü" class="form-control zorunlu">
		   			<option value="">Seçiniz</option>
					<c:forEach items="${aramakanalitipi}" var="n">					
						<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
					</c:forEach>									   			
		   		</select>											
			</div>
		 </div>	
		 
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Arama Kanalı:</label>
			<div class="col-xl-10 col-lg-10">
			   	<select class="form-control zorunlu" name="fk_aramakanali" id="fk_aramakanali" placeholder="Arama Kanalı">
			   			<option value="">--Seçiniz--</option>
			   	</select>												
			</div>
		 </div>	
		 
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Arama Sebebi:</label>
			<div class="col-xl-10 col-lg-10">
				<select class="form-control zorunlu" name="fk_aramasebebi" id="fk_aramasebebi" placeholder="Arama Sebebi">
			   		<option value="">--Seçiniz--</option>
					<c:forEach items="${aramasebebi}" var="n">
						<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
					</c:forEach>										   												   
			   	</select>
			</div>
		 </div>	
		 
		<div class="form-group m-form__group row d-none">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Bildirim Tipi:</label>
			<div class="col-xl-10 col-lg-10">
			   	<select class="form-control" name="fk_tip" id="fk_tip" placeholder="Bildirim Tipi">
			   		
			   	</select>											
			</div>
		 </div>	
		 
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Kurum:</label>
			<div class="col-xl-10 col-lg-10">
				<select class="form-control zorunlu" name="tip_level1" id="tip_level1" placeholder="Kurum" >
			   		<option value="">--Seçiniz--</option>
					<c:forEach items="${kurum}" var="n">
						<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
					</c:forEach>													
				</select>											
			</div>
		 </div>	
		 
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Alt Kurum:</label>
			<div class="col-xl-10 col-lg-10">
				<select class="form-control zorunlu" name="tip_level2" id="tip_level2" placeholder="Alt Kurum" >
				</select>											
			</div>
		 </div>		
		 
		<div class="form-group m-form__group row d-none tip_level3">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">*</span> Arama Konusu:</label>
			<div class="col-xl-10 col-lg-10">
				<select class="form-control zorunlu" name="tip_level3" id="tip_level3" placeholder="Başvuru Açıklaması" >
				</select>											
			</div>
		 </div>		
		 
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Başvuru Açıklaması:</label>
			<div class="col-xl-10 col-lg-10">
				<textarea class="form-control" name="aciklama" id="aciklama" style="min-height: 150px;" placeholder="Başvuru Açıklaması" ></textarea>										
			</div>
		</div>					
	</div>
</div>		
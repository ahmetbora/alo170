<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach items="${css}" var="c">
<link href="${c}" rel="stylesheet">
</c:forEach>
<c:forEach items="${js}" var="j">
<script src="${j}" type="text/javascript"></script>
</c:forEach>
<div class="modal-header" id="moda_header">
    <h5 class="modal-title" id="modalLabel">Kurum / Alt Kurum Güncelle</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body" id="modal_body">
    <form>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="recipient-name" class="form-control-label">Arama Kanalı Türleri:</label>
		   		<select id="arama_kanali_tipi" name="arama_kanali_tipi" placeholder="Arama Kanalı Türü" class="form-control zorunlu">
		   			<option value="">Seçiniz</option>
					<c:forEach items="${aramakanalitipi}" var="n">					
						<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
					</c:forEach>									   			
		   		</select>                
            </div>
            <div class="col-md-6">
                <label for="message-text" class="form-control-label">Arama Kanalı:</label>
			   	<select class="form-control zorunlu" name="fk_aramakanali" id="fk_aramakanali" placeholder="Arama Kanalı">
			   			<option value="">--Seçiniz--</option>
			   	</select>	                
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="message-text" class="form-control-label">Arama Sebebi:</label>
				<select class="form-control zorunlu" name="fk_aramasebebi" id="fk_aramasebebi" placeholder="Arama Sebebi">
			   		<option value="">--Seçiniz--</option>
					<c:forEach items="${aramasebebi}" var="n">
						<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
					</c:forEach>										   												   
			   	</select>                
            </div>
            <div class="col-md-6">
                <label for="message-text" class="form-control-label">Kurum:</label>
				<select class="form-control zorunlu" name="tip_level1" id="tip_level1" placeholder="Kurum" >
			   		<option value="">--Seçiniz--</option>
					<c:forEach items="${kurum}" var="n">
						<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
					</c:forEach>													
				</select>		                
            </div>
        </div>
        <div class="form-group row">    
            <div class="col-md-6 ">
                <label for="message-text" class="form-control-label">Alt Kurum:</label>
				<select class="form-control zorunlu" name="tip_level2" id="tip_level2" placeholder="Alt Kurum" >
				</select>	                
            </div>
            <div class="col-md-6">
                <label for="message-text" class="form-control-label">Arama Konusu:</label>
				<select class="form-control zorunlu" name="tip_level3" id="tip_level3" placeholder="Başvuru Açıklaması" style="width:100% !important">
				</select>	                
            </div>                                                
        </div>
    </form>
</div>
<div class="modal-footer" id="modal_footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
    <button type="button" class="btn btn-primary" id="btn_modal_kaydet">Güncelle</button>
</div>

<script>
alertify.set({ labels: {
    ok     : "Tamam",
    cancel : "Vazgeç"
} });
$('#pnlBildirimDetay').on('hidden.bs.modal', function (e) {
  $("#modalContent").html('');
  $(this).off('hidden.bs.modal');
})

		function aramaKanaliTurDetay(selectedId=0){
			var id=$('#arama_kanali_tipi option:selected').val();
			var token = '${_csrf.token}';
			var header ='${_csrf.headerName}';
			$.ajax({
				type: "POST",
				url: '/bildirim/aramaKanaliTurDetay',
				data:
						{
							id: id
						},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success: function (data, textStatus, jqXHR) {
					$("#fk_aramakanali").html(jqXHR.responseText);
					$("#fk_aramakanali").val(selectedId).select();
				},
				error: function (request, status, error) {
					console.log(status);
				}
			});
		}

		$("#arama_kanali_tipi").change(function(){
			aramaKanaliTurDetay();
		});

		function AltKurumlar(){
				var val=$("#fk_aramasebebi option:selected").val();
				var token = '${_csrf.token}';
				var header ='${_csrf.headerName}';
					$.ajax({
						type: "POST",
						url: '/bildirim/altKurumlar',
						data:
								{
									id: $('#tip_level1 option:selected').val()
								},
						beforeSend: function (xhr) {
							xhr.setRequestHeader(header, token);
						},
						success: function (data, textStatus, jqXHR) {			
							$('#tip_level2').html(jqXHR.responseText);
							$('#tip_level3').html('<option value="">--Seçiniz--</option>');								
							$(".tip_level3").addClass("d-none");

						},
						error: function (request, status, error) {
							console.log(status);
						}
					});

		}

		$('#tip_level1').change(function(){
			AltKurumlar();
		});    

		function aramaKonusu(){
			var token = '${_csrf.token}';
			var header ='${_csrf.headerName}';
			$.ajax({
				type: "POST",
				url: '/bildirim/aramaKonulari',
				data:
						{
							id: $('#tip_level2 option:selected').val()
						},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success: function (data, textStatus, jqXHR) {
					$("#tip_level3").select2("destroy");					
					$('#tip_level3').html(jqXHR.responseText);					
					$(".tip_level3").removeClass("d-none");
					$("#tip_level3").select2();
				},
				error: function (request, status, error) {
					//console.log(status);
				}
			});
			
			var val=$("#fk_aramasebebi option:selected").val();
		}

		$('#tip_level2').change(function(){
            $("#tip_level3").select2();
			aramaKonusu();
		});

$("#btn_modal_kaydet").unbind().click(function(){
	var t1=$("#tip_level1 option:selected").val();
	var t2=$("#tip_level2 option:selected").val();
	var t3=$("#tip_level3 option:selected").val();
	var fk_aramakanali=$("#fk_aramakanali option:selected").val();
	var fk_aramasebebi=$("#fk_aramasebebi option:selected").val();
    var id="${id}";
    var token = '${_csrf.token}';
    var header ='${_csrf.headerName}';

	if(t1=="" || t1=="0"){
		alert("Kurum Seçin");
		return false;
	}
	if(t2=="" || t2=="0"){
		alert("Alt Kurum Seçin");
		return false;
	}
	if(t3=="" || t3=="0"){
		alert("Arama Konusu Seçin");
		return false;
	}	
	if(fk_aramakanali=="" || fk_aramakanali=="0"){
		alert("Arama Kanalı Seçin");
		return false;
	}	
	if(fk_aramasebebi=="" || fk_aramasebebi=="0"){
		alert("Arama Sebebi Seçin");
		return false;
	}			
	$.ajax({
		type	: 'POST',
		url		: '/bildirim/kurumAltKurumKaydet',
        data:
                {
                    t1              : t1,
                    t2              : t2,
                    t3              : t3,
                    id              : id,
                    fk_aramakanali  : fk_aramakanali,
                    fk_aramasebebi  : fk_aramasebebi,
                },        
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
		success : function(r){
            $('#pnlBildirimDetay').modal("hide");
           	alertify.alert("Kurum / Alt Kurum Bilgileri Güncellendi",function(ex){
                   location.reload();
			});            
		},
        error: function (request, status, error) {
            xhr.abort();
           	alertify.alert(request.responseJSON.message,function(ex){   
			});
        }        
	});	
});
</script>
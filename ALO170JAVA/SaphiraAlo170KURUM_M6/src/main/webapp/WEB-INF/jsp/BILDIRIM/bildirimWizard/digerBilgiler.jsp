<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="kt-form__section kt-form__section--first">
	<div class="kt-wizard-v3__form">
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Telefon:</label>
			<div class="col-xl-4 col-lg-4">
				<input type="text" name="tel1" id="tel1" class="form-control m-input" placeholder="" value="${cid }">
			</div>
		
			<label class="col-xl-2 col-lg-2 col-form-label">SSK/Bağkur/Emekli Sandık No:</label>
			<div class="col-xl-4 col-lg-4">
				<input type="text" class="form-control " name="sskno" id="sskno" autocomplete="off" placeholder="SSK/Bağkur/Emekli Sandık No" value="">
			</div>
		</div>
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">GSM:</label>
			<div class="col-xl-4 col-lg-4">
				<input type="text" class="form-control " name="gsm" id="gsm" autocomplete="off" placeholder="GSM / Cep Telefonu" value="">
			</div>
		
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger d-none" id="email_zorunlu">*</span> E-Posta:</label>
			<div class="col-xl-4 col-lg-4">
				<input type="text" class="form-control " name="email" id="email" autocomplete="off" placeholder="E-Posta" value="">
			</div>
		</div>		
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Faks:</label>
			<div class="col-xl-4 col-lg-4">
				<input type="text" class="form-control " name="fax" id="fax"  autocomplete="off" placeholder="Faks" value="">	
			</div>
		
			<label class="col-xl-2 col-lg-2 col-form-label">Cinsiyet:</label>
			<div class="col-xl-4 col-lg-4">
			   	<select class="form-control" name="fk_cinsiyet" id="fk_cinsiyet">
					<option value='0'>--- Seciniz ---</option>
					<c:forEach items="${cinsiyet}" var="n">
						<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
					</c:forEach>										   	
			   	</select>	
			</div>
		</div>		
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Eğitim Durumu:</label>
			<div class="col-xl-4 col-lg-4">
			   	<select class="form-control" name="fk_egitimdurumu" id="fk_egitimdurumu">
					<option value='0'>--- Seciniz ---</option>
					<c:forEach items="${egitimdurumu}" var="n">
						<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
					</c:forEach>											   		
			   	</select>	
			</div>
		
		</div>		
		
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Geri Dönüş Kanalı:</label>
			<div class="col-xl-10 col-lg-10">
			   	<select class="form-control" name="fk_geri_donus_kanali" id="fk_geri_donus_kanali">
					<option value='0'>--- Seciniz ---</option>
					<c:forEach items="${geridonuskanali}" var="n">
						<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
					</c:forEach>											   										
			   	</select>	
			</div>
		
		</div>	
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label">Adres:</label>
			<div class="col-xl-10 col-lg-10">
				<textarea class="form-control " name="adres" id="adres" style="min-height: 40px; text-transform:uppercase" placeholder="Adres" rows="5"></textarea>
			</div>
		
		</div>		
		
		<div class="form-group m-form__group row " style="padding-top:0;padding-bottom:0">
			<label class="col-xl-2 col-lg-2 col-form-label">&nbsp;</label>
			<div class="col-xl-10 col-lg-10">
				<label class="m-checkbox">
					<input type="checkbox"> Özel Liste
					<span></span>
				</label>
			</div>
		
		</div>	
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">Güvenlik Sorusu:</span></label>
			<div class="col-xl-10 col-lg-10">
			   	<select class="form-control" name="fk_gsoru" id="fk_gsoru">
					<option value='0'>--- Seciniz ---</option>
					<c:forEach items="${guvenliksorusu}" var="n">
						<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
					</c:forEach>											   		
			   	</select>
			</div>
		
		</div>	
		
		<div class="form-group m-form__group row">
			<label class="col-xl-2 col-lg-2 col-form-label"><span class="kt-font-danger">Cevap:</span></label>
			<div class="col-xl-10 col-lg-10">
				<textarea class="form-control text-danger" name="gsoru_cevap" id="gsoru_cevap" style="min-height: 40px;" placeholder="Cevap" ></textarea>
			</div>
		
		 </div>							
								
	</div>
</div>
		
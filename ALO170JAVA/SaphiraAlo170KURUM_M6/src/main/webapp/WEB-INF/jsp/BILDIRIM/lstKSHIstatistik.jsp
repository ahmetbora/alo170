<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>

                <table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                    	<th>�ehir</th>
                        <th>�ube / Havuz</th>
                        <th>�stlenilmeyen Ba�vuru</th>
                        <th>�stlenilen Ba�vuru</th>
                        <th>�nceleniyor</th>
                        <th>�nceleniyor - Ek S�re</th>
                        <th>G�ncelleme Talebi</th>
                        <th>Reddedildi</th>
                        <th>Sonu�lanm��</th>
                        <th>Toplam</th>
                        <th>48 Saat ��inde Kapanan</th>
                        <th>48 Saati A�an Kapanan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${istatistik}" var="i" >
                        <tr>
                        	<td>${i.il }</td>
                           	<td>${i.adi }</td>
                            <td>${i.islem_yapilmamis }</td>
                            <td>${i.birime_atandi }</td>
                            <td>${i.inceleniyor }</td>
                            <td>${i.inceleniyor_eksure }</td>
                            <td>${i.guncelleme_talebi }</td>
                            <td>${i.reddedildi }</td>
                            <td>${i.kapanan }</td>
                            <td>${i.toplam }</td>
                            <td>${i.zamaninda_kapatilmayan }</td>
                            <td>${i.zamaninda_kapanan }</td>                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
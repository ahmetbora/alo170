<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="header.jsp" />
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation-1"></i>
			<span></span>
		</div>
		<div class="m-alert__text">
			<strong>HATA!</strong> 
			BU SAYFAYI GÖRME YETKİNİZ BULUNMAMAKTADIR.<BR>YETKİ İŞLEMLERİ İÇİN YÖNETİCİNİZE BAŞVURUNUZ.
		</div>
		<div class="m-alert__close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			</button>
		</div>
	</div>
</div>
<jsp:include page="footer.jsp" />
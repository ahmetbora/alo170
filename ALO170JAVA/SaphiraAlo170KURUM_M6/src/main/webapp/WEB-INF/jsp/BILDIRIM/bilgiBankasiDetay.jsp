<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
 <div class="m-section__content">
	<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
		<div class="m-demo__preview">
			<btn class="btn btn-sm btn-danger pull-right" id="detay_kapat"><i class="fa fa-times"></i> Kapat</btn>
			<div class="clearfix"></div>
			<h3 style="margin:0">
			${sss.baslik}
			</h3>
			<hr>
			<p>
			${sss.aciklama}
			</p>
			
		</div>
	</div>
</div>
<script>
$("#detay_kapat").click(function(){
	$("#bilgi_bankasi_detay").hide();
	$("#bilgi_bankasi_detay").html('');
	$('#grid').show();		
});
</script>
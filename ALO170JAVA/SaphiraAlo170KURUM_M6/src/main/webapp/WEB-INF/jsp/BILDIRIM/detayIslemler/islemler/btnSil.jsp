<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${yetki.bildirim_Sil == 'true'}">
    <a href="javascript:void(0);" onclick="" class="btn btn-dark btn-sm" id="btn_sil"><i class="fa fa-trash"></i> Bildirim Sil</a>&nbsp;
</c:if>	
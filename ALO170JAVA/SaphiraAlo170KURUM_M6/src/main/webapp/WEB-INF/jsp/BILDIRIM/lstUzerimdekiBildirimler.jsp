<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
 
 
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Üzerimdeki Bildirimler </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Bildirimler </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Üzerimdeki Bildirimler </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar d-none">
				<div class="kt-subheader__wrapper">
					<a href="#" class="btn kt-subheader__btn-primary">
						Actions &nbsp;
	
						<!--<i class="flaticon2-calendar-1"></i>-->
					</a>
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
						<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000" />
								</g>
							</svg>
	
							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
	
							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Add anything or jump to:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Order</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Ticket</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Goal</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support Case</span>
										<span class="kt-nav__link-badge">
											<span class="kt-badge kt-badge--success">5</span>
										</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
									<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
								</li>
							</ul>
	
							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-elevate alert-light alert-bold" role="alert">
					<div class="alert-text kt-font-info">
						<i class="fa fa-exclamation-circle"></i> <strong>ÜZERİMDEKİ BİLDİRİMLER</strong> Bu ekranda görevli olduğunuz bildirimler listelenir.
					</div>
				</div>			
			</div>	
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								Üzerimdeki Bildirimler
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div id=bildirimGrid></div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>


<script id="havuzSureTemplate" type="text/x-kendo-template">
	# if(havuzSure >= 0 && havuzSure <= 24) { #
		<span class="kt-badge kt-badge--inline kt-badge--success">#: havuzSure # Saat</span>
	# } else if(havuzSure > 24 && havuzSure < 48) { #
		<span class="kt-badge kt-badge--inline kt-badge--warning">#: havuzSure # Saat</span>
	# } else if(havuzSure > 48) { #
		<span class="kt-badge kt-badge--inline kt-badge--danger">#: havuzSure # Saat</span>
	# } else { #
		<span class="kt-badge kt-badge--inline kt-badge--success">#: havuzSure # Saat</span>
	# } #
</script>

<script type="text/x-kendo-template" id="bildirimGridToolbar">
    <div class="toolbar">
		<div class="col-md-7 col-sm-12 float-left text-left m--font-info">
			<h4 id="grid_title"></h4>
		</div>
        <div class="col-md-4 col-sm-12 float-right text-right">
        
        </div>
    </div>
</script>
<script id="vatandasTemplate" type="text/x-kendo-template">
	# if(isOnayBekle=='0') { #
		#: basvuruSahibi #
	# } else { #
		***** *****
	# } #
</script>

<script id="yapacakAgentTemplate" type="text/x-kendo-template">
	# if(fk_agent=='0') { #
		<span class="m-badge m-badge--danger m-badge--wide">Atanmamış</span>
	# } else { #
		#: isiYapacakAgent #
	# } #
</script>
<script id="konuTemplate" type="text/x-kendo-template">
	#: konu1 # / #: konu2 # / <strong>#: konu3 # </strong>
</script>
<script id="statuTemplate" type="text/x-kendo-template">

	# if(fkGorevStatu=="20"){ #
		<span class="kt-badge kt-badge--danger kt-badge--inline">Yeniden Açılmış</span>
	# } else { #
		<span class="kt-badge kt-badge--#: cls # kt-badge--inline">#: statu #</span>
	# } #
</script>	
<script id="tarihTemplate" type="text/x-kendo-template">
	#: kendo.toString(kendo.parseDate(tarih), 'dd-MM-yyyy HH:mm') #
</script>
<script id="chkTemplate" type="text/x-kendo-template">
<input type="checkbox" id="#: id #" class="k-checkbox chkTask">
<label class="k-checkbox-label" for="#: id #"></label>
</script>
<script id="detayTemplate" type="text/x-kendo-template">
<a class="btn btn-info btn-sm" href="/bildirim/bildirimdetay/#: id #" >
	Detay
</a>
</script>


<script>

var crudServiceBaseUrl="/bildirim/UzerimdekiBildirimListesi";
bildirimGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "tarih"				:   { editable:false,type: "string" },
                "basvuruSahibi"		:   { editable:false,type: "string" },
                "isiYapacakAgent"	:   { editable:false,type: "string" },
                "konuFull"			:   { editable:false,type: "string" },
                "statu"				:   { editable:false,type: "string" },
                "ilAdi"				:   { editable:false,type: "string" },
                "lokasyon"			:   { editable:false,type: "string" },
                "kaydiAcanAgent"	:   { editable:false,type: "string" },
                "isOnayBekle"		:   { editable:false,type: "string" },
                "saat"				:	{ editable:false,type: "string" },
                "fkGorevStatu"		:	{ editable:false,type: "number" },
                "havuzSure"			:	{ editable:false,type: "number" },
                "havuzAdi"			:	{ editable:false,type: "string" },
                
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});

function bildirimGridDsFilter(){
	bildirimGridDataSource.filter([
            {
                field: "tip",
                operator: "eq",
                value: ""
            }
        ]
    );

}

var bildirimGrid = $("#bildirimGrid").kendoGrid({
    "columns": [

        {
            "field": "id",
            "filterable": false, "groupable": false, "title": "#",width:85
        },
        {
            template:kendo.template($("#tarihTemplate").html()),
            width:120,
            title:"Tarih"
        },
        {
            template:kendo.template($("#vatandasTemplate").html()),
            title:"Başvuru Sahibi",
            width:200
            
        },  
        {
            template:kendo.template($("#konuTemplate").html()),
            title:"Konu",
         
        },  
        {
            template:kendo.template($("#statuTemplate").html()),
            title:"Görev Durumu",
            width:140
        },  

        {
            "field": "havuzAdi",
            "filterable": false, "groupable": false, "title": "Havuz",
            width:200
        },        
        {
        	template:kendo.template($("#havuzSureTemplate").html()),
        	title:"Süre",
            width:100
        },        
        {
            template:kendo.template($("#detayTemplate").html()),
            width:85
        }   
                   
    ],
    "autoBind": true,
    "dataSource":bildirimGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": false,
    "navigatable": false,
    "editable": "false",

    "pageable": {"alwaysVisible": true, "pageSize": 20,"responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [30, 50]},
    "height":800,
    "toolbar": kendo.template($("#bildirimGridToolbar").html())
});

var grid = $("#bildirimGrid").data("kendoGrid");

grid.table.on("click", ".k-checkbox" , selectRow);


var checkedIds = {};
function selectRow() {
  var checked = this.checked,
      row = $(this).closest("tr"),
      grid = $("#bildirimGrid").data("kendoGrid"),
      dataItem = grid.dataItem(row);

  checkedIds[dataItem.id] = checked;
  if (checked) {
    //-select the row
    row.addClass("k-state-selected");
  } else {
    //-remove selection
    row.removeClass("k-state-selected");
  }
  console.log(checkedIds);
}

</script>


<jsp:include page="footer.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>VATANDAŞ</th>
                        <th>TARİH SAAT</th>
                        <th>İŞİ YAPACAK AGENT</th>
                        <th>KONU</th>
                        <th>GÖREV DURUMU</th>
                        <th>ŞEHİR</th>
                        <th>SÜRE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${filtre}" var="l" >
                        <tr>
                            <td><a href="/bildirim/bildirimdetay/${l.id}/${l.ukey}">${l.id }</a></td>
                            <td>${l.basvuruSahibi }</td>
                            <td>${l.tarih } ${l.saat }</td>
                            <td>${l.isiYapacakAgent }</td>
                            <td>
                            <c:if test="${l.konu1 ne ''}">${l.konu1 } /</c:if>
                            <c:if test="${l.konu2 ne ''}">${l.konu2 } /</c:if>
                            <c:if test="${l.konu3 ne ''}">${l.konu3 }</c:if>                                                      
                            </td>
                            <td>
                                <span class="m-badge  m-badge--${l.cls } m-badge--wide"> 
                                ${l.statu }</span>
                            </td>
                            <td>${l.ilAdi }</td>
                            <td>
                                <span class="m-badge  m-badge--${l.cls } m-badge--wide"> 
                               </span>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="header.jsp" />

<style>
	.zorunluclass {
		color: red;
	}
	.perc-w-100{
	width:100%;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Wizard 3 </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="" class="kt-subheader__breadcrumbs-link">
						Pages </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="" class="kt-subheader__breadcrumbs-link">
						Wizard 3 </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar d-none">
				<div class="kt-subheader__wrapper">
					<a href="#" class="btn kt-subheader__btn-primary">
						Actions &nbsp;
	
						<!--<i class="flaticon2-calendar-1"></i>-->
					</a>
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
						<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000" />
								</g>
							</svg>
	
							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
	
							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Add anything or jump to:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Order</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Ticket</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Goal</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support Case</span>
										<span class="kt-nav__link-badge">
											<span class="kt-badge kt-badge--success">5</span>
										</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
									<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
								</li>
							</ul>
	
							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
	<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-elevate alert-light alert-bold" role="alert">
					<div class="alert-text kt-font-success">
						BİLDİRİM NUMARASI <strong id="sonBildirimNo" class="">${agentSonBildirimNo}</strong>
					</div>
				</div>			
			</div>
			<div class="col-md-9">
				<div class="kt-portlet kt-portlet--tabs">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Yeni Bildirim   ${ext}
								<small>${cid}</small>						
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#bildirimform" role="tab" aria-selected="true">
										BİLDİRİM
									</a>
								</li>
								<li class="nav-item d-none">
									<a class="nav-link" data-toggle="tab" href="#gecmis" role="tab" aria-selected="false">
										DAHA ÖNCEKİ BİLDİRİMLER
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#bilgibankasi" role="tab" aria-selected="false">
										BİLGİ BANKASI
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#bildirim_scriptleri" role="tab" aria-selected="false">
										BİLDİRİM SCRIPTLERİ
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#mesajlar" role="tab" aria-selected="false">
										MESAJLAR
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#webserviscont" role="tab" aria-selected="false">
										WEBSERVİS
									</a>
								</li>																					
							</ul>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="tab-content">
							<div class="tab-pane active " id="bildirimform">
								<jsp:include page="frmBildirim.jsp" />
							</div>
							<div class="tab-pane gecmis_content" id="gecmis">
								
							</div>					
							<div class="tab-pane" id="bilgibankasi">
								<jsp:include page="bilgiBankasi.jsp" />
							</div>
							<div class="tab-pane" id="bildirim_scriptleri">
								<jsp:include page="bildirimScript.jsp" />
							</div>
							<div class="tab-pane" id="mesajlar">
								<jsp:include page="vtMesaj.jsp" />
							</div>	
							<div class="tab-pane" id="webserviscont">
								<div id="webservis_sonuc"></div>
							</div>	
						</div>
					</div>
				</div>				
			</div>
			<div class="col-md-3">
				<div class="kt-portlet">
					<div class="kt-portlet__body">
			            <div class="details">
			                <div class="number">
			                    <h2 id="sayac_basla" class="m--font-primary"></h2>
			                </div>
			                <div class="desc"><h2 class="m--font-primary">Görüşme Süreniz</h2></div>
			            </div>							
					</div>
				</div>	
				<jsp:include page="webServisleri.jsp" />						
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="mdl_bildirim" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
				
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function(){

		$("#konu1").select2({ placeholder: "Seçiniz", minimumResultsForSearch: 1 / 0 });
		$("#konu2").select2({ placeholder: "Seçiniz", minimumResultsForSearch: 1 / 0 });
		$("#konu3").select2({ placeholder: "Seçiniz", minimumResultsForSearch: 1 / 0 });
	});
</script>

<style>


	.form-group.m-form__group > label {
		color: #000 !important;
	}

	.m-form__help {
		font-weight: 300 !important;;
		font-size: .85rem !important;
		padding-top: 7px !important;
	}
	.m-select2 {
		width:100%;
	}


</style>


<script>
	var _csrf_token = '${_csrf.token}' ;
	var _csrf_param_name = '${_csrf.parameterName}' ;


	$(document).ready(function(){
		$("input[type=text]").each(function(){
			$(this).css("text-transform","uppercase");
		});
		$("textarea").each(function(){
			$(this).css("text-transform","uppercase");
		});

		$(function () {
			var token = $("input[name='_csrf']").val();
			var header = "X-CSRF-TOKEN";
			$(document).ajaxSend(function(e, xhr, options) {
				xhr.setRequestHeader(header, _csrf_token);
			});
		});


		$.ajaxSetup({
			headers:
					{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		});



		if( $('#ulkeler').val() != "" || $('#ulkeler').val() != 0){
			ulkeDetay();
		}
		if( $('#iller').val() != "" || $('#iller').val() != 0){
			ilDetay($("#iller").val());
		}

		$("#ulkeler").change(function(){
			ulkeDetay($("#ulkeler").val());
		});
		$("#iller").change(function(){
			ilDetay($("#iller").val());
		});

		$("#f_fk_il").change(function(){
			var id=$("#f_fk_il option:selected").val();
			ilDetayFirma(id);
		});

		$("#tckimlik").keyup(function(e){
			if(e.which==67 || e.which==17 || e.ctrlKey==true) {
				// alertify.error("Kopyala butonuna basınız");
				return false;
			}
		});
		$("#btnDogrula").click(function(e){
			e.preventDefault();

			var tckn=$("#tckimlik").val();
			if(tckn.length!=11){
				alertify.error("T.C. Kimlik 11 Haneli Olmalıdır.");
				return false;
			}
			$("#dogrulamaload").removeClass("d-none");
			$("#dogrulama_content").addClass("d-none");
			$.ajax({
				type	: 'POST',
				url		: 'http://172.30.10.253/tools/alo170KimlikDogrulama.php',
				data	: "tckn="+$("#tckimlik").val()+"fk_agent=${userID}&ip=${ipAdres}",
				success : function(r){
					$("#dogrulama_content").removeClass("d-none");
					$("#dogrulama_content").html(r);
					$("#dogrulamaload").addClass("d-none");

				}
			});
		});

		var options = {
			    startStep: 1,
			    manualStepForward: false,
			    clickableSteps: false
			};
		
		var bildirimWizard = new KTWizard('kt_wizard_v3',options).
		on("change", function(r) {
			
			var ls=bildirimWizard.isLastStep();
			var currentStep=bildirimWizard.getStep();
			if(ls==true){
				$("#btn_next").hide();
			}else{
				$("#btn_next").show();
			}

		});



		$("#btn_next").click(function(e){
			e.preventDefault();

			var currentStep=bildirimWizard.getStep();
			if(currentStep=="1"){
				checkStep1()
			}
			if(currentStep=="2"){
				checkStep2()
			}
			if(currentStep=="3"){
				checkStep3()
			}
			if(currentStep=="4"){
				checkStep4()
			}
		});

		$(".m-wizard__step-number").click(function(e){
			alert("asd");
			e.preventDefault();
			var step=$(this).attr("data-step");
			var currentStep=bildirimWizard.getStep();
			
		});

		function checkStep1(){
			var adi=$("#adi").val();
			var soyadi=$("#soyadi").val();
			var tckimlik=$("#tckimlik").val();
			var fk_ulke=$("#fk_ulke option:selected").val();
			var fk_il=$("#fk_il option:selected").val();
			var fk_ilce=$("#fk_ilce option:selected").val();
			var dt=$("#dt").val();
			var vbilgi=$("#vbilgi option:selected").val();
			var isOnayBekle=$("#isOnayBekle option:selected").val();
			var kart_update=$("#kart_update option:selected").val();
			
			if(vbilgi==""){
				alertify.error("Vatandaş Kişisel Bilgi Seçeneği Boş Bırakılamaz");
				$("#vbilgi").focus();
				return false;
			}
			if(isOnayBekle==""){
				alertify.error("Vatandaş Bilgi Gizliliği Boş Bırakılamaz");
				$("#isOnayBekle").focus();
				return false;
			}
			if(kart_update==""){
				alertify.error("Vatandaş Bilgi Güncelleme Seçeneği Boş Bırakılamaz");
				$("#kart_update").focus();
				return false;
			}
			
			if(adi==""){
				alertify.error("Vatandaş Adı Boş Bırakılamaz");
				$("#adi").focus();
				return false;
			}
			if(soyadi==""){
				alertify.error("Vatandaş Soyadı Boş Bırakılamaz");
				$("#soyadi").focus();
				return false;
			}
			if(dt==""){
				alertify.error("Vatandaş Doğum Tarihi Boş Bırakılamaz");
				$("#dt").focus();
				return false;
			}

			if(tckimlik.length<11){
				alertify.error("Vatandaş T.C. Kimlik Numarası 11 Haneli Olarak Giriniz");
				$("#tckimlik").focus();
				return false;
			}



			bildirimWizard.goNext();
		}

		function checkStep2(){
			var email=$("#email").val();
			var fk_geri_donus_kanali=$("#fk_geri_donus_kanali option:selected").val();
			if(fk_geri_donus_kanali=="1"){
				if(email==""){
					alertify.error("E-Posta Adresi Giriniz.");
					$("#email").focus();
					return false;
				}
			}
			bildirimWizard.goNext();
		}
		
		function checkStep3(){
			var firmaBilgi=$(".firma_bilgileri").hasClass('d-none');
	
			var arayan_numara=$("#arayan_numara").val();
			var arama_kanali_tipi=$("#arama_kanali_tipi option:selected").val();
			var fk_aramakanali=$("#fk_aramakanali option:selected").val();
			var fk_aramasebebi=$("#fk_aramasebebi option:selected").val();
			var tip_level1=$("#tip_level1 option:selected").val();
			var tip_level2=$("#tip_level2 option:selected").val();
			var tip_level3=$("#tip_level3 option:selected").val();
			if(arayan_numara==""){
				alertify.error("Arayan Numara Boş Bırakılamaz");
				$("#arayan_numara").focus();
				return false;
			}
			if(arama_kanali_tipi=="" || arama_kanali_tipi=="0"){
				alertify.error("Arama Kanalı Türü Boş Bırakılamaz");
				$("#arama_kanali_tipi").focus();
				return false;
			}
			if(fk_aramakanali=="" || fk_aramakanali=="0"){
				alertify.error("Arama Kanalı Boş Bırakılamaz");
				$("#fk_aramakanali").focus();
				return false;
			}
			if(fk_aramasebebi=="" || fk_aramasebebi=="0"){
				alertify.error("Arama Sebebi Boş Bırakılamaz");
				$("#fk_aramasebebi").focus();
				return false;
			}
			if(tip_level1=="" || tip_level1=="0"){
				alertify.error("Kurum Boş Bırakılamaz");
				$("#tip_level1").focus();
				return false;
			}
			if(tip_level2=="" || tip_level2=="0"){
				alertify.error("Alt Kurum Boş Bırakılamaz");
				$("#tip_level2").focus();
				return false;
			}
			if(tip_level3=="" || tip_level3=="0"){
				alertify.error("Arama Konusu Boş Bırakılamaz");
				$("#tip_level3").focus();
				return false;
			}


		
			if(firmaBilgi==true){		
				alert("kaydet");
			}else{
				//firma bilgileri formuna git
				bildirimWizard.goNext();
				
			}
			
		}
		
		function checkStep4(){
			alert("step4 check");
			bildirimWizard.goNext();
		}





		function ulkeDetay(){
			var token = '${_csrf.token}';
			var header ='${_csrf.headerName}';
			var ulke=$("#ulkeler option:selected").val();
			$.ajax({
				type: "POST",
				url: '/bildirim/ulkeDetay',
				data:
						{
							id: ulke
						},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success: function (data, textStatus, jqXHR) {
					$("#iller").html(jqXHR.responseText);
					$("#iller").select2("destroy");
					$("#iller").select2();
				},
				error: function (request, status, error) {
					console.log(status);
				}
			});

		}

		function ilDetay(id){
			var token = '${_csrf.token}';
			var header ='${_csrf.headerName}';
			$.ajax({
				type: "POST",
				url: '/bildirim/ilDetay',
				data:
						{
							id: id
						},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success: function (data, textStatus, jqXHR) {
					$("#ilceler").html(jqXHR.responseText);
					$("#ilceler").select2("destroy");
					$("#ilceler").select2();
				},
				error: function (request, status, error) {
					console.log(status);
				}
			});

		}

		function ilDetayFirma(id){
			var token = '${_csrf.token}';
			var header ='${_csrf.headerName}';
			$.ajax({
				type: "POST",
				url: '/bildirim/ilDetay',
				data:
						{
							id: id
						},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success: function (data, textStatus, jqXHR) {
					$("#f_fk_ilce").html(jqXHR.responseText);
					//$("#ilceler").select2("destroy");
					$("#f_fk_ilce").select2();
				},
				error: function (request, status, error) {
					console.log(status);
				}
			});

		}

		function aramaKanaliTurDetay(selectedId=0){
			var id=$('#arama_kanali_tipi option:selected').val();
			var token = '${_csrf.token}';
			var header ='${_csrf.headerName}';
			$.ajax({
				type: "POST",
				url: '/bildirim/aramaKanaliTurDetay',
				data:
						{
							id: id
						},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success: function (data, textStatus, jqXHR) {
					$("#fk_aramakanali").html(jqXHR.responseText);
					$("#fk_aramakanali").val(selectedId).select();
				},
				error: function (request, status, error) {
					console.log(status);
				}
			});
		}

		$("#arama_kanali_tipi").change(function(){
			aramaKanaliTurDetay();
		});



		function AltKurumlar(){
				var val=$("#fk_aramasebebi option:selected").val();
				var token = '${_csrf.token}';
				var header ='${_csrf.headerName}';
					if(val=="13"){
						$(".firma_bilgileri").removeClass("d-none");
						$("#firma_bilgi").val("1");
						$(".BildirimFormDBBTN[data-kod=kaydetkapat]").attr("disabled",true);
					}else{
						$(".firma_bilgileri").addClass("d-none");
						$(".firma_bilgileri input , .firma_bilgileri textarea").val('');
						$(".firma_bilgileri select").val('');
						$("#firma_bilgi").val("0");
						$(".BildirimFormDBBTN[data-kod=kaydetkapat]").attr("disabled",false);
					}
					$.ajax({
						type: "POST",
						url: '/bildirim/altKurumlar',
						data:
								{
									id: $('#tip_level1 option:selected').val()
								},
						beforeSend: function (xhr) {
							xhr.setRequestHeader(header, token);
						},
						success: function (data, textStatus, jqXHR) {			
							$('#tip_level2').html(jqXHR.responseText);
							$('#tip_level3').html('<option value="">--Seçiniz--</option>');								
							$(".tip_level3").addClass("d-none");

						},
						error: function (request, status, error) {
							console.log(status);
						}
					});

		}

		$('#tip_level1').change(function(){
			AltKurumlar();
		});


		function aramaKonusu(){
			var token = '${_csrf.token}';
			var header ='${_csrf.headerName}';
			$.ajax({
				type: "POST",
				url: '/bildirim/aramaKonulari',
				data:
						{
							id: $('#tip_level2 option:selected').val()
						},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success: function (data, textStatus, jqXHR) {
					$("#tip_level3").select2("destroy");					
					$('#tip_level3').html(jqXHR.responseText);					
					$(".tip_level3").removeClass("d-none");
					$("#tip_level3").select2();
				},
				error: function (request, status, error) {
					console.log(status);
				}
			});
			
			var val=$("#fk_aramasebebi option:selected").val();
			if(val=="13"){
				$(".firma_bilgileri").removeClass("d-none");
				$("#firma_bilgi").val("1");
				$(".BildirimFormDBBTN[data-kod=kaydetkapat]").attr("disabled",true);
			}else{
				$(".firma_bilgileri").addClass("d-none");
				$(".firma_bilgileri input , .firma_bilgileri textarea").val('');
				$(".firma_bilgileri select").val('');
				$("#firma_bilgi").val("0");
				$(".BildirimFormDBBTN[data-kod=kaydetkapat]").attr("disabled",false);
			}
		}

		$('#tip_level2').change(function(){
			aramaKonusu();
		});


		$("#fk_aramasebebi").change(function(){
			var val=$("#fk_aramasebebi option:selected").val();
			var t2=$("#tip_level2 option:selected").val();
			if(val=="13"){
				$(".firma_bilgileri").removeClass("d-none");
				$("#firma_bilgi").val("1");
				$(".BildirimFormDBBTN[data-kod=kaydetkapat]").attr("disabled",true);
			}else{
				$("#firma_bilgi").val("0");
				$(".firma_bilgileri").addClass("d-none");
				$(".firma_bilgileri input , .firma_bilgileri textarea").val('');
				$(".firma_bilgileri select").val('');
				$(".BildirimFormDBBTN[data-kod=kaydetkapat]").attr("disabled",false);
			}
		});


		if( $('#tip_level1').val() != "" || $('#tip_level1').val() != 0){
			$('#tip_level1').change();
		}

	
		<c:if test="${ivrtc[0].que!=''}">
		$('#arama_kanali_tipi').val('1').change();
		</c:if>
		
		var ara="1";
		<c:if test="${fromQue=='7136'}">
		ara="2";
		</c:if>	
		
		if( $('#arama_kanali_tipi').val() != "" || $('#arama_kanali_tipi').val() != 0){		
			aramaKanaliTurDetay(ara);
		}

/*
 * girilen tc yi clipboarda alıyor
 */
		$('#copyButton').on("click", function(){
			value = $("#tckimlik").val();
			if(value.length!=11){
				alertify.error("T.C. Kimlik 11 Haneli Olmalıdır.");
				return false;
			}

			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val(value).select();
			document.execCommand("copy");
			$temp.remove();
			alertify.success(value + " T.C. Kimlik Numarası Kopyalandı.");
			})
			
			
			$('#fk_geri_donus_kanali').change(function(){
				if( $('#fk_geri_donus_kanali option:selected').val() == 1){
					$("#email_zorunlu").removeClass("d-none");		
				}else{
					$("#email_zorunlu").addClass("d-none");
				}
			});

		$("#ulkeler").select2();
		$("#iller").select2();
		$("#ilceler").select2();
		$("#tip_level3").select2();
		$("#f_fk_il").select2();
		$("#fk_proje").select2();
		
		$("#tckimlik,#scs,#sskno").inputFilter(function(value) {
			  return /^\d*$/.test(value); });



/*
 * girilen tc kimlik no yu sorgulayıp verileri alıp formu doldururyor
 */
		function tcSorgula(){
			var tckn=$("#tckimlik").val();

			if(tckn.length>10) {
			$("#tab_gecmis").addClass("d-none");
			if(tckn.length!=11){
				alertify.error("T.C. Kimlik 11 Haneli Olmalıdır.");
				return false;
			}
			$("#mernisload").removeClass("d-none");
			$("#dogrulama_content").addClass("d-none");
			$.ajax({
				type	: 'POST',
				url		: '/bildirim/openKart',
				dataType:"JSON",
				data	: "tckn="+$("#tckimlik").val(),
				success : function(r){
					var id=parseFloat(r.kartid);
					
					if(id>0){
						$("#adi").val(r.kartadi);
						$("#soyadi").val(r.kartSoyadi);
						$("#email").val(r.email);
						$("#tel1").val(r.tel);
						$("#gsm").val(r.gsm);
						$("#fk_kart").val(r.kartid);
						$("#sskno").val(r.sskNo);
						$("#faks").val(r.fax);
						$("#iller").val(r.fkIl).change();
						setTimeout(function(){
							$("#ilceler").val(r.fkIlce).change();
						}, 2000);
						
						$('#fk_egitimdurumu').val(r.fkEgitimDurumu);
						$("#fk_cinsiyet").val(r.fkCinsiyet);

						$.ajax({
							type	: 'POST',
							url		: '/bildirim/bildirimGecmisi',
							data	: "id="+r.kartid,
							success : function(r){
								$("#tab_gecmis").removeClass("d-none");
								$(".gecmis_content").html(r);
							}
						});
					} else if(r.kartid=="0") {
						$("#fk_kart").val(0);
					}else{
					}
				}
			});

			$.ajax({
				type	: 'POST',
				url		: 'http://172.30.10.253/tools/WebServis.php',
				data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
				success : function(r){
						//$("#mernis_content").html(r);
						if($("#mernis_sonuc").val()=="1"){

							$("#dt").val($("#mernis_dt").html());
							$("#adi").val($("#mernis_adi").html());
							$("#soyadi").val($("#mernis_soyadi").html());
							var c=$("#mernis_cins").html();

							if(c=="Erkek"){
								$("#fk_cinsiyet option[value=1]").attr("selected",true);
							}else{
								$("#fk_cinsiyet option[value=2]").attr("selected",true);
							}
							$("#mernisload").addClass("d-none");
						}
						$("#mernisload").addClass("d-none");
				}
			});
			}
		}

		$("#tchange").click(function(){
			var tckn=$("#tckimlik").val();
			if(tckn.length!=11){
				alertify.error("T.C. Kimlik 11 Haneli Olmalıdır.");
				return false;
			}
			tcSorgula();
		});
		$("#tckimlik").keyup(function(e){
			 if(e.which==67 || e.which==17 || e.ctrlKey==true) {
				// alertify.error("Kopyala butonuna basınız");
				 return false;
			 }
			tcSorgula();
		});

/*
 * vatandaş bilgilerini vermek istemiyorsa bilinmeyen vatanaş datası çekiliyor
 */
		function BilinmeyenVatandasBilgisi(){
				var val=$("#vbilgi option:selected").val();
				if(val=="0"){
					$("#tckimlik").attr("disabled",true);
					$("#adi").attr("disabled",true);
					$("#soyadi").attr("disabled",true);
					$("#email").attr("disabled",true);
					$("#tel1").attr("disabled",true);
					$("#gsm").attr("disabled",true);
					$("#fk_kart").attr("disabled",true);
					$("#sskno").attr("disabled",true);
					$("#faks").attr("disabled",true);
					$("#dt").attr("disabled",true);
					$("#kart_update").attr("disabled",true);
					$("#isOnayBekle").attr("disabled",true);
					$.ajax({
						type	: 'POST',
						url		: '/bildirim/openKart',
						dataType:"JSON",
						data	: "tckn=10000000002999999999",
						success : function(r){
							var id=parseFloat(r.kartid);
							if(id>0){
								$("#adi").val(r.kartadi);
								$("#soyadi").val(r.kartSoyadi);
								$("#email").val(r.email);
								$("#tel1").val(r.tel);
								$("#gsm").val(r.gsm);
								$("#fk_kart").val(r.kartid);
								$("#sskno").val(r.sskNo);
								$("#faks").val(r.fax);						
								$('#fk_egitimdurumu').val(r.fkEgitimDurumu);
								$("#fk_cinsiyet").val(r.fkCinsiyet);
								$("#tckimlik").val("10000000002999999999");
								$("#dt").val("2016-01-17");
								$("#kart_update").attr("disabled",true);
								$("#kart_update").val("0");
								$("#isOnayBekle").attr("disabled",true);
								$("#isOnayBekle").val("0");

								
							}else{
							}
						}
					});

				}else{
					$("#tckimlik").attr("disabled",false);
					$("#adi").attr("disabled",false);
					$("#soyadi").attr("disabled",false);
					$("#email").attr("disabled",false);
					$("#tel1").attr("disabled",false);
					$("#gsm").attr("disabled",false);
					$("#fk_kart").attr("disabled",false);
					$("#sskno").attr("disabled",false);
					$("#faks").attr("disabled",false);
					$("#dt").attr("disabled",false);
					$("#kart_update").attr("disabled",false);
					$("#isOnayBekle").attr("disabled",false);
					$("#tckimlik").val("");
					$("#adi").val("");
					$("#soyadi").val("");
					$("#email").val("");
					$("#tel1").val("");
					$("#gsm").val("");
					$("#fk_kart").val("");
					$("#sskno").val("");
					$("#faks").val("");
					$("#tckimlik").val("");
					$("#dt").val("");
				}

		}



		$("#vbilgi").change(function(){
			BilinmeyenVatandasBilgisi();
		});


		Date.prototype.addSeconds= function(h){
		    this.setSeconds(this.getSeconds()+h);
		    return this;
		}
		var date = new Date().addSeconds(0) / 1000

		$('#sayac_basla').countid({
			clock: true,
			dateTime: date,
			dateTplRemaining: "%H:%M:%S",
			dateTplElapsed: "%H:%M:%S",
		});



		
		$("#btnMernis").click(function(){
			var tckn=$("#tckimlik").val();
			if(tckn.length!=11){
				alertify.error("T.C. Kimlik 11 Haneli Olmalıdır.");
				return false;
			}
			$("#mernisload").removeClass("d-none");
			$("#dogrulama_content").addClass("d-none");
			$.ajax({
				type	: 'POST',
				url		: 'http://172.30.10.253/tools/WebServis.php',
				data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
				success : function(r){
							$("#mernis_content").html(r);
							if($("#mernis_sonuc").val()=="1"){

								$("#dt").val($("#mernis_dt").html());
								$("#adi").val($("#mernis_adi").html());
								$("#soyadi").val($("#mernis_soyadi").html());
								var c=$("#mernis_cins").html();

								if(c=="Erkek"){
									$("#fk_cinsiyet option[value=1]").attr("selected",true);
								}else{
									$("#fk_cinsiyet option[value=2]").attr("selected",true);
								}
								$("#mernisload").addClass("d-none");
							}
							$("#mernisload").addClass("d-none");

				}
			});
		});	



		<c:if test="${kuyrukBilgisiGoster==true}">
		setTimeout(function(){
			$.ajax({
				type 	: 'POST',
				url		: '/bildirim/kuyrukBilgisi',
				data	: "",
				success : function(e){
					$("#statu_div").removeClass("d-none");
					$("#statu_div").html(e);
		           
				}
			});

		}, 4000);		
		</c:if>


		$(".btn_karaliste").click(function(){

			$("#mdl_bildirim .modal-title").html("Numarayı Kara Listeye Ekle");

			var txtKaraListeNo = document.createElement("input");
			txtKaraListeNo.setAttribute("id","txtKaraListeNo");
			txtKaraListeNo.setAttribute("type","text");
			txtKaraListeNo.classList.add("form-control");
			txtKaraListeNo.setAttribute("readonly",true);
			txtKaraListeNo.setAttribute("disabled",true);
			txtKaraListeNo.setAttribute("style","margin-bottom:7px");
			txtKaraListeNo.setAttribute("value",'${cid}')
			$("#mdl_bildirim .modal-body").append(txtKaraListeNo);
			
			var txtAciklama = document.createElement("textarea");
			txtAciklama.setAttribute("id","txtKaraListeAciklama");
			txtAciklama.classList.add("form-control");
			txtAciklama.setAttribute("placeholder","Kara Liste Sebebi");
			txtAciklama.setAttribute("style","min-height: 150px; text-transform: uppercase;");
			$("#mdl_bildirim .modal-body").append(txtAciklama);


			var modalSaveButton=document.createElement("button");
			modalSaveButton.setAttribute("id","btnKaraListeSave");
			modalSaveButton.classList.add("btn");
			modalSaveButton.classList.add("btn-primary");
			modalSaveButton.innerHTML = "Kaydet";
			
			$("#mdl_bildirim .modal-footer").append(modalSaveButton)
			
			 $("#mdl_bildirim").modal({
				backdrop:"static",
				keyboard:false
			});
			
			modalSaveButton.addEventListener( 'click', function(){	
				var ack=$("#txtKaraListeAciklama").val();
				var numarasi=$("#txtKaraListeNo").val();		
				if(ack==""){
					alertify.error("Kara Listeye Ekleme Sebebini Açıklama Alanına Yazınız.");
					return false;
					$("#txtKaraListeAciklama").focus();
				}else{
					$("#btn_karaliste").attr("disabled",true);
				$.ajax({
					type 	: 'POST',
					url		: '/bildirim/karaListe',
					dataType:"json",
					data	: 'numara='+numarasi+'&aciklama='+ack,
					success : function(r){
						if(r.sonuc=="1"){
						//location.href = '/bildirim';
							alert("ok");
						}else{
							alert("hata");
						}
					}
				});
				}
			} );

		});


		$('#mdl_bildirim').on('hidden.bs.modal', function () {
		   $("#mdl_bildirim .modal-body").html('');
		   var el = document.getElementById('btnKaraListeSave');
		   el.remove();
		   
		})
		
		
		$('.BildirimFormDBBTN').click(function(e){

           
			var kullanici_id=$("#kullanici_id").val();
			var kod=$(this).attr("data-kod");
			var isOnayBekle=$("#isOnayBekle option:selected").val();
			var adi=$("#adi").val();
			var soyadi=$("#soyadi").val();
			var tckimlik=$("#tckimlik").val();
			var fk_ulke=$("#ulkeler option:selected").val();
			var fk_il=$("#iller option:selected").val();
			var fk_ilce=$("#ilceler option:selected").val();
			var sskno=$("#sskno").val();
			var dt=$("#dt").val();
			var email=$("#email").val();
			var tel1=$("#tel1").val();
			var fk_cinsiyet=$("#fk_cinsiyet option:selected").val();
			var gsm=$("#gsm").val();
			var fax=$("#fax").val();
			var fk_egitimdurumu=$("#fk_egitimdurumu option:selected").val();
			var fk_geri_donus_kanali=$("#fk_geri_donus_kanali option:selected").val();
			var adres=$("#adres").val();
			var fk_gsoru=$("#fk_gsoru option:selected").val();
			var gsoru_cevap=$("#gsoru_cevap").val();
			var arayan_numara=$("#arayan_numara").val();
			var arama_kanali_tipi=$("#arama_kanali_tipi option:selected").val();
			var fk_aramakanali=$("#fk_aramakanali option:selected").val();
			var fk_aramasebebi=$("#fk_aramasebebi option:selected").val();
			var tip_level1=$("#tip_level1 option:selected").val();
			var tip_level2=$("#tip_level2 option:selected").val();
			var tip_level3=$("#tip_level3 option:selected").val();
			var aciklama=$("#aciklama").val();
			var tckimlikl=tckimlik.length;
			var uid=$("#uid").val();
			var cid=$("#cid").val();
			var fk_kart=$("#fk_kart").val();
			var firma_bilgi=$("#firma_bilgi").val();
			var kart_update=$("#kart_update option:selected").val();

			//$(".form_islemler").addClass("d-none");


			if(isOnayBekle==""){
				alertify.error("Vatandaş Bilgi Gizliliği Boş Bırakılamaz");
				$("#isOnayBekle").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(adi==""){
				alertify.error("Vatandaş Adı Boş Bırakılamaz");
				$("#adi").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(soyadi==""){
				alertify.error("Vatandaş Soyadı Boş Bırakılamaz");
				$("#soyadi").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}

			if(tckimlik==""){
				alertify.error("Vatandaş T.C. Kimlik Numarası 11 Haneli Olarak Giriniz");
				$("#tckimlik").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}

			if(dt==""){
				alertify.error("Vatandaş Doğum Tarihi Boş Bırakılamaz");
				$("#dt").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(arayan_numara==""){
				alertify.error("Arayan Numara Boş Bırakılamaz");
				$("#arayan_numara").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(arama_kanali_tipi=="" || arama_kanali_tipi=="0"){
				alertify.error("Arama Kanalı Türü Boş Bırakılamaz");
				$("#arama_kanali_tipi").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(fk_aramakanali=="" || fk_aramakanali=="0"){
				alertify.error("Arama Kanalı Boş Bırakılamaz");
				$("#fk_aramakanali").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(fk_aramasebebi=="" || fk_aramasebebi=="0"){
				alertify.error("Arama Sebebi Boş Bırakılamaz");
				$("#fk_aramasebebi").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(tip_level1=="" || tip_level1=="0"){
				alertify.error("Kurum Boş Bırakılamaz");
				$("#tip_level1").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(tip_level2=="" || tip_level2=="0"){
				alertify.error("Alt Kurum Boş Bırakılamaz");
				$("#tip_level2").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}
			if(tip_level3=="" || tip_level3=="0"){
				alertify.error("Arama Konusu Boş Bırakılamaz");
				$("#tip_level3").focus();
				$(".form_islemler").removeClass("d-none");
				return false;
			}			


			var data="adi="+adi+"&soyadi="+soyadi+"&tckimlik="+tckimlik+"&fk_ulke="+fk_ulke+"&fk_il="+fk_il+"&fk_ilce="+fk_ilce+"&sskno="+sskno+"&dt="+dt+"&email="+email+"&tel1="+tel1+"&fk_cinsiyet="+fk_cinsiyet;
			data =data+"&gsm="+gsm+"&fax="+fax+"&fk_egitimdurumu="+fk_egitimdurumu+"&fk_geri_donus_kanali="+fk_geri_donus_kanali+"&adres="+adres+"&fk_gsoru="+fk_gsoru;
			data =data+"&gsoru_cevap="+gsoru_cevap+"&arayan_numara="+arayan_numara+"&arama_kanali_tipi="+arama_kanali_tipi+"&fk_aramakanali="+fk_aramakanali;
			data =data+"&fk_aramasebebi="+fk_aramasebebi+"&tip_level1="+tip_level1+"&tip_level2="+tip_level2+"&tip_level3="+tip_level3+"&aciklama="+aciklama;
			data =data+"&fk_kart="+fk_kart+"&uid="+uid+"&kod="+kod+"&firma_bilgi="+firma_bilgi+"&kart_update="+kart_update+"&isOnayBekle="+isOnayBekle+"&kullanici_id="+kullanici_id;

/*
 * eğer şikayet seçili ise firma bilgileri formu girişi yapılır
 */
			if($("#fk_aramasebebi option:selected").val()=="13"){
				var f_adi=encodeURIComponent($("#f_adi").val());
				var fk_proje=$("#fk_proje option:selected").val();
				var f_adres=encodeURIComponent($("#f_adres").val());
				var f_tel=$("#f_tel").val();
				var f_fk_il=$("#f_fk_il option:selected").val();
				var f_fk_ilce=$("#f_fk_ilce option:selected").val();
				var nace=$("#nace").val();
				var scs=$("#scs").val();

				if(f_adi==""){
					alertify.error("Firma Adı Boş Bırakılamaz");
					$("#f_adi").focus();
					$(".form_islemler").removeClass("d-none");
					return false;
				}
				if(f_adres==""){
					alertify.error("Firma Adresi Boş Bırakılamaz");
					$("#f_adres").focus();
					$(".form_islemler").removeClass("d-none");
					return false;
				}
				if(f_fk_il=="" || $.isNumeric(f_fk_il)==false || f_fk_il=="0"){
					alertify.error("Firma İl Boş Bırakılamaz");
					$("#f_fk_il").focus();
					$(".form_islemler").removeClass("d-none");
					return false;
				}
				if(f_fk_ilce=="" || $.isNumeric(f_fk_ilce)==false || f_fk_ilce=="0"){
					alertify.error("Firma İlçe Boş Bırakılamaz");
					$("#f_fk_ilce").focus();
					$(".form_islemler").removeClass("d-none");
					return false;
				}
				data =data+"&f_adi="+f_adi+"&fk_proje="+fk_proje+"&f_adres="+f_adres+"&f_tel="+f_tel+"&f_fk_il="+f_fk_il+"&f_fk_ilce="+f_fk_ilce+"&nace="+nace+"&scs="+scs;

			}
			
			console.log(data);
            KTApp.block('body', {
                overlayColor: '#000000',
                type: 'v2',
                size: 'lg',
                message:"Bildirim Oluşturuluyor...",
                state:"primary"
            });
            
		   
			$.ajax({
				type 	: 'POST',
				url		: '/bildirim/bildirimKaydet',
				dataType: 'json',
				data	: data,
				success : function(e){
					
					var datakart=e;
			          setTimeout(function() {
			        	  KTApp.unblock("body");
			            }, 1000);
						$("#sonBildirimNo").html(e.sonBildirim);
					
					//location.href = '/bildirim';
				},
			    error: function (request, status, error) {
			        alertify.error("Bildirim Oluşturulurken Sorun Oluştu.");
			        KTApp.unblock("body");
			        $(".form_islemler").removeClass("d-none");
			    }
			});		    
			console.log(e);
		});






	});
	if($(".wsLink").length>0){
		$(".wsLink").click(function(e){
			e.preventDefault();
			var tckn=$("#tckimlik").val();
			if(tckn.length!=11){
				alertify.error("T.C. Kimlik Numarası Hatalı.");
				return false;
			}
			KTApp.block('body', {});
			var datatext=$(this).attr("data-text");
			$("li#webservis a").html(datatext);
			$("li#webservis").removeClass("hidden");
			$("#webservis_sonuc").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
			$(".aktif").removeClass("aktif");
			$('.nav-tabs a[href="#webserviscont"]').tab('show');
			$(this).addClass("aktif");
			
			if($(this).attr("data-ws")=="odemedurumbilgisiemekli"){
				$.ajax({

					type 	: 'POST',
					url		: '//'+location.host+'/bildirim/bildirim/ws-odemebilgiemekli',
					data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
					success : function(e){
						$("#webservis_sonuc").html(e);
						$("#tcemekli").val(tckn);
						//$("#wssonuc").html(e);
						$("#btn_go").click(function(){
							if($("#tc").val()==""){
								alertify.error("Hatalı T.C.");
								return false;
							}
							if($("#ay").val()==""){
							alertify.error("Dönem Ay Bilgisi Girin");
							return false;
						}
							if($("#yil").val()==""){
							alertify.error("Dönem Yıl Bilgisi Girin");
							return false;
						}
							var tckn=$("#tcemekli").val();
							var ay=$("#ay").val();
							var yil=$("#yil").val();

							$("#btn_go").attr("disabled",true);
							$("#sonuc_emekli").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
							$.ajax({
								type	: 'POST',
								url		: 'http://172.30.10.253/tools/odemedurumbilgisiemekli.php',
								data	: "tckn="+tckn+"&ay="+ay+"&yil="+yil+"&fk_agent=${userID}&ip=${ipAdres}",
								success : function(r){
									$("#sonuc_emekli").html(r);
									$("#btn_go").attr("disabled",false);
								}
							});

						});

					}
				});
			} else if($(this).attr("data-ws")=="spas"){

				$.ajax({

					type 	: 'POST',
					url		: '//'+location.host+'/bildirim/bildirim/ws-spas',
					data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
					success : function(e){
						$("#webservis_sonuc").html(e);
						$("#tcemekli").val(tckn);
						//$("#wssonuc").html(e);
						$("#btn_go").click(function(){
							if($("#tc").val()==""){
								alertify.error("Hatalı T.C.");
								return false;
							}
						/*	if($("#spastarih").val()==""){
							alertify.error("Tarih Seçin");
							return false;
						}*/
							var tckn=$("#tcemekli").val();
							var spastarih=$("#spastarih").val();

							$("#btn_go").attr("disabled",true);
							$("#sonuc_saglik").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
							$.ajax({
								type	: 'POST',
								url		: 'http://172.30.10.253/tools/spas.php',
								data	: "tckn="+tckn+"&tarih="+spastarih+"&fk_agent=${userID}&ip=${ipAdres}",
								success : function(r){
									$("#sonuc_saglik").html(r);
									$("#btn_go").attr("disabled",false);
								}
							});

						});

					}
				});
			} else 	if($(this).attr("data-ws")=="kesintibilgisiemekli"){

				$.ajax({

					type 	: 'POST',
					url		: '//'+location.host+'/bildirim/bildirim/ws-kesintiemekli',
					data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
					success : function(e){
						$("#webservis_sonuc").html(e);
						$("#tcemekli").val(tckn);
						//$("#wssonuc").html(e);
						$("#btn_go").click(function(){
							if($("#tc").val()==""){
								alertify.error("Hatalı T.C.");
								return false;
							}
							if($("#ay").val()==""){
							alertify.error("Dönem Ay Bilgisi Girin");
							return false;
						}
							if($("#yil").val()==""){
							alertify.error("Dönem Yıl Bilgisi Girin");
							return false;
						}
							if($("#kisitip").val()==""){
								alertify.error("Emekli türünü seçin");
								return false;
							}
							var tckn=$("#tcemekli").val();
							var ay=$("#ay").val();
							var yil=$("#yil").val();
							var kisitip=$("#kisitip").val();

							$("#btn_go").attr("disabled",true);
							$("#sonuc_kesinti").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
							$.ajax({
								type	: 'POST',
								url		: 'http://172.30.10.253/tools/kesintibilgisiemekli.php',
								data	: "tckn="+tckn+"&ay="+ay+"&yil="+yil+"&kisitip="+kisitip+"&fk_agent=${userID}&ip=${ipAdres}",
								success : function(r){
									$("#sonuc_kesinti").html(r);
									$("#btn_go").attr("disabled",false);
								}
							});

						});

					}
				});
			} else if($(this).attr("data-ws")=="evraksorgulama"){

				$.ajax({

					type 	: 'POST',
					url		: '//'+location.host+'/bildirim/bildirim/ws-evraksorgulama',
					data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
					success : function(e){
						$("#webservis_sonuc").html(e);
						$("#tcevrak").val(tckn);
						//$("#wssonuc").html(e);
						$("#btn_go").click(function(){
							if($("#tcevrak").val()==""){
								alertify.error("Hatalı T.C.");
								return false;
							}

							if($("#varideyil").val()==""){
							alertify.error("Varide / Evrak Yıl Bilgisi Girin");
							return false;
						}
							var tckn=$("#tcevrak").val();
							var varideno=$("#varideno").val();
							var varideyil=$("#varideyil").val();

							$("#btn_go").attr("disabled",true);
							$("#sonuc_evrak").html('<div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show"><span class="vd_alert-icon"><i class="fa fa-exclamation-triangle vd_yellow"></i></span>SORGULAMA YAPILIYOR...</div>');
							$.ajax({
								type	: 'POST',
								url		: 'http://172.30.10.253/tools/EvrakDetaySorgusu.php',
								data	: "tckn="+tckn+"&varideno="+varideno+"&varideyil="+varideyil+"&fk_agent=${userID}&ip=${ipAdres}",
								success : function(r){
									$("#sonuc_evrak").html(r);
									$("#btn_go").attr("disabled",false);
								}
							});

						});

					}
				});
			}else{
				$.ajax({

					type 	: 'POST',
					url		: 'http://172.30.10.253/tools/' +$(this).attr('data-ws')+ '.php',
					data	: "tckn="+$("#tckimlik").val()+"&fk_agent=${userID}&ip=${ipAdres}",
					success : function(e){
						$("#webservis_sonuc").html(e);
						if($("#rapor_sonuc").val()=="1"){

							$.ajax({

								type 	: 'POST',
								url		: 'http://172.30.10.253/tools/odemedurumbilgisi.php',
								data	: "tckn="+$("#tckimlik").val(),
								success : function(e){

									$("#odemesonucrapor").html(e);


								}
							});
						}
						//$("#wssonuc").html(e);

					}
				});
			}		
			KTApp.unblock('body', {});			
			return false;
		});
	}
	
	$(".kt-wizard-v3__nav-item").click(function(){
		return false;
	});

	
</script>

<jsp:include page="footer.jsp" />
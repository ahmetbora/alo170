<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="newline" value="
"/>
<jsp:include page="header.jsp" />
<style>
.alertify-buttons{
	padding:0 !important;
}
.btn{
	margin-bottom: 10px;
	margin-top: 10px;
}
.kt-portlet .kt-portlet__head{
	min-height: 40px !important;
	padding: 0 10px !important;
}

	.m-portlet__head{
		padding:2rem 1.2rem!important;
	}
	.k-grid td {
		padding: .929em 1.286em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
		}
	.k-filter-row th, .k-grid-header th.k-header {
		padding-top: .286em!important;
		padding-bottom: .286em!important;
		padding-right: .286em!important;
		padding-left: .286em!important;
	}   
	.k-grid-toolbar{
		padding-top:8px!important;
		padding-bottom:8px!important;
	}
	.m-portlet__body{
	padding:3px!important;
	}
	.k-grid tr td {
		border-bottom: 1px solid rgb(238, 238, 238);
	}
	.k-grid td {
		padding: .500em .600em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
	}
	.btn-sm, .btn-group-sm > .btn {
		padding: 0.3rem 0.5rem !important;
		font-size: 0.875rem;
		line-height: 1.5;
		border-radius: 0.2rem;
	}
	.td-no-border{
		border:none !important
			}

.kt-timeline-v1 .kt-timeline-v1__items .kt-timeline-v1__item .kt-timeline-v1__item-content{
	padding:1.14rem !important
}			
	</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Bildirim Detay </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Bildirimler </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Bildirim Detay </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar d-none">
				<div class="kt-subheader__wrapper">
					<a href="#" class="btn kt-subheader__btn-primary">
						Actions &nbsp;
	
						<!--<i class="flaticon2-calendar-1"></i>-->
					</a>
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
						<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000" />
								</g>
							</svg>
	
							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
	
							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Add anything or jump to:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Order</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Ticket</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Goal</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support Case</span>
										<span class="kt-nav__link-badge">
											<span class="kt-badge kt-badge--success">5</span>
										</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
									<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
								</li>
							</ul>
	
							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<div class="row">
			<div class="col-md-9">
				<div class="kt-portlet">
					
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								VATANDAŞ BİLGİLERİ
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
							<c:choose>
								<c:when test="${task.isOnayBekle==1}">
									<c:choose>
										<c:when test="${yetki.bildirim_VatandasBilgisiAcKapat=='true'}">
										<a href="javascript:void(0);" class="btn btn-outline-brand btn-bold btn-sm" id="btn_bilgi_ac">
											Bilgileri Göster
										</a>	
										</c:when>
									</c:choose>																
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${yetki.bildirim_VatandasBilgisiAcKapat=='true'}">
										<a href="javascript:void(0);" class="btn btn-outline-brand btn-bold btn-sm" id="btn_bilgi_kapat">
											Bilgileri Kapat
										</a>
										</c:when>
									</c:choose>									
								</c:otherwise>
							</c:choose>	
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div class="row">
							<c:choose>
								<c:when test="${yetki.bildirim_VatandasBilgisiGoster=='true'}">
								 <c:choose>
								<c:when test="${task.isOnayBekle==1}">
									<div class="col-md-12">
										<div class="m-alert m-alert--icon m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												<strong>Dikkat!</strong> Vatandaş bilgilerini görüntülemek için yetkiniz bulunmamaktadır.
											</div>
										</div>	
									</div>						    	
								</c:when>  
								<c:otherwise>
									<div class="col-md-6 col-sm-12">
										<table class="table">
											<tr>
												<td style="width:20%;" class="td-no-border">Adı Soyadı</td>
												<td class="td-no-border"><strong>${kart.kartadi}</strong></td>
											</tr>
											<tr>
												<td>T.C. Kimlik</td>
												<td><strong>${kart.tckimlik}</strong></td>
											</tr>
											<tr>
												<td>Telefon</td>
												<td><strong>${kart.tel1}</strong></td>
											</tr>
											
											<tr>
												<td>GSM</td>
												<td><strong>${kart.gsm}</strong></td>
											</tr>
											
											<tr>
												<td>Ülke / Şehir</td>
												<td><strong>${kart.ulkeadi} ${kart.iladi} ${kart.ilceadi}</strong></td>
											</tr>
											<tr>
												<td>Cinsiyet</td>
												<td><strong>${kart.cinsiyet}</strong></td>
											</tr>							
										</table>
									</div>
									
									<div class="col-md-6 col-sm-12">
									<table class="table">
											<tr>
												<td style="width:20%;" class="td-no-border">EPosta</td>
												<td class="td-no-border"><strong>${kart.email}</strong></td>
											</tr>
											<tr>
												<td>Fax</td>
												<td><strong>${kart.fax}</strong></td>
											</tr>
											<tr>
												<td>Eğitim Durumu</td>
												<td><strong>${kart.edurum}</strong></td>
											</tr>
											
											<tr>
												<td>Doğum Tarihi</td>
												<td><strong>
												
												<c:choose>
													   <c:when test="${kart.dogumtarihi.equals('1900-01-01')}">
															  -
													   </c:when>
													   <c:otherwise>
															  ${kart.dogumtarihi}
													   </c:otherwise>
												  </c:choose>`									  
												</strong>
												</td>
											</tr>
											
											<tr>
												<td>SSK/Bağkur/Emekli Sandık No</td>
												<td> ${kart.getSskNo()}</td>
											</tr>
																		
										</table>
										<br>
										
									</div>	
															    
								</c:otherwise>
								</c:choose>
							   
								</c:when>    
								<c:otherwise>
									<div class="col-md-12">
										<div class="m-alert m-alert--icon m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												<strong>Dikkat!</strong> Vatandaş bilgilerini görüntülemek için yetkiniz bulunmamaktadır.
											</div>
										</div>	
									</div>							        
								</c:otherwise>
							</c:choose>		
						
						</div>
						
					</div>
				</div>		
				<div class="kt-portlet">
					<div class="kt-portlet__head  kt-portlet__head--noborder  kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--dark">
						<div class="kt-ribbon__target" style="top: 12px;">
							<span class="kt-ribbon__inner"></span>${task.statu}
						</div>
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								${task.id} numaralı bildirim
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<div class="col-md-12">
							<h4 style="color:navy;"><c:if test="${task.havuzAdi ne ''}">, <span class="m--font-warning">${task.havuzAdi }</span> havuzunda</c:if>
							</h4>
							<table>
								<tr>
									<td>Bildirim Türü : ${task.konu1} / ${task.konu2} / ${task.konu3}
									<c:choose>
										<c:when test="${yetki.bildirim_kurumAltKurumGuncelle=='true'}">
										 <br>
											<button class="btn btn-xs btn-info p-1" id="btn_kurum_guncelle">
												Kurum / Alt Kurum Bilgileri Güncelle
											</button>															  
										</c:when>
										<c:otherwise>
										</c:otherwise>
									</c:choose>`		
									</td>
								</tr>
								<tr>
									<td>Arama Kanalı Türleri : ${task.aramakanalituru}</td>
								</tr>	
								<tr>
									<td>Arama Kanalı : ${task.aramakanali}</td>
								</tr>		
								<tr>
									<td>Arama Sebebi : ${task.aramasebebi}</td>
								</tr>															
								<tr>
									<td>Şube : ${task.getSubeAdi()}</td>
								</tr>
								<tr>
									<td>Havuz : ${task.getHavuzAdi()}</td>
								</tr>																
							</table>
						</div>
						<hr>
						<div class="col-md-12">
							<span class="text-danger">VT AÇIKLAMASI</span>
							<c:if test="${task.isEnc == 1}"><i class="la la-key"></i></c:if> <br>
							<span class="bild_aciklama">${fn:replace(task.aciklama, newline , '<br>')}</span>
						</div>
					</div>
				</div>		

			
				<div class="kt-portlet">
					<div class="kt-portlet__body p-3">
						<div class="col-md-12">
							<jsp:include page="detayIslemler/islemler/btnSil.jsp" />
							<jsp:include page="detayIslemler/islemler/btnUzerimeAl.jsp" />
							<jsp:include page="detayIslemler/islemler/btnAciklamaEkle.jsp" />
							<c:if test="${task.fkGorevStatu<99}">
								<jsp:include page="detayIslemler/islemler/btnHavuzaYonlendir.jsp" />
							</c:if>
							<jsp:include page="detayIslemler/islemler/btnKullaniciyaYonlendir.jsp" />
							<jsp:include page="detayIslemler/islemler/btnDurumGuncelle.jsp" />
							<jsp:include page="detayIslemler/islemler/btnYenidenAc.jsp" />
							<jsp:include page="detayIslemler/islemler/btnBildirimiKapat.jsp" />
							<jsp:include page="detayIslemler/islemler/btnIptal.jsp" />
						</div>
						
					</div>
				</div>
				<jsp:include page="detayIslemler/islemler/btnSektorGuncelle.jsp" />
		
				
				<c:choose>
				<c:when test="${yetki.bildirim_FirmaBilgisiGor=='true'}">														  				
				<div class="kt-portlet">
					<div class="kt-portlet__head  kt-portlet__head--noborder  kt-ribbon kt-ribbon--clip kt-ribbon--right ">
						<div class="kt-ribbon__target" style="top: 12px;">
							<span class="kt-ribbon__inner"></span>Firma No : ${task.getf_id()}
						</div>
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								FİRMA BİLGİLERİ
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								ss
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div class="row">
							<div class="col-md-6 col-sm-12">
								<table class="table">
									<tr>
										<td style="width:20%;" class="td-no-border">Kurum / Kişi Adı</td>
										<td class="td-no-border">${task.f_adi}</td>
									</tr>
									<tr>
										<td>Telefon </td>
										<td>${task.f_tel}</td>
									</tr>
									<tr>
										<td>Şehir </td>
										<td>${task.f_il}</td>
									</tr>
									
									<tr>
										<td>İlçe </td>
										<td>${task.f_ilce}</td>
									</tr>							
								</table>
							</div>	
							<div class="col-md-6 col-sm-12">
								<table class="table">
									<tr>
										<td style="width:20%;" class="td-no-border">NACE Kodu</td>
										<td class="td-no-border">${task.nace}</td>
									</tr>
									<tr>
										<td>Sigortasız Çalışma Süresi</td>
										<td>${task.scs}</td>
									</tr>
									<tr>
										<td>Adres </td>
										<td>${task.f_adres}</td>
									</tr>							
								</table>
							</div>								
						</div>			
					</div>
				</div>	
				</c:when>
				</c:choose>
					
				<c:if test="${(yetki.bildirim_DetayislemleriGor == 'true') or  (yetki.bildirim_AciklamaGuncellemeleriGor=='true')}">	
				<div class="kt-portlet kt-portlet--tabs">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-toolbar">
							<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
								<c:choose>
								<c:when test="${yetki.bildirim_DetayislemleriGor=='true'}">								
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#islemler" role="tab">
										<i class="la la-comments-o" aria-hidden="true"></i>İşlemler
									</a>
								</li>
								</c:when>
								</c:choose>		
								<c:choose>
								<c:when test="${yetki.bildirim_AciklamaGuncellemeleriGor=='true'}">														
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#aciklamaguncelleme" role="tab">
										<i class="la la-comment" aria-hidden="true"></i>Açıklama Güncellemeleri
									</a>
								</li>
								</c:when>
								</c:choose>									
							</ul>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="tab-content">
							<c:choose>
							<c:when test="${yetki.bildirim_DetayislemleriGor=='true'}">						
							<div class="tab-pane active" id="islemler" role="tabpanel">							
								<div class="kt-timeline-v1 kt-timeline-v1--justified">
									<div class="kt-timeline-v1__items">
										<div class="kt-timeline-v1__marker"></div>
											<c:forEach items="${detay}" var="d" >
											<div class="kt-timeline-v1__item">
												<div class="kt-timeline-v1__item-circle">
													<div class="kt-bg-${d.cls}"></div>
												</div>
												<span class="kt-timeline-v1__item-time kt-font-brand" style="width:100% !important">${d.tarih}</span>
												<div class="kt-timeline-v1__item-content">
													<div class="kt-timeline-v1__item-title">
														${d.agent} / 
														<c:if test="${d.ext ne ''}">${d.ext} -</c:if>
														<c:if test="${d.lokasyon ne ''}">${d.lokasyon} /</c:if>
														${d.rol_adi} / <span class="text-danger"> <c:if test="${d.islemtipi ne ''}">  ${d.islemtipi}</c:if></span>
													</div>
													<div class="kt-timeline-v1__item-body">
														<div class="kt-widget4">
															<div class="kt-widget4__item">
																<div class="kt-widget4__pic">
																	<img src="assets/media/users/100_4.jpg" alt="">
																</div>
																<div class="kt-widget4__info">
																	<p class="kt-widget4__text">
																		${fn:replace(d.aciklama, newline , '<br>')}
																	</p>
																</div>
															
															</div>
														</div>
													</div>
												</div>
											</div>	
											</c:forEach>								
									</div>
								</div>
							</div>
							</c:when>
							</c:choose>		
							<c:choose>
							<c:when test="${yetki.bildirim_AciklamaGuncellemeleriGor=='true'}">													
							<div class="tab-pane" id="aciklamaguncelleme" role="tabpanel">
							<c:if test="${detayGuncelleme.size()>0}">
								<div class="kt-timeline-v1 kt-timeline-v1--justified">
									<div class="kt-timeline-v1__items">
										<div class="kt-timeline-v1__marker"></div>
											<c:forEach items="${detayGuncelleme}" var="d" >
											<div class="kt-timeline-v1__item">
												<div class="kt-timeline-v1__item-circle">
													<div class="kt-bg-${d.cls}"></div>
												</div>
												<span class="kt-timeline-v1__item-time kt-font-brand" style="width:100% !important">${d.tarih}</span>
												<div class="kt-timeline-v1__item-content">
													<div class="kt-timeline-v1__item-title">
														${d.agent} / 
														<c:if test="${d.ext ne ''}">${d.ext} -</c:if>
														<c:if test="${d.lokasyon ne ''}">${d.lokasyon} /</c:if>
														${d.rol_adi} / <span class="text-danger"> <c:if test="${d.islemtipi ne ''}">  ${d.islemtipi}</c:if></span>
													</div>
													<div class="kt-timeline-v1__item-body">
														<div class="kt-widget4">
															<div class="kt-widget4__item">
																<div class="kt-widget4__pic">
																	<img src="assets/media/users/100_4.jpg" alt="">
																</div>
																<div class="kt-widget4__info">
																	<p class="kt-widget4__text">
																		${fn:replace(d.aciklama, newline , '<br>')}
																	</p>
																</div>
															
															</div>
														</div>
													</div>
												</div>
											</div>	
											</c:forEach>								
									</div>
								</div>	
							</c:if>								
							</div>
							</c:when>
							</c:choose>									
						</div>
					</div>
				</div>										
				</c:if>
							
			</div>
			<div class="col-md-3">
				<div class="kt-portlet">
					<div class="kt-portlet__body p-3">
						<h2># ${task.id}</h2>
						<span>Bildirim Konusu</span>
						<h5>${task.konu1} / ${task.konu2}</h5>
						
						<span>Kaydı Oluşturan</span>
						<h5>${task.getKaydiAcanAgent()}</h5>		
						
						<span>Kayıt Oluşturulma Tarihi</span>
						<h5>${task.tarih} ${task.saat}</h5>	
						<br>
						<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
							<div class="m-alert__icon">
								<i class="flaticon-exclamation-1"></i>
								<span></span>
							</div>
							<div class="m-alert__text">
								<strong>KVKK</strong> Kanun gereği bildirimlerin görüntülenme işlemleri loglanmaktadır.
							</div>
						
						</div>							
					</div>
				</div>	
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								CALLCENTER GÖRÜŞMELERİ
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						s
					</div>
				</div>	
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								DOSYALAR
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						s
					</div>
				</div>									
			</div>
		</div>
	</div>

</div>




<!--begin::Modal-->
<div class="modal fade" id="pnlBildirimDetay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" id="modalContent">


		</div>
	</div>
</div>

<!--end::Modal-->

<script>
function AciklamaEkle(){
        $('#modalLabel').html('Aciklama Ekle');
        $('#pnlBildirimDetay').modal('show');
        $('#bildirimdetaybody').html(ret);
}

</script>
<style>
	.bild_aciklama{
		font-size: 14pt !important;
		font-weight: 400 !important;
		line-height: 1.5em !important;
	}

	.m-timeline-3__item-text-baslik{
		font-size: 12pt !important;
		font-weight: 900 !important;
	}

	.m-timeline-3__item-time{
		font-size: 10pt !important;
		font-weight: 900 !important;
	}
</style>

<script>
var xhr=null;
$("#btn_bilgi_kapat").unbind().click(function(){
	var taskId="${task.id}";
	xhr = $.ajax({
		type 	: 'POST',
		dataType: 'json',
		/*********** bu kısım aktif olursa formdata request payload olarak gidiyor *************/
		//contentType: "application/json; charset=utf-8", 
		url		: '/bildirim/vatandasBilgisiKapat',
		data	: "id="+taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			location.reload();
		},
	    error: function (request, status) {
			xhr.abort();
			//console.log(request.responseJSON.message)
	    }
	});	
});
$("#btn_bilgi_ac").unbind().click(function(){
	var taskId="${task.id}";
	if (xhr) {
        xhr.abort();
    }
	xhr = $.ajax({
		type 	: 'POST',
		dataType: 'json',
		/*********** bu kısım aktif olursa formdata request payload olarak gidiyor *************/
		//contentType: "application/json; charset=utf-8", 
		url		: '/bildirim/vatandasBilgisiAc',
		data	: "id="+taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			location.reload();
		},
	    error: function (request, status) {
			xhr.abort();
			console.log(request.responseJSON.message)
	    }
	});	
});

$("#btn_kurum_guncelle").unbind().click(function(){
	var taskId="${task.id}";
		if (xhr) {
			xhr.abort();
		}
		xhr = $.ajax({
		type 	: 'POST',
		/*********** bu kısım aktif olursa formdata request payload olarak gidiyor *************/
		//contentType: "application/json; charset=utf-8", 
		url		: '/bildirim/kurumAltKurumGuncelle',
		data	: "id="+taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlBildirimDetay').modal({
				keyboard: false,
				backdrop:"static"
			})
					
		},
	    error: function (request, status) {
			xhr.abort();
           	alertify.error(request.responseText,function(ex){
                   
			});			
	    }
	});
});

$("#btn_sil").unbind().click(function(){
	alertify.set({ labels: {
				ok     : "Tamam",
				cancel : "Hayır"
			} });	
	alertify.alert("Bilgi Güvenliği Nedeni ile Veri Silme İşlemi Devredışı Bırakılmıştır");	
});
$("#btn_uzerime_al").unbind().click(function(){
	alertify.set({ labels: {
		ok     : "Evet",
		cancel : "Hayır"
	} });	
	alertify.confirm("Bildirimi Üzerinize Almak İstiyormusunuz?", function (e) {
    if (e) {
        uzerimeAl();
    } 
	});
	
});
$("#btn_yonlendir").unbind().click(function(){
	var taskId="${task.id}";
	if (xhr) {
		xhr.abort();
	}
	$.ajax({
		type	: 'POST',
		url		: "bildirim/havuzaYonlendir",
		data	: 'id=' + taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlBildirimDetay').modal({
				keyboard: false,
				backdrop:"static"
			})
		},
		error: function (request, status) {
			xhr.abort();
			console.log(request.responseJSON.message)
		}
	
	});
});
$("#btn_yonlendir_user").unbind().click(function(){
	var taskId="${task.id}";
	if (xhr) {
		xhr.abort();
	}
	$.ajax({
		type	: 'POST',
		url		: "bildirim/kullaniciyaYonlendir",
		data	: 'id=' + taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlBildirimDetay').modal({
				keyboard: false,
				backdrop:"static"
			})
		},
		error: function (request, status) {
			xhr.abort();
			console.log(request.responseJSON.message)
		}
	
	});
});
$("#btn_iptal").unbind().click(function(){
	alert("btn_iptal");
});
$("#btn_yeniden_ac").unbind().click(function(){
	var taskId="${task.id}";
	if (xhr) {
		xhr.abort();
	}
	$.ajax({
		type	: 'POST',
		url		: "bildirim/yenidenAc",
		data	: 'id=' + taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlBildirimDetay').modal({
				keyboard: false,
				backdrop:"static"
			})
		},
		error: function (request, status) {
			xhr.abort();
			console.log(request.responseJSON.message)
		}
	
	});
});
$("#btn_guncelle").unbind().click(function(){
	var taskId="${task.id}";
	$.ajax({
		type	: 'POST',
		url		: "bildirim/guncelle",
		data	: 'id=' + taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlBildirimDetay').modal({
				keyboard: false,
				backdrop:"static"
			})
		},
		error: function (request, status) {
			xhr.abort();
			console.log(request.responseJSON.message)
		}
	
	});
});

$("#btn_kapat").unbind().click(function(){
	var taskId="${task.id}";
	$.ajax({
		type	: 'POST',
		url		: "bildirim/kapat",
		data	: 'id=' + taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlBildirimDetay').modal({
				keyboard: false,
				backdrop:"static"
			})
		},
		error: function (request, status) {
			//xhr.abort();
			$('#modalContent').html(request.responseText);	
			$('#pnlBildirimDetay').modal({
				keyboard: false,
				backdrop:"static"
			})
		}
	
	});
});

$("#btn_aciklamaEkle").unbind().click(function(){
aciklamaEkle();
});

function aciklamaEkle(){
	var taskId="${task.id}";
	if (xhr) {
		xhr.abort();
	}
	$.ajax({
		type	: 'POST',
		url		: "bildirim/aciklamaEkle",
		data	: 'id=' + taskId,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},		
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlBildirimDetay').modal({
				keyboard: false,
				backdrop:"static"
			})
		},
		error: function (request, status) {
			xhr.abort();
			console.log(request.responseJSON.message)
		}
	
	});
}
function uzerimeAl(){
	var taskId="${task.id}";
		if (xhr) {
			xhr.abort();
		}
		alertify.confirm("Bildirimi üstünüze almak istediğinize eminmisiniz?", function (e) {
		    if (e) {
				$.ajax({
					type	: 'POST',
					url		: 'bildirim/uzerimeAl',
					data	: 'ids=' + taskId+",",
					dataType: 'json',
					beforeSend: function(req) {
						req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
					},	
					success : function(e){
						alertify.success("İşlem Başarılı");
						location.reload();
					},
					error: function (request, status) {
						xhr.abort();
						console.log(request.responseJSON.message)
					}
				});
		    }
		});	
}

</script>
<jsp:include page="footer.jsp" />
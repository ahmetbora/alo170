<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${(yetki.bildirim_DurumGuncelle == 'true') and (task.fk_agent == userID) and (task.fkGorevStatu<99)}">
    <a href="javascript:void(0);" class="btn btn-info btn-sm" id="btn_guncelle"><i class="fa fa-edit"></i> Bildirim Durumunu Güncelle</a>&nbsp;
</c:if>

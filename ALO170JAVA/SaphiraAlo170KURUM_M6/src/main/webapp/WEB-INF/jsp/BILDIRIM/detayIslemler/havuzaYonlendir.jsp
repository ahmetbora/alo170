<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach items="${css}" var="c">
<link href="${c}" rel="stylesheet">
</c:forEach>
<c:forEach items="${js}" var="j">
<script src="${j}" type="text/javascript"></script>
</c:forEach>
<div class="modal-header" id="moda_header">
    <h5 class="modal-title" id="modalLabel">Havuza Yönlendir</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body" id="modal_body">
    <form>
        <div class="form-group row">
            <div class="col-md-12">
                <label for="recipient-name" class="form-control-label">Kurum:</label>
		   		<select id="tip_level1" name="tip_level1" placeholder="Kurum" class="form-control zorunlu">
		   			<option value="">Seçiniz</option>
					<c:forEach items="${kurumlar}" var="n">					
						<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
					</c:forEach>									   			
		   		</select>                
            </div>
		</div>
		<div class="form-group row">	
            <div class="col-md-12">
                <label for="message-text" class="form-control-label">Şehir:</label>
			   	<select class="form-control zorunlu" name="fk_il" id="fk_il" placeholder="Şehir">
			   			<option value="">--Seçiniz--</option>
						<c:forEach items="${iller}" var="n">					
							<option data-id="${n.adi}" value="${n.id}">${n.adi}</option>
						</c:forEach>							   
			   	</select>	                
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label for="recipient-name" class="form-control-label">Şube:</label>
		   		<select id="fk_sube" name="fk_sube" placeholder="Şube" class="form-control zorunlu">
		   			<option value="">Seçiniz</option>								   			
		   		</select>                
            </div>
		</div>	
		<div class="form-group row">
            <div class="col-md-12">
                <label for="message-text" class="form-control-label">Havuzlar:</label>
			   	<select class="form-control zorunlu" name="fk_havuz" id="fk_havuz" placeholder="Havuzlar">
			   			<option value="">--Seçiniz--</option>
			   	</select>	                
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label for="message-text" class="form-control-label">Açıklama:</label>
				<textarea class="form-control" id="txtAciklama" placeholder="Açıklama" rows="5"></textarea>
            </div>
        </div>


    </form>
</div>
<div class="modal-footer" id="modal_footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
    <button type="button" class="btn btn-primary" id="btn_modal_kaydet">Yönlendir</button>
</div>

<script>
	var xhr=null;
alertify.set({ labels: {
    ok     : "Tamam",
    cancel : "Vazgeç"
} });
$('#pnlBildirimDetay').on('hidden.bs.modal', function (e) {
  $("#modalContent").html('');
  $(this).off('hidden.bs.modal');
})

function getSubeler(){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	$.ajax({
		type: "POST",
		url: '/bildirim/getSubeler',
		data:
				{
					tip_level1: $('#tip_level1 option:selected').val(),
					fk_il: $('#fk_il option:selected').val()
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_sube').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}

function getHavuzlar(){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	$.ajax({
		type: "POST",
		url: '/bildirim/getHavuzlar',
		data:
				{
					fk_sube: $('#fk_sube option:selected').val()
					
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_havuz').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});			
}
$('#fk_il').change(function(){
	getSubeler();
});
$('#tip_level1').change(function(){
	getSubeler();
});
$('#fk_sube').change(function(){
	getHavuzlar();
});


$("#btn_modal_kaydet").unbind().click(function(){
	var t1=$("#tip_level1 option:selected").val();
	var fk_il=$("#fk_il option:selected").val();
	var fk_sube=$("#fk_sube option:selected").val();
	var fk_havuz=$("#fk_havuz option:selected").val();
	var aciklama=$("#txtAciklama").val();
    var id="${id}";
    var token = '${_csrf.token}';
    var header ='${_csrf.headerName}';

	if(t1==""){
		alertify.error("Kurum Seçin");
		return false;
	}
	if(fk_il==""){
		alertify.error("Şehir Seçin");
		return false;
	}
	if(fk_sube==""){
		alertify.error("Şube Seçin");
		return false;
	}
	if(fk_havuz==""){
		alertify.error("Havuz Seçin");
		return false;
	}
			
	xhr=$.ajax({
		type	: 'POST',
		url		: '/bildirim/havuzaYonlendirKaydet',
        data:
                {
                    t1              : t1,
                    fk_il           : fk_il,
                    fk_sube         : fk_sube,
                    id              : id,
                    fk_havuz  		: fk_havuz,
					aciklama		: aciklama
                   
                },        
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        },
		success : function(r){
            $('#pnlBildirimDetay').modal("hide");
           	alertify.alert("Bildirim Yönlendirildi",function(ex){
                   location.reload();
			});            
		},
        error: function (request, status, error) {
            xhr.abort();
			alertify.alert(request.responseJSON.mesaj) 
        }        
	});	
});
</script>
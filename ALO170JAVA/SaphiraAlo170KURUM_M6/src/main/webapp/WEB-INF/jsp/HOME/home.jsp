<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

    
<jsp:include page="../BILDIRIM/header.jsp" />
<div class="m-content">

	<div class="m-portlet  m-portlet--unair">
		<div class="m-portlet__body  m-portlet__body--no-padding">
			<div class="row m-row--no-padding m-row--col-separator-xl">
				<div class="col-md-12 col-lg-6 col-xl-3">
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								A��lan Bildirim
							</h4><br>
							<span class="m-widget24__desc">
								---
							</span>
							<span class="m-widget24__stats m--font-brand" id="acilanbild">
								${home.bugun.acilan }
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-brand" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								
							</span>
							<span class="m-widget24__number">
								
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">
	
					<!--begin::New Feedbacks-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								Agent taraf�ndan ��z�len
							</h4><br>
							<span class="m-widget24__desc">
								---
							</span>
							<span class="m-widget24__stats m--font-info" id="agentkapanan">
								${home.bugun.agent_kapanan }
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								Change
							</span>
							<span class="m-widget24__number">
								84%
							</span>
						</div>
					</div>
	
					<!--end::New Feedbacks-->
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">
	
					<!--begin::New Orders-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								Kapanan Bildirim
							</h4><br>
							<span class="m-widget24__desc">
								---
							</span>
							<span class="m-widget24__stats m--font-danger" id="kapananbild">
								${home.bugun.task_kapanan }
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								 
							</span>
							<span class="m-widget24__number">
								 
							</span>
						</div>
					</div>
	
					<!--end::New Orders-->
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">
	
					<!--begin::New Users-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								Gelen �a�r�
							</h4><br>
							<span class="m-widget24__desc">
								---
							</span>
							<span class="m-widget24__stats m--font-success" id="gelencagri">
								
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								Change
							</span>
							<span class="m-widget24__number">
								90%
							</span>
						</div>
					</div>
	
					<!--end::New Users-->
				</div>
			</div>
		</div>
	</div>


<div class="m-portlet">
							<div class="m-portlet__body  m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">
									<div class="col-xl-4">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
														<h3 class="m-widget1__title">��lem G�ren</h3>
														<span class="m-widget1__desc">Bildirim Say�s�</span>
													</div>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
														<fmt:formatNumber type = "number" maxIntegerDigits="9" value = "${home.acik }" />
														</span>
													</div>
												</div>
											</div>
																					
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
														<h3 class="m-widget1__title">Toplam Kay�t</h3>
														<span class="m-widget1__desc">Bildirim Say�s�</span>
													</div>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
															<fmt:formatNumber type = "number" maxIntegerDigits="9" value = "${home.bildirim }" /> 
														</span>
													</div>
												</div>
											</div>

											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
														<h3 class="m-widget1__title">Toplam A��klama</h3>
														<span class="m-widget1__desc">Bildirime Eklenen A��klama</span>
													</div>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
														<fmt:formatNumber type = "number" maxIntegerDigits="9" value = "${home.detay }" />
														</span>
													</div>
												</div>
											</div>
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-4">

										<!--begin:: Widgets/Daily Sales-->
										<div class="m-widget14">
											<div class="m-widget14__header m--margin-bottom-30">
												<h3 class="m-widget14__title">
													Bugun Saat Baz�nda
												</h3>
												<span class="m-widget14__desc">
													A��lan Bildirimler Say�s�
												</span>
											</div>
											<div class="m-widget14__chart" style="height:120px;">
												<canvas id="gr_bugun_task"></canvas>
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-4">

										<!--begin:: Widgets/Profit Share-->
										<div class="m-widget14">
											<div class="m-widget14__header">
												<h3 class="m-widget14__title">
													Profit Share
												</h3>
												<span class="m-widget14__desc">
													Profit Share between customers
												</span>
											</div>
											<div class="row  align-items-center">
												<div class="col">
													<div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
														<div class="m-widget14__stat">45</div>
													</div>
												</div>
												<div class="col">
													<div class="m-widget14__legends">
														<div class="m-widget14__legend">
															<span class="m-widget14__legend-bullet m--bg-accent"></span>
															<span class="m-widget14__legend-text">37% Sport Tickets</span>
														</div>
														<div class="m-widget14__legend">
															<span class="m-widget14__legend-bullet m--bg-warning"></span>
															<span class="m-widget14__legend-text">47% Events</span>
														</div>
														<div class="m-widget14__legend">
															<span class="m-widget14__legend-bullet m--bg-brand"></span>
															<span class="m-widget14__legend-text">19% Others</span>
														</div>
													</div>
												</div>
											</div>
										</div>

										<!--end:: Widgets/Profit Share-->
									</div>
								</div>
							</div>
						</div>







	<div class="row">
		<div class="col-xl-4">
		<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit  m-portlet--unair" style="min-height: 300px">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													Inbound Bandwidth
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
													<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
														Today
													</a>
													<div class="m-dropdown__wrapper">
														<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36.5px;"></span>
														<div class="m-dropdown__inner">
															<div class="m-dropdown__body">
																<div class="m-dropdown__content">
																	<ul class="m-nav">
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-share"></i>
																				<span class="m-nav__link-text">Activity</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-chat-1"></i>
																				<span class="m-nav__link-text">Messages</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-info"></i>
																				<span class="m-nav__link-text">FAQ</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																				<span class="m-nav__link-text">Support</span>
																			</a>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">

										<!--begin::Widget5-->
										<div class="m-widget20">
											<div class="m-widget20__number m--font-success">670</div>
											<div class="m-widget20__chart" style="height:160px;">											
												<canvas id="m_chart_bandwidth1"></canvas>
											</div>
										</div>

										<!--end::Widget 5-->
									</div>
								</div>
		</div>
		
		<div class="col-xl-4">
<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit  m-portlet--unair" style="min-height: 300px">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													Outbound Bandwidth
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
													<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
														Today
													</a>
													<div class="m-dropdown__wrapper">
														<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36.5px;"></span>
														<div class="m-dropdown__inner">
															<div class="m-dropdown__body">
																<div class="m-dropdown__content">
																	<ul class="m-nav">
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-share"></i>
																				<span class="m-nav__link-text">Activity</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-chat-1"></i>
																				<span class="m-nav__link-text">Messages</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-info"></i>
																				<span class="m-nav__link-text">FAQ</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																				<span class="m-nav__link-text">Support</span>
																			</a>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">

										<!--begin::Widget5-->
										<div class="m-widget20">
											<div class="m-widget20__number m--font-warning">340</div>
											<div class="m-widget20__chart" style="height:160px;">
												<canvas id="m_chart_bandwidth2"></canvas>
											</div>
										</div>

										<!--end::Widget 5-->
									</div>
								</div>		
		</div>
		
		<div class="col-xl-4">
			<div class="m-portlet m-portlet--full-height m-portlet--fit  m-portlet--unair">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													Top Products
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
													<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
														All
													</a>
													<div class="m-dropdown__wrapper" style="z-index: 101;">
														<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 36.5px;"></span>
														<div class="m-dropdown__inner">
															<div class="m-dropdown__body">
																<div class="m-dropdown__content">
																	<ul class="m-nav">
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-share"></i>
																				<span class="m-nav__link-text">Activity</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-chat-1"></i>
																				<span class="m-nav__link-text">Messages</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-info"></i>
																				<span class="m-nav__link-text">FAQ</span>
																			</a>
																		</li>
																		<li class="m-nav__item">
																			<a href="" class="m-nav__link">
																				<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																				<span class="m-nav__link-text">Support</span>
																			</a>
																		</li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">

										<!--begin::Widget5-->
										<div class="m-widget4 m-widget4--chart-bottom">

											<div class="m-widget4__chart m-portlet-fit--sides m--margin-top-20" style="height:160px;">
												<canvas id="m_chart_trends_stats_2"></canvas>
											</div>
										</div>

										<!--end::Widget 5-->
									</div>
								</div>		
		</div>
						
	</div>




</div>		



		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>

<script>
	$(document).ready(function() {
		wallboard();
	});

	function wallboard(){
		 
            var e = $("#gr_bugun_task");
            if (0 != e.length) {
                var t = {
                    labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16","17" ,"18","19","20","21","22","23"],
                    datasets: [{
                        backgroundColor: mApp.getColor("info"),
                        data: [${home.bugun.o0}, ${home.bugun.o1}, ${home.bugun.o2}, ${home.bugun.o3}, ${home.bugun.o4}, ${home.bugun.o5}, ${home.bugun.o6}, ${home.bugun.o7}, ${home.bugun.o8}, ${home.bugun.o9}, ${home.bugun.o10}, ${home.bugun.o11}, ${home.bugun.o12}, ${home.bugun.o13}, ${home.bugun.o14}, ${home.bugun.o15}, ${home.bugun.o16},${home.bugun.o17},${home.bugun.o18},${home.bugun.o19},${home.bugun.o20},${home.bugun.o21},${home.bugun.o22},${home.bugun.o23}]
                    }]
                };
                new Chart(e,{
                    type: "bar",
                    data: t,
                    options: {
                        title: {
                            display: !1
                        },
                        tooltips: {
                            intersect: !1,
                            mode: "nearest",
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        },
                        legend: {
                            display: !1
                        },
                        responsive: !0,
                        maintainAspectRatio: !1,
                        barRadius: 4,
                        scales: {
                            xAxes: [{
                                display: !1,
                                gridLines: !1,
                                stacked: !0
                            }],
                            yAxes: [{
                                display: !1,
                                stacked: !0,
                                gridLines: !1
                            }]
                        },
                        layout: {
                            padding: {
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0
                            }
                        }
                    }
                })
            } 
        
	}
	
function encAciklama() {
	$.get('/home/encTaskDBAciklama',function(data){
		encAciklama();
	});
}	

function encDetay() {
	$.get('/home/encTaskDBDetay',function(data){
		encDetay();
	});
}
	
</script> 				
<jsp:include page="../BILDIRIM/footer.jsp" />
						
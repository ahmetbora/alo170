<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>

<jsp:include page="BILDIRIM/header.jsp" />

<div class="m-content">

	<div class="m-portlet  m-portlet--unair">
		<div class="m-portlet__body  m-portlet__body--no-padding">
			<div class="row m-row--no-padding m-row--col-separator-xl">
				<div class="col-md-12 col-lg-6 col-xl-3">
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								Bildirim Tablosu
							</h4><br>
							<span class="m-widget24__desc">
								---
							</span><br><br>
							<span class="m-widget24__stats m--font-brand" id="adetBildirim">
								
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								52.407.597
							</span>
							<span class="m-widget24__number">
								84%
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">
	
					<!--begin::New Feedbacks-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								Bildirim Detay Tablosu
							</h4><br>
							<span class="m-widget24__desc">
								---
							</span><br><br>
							<span class="m-widget24__stats m--font-info" id="adetBildirimDetay">
								
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								93.257.079
							</span>
							<span class="m-widget24__number">
								84%
							</span>
						</div>
					</div>
	
					<!--end::New Feedbacks-->
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">
	
					<!--begin::New Orders-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								Vatanda� Tablosu
							</h4><br>
							<span class="m-widget24__desc">
								---
							</span><br><br>
							<span class="m-widget24__stats m--font-danger" id="adetVatandas">
								
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								13.983.401
							</span>
							<span class="m-widget24__number">
								69%
							</span>
						</div>
					</div>
	
					<!--end::New Orders-->
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">
	
					<!--begin::New Users-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								Gelen �a�r�
							</h4><br>
							<span class="m-widget24__desc">
								---
							</span><br><br>
							<span class="m-widget24__stats m--font-success" >
								100
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								Change
							</span>
							<span class="m-widget24__number">
								100%
							</span>
						</div>
					</div>
	
					<!--end::New Users-->
				</div>
			</div>
		</div>
	</div>

</div>		



		

<script>
	$(document).ready(function() {
		 
		dataAl();
		dataVatandas();
	});

	function bildirimAdet(){
		$.getJSON('/task/transferHomeB',function(data){
			$('#adetBildirim').html(data.id);
		});
	}
	function dataAl(){
		bildirimAdet();
		bildirimDetayAdet();
		vatandasAdet();
		
		$.get('/task/transfer',function(){
			 
			dataAl();
		});
	}

	function dataVatandas(){
			
		$.get('/task/transferVatandas',function(){
			 
			dataVatandas();
		});
	}
	
	
	function bildirimDetayAdet(){
		$.getJSON('/task/transferHomeBD',function(data){
			$('#adetBildirimDetay').html(data.id);
		});
	}
	function vatandasAdet(){
		$.getJSON('/task/transferHomeV',function(data){
			$('#adetVatandas').html(data.id);
		});
	}
	
</script> 	

<jsp:include page="BILDIRIM/footer.jsp" />
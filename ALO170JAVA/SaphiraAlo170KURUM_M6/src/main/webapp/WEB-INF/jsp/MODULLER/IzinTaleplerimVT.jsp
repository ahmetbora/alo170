<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../BILDIRIM/header.jsp" />

<style>

	.m-portlet__head{
		padding:2rem 1.2rem!important;
	}
	.k-grid td {
		padding: .929em 1.286em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
		}
	.k-filter-row th, .k-grid-header th.k-header {
		padding-top: .286em!important;
		padding-bottom: .286em!important;
		padding-right: .286em!important;
		padding-left: .286em!important;
	}   
	.k-grid-toolbar{
		padding-top:8px!important;
		padding-bottom:8px!important;
	}
	.m-portlet__body{
	padding:3px!important;
	}
	.k-grid tr td {
		border-bottom: 1px solid rgb(238, 238, 238);
	}
	.k-grid td {
		padding: .500em .600em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
	}
	.btn-sm, .btn-group-sm > .btn {
		padding: 0.3rem 0.5rem !important;
		font-size: 0.875rem;
		line-height: 1.5;
		border-radius: 0.2rem;
	}
	</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle} VT </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Personel İşlemleri </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
	<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-elevate alert-light alert-bold" role="alert">
					<div class="alert-text kt-font-info">
						<i class="fa fa-exclamation-circle"></i> <strong>İZİN TALEPLERİM</strong> Bu Ekranda Kendi Adınıza Ve Diğer Personel Adına Yapmış Olduğunuz İzin Talepleri Bulunur.	
						
					</div>
				</div>			
			</div>
			
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__body p-3">
						<select id="isActive" class="form-control">
							<option value="1" >Onaylanmış Taleplerim</option>
							<option value="2"  >Onaylanmamış Taleplerim</option>
							<option value="0" >Onay Bekleyen Taleplerim</option>
						</select>	
						<br>	
						<div id=kullaniciGrid></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>	

<script type="text/x-kendo-template" id="svOnayColTemplate">
	#if(sv_onay=='1'){#
		<span class="label label-sm label-success">Onaylandı</span>
	#}else if(sv_onay=='0'){#
		<span class="label label-sm label-info">Onay Bekliyor</span>	
	#}else if(sv_onay=='2'){#
		<span class="label label-sm label-danger">Reddedildi</span>
	#}#	
</script>
<script type="text/x-kendo-template" id="adOnayColTemplate">
	#if(admin_onay=='1'){#
		<span class="label label-sm label-success">Onaylandı</span>
	#}else if(admin_onay=='0'){#
		<span class="label label-sm label-info">Onay Bekliyor</span>	
	#}else if(admin_onay=='2'){#
		<span class="label label-sm label-danger">Reddedildi</span>
	#}#	
</script>
<script type="text/x-kendo-template" id="sonucColTemplate">
	#if(isActive=='1'){#
		<span class="label label-sm label-success">Onaylandı</span>
	#}else if(isActive=='0'){#
		<span class="label label-sm label-info">Onay Bekliyor</span>	
	#}else if(isActive=='2'){#
		<span class="label label-sm label-danger">Reddedildi</span>
	#}#	
</script>
<script type="text/x-kendo-template" id="sdateColTemplate">
	#:starih# #:ssaat#
</script>
<script type="text/x-kendo-template" id="fdateColTemplate">
	#:ftarih# #:fsaat#
</script>
<script>
var crudServiceBaseUrl="/modul/izin-taleplerimVT";
kullaniciGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, 	nullable: true },
                "user"            	:   { editable: false, 	nullable: true },
                "sdate"          	:   { editable:false,	nullable: true },
                "ssaat"      		:   { editable:false,	nullable: true },
                "fdate"      		:   { editable:false,	nullable: true },
                "fsaat"      		:   { editable:false,	nullable: true },
                "sebep"      		:   { editable:false,	nullable: true },
                "aciklama"      	:   { editable:false,	nullable: true },
                "sv_onay"      		:   { editable:false,	nullable: true },
                "admin_onay"      	:   { editable:false,	nullable: true },
                "isActive"      	:   { editable:false,	nullable: true },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": false,
    "autoSync": false,
    "serverPaging": true,

});

function kullaniciGridDsFilter(){
	var isActive=$("#isActive option:selected").val();
	kullaniciGridDataSource.filter([
            {
                field: "isActive",
                operator: "eq",
                value: isActive
            }          
        ]
    );

}

var kullaniciGrid = $("#kullaniciGrid").kendoGrid({
    "columns": [
        {
            field: "user",
            filterable: false, groupable:false,editable:false, "title": "AGENT"
        },    
        {
            filterable: false, groupable:false,editable:false,  "title": "Başlama Tarihi",
			template:kendo.template($("#sdateColTemplate").html())
        },   
        {
            filterable: false, groupable:false,editable:false,  "title": "Bitiş Tarihi",
			template:kendo.template($("#fdateColTemplate").html())
        },                         
        {
            field: "sebep",
            filterable: false, groupable:true,editable:false,  "title": "İzin Nedeni"
        },                         
        {
            field: "aciklama",
            filterable: false, groupable:false,editable:false,  "title": "Açıklama"
        },                         
        {
            filterable: false, groupable:false,editable:false,  "title": "Supervisor Onay",
			template:kendo.template($("#svOnayColTemplate").html())
        },                         
        {
            filterable: false, groupable:false,editable:false,  "title": "Admin Onay",
			template:kendo.template($("#adOnayColTemplate").html())
        },                         
        {
            filterable: false, groupable:false,editable:false,  "title": "Sonuç",
			template:kendo.template($("#sonucColTemplate").html())
        },                         
    ],
    "autoBind": true,
    "dataSource":kullaniciGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": true,
    "navigatable": false,
    "editable": false,

    "pageable": {"alwaysVisible": true, "pageSize": 20,"responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [20,30,40,50]},
    "height":800,
 
});
kullaniciGridDsFilter();
$("#isActive").change(function(){
	kullaniciGridDsFilter();	
})
</script>
<jsp:include page="../BILDIRIM/footer.jsp" />
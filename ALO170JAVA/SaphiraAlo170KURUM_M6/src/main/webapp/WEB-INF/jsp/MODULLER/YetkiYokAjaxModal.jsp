<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <div class="modal-header" id="moda_header">
        <h5 class="modal-title" id="modalLabel">HATA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
    </div>
    <div class="modal-body" id="modal_body">
        <div class="alert alert-danger" role="alert">
            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
            <div class="alert-text">${msj}</div>
        </div>
    </div>
    <div class="modal-footer" id="modal_footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Kapat</button>
    </div>
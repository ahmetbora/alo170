<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../BILDIRIM/header.jsp" />
 
 <style>
	 #bildirimGrid td{
		 font-weight: 400 !important;
	 }
 </style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle} </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Modüller </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar">
				<div class="kt-subheader__wrapper">
					<a href="/modul/sss-ekle" class="btn btn-sm btn-brand" >
						Yeni SSS 
					</a>
					<a href="/modul/sss" class="btn btn-sm btn-brand" >
						SSS Liste 
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">

			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${pageTitle}
							</h3>
						</div>
			
					</div>
					<div class="kt-portlet__body">	
						<form class="form-horizontal" role="form">	
							<c:if test="${yetki.yetki_bilgibankasisv=='true'}">						
							<div class="form-group">
								<label class="control-label">Onay Durumu</label>
								<div class="controls">
									<select id="statu" name="statu" class="form-control">
										<option value="0" <c:if test="${sss.isActive==0}"> selected</c:if>>İçerik Pasif
										</option>
										<option value="1" <c:if test="${sss.isActive==1}"> selected</c:if>>İçerik Aktif</option>
									</select>
								</div>
							</div>
							</c:if>	
							<c:if test="${yetki.yetki_bilgibankasisv=='true'}">	
							<div class="form-group">
								<label class="control-label">Web Durumu</label>
								<div class=" controls">
									<select id="web" name="web" class="form-control">
										<option value="0" <c:if test="${sss.isWeb==0}"> selected</c:if>>Webde Yayınlanmasın
										</option>
										<option value="1" <c:if test="${sss.isWeb==1}"> selected</c:if>>Webde Yayınlansın</option>
									</select>
								</div>
							</div>
							</c:if>	
							<div class="form-group">
								<label class="control-label">Konu</label>
								<div class="controls">
									<select id="fk_sss_grup" class="form-control">
										<option value="0" selected="">SEÇİNİZ</option>
										<c:forEach items="${konu}" var="n">
											<option data-ext="${n.id}" value="${n.id}" <c:if test="${sss.fk_sss_grup==n.id}"> selected</c:if>>${n.adi}</option>
										</c:forEach>
									</select>
								</div>
							</div>																	
							<div class="form-group">
								<label class="control-label">Alt Konu</label>
								<div class="controls">
									<select id="fk_sss_altgrup" class="form-control">
										<option value="0">SEÇİNİZ</option>
				                    	<c:forEach items="${altkonu}" var="n">
		                    			<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    			</c:forEach>
									</select>
								</div>
							</div>												
							<div class="form-group">
								<label class="control-label">Başlık</label>
								<div class="controls">
									<input type="text" name="baslik" id="baslik" class="form-control" value="${sss.baslik}">
								</div>
							</div>							
							<div class="form-group">
								<label class="control-label">Detay</label>
								<div class=" controls">
									<textarea name="detay" id="detay" class="form-control" rows="20" cols="30" style="width:100%;" aria-label="detay">${sss.aciklama}</textarea>
								</div>
							</div>
						</form>
																									
						<div class="form-group m-form__group">						
							<button class="btn btn-brand btn-sm float-right" id="btnSave">
								<i class="fa fa-save"></i> 
								Kaydet
							</button>
						</div>	
									
					</div>
				</div>				
			</div>

		</div>
	</div>

</div>


<script>

var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;

var token = $("input[name='_csrf']").val();
var header = "X-CSRF-TOKEN";
$(document).ajaxSend(function(e, xhr, options) {
	xhr.setRequestHeader(header, _csrf_token);
});

$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});
$("#fk_sss_grup").change(function(){
        var fk_sss_grup=$("#fk_sss_grup option:selected").val();
        $.ajax({
            type	: 'POST',
            url		: '/lookup/getSssAltKonu',
			data:
			{
				fk_sss_grup: fk_sss_grup
			},
			beforeSend: function (xhr) {
				xhr.setRequestHeader(header, token);
			},
            success : function(r){
                $("#fk_sss_altgrup").html(r);
				$("#fk_sss_altgrup option[value=${sss.fk_sss_altgrup}]").attr("selected","selected");
            }
        });
    });


$("#fk_sss_grup").change();

$("#btnSave").click(function(){
	<c:if test="${yetki.yetki_bilgibankasisv=='true'}">
        var statu=$("#statu option:selected").val();
		var web=$("#web option:selected").val();
	</c:if>

	<c:if test="${yetki.yetki_bilgibankasisv!='true'}">
        var statu=0;
		var web=0;
	</c:if>
        var fk_sss_grup=$("#fk_sss_grup option:selected").val();
        var fk_sss_altgrup=$("#fk_sss_altgrup option:selected").val();
        var baslik=encodeURIComponent($("#baslik").val());
        var aciklama=encodeURIComponent($("#detay").val());
        if(fk_sss_grup=="0"){
            alertify.error("Konu Seçiniz");
            return false;
        }
        if(fk_sss_altgrup=="0"){
            alertify.error("Altkonu Seçiniz");
            return false;
        }
        if(baslik==""){
            alertify.error("Başlık Giriniz");
            return false;
        }
        if(aciklama==""){
            alertify.error("Detay Giriniz");
            return false;
        }
        $.ajax({
            type	: 'POST',
            url		: '/modul/sss-guncelle',
			data:
			{
				statu: statu,
				fk_sss_grup: fk_sss_grup,
				fk_sss_altgrup: fk_sss_altgrup,
				baslik: baslik,
				aciklama: aciklama,
				web: web
			},
			beforeSend: function (xhr) {
				xhr.setRequestHeader(header, token);
			},
            success : function(r){
           
				alertify.alert("İçerik Kaydedildi");
                location.reload();
            }
        });
    });
</script>

<script>
var editor = $("#detay").kendoEditor({
tools: [
	"bold",
	"italic",
	"underline",
	"fontSize",
	//"foreColor",
	//"backColor",
]
});
</script>
<jsp:include page="../BILDIRIM/footer.jsp" />
 
 
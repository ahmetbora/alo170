<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../BILDIRIM/header.jsp" />
 
 <style>
	 #bildirimGrid td{
		 font-weight: 400 !important;
	 }
 </style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle} </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Modüller </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar">
				<div class="kt-subheader__wrapper">
					<a href="/modul/sss-ekle" class="btn btn-sm btn-brand" >
						Yeni SSS 
					</a>

				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<!-- rapor grid start-->
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${pageTitle}
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
                        <table class="table table-striped table-bordered table-advance table-hover gtreetable gtreetable-fullAccess" id="gtreetable">
                            <thead>
                                <tr>
                                    <th>TELEFON</th>
                                    <th>EKLEYEN</th>
                                    <th>TARIH</th>
                                    <th>SON TARIH</th>
                                    <th width="60%">NOT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>${item.numara}</td>
                                    <td>${item.ekleyen}</td>
                                    <td>${item.ilktarih}</td>
                                    <td>
                                        <c:if test="${item.isSuresiz=='0'}">${item.sontarih}</c:if>
                                        <c:if test="${item.isSuresiz=='1'}">-- SÜRESİZ --</c:if>
                                    </td>
                                    <td>${item.neden}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td>
                                        <input type="text" id="sure" name="sure" style="width: 100%">
                                    </td>
                                    
                                    <td>
                                        <button class="btn btn-brand btn-sm btn_onayla" data-id="${item.id}">Onayla</button>
                                        <button class="btn btn-danger btn-sm btn_sonayla" data-id="${item.id}">Süresiz Onayla</button>
                                        <button class="btn btn-info btn-sm btn_sil" data-id="${item.id}">Sil</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>


<script>

var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;

var token = $("input[name='_csrf']").val();
var header = "X-CSRF-TOKEN";
$(document).ajaxSend(function(e, xhr, options) {
	xhr.setRequestHeader(header, _csrf_token);
});

$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});
$("#sure").kendoDatePicker({
	format:'yyyy-MM-dd',
	culture:"tr-TR",
});

$(".btn_onayla").click(function(){
	var tarih=$("#sure").val();
	var id=$(this).attr("data-id");
	if(tarih==""){
		alertify.error("Tarih Seçimi Yapınız");
		$("#sure").focus();
		return false;
	}else{
		$.ajax({
			type	: 'POST',
			url		: '/modul/karaliste-onay',
        
			dataType: 'json',
            data:
			{
				id    	    : id,
				tarih       : tarih
			},
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            },
			success : function(r){
				alertify.success("Onaylama Başarılı");
				location.reload();
			}
		});		
	}
})

$(".btn_sonayla").click(function(){
	var id=$(this).attr("data-id");
		$.ajax({
			type	: 'POST',
			url		: '/modul/karaliste-suresizonay',
          
			dataType: 'json',
            data:
			{
				id    	    : id
			},
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            },
			success : function(r){
				alertify.success("Onaylama Başarılı");
				location.reload();
			}
		});		
})

$(".btn_sil").click(function(){
	var id=$(this).attr("data-id");
		$.ajax({
			type	: 'POST',
			url		: '/modul/karaliste-sil',
         
			dataType: 'json',
            data:
			{
				id    	    : id
			},
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            },
			success : function(r){
				alertify.success("İşlem Başarılı");
				location.href="/modul/karaliste"
			}
		});		
})

</script>

<jsp:include page="../BILDIRIM/footer.jsp" />
 
 
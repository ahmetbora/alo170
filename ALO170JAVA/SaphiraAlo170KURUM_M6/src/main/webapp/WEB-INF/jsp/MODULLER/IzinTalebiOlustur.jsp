<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../BILDIRIM/header.jsp" />

<style>

	.m-portlet__head{
		padding:2rem 1.2rem!important;
	}
	.k-grid td {
		padding: .929em 1.286em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
		}
	.k-filter-row th, .k-grid-header th.k-header {
		padding-top: .286em!important;
		padding-bottom: .286em!important;
		padding-right: .286em!important;
		padding-left: .286em!important;
	}   
	.k-grid-toolbar{
		padding-top:8px!important;
		padding-bottom:8px!important;
	}
	.m-portlet__body{
	padding:3px!important;
	}
	.k-grid tr td {
		border-bottom: 1px solid rgb(238, 238, 238);
	}
	.k-grid td {
		padding: .500em .600em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
	}
	.btn-sm, .btn-group-sm > .btn {
		padding: 0.3rem 0.5rem !important;
		font-size: 0.875rem;
		line-height: 1.5;
		border-radius: 0.2rem;
	}
	</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle}</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Personel İşlemleri </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
	<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
							</span>
							<h3 class="kt-portlet__head-title">
								İZİN TALEBİ OLUŞTUR
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<form class="kt-form kt-form--label-right">
							<div class="form-group row">
								<label class="control-label col-md-3">İzin Tarihi</label>
								<div class="col-md-5">
									<div class="input-group">
										<input id="sdate"  style="width: 100%" />
									</div>					
								</div>
							</div>
							<div class="form-group row">
								<label class="control-label col-md-3">İzin Saati</label>
								<div class="col-md-5">
									<div class="input-group"> 
										<input id="ssaat" style="width: 100%;" value="00:00" />
									</div>					
								</div>
							</div>
							<div class="form-group row">
								<label class="control-label col-md-3">Bitiş Tarihi</label>
								<div class="col-md-5">
									<div class="input-group">
										<input id="fdate"  style="width: 100%" />
									</div>					
								</div>
							</div>
							<div class="form-group row">
								<label class="control-label col-md-3">Bitiş Saati</label>
								<div class="col-md-5">
									<div class="input-group">
										<input id="fsaat" style="width: 100%;" value="00:00" />
									</div>					
								</div>
							</div>
							<div class="form-group row">
								<label class="control-label col-md-3">İzin Nedeni</label>
								<div class="col-md-5">
									<div class="input-group">
										<select class="form-control" id="fk_sebep">
											<c:forEach items="${vardiya}" var="n">
												<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
											</c:forEach>
										</select>
									</div>					
								</div>
							</div>
							<div class="form-group row">
								<label class="control-label col-md-3">İzin Tarihi</label>
								<div class="col-md-5">
									<div class="input-group">
										<textarea class="form-control" rows="5" id="aciklama"></textarea>
									</div>					
								</div>
							</div>
						</form>
					</div>
					<div class="kt-portlet__foot p-2">
						<div class="kt-form__actions p-2 float-right">
							<button type="button" class="btn btn-primary" id="btnSave"><i class="fa fa-save"></i> Kaydet</button>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>	
<script>
var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;
$(function () {
    var token = $("input[name='_csrf']").val();
    var header = "X-CSRF-TOKEN";
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, _csrf_token);
    });
});
$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});

$("#ssaat").kendoTimePicker({
	dateInput: true,
	format:"HH:mm"
});
$("#fsaat").kendoTimePicker({
	dateInput: true,
	format:"HH:mm"
});
$("#sdate").kendoDatePicker({
	format:'yyyy-MM-dd',
	culture:"tr-TR",
});
$("#fdate").kendoDatePicker({
	format:'yyyy-MM-dd',
	culture:"tr-TR",
});
$("#sdate").data("kendoDatePicker").value("${sdate}");
$("#fdate").data("kendoDatePicker").value("${fdate}");

	$("#btnSave").click(function(){
		var sdate=$("#sdate").val();
		var ssaat=$("#ssaat").val();
		var fdate=$("#fdate").val();
		var fsaat=$("#fsaat").val();	
		var fk_sebep=$("#fk_sebep option:selected").val();
		var fk_agent="${userId}";
		var aciklama=$("#aciklama").val();
		if(sdate==""){
			alertify.error("İzin Tarihi Seçin");
			return false;
		}
		if(fdate==""){
			alertify.error("Bitiş Tarihi Seçin");
			return false;
		}	
	
		if(fk_sebep==""){
			alertify.error("İzin Nedenini Seçin");
			return false;
		}		
		if(fk_sebep=="0"){
			alertify.error("İzin Nedenini Seçin");
			return false;
		}					
		$.ajax({
			type	: 'POST',
			url		: '/modul/izin-talep-kaydet',
			data	: 'sdate='+sdate+'&ssaat='+ssaat+'&fdate='+fdate+'&fsaat='+fsaat+'&fk_sebep='+fk_sebep+'&aciklama='+encodeURIComponent(aciklama)+'&fk_agent='+fk_agent,
			beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            },
			success : function(e){
				alertify.success("İzin Talebi Oluşturuldu.");
				
				setTimeout(function(){ location.href="/modul/izin-taleplerim" }, 2000);
			}
		});	
	});

	</script>
<jsp:include page="../BILDIRIM/footer.jsp" />
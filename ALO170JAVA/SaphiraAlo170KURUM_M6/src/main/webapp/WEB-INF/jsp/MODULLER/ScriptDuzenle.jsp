<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../BILDIRIM/header.jsp" />

<style>

	.m-portlet__head{
		padding:2rem 1.2rem!important;
	}
	.k-grid td {
		padding: .929em 1.286em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
		}
	.k-filter-row th, .k-grid-header th.k-header {
		padding-top: .286em!important;
		padding-bottom: .286em!important;
		padding-right: .286em!important;
		padding-left: .286em!important;
	}   
	.k-grid-toolbar{
		padding-top:8px!important;
		padding-bottom:8px!important;
	}
	.m-portlet__body{
	padding:3px!important;
	}
	.k-grid tr td {
		border-bottom: 1px solid rgb(238, 238, 238);
	}
	.k-grid td {
		padding: .500em .600em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
	}
	.btn-sm, .btn-group-sm > .btn {
		padding: 0.3rem 0.5rem !important;
		font-size: 0.875rem;
		line-height: 1.5;
		border-radius: 0.2rem;
	}
	</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle}</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
	<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
							</span>
							<h3 class="kt-portlet__head-title">
								${pageTitle}
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<form class="form-horizontal" role="form" method="post" id="frm">		
							<div class="form-group">
								<label>Script Konusu:</label>
								<select class="form-control zorunlu" name="fk_parent" id="fk_parent" placeholder="Arama Sebebi">
									<option value="0">Script Konusu</option>
									<c:forEach items="${konu}" var="n">
										<option value="${n.id}" <c:if test="${n.id eq item.fk_parent}"> selected </c:if>>${n.adi}</option>
									</c:forEach>
								</select>
								
							</div>
							<div class="form-group">
								<label>Arama Sebebi:</label>
								<select class="form-control zorunlu" name="fk_aramasebebi" id="fk_aramasebebi">
									<option value="0">Arama Sebebi</option>
									<c:forEach items="${fk_aramasebebi}" var="n">
										<option value="${n.id}"  <c:if test="${n.id eq item.fk_aramasebebi}"> selected </c:if>>${n.adi}</option>
									</c:forEach>
								</select>
								
							</div>
							<div class="form-group">
								<label>Kurum:</label>
								<select class="form-control zorunlu" name="tip_level1" id="tip_level1" >
									<option value="0">Kurum</option>
									<c:forEach items="${kurum}" var="n">
										<option value="${n.id}"  <c:if test="${n.id eq item.tip_level1}"> selected </c:if>>${n.adi}</option>
									</c:forEach>
								</select>
								
							</div>
							<div class="form-group">
								<label>Alt Kurum:</label>
								<select class="form-control zorunlu" name="tip_level2" id="tip_level2">
									<option value="">--Seçiniz--</option>
								</select>
								
							</div>
							<div class="form-group">
								<label>Arama Konusu:</label>
								<select class="form-control zorunlu" name="tip_level3" id="tip_level3" >
									<option value="">--Seçiniz--</option>
								</select>
								
							</div>
							<div class="form-group">
								<label>Script İçeriği:</label>
								<textarea id="icerik" rows="7" class="form-control" name="icerik">${item.script}</textarea>
								
							</div>
							<div class="form-group">
								<label>Açıklama:</label>
								<textarea id="aciklama" rows="4" class="form-control" name="aciklama">${item.aciklama}</textarea>
								
							</div>
							<input type="hidden" name="id" id="id" value="${item.id}">
						</form>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions float-right">
							<button type="button" class="btn btn-primary" id="btn_kaydet">Kaydet</button>
							<a type="button" class="btn btn-secondary" href="/modul/script-liste">Vazgeç</a>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>	

<script>
$('#tip_level1').change(function(){
	var val=$("#tip_level1 option:selected").val();
		var data=
			{
			"tbl": "task_alt_konu",
			"selectedId":"${item.tip_level2}",
			"fields":[
				{
				"keyfiled":"fk_parent",
				"keyval":val
				}
			]
			}
	$.ajax({
		type	: 'POST',

		url		: '/lookup/getCmbDetayWhere',
		data	:JSON.stringify(data),
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(r){
			$('#tip_level2').html(r);
			$('#tip_level2').change();
		}
	});
});
$('#tip_level2').change(function(){
	var val=$("#tip_level2 option:selected").val();
		var data=
			{
			"tbl": "task_alt_konu_detay",
			"selectedId":"${item.tip_level3}",
			"fields":[
				{
				"keyfiled":"fk_parent",
				"keyval":val
				}
			]
			}
	$.ajax({
		type	: 'POST',
		url		: '/lookup/getCmbDetayWhere',
		data	:JSON.stringify(data),
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(r){
			$('#tip_level3').html(r);
		}
	});
});

			
$('#tip_level1').change();

$("#btn_kaydet").click(function(){
	var frm=$("#frm").serialize();
	console.log(frm);
	//return false;
	var fk_parent=$("#fk_parent option:selected").val();
	var fk_aramasebebi=$("#fk_aramasebebi option:selected").val();
	var tip_level1=$("#tip_level1 option:selected").val();
	var tip_level2=$("#tip_level2 option:selected").val();
	var tip_level3=$("#tip_level3 option:selected").val();
	var icerik=$("#icerik").val();
	var aciklama=$("#aciklama").val();
	if(fk_parent=="" || fk_parent=="0"){
		alertify.error("Script Konusu Seçin");
		return false;
	}		
	if(fk_aramasebebi=="" || fk_aramasebebi=="0"){
		alertify.error("Arama Sebebi Seçin");
		return false;
	}			
	if(tip_level1=="" || tip_level1=="0"){
		alertify.error("Kurum Seçin");
		return false;
	}		
	
	$.ajax({
		type	: 'POST',
		url		: '/modul/bildirim-script-kaydet',
		data	:frm,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(r){
			alertify.success("Script Güncellendi");
		}
	});
})

</script>
<jsp:include page="../BILDIRIM/footer.jsp" />
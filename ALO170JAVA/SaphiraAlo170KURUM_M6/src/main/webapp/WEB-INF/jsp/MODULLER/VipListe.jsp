<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../BILDIRIM/header.jsp" />
 
 <style>
	 #bildirimGrid td{
		 font-weight: 400 !important;
	 }
 </style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle} </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Modüller </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar">
				<div class="kt-subheader__wrapper">
					<a href="/modul/vipliste-ekle" class="btn btn-sm btn-brand d-none" >
						Yeni SSS 
					</a>

				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<!-- rapor grid start-->
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${pageTitle}
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div id="bildirimGrid"></div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>

<script type="text/x-kendo-template" id="islemColTemplate">
	<button class="btn btn-danger btn-sm " data-id="#=id#" onclick="sil(#=id#)"><i class="fa fa-times"></i> Sil</button>
</script>

<script>

var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;

var token = $("input[name='_csrf']").val();
var header = "X-CSRF-TOKEN";
$(document).ajaxSend(function(e, xhr, options) {
	xhr.setRequestHeader(header, _csrf_token);
});

$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});


var crudServiceBaseUrl="/modul/vipliste";
bildirimGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            		:   { editable: false, nullable: true },
                "numara"            	:   { editable: false, nullable: true },
                "info"            	    :   { editable: false, nullable: true },
                "agent"            	    :   { editable: false, nullable: true },
                "tarih"            	    :   { editable: false, nullable: true },

            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});



var bildirimGrid = $("#bildirimGrid").kendoGrid({
    "columns": [
        {
            "field": "numara",
            "filterable": false, "groupable": false, "title": "TELEFON"
        },
        {
            "field": "info",
            "filterable": false, "groupable": false, "title": "ADI"
        },
        {
            "field": "agent",
            "filterable": false, "groupable": false, "title": "EKLEYEN"
        },
        {
            "field": "tarih",
            "filterable": false, "groupable": false, "title": "TARIH"
        },

        {
            "filterable": false, "groupable": false,width:85,
			template:kendo.template($("#islemColTemplate").html())
        },
           
    ],
    "autoBind": true,
    "dataSource":bildirimGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": false,
    "navigatable": false,
    "editable": "false",
    "pageable": {"alwaysVisible": true, "responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [10,20,30,50]},
    "height":800

});


    function sil(id){
        alertify.confirm("Silmek İstediğinize Eminmisiniz?", function (e) {
        if (e) {
            $.ajax({
            type	: 'POST',
            url		: '/modul/vipliste-sil',
			data:
			{
				id: id,
			},
			beforeSend: function (xhr) {
				xhr.setRequestHeader(header, token);
			},
            success : function(r){
                location.reload();
            }
        });
        }
    });
    }
   

</script>

<jsp:include page="../BILDIRIM/footer.jsp" />
 
 
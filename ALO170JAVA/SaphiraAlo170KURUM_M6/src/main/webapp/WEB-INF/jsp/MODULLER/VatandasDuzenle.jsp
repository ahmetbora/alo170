<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../BILDIRIM/header.jsp" />
 
 <style>
	 .form-group {
    margin-bottom: 1rem !important;
	 }
 </style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle}</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Vatandaş İşlemleri </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar d-none">
				<div class="kt-subheader__wrapper">
					<a href="#" class="btn kt-subheader__btn-primary">
						Actions &nbsp;
	
						<!--<i class="flaticon2-calendar-1"></i>-->
					</a>
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
						<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000" />
								</g>
							</svg>
	
							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
	
							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Add anything or jump to:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Order</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Ticket</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Goal</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support Case</span>
										<span class="kt-nav__link-badge">
											<span class="kt-badge kt-badge--success">5</span>
										</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
									<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
								</li>
							</ul>
	
							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${pageTitle}
							</h3>
						</div>
				
					</div>
					<div class="kt-portlet__body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class=" control-label">Adı</label>
									<div class=" controls">
										<input type="text" class="form-control zorunlu" name="adi" id="adi"  autocomplete="off" placeholder="Adi" value="${v.adi}">
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">Soyadı</label>
									<div class=" controls">
										<input type="text" class="form-control zorunlu" name="soyadi" id="soyadi" autocomplete="off" placeholder="Soyadı"  value="${v.soyadi}">
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">T.C. Kimlik No</label>
									<div class=" controls">
										<input type="text" class="form-control zorunlu" name="tckimlik" id="tckimlik"  autocomplete="off" placeholder="T.C. Kimlik No"  value="${v.tckimlik}">
									</div>
									
								</div>
								<div class="form-group">
									<label class=" control-label">Doğum Tarihi</label>
									<div class=" controls">
										<input type="text" class=" zorunlu" name="dt" id="dt"  autocomplete="off" placeholder="Doğum Tarihi"  value="${v.dt}" style="width: 100%;">
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">Telefon</label>
									<div class=" controls">
										<input type="text" class="form-control zorunlu phone" name="tel" id="tel"  autocomplete="off" placeholder="Telefon"  value="${v.tel1}">
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">GSM</label>
									<div class=" controls">
										<input type="text" class="form-control phone" name="gsm" id="gsm"  autocomplete="off" placeholder="GSM"  value="${v.gsm}">
									</div>
								</div>
								<div class="form-group fk_egitim_durumu">
									<label class=" control-label">Eğitim Durumu</label>
									<div class=" controls">
									   	<select class="form-control" name="fk_egitimdurumu" id="fk_egitimdurumu">
											<c:forEach items="${ed}" var="n">
												<option data-ext="${n.id}" value="${n.id}" <c:if test="${v.fk_egitim_durumu==n.id}"> selected</c:if>>${n.adi}</option>
											</c:forEach>
									   	</select>				
									</div>
								</div>													
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label class=" control-label">Ülke</label>
									<div class=" controls">
									   	<select class="form-control" name="fk_ulke" id="fk_ulke">
											<c:forEach items="${ulke}" var="n">
												<option data-ext="${n.id}" value="${n.id}" <c:if test="${v.fk_ulke==n.id}"> selected</c:if>>${n.adi}</option>
											</c:forEach>
									   	</select>
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">İl</label>
									<div class=" controls">
									   	<select class="form-control" name="fk_il" id="fk_il">

									   	</select>
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">İlçe</label>
									<div class=" controls">
									   	<select class="form-control" name="fk_ilce" id="fk_ilce">

									   	</select>
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">SSK/Bağkur/Emekli Sandık No</label>
									<div class=" controls">
										<input type="text" class="form-control " name="sskno" id="sskno"  autocomplete="off" placeholder="SSK/Bağkur/Emekli Sandık No"   value="${v.sskno}">
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">E-Posta</label>
									<div class=" controls">
										<input type="text" class="form-control " name="email" id="email"  autocomplete="off" placeholder="E-Posta"  value="${v.email}">
									</div>
								</div>
								<div class="form-group">
									<label class=" control-label">Cinsiyet</label>
									<div class=" controls">
									   	<select class="form-control" name="fk_cinsiyet" id="fk_cinsiyet">
											<c:forEach items="${cinsiyet}" var="n">
												<option data-ext="${n.id}" value="${n.id}" <c:if test="${v.fk_cinsiyet==n.id}"> selected</c:if>>${n.adi}</option>
											</c:forEach>
									   	</select>	
									</div>
								</div>
								<div class="form-group faks">
									<label class=" control-label">Faks</label>
									<div class=" controls">
										<input type="text" class="form-control phone" name="fax" id="fax"  autocomplete="off" placeholder="Faks" value="${v.fax}">
									</div>
								</div>	
							</div>
						</div>					
					</div>
					<div class="kt-portlet__foot">
						<button class="btn btn-danger btn-sm float-left" id="btnTemizle">
							<i class="fa fa-times"></i> 
							Temizle
						</button>						
						<button class="btn btn-brand btn-sm float-right" id="btnSorgula">
							<i class="fa fa-search"></i> 
							Kaydet
						</button>
					</div>
				</div>				
			</div>	

		</div>
	</div>

</div>

<script>

$("#dt").kendoDatePicker({
	format:'yyyy-MM-dd',
	culture:"tr-TR",
});


function getIller(fk_ulke,resume){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	
	$.ajax({
		type: "POST",
		url: '/lookup/getIller',
		data:
				{
					fk_ulke: fk_ulke
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_il').html(jqXHR.responseText);			
			if(resume=="1"){
				$("#fk_il option[value=${v.fk_il}]").attr("selected","selected");
				var fk_il=$("#fk_il option:selected").val();
				getIlceler(fk_il);
			}		
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}

function getIlceler(fk_il){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	
	$.ajax({
		type: "POST",
		url: '/lookup/getIlceler',
		data:
				{
					fk_il: fk_il
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_ilce').html(jqXHR.responseText);		
			$("#fk_ilce option[value=${v.fk_ilce}]").attr("selected","selected");		
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}

$("#fk_ulke").change(function(){
	var fk_ulke=$("#fk_ulke option:selected").val();
	getIller(fk_ulke,1);
});
$("#fk_il").change(function(){
	var fk_il=$("#fk_il option:selected").val();
	getIlceler(fk_il);
});

 $("#fk_ulke").select2();
 $("#fk_il").select2();
 $("#fk_ilce").select2();

$("#btnTemizle").click(function(){
location.reload();
});
$("#mnuExport a").click(function(){
	var type=$(this).attr("export-type");
	alert(type)
});

$("#fk_ulke").change();
$("#btnSorgula").click(function(){
	var adi=$("#adi").val();
	var soyadi=$("#soyadi").val();
	var tckimklik=$("#tckimlik").val();
	var dt=$("#dt").val();
	var tel=$("#tel").val();
	var gsm=$("#gsm").val();
	var fk_egitim_durumu=$("#fk_egitimdurumu option:selected").val();
	var fk_ulke=$("#fk_ulke option:selected").val();
	var fk_il=$("#fk_il option:selected").val();
	var fk_ilce=$("#fk_ilce option:selected").val();
	var ssk=$("#sskno").val();
	var email=$("#email").val();
	var fk_cinsiyet=$("#fk_cinsiyet option:selected").val();
	var fax=$("#fax").val();
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	var id="${v.id}";
	if(adi==""){
		alertify.alert("Vatandaş Adı Girin.");
		return false;
	}
	if(soyadi==""){
		alertify.alert("Vatandaş Soyadı Girin.");
		return false;
	}
	if(tckimklik==""){
		alertify.alert("Vatandaş T.C. Kimlik Girin.");
		return false;
	}
	if(dt==""){
		alertify.alert("Vatandaş Doğum Tarihi Girin.");
		return false;
	}
	if(tel==""){
		alertify.alert("Vatandaş Telefon Girin.");
		return false;
	}
	$.ajax({
		type: "POST",
		url: '/modul/vatandas-guncelle',
		data:
				{
					adi:adi,
					soyadi:soyadi,
					tckimklik:tckimklik,
					dt:dt,
					tel:tel,
					gsm:gsm,
					fk_egitim_durumu:fk_egitim_durumu,
					fk_ulke:fk_ulke,
					fk_il:fk_il,
					fk_ilce:fk_ilce,
					ssk:ssk,
					email:email,
					fk_cinsiyet:fk_cinsiyet,
					fax:fax,
					id:id
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			alertify.alert("Vatandaş Bilgisi Güncellendi");
		},
		error: function (request, status, error) {
			alertify.alert("İşlem Sırasında Sorun Oluştu");
		}
	});

});

</script>

<jsp:include page="../BILDIRIM/footer.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../BILDIRIM/header.jsp" />
 
 <style>
	 .form-group {
    margin-bottom: 1rem !important;
	 }
 </style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle}</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Vatandaş İşlemleri </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar d-none">
				<div class="kt-subheader__wrapper">
					<a href="#" class="btn kt-subheader__btn-primary">
						Actions &nbsp;
	
						<!--<i class="flaticon2-calendar-1"></i>-->
					</a>
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
						<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000" />
								</g>
							</svg>
	
							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
	
							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Add anything or jump to:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Order</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Ticket</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Goal</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support Case</span>
										<span class="kt-nav__link-badge">
											<span class="kt-badge kt-badge--success">5</span>
										</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
									<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
								</li>
							</ul>
	
							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								Filtre Ayarları
							</h3>
						</div>
				
					</div>
					<div class="kt-portlet__body">
						<div class="form-group m-form__group">
							<label for="statu">Statü</label>
					    	<select class="form-control" name="kayit_tipi" id="kayit_tipi">
					    		<option value="10">Onay Bekleyen</option>
					    		<option value="20" >Mevcut Üye</option>
					    		<option value="30">Hesabı Kapatılanlar</option>
					    		<option value="1" >Aramış Müşteriler</option>
					    	</select>
					    </div> 	
		        		<div class="form-group m-form__group">
							<label for="statu">İsim</label>
							<input type="text" class="form-control" name="adi" id="adi">
					    </div> 						
		        		<div class="form-group m-form__group">
							<label for="statu">Soy İsim</label>
							<input type="text" class="form-control" name="soyadi" id="soyadi" >
					    </div> 						
		        		<div class="form-group m-form__group">
							<label for="statu">T.C. Kimlik</label>
							<input type="text" class="form-control" name="tckimlik" id="tckimlik" maxlength="11" minlength="11" onkeypress="return isNumberKey(event)">
					    </div> 						
		        		<div class="form-group m-form__group">
							<label for="statu">SSK/Bağkur/Emekli Sandık No</label>
							<input type="text" class="form-control" name="sskno" id="sskno" >
					    </div> 						
		        		<div class="form-group m-form__group">
							<label for="statu">Telefon</label>
							<input type="text" class="form-control" name="tel" id="tel" >
					    </div> 						
		        		<div class="form-group m-form__group">
							<label for="statu">Ülke</label>
							<select class="form-control" name="fk_ulke" id="fk_ulke">
								<option value='0'>--- TÜM ÜLKELER ---</option>
		                    	<c:forEach items="${ulkeler}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
					    	</select>
					    </div> 						
		        		<div class="form-group m-form__group">
							<label for="statu">Şehir</label>
							<select class="form-control" name="fk_il" id="fk_il">
								<option value="0"> --- TÜM İLLER --- </option>	
							</select>
					    </div> 						
		        		<div class="form-group m-form__group">
							<label for="statu">İlçe</label>
							<select class="form-control" name="fk_ilce" id="fk_ilce">				    	
								<option value="0"> --- TÜM İLÇELER --- </option>
							</select>
					    </div> 						
					</div>
					<div class="kt-portlet__foot">
						<button class="btn btn-danger btn-sm float-left" id="btnTemizle">
							<i class="fa fa-times"></i> 
							Temizle
						</button>						
						<button class="btn btn-brand btn-sm float-right" id="btnSorgula">
							<i class="fa fa-search"></i> 
							Filtrele
						</button>
					</div>
				</div>				
			</div>	
			<div class="col-md-9">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${pageTitle}
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div id=bildirimGrid></div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>



<script type="text/x-kendo-template" id="bildirimGridToolbar">
    <div class="toolbar">

    </div>
</script>


<script id="adiColTemplate" type="text/x-kendo-template">
	#: adi # #: soyadi #
</script>
<script id="telColTemplate" type="text/x-kendo-template">
	#if(tel1!=''){#
		#: tel1 #
	#}else{#
		#: gsm #
	#}#		
</script>
<script id="islemlerColTemplate" type="text/x-kendo-template">
	#if(kayit_tipi!='20'){#
		<button class="btn btn-label-info btn-icon btn-sm " title="Üye Onayla" data-id="#:id#" data-statu="20" onclick="btnOnay('#:id#','20');"><i class="fa fa-check"></i></button>&nbsp;&nbsp;
	#}else{#
		<button class="btn btn-label-danger btn-icon btn-sm " title="Üye Hesap Durdur" data-id="#:id# data-statu="30" onclick="btnOnay('#:id#','30');"><i class="fa fa-times"></i></button>&nbsp;&nbsp;
	#}#
	<a class="btn btn-label-info btn-sm btn-icon btn_sifre" title="Şifre Oluştur" data-id="#:id#" data-statu="20"  onclick="btnSifre('#:id#','20');"><i class="fa fa-lock"></i></button>&nbsp;&nbsp;
</script>


<script>
var crudServiceBaseUrl="/modul/mobil";
bildirimGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
			
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				:   { editable:false,type: "string" },
                "soyadi"			:   { editable:false,type: "string" },
                "tckimlik"			:   { editable:false,type: "string" },
                "tel"				:   { editable:false,type: "string" },
                "sehir"				:   { editable:false,type: "string" },
                "email"				:   { editable:false,type: "string" },
                "gsm"				:   { editable:false,type: "string" },
				"kayit_tipi"		: 	{ editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});

function bildirimGridDsFilter(adi,soyadi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,kayit_tipi){
	bildirimGridDataSource.filter([

            {
                field: "adi",
                operator: "eq",
                value: adi
            },
            {
                field: "soyadi",
                operator: "eq",
                value: soyadi
            },
            {
                field: "tckimlik",
                operator: "eq",
                value: tckimlik
            },
            {
                field: "ssk",
                operator: "eq",
                value: ssk
            },          
            {
                field: "tel",
                operator: "eq",
                value: tel
            },                                                  
            {
                field: "fk_ulke",
                operator: "eq",
                value: fk_ulke
            },                                                  
            {
                field: "fk_il",
                operator: "eq",
                value: fk_il
            },                                                  
            {
                field: "fk_ilce",
                operator: "eq",
                value: fk_ilce
            },                                                                                                    
            {
                field: "kayit_tipi",
                operator: "eq",
                value: kayit_tipi
            }                                                  
        ]
    );

}

var bildirimGrid = $("#bildirimGrid").kendoGrid({
    "columns": [

        {
            "filterable": false, "groupable": false, "title": "Ad Soyad",
			template:kendo.template($("#adiColTemplate").html()),
        },
        {
            "field": "tckimlik",
            "filterable": false, "groupable": false, "title": "T.C. Kimlik"
        },
        {
            "filterable": false, "groupable": false, "title": "Telefon",
			template:kendo.template($("#telColTemplate").html())
        },
        {
            "field": "sehir",
            "filterable": false, "groupable": false, "title": "Şehir"
        },
        {
            "field": "email",
            "filterable": false, "groupable": false, "title": "E-Posta"
        },

        {
	
            template:kendo.template($("#islemlerColTemplate").html()),
        }   
                   
    ],
    "autoBind": true,
    "dataSource":bildirimGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": false,
    "navigatable": false,
    "editable": "false",
    "pageable": {"alwaysVisible": true, "responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [10,20,30,50]},
    "height":800,
//	toolbar: ["excel"],
/*	excel: {
		fileName: "Kendo UI Grid Export.xlsx",
		filterable: true
	},*/
});

var grid = $("#bildirimGrid").data("kendoGrid");


function getIller(fk_ulke){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	
	$.ajax({
		type: "POST",
		url: '/lookup/getIller',
		data:
				{
					fk_ulke: fk_ulke
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_il').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}

function getIlceler(fk_il){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	
	$.ajax({
		type: "POST",
		url: '/lookup/getIlceler',
		data:
				{
					fk_il: fk_il
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_ilce').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}


$("#fk_ulke").change(function(){
	var fk_ulke=$("#fk_ulke option:selected").val();
	getIller(fk_ulke);
});
$("#fk_il").change(function(){
	var fk_il=$("#fk_il option:selected").val();
	getIlceler(fk_il);
});

 $("#fk_ulke").select2();
 $("#fk_il").select2();
 $("#fk_ilce").select2();
$("#btnSorgula").click(function(){
	var adi=$("#adi").val();
	var soyadi=$("#soyadi").val();
	var tc=$("#tckimlik").val();
	var ssk=$("#ssk").val();
	var tel=$("#tel").val();
	var fk_ulke=$("#fk_ulke option:selected").val();
	var fk_il=$("#fk_il option:selected").val();
	var fk_ilce=$("#fk_ilce option:selected").val();
	var kayit_tipi=$("#kayit_tipi option:selected").val();
	
	bildirimGridDsFilter(adi,soyadi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,kayit_tipi);
});

$("#btnTemizle").click(function(){
location.reload();
});
$("#mnuExport a").click(function(){
	var type=$(this).attr("export-type");
	alert(type)
});

function btnOnay(id,statu){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';

	alertify.confirm("Üye Statüsü Değiştirilecek ?", function (e) {	
		if (e) {
			$.ajax({
				type	: 'POST',
				url		: '/modul/mobil-onayla',
				data:
				{
					id: id,
					statu:statu
				},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success : function(r){
					alertify.success("İşlem Başarılı.");
					$("#btnSorgula").click();
				}
			});	
		}
	});	

}

function btnSifre(id,statu){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	alertify.confirm("Rastgele Şifre Oluşacak ?", function (e) {	
		if (e) {
			$.ajax({
				type	: 'POST',
				url		: '/modul/mobil-onayla',
				data:
				{
					id: id,
					statu:20,
					sifre:1
				},
				beforeSend: function (xhr) {
					xhr.setRequestHeader(header, token);
				},
				success : function(r){
					alertify.success("İşlem Başarılı.");
				}
			});	
		}
	});	
}
</script>

<jsp:include page="../BILDIRIM/footer.jsp" />
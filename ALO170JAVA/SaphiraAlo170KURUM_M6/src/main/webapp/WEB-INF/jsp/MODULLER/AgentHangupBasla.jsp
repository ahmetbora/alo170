<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">
	<table class="table table-striped table-bordered table-advance table-hover gtreetable gtreetable-fullAccess" id="gtreetable">
		<thead>
			<tr>
				<th>Dahili</th>
				<th>Adı Soyadı</th>
				<th>Kapatma Adeti</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${liste}" var="n">
				<tr>
					<td>${n.ext}</td>
					<td>${n.adi}</td>
					<td>${n.SAY}</td>
					<td><button class="btn btn-info " data-ext="${n.ext}"><i class="fa fa-search"></i> Detay</button></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<script>
$('.detaygoster').unbind().click(function(){
var sdate=$("#sdate").val();
var fdate=$("#fdate").val();
var lokasyon=$("#lokasyon").val();
var ext=$(this).attr("data-ext");

	$.ajax({
		type	: 'POST',
		url		: '/modul/agent-hangup-detay',	
		data:
		{
			sdate    	: sdate,
			fdate       : fdate,
			lokasyon	: lokasyon,
			ext			: ext
		},  
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success : function(e){
	
			$('#detay').html(e);		
		}
	});	
});
</script>
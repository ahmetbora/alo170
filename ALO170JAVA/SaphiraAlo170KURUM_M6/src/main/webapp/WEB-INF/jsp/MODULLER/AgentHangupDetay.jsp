<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">
	<div class="row" id="player"></div>
	<table class="table table-striped table-bordered table-advance table-hover gtreetable gtreetable-fullAccess" id="gtreetable">
		<thead>
			<tr>
				<th>Dahili</th>
				<th>Adı Soyadı</th>
				<th>İşlem Zamanı</th>
				<th>Arayan Numara</th>
				<th>Süre</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${liste}" var="n">
				<tr>
					<td>${n.ext}</td>
					<td>${n.adi}</td>
					<td>${n.time}</td>
					<td>${n.caller_id}</td>
					<td>${n.data2}</td>
					<td><button class="btn btn-info " data-ext="${n.callid}"><i class="fa fa-volume-up"></i> Ses Dinle</button></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<script>
			$('.sesdinle').click(function(){
			
			callid = $(this).attr("data-callid");

				$.ajax({
					type	: 'POST',
					url		: "/lpbx/modules/reports/PlaySound.php",
					data	: "CallID=" + callid,
					success : function(e){
						$('#player').html(e);
					}
				});


		});	
</script>
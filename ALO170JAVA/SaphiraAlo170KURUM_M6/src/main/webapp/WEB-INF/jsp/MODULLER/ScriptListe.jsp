<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../BILDIRIM/header.jsp" />

<style>

	.m-portlet__head{
		padding:2rem 1.2rem!important;
	}
	.k-grid td {
		padding: .929em 1.286em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
		}
	.k-filter-row th, .k-grid-header th.k-header {
		padding-top: .286em!important;
		padding-bottom: .286em!important;
		padding-right: .286em!important;
		padding-left: .286em!important;
	}   
	.k-grid-toolbar{
		padding-top:8px!important;
		padding-bottom:8px!important;
	}
	.m-portlet__body{
	padding:3px!important;
	}
	.k-grid tr td {
		border-bottom: 1px solid rgb(238, 238, 238);
	}
	.k-grid td {
		padding: .500em .600em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
	}
	.btn-sm, .btn-group-sm > .btn {
		padding: 0.3rem 0.5rem !important;
		font-size: 0.875rem;
		line-height: 1.5;
		border-radius: 0.2rem;
	}
	</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle} AD</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Personel İşlemleri </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
	<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<span class="kt-portlet__head-icon kt-hidden">
								<i class="la la-gear"></i>
							</span>
							<h3 class="kt-portlet__head-title">
								${pageTitle}
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								<a href="/modul/script-ekle" class="btn btn-outline-brand btn-bold btn-sm" >
									Yeni Script Ekle
								</a>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<div id=kullaniciGrid></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>	

<script type="text/x-kendo-template" id="kirilimColTemplate">
	#if(tip_level3=='0'){#
		#:knadi# <i class="la la-angle-double-right text-info"></i> #:konu# <i class="la la-angle-double-right text-info"></i> #:alt_konu# <i class="la la-angle-double-right text-info"></i> (KONUYU SEÇİN)
	#}else{#
		#:knadi# <i class="la la-angle-double-right text-info"></i> #:konu# <i class="la la-angle-double-right text-info"></i> #:alt_konu# <i class="la la-angle-double-right text-info"></i> #:alt_konu_detay#	
	#}#	
</script>
<script type="text/x-kendo-template" id="islemColTemplate">
	<a type="button" class="btn btn-info btn-sm" href="/modul/script-duzenle/#:id#"><i class="la la-edit"></i> Düzenle</a>
</script>
<script type="text/x-kendo-template" id="template">
	<div><br>
			<div class="kt-divider">
				<span></span>
				<span class="text-info">Açıklama</span>
				<span></span>
			</div>
			#:aciklama#
			<br>
			<div class="kt-divider">
				<span></span>
				<span class="text-info">Script İçeriği</span>
				<span></span>
			</div>
			#:script#
			<br><br>
	</div>
</script>
<script>
var crudServiceBaseUrl="/modul/script-liste";
kullaniciGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, 	nullable: true },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": false,
    "autoSync": false,
    "serverPaging": true,

});
            
           
            
var kullaniciGrid = $("#kullaniciGrid").kendoGrid({
    "columns": [  
        {
            field: "skadi","title": "Başlık",
        },                              
        {
           "title": "Kırılım",
		   template:kendo.template($("#kirilimColTemplate").html())
        },                                                           
        {
           "title": "",
		   template:kendo.template($("#islemColTemplate").html()),
		   width:110
        },                                                           
    ],
    "autoBind": true,
    "dataSource":kullaniciGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": false,
    "navigatable": false,
    "editable": false,
    "pageable": {"alwaysVisible": true, "pageSize": 20,"responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [20,30,40,50]},
    "height":800,
	detailTemplate: kendo.template($("#template").html()),
});

</script>
<jsp:include page="../BILDIRIM/footer.jsp" />
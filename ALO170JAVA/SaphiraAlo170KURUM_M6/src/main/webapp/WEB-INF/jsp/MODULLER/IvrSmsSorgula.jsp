<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">
	<table class="table table-striped table-bordered table-advance table-hover gtreetable gtreetable-fullAccess" id="gtreetable">
		<thead>
			<tr>
				<th>Tarih</th>
				<th>TC Numarası</th>
				<th>Telefon Numarası</th>
				<th>Web Servis Durumu</th>
				<th>İŞLEMLER</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${liste}" var="n">
				<tr>
					<td>${n.tarih}</td>
					<td>${n.tc}</td>
					<td>${n.numara}</td>
					<td>${n.webservis}</td>
					<td><button class="btn btn-danger <c:if test="${n.ws ==1}"> disabled </c:if>" id="ivr_sms_iptalet" data-id="${n.tc}"><i class="fa fa-times"></i> İptal Et</button></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<script>
$('#ivr_sms_iptalet').unbind().click(function(){
	var tcnosu=$(this).attr("data-id");
	alertify.confirm(tcnosu + " T.C. numaralı kişinin onayını iptal etmek istediğinize emin misiniz?", function (e) {
    if (e) {
		$.ajax({
			type	: 'POST',
			url		: '/modul/ivr-sms-iptal',	
			data:
			{
				sdate    	: sdate,
				fdate       : fdate,
				lokasyon	: lokasyon,
				ext			: ext
			},  
			beforeSend: function (xhr) {
				xhr.setRequestHeader(header, token);
			},
			success : function(e){
				alertify.success("İŞLEM TAMAM!");
				$('#tcnumarasi').val("");
				$('#sonuc').html("");		
			}
		});	
	} 
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<li class="kt-menu__item  kt-menu__item--submenu mnu_izin-taleplerim mnu_izin-talep-onayla mnu_izin-talebi-olustur mnu_personel-izin-talebi-olustur mnu_izin-talepleri" aria-haspopup="true" data-menu="izin">
    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
        <span class="kt-menu__link-icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect id="bound" x="0" y="0" width="24" height="24"></rect>
                    <rect id="Rectangle-62-Copy" fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"></rect>
                    <rect id="Rectangle-62-Copy-2" fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"></rect>
                    <rect id="Rectangle-62-Copy-4" fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"></rect>
                    <rect id="Rectangle-62-Copy-3" fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"></rect>
                </g>
            </svg>
        </span>
        <span class="kt-menu__link-text">İzin Talep İşlemleri</span>
        <i class="kt-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true" data-menu="izin-taleplerim">
                <a href="/modul/izin-taleplerim" class="kt-menu__link ">
                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                    <span class="kt-menu__link-text">İzin Taleplerim</span>
                </a>
            </li>	 	
            <li class="kt-menu__item " aria-haspopup="true" data-menu="izin-talep-onayla">
                <a href="/modul/izin-talep-onayla" class="kt-menu__link ">
                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                    <span class="kt-menu__link-text">Talep Onayla</span>
                </a>
            </li>	            
            <li class="kt-menu__item " aria-haspopup="true" data-menu="izin-talebi-olustur">
                <a href="/modul/izin-talebi-olustur" class="kt-menu__link ">
                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                    <span class="kt-menu__link-text">İzin Talebi Oluştur</span>
                </a>
            </li>	
            <li class="kt-menu__item " aria-haspopup="true" data-menu="personel-izin-talebi-olustur">
                <a href="/modul/personel-izin-talebi-olustur" class="kt-menu__link ">
                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                    <span class="kt-menu__link-text">Personel İzin Talebi Oluştur</span>
                </a>
            </li>		
	
        </ul>
    </div>
</li> 
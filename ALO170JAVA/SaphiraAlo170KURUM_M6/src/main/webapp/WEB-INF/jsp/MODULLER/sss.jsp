<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../BILDIRIM/header.jsp" />
 
 <style>
	 #bildirimGrid td{
		 font-weight: 400 !important;
	 }
 </style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle} </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Modüller </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar">
				<div class="kt-subheader__wrapper">
					<a href="/modul/sss-ekle" class="btn btn-sm btn-brand" >
						Yeni SSS 
					</a>

				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<!-- rapor filtre start-->
			<div class="col-md-3">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								FİLTRELE
							</h3>
						</div>
			
					</div>
					<div class="kt-portlet__body">	
						<div class="form-group m-form__group">
					    	<label for="statustatu">Statü</label>
		                    <select name="statu" id="statu" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>Onay Bekeleyen</option>
		                    	<option value='1'>Onaylanmış</option>
		                    </select>	    	
						</div>	
						<div class="form-group m-form__group">
					    	<label for="fk_sss_grup">Konular</label>
		                    <select name="fk_sss_grup" id="fk_sss_grup" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Seciniz ---</option>
		                    	<c:forEach items="${konu}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
		                    </select>	    	
						</div>	
						<div class="form-group m-form__group">
					    	<label for="fk_sss_altgrup">Alt Konular</label>
		                    <select name="fk_sss_altgrup" id="fk_sss_altgrup" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>--- Seciniz ---</option>
		                    	<c:forEach items="${altkonu}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
		                    </select>	    	
						</div>	
						<div class="form-group m-form__group">
					    	<label for="fk_il">Konu Başlığı</label>
							<input type="text" class="form-control" id="baslik">
						</div>	
																											
						<div class="form-group m-form__group">
							<button class="btn btn-danger btn-sm float-left" id="btnClear">
								<i class="fa fa-times"></i> 
								Temizle
							</button>						
							<button class="btn btn-brand btn-sm float-right" id="btnReport">
								<i class="fa fa-search"></i> 
								Filtrele
							</button>
						</div>						
					</div>
				</div>				
			</div>
			<!-- rapor filtre end-->	
			
			<!-- rapor grid start-->
			<div class="col-md-9">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								${pageTitle}
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div id="bildirimGrid"></div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>


<script type="text/x-kendo-template" id="ekleyenColTemplate">
	#=ekleyen# / #=ssstarih# 
</script>
<script type="text/x-kendo-template" id="islemColTemplate">
	<a href="/modul/sss-detay/#=id#" class="btn btn-info btn-sm" target="_blank">Detay</a>
</script>

<script>

var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;

var token = $("input[name='_csrf']").val();
var header = "X-CSRF-TOKEN";
$(document).ajaxSend(function(e, xhr, options) {
	xhr.setRequestHeader(header, _csrf_token);
});

$.ajaxSetup({
    headers:
    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});
$("#fk_sss_grup").change(function(){
        var fk_sss_grup=$("#fk_sss_grup option:selected").val();
        $.ajax({
            type	: 'POST',
            url		: '/lookup/getSssAltKonu',
			data:
			{
				fk_sss_grup: fk_sss_grup
			},
			beforeSend: function (xhr) {
				xhr.setRequestHeader(header, token);
			},
            success : function(r){
                $("#fk_sss_altgrup").html(r);
            }
        });
    });


var crudServiceBaseUrl="/modul/sss";
bildirimGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            			:   { editable: false, nullable: true },
                "baslik"            		:   { editable: false, nullable: true },
                "ekleyen"            		:   { editable: false, nullable: true },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});


function bildirimGridDsFilter(statu,fk_sss_grup,fk_sss_altgrup,baslik){
	bildirimGridDataSource.filter([
            {
                field: "statu",
                operator: "eq",
                value: statu
            },                                                 
            {
                field: "fk_sss_grup",
                operator: "eq",
                value: fk_sss_grup
            },                                                                                                 
            {
                field: "fk_sss_altgrup",
                operator: "eq",
                value: fk_sss_altgrup
            },                                                 
            {
                field: "baslik",
                operator: "eq",
                value: baslik
            },                                                                                               
                                
        ]
    );
}

var bildirimGrid = $("#bildirimGrid").kendoGrid({
    "columns": [
        {
            "field": "baslik",
            "filterable": false, "groupable": false, "title": "Başlık","width":"75%",
        },
        {
            "field": "ekleyen",
            "filterable": false, "groupable": false, "title": "Ekleyen Bilgisi",
			template:kendo.template($("#ekleyenColTemplate").html())
        },
        {
            "filterable": false, "groupable": false,width:85,
			template:kendo.template($("#islemColTemplate").html())
        },
           
    ],
    "autoBind": false,
    "dataSource":bildirimGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": false,
    "navigatable": false,
    "editable": "false",
    "pageable": {"alwaysVisible": true, "responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [10,20,30,50]},
    "height":800,

});

var grid = $("#bildirimGrid").data("kendoGrid");
$("#btnReport").unbind().click(function(){
	var statu=$("#statu option:selected").val();
	var fk_sss_grup=$("#fk_sss_grup option:selected").val();
	var fk_sss_altgrup=$("#fk_sss_altgrup option:selected").val();
	var baslik=$("#baslik").val();
	bildirimGridDsFilter(statu,fk_sss_grup,fk_sss_altgrup,baslik);

})

</script>

<jsp:include page="../BILDIRIM/footer.jsp" />
 
 
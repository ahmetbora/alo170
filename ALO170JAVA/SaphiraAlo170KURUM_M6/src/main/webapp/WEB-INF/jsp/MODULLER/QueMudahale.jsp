<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../BILDIRIM/header.jsp" />
 
 
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					${pageTitle} </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Moduller </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						${pageTitle} </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar d-none">
				<div class="kt-subheader__wrapper">
					<a href="#" class="btn kt-subheader__btn-primary">
						Actions &nbsp;
	
						<!--<i class="flaticon2-calendar-1"></i>-->
					</a>
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
						<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000" />
								</g>
							</svg>
	
							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
	
							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Add anything or jump to:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Order</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Ticket</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Goal</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support Case</span>
										<span class="kt-nav__link-badge">
											<span class="kt-badge kt-badge--success">5</span>
										</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
									<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
								</li>
							</ul>
	
							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">	
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								OPERATÖR İŞLEMLERİ
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<div class="form-group ">
							<label class="control-label"><strong>DAHİLİ</strong></label>
							<div class="input-group">
								<input type="text" class="form-control" id="ext" value="" maxlength="5">
								<div class="input-group-append">
									<button class="btn btn-primary" type="button" id="btn_kaydet">Müdahale Et</button>
								</div>
							</div>
						</div>
						<div class="row">
							<div id="sonuc"></div>
						</div>
					</div>
				</div>			
			</div>
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								TOPLU OPERATÖR İŞLEMLERİ
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<div class="row">
							<label class="control-label col-md-2"><strong>DAHİLİ BAŞLANGICI</strong></label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="dahili" value="" maxlength="2">
							</div>
							<div class="col-md-4">
								<select name="uzunluk" id="uzunluk" class="form-control">
								<option value="4" selected="selected">4</option>
								<option value="5">5</option>
								</select>
							</div>
							<button id="btn_hepsi" class="btn btn-primary">MÜDAHALE ET</button>
						</div>
						<div class="row">
							<div id="sonuc"></div>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>
<script>
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	$("#btn_kaydet").click(function(){
	var agent=$("#ext").val();

	alertify.confirm(agent + " numaralı kişiye müdahale etmek istediğinize emin misiniz?", function (e) {
		if (e) {
			$.ajax({
				type	: 'POST',
				url		: '/modul/que-mudahale',
				data:
                {
                    agent    		: agent,
                    uzunluk         : 0,
                },  
				beforeSend: function (xhr) {
            		xhr.setRequestHeader(header, token);
        		},
				success : function(e){      		
					if(e=="1"){
						alertify.error("SADECE KENDİ LOKASYONUNUZDA TANIMLI AGENTLARA MÜDAHALE EDEBİLİRSİNİZ!");
						return false;
					}				
					alertify.success("İŞLEM TAMAM!");
					$('#ext').val("");
				}
			});
		} 
	});
});

$("#btn_hepsi").click(function(){
	var agent=$("#dahili").val();
	var uzunluk=$("#uzunluk :selected").val();
	
	alertify.confirm(agent + " ile başlayan "+uzunluk+" uzunluğundaki dahililere müdahale etmek istediğinize emin misiniz?", function (e) {
		if (e) {
			$.ajax({
				type	: 'POST',
				url		: '/que-mudahale',
				data:
                {
                    agent    		: agent,
                    uzunluk         : uzunluk,
                },  
				beforeSend: function (xhr) {
            		xhr.setRequestHeader(header, token);
        		},
				success : function(e){      				
					alertify.success("İŞLEM TAMAM!");
					$('#ext').val("");
				}
			});
		} 
	});
});
</script>
<jsp:include page="../BILDIRIM/footer.jsp" />
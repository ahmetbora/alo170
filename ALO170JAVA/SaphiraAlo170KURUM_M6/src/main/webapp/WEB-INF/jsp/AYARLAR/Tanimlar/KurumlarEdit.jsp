<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="modal-header" id="moda_header">
    <h5 class="modal-title" id="modalLabel">KURUM BİLGİSİ GÜNCELLE / EKLE</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body" id="modal_body">
    <c:if test = "${islem=='Edit'}">edit</c:if>
    <c:if test = "${islem=='Add'}">Add</c:if>
    <form>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Adı:</label>
            <input type="text" class="form-control" id="txtAdi" value="${kurumlar.adi}">
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Durumu:</label>
            <select class="form-control" id="isActive">
                <c:forEach items="${aktifpasiflist}" var="n">					
                    <option data-id="${n.adi}" value="${n.value}" <c:if test="${n.value eq kurumlar.isActive}"> selected</c:if>>${n.adi}</option>
                </c:forEach>
            </select>
        </div>
    </form>
</div>
<div class="modal-footer" id="modal_footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
    <button type="button" class="btn btn-primary" id="btn_modal_kaydet">Kaydet</button>
</div>
<input type="hidden" id="id" value="${kurumlar.id}">
<script>
$('#pnlBildirimDetay').on('hidden.bs.modal', function (e) {
  $("#modalContent").html('');
  $(this).off('hidden.bs.modal');
})		
</script>
<c:if test = "${islem=='Edit'}">
    <script>
        var token = '${_csrf.token}';
        var header ='${_csrf.headerName}';
        $("#btn_modal_kaydet").unbind().click(function(){
        var adi=$("#txtAdi").val();
        var id=$("#id").val();
        var isActive=$("#isActive option:selected").val();
        if(adi==""){
            alertify.error("Adı Alanı Boş Bırakılamaz.");
            return false;
        }
        $.ajax({
            type 	: 'POST',
            url		: "/ayarlar/tanimlar/Update",
            data	: "id="+id+"&cols=adi,isActive&vals="+adi + "," + "&tbl=task_konu&splitter=,",
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            },
            success : function(e){
                alertify.success("Tanım Güncellendi.")
              
                $("#divGrid").data("kendoGrid").dataSource.read();
                $('#pnlTanimlar').modal("hide");
            },
            error: function (request, status, error) {
                alertify.error("Tanım Güncellemesi Sırasında Hata.")
            }
        });
    })
    </script>
</c:if>
<c:if test = "${islem=='Add'}">
    <script>
        var token = '${_csrf.token}';
        var header ='${_csrf.headerName}';
        $("#btn_modal_kaydet").unbind().click(function(){
        var adi=$("#txtAdi").val();
        var id=$("#id").val();
        var isActive=$("#isActive option:selected").val();
        if(adi==""){
            alertify.error("Adı Alanı Boş Bırakılamaz.");
            return false;
        }
        $.ajax({
            type 	: 'POST',
            url		: "/ayarlar/tanimlar/Insert",
            data	: "id="+id+"&cols=adi,isActive&vals="+adi+","+isActive+"&tbl=task_konu&splitter=,",
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            },
            success : function(e){
                alertify.success("Tanım Güncellendi.")
              
                $("#divGrid").data("kendoGrid").dataSource.read();
                $('#pnlTanimlar').modal("hide");
            },
            error: function (request, status, error) {
                alertify.error("Tanım Güncellemesi Sırasında Hata.")
            }
        });
    })
    </script>
</c:if>

<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>

	                 <div class="m-portlet">
						<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									KULLANICI L�STES�
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item">
									<a href="/ayarlar/kullanici/ekle" onclick="" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-plus"> Yeni Ekle</i></a>
								</li>
							</ul>
						</div>
			  			</div>
			  			
			  			<div class="m-portlet__body" >



				<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                    	<th>Ad�</th>
                        <th>Kullan�c� Ad�</th>
                        <th>E-Posta</th>
                        <th>Yetki/Rol</th>
                        <th>Havuzlar</th>
                        
                        
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${filtre}" var="l" >
                        <tr>
                            <td> ${l.adi } </td>
                            <td> ${l.luser } </td>
                            <td> ${l.email } </td>
                            <td> ${l.rol } </td>
                            <td>
							---
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                
	</div>
</div> 
                

                
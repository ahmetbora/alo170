<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../header.jsp" />

<style>

	.m-portlet__head{
		padding:2rem 1.2rem!important;
	}
	.k-grid td {
		padding: .929em 1.286em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
		}
	.k-filter-row th, .k-grid-header th.k-header {
		padding-top: .286em!important;
		padding-bottom: .286em!important;
		padding-right: .286em!important;
		padding-left: .286em!important;
	}   
	.k-grid-toolbar{
		padding-top:8px!important;
		padding-bottom:8px!important;
	}
	.m-portlet__body{
	padding:3px!important;
	}
	.k-grid tr td {
		border-bottom: 1px solid rgb(238, 238, 238);
	}
	.k-grid td {
		padding: .500em .600em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
	}
	.btn-sm, .btn-group-sm > .btn {
		padding: 0.3rem 0.5rem !important;
		font-size: 0.875rem;
		line-height: 1.5;
		border-radius: 0.2rem;
	}
	</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Kullanıcı Listesi </h3>
				<span class="kt-subheader__separator kt-hidden"></span>

			</div>
			<div class="kt-subheader__toolbar">
				<div class="kt-subheader__wrapper">
					<a href="/ayarlar/kullanici/ekle" class="btn kt-subheader__btn-primary">
						Yeni Kullanıcı &nbsp;
						<i class="fa fa-plus"></i>
					</a>
		
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
	<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								KULLANICI FİLTRELE
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<form class="m-form m-form--fit m-form--label-align-right">
							<div class="form-group m-form__group">
								<label for="adi">Ad Soyad</label>
									<input type="text" name="adi" id="adi" value="" class="form-control  m-input" placeholder="Adı Soyadı"/>
							</div>
							<div class="form-group m-form__group">
								<label for="email">E-Posta</label>
								<input type="text" name="email" id="email" value="" class="form-control  m-input" placeholder="E-Posta"/>
							</div>
							<div class="form-group m-form__group">
								<label for="isActive">Durumu</label>
								<select name="isActive" id="isActive" class="form-control" style="width:100%">
									<option value='9999'>TÜMÜ</option>
									<option value='1'>AKTİF</option>
									<option value='0'>PASİF</option>
								</select>
							</div>					    	
							<div class="form-group m-form__group">
								<label for="fk_rol">Yetki Rolü</label>
								<select name="fk_rol" id="fk_rol" class="form-control" style="width:100%">
									<option value='0'>-- Seciniz --</option>
									<c:forEach items="${roller}" var="n">
									
											<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
								
									</c:forEach>
								</select>
							</div>					
						</form>						
					</div>
					<div class="m-portlet__foot p-3">
						<a href="javascript:void(0);"  class="btn btn-danger " id="btnKullaniciFiltreTemizle"><i class="fa fa-times"></i> Temizle</a>
						<a href="javascript:void(0);"  class="btn btn-brand " id="btnKullaniciFiltre"><i class="fa fa-search"></i> Filtrele</a>
					</div>					
				</div>				
			</div>
			<div class="col-md-9">
				<div class="kt-portlet">
					<div class="kt-portlet__body p-3">
						<div id=kullaniciGrid></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>	


<script id="isActiveTemplate" type="text/x-kendo-template">
	<span class="m-badge # if(isActive=='0') { # m-badge--danger # } else { # m-badge--success # } # m-badge--wide">#: durum #</span>
</script>
<script id="islemlerTemplate" type="text/x-kendo-template">
	<a class="btn btn-secondary btn-elevate btn-circle btn-icon" title="Düzenle" href="/ayarlar/kullanici/duzenle/#: id #">
		<i class="fa fa-user-edit"></i>
	</a>
	<a class="btn btn-secondary btn-elevate btn-circle btn-icon" title="Havuzları Düzenle" href="/ayarlar/kullanici/havuzDuzenle/#: id #">
		<i class="fa fa-user-tag"></i>
	</a>
	<a class="btn btn-secondary btn-elevate btn-circle btn-icon" title="Yetkileri Düzenle" href="/ayarlar/kullanici/yetkiDuzenle/#: id #">
		<i class="fa fa-user-cog"></i>
	</a>

</script>
<script type="text/x-kendo-template" id="detailTemplate">
	<div class="detailTabstrip">
		<ul>
			<li class="k-state-active">
				Havuzlar
			</li>
		</ul>
		<div>
			<div class="orders"></div>
		</div>
	</div>
</script>
<script>
var crudServiceBaseUrl="/ayarlar/kullanici/listeData";
kullaniciGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"            	:   { editable: false, nullable: true },
                "email"            	:   { editable: false, nullable: true },
                "luser"            	:   { editable: false, nullable: true },
                "isActive"          :   { editable: false, nullable: true },
                "rol"          		:   { editable: false, nullable: true },
                "ukey"          	:   { editable: false, nullable: true },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": false,
    "autoSync": false,
    "serverPaging": true
});

function kullaniciGridDsFilter(adi,email,fkRol,isActive){
	kullaniciGridDataSource.filter([
            {
                field: "adi",
                operator: "eq",
                value: adi
            },
            {
                field: "email",
                operator: "eq",
                value: email
            },
            {
                field: "fkRol",
                operator: "eq",
                value: fkRol
            },
            {
                field: "isActive",
                operator: "eq",
                value: isActive
            }            
        ]
    );

}

var kullaniciGrid = $("#kullaniciGrid").kendoGrid({
    "columns": [
        {
            field: "adi",
            filterable: false, groupable:false, "title": "Adı Soyadı"
        },    
        {
            field: "luser",
            filterable: false, groupable:false,  "title": "Kullanıcı Adı"
        },   
        {
            field: "email",
            filterable: false, groupable:false, "title": "E-Posta"
        },    
        {
            field: "rol",
            filterable: false,  "title": "Yetki Grubu"
        },            
        {
            field:"durum",
            template:kendo.template($("#isActiveTemplate").html()),
            title:"Durumu",
            groupable: true,
            width:110
        },  
        {
        	template:kendo.template($("#islemlerTemplate").html()),
            filterable: false,  
            title: "",
			width:130
      
        },                                               
    ],
    "autoBind": false,
    "dataSource":kullaniciGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": true,
    "navigatable": false,
    "editable": "false",
    "pageable": {"alwaysVisible": true, "pageSize": 20,"responsive": false,"input": false,"refresh": true, "info":true,"pageSizes": [20,30,40,50]},
    "height":800,
	detailInit: detailInit,
	detailTemplate: kendo.template($("#detailTemplate").html()),
	noRecords: true,
});

var grid = $("#kullaniciGrid").data("kendoGrid");


$("#btnKullaniciFiltre").click(function(){
	var adi=$("#adi").val();
	var email=$("#email").val();
	var fkRol=$("#fk_rol option:selected").val();
	var isActive=$("#isActive option:selected").val();
	kullaniciGridDsFilter(adi,email,fkRol,isActive);
});

$("#btnKullaniciFiltreTemizle").click(function(){
	$("#adi").val('');
	$("#email").val('');
	$("#fk_rol").val('0');
	$("#isActive").val('9999');
	var adi=$("#adi").val();
	var email=$("#email").val();
	var fkRol=$("#fk_rol option:selected").val();
	var isActive=$("#isActive option:selected").val();
	kullaniciGridDsFilter(adi,email,fkRol,isActive);
})


function detailInit(e) {
	e.preventDefault();
	var detailRow = e.detailRow;
	detailRow.find(".detailTabstrip").kendoTabStrip({
		animation: {
			open: { effects: "fadeIn" }
		}
	})

	var havuzCrudServiceBaseUrl="/ayarlar/kullanici/userHavuz";
	havuzGridDataSource = new kendo.data.DataSource({
		transport: {
			read:  {
				type: "POST",
				url: havuzCrudServiceBaseUrl,
				contentType: "application/json; charset=utf-8",            
				dataType: 'json',
				beforeSend: function(req) {
					req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
				}
			},
			
			parameterMap: function(options, operation) {
				return kendo.stringify(options);
				if (operation !== "read" && options.models) {
					//return {models: kendo.stringify(options.models)};
				}
			}
		},
		batch: true,
		pageSize: 30,
		schema: {
			data:'data', 
			model: {
				id: "id",
				fields: {
					"id"            	:   { editable: false, nullable: true },
					"adi"            	:   { editable: false, nullable: true }
				}
			},
			"total": "total"
		},
		"serverFiltering": true,
		"serverSorting": false,
		"autoSync": false,
		"serverPaging": true,
		filter: { field: "userId", operator: "eq", value: e.data.id }
	});
	detailRow.find(".orders").kendoGrid({
		dataSource:havuzGridDataSource,
		scrollable: false,
		sortable: true,
		pageable: true,
		columns: [
			{ field: "adi", title:"Havuz"},
		]
    });
}


</script>
<style>
	.k-detail-cell .k-tabstrip .k-content {
		padding: 0.2em;
	}
	.employee-details ul
	{
		list-style:none;
		font-style:italic;
		margin: 15px;
		padding: 0;
	}
	.employee-details ul li
	{
		margin: 0;
		line-height: 1.7em;
	}

	.employee-details label
	{
		display:inline-block;
		width:90px;
		padding-right: 10px;
		text-align: right;
		font-style:normal;
		font-weight:bold;
	}
	.k-grid .btn.btn-icon {
    height: 2.5rem !important;
    width: 2.5rem !important;
	}
	.k-grid .btn-icon.btn [class^="fa-"], .btn [class*=" fa-"] {
    font-size: 1.1rem !important;
	}
</style>
<jsp:include page="../footer.jsp" />
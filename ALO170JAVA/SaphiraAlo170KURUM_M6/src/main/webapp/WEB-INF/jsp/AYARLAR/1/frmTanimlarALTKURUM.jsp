<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="modal-header" id="moda_header">
	<h5 class="modal-title" id="modalLabel">Alt Kurum Düzenle</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body" id="modal_body">
	<div class="col-12">
		<input type="hidden" name="id" id="id" value="${list.id }" />
		<div class="form-group m-form__group row">
			<label for="adi">Kurum Adı</label>
				<select name="fk_parent" id="fk_parent" class="form-control m-select2" value="${list.fk_parent }" style="width:100%">				                    
					<option value='0'>-- Seçiniz --</option>
					<c:forEach items="${kurumlar}" var="n">
					<option data-ext="${n.id}" ${list.fk_parent == n.id ? 'selected' : ' '} value="${n.id}">${n.adi}</option>
					</c:forEach>
				</select>
		</div>
		<div class="form-group m-form__group row">
			<label for="adi">Alt Kurum Adı</label>
			<input type="text" name="adi" id="adi" value="${list.adi }" class="form-control  m-input" />
		</div>
		<div class="form-group m-form__group row">
			<label for="adi">Statu</label>
			<select name="active" id="active" class="form-control m-select2" style="width:100%">
				<option value='1' ${list.isActive == 1 ? 'selected' : ' '}>Aktif</option>
				<option value='0' ${list.isActive == 0 ? 'selected' : ' '}>Pasif</option>
			</select>
		</div>
			<div class="form-group m-form__group row">
			<label for="email">E-Mailler</label>
			<textarea id="email" name="email" class="form-control" data-provide="summernote"  rows="10">${list.email }</textarea>
		</div>
	</div>
</div>
<div class="modal-footer" id="modal_footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
	<button type="button" class="btn btn-primary" id="btn_modal_kaydet" onclick="AltKurumKaydet();">Kaydet</button>
</div>

				
							
							
<script>

$(document).ready(function(){

	$("#fk_parent").select2({ placeholder: "Seçiniz", minimumResultsForSearch: 1 / 0 });
	$("#active").select2({ placeholder: "Seçiniz", minimumResultsForSearch: 1 / 0 });

});
function AltKurumKaydet(){
	var data = {
			
			"id" : $('#id').val()
			,"adi" : $('#adi').val()
			,"fk_parent" : $('#fk_parent').val()
			,"email" : $('#email').val()
			, "active" : $('#active').val()
			, "${_csrf.parameterName}" : "${_csrf.token}"

};
	$.post("/ayarlar/tanimlarEkleAltKurum?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#pnlAltKurum').modal('hide');
		AltKurumlar();
		});
}

 
</script>
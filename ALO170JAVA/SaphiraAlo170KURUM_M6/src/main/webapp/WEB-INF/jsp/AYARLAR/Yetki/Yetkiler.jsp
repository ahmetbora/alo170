<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<jsp:include page="../header.jsp" />
<style>
.m-portlet__head{
	padding:2rem 1.2rem!important;
}
.k-grid td {
    padding: .50em .50em!important;
    padding-right: .286em!important;
    padding-left: .286em!important;
	font-size: 13px!important;
    }
.k-filter-row th, .k-grid-header th.k-header {
	padding-top: .286em!important;
	padding-bottom: .286em!important;
    padding-right: .286em!important;
    padding-left: .286em!important;
}   
.k-grid-toolbar{
	padding-top:8px!important;
	padding-bottom:8px!important;
}
.m-portlet__body{
padding:3px!important;
}
.k-grid-content .btn{
    padding: .40rem .60rem!important;
}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Yetki Grupları </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Ayarlar </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Yetki Grupları </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">	
			<div class="col-md-12 col-lg-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								YETKİ GRUPLARI
								<small id="adr"></small>
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
                            <div id="ptools"></div>
                            
						</div>
					</div>
					<div class="kt-portlet__body p-2">
                        <div class="row">
                            <div class="col-md-12 col-lg-3">
                                <div id="yetkiGrubuGrid">
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-9">
                                <div id="yetkiGrubuDetayCont" class="p-2"></div>
                            </div>
                        </div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>
<style>
    .toolbar{
        width: 100% !important;
    }
</style>
<script type="text/x-kendo-template" id="yetkiGrubuGridToolbar">
    <div class="toolbar">
        <input type="search" id="category" style="width: 150px; margin-right:30px"/>
        <button class="btn btn-brand btn-sm " id="btnEkle"><i class="fa fa-plus"></i> Ekle</button>
    </div>
</script>
<script type="text/x-kendo-template" id="editTemplate">
<div class="toolbar">
	<button class="btn btn-sm btn-info btnDetay" data-id="#: id #"><i class="fa fa-search"></i> Detay</button>
</div>
</script>
<script type="text/x-kendo-template" id="adiTemplate">
 #: adi #
</script>
<script>

$(document).ready(function(){
var crudServiceBaseUrl="/ayarlar/yetkiGruplariData";
yetkiGrubuGridDataSource = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
			dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    batch: true,
    pageSize: 30,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				:	{editable:false,nullable:true}
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function yetkiGrubuGridDsFilter(tip){
	yetkiGrubuGridDataSource.filter([
            {
                field: "tip",
                operator: "eq",
                value: tip
            }
        ]
    );

}

var yetkiGrubuGrid = $("#yetkiGrubuGrid").kendoGrid({
    "columns": [
    	{ 
        	title:"Yetki Grubu",
        	template:kendo.template($("#adiTemplate").html()),
        },
        {
            template:kendo.template($("#editTemplate").html()),
            width:85
        }          
    
    ],
    "autoBind": true,
    "dataSource":yetkiGrubuGridDataSource,
    "scrollable": true,
    "sortable": false,
    "persistSelection": true,
    "filterable": false,
    "reorderable": true,
    "resizable": true,
    "columnMenu": false,
    "groupable": false,
    "navigatable": false,
    "editable": "false",
    "pageable": {"alwaysVisible": false, "pageSize": 500,"responsive": false,"input": false,"refresh": true, "info":false},
    "toolbar": kendo.template($("#yetkiGrubuGridToolbar").html())
});

var dropDownData = [
    { text: "Aktif", value: "1" },
    { text: "Pasif", value: "0" }
];
var dropDown = yetkiGrubuGrid.find("#category").kendoDropDownList({
    dataTextField: "text",
    dataValueField: "value",
    dataSource: dropDownData,
    index: 0,
    change: function() {
        var value = this.value();
        yetkiGrubuGridDsFilter(value);
        
    }    
	
});

$("#yetkiGrubuGrid").on("click", ".btnDetay", function(){
   var id=$(this).attr("data-id");
    displayLoading(document.body);
	$.ajax({
		type 	: 'POST',
		url		: '/ayarlar/yetkiGrubuDetay',
		data	: "id="+id,
        beforeSend: function(req) {
            req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
        },
		success : function(e){
			$("#yetkiGrubuDetayCont").html(e);
            hideLoading(document.body);
			  
		},
		
	    error: function (request, status, error) {
            hideLoading(document.body);
	    }
	});
  });
});
</script>
<jsp:include page="../footer.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../header.jsp" />

<style>
.form-group{
	margin-bottom:1rem !important;
}
.m-portlet__head{
	padding:2rem 1.2rem!important;
}
.k-grid td {
    padding: .929em 1.286em;
    padding-right: .286em!important;
    padding-left: .286em!important;
	font-size: 13px!important;
    }
.k-filter-row th, .k-grid-header th.k-header {
	padding-top: .286em!important;
	padding-bottom: .286em!important;
    padding-right: .286em!important;
    padding-left: .286em!important;
}   
.k-grid-toolbar{
	padding-top:8px!important;
	padding-bottom:8px!important;
}
.m-portlet__body{
padding:3px!important;
}
.k-grid tr td {
    border-bottom: 1px solid rgb(238, 238, 238);
}
.k-grid td {
    padding: .500em .600em;
    padding-right: .286em!important;
    padding-left: .286em!important;
    font-size: 13px!important;
}
.btn-sm, .btn-group-sm > .btn {
    padding: 0.3rem 0.5rem !important;
    font-size: 0.875rem;
    line-height: 1.5;
    border-radius: 0.2rem;
}
.text-white{
	color:#ffffff !important;
}
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Yeni Kullanıcı </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Ayarlar </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Kullanıcı İşlemleri</a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>

		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	
			<div class="col-md-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								KULLANICI OLUŞTUR
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<form class="kt-form kt-form--label-right" autocomplete="off">										

							<div class="form-group  row">
								<div class="col-lg-6">
									<label><span class="m--font-danger">*</span> Kullanıcı Adı:</label>
									<input type="text" class="form-control m-input" placeholder="Kullanıcı Adı" autocomplete="off" name="username" id="username" value="${UserName}">
									
								</div>
								<div class="col-lg-6">
									<label class="">Dahili:</label>
									<input type=text class="form-control m-input" placeholder="Dahili" autocomplete="off" name="ext" id="ext" value="${User.ext}">
								</div>
							</div>
							<div class="form-group  row">
								<div class="col-lg-6">
									<label>E-Posta:</label>
									<input type="email" class="form-control m-input" placeholder="E-Posta" autocomplete="off" name="email" id="email" value="${User.email}">
								</div>
								<div class="col-lg-6">
									<label><span class="m--font-danger">*</span> Ad Soyad:</label>
									<input type="text" class="form-control m-input" placeholder="Ad Soyad" autocomplete="off" name="adi" id="adi" value="${User.adi}">
								</div>
							</div>
							<div class="form-group  row">
								<div class="col-lg-6">
									<label>Şifre:</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<button class="btn btn-success" type="button" id="copyPassword"><i class="fa fa-copy text-white"></i></button>
											<button class="btn btn-brand " type="button" id="showPassword"><i class="fa fa-eye text-white"></i></button>
										</div>
										<input type="password" class="form-control m-input" placeholder="Şifre" name="password" id="password">
										<div class="input-group-append">
											<button class="btn btn-primary" type="button" id="generatePassword"><i class="fa fa-history text-white"></i></button>
										</div>
										
									</div>
									<span class="m-form__help">
										Şifre en az 9 karakterden oluşmalıdır. En az 1 büyük harf,1 küçük harf,1 rakam,1 özel karakter içermelidir.Örnek : c2w63#I9T-15
									</span>
								</div>
								<div class="col-lg-6">
									<label class="">Şifre Tekrar:</label>
									<input type="password" class="form-control m-input" placeholder="Şifre Tekrar" name="password_t" id="password_t">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-6">
									<label>Kullanıcı Rol:</label>
									<select name="fk_rol" id="fk_rol" class="form-control" style="width:100%">
										<option value='0'>-- Seciniz --</option>
										<c:forEach items="${roller}" var="n">
											
												<option data-ext="${n.id}" value="${n.id}" <c:if test="${User.getFkRol()==n.id}"> selected </c:if> >${n.adi}</option>
											
										</c:forEach>
									</select>
								</div>
								<div class="col-lg-6">
									<label>Lokasyon:</label>
									<select name="fk_lokasyon" id="fk_lokasyon" class="form-control" style="width:100%">
										<option value='0'>-- Seciniz --</option>
										<c:forEach items="${lokasyon}" var="n">
										<option data-ext="${n.id}" value="${n.id}" <c:if test="${User.getFkLokasyon()==n.id}"> selected </c:if> >${n.adi}</option>
										</c:forEach>
									</select>
								</div>
							</div>
	
							<div class="form-group row" id="row_kurum">
								<div class="col-lg-6">
									<label>Kurum:</label>
									<select name="fk_kurum" id="fk_kurum" class="form-control" style="width:100%">
										<option value='0'>-- Seciniz --</option>
										<c:forEach items="${kurumlar}" var="n">
										<option data-ext="${n.id}" value="${n.id}" <c:if test="${User.getfk_kurum()==n.id}"> selected </c:if> >${n.adi}</option>
										</c:forEach>
									</select>
								</div>
								<div class="col-lg-6">
									<label class="">GSM:</label>
									<input type="text" class="form-control m-input" placeholder="GSM" autocomplete="off" name="gsm" id="gsm" value="${User.gsm}">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-6">
									<label>T.C. Kimlik No:</label>
									<input type="text" class="form-control m-input" placeholder="T.C. Kimlik No" autocomplete="off" name="tckimlik" id="tckimlik" value="${User.tckimlik}">
								</div>
								<div class="col-lg-6">
									<label class="">Kurum Sicil No:</label>
									<input type="text" class="form-control m-input" placeholder="Kurum Sicil No" autocomplete="off" name="ksicil" id="ksicil" value="${User.ksicil}">
								</div>
							</div>
							<div class="form-group row d-none">
								<div class="col-lg-6">
									<label>Emekli San. Sicil No:</label>
									<input type="text" class="form-control m-input" placeholder="Emekli San. Sicil No" autocomplete="off" name="emsicil" id="emsicil" value="${User.emsicil}">
								</div>

							</div>
							<div class="form-group row d-none">
								<div class="col-lg-6">
									<label>Facebook:</label>
									<input type="text" class="form-control m-input" placeholder="Facebook" autocomplete="off" name="face" id="face" value="${User.facebook}">
								</div>
								<div class="col-lg-6  d-none">
									<label class="">Twitter:</label>
									<input type="text" class="form-control m-input" placeholder="Twitter" autocomplete="off" name="twit" id="twit" value="${User.twitter}">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-6">
									<label>E-mail Bildirimi:</label>
									<select id="email_bildirim" name="email_bildirim" class="form-control">
										<option value="0" <c:if test="${User.email_bildirim=='0'}"> selected="selected" </c:if> >Bildirim Almasın</option>
										<option value="1"  <c:if test="${User.email_bildirim=='1'}"> selected="selected" </c:if> >Bildirim Alsın</option>
									</select>
								</div>
								<div class="col-lg-6">
									<label class="">Durumu:</label>
									<select id="isActive" name="isActive" class="form-control">
										<option value="1"  <c:if test="${User.isActive=='1'}"> selected="selected" </c:if> >Aktif</option>
										<option value="0"   <c:if test="${User.isActive=='0'}"> selected="selected" </c:if> >Pasif</option>
									</select>
								</div>
							</div>
							</form>	
					
					</div>
					<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">				
						<button class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon btn-md " id="btnIptal">
							<span>
								<span>Vazgeç </span>&nbsp;&nbsp;
								<i class="fa fa-times"></i>
							</span>
						</button>					
						<button class="btn btn-outline-success m-btn m-btn--custom m-btn--icon btn-md " id="btnKullaniciGuncelle" >
							<span>
								<span>Kaydet </span>&nbsp;&nbsp;
								<i class="fa fa-save"></i>
							</span>
						</button>				
						<span class="clearfix"></span>				
					</div>
				</div>			
			</div>
		</div>
	</div>

</div>


<jsp:include page="../footer.jsp" />

<script>
var token = '${_csrf.token}';
var header ='${_csrf.headerName}';
$("#showPassword").click(function(){
	var attr=$("#password").attr("type");
	if(attr=="text"){
		$("#password").attr("type","password");
		$("#showPassword i").removeClass("fa-eye-slash");
		$("#showPassword i").addClass("fa-eye");
	}else{
		$("#password").attr("type","text");
		$("#showPassword i").removeClass("fa-eye");
		$("#showPassword i").addClass("fa-eye-slash");
	}
});

$("#generatePassword").click(function(){
	displayLoading("#yeniKullaniciCont");
	$("#generatePassword").attr("disabled",true);
	$.ajax({
		type	: 'POST',
		dataType:'JSON',
		url		: '/ayarlar/generateUserPassword',
		data	: '',
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success : function(r){
			$("#password").val(r.mesaj);
			$("#password_t").val(r.mesaj);
			hideLoading("#yeniKullaniciCont");
			$("#generatePassword").attr("disabled",false);
			return false;
		},
		error: function (request, status, error) {
			hideLoading("#yeniKullaniciCont");
			$("#generatePassword").attr("disabled",false);
			alertify.error("Şifre Oluşturulamadı")
		}
	});	
});

$('#copyPassword').on("click", function(){
	value = $("#password").val();
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val(value).select();
	document.execCommand("copy");
	$temp.remove();
	alertify.success(value + " Şifre Kopyalandı.");
});


$("#btnKullaniciGuncelle").click(function(){
	var username=$("#username").val();
	var ext=$("#ext").val();
	var email=$("#email").val();
	var password=$("#password").val();
	var password_t=$("#password_t").val();
	var adi=$("#adi").val();
	var gsm = $("#gsm").val();
	var face = $("#face").val();
	var twit = $("#twit").val();
	var tckimlik = $("#tckimlik").val();
	var ksicil = $("#ksicil").val();
	var emsicil = $("#emsicil").val();
	var email_bildirim=$("#email_bildirim option:selected").val();
	var isActive=$("#isActive option:selected").val();
	var fk_rol=$("#fk_rol option:selected").val();
	var fk_lokasyon=$("#fk_lokasyon option:selected").val();
	var fk_kurum=$("#fk_kurum option:selected").val();
	if(adi==""){
		alertify.error("İsim Soy İsim Giriniz");
		$("#adi").focus();
		return false;
	}
	if(username==""){
		alertify.error("Kullanıcı Adı Giriniz");
		$("#username").focus();
		return false;
	}

	if(password!=""){
		if(checkPassword(password)!="1"){
			alertify.error("Şifreniz en az 9 karakter, 1 büyük harf, 1 küçük harf ve en az 1 özel karakter içermelidir.");
			$("#password").focus();
			return false;
		}
		if(password!=password_t){
			alertify.error("Şifreler Uyuşmuyor");
			$("#password").focus();
			return false;
		}				
	}

	if(fk_rol==0){
		alertify.error("Kullanıcı Rol Seçimi Yapınız.");
			$("#fk_rol").focus();
			return false;
	}
	displayLoading("body");
	$.ajax({
		type	: 'POST',
		url		: '/ayarlar/kullanici/kullaniciKaydet',
		data	: 'adi='+adi+'&email='+email+'&password='+password+'&password_t='+password_t+'&username='+username+'&gsm='+gsm+'&ext='+ext+'&tckimlik='+tckimlik+'&emsicil='+emsicil+'&ksicil='+ksicil+'&email_bildirim='+email_bildirim+'&IsActive='+isActive+'&fk_rol='+fk_rol+'&fk_lokasyon='+fk_lokasyon+"&fk_kurum="+fk_kurum,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success : function(r){
			hideLoading("body");
			if(r.sonuc=="2"){
				alertify.error(r.mesaj);
				return false;
			}
			if(r.sonuc=="0"){
				alertify.error("Kullanıcı Güncellenirken Sorun Oluştu");
				return false;
			}

			if(r.sonuc=="1"){
				alertify.set({ labels: {
				    ok     : "Tamam",
				} });
				alertify.alert("Kullanıcı Güncellendi.");
			
				setTimeout(function(){
					location.reload();
				}, 2000);
			}
		},
		error: function (request, status, error) {
			alertify.error("Kullanıcı Güncellenirken Sorun Oluştu")
			hideLoading("body");
		}		
	});
})
$("#fk_rol").kendoDropDownList()
$("#fk_lokasyon").kendoDropDownList()
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>

<jsp:include page="../header.jsp" />
<script src="./assets/js/scripts.bundle.js" type="text/javascript"></script>
<style>

	.m-portlet__head{
		padding:2rem 1.2rem!important;
	}
	.k-grid td {
		padding: .929em 1.286em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
		}
	.k-filter-row th, .k-grid-header th.k-header {
		padding-top: .286em!important;
		padding-bottom: .286em!important;
		padding-right: .286em!important;
		padding-left: .286em!important;
	}   
	.k-grid-toolbar{
		padding-top:8px!important;
		padding-bottom:8px!important;
	}
	.m-portlet__body{
	padding:3px!important;
	}
	.k-grid tr td {
		border-bottom: 1px solid rgb(238, 238, 238);
	}
	.k-grid td {
		padding: .500em .600em;
		padding-right: .286em!important;
		padding-left: .286em!important;
		font-size: 13px!important;
	}
	.btn-sm, .btn-group-sm > .btn {
		padding: 0.3rem 0.5rem !important;
		font-size: 0.875rem;
		line-height: 1.5;
		border-radius: 0.2rem;
	}
	.text-white{
		color:#ffffff !important;
	}
	</style>
	
	<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
		<!-- begin:: Subheader -->
		<div class="kt-subheader   kt-grid__item" id="kt_subheader">
			<div class="kt-container  kt-container--fluid ">
				<div class="kt-subheader__main">
					<h3 class="kt-subheader__title">
						Ayarlar </h3>
					<span class="kt-subheader__separator kt-hidden"></span>
					<div class="kt-subheader__breadcrumbs">
						<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
						<span class="kt-subheader__breadcrumbs-separator"></span>
						<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
							Kullanıcı İşlemleri </a>
						<span class="kt-subheader__breadcrumbs-separator"></span>
						<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
							Kullanıcı Yetki Tanımlamaları </a>
		
						<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
					</div>
				</div>
	
			</div>
		</div>
		<!-- end:: Subheader -->
			<!-- begin:: Content -->
		<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" >
			<div class="row">	
				<div class="col-md-12">
					<div class="kt-portlet kt-portlet--last  kt-portlet--responsive-mobile" id="kt_page_portlet">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title kt-font-primary">
									YETKİ TANIMLAMALARI
								</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="btn-group">
									<button class="btn btn-danger m-btn m-btn--custom m-btn--icon btn-md " id="btnIptal">
										<span>
											<span>Vazgeç </span>&nbsp;&nbsp;
											<i class="fa fa-times"></i>
										</span>
									</button>	&nbsp;				
									<button class="btn btn-success m-btn m-btn--custom m-btn--icon btn-md " id="btnUserYetkiDuzenle" >
										<span>
											<span>Kaydet </span>&nbsp;&nbsp;
											<i class="fa fa-save"></i>
										</span>
									</button>	
								</div>
							</div>
						</div>
						<div class="kt-portlet__body p-3">
							<div class="alert alert-metal alert-dismissible fade show   m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert">
								<strong>${UserName} / ${User.adi}</strong>&nbsp;Kullanıcısının yetkileri düzenleniyor.	
							</div>				
							<select id="fk_rol" style="width:100%">
								<option value="0" selected>Mevcut Yetkileri Göster...</option>
								<c:forEach items="${roller}" var="n">
									<option data-id="${n.id}" value="${n.id}" <c:if test="${User.getFkRol()==n.id}"> selected </c:if>>${n.adi}</option>
								</c:forEach>
							</select>
							<div class=clearfix">&nbsp;</div>
							<select id="fk_lokasyon" style="width:100%">
								<option value="0" selected>Lokasyon Seçin ...</option>
								<c:forEach items="${lokasyon}" var="n">
									<option data-id="${n.id}" value="${n.id}" <c:if test="${User.getFkLokasyon()==n.id}"> selected </c:if>>${n.adi}</option>
								</c:forEach>
							</select>			
						
						</div>
					</div>			
<br><br>

					<div class="kt-portlet kt-portlet--last  kt-portlet--responsive-mobile" id="kt_page_portlet">
						<div class="kt-portlet__body p-3">
							<div id="yetkiGrubuDetayCont"></div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	
	</div>

<script>
function initYetkiGrubuCombo(){
	var yetkiGrubuCombo=$("#fk_rol").kendoDropDownList({
		filter: "contains",
		change:yetkiGrubuOnChange,

		});
	
}
initYetkiGrubuCombo();

function initLokasyonCombo(){
	var lokasyonCombo=$("#fk_lokasyon").kendoDropDownList({
		filter: "contains"
	});
}
initLokasyonCombo();
	
function yetkiGrubuOnChange(e){
	 var value = this.value();
	   displayLoading(document.body);
	   if(value=="0"){
		loadUserYetkiDetay("${userId}",1);
	   }else{
	   	loadUserYetkiDetay(value,0);
	   }
}

function loadUserYetkiDetay(id,tip){
	   displayLoading(document.body);
		$.ajax({
			type 	: 'POST',
			url		: '/ayarlar/yetkiGrubuDetayDuzenleUser',
			data	: "id="+id+"&tip="+tip,
	        beforeSend: function(req) {
	            req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	        },
			success : function(e){
				$("#yetkiGrubuDetayCont").html(e);
				  hideLoading(document.body);
				  
			},
			
		    error: function (request, status, error) {
		    	hideLoading(document.body);
		    }
		});	
}

var dropdownlist = $("#fk_rol").data("kendoDropDownList");
//dropdownlist.trigger("change"); 
loadUserYetkiDetay("${userId}",1);

$("#btnUserYetkiDuzenle").click(function(){
	var yetkiler="";
	var i=0;
	
	$("input.chk_yetki").each(function(){
		var checked=$(this).prop("checked");
		var id=$(this).attr("data-id");
		if(checked==true){
			i++;
			if(i==1){
				yetkiler +=id;
			}else{
				yetkiler +=","+id;
			}
		}
	});

	var switchInstance137 = $("#chk_yetki_137").data("kendoSwitch");
	var checked137=switchInstance137.element.attr("checked");
	var switchInstance138 = $("#chk_yetki_138").data("kendoSwitch");
	var checked138=switchInstance138.element.attr("checked");
	var yetkiRol="";
	if(checked137=="checked" || checked138=="checked"){
		var ii=0;
		$("input.chk_yetki_rol:checked").each(function(){
		var checked=$(this).prop("checked");
		var id=$(this).attr("data-id");
			ii++;
			if(ii==1){
				yetkiRol +=id;
			}else{
				yetkiRol +=","+id;
			}
		
	});
	}
	

	var yetkiGrubu=$("#fk_rol option:selected").val();
	var lokasyon=$("#fk_lokasyon option:selected").val();

	   displayLoading(document.body);
		$.ajax({
			type 	: 'POST',
			url		: '/ayarlar/kullanici/yetkiGrubuDetayGuncelleUser',
			data	: "uid=${uid}&fk_rol="+yetkiGrubu+"&fk_lokasyon="+lokasyon+"&yetkiler="+yetkiler+"&yetkiRol="+yetkiRol,
	        beforeSend: function(req) {
	            req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	        },
			success : function(e){
				alertify.set({ labels: {
				    ok     : "Tamam",
				    cancel : "İptal"
				} });
				hideLoading(document.body);
				if(e.sonuc=="1"){
					alertify.alert("Yetkiler Güncellendi",function(ex){
					//	location.reload();
					});	
				}
			},
		    error: function (request, status, error) {
		    	hideLoading(document.body);
		    }
		});	
})
</script>
<jsp:include page="../footer.jsp" />
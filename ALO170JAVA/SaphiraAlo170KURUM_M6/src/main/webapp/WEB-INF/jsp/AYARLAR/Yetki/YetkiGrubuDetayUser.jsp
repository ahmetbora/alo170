<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <style>
             .demo-section  li {
                margin: 0;
                padding: 10px 10px 10px 20px;
                min-height: 28px;
                line-height: 28px;
                vertical-align: middle;
                border-bottom: 1px solid rgba(128,128,128,.5);
                list-style:none
            }
            .demo-section li .k-switch {
                float: right;
            }            
 </style>

<c:forEach items="${yetkiAlanlari}" var="item">
	<c:if test="${item.getIsroot()==1}">
	<div id="acc${item.id}">
		<div class="kt-section">								
			<div class="kt-section__content kt-section__content--solid">
				<button type="button" class="btn btn-sm btn-brand btnSelectAll" data-id="${item.id}" data-statu="">Tümünü Seç</button>
				<button type="button" class="btn btn-sm btn-warning btnDeSelectAll d-none" data-id="${item.id}" data-statu="">Tümünü Kaldır</button>
			</div>
		</div>	
		<div class="row">
	</c:if>	
		<c:forEach items="${yetkiAlanlari}" var="subItem">
			<c:if test="${subItem.getFkroot()==item.getId()}">
			<div class="col-md-12 col-lg-5 mb-2 mt-2 " style=" margin-right: 5rem !important;">
				<i class="fa fa-question-circle ttip"  title="${subItem.aciklama}"></i>
				${subItem.adi}
				<div class="float-right">
					<input type="checkbox" id="chk_yetki_${subItem.id}" aria-label="${subItem.adi}" <c:if test="${subItem.getFkRol()>0}"> checked="checked" </c:if> class="chk_yetki" data-id="${subItem.id}"/>						
				</div>
			</div>	
			</c:if>						
		</c:forEach>
	<c:if test="${item.getIsroot()==1}">
	</div>	
	</div>	
	</c:if>	
</c:forEach>
		
<script>

		var btn='<button class="btn btn-sm btn-success" id="btnYetkiGrubuKaydet"><i class="fa fa-save"></i> Kaydet</button>';
		$("#ptools").html(btn);
        $(".yetkiler").kendoSwitch({
            checked: true,
            change: function (e) {
               // console.log("event :: change (" + (e.checked ? "checked" : "unchecked") + ")");
            }
        });
        $(".chk_yetki").kendoSwitch();
		$(".btnSelectAll").unbind().click(function(){
			var id=$(this).attr("data-id");
			var statu=$(this).attr("data-statu");
			
			$("#acc"+id+" input[type=checkbox]").each(function(){
				var chkId=$(this).attr("data-id");
				var switchInstance = $("#chk_yetki_"+chkId).data("kendoSwitch");
				var checked=switchInstance.element.attr("checked");
				if(checked!="checked"){
					switchInstance.toggle();
				}

			});
			$(this).addClass("d-none");
			$(".btnDeSelectAll[data-id="+id+"]").removeClass("d-none");
		});
		$(".btnDeSelectAll").unbind().click(function(){
			var id=$(this).attr("data-id");
			var statu=$(this).attr("data-statu");
			
			$("#acc"+id+" input[type=checkbox]").each(function(){
				var chkId=$(this).attr("data-id");
				var switchInstance = $("#chk_yetki_"+chkId).data("kendoSwitch");
				var checked=switchInstance.element.attr("checked");
				if(checked=="checked"){
					switchInstance.toggle();
				}

			});
			$(this).addClass("d-none");
			$(".btnSelectAll[data-id="+id+"]").removeClass("d-none");
		});

		var tooltip = $("#yetkiGrubuDetayCont").kendoTooltip({
			filter: ".ttip",
			width: 200,
			position: "top",
			animation: {
				open: {
					effects: "zoom",
					duration: 150
				}
			}
		}).data("kendoTooltip");		
</script>        
<c:forEach items="${yetkiAlanlari}" var="item">
	<c:if test="${item.getIsroot()==1}">
		<script>
        $('#acc${item.id}').kendoExpansionPanel({
            title: '${item.adi}',
            subTitle: '${item.adi} Yetkileri',
            expanded: true
        });
		</script>
	</c:if>
</c:forEach>
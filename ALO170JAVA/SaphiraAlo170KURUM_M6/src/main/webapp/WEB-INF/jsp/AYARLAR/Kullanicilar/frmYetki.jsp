<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<div class="m-portlet">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					YETKİ TANIMLAMALARI
				</h3>
			</div>
		</div>
	</div>			
	<div class="m-portlet__body">
		<select id="fk_rol" style="width:100%">
			<option value="0" selected>Yetki Grubu Seçin...</option>
			<c:forEach items="${roller}" var="n">
				<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
			</c:forEach>
		</select>
		<div class=clearfix">&nbsp;</div>
		<select id="fk_lokasyon" style="width:100%">
			<option value="0" selected>Lokasyon Seçin ...</option>
			<c:forEach items="${lokasyon}" var="n">
				<option data-id="${n.id}" value="${n.id}">${n.adi}</option>
			</c:forEach>
		</select>		
		<div id="yetkiGrubuDetayCont"></div>
	</div>
				
</div>

<script>
function initYetkiGrubuCombo(){
	var yetkiGrubuCombo=$("#fk_rol").kendoDropDownList({
		filter: "contains",
		change:yetkiGrubuOnChange,

		});
}
initYetkiGrubuCombo();

function initLokasyonCombo(){
	var lokasyonCombo=$("#fk_lokasyon").kendoDropDownList({
		filter: "contains"
	});
}
initLokasyonCombo();
	
function yetkiGrubuOnChange(e){
	 var value = this.value();
	   displayLoading(document.body);
		$.ajax({
			type 	: 'POST',
			url		: '/ayarlar/yetkiGrubuDetayDuzenleUser',
			data	: "id="+value+"&tip=0",
	        beforeSend: function(req) {
	            req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	        },
			success : function(e){
				$("#yetkiGrubuDetayCont").html(e);
				  hideLoading(document.body);
				  
			},
			
		    error: function (request, status, error) {
		    	hideLoading(document.body);
		    }
		});
}
</script>
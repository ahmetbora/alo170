<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>

<div class="m-portlet">
	<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
			
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						�ubeler
					</h3>
				</div>
			</div>
						
			<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#" onclick="SubeEkle(0);" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-plus"> Yeni Ekle</i></a>
						</li>
					</ul>
				</div>
  	</div>
			  			
<div class="m-portlet__body">

	<div class="form-group m-form__group">
                 <select name="fk_konu" id="fk_konu" class="form-control m-select2" style="width:100%">
                 	<option value='0'>-- T�M KURUMLAR --</option>
                 	<c:forEach items="${kurumlar}" var="n">
                 	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
                 	</c:forEach>
                 </select>
	</div>
						
<div class="row" id="subeFiltre">

				<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                    	
                    	<th>Ad�</th>
                    	<th>Kurum</th>
                    	<th>�l</th>
                    	<th>Havuz Say�s�</th>
                       	<th class="text-right">Durum</th>
                       	<th class="text-right">��lemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="l" >
                        <tr>
                        	
                            <td> ${l.adi } </td>
                            <td> ${l.kurum } </td>
                            <td> ${l.il } </td>
                            <td> ${l.havuz_sayisi } </td>
							<td class="text-right">
                            <c:if test="${l.isActive == 0}"><span class="m--font-danger">Pasif</span></c:if>
                            <c:if test="${l.isActive ne 0}"><span class="m--font-bold">Aktif</span></c:if>
                            </td>
                            <td class="text-right"> <a href="#" onclick="SubeEkle(${l.id})" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill">
								 <i class="la la-search"></i>
								 </a>
							</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
</div>
                
<div class="modal fade" id="pnlSube">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="fa fa-times" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="seklebody">
                    <div >

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

    </div>
</div>

<script>
function SubeEkle(id){
    
    $('#modalLabel').html('Sube Ekle / D�zenle');
    $('#pnlSube').modal('show');
    

	$.get("/ayarlar/tanimlarSube/" +id ,function(data) {
		$('#seklebody').html(data);
		});
}

var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;

$(document).ready(function(){

	$(function () {
	    var token = $("input[name='_csrf']").val();
	    var header = "X-CSRF-TOKEN";
	    $(document).ajaxSend(function(e, xhr, options) {
	        xhr.setRequestHeader(header, _csrf_token);
	    });
	});
	

		$.ajaxSetup({
		headers:
		{ 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		});
		
		$("#fk_konu").change(function() {
		
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		
		var headers = {};
		headers[csrfHeader] = csrfToken;
		
		$.ajax({
		    type: 'POST',
		    url: '/ayarlar/SubelerFiltre',
		    data: 
		    {
		        id: $('#fk_konu').val()
		        
		    },
		    success: function(response)
		    {
		    	$('#subeFiltre').html(response);
		    }
		});

});
	$("#fk_konu").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });

});

 
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<jsp:include page="../header.jsp" />
<style>
.k-grid tr td{
	border-width: 1px 0 0 0px !important;
    border-style: solid !important;
	border-color: #f1f1f1 !important;
}
.k-grid td{
	padding: .6rem;
}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Bildirim Havuzu </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Bildirimler </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Bildirim Havuzu </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">	
			<div class="col-md-12 col-lg-3">
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head  kt-portlet__head--noborder">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								TANIMLAR
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body kt-portlet__body--fit-y">
						<!--begin::Widget -->
						<div class="kt-widget kt-widget--user-profile-1">
							<div class="kt-widget__body">
								<div class="kt-widget__items tanimlar_list">
									<a href="/ayarlar/tanimlarKurum" class="kt-widget__item kt-widget__item--active widgetItem" data-islem="kurumlar">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Kurumlar
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarAltKurum" class="kt-widget__item widgetItem" data-islem="altkurumlar">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Alt Kurumlar
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarAramaKonusu" class="kt-widget__item widgetItem" data-islem="aramakonusu">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Arama Konusu
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarSube" class="kt-widget__item widgetItem" data-islem="subeler">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Şubeler / Merkezler
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarHavuz" class="kt-widget__item widgetItem" data-islem="havuzlar">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Havuzlar
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarAramaKanali" class="kt-widget__item widgetItem" data-islem="aramakanali">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Arama Kanalı Listesi
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarEgitimDurumu" class="kt-widget__item widgetItem" data-islem="egitim">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Eğitim Durumları
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarGuvenlikSorusu" class="kt-widget__item widgetItem" data-islem="guvenlik">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Güvenlik Sorusu
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarIller" class="kt-widget__item widgetItem" data-islem="iller">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Şehir Listesi
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarIlceler" class="kt-widget__item widgetItem" data-islem="ilceler">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												İlçe Listesi
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarUlkeler" class="kt-widget__item widgetItem" data-islem="ulkeler">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Ülke Listesi
											</span>
										</span>
									</a>
									<a href="/ayarlar/tanimlarDahili" class="kt-widget__item widgetItem" data-islem="dahili">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Dahili Tanımları
											</span>
										</span>
									</a>
									<a  class="kt-widget__item widgetItem" data-islem="kkoBaslik">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												K.K.O. Ana Kriterler
											</span>
										</span>
									</a>
									<a  class="kt-widget__item widgetItem" data-islem="kkoSoru">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												K.K.O. Alt Kriterler
											</span>
										</span>
									</a>
									<a  class="kt-widget__item widgetItem" data-islem="Periyodik">
										<span class="kt-widget__section">
											<span class="kt-widget__icon">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
														<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
													</g>
												</svg> </span>
											<span class="kt-widget__desc">
												Periyodik Rapor
											</span>
										</span>
									</a>
								</div>
							</div>
						</div>
						<!--end::Widget -->
					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-9">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								TANIMLAR
								<small id="adr"></small>
							</h3>

						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="form-group mb-0 ">
								<button class="btn btn-primary" id="btnAdd">Yeni <i class="fa fa-plus"></i></button>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-1">
						<div id="result-area">
							<div id="divGrid" data-bind="visible: showingGrid">
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="pnlTanimlar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" id="modalContent">


		</div>
	</div>
</div>

<!--end::Modal-->

<input type="hidden" id="saphira_csrf" value="'${_csrf.token}'">
<script id="islemlerTemplate" type="text/x-kendo-template">
	<button type="button" class="btn btn-info btn-sm btn-icon btnEdit" title="Düzenle" data-id="#: id #" id="btn_#:id#" onclick="Edit('#:id#');" ><i class="fa fa-edit"></i></button>
</script>
<script id="periyodikIslemlerTemplate" type="text/x-kendo-template">
	<button type="button" class="btn btn-info btn-sm btn-icon btnDel" title="Sil" data-id="#: id #" id="btn_#:id#" onclick="Del('#:id#');" ><i class="fa fa-times"></i></button>
</script>

<script>


var activeGrid="";
var loadMsg='<div class="blockui " style="margin-left:-84px; -webkit-box-shadow:0 0 20px blue; -moz-box-shadow: 0 0 20px blue;box-shadow:0 0 20px #93a2dd ;"><span>Yükleniyor...</span><span><div class="kt-spinner kt-spinner--v2 kt-spinner--primary "></div></span></div>';
var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': loadMsg});
var model = kendo.observable ({
	refreshing: false,
	showingGrid: function() {
		return !this.get("refreshing");
	} 
});
kendo.bind("#result-area", model);

var grid,
    initKurumlarGrid = function () {
		 grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},
			
				{
					field:		"durum",
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		85,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsKurumlar,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			editable: false,
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	},
	initAltKurumlarGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",
				
					title:		"Adı",
					groupable:	false
				},
				{
					field:		"kurumadi",
			
					title:		"Kurum",
					groupable:	false
				},
				{
					field:		"durum",
				
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		85,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsAltKurumlar,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initAramaKonusuGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},
				{
					field:		"durum",

					title:		"Durum",
					groupable:	false
				},
				{
					field:		"kurumadi",
					title:		"Kurum",
					groupable:	true
				},
				{
					field:		"konuadi",
					title:		"Alt Kurum",
					groupable:	true
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		85,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsAramaKonusu,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initSubelerGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},
				{
					field:		"kurum",

					title:		"Kurum",
					groupable:	false
				},
				{
					field:		"il",

					title:		"Şehir",
					groupable:	false
				},
				{
					field:		"havuz_sayisi",

					title:		"Havuz Sayısı",
					groupable:	false
				},
				{
					field:		"durumm",

					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		85,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsSube,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initHavuzlarGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},
				{
					field:		"konu",

					title:		"Kurum",
					groupable:	false
				},
				{
					field:		"iladi",

					title:		"Şehir",
					groupable:	false
				},
				{
					field:		"sube",

					title:		"Şube",
					groupable:	false
				},
				{
					field:		"kullanici_sayisi",

					title:		"Kullanıcı Sayısı",
					groupable:	false
				},
				{
					field:		"havuz_durum",
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),

					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsHavuzlar,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initAramaKanaliGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},

				{
					field:		"durumu",
	
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsAramaKanali,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initEgitimDurumlariGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},

				{
					field:		"durumu",
	
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsEgitim,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	
	initGuvenlikGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},

				{
					field:		"durumu",
	
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsGuvenlik,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initIlGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},

				{
					field:		"durumu",
	
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsIller,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initIlceGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},

				{
					field:		"durumu",
	
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsIlceler,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initUlkelerGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					"field": "id",
					"filterable": false, "groupable": false, "title": "#",width:75
				},
				{
					field:		"adi",

					title:		"Adı",
					groupable:	false
				},

				{
					field:		"durumu",
	
					title:		"Durum",
					groupable:	false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsUlkeler,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			"filterable": false,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	
	initDahiliGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					field:		"dahili",
					title:		"Dahili",
					groupable:	false,
					filterable:false
				},
				{
					field:		"sahibi",
					title:		"Agent",
					groupable:	false,
					filterable:false
				},
				{
					field:		"lokasyonAdi",
					title: 		"Lokasyon",
					filterable: {
                    	extra: false,
						ui: lokasyonFilter,
						operators: {
							string: {
								eq: "Eşittir",
							},
							
						}
                	}
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsDahili,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			filterable:true,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	
	initKkoBaslikGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					field:		"adi",
					title:		"Kriter",
					groupable:	false,
					filterable:false
				},
				{
					field:		"durum",
					title:		"Durum",
					groupable:	false,
					filterable:false
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsKkoBaslik,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			filterable:true,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	initKkoSoruGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					field:		"adi",
					title:		"Kriter",
					groupable:	false,
					filterable:false
				},
				{
					field:		"durum",
					title:		"Durum",
					groupable:	false,
					filterable:false,
					width:100
				},
				{
					field:		"puan",
					title:		"Puan",
					groupable:	false,
					filterable:false,
					width:100
				},
				{
					template:kendo.template($("#islemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsKkoSoru,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			filterable:true,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}	

	initPeriyodikGrid=function(){
		grid = $("#divGrid").kendoGrid({
			"columns": [
				{
					field:		"email",
					title:		"E-Mail",
					groupable:	false,
					filterable:false
				},
				{
					template:kendo.template($("#periyodikIslemlerTemplate").html()),
					width:		100,
					title:		"İşlemler",
					groupable:	false
				},
							
			],
			"autoBind": true,
			"dataSource":dsPeriyodik,
			"scrollable": true,
			"sortable": false,
			"persistSelection": true,
			filterable:true,
			"reorderable": true,
			"resizable": true,
			"columnMenu": false,
			"groupable": false,
			"navigatable": false,
			"editable": "false",
			"pageable": {"alwaysVisible": true, "pageSize": 100,"responsive": false,"input": true,"refresh": true, "info":true},
			"height":800,
		}).data("kendoGrid");
	}		
	function lokasyonFilter(element) {
            element.kendoDropDownList({
				dataTextField: "adi",
                dataValueField: "id",
				dataSource: {
					transport: {
						read: {
							type: "POST",
							dataType: "json",
							url: "/ayarlar/tnmLokasyonlar",
							beforeSend: function(req) {
                				req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
           					}
						}
					}
                },
                optionLabel: "--Lokasyonlar--"
            });
        }
	initKurumlarGrid();


	$(".tanimlar_list a.widgetItem").unbind().click(function(e){
		e.preventDefault();
		var grid = $("#divGrid").data("kendoGrid");
		var isactive=$(this).hasClass("kt-widget__item--active");
		
		if(isactive==false){
		var islem=$(this).attr("data-islem");
		activeGrid=islem;
		$(".tanimlar_list a").each(function(){
			$(this).removeClass("kt-widget__item--active");
		})
		$(this).addClass("kt-widget__item--active");
	//	loading.show();
		switch (islem) {
			case "kurumlar":
				grid.destroy();
        		$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initKurumlarGrid();
			$("#adr").html(dsKurumlar.transport.options.read.url);
				break;
			case "altkurumlar":
				grid.destroy();
        		$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initAltKurumlarGrid();
			$("#adr").html(dsAltKurumlar.transport.options.read.url);
				break;
			case "aramakonusu":
			grid.destroy();
        		$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initAramaKonusuGrid();
				$("#adr").html(dsAramaKonusu.transport.options.read.url);
				break;
			case "subeler":
				grid.destroy();
				$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initSubelerGrid();
			$("#adr").html(dsSube.transport.options.read.url);
				break;
			case "havuzlar":
				grid.destroy();
				$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initHavuzlarGrid();
			$("#adr").html(dsHavuzlar.transport.options.read.url);
				break;
			case "aramakanali":
				grid.destroy();
				$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initAramaKanaliGrid();
			$("#adr").html(dsAramaKanali.transport.options.read.url);
				break;
			case "egitim":
				grid.destroy();
				$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initEgitimDurumlariGrid();
			$("#adr").html(dsEgitim.transport.options.read.url);
				break;
			case "guvenlik":
			
			grid.destroy();
				$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initGuvenlikGrid();
			$("#adr").html(dsGuvenlik.transport.options.read.url);
				break;
			case "iller":
			$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initIlGrid();
			$("#adr").html(dsIller.transport.options.read.url);
				break;
			case "ilceler":
			$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initIlceGrid();
			$("#adr").html(dsIlceler.transport.options.read.url);
				break;
			case "ulkeler":
			$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initUlkelerGrid();
			$("#adr").html(dsUlkeler.transport.options.read.url);
				break;
			case "dahili":
			$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initDahiliGrid();
			$("#adr").html(dsDahili.transport.options.read.url);
				break;
			case "kkoBaslik":
			$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initKkoBaslikGrid();
			$("#adr").html(dsKkoBaslik.transport.options.read.url);
				break;
			case "kkoSoru":
			$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initKkoSoruGrid();
			$("#adr").html(dsKkoSoru.transport.options.read.url);
				break;
				case "Periyodik":
				$("#divGrid").empty();
				kendo.unbind("#result-area", model);
        		model.set("refreshing", true);
        		model.set("refreshing", false);
				kendo.bind("#result-area", model);
				initPeriyodikGrid();
			$("#adr").html(dsPeriyodik.transport.options.read.url);
				break;
		
			default:
				break;
		}
	}
	});


	function Edit(id){
		var url=grid.options.dataSource.options.transport.read.url;
		
		$.ajax({
		type 	: 'POST',
		url		: url+"/Edit",
		data	: "id="+id,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlTanimlar').modal({
				keyboard: false,
				backdrop:"static"
			})
		},
		error: function (request, status, error) {
			$('#modalContent').html(request.responseText);	
			$('#pnlTanimlar').modal({
				keyboard: false,
				backdrop:"static"
			})
			//alertify.error(request.responseText)
		}
	});	
	}
	function Del(id){
		var url=grid.options.dataSource.options.transport.read.url;
		
		$.ajax({
		type 	: 'POST',
		url		: url+"/Del",
		data	: "id="+id,
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(e){
			alertify.success("Silme İşlemi Başlarılı.");
			$("#divGrid").data("kendoGrid").dataSource.read();
		},
		error: function (request, status, error) {
			alertify.error(request.responseText)
		}
	});	
	}

	function Add(){
		var url=grid.options.dataSource.options.transport.read.url;
		$.ajax({
		type 	: 'POST',
		url		: url+"/Add",
		data	: "",
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(e){
			$('#modalContent').html(e);	
			$('#pnlTanimlar').modal({
				keyboard: false,
				backdrop:"static"
			})
		},
		error: function (request, status, error) {
			$('#modalContent').html(request.responseText);	
			$('#pnlTanimlar').modal({
				keyboard: false,
				backdrop:"static"
			})
			//alertify.error(request.responseText)
		}
	});	
	}
	$("#btnAdd").unbind().click(function(){
		Add();
	});

</script>
<jsp:include page="../footer.jsp" />
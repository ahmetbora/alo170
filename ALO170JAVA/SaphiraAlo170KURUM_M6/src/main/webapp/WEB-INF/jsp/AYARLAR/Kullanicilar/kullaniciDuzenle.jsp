<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>

<jsp:include page="../header.jsp" />

<div class="m-content">
	<div class="row">
		<div class="col-md-12">
			<div class="m-portlet m-portlet--tabs"  id="yeniKullaniciCont">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								KULLANICI DÜZENLE
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="m-wizard m-wizard--1 m-wizard--success" id="m_wizard">
						<!--begin: Form Wizard Head -->
						<div class="m-wizard__head m-portlet__padding-x">
				
							<!--begin: Form Wizard Progress -->
							<div class="m-wizard__progress">
								<div class="progress">
									<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
				
							<!--end: Form Wizard Progress -->
				
							<!--begin: Form Wizard Nav -->
							<div class="m-wizard__nav">
								<div class="m-wizard__steps">
									<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
										<div class="m-wizard__step-info">
											<a href="#" class="m-wizard__step-number"  data-step="1">
												<span><span>1</span></span>
											</a>
											<div class="m-wizard__step-line">
												<span></span>
											</div>
											<div class="m-wizard__step-label">
												Kullanıcı Bilgileri
											</div>
										</div>
									</div>
									<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
										<div class="m-wizard__step-info">
											<a href="#" class="m-wizard__step-number" data-step="2">
												<span><span>2</span></span>
											</a>
											<div class="m-wizard__step-line">
												<span></span>
											</div>
											<div class="m-wizard__step-label">
												Havuz Bilgileri
											</div>
										</div>
									</div>
									<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
										<div class="m-wizard__step-info">
											<a href="#" class="m-wizard__step-number" data-step="3">
												<span><span>3</span></span>
											</a>
											<div class="m-wizard__step-line">
												<span></span>
											</div>
											<div class="m-wizard__step-label">
												Kullanıcı Yetki İşlemleri
											</div>
										</div>
									</div>

								</div>
							</div>
				
							<!--end: Form Wizard Nav -->
						</div>
				
						<!--end: Form Wizard Head -->	
						<!--begin: Form Wizard Form-->
						<div class="m-wizard__form">
							<form class="m-form m-form--fit m-form--label-align-right " id="m_form" autocomplete="off">
				
								<!--begin: Form Body -->
								<div class="m-portlet__body">
									<!--begin: Form Wizard Step 1-->
									<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
										<jsp:include page="frmKullaniciDuzenle.jsp" />
									</div>
									<!--end: Form Wizard Step 1-->
				
									<!--begin: Form Wizard Step 2-->
									<div class="m-wizard__form-step" id="m_wizard_form_step_2">
										<jsp:include page="frmHavuzDuzenle.jsp" />																																																																	
									</div>
									<!--end: Form Wizard Step 2-->
				
									<!--begin: Form Wizard Step 3-->
									<div class="m-wizard__form-step" id="m_wizard_form_step_3">
										<jsp:include page="frmYetkiDuzenle.jsp" />						 								 								 									 									 									 									 									 										
									</div>
									<!--end: Form Wizard Step 3-->
				
								</div>
				
								<!--end: Form Body -->
				
				
							</form>
						</div>
				
						<!--end: Form Wizard Form-->										
					</div>	
				</div>
				<div class="m-portlet__foot">
				
					<button class="btn btn-outline-success m-btn m-btn--custom m-btn--icon btn-md float-right" id="btnKullaniciKaydet" style="float:right!important;display:none">
						<span>
							<span>Kaydet </span>&nbsp;&nbsp;
							<i class="fa fa-save"></i>
						</span>
					</button>				
					<button class="btn btn-outline-info m-btn m-btn--custom m-btn--icon btn-md float-right" id="btn_next" style="float:right!important">
						<span>
							<span>İlerle </span>&nbsp;&nbsp;
							<i class="la la-arrow-right"></i>
						</span>
					</button>
					<button class="btn btn-outline-info m-btn m-btn--custom m-btn--icon  btn-md float-right" id="btn_prev" data-wizard-action="prev" style="margin-right:50px">
						<span>
							<i class="la la-arrow-left"></i>&nbsp;&nbsp;
							<span>Önceki</span>
						</span>
					</button>	
					<span class="clearfix"></span>				
				</div>				
			</div>	
				
		</div>
	</div>
</div>
<input type="hidden" id="checkp" value="0">
 
<script>

var token = '${_csrf.token}';
var header ='${_csrf.headerName}';

var options = {
		startStep: 1,
		manualStepForward: false
	};
	var bildirimWizard = new mWizard('m_wizard',options).
	on("change", function(r) {
		var ls=bildirimWizard.isLastStep();
		var fs=bildirimWizard.isFirstStep();
		var currentStep=bildirimWizard.getStep();
		if(ls==true){
			$("#btn_next").hide();
			$("#btnKullaniciKaydet").show();
		}else{
			$("#btn_next").show();
			$("#btnKullaniciKaydet").hide();
		}
		if(fs==true){
			$("#btn_prev").hide();
			$("#btnKullaniciKaydet").hide();
		}else{
			$("#btn_prev").show();
		}		
	});

	var fs=bildirimWizard.isFirstStep();
	if(fs==true){
		$("#btn_prev").hide();
	}else{
		$("#btn_prev").show();
	}	
	
	$("#btn_next").click(function(e){
		e.preventDefault();
		var currentStep=bildirimWizard.getStep();
		if(currentStep=="1"){
			checkStep1()
		}
		if(currentStep=="2"){
			checkStep2()
		}
		if(currentStep=="3"){
			
			checkStep3()
		}

		
	});

	$("#btn_prev").click(function(){
		bildirimWizard.goPrev();
	});
	$(".m-wizard__step-number").click(function(e){
		e.preventDefault();
		var step=$(this).attr("data-step");
		var currentStep=bildirimWizard.getStep();
		
	});

	function checkStep1(){
		var username=$("#username").val();
		var ext=$("#ext").val();
		var email=$("#email").val();
		var password=$("#password").val();
		var password_t=$("#password_t").val();
		var adi=$("#adi").val();
		var gsm = $("#gsm").val();
		var face = $("#face").val();
		var gsm = $("#twit").val();
		var tckimlik = $("#tckimlik").val();
		var ksicil = $("#ksicil").val();
		var emsicil = $("#emsicil").val();
		var email_bildirim=$("#email_bildirim option:selected").val();
		
		if(adi==""){
			alertify.error("İsim Soy İsim Giriniz");
			$("#adi").focus();
			return false;
		}
		if(username==""){
			alertify.error("Kullanıcı Adı Giriniz");
			$("#username").focus();
			return false;
		}


		
		if(password!=""){
			if(checkPassword(password)!="1"){
				alertify.error("Şifreniz en az 9 karakter, 1 büyük harf, 1 küçük harf ve en az 1 özel karakter içermelidir.");
				$("#password").focus();
				return false;
			}
			if(password!=password_t){
				alertify.error("Şifreler Uyuşmuyor");
				$("#password").focus();
				return false;
			}				
		}

	
		
	
		bildirimWizard.goNext();
	}
	
	function checkStep2(){
		bildirimWizard.goNext();
	}
	
	function checkStep3(){
		bildirimWizard.goNext();
	}



	
$("#btnKullaniciKaydet").click(function(){
	var username=$("#username").val();
	var ext=$("#ext").val();
	var email=$("#email").val();
	var password=$("#password").val();
	var password_t=$("#password_t").val();
	var adi=$("#adi").val();
	var gsm = $("#gsm").val();
	var face = $("#face").val();
	var gsm = $("#twit").val();
	var tckimlik = $("#tckimlik").val();
	var ksicil = $("#ksicil").val();
	var emsicil = $("#emsicil").val();
	var fk_rol=$("#fk_rol option:selected").val();
	var fk_lokasyon=$("#fk_lokasyon option:selected").val();
	var email_bildirim=$("#email_bildirim option:selected").val();
	var isActive=$("#isActive option:selected").val();
	if(adi==""){
		alertify.error("İsim Soy İsim Giriniz");
		$("#adi").focus();
		return false;
	}
	if(username==""){
		alertify.error("Kullanıcı Adı Giriniz");
		$("#username").focus();
		return false;
	}
	if(password==""){
		alertify.error("Şifre Giriniz");
		$("#password").focus();
		return false;
	}
	if(password!=password_t){
		alertify.error("Şifreler Uyuşmuyor");
		$("#password").focus();
		return false;
	}

	if(checkPassword(password)!="1"){
		alertify.error("Şifreniz en az 9 karakter, 1 büyük harf, 1 küçük harf ve en az 1 özel karakter içermelidir.");
		$("#password").focus();
		return false;
	}
	
	if(fk_rol=="" || fk_rol=="0"){
		alertify.error("Kullanıcı Rolünü Seçin");
		$("#fk_rol").focus();
		return false;
	}
	if(fk_rol=="1" || fk_rol=="2" || fk_rol=="4" || fk_rol=="11"){
		if(fk_lokasyon=="" || fk_lokasyon=="0"){
			alertify.error("Kullanıcı Lokasyonu Seçin");
			$("#fk_lokasyon").focus();
			return false;
		}
	}


		var yetkiler="";
		var i=0;
		$("input.chk_yetki").each(function(){
			i++;
			var checked=$(this).prop("checked");
			var id=$(this).attr("data-id");
			if(checked==true){
				if(i==1){
					yetkiler +=id;
				}else{
					yetkiler +=","+id;
				}
			}
		});
		
		var gridData=eklenenDataSource.data();
		var i;
		var havuzlar="";
		for (i = 0; i < gridData.length; i++) { 
			if(i==0){
				havuzlar +=gridData[i].id;
			}else{
				havuzlar +=","+gridData[i].id;
			}
		}
		

		displayLoading("body");
	$.ajax({
		type	: 'POST',
		url		: '/ayarlar/kullanici/kullaniciKaydet',
		data	: 'adi='+adi+'&email='+email+'&password='+password+'&password_t='+password_t+'&adi='+adi+'&username='+username+'&gsm='+gsm+'&ext='+ext+'&fk_rol='+fk_rol+'&tckimlik='+tckimlik+'&emsicil='+emsicil+'&ksicil='+ksicil+'&fk_lokasyon='+fk_lokasyon+'&email_bildirim='+email_bildirim+"&havuzlar="+havuzlar+"&yetkiler="+yetkiler+'&uuid=${uuid}&IsActive='+isActive,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success : function(r){
			//hideLoading("body");
			alertify.set({ labels: {
			    ok     : "Tamam",
			} });
			alertify.alert("Kullanıcı Eklendi.");
			setTimeout(function(){
				location.reload();
			}, 2000);
		},
		error: function (request, status, error) {
			alertify.error("Kullanıcı Eklenirken Sorun Oluştu")
			hideLoading("body");
		}		
	});		
});


</script>

    
<jsp:include page="../footer.jsp" />
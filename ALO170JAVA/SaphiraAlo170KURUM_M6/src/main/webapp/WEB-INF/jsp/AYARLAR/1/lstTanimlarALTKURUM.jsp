<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Tanımlar </h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Ayarlar </a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Tanımlar </a>
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
			<div class="kt-subheader__toolbar d-none">
				<div class="kt-subheader__wrapper">
					<a href="javascript:void(0);" class="btn kt-subheader__btn-primary">
						Actions &nbsp;
	
						<!--<i class="flaticon2-calendar-1"></i>-->
					</a>
					<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="left">
						<a href="javascript:void(0);" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
									<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" id="Combined-Shape" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" id="Combined-Shape" fill="#000000" />
								</g>
							</svg>
	
							<!--<i class="flaticon2-plus"></i>-->
						</a>
						<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
	
							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Add anything or jump to:
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="javascript:void(0);" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-drop"></i>
										<span class="kt-nav__link-text">Order</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="javascript:void(0);" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
										<span class="kt-nav__link-text">Ticket</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="javascript:void(0);" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Goal</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="javascript:void(0);" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support Case</span>
										<span class="kt-nav__link-badge">
											<span class="kt-badge kt-badge--success">5</span>
										</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-brand btn-bold btn-sm" href="javascript:void(0);">Upgrade plan</a>
									<a class="btn btn-clean btn-bold btn-sm" href="javascript:void(0);" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more...">Learn more</a>
								</li>
							</ul>
	
							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Subheader -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">	
			<jsp:include page="tanimlarMenu.jsp" />
			<div class="col-md-12 col-lg-9">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title kt-font-primary">
								ALT KURUMLAR
							</h3>

						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="form-group mb-0">
								<a href="javascript:void(0);" class="btn btn-primary btn-sm btn-ekle"><i class="la la-plus"></i> Yeni Ekle</a>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body p-3">
						<div class="form-group m-form__group">
							<select name="fk_konu" id="fk_konu" class="form-control m-select2" style="width:100%">
								<option value='0'>-- TÜM KURUMLAR --</option>
								<c:forEach items="${kurumlar}" var="n">
								<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
								</c:forEach>
							</select>
					 </div>
				 
					 <div class="row" id="altkurumFiltre">
						 <table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
							 <thead>
							 <tr>
								 <th>#</th>
								 <th>Adı</th>
								 <th>Kurum</th>
									<th class="text-right">Durum</th>
									<th class="text-right">İşlemler</th>
							 </tr>
							 </thead>
							 <tbody>
							 <c:forEach items="${list}" var="l" >
								 <tr>
									 <td> ${l.id } </td>
									 <td> ${l.adi } </td>
									 <td> ${l.kurumadi } </td>
									 <td class="text-right">
									 <c:if test="${l.isActive == 0}"><span class="m--font-danger">Pasif</span></c:if>
									 <c:if test="${l.isActive ne 0}"><span class="m--font-bold">Aktif</span></c:if>
									 </td>
									 <td class="text-right"> 
									 <a href="javascript:void(0);" class="btn btn-primary btn-sm btn-icon btn-circle btn-edit" data-id="${l.id}">
									 <i class="la la-search"></i>
									 </a>
									 </td>
								 </tr>
							 </c:forEach>
							 </tbody>
						 </table>
					</div>  
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="pnlAltKurum" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" id="akeklebody">

		</div>
	</div>
</div>

<script>
function AltKurumEkle(id){
	$.get("/ayarlar/tanimlarAltKurum/" +id ,function(data) {
		$('#akeklebody').html(data);
		$('#pnlAltKurum').modal({
			keyboard: false,
			backdrop:"static"
		})
	});
}

var _csrf_token = '${_csrf.token}' ;
var _csrf_param_name = '${_csrf.parameterName}' ;

$(".btn-ekle").unbind().click(function(e){
	e.preventDefault();
	AltKurumEkle(0);
});
$(".btn-edit").unbind().click(function(e){
	e.preventDefault();
	var id=$(this).attr("data-id");
	AltKurumEkle(id);
});	
$(document).ready(function(){
	$(function () {
		var token = $("input[name='_csrf']").val();
		var header = "X-CSRF-TOKEN";
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, _csrf_token);
		});
	});
			
	
	$.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

	$("#fk_konu").change(function() {
		
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		
		var headers = {};
		headers[csrfHeader] = csrfToken;
		
	    $.ajax({
	        type: 'POST',
	        url: '/ayarlar/AltKurumlarFiltre',
	        data: 
	        {
                id: $('#fk_konu').val()
                
            },
	        success: function(response)
	        {
	        	$('#altkurumFiltre').html(response);
				$(".btn-edit").unbind().click(function(e){
					e.preventDefault();
					var id=$(this).attr("data-id");
					AltKurumEkle(id);
				});	
	        }
	    });

	});
});
</script>

<jsp:include page="footer.jsp" />
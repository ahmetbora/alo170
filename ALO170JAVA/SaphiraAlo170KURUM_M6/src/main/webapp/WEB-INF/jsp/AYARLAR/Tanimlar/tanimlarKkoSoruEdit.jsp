<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="modal-header" id="moda_header">
    <h5 class="modal-title" id="modalLabel">KKO ALT KRİTER BİLGİSİ GÜNCELLE / EKLE</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body" id="modal_body">
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Durumu:</label>
            <select class="form-control" id="isActive">
                <c:forEach items="${aktifpasiflist}" var="n">					
                    <option data-id="${n.adi}" value="${n.value}" <c:if test="${n.value eq kko.isActive}"> selected</c:if>>${n.adi}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Ana Kriter:</label>
            <select class="form-control" id="fk_parent">
                <c:forEach items="${kkoparent}" var="n">					
                    <option data-id="${n.adi}" value="${n.id}" <c:if test="${n.id eq kko.fk_parent}"> selected</c:if>>${n.adi}</option>
                </c:forEach>
            </select>
        </div>
         
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Soru:</label>
            <input type="text" class="form-control" id="txtAdi" value="${kko.adi}">
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Puan:</label>
            <input type="text" class="form-control" id="puan" value="${kko.puan}">
        </div>
  
</div>
<div class="modal-footer" id="modal_footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
    <button type="button" class="btn btn-primary" id="btn_modal_kaydet">Kaydet</button>
</div>
<input type="hidden" id="id" value="${kko.id}">
<script>
var token = '${_csrf.token}';
var header ='${_csrf.headerName}';
$('#pnlBildirimDetay').on('hidden.bs.modal', function (e) {
  $("#modalContent").html('');
  $(this).off('hidden.bs.modal');
})
$("#btn_modal_kaydet").unbind().click(function(){
    var adi=$("#txtAdi").val();
    var id=$("#id").val();
    var puan=$("#puan").val();
    var isActive=$("#isActive option:selected").val();
    var fk_parent=$("#fk_parent option:selected").val();
    if(adi==""){
        alertify.error("Adı Alanı Boş Bırakılamaz.");
        return false;
    }
    if(puan==""){
        alertify.error("Puan Alanı Boş Bırakılamaz.");
        return false;
    }

    $.ajax({
		type 	: 'POST',
		url		: "/ayarlar/tanimlar/Update",
		data	: "cols=adi-splitter-isActive-splitter-puan-splitter-fk_parent&id="+id+"&vals="+adi+"-splitter-"+isActive+"-splitter-"+puan+"-splitter-"+fk_parent+"&tbl=kko_sorular&dName=pbx&splitter=-splitter-",
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(e){
            alertify.success("Tanım Güncellendi.")
            $("#divGrid").data("kendoGrid").dataSource.read();
            $('#pnlTanimlar').modal("hide");
		},
		error: function (request, status, error) {
			alertify.error("Tanım Güncellemesi Sırasında Hata.")
		}
	});
})		

</script>

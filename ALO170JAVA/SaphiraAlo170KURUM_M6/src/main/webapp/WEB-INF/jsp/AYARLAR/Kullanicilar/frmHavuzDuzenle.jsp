<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>

<jsp:include page="../header.jsp" />
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
	<!-- begin:: Subheader -->
	<div class="kt-subheader   kt-grid__item" id="kt_subheader">
		<div class="kt-container  kt-container--fluid ">
			<div class="kt-subheader__main">
				<h3 class="kt-subheader__title">
					Raporlar</h3>
				<span class="kt-subheader__separator kt-hidden"></span>
				<div class="kt-subheader__breadcrumbs">
					<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
					<span class="kt-subheader__breadcrumbs-separator"></span>
					<a href="javascript:void(0);" class="kt-subheader__breadcrumbs-link">
						Raporlar </a>
				
	
					<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
				</div>
			</div>
	
		</div>
	</div>
	<!-- end:: Subheader -->
		<!-- begin:: Content -->
	<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
			<div class="col-md-12 col-lg-4">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								HAVUZ TANIMLAMALARI
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body">
						<form class="m-form m-form--fit m-form--label-align-right">		
							<div class="form-group m-form__group">
								<label for="tip_level1">KURUM</label>
								<select id="tip_level1"  style="width:100%">
									
								</select>
							</div>		
							<div class="form-group m-form__group">
								<label for="fk_il">ŞEHİR</label>
								<select id="fk_il" style="width:100%">
									
								</select>			    		
							</div>	
							<div class="form-group m-form__group">
								<label for="fk_sube">ŞUBELER</label>
								<select id="fk_sube"  style="width:100%">
									
								</select>
							</div>	
							<div class="form-group m-form__group">
								<label for="fk_havuz">HAVUZLAR</label>
								<select id="fk_havuz" style="width:100%">
									
								</select>
							</div>																						    					
						</form>
					</div>
					<div class="kt-portlet__foot">
						<a href="javascript:void(0);" class="btn btn-brand btn-block" id="btnHavuzEkle"><i class="fa fa-plus"></i> Havuz Ekle</a>
					</div>
				</div>				
			</div>
			<div class="col-md-12 col-lg-8">
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Tanımlanmış Havuzlar
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-actions">
								<button class="btn btn-brand btn-sm " id="btnUserHavuzGuncelle"><i class="fa fa-save"></i> Değişiklikleri Kaydet</button>
							</div>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="alert alert-metal alert-dismissible fade show   m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert">
							<strong>${UserName} / ${User.adi}</strong> Kullanıcısının havuzları düzenleniyor.	
						</div>				
									
						<div id="eklenenGrid"></div>
					</div>
				</div>				
			</div>
		</div>
	</div>

</div>

<script id="islemlerTemplate" type="text/x-kendo-template">
	<button type="button" class="btn btn-info btn-sm btnHavuzSil" onclick="havuzSil('#: id #','#: uid#');"><i class="fa fa-times"></i></button>
</script>

<script>

var token = '${_csrf.token}';
var header ='${_csrf.headerName}';

// data sources
function getKurumData(){
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    data: JSON.stringify({
	        term: ''
	    }),
	    url: "/ayarlar/kurumlar",
	    contentType: "application/json; charset=utf-8",
	    beforeSend: function(req) {
	        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	    },
	    success: function (result) {
	    	var combobox = $("#tip_level1").data("kendoDropDownList");
	    	combobox.setDataSource(result.data);
	    }
	});	
}
getKurumData();



function getSehirData(){
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    data: JSON.stringify({
	        term: ''
	    }),
	    url: "/ayarlar/sehirler",
	    contentType: "application/json; charset=utf-8",
	    beforeSend: function(req) {
	        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	    },
	    success: function (result) {
	    	var combobox = $("#fk_il").data("kendoDropDownList");
	    	combobox.setDataSource(result.data);
	    }
	});	
}
getSehirData();


function getSubeData(sehir,kurum){
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    data: JSON.stringify({
	        fk_il: sehir,
	        tip_level1:kurum
	    }),
	    url: "/ayarlar/subeler",
	    contentType: "application/json; charset=utf-8",
	    beforeSend: function(req) {
	        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	    },
	    success: function (result) {

	    	var combobox = $("#fk_sube").data("kendoDropDownList");
	    	combobox.setDataSource(result.data);
	   
	    	 
	    	combobox.trigger("change"); //this will trigger change event
	    }
	});	
}


function getHavuzData(sube,ids){
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    data: JSON.stringify({
	        fk_sube: sube,
	        ids:ids
	    }),
	    url: "/ayarlar/havuzlar",
	    contentType: "application/json; charset=utf-8",
	    beforeSend: function(req) {
	        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	    },
	    success: function (result) {

	    	var combobox = $("#fk_havuz").data("kendoDropDownList");
	    	combobox.setDataSource(result.data);
	    }
	});	
}

// dropdown events
function kurumOnChange(){
	var sehir=$("#fk_il").val();
	var kurum=$("#tip_level1").val();
	if(sehir>0 && kurum>0){
		getSubeData(sehir,kurum)
	}
}
function sehirOnChange(){
	var sehir=$("#fk_il").val();
	var kurum=$("#tip_level1").val();
	if(sehir>0 && kurum>0){
		getSubeData(sehir,kurum)
		
	}
}

function subeOnChange(){
	var sube=$("#fk_sube").val();
	var ids="";
	//if(sube>0){
	    var gridData=eklenenDataSource.data();
    	var i;
    	for (i = 0; i < gridData.length; i++) { 
    	//	ids += gridData[i].fkHavuz + ",";
    	}
		getHavuzData(sube,ids);
	//}
}



//set dropdowns
function initKurumCombo(){
	var kurumCombo=$("#tip_level1").kendoDropDownList({
		dataTextField: "adi",
		dataValueField: "id",
		optionLabel: {
		    adi: "Kurum Seçin...",
		    id: "0"
		},
		change:kurumOnChange
	});
}
function initSehirCombo(){
	var sehirCombo=$("#fk_il").kendoDropDownList({
		dataTextField: "adi",
		dataValueField: "id",
		optionLabel: {
		    adi: "Şehir Seçin...",
		    id: "0"
		},
		change:sehirOnChange
	});
}
function initSubeCombo(){
	var subeCombo=$("#fk_sube").kendoDropDownList({
		dataTextField: "adi",
		dataValueField: "id",
		optionLabel: {
		    adi: "Şube Seçin...",
		    id: "0"
		},
		change:subeOnChange
	});
}
function initHavuzCombo(){
	var havuzCombo=$("#fk_havuz").kendoDropDownList({
		dataTextField: "adi",
		dataValueField: "id",
		optionLabel: {
		    adi: "Havuz Seçin...",
		    id: "0"
		}
	});
}

initKurumCombo();
initSehirCombo();
initSubeCombo();
initHavuzCombo();




    var crudServiceBaseUrl="/ayarlar/kullanici/userHavuz";
    eklenenDataSource = new kendo.data.DataSource({
        transport: {
            read:  {
                type: "POST",
                url: crudServiceBaseUrl,
                contentType: "application/json; charset=utf-8",            
    			dataType: 'json',
                beforeSend: function(req) {
                    req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
                }
            },
            
            parameterMap: function(options, operation) {
                return kendo.stringify(options);
                if (operation !== "read" && options.models) {
                    //return {models: kendo.stringify(options.models)};
                }
            }
        },
  
        batch: false,
        pageSize: 100,
        autoSync: false,
        "serverFiltering": true,
        schema: {
            data:'data', 
            model: {
                id: "id",
                fields: {
                    "id"            	:   { editable: false, nullable: true },
                    "adi"            	:   { editable: false, nullable: true },
                    "userHavuzId"       :   { editable: false, nullable: true },

                }
            },
            "total": "total"
        },

    });

    function eklenenDataSourceDsFilter(fk_user){
    	eklenenDataSource.filter([
                {
                    field: "fk_user",
                    operator: "eq",
                    value: fk_user
                }          
            ]
        );
    }
    eklenenDataSourceDsFilter("${userId}");
    
    var eklenenGrid = $("#eklenenGrid").kendoGrid({
        "columns": [
            {
                "field": "fkHavuz",
                "filterable": false, "groupable": false, "title": "ID",
				width:75
            },
            {
                "field": "adi",
                "filterable": false, "groupable": false, "title": "ADI",
            },
            {
            	template:kendo.template($("#islemlerTemplate").html()),
                filterable: false,  
                title: "",
                width:50
            }, 
                       
        ],
        "dataSource":eklenenDataSource,
    });



 
    $("#btnHavuzEkle").click(function(){
    	var dropdownlist = $("#fk_havuz").data("kendoDropDownList");
    	var id=dropdownlist.value();
    	var txt=dropdownlist.text();
    	var gridData=eklenenDataSource.data();
    	const findData = gridData.find(havuz => havuz.id === id);
    	var findDataType = typeof findData;
    	if (findDataType != 'object') {
        	if(id>0){
    			eklenenDataSource.add({ fkHavuz:id, adi: txt });
        	}
        }
    	//dropdownlist.select(0);

    	var subeCombo = $("#fk_sube").data("kendoDropDownList");
    	subeCombo.select(0);
    	subeCombo.trigger("change");
    	var gridData=eklenenDataSource.data();
		//console.log(gridData);
    	
    })

function havuzSil(id,uid){
    	var row=eklenenDataSource.getByUid(uid);
    	eklenenDataSource.remove(row);
    	var data = eklenenDataSource.data();
    	var subeCombo = $("#fk_sube").data("kendoDropDownList");
    	subeCombo.select(0);
    	subeCombo.trigger("change");
    	var gridData=eklenenDataSource.data();
    
}

$("#btnUserHavuzGuncelle").click(function(){
	var gridData=eklenenDataSource.data();
	var i;
	var havuzlar="";
	for (i = 0; i < gridData.length; i++) { 
		if(i==0){
			havuzlar +=gridData[i].fkHavuz;
		}else{
			havuzlar +=","+gridData[i].fkHavuz;
		}
	}

	displayLoading("body");
	$.ajax({
		type	: 'POST',
		url		: '/ayarlar/kullanici/userHavuzGuncelle',
		data	: '&havuzlar='+havuzlar+'&uid=${uid}',
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success : function(r){
			hideLoading("body");
			
			alertify.set({ labels: {
			    ok     : "Tamam",
			} });
			if(r.sonuc=="1"){
				alertify.alert("Değişiklikler Kaydedildi.");
			}else{
				alertify.error("Havuzlar Güncellenirken Sorun Oluştu")
			}
		},
		error: function (request, status, error) {
			alertify.error("Havuzlar Güncellenirken Sorun Oluştu")
			hideLoading("body");
		}		
	});
});  

</script>
<jsp:include page="../footer.jsp" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <style>
.demo-section  li {
	margin: 0;
	padding: 10px 10px 10px 20px;
	min-height: 28px;
	line-height: 28px;
	vertical-align: middle;
	border-bottom: 1px solid rgba(128,128,128,.5);
	list-style:none
}
.demo-section li .k-switch {
	float: right;
}    

         
 </style>


<legend>Yetki Grubu Detayları Düzenle</legend>
<div class="form-group m-form__group row">
	<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Yetki Grubu Adı:</label>
	<div class="col-xl-10 col-lg-10">
		<input type="text" name="adi" id="adi" class="form-control m-input zorunlu" placeholder="" value="${yetkiGrubu.getAdi()}">
	</div>
</div>
<div class="form-group m-form__group row">
	<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Durumu:</label>
	<div class="col-xl-4 col-lg-4">
		<select class="form-control" id="isActive">
			<option value="1" <c:if test="${yetkiGrubu.getisActive()==1}"> selected </c:if> >Aktif</option>
			<option value="0" <c:if test="${yetkiGrubu.getisActive()==0}"> selected </c:if> >Pasif</option>
		</select>
	</div>
	<label class="col-xl-2 col-lg-2 col-form-label"><span class="m--font-danger">*</span> Gizli:</label>
	<div class="col-xl-4 col-lg-4">
		<select class="form-control" id="isHidden">
			<option value="1" <c:if test="${yetkiGrubu.getisHidden()==1}"> selected </c:if> >Gizli</option>
			<option value="0" <c:if test="${yetkiGrubu.getisHidden()==0}"> selected </c:if> >Normal</option>
		</select>		
	</div>
</div>
<br>
<hr>
<br>
		<c:forEach items="${yetkiAlanlari}" var="item">
		<c:if test="${item.getIsroot()==1}">
        <div id="acc${item.id}">
			<div class="kt-section">								
				<div class="kt-section__content kt-section__content--solid">
					<button type="button" class="btn btn-sm btn-brand btnSelectAll" data-id="${item.id}" data-statu="">Tümünü Seç</button>
					<button type="button" class="btn btn-sm btn-warning btnDeSelectAll d-none" data-id="${item.id}" data-statu="">Tümünü Kaldır</button>
				</div>
			</div>			
			<div class="row">
		</c:if>	
			<c:forEach items="${yetkiAlanlari}" var="subItem">
				<c:if test="${subItem.getFkroot()==item.getId()}">
				<div class="col-md-12 col-lg-5 mb-2 mt-2 " style=" margin-right: 5rem !important;">
					<i class="fa fa-question-circle ttip"  title="${subItem.aciklama}"></i>
					${subItem.adi}
					<div class="float-right">
						<input type="checkbox" id="chk_yetki_${subItem.id}" aria-label="${subItem.adi}" <c:if test="${subItem.getFkRol()>0}"> checked="checked" </c:if> class="chk_yetki" data-id="${subItem.id}"/>						
					</div>
				</div>	
				</c:if>						
			</c:forEach>
		<c:if test="${item.getIsroot()==1}">
		</div>	
		</div>	
		</c:if>	
	</c:forEach>
</div>
<script>
var tooltip = $("#yetkiGrubuDetayCont").kendoTooltip({
	filter: ".ttip",
	width: 200,
	position: "top",
	animation: {
		open: {
			effects: "zoom",
			duration: 150
		}
	}
}).data("kendoTooltip");
		var btn='<button class="btn btn-sm btn-success" id="btnYetkiGrubuKaydet"><i class="fa fa-save"></i> Kaydet</button>';
		$("#ptools").html(btn);
        $(".yetkiler").kendoSwitch({
            checked: true,
            change: function (e) {
               // console.log("event :: change (" + (e.checked ? "checked" : "unchecked") + ")");
            }
        });
        $(".chk_yetki").kendoSwitch();

        $("#btnYetkiGrubuKaydet").click(function(){
		
            var adi=$("#adi").val();
            var isActive=$("#isActive option:selected").val();
            var isHidden=$("#isHidden option:selected").val();
            var fkRol="${fkRol}";
            if(adi==""){
				alertify.error("Yetki Grubu Adını Girin.");
				return false;
            }
            if(isActive==""){
				alertify.error("Yetki Grubu Durumunu Seçin.");
				return false;
            }      
            if(isHidden==""){
				alertify.error("Yetki Grubu Gizlilik Durumunu Seçin.");
				return false;
            }  
            data="adi="+adi+"&isActive="+isActive+"&isHidden="+isHidden+"&fkRol="+fkRol+"&";
            dataChk="";
            $("input.chk_yetki").each(function(){
    			var checked = $(this).prop('checked');
    			var id=$(this).attr("data-id");
    			if(checked==true){
					checked="1";
        		}else{
					checked="0";
            	}
    			dataChk +=id+"_"+checked+",";	
    			
            });   
       

            displayLoading(document.body);
        	$.ajax({
        		type 	: 'POST',
        		url		: '/ayarlar/yetkiGuncelle',
        		data	: data+"&chks="+dataChk,
                beforeSend: function(req) {
                    req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
                },
        		success : function(e){
        			  hideLoading(document.body); 
					  alertify.success("İşlem Tamamlandı.");
        				yetkiGrubuGridDataSource.filter([
        		            {
        		                field: "tip",
        		                operator: "eq",
        		                value: 1
        		            }
        		        ]
        		    );
			
        		},
        	    error: function (request, status, error) {
        	    	hideLoading(document.body);
        	    }
        	});                   
		})
		$(".btnSelectAll").unbind().click(function(){
			var id=$(this).attr("data-id");
			var statu=$(this).attr("data-statu");
			
			$("#acc"+id+" input[type=checkbox]").each(function(){
				var chkId=$(this).attr("data-id");
				var switchInstance = $("#chk_yetki_"+chkId).data("kendoSwitch");
				var checked=switchInstance.element.attr("checked");
				if(checked!="checked"){
					switchInstance.toggle();
				}

			});
			$(this).addClass("d-none");
			$(".btnDeSelectAll[data-id="+id+"]").removeClass("d-none");
		});
		$(".btnDeSelectAll").unbind().click(function(){
			var id=$(this).attr("data-id");
			var statu=$(this).attr("data-statu");
			
			$("#acc"+id+" input[type=checkbox]").each(function(){
				var chkId=$(this).attr("data-id");
				var switchInstance = $("#chk_yetki_"+chkId).data("kendoSwitch");
				var checked=switchInstance.element.attr("checked");
				if(checked=="checked"){
					switchInstance.toggle();
				}

			});
			$(this).addClass("d-none");
			$(".btnSelectAll[data-id="+id+"]").removeClass("d-none");
		});
</script>        
<c:forEach items="${yetkiAlanlari}" var="item">
	<c:if test="${item.getIsroot()==1}">
		<script>
        $('#acc${item.id}').kendoExpansionPanel({
            title: '${item.adi}',
            subTitle: '<${item.adi} Yetkileri>',
            expanded: true
        });
		</script>
	</c:if>
</c:forEach>

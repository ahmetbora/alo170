<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="modal-header" id="moda_header">
    <h5 class="modal-title" id="modalLabel">ÜLKE BİLGİSİ GÜNCELLE / EKLE</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body" id="modal_body">
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Dahili:</label>
            <input type="text" class="form-control" disabled="disabled" value="${dahili.dahili}" id="ext">
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Adı Soyadı:</label>
            <input type="text"  class="form-control"  id="txtAdi" value="${dahili.sahibi}">
        </div>
         
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Şifre:</label>
            <input type="text" class="form-control" id="sifre" value="${dahili.password}">
        </div>
  
</div>
<div class="modal-footer" id="modal_footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
    <button type="button" class="btn btn-primary" id="btn_modal_kaydet">Kaydet</button>
</div>
<input type="hidden" id="id" value="${dahili.id}">
<script>
var token = '${_csrf.token}';
var header ='${_csrf.headerName}';
$('#pnlBildirimDetay').on('hidden.bs.modal', function (e) {
  $("#modalContent").html('');
  $(this).off('hidden.bs.modal');
})
$("#btn_modal_kaydet").unbind().click(function(){
    var adi=$("#txtAdi").val();
    var ext=$("#ext").val();
    var sifre=$("#sifre").val();
    var id=$("#id").val();
    if(adi==""){
        alertify.error("Adı Alanı Boş Bırakılamaz.");
        return false;
    }
    if(sifre==""){
        alertify.error("Şifre Boş Bırakılamaz.");
        return false;
    }
    $.ajax({
		type 	: 'POST',
		url		: "/ayarlar/tanimlar/Update",
        data	: "cols=agent_adi-splitter-password&vals="+adi+"-splitter-"+sifre+"&tbl=pbx_agent_phones"+"&id="+id+"&splitter=-splitter-",
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(e){
            alertify.success("Tanım Güncellendi.")
            $("#divGrid").data("kendoGrid").dataSource.read();
            $('#pnlTanimlar').modal("hide");
		},
		error: function (request, status, error) {
			alertify.error("Tanım Güncellemesi Sırasında Hata.")
		}
	});
})		

</script>

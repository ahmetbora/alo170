<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
    <thead>
    <tr>
        <th>#</th>
        <th>Adı</th>
        <th>Adı</th>
        <th>Adı</th>
       <th class="text-right">Durum</th>
       <th class="text-right">İşlemler</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${list}" var="l" >
        <tr>
           <td> ${l.id } </td>
           <td> ${l.adi } </td>
           <td> ${l.kurumadi } </td>
           <td> ${l.konuadi } </td>
           <td class="text-right">
               <c:if test="${l.isActive == 0}"><span class="m--font-danger">Pasif</span></c:if>
               <c:if test="${l.isActive ne 0}"><span class="m--font-bold">Aktif</span></c:if>
           </td>
            <td class="text-right"> 
               <a href="javascript:void(0);" class="btn btn-primary btn-sm btn-icon btn-circle btn-edit" data-id="${l.id}">
                   <i class="la la-search"></i>
               </a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
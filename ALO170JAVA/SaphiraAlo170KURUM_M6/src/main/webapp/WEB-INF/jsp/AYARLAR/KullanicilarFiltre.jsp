<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
	pageEncoding="ISO-8859-9"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<jsp:include page="header.jsp" />
		
	<div class="row" style="padding-top: 10px;">
        <div class="col-md-3">
        
        <div class="m-portlet">					
				<div class="m-portlet__body">
					<div class="form-group m-form__group">
						<strong>KULLANICI B�LG�LER�</strong> <hr/>
						<div class="row m-stack__items--right" style="padding-left: 3px;">
							<a href="#" onclick="KullaniciFiltrele()" class="btn btn-brand btn-block"><i class="fa fa-search"></i> Filtrele</a>
						</div>
					</div> 
                </div>
        </div>

		<div class="m-portlet">
			<div class="m-portlet__body">
					<div class="form-group m-form__group">
						<label for="adi">Ad Soyad</label>
							<input type="text" name="adi" id="adi" value="" class="form-control  m-input" placeholder="Ad� Soyad�"/>
					</div>
					<div class="form-group m-form__group">
						<label for="email">E-Posta</label>
						<input type="text" name="email" id="email" value="" class="form-control  m-input" placeholder="E-Posta"/>
					</div>
			    	
					<div class="form-group m-form__group">
			    	<label for="fk_rol">Yetki Rol�</label>
		                    <select name="fk_rol" id="fk_rol" class="form-control m-select2" style="width:100%">
		                    	<option value='0'>-- Seciniz --</option>
		                    	<c:forEach items="${roller}" var="n">
		                    	<option data-ext="${n.id}" value="${n.id}">${n.adi}</option>
		                    	</c:forEach>
		                    </select>
					</div>
			</div>
		</div>

    </div>
         
         
         
               <div class="col-md-9" id="kullaniciListe">
        

				</div>

</div>



<script>
$(document).ready(function(){

	$("#fk_rol").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	

});

function KullaniciFiltrele() {
	$('#kullaniciListe').html("");
	
	var data = {
			
				"adi" : $('#adi').val()
				, "email" : $('#email').val()
				, "fk_rol" : $('#fk_rol').val()


	};
	$.post("/ayarlar/kullanici/filtre?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#kullaniciListe').html(data);
		});


}
</script>
<jsp:include page="footer.jsp" />
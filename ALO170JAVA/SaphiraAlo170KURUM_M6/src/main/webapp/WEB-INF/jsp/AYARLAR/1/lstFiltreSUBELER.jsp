<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>

<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                    	
                    	<th>Ad�</th>
                    	<th>Kurum</th>
                    	<th>�l</th>
                    	<th>Havuz Say�s�</th>
                       	<th class="text-right">Durum</th>
                       	<th class="text-right">��lemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="l" >
                        <tr>
                        	
                            <td> ${l.adi } </td>
                            <td> ${l.kurum } </td>
                            <td> ${l.il } </td>
                            <td> ${l.havuz_sayisi } </td>
							<td class="text-right">
                            <c:if test="${l.isActive == 0}"><span class="m--font-danger">Pasif</span></c:if>
                            <c:if test="${l.isActive ne 0}"><span class="m--font-bold">Aktif</span></c:if>
                            </td>
                            <td class="text-right"> <a href="#" onclick="SubeEkle(${l.id})" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill">
								 <i class="la la-search"></i>
								 </a>
							</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
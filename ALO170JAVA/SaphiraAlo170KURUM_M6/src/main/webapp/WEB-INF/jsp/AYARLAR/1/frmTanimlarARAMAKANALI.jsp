<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
						

<div class="col-12">
<input type="hidden" name="id" id="id" value="${list.id }" />

	<div class="form-group m-form__group row">
		<div class="col-2">
		<label for="adi">Arama Kanal� Ad�</label>
		</div>
		<div class="col-10">
			<input type="text" name="adi" id="adi" value="${list.adi }" class="form-control  m-input" />
		</div>	
	</div>
	
	<div class="form-group m-form__group row">
		<div class="col-2">
		<label for="adi">Statu</label>
		</div>
		<div class="col-10">
                  <select name="active" id="active" class="form-control m-select2" style="width:100%">
                  	<option value='1' ${list.isActive == 1 ? 'selected' : ' '}>Aktif</option>
                  	<option value='0' ${list.isActive == 0 ? 'selected' : ' '}>Pasif</option>
                  </select>
                 </div>
	</div>
	
	<div class="form-group m-form__group row pull-right">
		<div class="col-12">
			<a href="#" onclick="AramaKanaliKaydet();" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Kaydet</a>&nbsp;
		</div>	
	</div>
</div>
				
							
							
<script>

$(document).ready(function(){

	$("#active").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });

});

function AramaKanaliKaydet(){
	var data = {
			
			"id" : $('#id').val()
			,"adi" : $('#adi').val()
			, "active" : $('#active').val()
			, "${_csrf.parameterName}" : "${_csrf.token}"

};
	$.post("/ayarlar/tanimlarEkleAramaKanali?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#pnlAramaKanali').modal('hide');
		AramaKanali();
		});
}


 
</script>
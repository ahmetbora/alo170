<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<div class="row">
	<div class="col-md-5">
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							HAVUZ TANIMLAMALARI
						</h3>
					</div>
				</div>
			</div>			
			<div class="m-portlet__body">
				<form class="m-form m-form--fit m-form--label-align-right">
					<div class="form-group m-form__group">
			    		<label for="tip_level1">KURUM</label>
	                    <select id="tip_level1"  style="width:100%">
							
						</select>
					</div>		
					<div class="form-group m-form__group">
			    		<label for="fk_il">ŞEHİR</label>
						<select id="fk_il" style="width:100%">
							
						</select>			    		
					</div>	
					<div class="form-group m-form__group">
			    		<label for="fk_sube">ŞUBELER</label>
						<select id="fk_sube"  style="width:100%">
							
						</select>
					</div>	
					<div class="form-group m-form__group">
			    		<label for="fk_havuz">HAVUZLAR</label>
						<select id="fk_havuz" style="width:100%">
							
						</select>
					</div>																						    					
				</form>
			</div>
			<div class="m-portlet__foot">
				<a href="javascript:void(0);" class="btn btn-brand btn-block" id="btnHavuzEkle"><i class="fa fa-plus"></i> Havuz Ekle</a>
			</div>				
		</div>
	</div>
	<div class="col-md-7">
		<div id="eklenenGrid"></div>
	</div>
</div>

<script id="islemlerTemplate" type="text/x-kendo-template">
	<button type="button" class="btn btn-info btn-sm btnHavuzSil" onclick="havuzSil('#: id #','#: uid#');"><i class="fa fa-times"></i></button>
</script>

<script>



// data sources
function getKurumData(){
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    data: JSON.stringify({
	        term: ''
	    }),
	    url: "/ayarlar/kurumlar",
	    contentType: "application/json; charset=utf-8",
	    beforeSend: function(req) {
	        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	    },
	    success: function (result) {
	    	var combobox = $("#tip_level1").data("kendoDropDownList");
	    	combobox.setDataSource(result.data);
	    }
	});	
}
getKurumData();



function getSehirData(){
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    data: JSON.stringify({
	        term: ''
	    }),
	    url: "/ayarlar/sehirler",
	    contentType: "application/json; charset=utf-8",
	    beforeSend: function(req) {
	        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	    },
	    success: function (result) {
	    	var combobox = $("#fk_il").data("kendoDropDownList");
	    	combobox.setDataSource(result.data);
	    }
	});	
}
getSehirData();


function getSubeData(sehir,kurum){
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    data: JSON.stringify({
	        fk_il: sehir,
	        tip_level1:kurum
	    }),
	    url: "/ayarlar/subeler",
	    contentType: "application/json; charset=utf-8",
	    beforeSend: function(req) {
	        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	    },
	    success: function (result) {

	    	var combobox = $("#fk_sube").data("kendoDropDownList");
	    	combobox.setDataSource(result.data);
	   
	    	 
	    	combobox.trigger("change"); //this will trigger change event
	    }
	});	
}


function getHavuzData(sube,ids){
	$.ajax({
	    type: "POST",
	    dataType: 'json',
	    data: JSON.stringify({
	        fk_sube: sube,
	        ids:ids
	    }),
	    url: "/ayarlar/havuzlar",
	    contentType: "application/json; charset=utf-8",
	    beforeSend: function(req) {
	        req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
	    },
	    success: function (result) {

	    	var combobox = $("#fk_havuz").data("kendoDropDownList");
	    	combobox.setDataSource(result.data);
	    }
	});	
}

// dropdown events
function kurumOnChange(){
	var sehir=$("#fk_il").val();
	var kurum=$("#tip_level1").val();
	if(sehir>0 && kurum>0){
		getSubeData(sehir,kurum)
	}
}
function sehirOnChange(){
	var sehir=$("#fk_il").val();
	var kurum=$("#tip_level1").val();
	if(sehir>0 && kurum>0){
		getSubeData(sehir,kurum)
		
	}
}

function subeOnChange(){
	var sube=$("#fk_sube").val();
	var ids="";
	//if(sube>0){
	    var gridData=eklenenDataSource.data();
    	var i;
    	for (i = 0; i < gridData.length; i++) { 
    		ids += gridData[i].id + ",";
    	}
		getHavuzData(sube,ids);
	//}
}



//set dropdowns
function initKurumCombo(){
	var kurumCombo=$("#tip_level1").kendoDropDownList({
		dataTextField: "adi",
		dataValueField: "id",
		optionLabel: {
		    adi: "Kurum Seçin...",
		    id: "0"
		},
		change:kurumOnChange
	});
}
function initSehirCombo(){
	var sehirCombo=$("#fk_il").kendoDropDownList({
		dataTextField: "adi",
		dataValueField: "id",
		optionLabel: {
		    adi: "Şehir Seçin...",
		    id: "0"
		},
		change:sehirOnChange
	});
}
function initSubeCombo(){
	var subeCombo=$("#fk_sube").kendoDropDownList({
		dataTextField: "adi",
		dataValueField: "id",
		optionLabel: {
		    adi: "Şube Seçin...",
		    id: "0"
		},
		change:subeOnChange
	});
}
function initHavuzCombo(){
	var havuzCombo=$("#fk_havuz").kendoDropDownList({
		dataTextField: "adi",
		dataValueField: "id",
		optionLabel: {
		    adi: "Havuz Seçin...",
		    id: "0"
		}
	});
}

initKurumCombo();
initSehirCombo();
initSubeCombo();
initHavuzCombo();



	var eklenenDataSource= new kendo.data.DataSource({
		  data: [],
		  schema: 
			  {
			    model: { id: "id" },
				fields: {
				    "id"            :   { editable: false, nullable: true },
				    "adi"			:   { editable:false,type: "string" },
				}
			  }
		});
	var data = eklenenDataSource.data();
	var lastItem = data[data.length - 1];
    var eklenenGrid = $("#eklenenGrid").kendoGrid({
        "columns": [
            {
                "field": "id",
                "filterable": false, "groupable": false, "title": "ID",
            },
            {
                "field": "adi",
                "filterable": false, "groupable": false, "title": "ADI",
            },
            {
            	template:kendo.template($("#islemlerTemplate").html()),
                filterable: false,  
                title: "",
                width:50
            }, 
                       
        ],
        "dataSource":eklenenDataSource,
    });



 
    $("#btnHavuzEkle").click(function(){
    	var dropdownlist = $("#fk_havuz").data("kendoDropDownList");
    	var id=dropdownlist.value();
    	var txt=dropdownlist.text();
    	var gridData=eklenenDataSource.data();
    	const findData = gridData.find(havuz => havuz.id === id);
    	var findDataType = typeof findData;
    	if (findDataType != 'object') {
        	if(id>0){
    			eklenenDataSource.add({ id:id, adi: txt });
        	}
        }
    	//dropdownlist.select(0);

    	var subeCombo = $("#fk_sube").data("kendoDropDownList");
    	subeCombo.select(0);
    	subeCombo.trigger("change");
    	var gridData=eklenenDataSource.data();
		//console.log(gridData);
    	
    })

function havuzSil(id,uid){
    	var row=eklenenDataSource.getByUid(uid);
    	eklenenDataSource.remove(row);
    	eklenenDataSource.sync();
}

</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
						

<div class="col-12">
<input type="hidden" name="id" id="id" value="${list.id }" />
								<div class="form-group m-form__group row">
									<div class="col-2">
									<label for="adi">Kurum Ad�</label>
									</div>
									<div class="col-10">
					                    <select name="fk_konu" id="konu" class="form-control m-select2" style="width:100%">
					                    	<option value='0'>-- Se�iniz --</option>
					                    	<c:forEach items="${kurumlar}" var="n">
					                    	<option data-ext="${n.id}" ${list.fk_konu == n.id ? 'selected' : ' '} value="${n.id}">${n.adi}</option>
					                    	</c:forEach>
					                    </select>
				                    </div>
								</div>
								<div class="form-group m-form__group row">
									<div class="col-2">
									<label for="adi">�l</label>
									</div>
									<div class="col-10">
					                    <select name="fk_il" id="fk_il" class="form-control m-select2" style="width:100%">
					                    	<option value='0'>-- Se�iniz --</option>
					                    	<c:forEach items="${iller}" var="n">
					                    	<option data-ext="${n.id}" ${list.fk_il == n.id ? 'selected' : ' '} value="${n.id}">${n.adi}</option>
					                    	</c:forEach>
					                    </select>
				                    </div>
								</div>
								<div class="form-group m-form__group row">
									<div class="col-2">
									<label for="adi">Sube</label>
									</div>
									<div class="col-10">
					                    <select name="fk_sube" id="fk_sube" class="form-control m-select2" style="width:100%">
					                    	<option value='0'>-- Se�iniz --</option>
					                    	<c:forEach items="${subeler}" var="n">
					                    	<option data-ext="${n.id}" ${list.fk_sube == n.id ? 'selected' : ' '} value="${n.id}">${n.adi}</option>
					                    	</c:forEach>
					                    </select>
				                    </div>
								</div>
								<div class="form-group m-form__group row">
									<div class="col-2">
									<label for="adi">Havuz Ad�</label>
									</div>
									<div class="col-10">
										<input type="text" name="adi" id="adi" value="${list.adi }" class="form-control  m-input" />
									</div>	
								</div>
								<div class="form-group m-form__group row">
									<div class="col-2">
									<label for="adi">Statu</label>
									</div>
									<div class="col-10">
							                  <select name="active" id="active" class="form-control m-select2" style="width:100%">
							                  	<option value='1' ${list.isActive == 1 ? 'selected' : ' '}>Aktif</option>
							                  	<option value='0' ${list.isActive == 0 ? 'selected' : ' '}>Pasif</option>
							                  </select>
							                 </div>
								</div>
										<div class="form-group m-form__group row">
									<label class="col-form-label col-lg-3 col-sm-12">E-Mailler</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<textarea id="email" name="email" class="form-control" data-provide="summernote"  rows="10">${list.email }</textarea>
									</div>
								</div>
								<div class="form-group m-form__group row pull-right">
									<div class="col-12">
										<a href="#" onclick="HavuzKaydet();" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Kaydet</a>&nbsp;
									</div>	
								</div>
</div>
				
							
							
<script>

$(document).ready(function(){

	$("#konu").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#fk_il").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#fk_sube").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });
	$("#active").select2({ placeholder: "Se�iniz", minimumResultsForSearch: 1 / 0 });

});
function HavuzKaydet(){
	var data = {
			
			"id" : $('#id').val()
			,"adi" : $('#adi').val()
			,"fk_konu" : $('#konu').val()
			,"fk_il" : $('#fk_il').val()
			,"fk_sube" : $('#fk_sube').val()
			,"email" : $('#email').val()
			, "active" : $('#active').val()
			, "${_csrf.parameterName}" : "${_csrf.token}"

};
	$.post("/ayarlar/tanimlarEkleHavuz?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#pnlHavuz').modal('hide');
		Havuzlar();
		});
}

 
</script>
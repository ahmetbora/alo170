<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="newline" value="
"/>

<div class="modal-header" id="moda_header">
	<h5 class="modal-title" id="modalLabel">Alt Kurum EKle / Düzenle</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	</button>
</div>
<div class="modal-body" id="modal_body">
	<div class="col-12">
		<input type="hidden" name="id" id="id" value="${kurumlar.id }" />
		<div class="form-group m-form__group row">
			<label for="adi">Kurum Adı</label>
			<input type="text" name="adi" id="adi" value="${kurumlar.adi }" class="form-control  m-input" />
		</div>
		
		<div class="form-group m-form__group row">
			<label for="adi">Statu</label>
			<select name="active" id="active" class="form-control m-select2" style="width:100%">
				<option value='1' ${kurumlar.isActive == 1 ? 'selected' : ' '}>Aktif</option>
				<option value='0' ${kurumlar.isActive == 0 ? 'selected' : ' '}>Pasif</option>
			</select>
		</div>
		<div class="form-group m-form__group row">
			<label >E-Mailler</label>
			<textarea id="email" name="email" class="form-control" data-provide="summernote"  rows="10">${kurumlar.email }</textarea>
		</div>

	</div>
</div>
<div class="modal-footer" id="modal_footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
	<button type="button" class="btn btn-primary" id="btn_modal_kaydet" onclick="KurumKaydet();">Kaydet</button>
</div>

				
							
							
<script>

$(document).ready(function(){

	$("#active").select2({ placeholder: "Seçiniz", minimumResultsForSearch: 1 / 0 });

});

function KurumKaydet(){
	var data = {
			
			"id" : $('#id').val()
			,"adi" : $('#adi').val()
			, "email" : $('#email').val()
			, "active" : $('#active').val()
			, "${_csrf.parameterName}" : "${_csrf.token}"

};
	$.post("/ayarlar/tanimlarEkleKurum?${_csrf.parameterName}=${_csrf.token}", data, function(data) {
		$('#pnlKurum').modal('hide');
		Kurumlar();
		});
}


 
</script>
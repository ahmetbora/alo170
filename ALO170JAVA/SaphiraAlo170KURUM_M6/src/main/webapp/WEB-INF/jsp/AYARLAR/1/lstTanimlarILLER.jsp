<%@ page language="java" contentType="text/html; charset=ISO-8859-9"
    pageEncoding="ISO-8859-9"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>

<div class="m-portlet">
	<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
			
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						�ehirler
					</h3>
				</div>
			</div>
						
			<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#" onclick="IlEkle(0);" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-plus"> Yeni Ekle</i></a>
						</li>
					</ul>
				</div>
  	</div>
			  			
<div class="m-portlet__body">

				<table id="liste" class="table table-striped-  table-hover table-checkable dataTable no-footer dtr-inline collapsed">
                    <thead>
                    <tr>
                    	
                    	<th>Ad�</th>
                       	<th class="text-right">Durum</th>
                       	<th class="text-right">��lemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="l" >
                        <tr>
                        	
                            <td> ${l.adi } </td>
							<td class="text-right">
                            <c:if test="${l.isActive == 0}"><span class="m--font-danger">Pasif</span></c:if>
                            <c:if test="${l.isActive ne 0}"><span class="m--font-bold">Aktif</span></c:if>
                            </td>
                            <td class="text-right"> <a href="#" onclick="IlEkle(${l.id})" class="btn btn-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill">
								 <i class="la la-search"></i>
								 </a>
							</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                
<div class="modal fade" id="pnlIller">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel"></h5>
                <button type="button" class="fa fa-times" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="ieklebody">
                    <div >

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
    </div>
</div>

<script>
function IlEkle(id){
    
    $('#modalLabel').html('�ehir Ekle / D�zenle');
    $('#pnlIller').modal('show');
    

	$.get("/ayarlar/tanimlarIl/" +id, function(data) {
		$('#ieklebody').html(data);
		});
}

 
</script>
                

                
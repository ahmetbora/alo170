<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="modal-header" id="moda_header">
    <h5 class="modal-title" id="modalLabel">HAVUZ BİLGİSİ GÜNCELLE / EKLE</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    </button>
</div>
<div class="modal-body" id="modal_body">
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Kurum:</label>
            <select class="form-control" id="fk_konu">
                <c:forEach items="${kurumlar}" var="n">					
                    <option data-id="${n.adi}" value="${n.id}" <c:if test="${n.id eq havuz.fk_konu}"> selected</c:if>>${n.adi}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">İl:</label>
            <select class="form-control" id="fk_il">
                <c:forEach items="${iller}" var="n">					
                    <option data-id="${n.adi}" value="${n.id}" <c:if test="${n.id eq havuz.fk_il}"> selected</c:if>>${n.adi}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Şube:</label>
            <select class="form-control" id="fk_sube">
                <c:forEach items="${subeler}" var="n">					
                    <option data-id="${n.adi}" value="${n.id}" <c:if test="${n.id eq havuz.fk_sube}"> selected</c:if>>${n.adi}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Durumu:</label>
            <select class="form-control" id="isActive">
                <c:forEach items="${aktifpasiflist}" var="n">					
                    <option data-id="${n.adi}" value="${n.value}" <c:if test="${n.value eq havuz.isActive}"> selected</c:if>>${n.adi}</option>
                </c:forEach>
            </select>
        </div>
         
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Adı:</label>
            <input type="text" class="form-control" id="txtAdi" value="${havuz.adi}">
        </div>
  
</div>
<div class="modal-footer" id="modal_footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_modal_kapat">Vazgeç</button>
    <button type="button" class="btn btn-primary" id="btn_modal_kaydet">Kaydet</button>
</div>
<input type="hidden" id="id" value="${havuz.id}">
<script>
var token = '${_csrf.token}';
var header ='${_csrf.headerName}';
$('#pnlBildirimDetay').on('hidden.bs.modal', function (e) {
  $("#modalContent").html('');
  $(this).off('hidden.bs.modal');
})
$("#btn_modal_kaydet").unbind().click(function(){
    var adi=$("#txtAdi").val();
    var id=$("#id").val();
    var fk_il=$("#fk_il option:selected").val();
    var fk_konu=$("#fk_konu option:selected").val();
    var fk_sube=$("#fk_sube option:selected").val();
    var isActive=$("#isActive option:selected").val();
    if(adi==""){
        alertify.error("Adı Alanı Boş Bırakılamaz.");
        return false;
    }
    $.ajax({
		type 	: 'POST',
		url		: "/ayarlar/tanimlar/Update",
		data	: "cols=adi-splitter-fk_il-splitter-fk_konu-splitter-isActive-splitter-fk_sube&id="+id+"&vals="+adi+"-splitter-"+fk_il+"-splitter-"+fk_konu+"-splitter-"+isActive+"-splitter-"+fk_sube+"&tbl=task_havuzlar&splitter=-splitter-",
		beforeSend: function(req) {
			req.setRequestHeader("X-CSRF-TOKEN",'${_csrf.token}');
		},
		success : function(e){
            alertify.success("Tanım Güncellendi.")
          
            $("#divGrid").data("kendoGrid").dataSource.read();
            $('#pnlTanimlar').modal("hide");
		},
		error: function (request, status, error) {
			alertify.error("Tanım Güncellemesi Sırasında Hata.")
		}
	});
})		

function getSubeler(){
	var token = '${_csrf.token}';
	var header ='${_csrf.headerName}';
	$.ajax({
		type: "POST",
		url: '/bildirim/getSubeler',
		data:
				{
					tip_level1: $('#fk_konu option:selected').val(),
					fk_il: $('#fk_il option:selected').val()
				},
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success: function (data, textStatus, jqXHR) {
			$('#fk_sube').html(jqXHR.responseText);					
		},
		error: function (request, status, error) {
			//console.log(status);
		}
	});
}
$('#fk_il').change(function(){
	getSubeler();
});
$('#fk_konu').change(function(){
	getSubeler();
});

</script>
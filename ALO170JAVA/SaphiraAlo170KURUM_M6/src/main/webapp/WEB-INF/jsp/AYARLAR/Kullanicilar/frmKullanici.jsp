<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" autocomplete="off">
<div class="form-group m-form__group row">
	<div class="col-lg-12">
		<div class="alert alert-metal alert-dismissible fade show   m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert">
			Şifre oluşturma işleminden sonra, şifreyi kopyaladığınızdan emin olunuz. 
		</div>
	</div>
</div>
<div class="form-group m-form__group row">
	<div class="col-lg-6">
		<label><span class="m--font-danger">*</span> Kullanıcı Adı:</label>
		<input type="text" class="form-control m-input" placeholder="Kullanıcı Adı" autocomplete="off" name="username" id="username">
		
	</div>
	<div class="col-lg-6">
		<label class="">Dahili:</label>
		<input type=text class="form-control m-input" placeholder="Dahili" autocomplete="off" name="ext" id="ext">
	</div>
</div>
<div class="form-group m-form__group row">
	<div class="col-lg-6">
		<label>E-Posta:</label>
		<input type="email" class="form-control m-input" placeholder="E-Posta" autocomplete="off" name="email" id="email">
	</div>
	<div class="col-lg-6">
		<label><span class="m--font-danger">*</span> Ad Soyad:</label>
		<input type="text" class="form-control m-input" placeholder="Ad Soyad" autocomplete="off" name="adi" id="adi">
	</div>
</div>
<div class="form-group m-form__group row">
	<div class="col-lg-6">
		<label><span class="m--font-danger">*</span> Şifre:</label>
		<div class="input-group">
			<div class="input-group-prepend">
				<button class="btn btn-success" type="button" id="copyPassword"><i class="fa fa-copy"></i></button>
				<button class="btn btn-info" type="button" id="showPassword"><i class="fa fa-eye"></i></button>
			</div>
			<input type="password" class="form-control m-input" placeholder="Şifre" name="password" id="password">
			<div class="input-group-append">
				<button class="btn btn-primary" type="button" id="generatePassword"><i class="fa fa-history"></i></button>
			</div>
			
		</div>
		<span class="m-form__help">
			Şifre en az 9 karakterden oluşmalıdır. En az 1 büyük harf,1 küçük harf,1 rakam,1 özel karakter içermelidir. Örnek : c2w63#I9T-15
		</span>
	</div>
	<div class="col-lg-6">
		<label class=""><span class="m--font-danger">*</span> Şifre Tekrar:</label>
		<input type="password" class="form-control m-input" placeholder="Şifre Tekrar" name="password_t" id="password_t">
	</div>
</div>
<div class="form-group m-form__group row">
	<div class="col-lg-6">
		<label>T.C. Kimlik No:</label>
		<input type="text" class="form-control m-input" placeholder="T.C. Kimlik No" autocomplete="off" name="tckimlik" id="tckimlik">
	</div>
	<div class="col-lg-6">
		<label class="">Kurum Sicil No:</label>
		<input type="text" class="form-control m-input" placeholder="Kurum Sicil No" autocomplete="off" name="ksicil" id="ksicil">
	</div>
</div>
<div class="form-group m-form__group row">
	<div class="col-lg-6">
		<label>Emekli San. Sicil No:</label>
		<input type="text" class="form-control m-input" placeholder="Emekli San. Sicil No" autocomplete="off" name="emsicil" id="emsicil">
	</div>
	<div class="col-lg-6">
		<label class="">GSM:</label>
		<input type="text" class="form-control m-input" placeholder="GSM" autocomplete="off" name="gsm" id="gsm">
	</div>
</div>
<div class="form-group m-form__group row">
	<div class="col-lg-6">
		<label>Facebook:</label>
		<input type="text" class="form-control m-input" placeholder="Facebook" autocomplete="off" name="face" id="face">
	</div>
	<div class="col-lg-6">
		<label class="">Twitter:</label>
		<input type="text" class="form-control m-input" placeholder="Twitter" autocomplete="off" name="twit" id="twit">
	</div>
</div>
<div class="form-group m-form__group row">
	<div class="col-lg-6">
		<label>E-mail Bildirimi:</label>
		<select id="email_bildirim" name="email_bildirim" class="form-control">
			<option value="0" selected="selected">Bildirim Almasın</option>
			<option value="1">Bildirim Alsın</option>
		</select>
	</div>
	<div class="col-lg-6">
		<label class="">Durumu:</label>
		<select id="isActive" name="isActive" class="form-control">
			<option value="1" selected="selected">Aktif</option>
			<option value="0">Pasif</option>
		</select>
	</div>
</div>
</form>
<script>
$("#showPassword").click(function(){
	var attr=$("#password").attr("type");
	if(attr=="text"){
		$("#password").attr("type","password");
		$("#showPassword i").removeClass("fa-eye-slash");
		$("#showPassword i").addClass("fa-eye");
	}else{
		$("#password").attr("type","text");
		$("#showPassword i").removeClass("fa-eye");
		$("#showPassword i").addClass("fa-eye-slash");
	}
});

$("#generatePassword").click(function(){
	displayLoading("#yeniKullaniciCont");
	$("#generatePassword").attr("disabled",true);
	$.ajax({
		type	: 'POST',
		dataType:'JSON',
		url		: '/ayarlar/generateUserPassword',
		data	: '',
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
		},
		success : function(r){
			$("#password").val(r.mesaj);
			$("#password_t").val(r.mesaj);
			hideLoading("#yeniKullaniciCont");
			$("#generatePassword").attr("disabled",false);
			return false;
		},
		error: function (request, status, error) {
			var res=request.responseJSON.sonuc;
			var mesaj=request.responseJSON.mesaj;
			
			hideLoading("#yeniKullaniciCont");
			$("#generatePassword").attr("disabled",false);
			alertify.set({ labels: {
			    ok     : "Tamam",
			    cancel : "Deny"
			} });
			if(res=="99"){
				alertify.alert(mesaj,function(){
					location.href="/logout";
				});
			}else{
				alertify.error("Şifre Oluşturulamadı")
			}
		}
	});	
});

$('#copyPassword').on("click", function(){
	value = $("#password").val();
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val(value).select();
	document.execCommand("copy");
	$temp.remove();
	alertify.success(value + " Şifre Kopyalandı.");
});
</script>
"use strict";

var KTPortletTools = function () {
    // Toastr
    var initToastr = function() {
        toastr.options.showDuration = 1000;
    }

    // Demo 2
    var demo2 = function() {
        // This portlet is lazy initialized using data-portlet="true" attribute. You can access to the portlet object as shown below and override its behavior
        var portlet = new KTPortlet('kt_portlet_tools_2');

        // Toggle event handlers
        portlet.on('beforeCollapse', function(portlet) {
          
        });

        portlet.on('afterCollapse', function(portlet) {
                    
        });

        portlet.on('beforeExpand', function(portlet) {
          
        });

        portlet.on('afterExpand', function(portlet) {
        
        });

        // Remove event handlers
        portlet.on('beforeRemove', function(portlet) {
        
        });

        portlet.on('afterRemove', function(portlet) {
                   
        });

        // Reload event handlers
        portlet.on('reload', function(portlet) {
         

            KTApp.block(portlet.getSelf(), {
                overlayColor: '#000000',
                type: 'spinner',
                state: 'brand',
                opacity: 0.05,
                size: 'lg'
            });

            // update the content here

            setTimeout(function() {
                KTApp.unblock(portlet.getSelf());
            }, 2000);
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            initToastr();

            // init demos
           
            demo2();
    
        }
    };
}();

jQuery(document).ready(function() {
    KTPortletTools.init();
});
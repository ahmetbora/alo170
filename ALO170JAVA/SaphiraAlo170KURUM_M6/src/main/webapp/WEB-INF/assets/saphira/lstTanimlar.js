var m = $("meta[name=csrf-token]");
var csrf_token=m.attr("content");    
var loadMsg='<div class="blockui " style="margin-left:-84px; -webkit-box-shadow:0 0 20px blue; -moz-box-shadow: 0 0 20px blue;box-shadow:0 0 20px #93a2dd ;"><span>Yükleniyor...</span><span><div class="kt-spinner kt-spinner--v2 kt-spinner--primary "></div></span></div>';
var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': loadMsg});
function hideLoading(){
 loading.hide();
}
function showLoading(){
    loading.show();
}
var crudServiceBaseUrl="/ayarlar/tanimlarKurum";
dsKurumlar = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
       
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
            
        }
    },
    requestEnd: function(e) {
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				:   { validation: { required: true } },
               // "durum"				:   { editable:true,type: "string" },
               durum                :   { defaultValue:   { durumId: 1, durumAdi: "Aktif"} },
                "kurumadi"			:   { editable:false,type: "string" },
                "url"               :   {defaultValue:crudServiceBaseUrl+"/tanimlarKurum/"}      
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});

function kurumlarFiltre(term){
	dsKurumlar.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}




var crudServiceBaseUrl="/ayarlar/tanimlarAltKurum";
dsAltKurumlar = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "durum"				    :   { editable:false,type: "string" },
                "kurum"				    :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function altKurumlarFiltre(term){
	dsAltKurumlar.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}


var crudServiceBaseUrl="/ayarlar/tanimlarAramaKonusu";
dsAramaKonusu = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "durum"				    :   { editable:false,type: "string" },
                "konuadi"				    :   { editable:false,type: "string" },
                "kurumadi"		    :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function aramaKonusurFiltre(term){
	dsAramaKonusu.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}


var crudServiceBaseUrl="/ayarlar/tanimlarSube";
dsSube = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "il"	  			    :   { editable:false,type: "string" },
                "kurum"				    :   { editable:false,type: "string" },
                "havuzsayisi"	    :   { editable:false,type: "string" },
                "durum"     	    :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function subeFiltre(term){
	dsSube.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}




var crudServiceBaseUrl="/ayarlar/tanimlarHavuz";
dsHavuzlar = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "il"	  			    :   { editable:false,type: "string" },
                "kurum"				    :   { editable:false,type: "string" },
                "sube"      	    :   { editable:false,type: "string" },
                "kullanicisayisi" :   { editable:false,type: "string" },
                "durum"           :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function havuzlarFiltre(term){
	dsHavuzlar.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}



var crudServiceBaseUrl="/ayarlar/tanimlarAramaKanali";
dsAramaKanali = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "durum"           :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function aramaKanaliFiltre(term){
	dsAramaKanali.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}


var crudServiceBaseUrl="/ayarlar/tanimlarEgitimDurumu";
dsEgitim = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },  
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "durum"           :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function egitimFiltre(term){
	dsEgitim.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}



var crudServiceBaseUrl="/ayarlar/tanimlarGuvenlikSorusu";
dsGuvenlik = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },   
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },  
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "durum"           :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function guvenlikFiltre(term){
	dsGuvenlik.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}



var crudServiceBaseUrl="/ayarlar/tanimlarIller";
dsIller = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "durum"           :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function illerFiltre(term){
	dsIller.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}



var crudServiceBaseUrl="/ayarlar/tanimlarIlceler";
dsIlceler = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "il"				      :   { editable:false,type: "string" },
                "durum"           :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function ilcelerFiltre(term){
	dsIlceler.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}



var crudServiceBaseUrl="/ayarlar/tanimlarUlkeler";
dsUlkeler = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
            
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      setTimeout(function() {loading.hide()},1000);
    }, 
    requestStart: function(e) {
     loading.show();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				      :   { editable:false,type: "string" },
                "durum"           :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});
function ulkelerFiltre(term){
	dsUlkeler.filter([        
            {
                field: "adi",
                operator: "eq",
                value: term
            }                                                  
        ]
    );
}

var crudServiceBaseUrl="/ayarlar/tanimlarDahili";
dsDahili = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
            
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      hideLoading();
    }, 
    requestStart: function(e) {
        showLoading();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				:   { editable:false,type: "string" },
                "durum"             :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});

var crudServiceBaseUrl="/ayarlar/tanimlarKkoBaslik";
dsKkoBaslik = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
            
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      hideLoading();
    }, 
    requestStart: function(e) {
        showLoading();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				:   { editable:false,type: "string" },
                "durum"             :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});

var crudServiceBaseUrl="/ayarlar/tanimlarKkoSoru";
dsKkoSoru = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
            
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      hideLoading();
    }, 
    requestStart: function(e) {
        showLoading();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				:   { editable:false,type: "string" },
                "durum"             :   { editable:false,type: "string" },
                "fk_parent"             :   { editable:false,type: "string" },
                "parent"             :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});



var crudServiceBaseUrl="/ayarlar/tanimlarKkoSoru";
dsKkoSoru = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
            
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      hideLoading();
    }, 
    requestStart: function(e) {
        showLoading();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				:   { editable:false,type: "string" },
                "durum"             :   { editable:false,type: "string" },
                "puan"             :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});

var crudServiceBaseUrl="/ayarlar/tanimlarPeriyodik";
dsPeriyodik = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        destroy: {
            type: "POST",
            url: crudServiceBaseUrl+"/Del",
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
            
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      hideLoading();
    }, 
    requestStart: function(e) {
        showLoading();
    },
    batch: true,
    pageSize: 100,
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "email"				:   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": true,
    "serverSorting": true,
    "autoSync": true,
    "serverPaging": true
});



var crudServiceBaseUrl="/ayarlar/tnmLokasyonlar";
dsLokasyonlar = new kendo.data.DataSource({
    transport: {
        read:  {
            type: "POST",
            url: crudServiceBaseUrl,
            contentType: "application/json; charset=utf-8",            
            dataType: 'json',
            beforeSend: function(req) {
                req.setRequestHeader("X-CSRF-TOKEN",csrf_token);
            }
        },
        
        parameterMap: function(options, operation) {
            return kendo.stringify(options);
            if (operation !== "read" && options.models) {
                //return {models: kendo.stringify(options.models)};
            }
            
        }
    },
    requestEnd: function(e) {
      //check the "response" argument to skip the local operations
      hideLoading();
    }, 
    requestStart: function(e) {
        showLoading();
    },
    schema: {
        data:'data', 
        model: {
            id: "id",
            fields: {
                "id"            	:   { editable: false, nullable: true },
                "adi"				:   { editable:false,type: "string" },
                "durum"             :   { editable:false,type: "string" },
            }
        },
        "total": "total"
    },
    "serverFiltering": false,
    "serverSorting": false,
    "autoSync": true,
    "serverPaging": false
});

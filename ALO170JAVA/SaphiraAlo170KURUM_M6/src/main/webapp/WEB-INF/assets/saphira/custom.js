/**********************************************************************
 * 
 * 
 * NUMERIC INPUT
 * @param $
 * @returns
 */
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));

function displayLoading(target) {
    var element = $(target);
    kendo.ui.progress(element, true);      
}

function hideLoading(target){
	 var element = $(target);
	 kendo.ui.progress(element, false);
}



function checkPassword(pass){
	var ret="0";
   var 	ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
   var	ALPHA = "abcdefghijklmnopqrstuvwxyz";
   var	NUMERIC = "0123456789";
   var	SPECIAL = "!-_?+!@#$%()=}][{/";
   var	maxPasswordLength=50;
   var 	alphaCapsVar = false;
   var 	alphaVar = false;
   var 	numericVar = false;
   var 	specialVar = false;
   var 	passLengthVar=false;
   var 	dupeVar=false;
   for (i=0; i < pass.length; i++){
		if (ALPHA_CAPS.indexOf(pass.charAt(i)) > 0) {
			alphaCapsVar=true;
			break;
		}		
	}
   for (i=0; i < pass.length; i++){
		if (ALPHA.indexOf(pass.charAt(i)) > 0) {
			alphaVar=true;
			break;
		}		
	}
   for (i=0; i < pass.length; i++){
		if (NUMERIC.indexOf(pass.charAt(i)) > 0) {
			numericVar=true;
			break;
		}		
	}		
   for (i=0; i < pass.length; i++){
		if (SPECIAL.indexOf(pass.charAt(i)) > 0) {
			specialVar=true;
			break;
		}		
	}	

	if(alphaCapsVar==true && alphaVar==true && numericVar==true && specialVar==true){
		ret="1";
	}else{
		ret="0";
	}		
	return ret;
}

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
  return true;
}


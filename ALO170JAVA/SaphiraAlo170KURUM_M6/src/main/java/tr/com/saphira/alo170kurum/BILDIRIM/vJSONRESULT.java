package tr.com.saphira.alo170kurum.BILDIRIM;

import java.util.List;
@SuppressWarnings("unused")
public class vJSONRESULT {
	private String sonuc;
	private String mesaj;
	private Integer sonBildirim;
	private String sonKart;
	private String total;
	private Integer sonTaskDetayIslem;
	private Integer sonTaskDetayIslemTipi;
	private List<vBILDIRIM> bildirimListe;
	public vJSONRESULT() {
		super();
	
	}
	public String getTotal() {
		return total;
	}
	public String setTotal(String total) {
		return this.total;
	}
	public String getSonuc() {
		return sonuc;
	}
	public void setSonuc(String sonuc) {
		this.sonuc = sonuc;
	}
	public String getMesaj() {
		return mesaj;
	}
	public void setMesaj(String mesaj) {
		this.mesaj = mesaj;
	}
	public Integer getSonBildirim() {
		return sonBildirim;
	}
	public void setSonBildirim(Integer sonBildirim) {
		this.sonBildirim = sonBildirim;
	}
	public String getSonkart() {
		return sonKart;
	}
	public void setSonKart(String sonKart) {
		this.sonKart = sonKart;
	}	
	public Integer getsonTaskDetayIslem() {
		return sonTaskDetayIslem;
	}
	public void setsonTaskDetayIslem(Integer sonTaskDetayIslemTipi) {
		this.sonTaskDetayIslem = sonTaskDetayIslemTipi;
	}	
	public Integer getsonTaskDetayIslemTipi() {
		return sonTaskDetayIslem;
	}
	public void setsonTaskDetayIslemTipi(Integer sonTaskDetayIslemTipi) {
		this.sonTaskDetayIslemTipi=sonTaskDetayIslemTipi;
	}	

	public List<vBILDIRIM> getbildirimListe() {
		return bildirimListe;
	}
	public List<vBILDIRIM> setbildirimListe(List<vBILDIRIM> bildirimListe) {
		return this.bildirimListe = bildirimListe;
	}

}

package tr.com.saphira.alo170kurum.BILDIRIM;

import java.util.List;
import java.util.Map;

import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.KART.vKART;
import tr.com.saphira.alo170kurum.SCRIPT.vSCRIPT;
import tr.com.saphira.alo170kurum.SSS.vSSS;
import tr.com.saphira.alo170kurum.model.vTotal;



public interface BildirimDao {
	public List<vBILDIRIM> getKullaniciHavuzBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki);
	public List<vTotal> countKullaniciHavuzBildirimListesi(Integer userId);
	
	public List<vBILDIRIM> getUzerimdekiBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki);
	public List<vTotal> countUzerimdekiBildirimListesi(Integer userId,Map<String,Object> userYetki);
	
	public List<vBILDIRIM> SonuclandirdigimBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki);
	public List<vTotal> countSonuclandirdigimBildirimListesi(Integer userId,Map<String,Object> userYetki);
	
	public List<vBILDIRIM> ActigimBildirimListesi(Integer KullaniciID,Map<String,Object> userYetki);

	public List<vBILDIRIM> GuncellenenBildirimListesi(Integer skip,Integer take,Integer KullaniciID,Map<String,Object> userYetki);
	public List<vTotal> countGuncellenenBildirimListesi(Integer userId,Map<String,Object> userYetki);
	
	public List<vBILDIRIM> getYenidenAcilanBildirimListesi(Integer skip,Integer take,Integer userLokıasyon,Map<String,Object> userYetki);
	public List<vTotal> countYenidenAcilanBildirimListesi(Integer userLokıasyon);

	public List<vBILDIRIM> getOlusturdugumBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki);
	public List<vTotal> countOlusturdugumBildirimListesi(Integer userId);
	
	public List<vBILDIRIM> getVtAciklamaBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki);
	public List<vTotal> countVtAciklamaBildirimListesi(Integer userId);
	
	public List<vBILDIRIM> getWebSitesiBildirimListesi(Integer skip,Integer take,String userIds,Map<String,Object> userYetki);
	public List<vTotal> countWebSitesiBildirimListesi(String userIds);
	
	public List<vBILDIRIM> getMobilBildirimListesi(Integer skip,Integer take,Map<String,Object> userYetki);
	public List<vTotal> countMobilBildirimListesi();


	
	public List<vBILDIRIMDETAY> getBildirimDetay(Integer TaskID,Integer fkHavuz);
	public List<vBILDIRIMDETAY> getBildirimDetayGuncelleme(Integer TaskID);

	public vKART getKart(Integer KartID,Map<String,Object> userYetki);
	
	public vBILDIRIM getTask(Integer TaskID,Map<String,Object> userYetki);
	
	public List<vBILDIRIM> getBildirimAra(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki,Integer Statu,String Adi,String SoyAdi,String Tel,String Tc,Integer BildirimNo);
	public List<vTotal> countBildirimAra(Integer userId,Map<String,Object> userYetki,Integer Statu,String Adi,String SoyAdi,String Tel,String Tc,Integer BildirimNo);
	
	public List<vBILDIRIM> BildirimDetayRaporu(Integer AGENT, Integer IL, Integer ILCE, Integer STATU,Map<String,Object> userYetki);
	public List<vBILDIRIM> BildirimSureRaporu(String ilkTAR, String sonTAR, Integer HAVUZ);
	public List<vBILDIRIM> KurumYetkilisiSureRaporu(String ilkTAR, String sonTAR, Integer TIP);
	public void TCSifrele();
	
	public vADET adetUzerimdekiBild(Integer AGENT);
	
	public List<vBILDIRIM> exportExcel(Integer AGENT, String ADI , String SOYADI, String TC, String TEL, Integer TaskID, Integer STATU,Map<String,Object> userYetki);
	public List<vIVRTC> getIvrTc(String CALLID);
	public vKART getKartByTc(String TC);
	public List<vBILDIRIM> getBildirimGecmisi(Integer kartId,Map<String,Object> userYetki);
	public List<vBILDIRIM> bildirimKaydet(
			    // bildirim bilgileri
				String aciklama,Integer tip_level1,Integer tip_level2,Integer tip_level3,String arayan_numara,Integer fk_aramakanali,Integer fk_aramasebebi,Integer fk_kart,
				String uid,Integer fk_proje,Integer fk_sonuckod,Integer fk_lokasyon,Integer isOnayBekle,Integer fk_kaydi_acan,String referans,Integer fk_gorev_statu,
				Integer fk_kapatan_agent,
				// vatandaş bilgileri
				String adres,String adi,String soyadi,String tckimlik,String tel1,String gsm,Integer fk_egitimdurumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,
				String sskno,String email,Integer fk_cinsiyet,String fax,Integer fk_gsoru,String gsoru_cevap,
				// firma bilgileri
				String f_adi,String f_adres,String f_tel,Integer f_fk_il,Integer f_fk_ilce,String nace,String scs,
			
				// kart ve firma update kontrol
				Integer kart_update,Integer firma_bilgi,	
				Map<String,Object> userYetki
				);
	/*
	public void bildirimKaydet(
		    // bildirim bilgileri
		    String aciklama,Integer tip_level1,Integer tip_level2,Integer tip_level3,String arayan_numara,Integer fk_aramakanali,Integer fk_aramasebebi,Integer fk_kart,
			String uid,Integer fk_proje,Integer fk_sonuckod,Integer fk_lokasyon,Integer isOnayBekle,Integer fk_kaydi_acan,String referans,Integer fk_gorev_statu,
			Integer fk_kapatan_agent,
			// vatandaş bilgileri
			String adres,String adi,String soyadi,String tckimlik,String tel1,String gsm,Integer fk_egitimdurumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,
			String sskno,String email,Integer fk_cinsiyet,String fax,Integer fk_gsoru,String gsoru_cevap,
			// firma bilgileri
			String f_adi,String f_adres,String f_tel,Integer f_fk_il,Integer f_fk_ilce,String nace,String scs,
		
			// kart ve firma update kontrol
			Integer kart_update,Integer firma_bilgi
		);
	*/
	public List<vSSS> getBilgiBankasiListe(String term,Integer skip,Integer take);
	public List<vTotal> countBilgiBankasiListe(String term);
	public vSSS getSssById(Integer ID);
	
	
	public List<vSCRIPT> getBildirimScriptListe(String term,Integer skip,Integer take);
	public List<vTotal> countBildirimScriptListe(String term);
	
	public List<vKULLANICILAR> getVtMesajKullaniciListe(String term,Integer UserId);
	
	public List<vVTMESAJ> getVtMesajListe(String tip,Integer skip,Integer take,Integer userId);
	public List<vTotal> countVtMesajListe(String term,Integer userId);
	public vVTMESAJ getVtMesajById(Integer ID);

	public vJSONRESULT vtMesajGonder(String mesaj,Integer alici,Integer gonderen,Integer parent,Integer fkroot);
	
	public List<vBILDIRIM> getBildirimListeNoHavuz(Integer skip,Integer take,Map<String,Object> userYetki);
	public List<vTotal> countBildirimListeNoHavuz();
	

	public vJSONRESULT changeTaskUser(Integer id,Integer fkGorevStatu,Integer userId);
	public vJSONRESULT addTaskDetay(Integer sistem,Integer id,Integer userId,String aciklama,String ipAdres,Integer fk_detay_tip);
	public vJSONRESULT addChangeUserLog(Integer svUserId,Integer userId,Integer login,Integer bildirimId,Integer fkIslem);
	public vTASKDETAYISLEM openDetayIslem(Integer Id,Integer fkGorev,Integer Sistem);
	public vJSONRESULT addCrmIslemSureleri(Integer fkGorev,Integer fkAgent,Integer Sistem,Integer fkDetayIslem,Integer Kurum,Integer Kurum1,Integer Kurum2,Integer fkSube,Integer fkHavuz,Integer fkIl,String Tarih);
	

	public vJSONRESULT vatandasBilgisiKapataC(Integer Id, Integer isOnay);
	public vJSONRESULT kurumAltKurumGuncelle(Integer id,Integer t1,Integer t2,Integer t3,Integer fk_aramakanali, Integer fk_aramasebebi); 

	public vJSONRESULT havuzaYonlendir(Integer id,Integer fk_havuz,Integer fkAgent,String aciklama);
	public vJSONRESULT taskDBUP(Integer id);
	public vTASKHAVUZKULLANICI getTaskHavuzKullanici(Integer userId,Integer whereIN,String IDS,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu);
	public vJSONRESULT taskHavuzDetayEkle(Integer fkAgent,Integer fkGorev,Integer detayIslemId,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu);
	public vJSONRESULT kullaniciyaYonlendir(Integer id,Integer fk_havuz,Integer fk_user, Integer tip_level4 );
	public vJSONRESULT taskChangeStatu(Integer id,Integer statu);
	public vJSONRESULT bildirimKapat(Integer id,Integer fk_agent, Integer fk_taskb_kapanma_statu,String kurumYetki,String aciklama,Map<String,Object> userYetki);
	public vJSONRESULT yenidenAc(Integer id, Integer userLokasyon,Map<String,Object> userYetki);
	public vJSONRESULT addSmsListe(Integer fk_gorev, String gsm, String mesaj);
	public Map<String, Object> getTaskDBDetay(Integer sistem, Integer fkGorev, Integer fkAgent,Integer limit,Integer id);
}




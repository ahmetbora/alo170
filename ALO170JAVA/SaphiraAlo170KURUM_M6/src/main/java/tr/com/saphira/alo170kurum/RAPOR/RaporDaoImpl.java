package tr.com.saphira.alo170kurum.RAPOR;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tr.com.saphira.alo170kurum.model.vTotal;

@Transactional
@Repository
public class RaporDaoImpl implements RaporDao {
	
	 @Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;
    
	 		/***
	 * listelerde toplam kayıt sayısı için liste olusturur vTotal class içine aktarır
	 * @param rows
	 * @return
	 */
	private List<vTotal> getCountList(List<Map<String, Object>> rows) {
		List<vTotal> ret = new ArrayList<vTotal>();
		for (Map<?, ?> row : rows) {
			vTotal res = new vTotal();
			try {		
				res.setTotal((Long)row.get("total"));
			} catch (Exception ex) {
					System.out.println("total Hata :" + ex.toString());
			}
			ret.add(res);
		}
		return ret;
	}


	 @Override
	 public List<rptKurumSubeIstatistik> KurumSubeHavuzIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer fk_havuz,
		Integer skip,
		Integer take,
		Integer userId,
		String yetki

	 ) {
		
		   String sql1 = "call rptKurumSubeHavuzIstatistik (?,?,?,?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { sdate,fdate,tip_level1,fk_il,fk_sube,fk_havuz,skip,take,userId,yetki });
		   
		   List<rptKurumSubeIstatistik> ret = new ArrayList<rptKurumSubeIstatistik>();
		   
			 for (Map<String, Object> row : list ) {
				 
				rptKurumSubeIstatistik rpt = new rptKurumSubeIstatistik();	
				try {		
					rpt.setId((Integer)row.get("havuzid"));
				} catch (Exception ex) {
					System.out.println("havuzid Hata :" + ex.toString());
				}
				try {		
					rpt.setIl(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("il Hata :" + ex.toString());
				}
				try {		
						rpt.setHavuz(row.get("havuzadi").toString());
				} catch (Exception ex) {
					System.out.println("havuzadi Hata :" + ex.toString());
				}
				try {		
						rpt.setSube(row.get("sube").toString());
				} catch (Exception ex) {
					System.out.println("havuz Hata :" + ex.toString());
				}
				try {		
					rpt.setKapanan(row.get("kapanan").toString());
				} catch (Exception ex) {
					System.out.println("kapanan Hata :" + ex.toString());
				}try {		
					rpt.setGuncelleme_talebi(row.get("guncelleme_talebi").toString());
				} catch (Exception ex) {
				System.out.println("guncelleme_talebi Hata :" + ex.toString());
				}try {		
					rpt.setBirime_atandi(row.get("birime_atandi").toString());
				} catch (Exception ex) {
				System.out.println("birime_atandi Hata :" + ex.toString());
				}try {		
					rpt.setInceleniyor(row.get("inceleniyor").toString());
				} catch (Exception ex) {
				System.out.println("inceleniyor Hata :" + ex.toString());
				}try {		
					rpt.setInceleniyor_eksure(row.get("inceleniyor_eksure").toString());
				} catch (Exception ex) {
				System.out.println("inceleniyor_eksure Hata :" + ex.toString());
				}try {		
					rpt.setIslem_yapilmamis(row.get("islem_yapilmamis").toString());
				} catch (Exception ex) {
				System.out.println("islem_yapilmamis Hata :" + ex.toString());
				}try {		
					rpt.setReddedildi(row.get("reddedildi").toString());
				} catch (Exception ex) {
				System.out.println("reddedildi Hata :" + ex.toString());
				}try {		
					rpt.setToplam(row.get("toplam").toString());
				} catch (Exception ex) {
				System.out.println("toplam Hata :" + ex.toString());
				}try {		
					rpt.setZamaninda_kapatilmayan(row.get("zamaninda_kapatilmayan").toString());
				} catch (Exception ex) {
				System.out.println("zamaninda_kapatilmayan Hata :" + ex.toString());
				}
				try {		
					rpt.setZamaninda_kapanan(row.get("zamaninda_kapanan").toString());
				} catch (Exception ex) {
				System.out.println("zamaninda_kapanan Hata :" + ex.toString());
				}
				try {		
					rpt.setKurum(row.get("kurum").toString());
				} catch (Exception ex) {
				System.out.println("kurum Hata :" + ex.toString());
				}
				
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 @Override
	 public  List<vTotal> countKurumSubeHavuzIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer fk_havuz,
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptKurumSubeHavuzIstatistikCount (?,?,?,?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			 sdate, 
			 fdate, 
			 tip_level1, 
			 fk_il, 
			 fk_sube, 
			 fk_havuz,
			 userId,
			 yetki
		  });
		 return getCountList(rows);
	 }	



	 @Override
	 public List<rptKurumKullaniciIstatistik> KurumKullaniciIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki

	 ) {
		   String sql1 = "call rptKurumKullaniciIstatistik (?,?,?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { sdate, fdate, tip_level1, fk_il, fk_sube, skip,take,userId,yetki });
		   List<rptKurumKullaniciIstatistik> ret = new ArrayList<rptKurumKullaniciIstatistik>();
			 for (Map<String, Object> row : list ) {
				 
				rptKurumKullaniciIstatistik rpt = new rptKurumKullaniciIstatistik();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setIl(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("il Hata :" + ex.toString());
				}
				try {		
						rpt.setSube(row.get("sube").toString());
				} catch (Exception ex) {
					System.out.println("sube Hata :" + ex.toString());
				}
				try {		
					rpt.setKapanan(row.get("kapanan").toString());
				} catch (Exception ex) {
					System.out.println("kapanan Hata :" + ex.toString());
				}
				try {		
					rpt.setAcik(row.get("acik").toString());
				} catch (Exception ex) {
				System.out.println("acik Hata :" + ex.toString());
				}
				try {		
					rpt.setUser(row.get("user").toString());
				} catch (Exception ex) {
				System.out.println("setUser Hata :" + ex.toString());
				}
				
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
 	 @Override
	 public  List<vTotal> countKurumKullaniciIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptKurumKullaniciIstatistikCount (?,?,?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			tip_level1, 
			fk_il, 
			fk_sube,
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }	   


	 @Override
	 public List<rptBildirimDetay> BildirimDetayRaporu(
        String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer tip_level2,
		Integer tip_level3,
		Integer fk_il, 
		Integer fk_ilce, 
		Integer fk_havuz, 
		Integer fk_gorev_statu, 
		Integer arama_kanali_tipi, 
		Integer fk_aramakanali, 
		Integer fk_aramasebebi, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki

	 ) {

		   String sql1 = "call rptBildirimDetay (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			tip_level1, 
			tip_level2,
			tip_level3,
			fk_il, 
			fk_ilce, 
			fk_havuz, 
			fk_gorev_statu, 
			arama_kanali_tipi, 
			fk_aramakanali, 
			fk_aramasebebi, 
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptBildirimDetay> ret = new ArrayList<rptBildirimDetay>();
			 for (Map<String, Object> row : list ) {
				 
				rptBildirimDetay rpt = new rptBildirimDetay();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setVatandas(row.get("vatandas").toString());
				} catch (Exception ex) {
					System.out.println("vatandas Hata :" + ex.toString());
				}
				try {		
					rpt.setTarih(row.get("gtarih").toString()+" "+row.get("gsaat").toString());
				} catch (Exception ex) {
					System.out.println("tarih Hata :" + ex.toString());
				}
				try {		
					rpt.setAgent(row.get("yapacak_agent").toString());
				} catch (Exception ex) {
					System.out.println("agent Hata :" + ex.toString());
				}
				try {		
					rpt.setKonu(row.get("konusu").toString());
				} catch (Exception ex) {
					System.out.println("konu Hata :" + ex.toString());
				}
				try {		
					rpt.setIl(row.get("iladi").toString());
				} catch (Exception ex) {
					System.out.println("il Hata :" + ex.toString());
				}
				try {		
					rpt.setStatu(row.get("statu").toString());
				} catch (Exception ex) {
					System.out.println("statu Hata :" + ex.toString());
				}
				try {		
					rpt.setSure(row.get("fark").toString());
				} catch (Exception ex) {
					System.out.println("fark Hata :" + ex.toString());
				}
				try {		
					rpt.setfk_agent((Integer)row.get("fk_agent"));
				} catch (Exception ex) {
					System.out.println("fk_agent Hata :" + ex.toString());
				}
				try {		
					rpt.setfk_gorev_statu((Integer)row.get("fk_gorev_statu"));
				} catch (Exception ex) {
					System.out.println("fk_gorev_statu Hata :" + ex.toString());
				}
				try {		
					rpt.setstatuClass((String)row.get("statu_class"));
				} catch (Exception ex) {
					System.out.println("statuClass Hata :" + ex.toString());
				}

		
				
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 @Override
	 public  List<vTotal> countBildirimDetayRaporu(
        String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer tip_level2,
		Integer tip_level3,
		Integer fk_il, 
		Integer fk_ilce, 
		Integer fk_havuz, 
		Integer fk_gorev_statu, 
		Integer arama_kanali_tipi, 
		Integer fk_aramakanali, 
		Integer fk_aramasebebi, 
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptBildirimDetayCount (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			tip_level1, 
			tip_level2,
			tip_level3,
			fk_il, 
			fk_ilce, 
			fk_havuz, 
			fk_gorev_statu, 
			arama_kanali_tipi, 
			fk_aramakanali, 
			fk_aramasebebi, 
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }



	 @Override
	 public List<rptMuAktivite> MuAktiviteRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
        Integer sistem, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
	 ) {
		   String sql1 = "call rptMuAktivite (?,?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			mu, 
			sistem,  
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptMuAktivite> ret = new ArrayList<rptMuAktivite>();
			 for (Map<String, Object> row : list ) {
				 
				rptMuAktivite rpt = new rptMuAktivite();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setmu(row.get("agent").toString());
				} catch (Exception ex) {
					System.out.println("mu Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("gtarih").toString());
				} catch (Exception ex) {
					System.out.println("gtarih Hata :" + ex.toString());
				}
				try {		
					rpt.setislem(row.get("sistem").toString());
				} catch (Exception ex) {
					System.out.println("sistem Hata :" + ex.toString());
				}
				try {		
					rpt.setstatu(row.get("statu").toString());
				} catch (Exception ex) {
					System.out.println("statu Hata :" + ex.toString());
				}
				try {		
					rpt.setaciklama(row.get("aciklama").toString());
				} catch (Exception ex) {
					System.out.println("aciklama Hata :" + ex.toString());
				}
				try {		
					rpt.setdetayIslem(row.get("detay_islem").toString());
				} catch (Exception ex) {
					System.out.println("detay_islem Hata :" + ex.toString());
				}
	
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 @Override
	 public  List<vTotal> countMuAktiviteRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
        Integer sistem, 
        Integer userId, 
        String yetki
	 ) {
		String sql1 = "call rptMuAktiviteCount (?,?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			mu, 
			sistem,  
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }


	 @Override
	 public List<rptMuSure> MuSureRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
		Integer tip_level1, 
        Integer sistem, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
	 ) {
		   String sql1 = "call rptMuSure (?,?,?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			mu, 
			tip_level1, 
			sistem,  
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptMuSure> ret = new ArrayList<rptMuSure>();
			 for (Map<String, Object> row : list ) {
				 
				rptMuSure rpt = new rptMuSure();	
				try {		
					rpt.setId((Integer)row.get("fk_gorev"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setmu(row.get("mu").toString());
				} catch (Exception ex) {
					System.out.println("mu Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("btarih").toString());
				} catch (Exception ex) {
					System.out.println("tarih Hata :" + ex.toString());
				}
				try {		
					rpt.setkurumbilgisi(row.get("kurumBilgisihtml").toString());
				} catch (Exception ex) {
					System.out.println("kurumBilgisi Hata :" + ex.toString());
				}
				try {		
					rpt.sethavuz(row.get("havuz").toString());
				} catch (Exception ex) {
					System.out.println("havuz Hata :" + ex.toString());
				}
	
				try {		
					rpt.setsube(row.get("sube").toString());
				} catch (Exception ex) {
					System.out.println("sube Hata :" + ex.toString());
				}
	
				try {		
					rpt.setsure(row.get("sure").toString());
				} catch (Exception ex) {
					System.out.println("sure Hata :" + ex.toString());
				}
	
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 @Override
	 public  List<vTotal> countMuSureRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
        Integer tip_level1, 
        Integer sistem, 
        Integer userId, 
        String yetki
	 ) {
		String sql1 = "call rptMuSureCount (?,?,?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			mu, 
			tip_level1, 
			sistem,  
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }    



	 @Override
	 public List<rptKySure> KySureRapor(
        String sdate, 
        String fdate, 
        Integer ky, 
        Integer sistem, 
        Integer tip_level1, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
	 ) {
		   String sql1 = "call rptKySure (?,?,?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			ky, 
			sistem,  
			tip_level1,  
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptKySure> ret = new ArrayList<rptKySure>();
			 for (Map<String, Object> row : list ) {
				 
				rptKySure rpt = new rptKySure();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("tarih").toString());
				} catch (Exception ex) {
					System.out.println("tarih Hata :" + ex.toString());
				}
				try {		
					rpt.setmu(row.get("mu").toString());
				} catch (Exception ex) {
					System.out.println("setmu Hata :" + ex.toString());
				}
				try {		
					rpt.sethavuz(row.get("havuz").toString());
				} catch (Exception ex) {
					System.out.println("havuz Hata :" + ex.toString());
				}
				try {		
					rpt.setkurum(row.get("kurum").toString());
				} catch (Exception ex) {
					System.out.println("setkurum Hata :" + ex.toString());
				}
				try {		
					rpt.setaltkurum(row.get("altkurum").toString());
				} catch (Exception ex) {
					System.out.println("setaltkurum Hata :" + ex.toString());
				}
				try {		
					rpt.setarama_konusu(row.get("arama_konusu").toString());
				} catch (Exception ex) {
					System.out.println("setarama_konusu Hata :" + ex.toString());
				}
				try {		
					rpt.setsube(row.get("sube").toString());
				} catch (Exception ex) {
					System.out.println("setsube Hata :" + ex.toString());
				}
				try {		
					rpt.setsure(row.get("sure").toString());
				} catch (Exception ex) {
					System.out.println("setsure Hata :" + ex.toString());
				}

	
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 @Override
	 public  List<vTotal> countKySureRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
        Integer sistem, 
        Integer tip_level1, 
        Integer userId, 
        String yetki
	 ) {
		String sql1 = "call rptMuSureCount (?,?,?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			 sdate, 
			 fdate, 
			 mu, 
			 sistem, 
			 tip_level1, 
			 userId, 
			 yetki
		  });
		 return getCountList(rows);
	 }    


	 @Override
	 public List<rptHavuzBildirimSure> HavuzBildirimSureRapor(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        Integer fk_havuz, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
	 ) {
		   String sql1 = "call rptHavuzBildirimSure (?,?,?,?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			tip_level1, 
			fk_il, 
			fk_sube, 
			fk_havuz, 
			skip, 
			take, 
			userId, 
			yetki
		});

		   List<rptHavuzBildirimSure> ret = new ArrayList<rptHavuzBildirimSure>();
			 for (Map<String, Object> row : list ) {
				 
				rptHavuzBildirimSure rpt = new rptHavuzBildirimSure();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("tarih").toString());
				} catch (Exception ex) {
					System.out.println("tarih Hata :" + ex.toString());
				}
				try {		
					rpt.setagent(row.get("agent").toString());
				} catch (Exception ex) {
					System.out.println("setagent Hata :" + ex.toString());
				}
				try {		
					rpt.sethavuz(row.get("havuz").toString());
				} catch (Exception ex) {
					System.out.println("sethavuz Hata :" + ex.toString());
				}
				try {		
					rpt.setsube(row.get("sube").toString());
				} catch (Exception ex) {
					System.out.println("setsube Hata :" + ex.toString());
				}
				try {		
					rpt.setkurum(row.get("kurum").toString());
				} catch (Exception ex) {
					System.out.println("setkurum Hata :" + ex.toString());
				}
				try {		
					rpt.setsure(row.get("sure").toString());
				} catch (Exception ex) {
					System.out.println("setsure Hata :" + ex.toString());
				}
				try {		
					rpt.setyon_tarih(row.get("yon_tarih").toString());
				} catch (Exception ex) {
					System.out.println("setyon_tarih Hata :" + ex.toString());
				}
				try {		
					rpt.setstatu(row.get("statu").toString());
				} catch (Exception ex) {
					System.out.println("setstatu Hata :" + ex.toString());
				}

	
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> countHavuzBildirimSureRapor(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        String fk_havuz, 
        Integer userId, 
        String yetki
	 ) {
		String sql1 = "call rptHavuzBildirimSure (?,?,?,?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			tip_level1, 
			fk_il, 
			fk_sube, 
			fk_havuz, 
			userId, 
			yetki
		  });
		 return getCountList(rows);
	 } 


	 @Override
	 public List<rptSektorBazinda> SektorBazindaRapor(
        String sdate, 
        String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
        String yetki
	 ) {
		
		   String sql1 = "call rptSektorBazinda (?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptSektorBazinda> ret = new ArrayList<rptSektorBazinda>();
			 for (Map<String, Object> row : list ) {
				 
				rptSektorBazinda rpt = new rptSektorBazinda();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setadet(row.get("adet").toString());
				} catch (Exception ex) {
					System.out.println("adet Hata :" + ex.toString());
				}
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> countSektorBazindaRapor(
        String sdate, 
        String fdate,  
        Integer userId, 
        String yetki
	 ) {
		String sql1 = "call rptSektorBazinda (?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			userId, 
			yetki
		  });
		 return getCountList(rows);
	 } 
  

	 @Override
	 public List<rptSektorBazindaIhbarSikayet> SektorBazindaIhbarSikayet(
        String sdate, 
		String fdate, 
		Integer fk_sektor, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki

	 ) {
		
		   String sql1 = "call rptSektorBazindaIhbarSikayet (?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			fk_sektor, 
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptSektorBazindaIhbarSikayet> ret = new ArrayList<rptSektorBazindaIhbarSikayet>();
			 for (Map<String, Object> row : list ) {
				 
				rptSektorBazindaIhbarSikayet rpt = new rptSektorBazindaIhbarSikayet();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setkart(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("kart Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("tarih Hata :" + ex.toString());
				}
				try {		
					rpt.setagent(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("agent Hata :" + ex.toString());
				}
				try {		
					rpt.setkonu(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("konu Hata :" + ex.toString());
				}
				try {		
					rpt.setstatu(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("statu Hata :" + ex.toString());
				}
				try {		
					rpt.setil(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("il Hata :" + ex.toString());
				}
				try {		
					rpt.setsure(row.get("il").toString());
				} catch (Exception ex) {
					System.out.println("sure Hata :" + ex.toString());
				}
				try {		
					rpt.setsektor(row.get("sektor").toString());
				} catch (Exception ex) {
					System.out.println("sektor Hata :" + ex.toString());
				}
		
				
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> countSektorBazindaIhbarSikayet(
        String sdate, 
		String fdate, 
		Integer fk_sektor, 
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptSektorBazindaIhbarSikayet (?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			fk_sektor, 
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }

	     

	 @Override
	 public List<rptIlBazinda> IlBazindaRapor(
        String sdate, 
		String fdate, 
		Integer fk_gorev_statu, 
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptIlBazinda (?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			fk_gorev_statu, 
			userId,
			yetki
		    });
		   List<rptIlBazinda> ret = new ArrayList<rptIlBazinda>();
			 for (Map<String, Object> row : list ) {
				 
				rptIlBazinda rpt = new rptIlBazinda();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setadet(row.get("adet").toString());
				} catch (Exception ex) {
					System.out.println("adet Hata :" + ex.toString());
				}
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 
	     

	 @Override
	 public List<rptKonularaGore> KonularaGore(
		String sdate, 
		String fdate, 
		Integer statu,
		Integer tip_level1, 
		Integer tip_level2, 
		Integer tip_level3, 
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptKonularaGore (?,?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			tip_level1, 
			tip_level2, 
			tip_level3, 
			userId,
			yetki
		    });
		   List<rptKonularaGore> ret = new ArrayList<rptKonularaGore>();
			 for (Map<String, Object> row : list ) {
				 
				rptKonularaGore rpt = new rptKonularaGore();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setadet(row.get("adet").toString());
				} catch (Exception ex) {
					System.out.println("adet Hata :" + ex.toString());
				}
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }


	     

	 @Override
	 public List<rptHavuzOzet> HavuzOzet(
		Integer skip,
		Integer take,
		Integer userId
	 ) {
		
		   String sql1 = "call rpthavuzOzet (?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] {  
			skip,
			take,
			userId
		    });
		   List<rptHavuzOzet> ret = new ArrayList<rptHavuzOzet>();
			 for (Map<String, Object> row : list ) {
				 
				rptHavuzOzet rpt = new rptHavuzOzet();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setHavuz(row.get("havuz").toString());
				} catch (Exception ex) {
					System.out.println("setHavuz Hata :" + ex.toString());
				}
				try {		
					rpt.settoplam(row.get("toplam").toString());
				} catch (Exception ex) {
					System.out.println("settoplam Hata :" + ex.toString());
				}
				try {		
					rpt.setikigun(row.get("ikigun").toString());
				} catch (Exception ex) {
					System.out.println("setikigun Hata :" + ex.toString());
				}
				try {		
					rpt.setongun(row.get("ongun").toString());
				} catch (Exception ex) {
					System.out.println("setongun Hata :" + ex.toString());
				}
				try {		
					rpt.setbiray(row.get("biray").toString());
				} catch (Exception ex) {
					System.out.println("setbiray Hata :" + ex.toString());
				}
				try {		
					rpt.setikiay(row.get("ikiay").toString());
				} catch (Exception ex) {
					System.out.println("setikiay Hata :" + ex.toString());
				}
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> countHavuzOzet(
		Integer userId
	 ) {
		String sql1 = "call rpthavuzOzetCount (?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			userId
		  });
		 return getCountList(rows);
	 }
  	     

	 @Override
	 public List<rptSgkBilgilendirme> SgkBilgilendirme(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptSgkBilgilendirme (?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptSgkBilgilendirme> ret = new ArrayList<rptSgkBilgilendirme>();
			 for (Map<String, Object> row : list ) {
				 
				rptSgkBilgilendirme rpt = new rptSgkBilgilendirme();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("tarih").toString());
				} catch (Exception ex) {
					System.out.println("settarih Hata :" + ex.toString());
				}
				try {		
					rpt.setadi(row.get("adi").toString());
				} catch (Exception ex) {
					System.out.println("setadi Hata :" + ex.toString());
				}
				try {		
					rpt.settc(row.get("tc").toString());
				} catch (Exception ex) {
					System.out.println("settc Hata :" + ex.toString());
				}
				try {		
					rpt.settel(row.get("tel").toString());
				} catch (Exception ex) {
					System.out.println("settel Hata :" + ex.toString());
				}
				try {		
					rpt.setemail(row.get("email").toString());
				} catch (Exception ex) {
					System.out.println("setemail Hata :" + ex.toString());
				}
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> countSgkBilgilendirme(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptSgkBilgilendirme (?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }


	 @Override
	 public List<rptIvrAgent> IvrAgent(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptIvrAgent (?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptIvrAgent> ret = new ArrayList<rptIvrAgent>();
			 for (Map<String, Object> row : list ) {
				 
				rptIvrAgent rpt = new rptIvrAgent();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("tarih").toString());
				} catch (Exception ex) {
					System.out.println("settarih Hata :" + ex.toString());
				}
				try {		
					rpt.setlokasyon(row.get("lokasyon").toString());
				} catch (Exception ex) {
					System.out.println("setlokasyon Hata :" + ex.toString());
				}
				try {		
					rpt.setdahili(row.get("dahili").toString());
				} catch (Exception ex) {
					System.out.println("setdahili Hata :" + ex.toString());
				}
				try {		
					rpt.setadi(row.get("adi").toString());
				} catch (Exception ex) {
					System.out.println("setadi Hata :" + ex.toString());
				}
				try {		
					rpt.setarayan_no(row.get("arayan_no").toString());
				} catch (Exception ex) {
					System.out.println("setarayan_no Hata :" + ex.toString());
				}
				try {		
					rpt.setkriter_1(row.get("skriter_1").toString());
				} catch (Exception ex) {
					System.out.println("setkriter_1 Hata :" + ex.toString());
				}
				try {		
					rpt.setkriter_2(row.get("kriter_2").toString());
				} catch (Exception ex) {
					System.out.println("setkriter_2 Hata :" + ex.toString());
				}
				try {		
					rpt.setkriter_3(row.get("kriter_3").toString());
				} catch (Exception ex) {
					System.out.println("setkriter_3 Hata :" + ex.toString());
				}
				try {		
					rpt.setkriter_4(row.get("kriter_4").toString());
				} catch (Exception ex) {
					System.out.println("setkriter_4 Hata :" + ex.toString());
				}
				try {		
					rpt.setkriter_5(row.get("kriter_5").toString());
				} catch (Exception ex) {
					System.out.println("setkriter_5 Hata :" + ex.toString());
				}
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> IvrAgentCount(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptIvrAgent (?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }
  	     

	 @Override
	 public List<rptTaseronHavuzu> TaseronHavuzu(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptTaseronHavuz (?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptTaseronHavuzu> ret = new ArrayList<rptTaseronHavuzu>();
			 for (Map<String, Object> row : list ) {
				 
				rptTaseronHavuzu rpt = new rptTaseronHavuzu();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("tarih").toString());
				} catch (Exception ex) {
					System.out.println("settarih Hata :" + ex.toString());
				}
				try {		
					rpt.setaramakanali(row.get("aramakanali").toString());
				} catch (Exception ex) {
					System.out.println("setaramakanali Hata :" + ex.toString());
				}
				try {		
					rpt.setbildirim_no(row.get("bildirim_no").toString());
				} catch (Exception ex) {
					System.out.println("setbildirim_no Hata :" + ex.toString());
				}
				try {		
					rpt.setvatandas(row.get("vatandas").toString());
				} catch (Exception ex) {
					System.out.println("setvatandas Hata :" + ex.toString());
				}
				try {		
					rpt.settc(row.get("tc").toString());
				} catch (Exception ex) {
					System.out.println("settc Hata :" + ex.toString());
				}
				try {		
					rpt.setaciklama(row.get("aciklama").toString());
				} catch (Exception ex) {
					System.out.println("setaciklama Hata :" + ex.toString());
				}
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> TaseronHavuzuCount(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptTaseronHavuz (?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }  	     


	 
	 @Override
	 public List<rptSesKayit> SesKayit(
		String sdate, 
		String fdate, 
		String tc, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptSesKayit (?,?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			tc, 
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptSesKayit> ret = new ArrayList<rptSesKayit>();
			 for (Map<String, Object> row : list ) {
				 
				rptSesKayit rpt = new rptSesKayit();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("tarih").toString());
				} catch (Exception ex) {
					System.out.println("settarih Hata :" + ex.toString());
				}
				try {		
					rpt.setvatandas(row.get("vatandas").toString());
				} catch (Exception ex) {
					System.out.println("setvatandas Hata :" + ex.toString());
				}
				try {		
					rpt.setsure(row.get("sure").toString());
				} catch (Exception ex) {
					System.out.println("setsure Hata :" + ex.toString());
				}
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> SesKayitCount(
		String sdate, 
		String fdate, 
		String tc, 
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptTaseronHavuzCount (?,?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			tc, 
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }  	






	 @Override
	 public List<rptKurumKapanmaIstatistik> KurumKapanmaIstatistik(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptKurumKapanmaIstatistik (?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			userId,
			yetki
		    });
		   List<rptKurumKapanmaIstatistik> ret = new ArrayList<rptKurumKapanmaIstatistik>();
			 for (Map<String, Object> row : list ) {
				 
				rptKurumKapanmaIstatistik rpt = new rptKurumKapanmaIstatistik();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setkurum(row.get("kurum").toString());
				} catch (Exception ex) {
					System.out.println("setkurum Hata :" + ex.toString());
				}
				try {		
					rpt.setanlik_kapatilan(row.get("anlik_kapatilan").toString());
				} catch (Exception ex) {
					System.out.println("setanlik_kapatilan Hata :" + ex.toString());
				}
				try {		
					rpt.setkuruma_yonlendirilen(row.get("kuruma_yonlendirilen").toString());
				} catch (Exception ex) {
					System.out.println("setkuruma_yonlendirilen Hata :" + ex.toString());
				}
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
  	     






	 @Override
	 public List<rptBirimlereGoreKullaniciSayisi> BirimlereGoreKullaniciSayisi(
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptBirimlereGoreKullaniciSayisi (?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] {  
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptBirimlereGoreKullaniciSayisi> ret = new ArrayList<rptBirimlereGoreKullaniciSayisi>();
			 for (Map<String, Object> row : list ) {
				 
				rptBirimlereGoreKullaniciSayisi rpt = new rptBirimlereGoreKullaniciSayisi();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setkurum(row.get("kurum").toString());
				} catch (Exception ex) {
					System.out.println("setkurum Hata :" + ex.toString());
				}
				try {		
					rpt.setaktif(row.get("aktif").toString());
				} catch (Exception ex) {
					System.out.println("setaktif Hata :" + ex.toString());
				}
				try {		
					rpt.setpasif(row.get("pasif").toString());
				} catch (Exception ex) {
					System.out.println("setpasif Hata :" + ex.toString());
				}
				ret.add(rpt);
	   }
		 return ret;
	 }
	 public  List<vTotal> BirimlereGoreKullaniciSayisiCount(
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptBirimlereGoreKullaniciSayisiCount (?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			userId,
			yetki
		  });
		 return getCountList(rows);
	 }  
	







	 @Override
	 public List<rptKurumBazindaZamanindaKapanan> KurumBazindaZamanindaKapanan(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptKurumBazindaZamanindaKapanan (?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] {  
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptKurumBazindaZamanindaKapanan> ret = new ArrayList<rptKurumBazindaZamanindaKapanan>();
			 for (Map<String, Object> row : list ) {
				 
				rptKurumBazindaZamanindaKapanan rpt = new rptKurumBazindaZamanindaKapanan();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setkurum(row.get("kurum").toString());
				} catch (Exception ex) {
					System.out.println("setkurum Hata :" + ex.toString());
				}
				try {		
					rpt.setbirim(row.get("birim").toString());
				} catch (Exception ex) {
					System.out.println("setbirim Hata :" + ex.toString());
				}
				try {		
					rpt.settoplam(row.get("toplam").toString());
				} catch (Exception ex) {
					System.out.println("settoplam Hata :" + ex.toString());
				}
				try {		
					rpt.setzamaninda_kapanan(row.get("zamaninda_kapanan").toString());
				} catch (Exception ex) {
					System.out.println("setzamaninda_kapanan Hata :" + ex.toString());
				}
				ret.add(rpt);
	   }
		 return ret;
	 }
	 public  List<vTotal> KurumBazindaZamanindaKapananCount(
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptKurumBazindaZamanindaKapananCount (?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			userId,
			yetki
		  });
		 return getCountList(rows);
	 } 
	






	@Override
	 public List<rptIvrRapor> IvrRapor(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptIvrRapor (?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			userId,
			yetki
		    });
		   List<rptIvrRapor> ret = new ArrayList<rptIvrRapor>();
			 for (Map<String, Object> row : list ) {
				 
				rptIvrRapor rpt = new rptIvrRapor();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setkatilmayan(row.get("katilmayan").toString());
				} catch (Exception ex) {
					System.out.println("setkatilmayan Hata :" + ex.toString());
				}
				try {		
					rpt.setolumlu(row.get("olumlu").toString());
				} catch (Exception ex) {
					System.out.println("setolumlu Hata :" + ex.toString());
				}
				try {		
					rpt.setolumsuz(row.get("olumsuz").toString());
				} catch (Exception ex) {
					System.out.println("setolumsuz Hata :" + ex.toString());
				}
				try {		
					rpt.sets1olumlu(row.get("s1olumlu").toString());
				} catch (Exception ex) {
					System.out.println("s1olumlu Hata :" + ex.toString());
				}
				try {		
					rpt.sets2olumlu(row.get("s2olumlu").toString());
				} catch (Exception ex) {
					System.out.println("s2olumlu Hata :" + ex.toString());
				}
				try {		
					rpt.sets3olumlu(row.get("s3olumlu").toString());
				} catch (Exception ex) {
					System.out.println("s3olumlu Hata :" + ex.toString());
				}
				try {		
					rpt.sets4olumlu(row.get("s4olumlu").toString());
				} catch (Exception ex) {
					System.out.println("s4olumlu Hata :" + ex.toString());
				}
				try {		
					rpt.sets5olumlu(row.get("s5olumlu").toString());
				} catch (Exception ex) {
					System.out.println("s5olumlu Hata :" + ex.toString());
				}
				try {		
					rpt.sets1olumsuz(row.get("s1olumsuz").toString());
				} catch (Exception ex) {
					System.out.println("s1olumsuz Hata :" + ex.toString());
				}
				try {		
					rpt.sets2olumsuz(row.get("s2olumsuz").toString());
				} catch (Exception ex) {
					System.out.println("s2olumsuz Hata :" + ex.toString());
				}
				try {		
					rpt.sets3olumsuz(row.get("s3olumsuz").toString());
				} catch (Exception ex) {
					System.out.println("s3olumsuz Hata :" + ex.toString());
				}
				try {		
					rpt.sets4olumsuz(row.get("s4olumsuz").toString());
				} catch (Exception ex) {
					System.out.println("s4olumsuz Hata :" + ex.toString());
				}
				try {		
					rpt.sets5olumsuz(row.get("s5olumsuz").toString());
				} catch (Exception ex) {
					System.out.println("s5olumsuz Hata :" + ex.toString());
				}
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	
	









	@Override
	 public List<rptSmsRapor> SmsRapor(
		Integer skip,
		Integer take,
		String tipi,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptSmsRapor (?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			skip,
			take,
			tipi,
			userId,
			yetki
		    });
		   List<rptSmsRapor> ret = new ArrayList<rptSmsRapor>();
			 for (Map<String, Object> row : list ) {
				 
				rptSmsRapor rpt = new rptSmsRapor();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.setnumara_email(row.get("numara_email").toString());
				} catch (Exception ex) {
					System.out.println("setnumara_email Hata :" + ex.toString());
				}
				try {		
					rpt.setbildirim_no(row.get("bildirim_no").toString());
				} catch (Exception ex) {
					System.out.println("setbildirim_no Hata :" + ex.toString());
				}
				try {		
					rpt.setstatu(row.get("statu").toString());
				} catch (Exception ex) {
					System.out.println("setstatu Hata :" + ex.toString());
				}
				ret.add(rpt);
		 
	   }
		 return ret;
	 }	
	 public  List<vTotal> SmsRaporCount(
		String tipi,
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptSmsRaporCount (?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			tipi,
			userId,
			yetki
		  });
		 return getCountList(rows);
	 } 









	@Override
	 public List<rptPeriyodikRapor> PeriyodikRaporGelenCagri(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptPeriyodikRaporGelenCagri (?,?,?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptPeriyodikRapor> ret = new ArrayList<rptPeriyodikRapor>();
			 for (Map<String, Object> row : list ) {
				 
				rptPeriyodikRapor rpt = new rptPeriyodikRapor();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.settoplam_gelen(row.get("toplam_gelen").toString());
				} catch (Exception ex) {
					System.out.println("settoplam_gelen Hata :" + ex.toString());
				}
				try {		
					rpt.settoplam_cevaplanan(row.get("toplam_cevaplanan").toString());
				} catch (Exception ex) {
					System.out.println("settoplam_cevaplanan Hata :" + ex.toString());
				}
				try {		
					rpt.settoplam_kayip(row.get("toplam_kayip").toString());
				} catch (Exception ex) {
					System.out.println("settoplam_kayip Hata :" + ex.toString());
				}
				try {		
					rpt.setORT_GOR_SURE(row.get("ORT_GOR_SURE").toString());
				} catch (Exception ex) {
					System.out.println("setORT_GOR_SURE Hata :" + ex.toString());
				}
				try {		
					rpt.setTOPL_SURE(row.get("TOPL_SURE").toString());
				} catch (Exception ex) {
					System.out.println("setTOPL_SURE Hata :" + ex.toString());
				}
				try {		
					rpt.setORT_BEKL_SURE(row.get("ORT_BEKL_SURE").toString());
				} catch (Exception ex) {
					System.out.println("setORT_BEKL_SURE Hata :" + ex.toString());
				}
				try {		
					rpt.settoplam_bildirim(row.get("toplam_bildirim").toString());
				} catch (Exception ex) {
					System.out.println("settoplam_bildirim Hata :" + ex.toString());
				}
				try {		
					rpt.setilk_kontak(row.get("ilk_kontak").toString());
				} catch (Exception ex) {
					System.out.println("setilk_kontak Hata :" + ex.toString());
				}
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }
	 public  List<vTotal> PeriyodikRaporGelenCagriCount(
		String sdate,
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		String sql1 = "call rptPeriyodikRaporGelenCagriCount (?,?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
			sdate,
			fdate,
			userId,
			yetki
		  });
		 return getCountList(rows);
	 } 
	
	 



	 @Override
	 public List<rptPeriyodikRapor> PeriyodikRaporIlkOnKonu(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptPeriyodikRaporIlkOnKonu (?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			userId,
			yetki
		    });
		   List<rptPeriyodikRapor> ret = new ArrayList<rptPeriyodikRapor>();
			 for (Map<String, Object> row : list ) {
				 
				rptPeriyodikRapor rpt = new rptPeriyodikRapor();	
				try {		
					rpt.setkonu(row.get("konu").toString());
				} catch (Exception ex) {
					System.out.println("konu Hata :" + ex.toString());
				}
				try {		
					rpt.setsay((Integer)row.get("say"));
				} catch (Exception ex) {
					System.out.println("say Hata :" + ex.toString());
				}		
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }




	 @Override
	 public List<rptPeriyodikRapor> PeriyodikRaporDisAramaSms(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptPeriyodikRaporDisAramaSms (?,?,?,?)";
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate, 
			fdate, 
			userId,
			yetki
		    });
		   List<rptPeriyodikRapor> ret = new ArrayList<rptPeriyodikRapor>();
			 for (Map<String, Object> row : list ) {
				 
				rptPeriyodikRapor rpt = new rptPeriyodikRapor();	
				try {		
					rpt.setsmssay((Integer)row.get("smssay"));
				} catch (Exception ex) {
					System.out.println("smssay Hata :" + ex.toString());
				}
				try {		
					rpt.setemailsay((Integer)row.get("emailsay"));
				} catch (Exception ex) {
					System.out.println("emailsay Hata :" + ex.toString());
				}
				try {		
					rpt.setsure((Integer)row.get("sure"));
				} catch (Exception ex) {
					System.out.println("sure Hata :" + ex.toString());
				}
				try {		
					rpt.setdissay((Integer)row.get("dissay"));
				} catch (Exception ex) {
					System.out.println("dissay Hata :" + ex.toString());
				}														
			
				ret.add(rpt);
		 
	   }
		 return ret;
	 }








	@Override
	 public List<rptRaporLog> RaporLog(
		String sdate,
		String fdate,
		Integer fk_user,
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	 ) {
		
		   String sql1 = "call rptRaporLog (?,?,?,?,?,?,?)";
		   System.out.println("call rptRaporLog ('"+sdate+"','"+fdate+"',"+fk_user+","+skip+","+take+","+userId+",'"+yetki+"')");
		   List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { 
			sdate,
			fdate,
			fk_user,
			skip,
			take,
			userId,
			yetki
		    });
		   List<rptRaporLog> ret = new ArrayList<rptRaporLog>();
			 for (Map<String, Object> row : list ) {
				 
				rptRaporLog rpt = new rptRaporLog();	
				try {		
					rpt.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
				}
				try {		
					rpt.settarih(row.get("fulltarih").toString());
				} catch (Exception ex) {
					System.out.println("settarih Hata :" + ex.toString());
				}
				try {		
					rpt.setrapor(row.get("rapor").toString());
				} catch (Exception ex) {
					System.out.println("setrapor Hata :" + ex.toString());
				}
				try {		
					rpt.setkullanici(row.get("kullanici").toString());
				} catch (Exception ex) {
					System.out.println("setkullanici Hata :" + ex.toString());
				}
				ret.add(rpt);
		 
	   }
		 return ret;
	 }	
	 @Override
	 public List<vTotal> countRaporLog(
		String sdate,
		String fdate,
		Integer fk_user,
		Integer userId,
		String yetki
	 ) {
		 String sql1 = "call rptRaporLogCount (?,?,?,?,?)";
		 List<Map<String, Object>> rows = jdbcro.queryForList(sql1,new Object[] {
			 sdate,
			fdate,
			fk_user,
			userId,
			yetki });
		 return getCountList(rows);
	 }
		
	 /* @Override
	 public List<Map<String,Object>> KurumSubeHavuzIstatistikRaporu(      
	 String sdate, 
	 String fdate, 
	 Integer tip_level1, 
	 Integer fk_il, 
	 Integer fk_sube, 
	 Integer fk_havuz,
	 Integer skip,
	 Integer take,
	 Integer userId,
	 vUSERYETKI yetki
	 ) {
		System.out.println("call rptKurumSubeHavuzIstatistik("+sdate+","+fdate+","+tip_level1+","+fk_il+","+fk_sube+","+fk_havuz+","+skip+","+take+","+userId+")"+"***************************************");

		String sql1 = "call rptKurumSubeHavuzIstatistik (?,?,?,?,?,?,?,?,?)";
		List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { sdate,fdate,tip_level1,fk_il,fk_sube,fk_havuz,skip,take,userId });
		List<Map<String, Object>> ret = new ArrayList<Map<String, Object>>();

		return list;
	 }*/
}

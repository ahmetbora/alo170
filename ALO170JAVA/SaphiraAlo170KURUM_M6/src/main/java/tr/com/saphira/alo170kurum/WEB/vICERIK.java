package tr.com.saphira.alo170kurum.WEB;

public class vICERIK {
	private Integer id;
	private String baslik;
	private String kisa_aciklama;
	private String resim;
	private String icerik;
	private String url;
	public vICERIK() {
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBaslik() {
		return baslik;
	}
	public void setBaslik(String baslik) {
		this.baslik = baslik;
	}
	public String getKisa_aciklama() {
		return kisa_aciklama;
	}
	public void setKisa_aciklama(String kisa_aciklama) {
		this.kisa_aciklama = kisa_aciklama;
	}
	public String getResim() {
		return resim;
	}
	public void setResim(String resim) {
		this.resim = resim;
	}
	public String getIcerik() {
		return icerik;
	}
	public void setIcerik(String icerik) {
		this.icerik = icerik;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	

}

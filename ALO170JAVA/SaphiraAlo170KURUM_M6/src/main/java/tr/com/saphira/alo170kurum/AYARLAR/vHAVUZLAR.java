package tr.com.saphira.alo170kurum.AYARLAR;

public class vHAVUZLAR {

	private Integer id;
	private String adi;
	private String kurum;
	private String il;
	private String sube;
	private Integer havuz_sayisi;
	private Integer isActive;
	private Integer fk_konu;
	private Integer fk_sube;
	private Integer fk_il;
	private String email;
	private Integer fk_havuz;
	public vHAVUZLAR() {
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getKurum() {
		return kurum;
	}
	public void setKurum(String kurum) {
		this.kurum = kurum;
	}
	public String getIl() {
		return il;
	}
	public void setIl(String il) {
		this.il = il;
	}
	public String getSube() {
		return sube;
	}
	public void setSube(String sube) {
		this.sube = sube;
	}
	public Integer getHavuz_sayisi() {
		return havuz_sayisi;
	}
	public void setHavuz_sayisi(Integer havuz_sayisi) {
		this.havuz_sayisi = havuz_sayisi;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public Integer getFk_konu() {
		return fk_konu;
	}
	public void setFk_konu(Integer fk_konu) {
		this.fk_konu = fk_konu;
	}
	public Integer getFk_sube() {
		return fk_sube;
	}
	public void setFk_sube(Integer fk_sube) {
		this.fk_sube = fk_sube;
	}
	public Integer getFk_il() {
		return fk_il;
	}
	public void setFk_il(Integer fk_il) {
		this.fk_il = fk_il;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	/*** task havuz kullanıcı için kullanılıyor*/
	public Integer getfkHavuz() {
		return fk_havuz;
	}
	public void setfkHavuz(Integer fk_havuz) {
		this.fk_havuz = fk_havuz;
	}	
	
}

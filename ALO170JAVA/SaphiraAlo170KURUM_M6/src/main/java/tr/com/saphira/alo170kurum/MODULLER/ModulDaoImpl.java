package tr.com.saphira.alo170kurum.MODULLER;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.KART.vKART;
import tr.com.saphira.alo170kurum.model.vTotal;
import tr.com.saphira.alo170kurum.BILDIRIM.*;
@SuppressWarnings("unused")
@Transactional
@Repository
public class ModulDaoImpl implements ModulDao {
	
	 @Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;
    
	 		/***
	 * listelerde toplam kayıt sayısı için liste olusturur vTotal class içine aktarır
	 * @param rows
	 * @return
	 */
	private List<vTotal> getCountList(List<Map<String, Object>> rows) {
		List<vTotal> ret = new ArrayList<vTotal>();
		for (Map<?, ?> row : rows) {
			vTotal res = new vTotal();
			try {		
				res.setTotal((Long)row.get("total"));
			} catch (Exception ex) {
					System.out.println("total Hata :" + ex.toString());
			}
			ret.add(res);
		}
		return ret;
	}

	@Override
	public vJSONRESULT queMudahale(String ext,Integer userId,String uzunluk) {
	
		String sql1 = "call spQueMudahale (?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("0");
		try {
			jdbcro.update(sql1,new Object[] { 
				ext,userId,uzunluk
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("Hata");
		}
		return res;
		
	}
	@Override
	public List<Map<String, Object>> AgentHangup(String sdate,String fdate,Integer lokasyon,String tablo){
		String sql1 = "call spAgentHangup (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { sdate, fdate, lokasyon, tablo });
		return rows;
	}
	@Override
	public List<Map<String, Object>> AgentHangupDetay(String sdate,String fdate,Integer lokasyon,String tablo,String ext){
		String sql1 = "call spAgentHangupDetay (?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { sdate, fdate, lokasyon, tablo,ext });
		return rows;
	}
	@Override
	public List<Map<String, Object>> IvrSms(String tc){
		String sql1 = "call spIvrSmsSorgula (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { tc });
		return rows;
	}
	@Override
	public vJSONRESULT IvrSmsIptal(String tc,Integer userId) {
		String sql1 = "call spIvrSmsIptal (?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				tc,userId	
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
		
	}

	@Override
	public List<Map<String, Object>> getsss(Integer id,Integer statu,Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,Integer skip,Integer take){
		String sql1 = "call spGetSss (?,?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {id,statu,fk_sss_grup,fk_sss_altgrup,baslik,skip,take});
		return rows;
	}
	
	@Override
	public List<vTotal> countsss(Integer statu,Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik) {
		String sql1 = "call spGetSssCount (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {statu,fk_sss_grup,fk_sss_altgrup,baslik});
		return getCountList(rows);
	}
	
	@Override
	public vJSONRESULT sssGuncelle(Integer statu,Integer isWeb, Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,String aciklama,Integer userId,Integer id){
	
		String sql1 = "call spSssGuncelle (?,?,?,?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				statu,isWeb, fk_sss_grup,fk_sss_altgrup,baslik,aciklama,userId,id
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
		
	}
	@Override
	public vJSONRESULT sssKaydet(Integer statu,Integer isWeb, Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,String aciklama,Integer userId){
	
		String sql1 = "call spSssKaydet (?,?,?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				statu,isWeb, fk_sss_grup,fk_sss_altgrup,baslik,aciklama,userId
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
		
	}
	
	
	@Override
	public List<Map<String, Object>> KaraListe(Integer id,Integer isOnay,Integer skip,Integer take){
		String sql1 = "call spGetKaraListe (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {id,isOnay, skip, take});
		return rows;
	}
	@Override
	public List<vTotal> countKaraListe(Integer isOnay){
		String sql1 = "call spGetKaraListeCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {isOnay});
		return getCountList(rows);
	}

	@Override
	public vJSONRESULT KaraListeOnay(Integer id,String tarih,Integer userId){
	System.out.println("spKaraListeOnay ("+id+","+tarih+","+userId+","+0+","+0+")");
		String sql1 = "call spKaraListeOnay (?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				id,tarih,userId,0,0
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}
	@Override
	public vJSONRESULT KaraListeSuresiz(Integer id,Integer userId){
	
		String sql1 = "call spKaraListeOnay (?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				id,"",userId,1,0
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}
	@Override
	public vJSONRESULT KaraListeSil(Integer id,Integer userId){
	
		String sql1 = "call spKaraListeOnay (?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				id,"",userId,0,1
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}
	
	@Override
	public List<Map<String, Object>> VipListe(Integer skip,Integer take){
		String sql1 = "call spGetVipListe (?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {skip, take});
		return rows;
	}
	@Override
	public List<vTotal> countVipListe(){
		String sql1 = "call spGetVipListeCount ()";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		return getCountList(rows);
	}
	@Override
	public vJSONRESULT VipListeSil(Integer id){
	
		String sql1 = "call spVipListeSil (?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				id
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}
	
	@Override
	public List<Map<String, Object>> MobilUyeListe(Integer statu,String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer skip,Integer take){
		String sql1 = "call spMobilUyeListe (?,?,?,?,?,?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {statu,adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,skip,take});
		return rows;
	}
	@Override
	public List<vTotal> countMobilUyeListe(Integer statu,String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce){
		String sql1 = "call spMobilUyeListeCount (?,?,?,?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {statu,adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce});
		return getCountList(rows);
	}


	@Override
	public List<Map<String, Object>> VatandasListe(String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer skip,Integer take,Integer kayit_tipi){
		String sql1 = "call spVatandasListe (?,?,?,?,?,?,?,?,?,?,?,?)";
		String tel2=null;
		if(tel!=null){
			 tel2=Genel.formatPhone(tel);
		}
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {adi,soaydi,tc,ssk,tel,tel2,fk_ulke,fk_il,fk_ilce,skip,take,kayit_tipi});
		return rows;
	}
	@Override
	public List<vTotal> countVatandasListe(String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer kayit_tipi){
		String sql1 = "call spVatandasListeCount (?,?,?,?,?,?,?,?,?,?)";
		String tel2=null;
		if(tel!=null){
			 tel2=Genel.formatPhone(tel);
		}
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {adi,soaydi,tc,ssk,tel,tel2,fk_ulke,fk_il,fk_ilce,kayit_tipi});
		return getCountList(rows);
	}

	@Override
	public Map<String, Object> getVatandas(Integer id){
		String sql1 = "call spGetVatandas (?)";
		Map<String, Object> row=jdbcro.queryForMap(sql1,new Object[] {id});
		return row;
	}

	@Override
	public vJSONRESULT VatandasGuncelle(Integer userId,Integer id,String adi,String soyadi,String tckimklik,String dt,String tel,String gsm,Integer fk_egitim_durumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,String ssk,String email,Integer fk_cinsiyet,String fax){
		System.out.println(id+","+adi+","+soyadi+","+tckimklik+","+dt+","+tel+","+gsm+","+fk_egitim_durumu+","+fk_ulke+","+fk_il+","+fk_ilce+","+ssk+","+email+","+fk_cinsiyet+","+fax);
		String sql1 = "call spVatandasGuncelle (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				userId,id,adi,soyadi,tckimklik,dt,tel,gsm,fk_egitim_durumu,fk_ulke,fk_il,fk_ilce,ssk,email,fk_cinsiyet,fax
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}

	@Override
	public vJSONRESULT VatandasOnayla(Integer id,Integer statu,String sifre){
		String sql1 = "call spVatandasOnayla (?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				id,statu,sifre
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}


	@Override
	public List<Map<String, Object>> izinTalepSv(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip, Integer skip,Integer take){
		String sql1 = "call spIzinTalepListeSV (?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {fk_lokasyon, userId, isActive, tip, skip,take});
		return rows;
	}
	@Override
	public List<vTotal> countizinTalepSv(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip){
		String sql1 = "call spIzinTalepListeSVCount (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {fk_lokasyon, userId, isActive, tip});
		return getCountList(rows);
	}
	@Override
	public List<Map<String, Object>> izinTalepTk(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip, Integer skip,Integer take){
		String sql1 = "call spIzinTalepListeTK (?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {fk_lokasyon, userId, isActive, tip, skip,take});
		return rows;
	}
	@Override
	public List<vTotal> countizinTalepTk(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip){
		String sql1 = "call spIzinTalepListeTKCount (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {fk_lokasyon, userId, isActive, tip});
		return getCountList(rows);
	}
	@Override
	public List<Map<String, Object>> izinTalepVT(Integer userId, Integer isActive, Integer skip,Integer take){
		String sql1 = "call spIzinTalepListeVT (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {userId, isActive, skip,take});
		return rows;
	}
	@Override
	public List<vTotal> countizinTalepVT(Integer userId, Integer isActive){
		String sql1 = "call spIzinTalepListeVTCount (?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId, isActive});
		return getCountList(rows);
	}
	@Override
	public List<Map<String, Object>> izinTalepAD(Integer userId, Integer skip,Integer take){
		String sql1 = "call spIzinTalepListeAD (?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {userId, skip,take});
		return rows;
	}
	@Override
	public List<vTotal> countizinTalepAD(Integer userId){
		String sql1 = "call spIzinTalepListeADCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId});
		return getCountList(rows);
	}
	
	@Override
	public vJSONRESULT izinTalepKaydet(String sdate, String ssaat,String fdate, String fsaat, Integer fk_sebep,String aciklama,Integer fk_agent,Integer fk_tipi,Integer fk_agent_,
	Integer fk_lokasyon,Integer admin_onay,Integer islem_admin,Integer tl_onay,Integer islem_tl,Integer sv_onay,Integer islem_sv,Integer isActive,String userFkRol
	){
		String sql1 = "call spizinTalepKaydet (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				sdate, ssaat,fdate, fsaat, fk_sebep,aciklama,fk_agent,fk_tipi,fk_agent_,fk_lokasyon,admin_onay,islem_admin,tl_onay,islem_tl,sv_onay,islem_sv,isActive,userFkRol
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}

	

	@Override
	public List<Map<String, Object>> izinTalepOnayListe(Integer fk_rol,Integer fk_lokasyon,Integer skip,Integer take){
		String sql1 = "call spIzinTalepOnayListe (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {fk_rol,fk_lokasyon,skip,take});
		return rows;
	}
	@Override
	public List<vTotal> countizinTalepOnayListe(Integer fk_rol,Integer fk_lokasyon){
		String sql1 = "call spIzinTalepOnayListeCount (?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {fk_rol,fk_lokasyon});
		return getCountList(rows);
	}

	@Override
	public vJSONRESULT izinTalepOnay(Integer userId,Integer id,Integer fk_rol,Integer onay){
		String sql1 = "call spizinTalepOnay (?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				userId,id,fk_rol,onay
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}

	@Override
	public List<Map<String, Object>> ScriptKonulari(Integer skip,Integer take){
		String sql1 = "select * from SMDR_1002.bildirim_script_konu where isActive=1 limit ?,?";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {skip,take});
		return rows;
	}
	@Override
	public List<vTotal> countScriptKonulari(){
		String sql1 = "select COUNT(*) total from SMDR_1002.bildirim_script_konu where isActive=1";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		return getCountList(rows);
	}

	
	@Override
	public vJSONRESULT ScriptKonuUpdate(Integer id,String adi){
		String sql1 = "call spScriptKonuUpdate (?,?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				id,adi
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}
	@Override
	public vJSONRESULT ScriptKonuInsert(String adi){
		String sql1 = "call spScriptKonuInsert (?)";
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				adi
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}

	@Override
	public List<Map<String, Object>> ScriptListe(Integer skip,Integer take){
		String sql1 = "call spScriptListe(?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {skip,take});
		return rows;
	}
	@Override
	public List<vTotal> countScriptListe(){
		String sql1 = "call spScriptListeCount()";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		return getCountList(rows);
	}
	@Override
	public Map<String, Object> ScriptDetay(Integer id){
		String sql1 = "call spScriptDetay (?)";
		Map<String, Object>  rows = jdbcro.queryForMap(sql1,new Object[] { id });
		return rows;
	}
	@Override
	public vJSONRESULT ScriptKaydet(String fk_parent,String fk_aramasebebi,String tip_level1,String tip_level2,String tip_level3,String icerik,String aciklama,String id){
		String sql1 = "call spScriptKaydet (?,?,?,?,?,?,?,?)";
		
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("1");
		try {
			jdbcro.update(sql1,new Object[] { 
				fk_parent,fk_aramasebebi,tip_level1,tip_level2,tip_level3,icerik,aciklama,id
			});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("İşlem Başarısız");
		}
		return res;
	}
}

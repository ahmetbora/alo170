package tr.com.saphira.alo170kurum.RAPOR;


import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lowagie.text.DocumentException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.AYARLAR.AyarlarService;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKANALI;
import tr.com.saphira.alo170kurum.AYARLAR.vILLER;
import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.AYARLAR.vKURUMLAR;
import tr.com.saphira.alo170kurum.LOOKUP.LOOKUPService;
import tr.com.saphira.alo170kurum.model.Lookup;
import tr.com.saphira.alo170kurum.model.vTotal;

@SuppressWarnings("unused")
@Controller
@EnableWebSecurity
@RequestMapping(value="/rapor")
public class RaporController {
    
    @Autowired 
	private RaporService service;
    
    @Autowired 
	private AyarlarService ayarlarService;
    
    @Autowired  
	private HttpServletRequest request;
    
    @Autowired
	LOOKUPService lookupService;
	
	@Autowired
    private JavaMailSender javaMailSender;
    

    @RequestMapping(value="", method= RequestMethod.GET)
	public ModelAndView home(HttpSession httpSession) {
		ModelAndView model = new ModelAndView("RAPOR/Home");
		return model;
	}

    // KURUM SUBE HAVUZ İSTATİSTİK
	@RequestMapping(value="/kshistatistikraporu", method= RequestMethod.GET)
	public ModelAndView istatistikraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/KSHIstatistikRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");

   
        model.addObject("js",js);
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
		model.addObject("reportName","Kurum / Şube / Havuz İstatistik Raporu");
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
		return model;
	}
   
	@RequestMapping(value="/kshistatistikraporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> kshistatistikraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer tip_level1=0;
        Integer fk_il=0;
        Integer fk_sube=0;
        Integer fk_havuz=0;
        Integer fk_konu=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}

		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}

		try {
            tip_level1=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level1=0;
		}	

		try {
            fk_il=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_il=0;
		}	

		try {
            fk_sube=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_sube=0;
		}	

		try {
            fk_havuz=filter.getAsJsonObject().getAsJsonArray("filters").get(5)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_havuz=0;
		}		
		try {
            fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(6)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_konu=0;
		}		


		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}
		
		List<rptKurumSubeIstatistik>  liste =  service.KurumSubeHavuzIstatistikRaporu(sdate, fdate, tip_level1, fk_il, fk_sube, fk_havuz, skip, take, userId, session.getAttribute("User_ROL").toString());
		List<vTotal> say = service.countKurumSubeHavuzIstatistikRaporu(sdate, fdate, tip_level1, fk_il, fk_sube, fk_havuz, userId, session.getAttribute("User_ROL").toString());
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}
    
	@GetMapping(value="/kshistatistikraporupdf")
    public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf;charset=UTF-8");
		response.setCharacterEncoding("utf-8");
		
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_1.pdf";
        response.setHeader(headerKey, headerValue);
         
        
		List<vKULLANICILAR>  vKullanicilar =  ayarlarService.KullaniciFiltreListesi(null, null, null,1,0,0,20000);
        UserPDFExporter exporter = new UserPDFExporter(vKullanicilar);
        exporter.export(response);
         
    }
	//xls export java ile cok fazla bekliyor. son eklediğim deneme void kullanılınca kasma olmuyor onu deniyecez
	@GetMapping("/kshistatistikraporuxls")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_1.xlsx";
        response.setHeader(headerKey, headerValue);
         
        List<vKULLANICILAR>  vKullanicilar =  ayarlarService.KullaniciFiltreListesi(null, null, null,1,0,0,1000);
         
        UserExcelExporter excelExporter = new UserExcelExporter(vKullanicilar);
         
        excelExporter.deneme(response);    
    } 



    // KURUM KULLANICI İSTATİSTİK
    @RequestMapping(value="/kurumkullaniciistatistikraporu", method= RequestMethod.GET)
	public ModelAndView kurumkullaniciistatistikraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/KurumKullaniciIstatistikRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
		model.addObject("reportName","Kurum Kullanıcı İstatistik Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/kurumkullaniciistatistikraporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> kurumkullaniciistatistikraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);

		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer tip_level1=0;
        Integer fk_il=0;
        Integer fk_sube=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}

		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}

		try {
            tip_level1=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level1=0;
		}	

		try {
            fk_il=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_il=0;
		}	

		try {
            fk_sube=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_sube=0;
		}			
        

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptKurumKullaniciIstatistik>  liste =  service.KurumKullaniciIstatistikRaporu(sdate, fdate, tip_level1, fk_il, fk_sube, skip, take, userId, session.getAttribute("User_ROL").toString());
		List<vTotal> say = service.countKurumKullaniciIstatistikRaporu(sdate, fdate, tip_level1, fk_il, fk_sube, userId, session.getAttribute("User_ROL").toString());
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

    
	

    // BİLDİRİM DETAY RAPORU
    @RequestMapping(value="/bildirimdetayraporu", method= RequestMethod.GET)
	public ModelAndView bildirimdetayraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/BildirimDetayRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		String sdate;
		try {
			sdate=session.getAttribute("rptBildirimDetay_sdate").toString();
		} catch (Exception e) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
  			LocalDateTime now = LocalDateTime.now();  
			sdate=now.toString();
		}
		String fdate;
		try {
			fdate=session.getAttribute("rptBildirimDetay_fdate").toString();
		} catch (Exception e) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			LocalDateTime now = LocalDateTime.now();  
		  	fdate=now.toString();
		}
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
		List<Lookup> aramakanalitipi=lookupService.getAramaKanaliTipi();
		List<Lookup> taskStatu = lookupService.getTaskStatu(0);
		List<vARAMAKANALI> aramaSebebi = lookupService.getAramaKanaliByTip(0);
        model.addObject("reportName","Bildirim Detay Raporu");
		model.addObject("iller",iller);
		model.addObject("taskStatu",taskStatu);
		model.addObject("aramakanalitipi",aramakanalitipi);
		model.addObject("kurumlar",kurumlar);
		model.addObject("aramaSebebi",aramaSebebi);
		model.addObject("sdate",sdate);
		model.addObject("fdate",fdate);
        model.addObject("reportName","Bildirim Detay Raporu");
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/bildirimdetayraporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> bildirimdetayraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);

		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer tip_level1=0;
        Integer tip_level2=0;
        Integer tip_level3=0;
        Integer fk_il=0;
        Integer fk_ilce=0;
        Integer fk_havuz=0;
        Integer fk_gorev_statu=0;
        Integer arama_kanali_tipi=0;
        Integer fk_aramakanali=0;
        Integer fk_aramasebebi=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();	
			session.setAttribute("rptBildirimDetay_sdate",sdate);									
		} catch (Exception e) {
			sdate=null;
		}

		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();	
			session.setAttribute("rptBildirimDetay_fdate",fdate);						
		} catch (Exception e) {
			fdate=null;
		}


		try {
            fk_il=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_il=0;
		}	

		try {
            fk_ilce=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_ilce=0;
		}			

		try {
            fk_gorev_statu=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_gorev_statu=0;
		}			

		try {
            fk_havuz=filter.getAsJsonObject().getAsJsonArray("filters").get(5)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_havuz=0;
		}			

		try {
            arama_kanali_tipi=filter.getAsJsonObject().getAsJsonArray("filters").get(6)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			arama_kanali_tipi=0;
		}			

		try {
            fk_aramakanali=filter.getAsJsonObject().getAsJsonArray("filters").get(7)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_aramakanali=0;
		}			

		try {
            fk_aramasebebi=filter.getAsJsonObject().getAsJsonArray("filters").get(8)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_aramasebebi=0;
		}			

		try {
            tip_level1=filter.getAsJsonObject().getAsJsonArray("filters").get(9)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level1=0;
		}			

		try {
            tip_level2=filter.getAsJsonObject().getAsJsonArray("filters").get(10)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level2=0;
		}			

		try {
            tip_level3=filter.getAsJsonObject().getAsJsonArray("filters").get(11)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level3=0;
		}			


		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptBildirimDetay>  liste =  service.BildirimDetayRaporu(
            sdate, 
            fdate, 
			tip_level1,
            tip_level2,
            tip_level3,
            fk_il, 
            fk_ilce,
			fk_havuz,
            fk_gorev_statu,
            arama_kanali_tipi,
            fk_aramakanali,
            fk_aramasebebi,
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		List<vTotal> say = service.countBildirimDetayRaporu(
			sdate, 
            fdate, 
			tip_level1,
            tip_level2,
            tip_level3,
            fk_il, 
            fk_ilce,
			fk_havuz,
            fk_gorev_statu,
            arama_kanali_tipi,
            fk_aramakanali,
            fk_aramasebebi,
            userId, 
            session.getAttribute("User_ROL").toString()
		);
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}


    // MU aktivite
    @RequestMapping(value="/mu-aktivite-rapor", method= RequestMethod.GET)
	public ModelAndView muaktiviterapor(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/MUAktiviteRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		String sdate;
		try {
			sdate=session.getAttribute("rptMuAktiviteRapor_sdate").toString();
		} catch (Exception e) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
  			LocalDateTime now = LocalDateTime.now();  
			sdate=now.toString();
		}
		String fdate;
		try {
			fdate=session.getAttribute("rptMuAktiviteRapor_fdate").toString();
		} catch (Exception e) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			LocalDateTime now = LocalDateTime.now();  
		  	fdate=now.toString();
		}

		List<vILLER> iller = lookupService.getIller(0);
        model.addObject("reportName","Mevzuat Uzmanı Aktivite Raporu");
		model.addObject("iller",iller);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		model.addObject("sdate",sdate);
		model.addObject("fdate",fdate);
		return model;
	}
    @RequestMapping(value="/mu-aktivite-rapor", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> muaktiviteraporData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer mu=0;
        Integer sistem=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();		
			session.setAttribute("rptMuAktiviteRapor_sdate",sdate);					
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();
			session.setAttribute("rptMuAktiviteRapor_fdate",fdate);								
		} catch (Exception e) {
			fdate=null;
		}
		try {
            mu=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			mu=0;
		}	
		try {
            sistem=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			sistem=0;
		}			

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptMuAktivite>  liste =  service.MuAktiviteRapor(
            sdate, 
            fdate, 
            mu, 
            sistem, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		List<vTotal>  say =  service.countMuAktiviteRapor(
            sdate, 
            fdate, 
            mu, 
            sistem, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

	public <K, V> K getKey(Map<K, V> map, V value) {
		for (Entry<K, V> entry : map.entrySet()) {
			if (entry.getValue().equals(value)) {
				return entry.getKey();
			}
		}
		return null;
	}

    // MEVZUAT UZMANI SÜRE 
    @RequestMapping(value="/mu-sure-rapor", method= RequestMethod.GET)
	public ModelAndView musurerapor(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/MUSureRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
        
		String sdate;
		try {
			sdate=session.getAttribute("rptMuSureRapor_sdate").toString();
		} catch (Exception e) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
  			LocalDateTime now = LocalDateTime.now();  
			sdate=now.toString();
		}
		String fdate;
		try {
			fdate=session.getAttribute("rptMuSureRapor_fdate").toString();
		} catch (Exception e) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			LocalDateTime now = LocalDateTime.now();  
		  	fdate=now.toString();
		}
		model.addObject("sdate",sdate);
		model.addObject("fdate",fdate);
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Mevzuat Uzmanı İşlem Süresi Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/mu-sure-rapor", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> musureraporData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer mu=0;
        Integer sistem=0;
        Integer tip_level1=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();		
			session.setAttribute("rptMuSureRapor_sdate",sdate);			
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();	
			session.setAttribute("rptMuSureRapor_fdate",fdate);			
		} catch (Exception e) {
			fdate=null;
		}
		try {
            mu=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			mu=0;
		}	
		try {
            tip_level1=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level1=0;
		}			
		try {
            sistem=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			sistem=0;
		}			


		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptMuSure>  liste =  service.MuSureRapor(
            sdate, 
            fdate, 
            mu, 
			tip_level1,
            sistem, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		List<vTotal>  say =  service.countMuSureRapor(
            sdate, 
            fdate, 
            mu, 
			tip_level1, 
            sistem,  
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}



	@RequestMapping(value="/ky-sure-rapor", method= RequestMethod.GET)
	public ModelAndView kysurerapor(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/KYSureRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		String sdate;
		try {
			sdate=session.getAttribute("rptMuSureRapor_sdate").toString();
		} catch (Exception e) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
  			LocalDateTime now = LocalDateTime.now();  
			sdate=now.toString();
		}
		String fdate;
		try {
			fdate=session.getAttribute("rptMuSureRapor_fdate").toString();
		} catch (Exception e) {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
			LocalDateTime now = LocalDateTime.now();  
		  	fdate=now.toString();
		}
		model.addObject("sdate",sdate);
		model.addObject("fdate",fdate);
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Kurum Yetkilisi İşlem Süresi Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/ky-sure-rapor", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> kysureraporData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer ky=0;
        Integer tip_level1=0;
        Integer sistem=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

			
		try {
			sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsString();		
			session.setAttribute("rptMuSureRapor_sdate",sdate);			
		} catch (Exception e) {
			sdate=null;
		}
		try {
			fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
						.getAsJsonObject().get("value")
						.getAsString();	
			session.setAttribute("rptMuSureRapor_fdate",fdate);			
		} catch (Exception e) {
			fdate=null;
		}
		try {
            tip_level1=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level1=0;
		}	
		try {
            ky=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			ky=0;
		}	
		try {
            sistem=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			sistem=0;
		}			


		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptKySure>  liste =  service.KySureRapor(
            sdate, 
            fdate, 
            ky, 
            sistem, 
            tip_level1, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		List<vTotal>  say =  service.countKySureRapor(
            sdate, 
            fdate, 
            ky, 
			tip_level1, 
            sistem,  
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}



    @RequestMapping(value="/havuz-bildirim-sure", method= RequestMethod.GET)
	public ModelAndView havuzbildirimsure(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/HavuzBildirimSureRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Havuz Bildirim Süre Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/havuz-bildirim-sure", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> havuzbildirimsureData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    Integer take = jsonObject.get("take").getAsInt();
	    Integer skip = jsonObject.get("skip").getAsInt();   
	    Integer page = jsonObject.get("page").getAsInt();
	    Integer pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer tip_level1=0;
        Integer fk_il=0;
        Integer fk_sube=0; 
        Integer fk_havuz=0; 
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}
		try {
            tip_level1=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level1=0;
		}	
		try {
            fk_il=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_il=0;
		}	
		try {
            fk_sube=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_sube=0;
		}			
		try {
            fk_havuz=filter.getAsJsonObject().getAsJsonArray("filters").get(5)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_havuz=0;
		}			

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptHavuzBildirimSure>  liste =  service.HavuzBildirimSureRapor(
            sdate, 
            fdate, 
            tip_level1, 
            fk_il, 
            fk_sube, 
            fk_havuz, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}


    @RequestMapping(value="/ham-data-raporu", method= RequestMethod.GET)
	public ModelAndView hamdataraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/HamDataRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Ham Data Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	


    @RequestMapping(value="/sektor-raporu", method= RequestMethod.GET)
	public ModelAndView sektorraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/SektorBazindaBildirimRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Sektor Bazında Bildirim Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/sektor-raporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> sektorraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    Integer take = jsonObject.get("take").getAsInt();
	    Integer skip = jsonObject.get("skip").getAsInt();   
	    Integer page = jsonObject.get("page").getAsInt();
	    Integer pageSize = jsonObject.get("pageSize").getAsInt();
	

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer tip_level1=0;
        Integer fk_il=0;
        Integer fk_sube=0; 
        Integer fk_havuz=0; 
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptSektorBazinda>  liste =  service.SektorBazindaRapor(
            sdate, 
            fdate, 
			skip,
			take,
			userId,
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}



    @RequestMapping(value="/sektor-detay-raporu", method= RequestMethod.GET)
	public ModelAndView sektordetayraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/SektorDetayRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Sektör Detay Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/sektor-detay-raporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> sektordetayraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);

		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer fk_sektor=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}

		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}


		try {
            fk_sektor=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_sektor=0;
		}	
		

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptSektorBazindaIhbarSikayet>  liste =  service.SektorBazindaIhbarSikayet(
            sdate, 
            fdate, 
            fk_sektor,
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}



    @RequestMapping(value="/ile-gore-bildirim-raporu", method= RequestMethod.GET)
	public ModelAndView ilegorebildirimraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/IleGoreBildirimRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","İl Bazında Bildirim Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/ile-gore-bildirim-raporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> ilegorebildirimraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    Integer take = jsonObject.get("take").getAsInt();
	    Integer skip = jsonObject.get("skip").getAsInt();   
	    Integer page = jsonObject.get("page").getAsInt();
	    Integer pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer fk_gorev_statu=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}
		try {
            fk_gorev_statu=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_gorev_statu=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptIlBazinda>  liste =  service.IlBazindaRapor(
            sdate, 
            fdate, 
            fk_gorev_statu,
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}




    @RequestMapping(value="/konulara-gore-bildirim-raporu", method= RequestMethod.GET)
	public ModelAndView konularagorebildirimraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/KonularaGoreBildirimRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Konulara Göre Bildirim Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/konulara-gore-bildirim-raporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> konularagorebildirimraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer fk_gorev_statu=0;
        Integer tip_level1=0;
        Integer tip_level2=0;
        Integer tip_level3=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}
		try {
            tip_level1=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level1=null;
		}
		try {
            tip_level2=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level2=null;
		}
		try {
            tip_level3=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level3=null;
		}
		try {
            fk_gorev_statu=filter.getAsJsonObject().getAsJsonArray("filters").get(5)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_gorev_statu=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptKonularaGore>  liste =  service.KonularaGore(
            sdate, 
            fdate, 
			fk_gorev_statu,
            tip_level1,
            tip_level2,
            tip_level3,
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}




    @RequestMapping(value="/havuz-ozet-raporu", method= RequestMethod.GET)
	public ModelAndView havuzozetraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/HavuzOzetRaporu");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Havuz Özet Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/havuz-ozet-raporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> havuzozetraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }



		List<rptHavuzOzet>  liste =  service.HavuzOzet(
            skip, 
            take, 
            userId
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}




	// pbx raporları 10 adet pbx raporu var burda sadece biltre yapılıp raporlar en son ayarlanacak
    @RequestMapping(value="/pbxreports", method= RequestMethod.GET)
	public ModelAndView pbxreports(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/PbxReports");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Çağrı Merkezi Raporları");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}





    @RequestMapping(value="/sgk-bilgilendirme", method= RequestMethod.GET)
	public ModelAndView sgkbilgilendirme(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/SgkBilgilendirme");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Kurum Bilgilendirme SMS Numaraları");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/sgk-bilgilendirme", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> sgkbilgilendirmeData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer fk_gorev_statu=0;
        Integer tip_level1=0;
        Integer tip_level2=0;
        Integer tip_level3=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}
		try {
            tip_level1=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level1=null;
		}
		try {
            tip_level2=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level2=null;
		}
		try {
            tip_level3=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			tip_level3=null;
		}
		try {
            fk_gorev_statu=filter.getAsJsonObject().getAsJsonArray("filters").get(5)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_gorev_statu=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}


		List<rptSgkBilgilendirme>  liste =  service.SgkBilgilendirme(
            sdate, 
            fdate, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}




    @RequestMapping(value="/ivr-agent", method= RequestMethod.GET)
	public ModelAndView ivragent(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/IVRAgent");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Agent Bazlı IVR Anket Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
    @RequestMapping(value="/ivr-agent", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> ivragentData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer fk_lokasyon=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}
		try {
            fk_lokasyon=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_lokasyon=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptIvrAgent>  liste =  service.IvrAgent(
            sdate, 
            fdate, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}






    @RequestMapping(value="/taseron-havuzu-rapor", method= RequestMethod.GET)
	public ModelAndView taseronhavuzurapor(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/TaseronHavuz");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Taşeron Havuz Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/taseron-havuzu-rapor", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> taseronhavuzuraporData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer fk_lokasyon=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}


		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptTaseronHavuzu>  liste =  service.TaseronHavuzu(
            sdate, 
            fdate, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}




    @RequestMapping(value="/ses-kayit", method= RequestMethod.GET)
	public ModelAndView seskayit(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/SesKayit");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Ses Kayıt Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/ses-kayit", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> seskayitData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        String tc=null;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}
		try {
            tc=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			tc=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptSesKayit>  liste =  service.SesKayit(
            sdate, 
            fdate, 
            tc, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}


	// 3 adet kko olcum raporu var pbx ile birlikte en son ayarlanacak
    @RequestMapping(value="/kko-olcum-yeni", method= RequestMethod.GET)
	public ModelAndView kkoolcumyeni(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/KKOOlcum");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","K.K.O. Raporları");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}



    @RequestMapping(value="/kurum-kapanma-istatistik-raporu", method= RequestMethod.GET)
	public ModelAndView kurumkapanmaistatistikraporu(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/KurumKapanmaIstatistik");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Kurum Kapanma İstatistik Raporu");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/kurum-kapanma-istatistik-raporu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> kurumkapanmaistatistikraporuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        String tc=null;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}


		List<rptKurumKapanmaIstatistik>  liste =  service.KurumKapanmaIstatistik(
            sdate, 
            fdate, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}


	// sonuclar listelenmiyor sonuc sayfasında direk xls export butou cıkıyor
    @RequestMapping(value="/islem-yapilmamis-kbb", method= RequestMethod.GET)
	public ModelAndView islemyapilmamiskbb(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/IslemYapilmamisKbb");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Kurum Birimleri Bazında İşlem Yapılmamış Bildirimler");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}




    @RequestMapping(value="/birimlere-gore-kullanici-sayisi", method= RequestMethod.GET)
	public ModelAndView birimleregorekullanicisayisi(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/BildirimlereGoreKullaniciSayisi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Birimlere Göre Kullanıcı Sayısı");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/birimlere-gore-kullanici-sayisi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> birimleregorekullanicisayisiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }


		List<rptBirimlereGoreKullaniciSayisi>  liste =  service.BirimlereGoreKullaniciSayisi(
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}


	// rapor calısmadı incelenecek
    @RequestMapping(value="/il-kurum-bildirim-dagilimi", method= RequestMethod.GET)
	public ModelAndView ilkurumbildirimdagilimi(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/IlKurumBildirimDagilimi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","İl Ve Kurum Bazlı Bildirim Dağılımı");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}



// rapor calısmadı incelenecek
    @RequestMapping(value="/kurum-birim-bildirim-dagilimi", method= RequestMethod.GET)
	public ModelAndView kurumbirimbildirimdagilimi(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/KurumBirimBildirimDagilimi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Kurum Birimlerine Göre Bildirim Dağılımı");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}



// rapor calısmadı incelenecek
    @RequestMapping(value="/arama-nedeni-kurum-bildirim-dagilimi", method= RequestMethod.GET)
	public ModelAndView aramanedenikurumbildirimdagilimi(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/AramaNedeniKurumBildirimDagilimi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Arama Nedeni Kurum Bildirim Dağılımı");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}




    @RequestMapping(value="/kurum-bazinda-zamaninda-kapanan", method= RequestMethod.GET)
	public ModelAndView kurumbazindazamanindakapanan(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/KurumBirimZamanindaKapanan");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Kurum Birim Zamanında Kapanan Bildirim İstatistikleri");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/kurum-bazinda-zamaninda-kapanan", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> kurumbazindazamanindakapananData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}


		List<rptKurumBazindaZamanindaKapanan>  liste =  service.KurumBazindaZamanindaKapanan(
            sdate, 
            fdate, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}



    @RequestMapping(value="/ivr-rapor", method= RequestMethod.GET)
	public ModelAndView ivrrapor(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/IVRRapor");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","IVR Anket Raporları");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/ivr-rapor", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> ivrraporData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptIvrRapor>  liste =  service.IvrRapor(
            sdate, 
            fdate, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}



    @RequestMapping(value="/sms-rapor", method= RequestMethod.GET)
	public ModelAndView smsrapor(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/SMSRapor");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","SMS Ve E-Mail Raporları");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/sms-rapor", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> smsraporData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

		String tipi=null;

		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            tipi=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			tipi=null;
		}
		


		List<rptSmsRapor>  liste =  service.SmsRapor(
            skip, 
            take, 
			tipi,
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}




    @RequestMapping(value="/periyodik-rapor", method= RequestMethod.GET)
	public ModelAndView periyodikrapor(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/PeriyodikRapor");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Periyodik Rapor");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/periyodik-rapor", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> periyodikraporData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptIvrRapor>  liste =  service.IvrRapor(
            sdate, 
            fdate, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 //maps.put("total",say.get(0).getTotal());
		return maps;
	}




    @RequestMapping(value="/raporlog", method= RequestMethod.GET)
	public ModelAndView raporlog(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/RaporLog");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","Rapor Logları");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/raporlog", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> raporlogData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String yetkiJson=session.getAttribute("yetki").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();


	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String sdate=null;
        String fdate=null;
        Integer fk_user=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            sdate=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			sdate=null;
		}
		try {
            fdate=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			fdate=null;
		}
		try {
            fk_user=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_user=null;
		}

		//System.out.println(sdate+"---------"+fdate+"-----------------"+fk_user);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = sdf.parse(sdate);
    	Date secondDate = sdf.parse(fdate);
		long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		if(diff>3){
			Date date = new Date();
			SimpleDateFormat sdate_=new SimpleDateFormat("yyyy-MM-dd");
			sdate=sdate_.format(date);
			fdate=sdate;
		}

		List<rptRaporLog>  liste =  service.RaporLog(
            sdate, 
            fdate, 
            fk_user, 
            skip, 
            take, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		List<vTotal>  say =  service.countRaporLog(
            sdate, 
            fdate, 
            fk_user, 
            userId, 
            session.getAttribute("User_ROL").toString()
        );
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}


	// cografi harita var en son yapılacak
    @RequestMapping(value="/geo-rapor", method= RequestMethod.GET)
	public ModelAndView georapor(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("RAPOR/GeoRapor");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		List<vILLER> iller = lookupService.getIller(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
        model.addObject("reportName","GEO Rapor");
		model.addObject("iller",iller);
		model.addObject("kurumlar",kurumlar);
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	



	@RequestMapping(value="/getUsers", method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> Users(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		JsonParser jsonParser = new JsonParser();
		String adi="";
		String rol="";
		Integer fk_lokasyon=0;
		Integer limit=0;
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            adi=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			adi="";
		}
		try {
            rol=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			rol="";
		}
		try {
            fk_lokasyon=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_lokasyon=0;
		}
		try {
            limit=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			limit=100;
		}
		limit=100;

		/**
		 * rol tablosundan level i mevcut kullanıcı rolunden düşük olan rolleri alır
		 */
		Integer userRolId=Integer.parseInt(session.getAttribute("User_fkROL").toString());
		List<Map<String,Object>> rolListe=ayarlarService.getRolLowerLevel(userRolId.intValue());
		Integer i=0;
		for (Map<String,Object> r : rolListe) {
			i++;
			if(i>1){
				rol +=","+r.get("id");
			}else{
				rol +=r.get("id");
			}
		}
		
		// ust duzey yetkisi olmayan kullanıcılarda sadece kendi havuzuna baglı olan kullanıcılar listelenecek ve isactive=1 olacak
        List<vKULLANICILAR>  l =  ayarlarService.KullaniciFiltreListesi(adi, "", rol,1,fk_lokasyon,0,limit);	
		Map<String,Object> maps=new HashMap<>();   
		maps.put("data",l); 
		return maps;
	}


	@RequestMapping(value="/cmbGetUsers", method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> getUsersByUserHavuz(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		JsonParser jsonParser = new JsonParser();
		Integer limit=0;
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		Integer userId=(Integer) session.getAttribute("User_ID");
		List<Map<String,Object>> l=lookupService.getUsersByUserHavuz(userId);
		Map<String,Object> maps=new HashMap<>();   
		maps.put("data",l); 
		return maps;				
	}
        



}


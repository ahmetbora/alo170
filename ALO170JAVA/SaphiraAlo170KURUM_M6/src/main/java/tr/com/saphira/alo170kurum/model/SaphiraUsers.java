package tr.com.saphira.alo170kurum.model;

public class SaphiraUsers {
	private Integer id;
	private String adi;
	private String ext;
	private String username;
	private String password;
	private Integer SysAdmin;
	private String resim;
	private Integer isSU;
	private Integer isAgent;
	private Integer isWallBoard;
	private Integer isAdmin;
	private Integer isCrm;
	private Integer istk;
	private Integer isActive;
	private Integer fk_yetki_grubu;
	private String auth_type;
	private Integer fk_lokasyon;
	private String rol="";
	private String fk_rol ="0";
	private String lokasyon="";
	private Integer userchange =0;
	private String yetki;
	private String rolKisa;
	private String yetkiJson=null;
	private Integer rolLevel=0;
	
	
	
	
	
	public SaphiraUsers(Integer id) {
		super();
		this.id = id;
	}
	public SaphiraUsers() {
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getSysAdmin() {
		return SysAdmin;
	}
	public void setSysAdmin(Integer sysAdmin) {
		SysAdmin = sysAdmin;
	}
	public String getResim() {
		return resim;
	}
	public void setResim(String resim) {
		this.resim = resim;
	}
	public Integer getIsSU() {
		return isSU;
	}
	public void setIsSU(Integer isSU) {
		this.isSU = isSU;
	}
	public Integer getIsAgent() {
		return isAgent;
	}
	public void setIsAgent(Integer isAgent) {
		this.isAgent = isAgent;
	}
	public Integer getIsWallBoard() {
		return isWallBoard;
	}
	public void setIsWallBoard(Integer isWallBoard) {
		this.isWallBoard = isWallBoard;
	}
	public Integer getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(Integer isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Integer getIsCrm() {
		return isCrm;
	}
	public void setIsCrm(Integer isCrm) {
		this.isCrm = isCrm;
	}
	public Integer getIstk() {
		return istk;
	}
	public void setIstk(Integer istk) {
		this.istk = istk;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public Integer getFk_yetki_grubu() {
		return fk_yetki_grubu;
	}
	public void setFk_yetki_grubu(Integer fk_yetki_grubu) {
		this.fk_yetki_grubu = fk_yetki_grubu;
	}
	public String getAuth_type() {
		return auth_type;
	}
	public void setAuth_type(String auth_type) {
		this.auth_type = auth_type;
	}
	public Integer getFk_lokasyon() {
		return fk_lokasyon;
	}
	public void setFk_lokasyon(Integer fk_lokasyon) {
		this.fk_lokasyon = fk_lokasyon;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	public String getRolKisa() {
		return rolKisa;
	}
	public void setRolKisa(String rolKisa) {
		this.rolKisa = rolKisa;
	}

	public String getFk_rol() {
		return fk_rol;
	}
	public void setFk_rol(String fk_rol) {
		this.fk_rol = fk_rol;
	}
	public String getLokasyon() {
		return lokasyon;
	}
	public void setLokasyon(String lokasyon) {
		this.lokasyon = lokasyon;
	}
	public Integer getUserchange() {
		return userchange;
	}
	public void setUserchange(Integer userchange) {
		this.userchange = userchange;
	}

	public String getYetki() {
		return yetki;
	}
	public void setYetki(String yetki) {
		this.yetki = yetki;
	}
	public String getyetkiJson() {
		return yetkiJson;
	}
	public void setyetkiJson(String yetkiJson) {
		this.yetkiJson = yetkiJson;
	}
	public Integer getRolLevel() {
		return rolLevel;
	}
	public void setRolLevel(Integer rolLevel) {
		this.rolLevel = rolLevel;
	}
}

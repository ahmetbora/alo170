package tr.com.saphira.alo170kurum.AYARLAR;

public class vYETKIALANLARI {

	private Integer id;
	private String adi;
	private String modul;
	private Integer fk_parent;
	private Integer fk_root;
	private Integer is_root;
	private String aciklama;
	
	private Integer rootid;
	private String rootadi;
	private String rootmodul;
	private String rootaciklama;
	
	private Long fk_rol;
	private Long fk_yetki;
	private Integer user_yetki_id;
	private Integer user_yetki;
	private Integer user_yetki_user_id;
	private String connected_items;
	
	public vYETKIALANLARI() {
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getModul() {
		return modul;
	}
	public void setModul(String modul) {
		this.modul = modul;
	}	
	public Integer getFkparent() {
		return fk_parent;
	}
	public void setFkparent(Integer fk_parent) {
		this.fk_parent = fk_parent;
	}
	public Integer getFkroot() {
		return fk_root;
	}
	public void setFkroot(Integer fk_root) {
		this.fk_root = fk_root;
	}
	public Integer getIsroot() {
		return is_root;
	}
	public void setIsroot(Integer is_root) {
		this.is_root = is_root;
	}
	public String getAciklama() {
		return aciklama;
	}
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	
	
	
	public Integer getRootId() {
		return rootid;
	}
	public void setRootId(Integer rootid) {
		this.rootid = rootid;
	}
	
	public String getRootadi() {
		return rootadi;
	}
	public void setRootadi(String rootadi) {
		this.rootadi = rootadi;
	}
	public String getRootmodul() {
		return rootmodul;
	}
	public void setRootmodul(String rootmodul) {
		this.rootmodul = rootmodul;
	}
	public String getRootaciklama() {
		return rootaciklama;
	}
	public void setRootaciklama(String rootaciklama) {
		this.rootaciklama = rootaciklama;
	}
	
	
	public Long getFkRol() {
		return fk_rol;
	}
	public void setFkRol(Long fk_rol) {
		this.fk_rol = fk_rol;
	}
	public Long getFkYetki() {
		return fk_yetki;
	}
	public void setFkYetki(Long fk_yetki) {
		this.fk_yetki = fk_yetki;
	}
	public Integer getUserYetkiId() {
		return user_yetki_id;
	}
	public void setUserYetkiId(Integer user_yetki_id) {
		this.user_yetki_id = user_yetki_id;
	}
	public Integer getUserYetki() {
		return user_yetki;
	}
	public void setUserYetki(Integer user_yetki) {
		this.user_yetki = user_yetki;
	}
	public Integer getUserYetkiUserId() {
		return user_yetki_user_id;
	}
	public void setUserYetkiUserId(Integer user_yetki_user_id) {
		this.user_yetki_user_id = user_yetki_user_id;
	}
	public String getConnectedItems() {
		return connected_items;
	}
	public void setConnectedItems(String connected_items) {
		this.connected_items = connected_items;
	}

	
}

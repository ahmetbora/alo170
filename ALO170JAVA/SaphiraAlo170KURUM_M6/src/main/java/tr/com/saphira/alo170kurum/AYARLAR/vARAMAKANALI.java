package tr.com.saphira.alo170kurum.AYARLAR;

public class vARAMAKANALI {

	private Integer id;
	private String adi;
	private Integer fk_parent_int;
	private Integer isActive;

	public vARAMAKANALI() {
		super();
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public Integer getFk_parent_int() {
		return fk_parent_int;
	}
	public void setFk_parent_int(Integer fk_parent_int) {
		this.fk_parent_int = fk_parent_int;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
}

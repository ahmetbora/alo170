package tr.com.saphira.alo170kurum.MODULLER;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.BILDIRIM.vJSONRESULT;
import tr.com.saphira.alo170kurum.KART.vKART;
import tr.com.saphira.alo170kurum.SCRIPT.vSCRIPT;
import tr.com.saphira.alo170kurum.SSS.vSSS;
import tr.com.saphira.alo170kurum.model.vTotal;

@SuppressWarnings("unused")
@Service
public class ModulServiceImpl implements ModulService {
    @Autowired
	ModulDao dao;

	@Override
	public vJSONRESULT queMudahale(String ext,Integer userId,String uzunluk) {
		
		return dao.queMudahale(ext,userId,uzunluk);
	}
	@Override
	public List<Map<String, Object>> AgentHangup(String sdate,String fdate,Integer lokasyon,String tablo){
		
		return dao.AgentHangup( sdate, fdate, lokasyon, tablo);
	}
	@Override
	public List<Map<String, Object>> AgentHangupDetay(String sdate,String fdate,Integer lokasyon,String tablo,String ext){
		
		return dao.AgentHangupDetay( sdate, fdate, lokasyon, tablo,ext);
	}
	@Override
	public List<Map<String, Object>> IvrSms(String tc){
		
		return dao.IvrSms( tc);
	}

	@Override
		public vJSONRESULT IvrSmsIptal(String tc,Integer userId){
		
		return dao.IvrSmsIptal(tc,userId);
	}
	
	@Override
	public List<Map<String, Object>> getsss(Integer id, Integer statu,Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,Integer skip,Integer take){
		return dao.getsss(id,statu,fk_sss_grup,fk_sss_altgrup,baslik,skip,take);
	}
	
	@Override
	public List<vTotal> countsss(Integer statu,Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik ){
		return dao.countsss(statu,fk_sss_grup,fk_sss_altgrup,baslik);
	}
	
	@Override
	public vJSONRESULT sssGuncelle(Integer statu,Integer isWeb, Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,String aciklama,Integer userId,Integer id){
		
		return dao.sssGuncelle(statu,isWeb, fk_sss_grup,fk_sss_altgrup,baslik,aciklama,userId,id);
	}

	@Override
	public vJSONRESULT sssKaydet(Integer statu,Integer isWeb, Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,String aciklama,Integer userId){
		
		return dao.sssKaydet(statu,isWeb, fk_sss_grup,fk_sss_altgrup,baslik,aciklama,userId);
	}

	@Override
	public List<Map<String, Object>> KaraListe(Integer id,Integer isOnay,Integer skip,Integer take){
		return dao.KaraListe(id,isOnay, skip, take);
	}
	@Override
	public List<vTotal> countKaraListe(Integer isOnay){
		return dao.countKaraListe(isOnay);
	}

	@Override
	public vJSONRESULT KaraListeOnay(Integer id,String tarih,Integer userId){
		
		return dao.KaraListeOnay(id,tarih,userId);
	}
	
	@Override
	public vJSONRESULT KaraListeSuresiz(Integer id,Integer userId){
		
		return dao.KaraListeSuresiz(id,userId);
	}
	
	@Override
	public vJSONRESULT KaraListeSil(Integer id,Integer userId){
		
		return dao.KaraListeSil(id,userId);
	}
	

	@Override
	public List<Map<String, Object>> VipListe(Integer skip,Integer take){
		return dao.VipListe(skip, take);
	}
	@Override
	public List<vTotal> countVipListe(){
		return dao.countVipListe();
	}
	@Override
	public vJSONRESULT VipListeSil(Integer id){
		return dao.VipListeSil(id);
	}

	public List<Map<String, Object>> MobilUyeListe(Integer statu,String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer skip,Integer take){
		return dao.MobilUyeListe(statu,adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,skip,take);
	}
	public List<vTotal> countMobilUyeListe(Integer statu,String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce){
		return dao.countMobilUyeListe(statu,adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce);
	}
	public List<Map<String, Object>> VatandasListe(String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer skip,Integer take,Integer kayit_tipi){
		return dao.VatandasListe(adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,skip,take,kayit_tipi);
	}
	public List<vTotal> countVatandasListe(String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer kayit_tipi){
		return dao.countVatandasListe(adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,kayit_tipi);
	}
	public Map<String, Object> getVatandas(Integer id){
		return dao.getVatandas(id);
	}
	@Override
	public vJSONRESULT VatandasGuncelle(Integer userId,Integer id,String adi,String soyadi,String tckimklik,String dt,String tel,String gsm,Integer fk_egitim_durumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,String ssk,String email,Integer fk_cinsiyet,String fax){
		return dao.VatandasGuncelle(userId,id,adi,soyadi,tckimklik,dt,tel,gsm,fk_egitim_durumu,fk_ulke,fk_il,fk_ilce,ssk,email,fk_cinsiyet,fax);
	}
	public vJSONRESULT VatandasOnayla(Integer id,Integer statu,String sifre){
		return dao.VatandasOnayla(id,statu,sifre);
	}
	public List<Map<String, Object>> izinTalepSv(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip, Integer skip,Integer take){
		return dao.izinTalepSv(fk_lokasyon, userId, isActive, tip, skip,take);
	}
	public List<vTotal> countizinTalepSv(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip){
		return dao.countizinTalepSv(fk_lokasyon, userId, isActive, tip);
	}
	public List<Map<String, Object>> izinTalepTk(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip, Integer skip,Integer take){
		return dao.izinTalepTk(fk_lokasyon, userId, isActive, tip, skip,take);
	}
	public List<vTotal> countizinTalepTk(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip){
		return dao.countizinTalepTk(fk_lokasyon, userId, isActive, tip);
	}
	public List<Map<String, Object>> izinTalepVT(Integer userId, Integer isActive, Integer skip,Integer take){
		return dao.izinTalepVT( userId, isActive, skip,take);
	}
	public List<vTotal> countizinTalepVT(Integer userId, Integer isActive){
		return dao.countizinTalepVT(userId, isActive);
	}
	public List<Map<String, Object>> izinTalepAD(Integer userId, Integer skip,Integer take){
		return dao.izinTalepAD( userId,skip,take);
	}
	public List<vTotal> countizinTalepAD(Integer userId){
		return dao.countizinTalepAD(userId);
	}
	public vJSONRESULT izinTalepKaydet(String sdate, String ssaat,String fdate, String fsaat, Integer fk_sebep,String aciklama,Integer fk_agent,Integer fk_tipi,Integer fk_agent_,
	Integer fk_lokasyon,Integer admin_onay,Integer islem_admin,Integer tl_onay,Integer islem_tl,Integer sv_onay,Integer islem_sv,Integer isActive,String userFkRol
	){
		return dao.izinTalepKaydet(sdate, ssaat,fdate, fsaat, fk_sebep,aciklama,fk_agent,fk_tipi,fk_agent_,fk_lokasyon,admin_onay,islem_admin,tl_onay,islem_tl,sv_onay,islem_sv,isActive,userFkRol);
	}
	public List<Map<String, Object>> izinTalepOnayListe(Integer fk_rol,Integer fk_lokasyon,Integer skip,Integer take){
		return dao.izinTalepOnayListe(fk_rol,fk_lokasyon,skip,take);
	}
	public List<vTotal> countizinTalepOnayListe(Integer fk_rol,Integer fk_lokasyon){
		return dao.countizinTalepOnayListe(fk_rol,fk_lokasyon);
	}
	public vJSONRESULT izinTalepOnay(Integer userId,Integer id,Integer fk_rol,Integer onay){
		return dao.izinTalepOnay(userId,id,fk_rol,onay);
	}
	public List<Map<String, Object>> ScriptKonulari(Integer skip,Integer take){
		return dao.ScriptKonulari(skip,take);
	}
	public List<vTotal> countScriptKonulari(){
		return dao.countScriptKonulari();
	}
	public vJSONRESULT ScriptKonuUpdate(Integer id,String adi){
		return dao.ScriptKonuUpdate(id,adi);
	}
	public vJSONRESULT ScriptKonuInsert(String adi){
		return dao.ScriptKonuInsert(adi);
	}
	public List<Map<String, Object>> ScriptListe(Integer skip,Integer take){
		return dao.ScriptListe(skip,take);
	}
	public List<vTotal> countScriptListe(){
		return dao.countScriptListe();
	}
	public Map<String, Object> ScriptDetay(Integer id){
		return dao.ScriptDetay(id);
	}
	public vJSONRESULT ScriptKaydet(String fk_parent,String fk_aramasebebi,String tip_level1,String tip_level2,String tip_level3,String icerik,String aciklama,String id){
		return dao.ScriptKaydet(fk_parent,fk_aramasebebi,tip_level1,tip_level2,tip_level3,icerik,aciklama,id);
	}
}


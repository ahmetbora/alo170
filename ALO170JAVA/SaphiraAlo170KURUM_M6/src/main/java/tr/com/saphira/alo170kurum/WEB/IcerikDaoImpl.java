package tr.com.saphira.alo170kurum.WEB;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;




@Transactional
@Repository
public class IcerikDaoImpl implements IcerikDao {
	
	 @Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;

	@Override
	public List<vICERIK> getIcerik(Integer ID) {
		String sql1 = "call getWebIcerik (?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  List<vICERIK> ret = new ArrayList<vICERIK>();
		  
			for (Map<String, Object> row : rows ) {
				
				vICERIK a = new vICERIK();	
			
		try {		
			a.setId((Integer)row.get("id"));
		}catch (Exception ex) {
			System.out.println("id Hata :" + ex.toString());
		}try {		
			a.setBaslik(row.get("baslik").toString());
		}catch (Exception ex) {
			System.out.println("baslik Hata :" + ex.toString());
		}try {		
			a.setKisa_aciklama(row.get("kisa_aciklama").toString());
		}catch (Exception ex) {
			System.out.println("kisa_aciklama Hata :" + ex.toString());
		}try {		
			a.setResim(row.get("resim").toString());
		}catch (Exception ex) {
			System.out.println("resim Hata :" + ex.toString());
		}try {		
			a.setIcerik(row.get("icerik").toString());
		}catch (Exception ex) {
			System.out.println("icerik Hata :" + ex.toString());
		}try {		
			a.setUrl(row.get("url").toString());
		}catch (Exception ex) {
			System.out.println("url Hata :" + ex.toString());
		}
		
		ret.add(a);
		
	  }
		return ret;
	}

	@Override
	public vICERIK Find(Integer ID) {
		String sql1 = "call getWebIcerik (?)";
		 Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });
				
		vICERIK a = new vICERIK();	
			
		try {		
			a.setId((Integer)row.get("id"));
		}catch (Exception ex) {
			System.out.println("id Hata :" + ex.toString());
		}try {		
			a.setBaslik(row.get("baslik").toString());
		}catch (Exception ex) {
			System.out.println("baslik Hata :" + ex.toString());
		}try {		
			a.setKisa_aciklama(row.get("kisa_aciklama").toString());
		}catch (Exception ex) {
			System.out.println("kisa_aciklama Hata :" + ex.toString());
		}try {		
			a.setResim(row.get("resim").toString());
		}catch (Exception ex) {
			System.out.println("resim Hata :" + ex.toString());
		}try {		
			a.setIcerik(row.get("icerik").toString());
		}catch (Exception ex) {
			System.out.println("icerik Hata :" + ex.toString());
		}try {		
			a.setUrl(row.get("url").toString());
		}catch (Exception ex) {
			System.out.println("url Hata :" + ex.toString());
		}
	
		return a;
	}
	
	 public void setIcerik(Integer ID, String BASLIK, String KACIKLAMA, String ICERIK, String RESIM, String URL) {
		  String sql1 = "call setWebIcerik (?,?,?,?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, BASLIK, KACIKLAMA, ICERIK, RESIM, URL });
		  
	 }
	

}

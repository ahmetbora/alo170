package tr.com.saphira.alo170kurum.AYARLAR;

public class vILCELER {

	private Integer id;
	private String adi;
	private Integer fk_il;
	private Integer isActive;
	public vILCELER() {
		super();

	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public Integer getFk_il() {
		return fk_il;
	}
	public void setFk_il(Integer fk_il) {
		this.fk_il = fk_il;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	
	
}

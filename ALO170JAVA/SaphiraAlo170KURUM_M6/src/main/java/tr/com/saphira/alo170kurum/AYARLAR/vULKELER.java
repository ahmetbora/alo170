package tr.com.saphira.alo170kurum.AYARLAR;

public class vULKELER {
	private Integer id;
	private String adi;
	private Integer isActive;
	
	public vULKELER() {
		super();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
}

package tr.com.saphira.alo170kurum.AYARLAR;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.BILDIRIM.vJSONRESULT;
import tr.com.saphira.alo170kurum.model.vTotal;


public interface AyarlarDao {
	
	List<vKULLANICILAR> KullaniciFiltreListesi(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon,Integer skip,Integer take);
	public List<vTotal> countKullaniciFiltreListesi(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon);
	
	public List<vKURUMLAR> getKurumlar(Integer ID,String TERM,Integer skip,Integer take,Map<String,Object> userYetki);
	public List<vTotal> countGetKurumlar(Integer ID,String TERM);

	public void setKurumlar(Integer ID, String ADI,String EMAIL, Integer ISACTIVE);

	public List<vALTKURUMLAR> getAltKurumlar(Integer ID,String TERM,Integer fk_parent,Integer skip,Integer take,Map<String,Object> userYetki);
	public List<vTotal> countGetAltKurumlar(Integer ID,String TERM,Integer fk_parent);
	public void setAltKurumlar(Integer ID, String ADI, Integer PARENT,String EMAIL, Integer ISACTIVE);
	public List<vARAMAKONUSU> getAramaKonusu(Integer ID,String TERM,Integer fk_konu,Integer skip,Integer take);
	public List<vTotal> countGetAramaKonusu(Integer ID,String TERM,Integer fk_konu);
	public void setAramaKonusu(Integer ID, String ADI, Integer KONU,Integer ALTKONU,String EMAIL, Integer ISACTIVE);
	List<Map<String, Object>> getSubeler(Integer ID,String TERM,Integer fk_konu,Integer fk_il,Integer skip,Integer take);
	public List<vTotal> countGetSubeler(Integer ID,String TERM,Integer fk_konu);
	public void setSubeler(Integer ID, String ADI, Integer KONU, Integer IL, Integer ISACTIVE);

	public void setHavuzlar(Integer ID, String ADI, Integer KONU, Integer IL, Integer SUBE,String EMAIL, Integer ISACTIVE);
	
	
	//ComboFiltre
	public List<vALTKURUMLAR> getAltKurumlarFiltre(Integer KONU);
	public List<vARAMAKONUSU> getAramaKonusuFiltre(Integer KONU);
	
	/**
	 * getSubelerFiltre
	 * @param ID 		sube id si 0 girilirse kullanılmaz. bir id girilirse o id ye ait subeyi tek kayıt olarka getirir.
	 * @param KONU		konu id si 0 olursa kullanılmaz
	 * @param TERM		sube adında aranacak boş bırakılırsa kullanılmaz
	 * @param FKIL		il id si 0 olursa kullanılmaz
	 * @param IDS		where in yada not için kullanılacak sube id leri. boş bırakılırsa kullanılmaz
	 * @param WHEREIN	where in mi yoksa not in mi olacağını belirler. 1 in 2 not in 0 olursa wherein kullanılmaz
	 * @return
	 */
	public List<vSUBELER> getSubelerFiltre(Integer ID,Integer KONU,String TERM,Integer FKIL,String IDS,Integer WHEREIN);
	
	/**
	 * 
	 * @param ID
	 * @param ISACTIVE 1,0 YADA BAŞKA INT DEĞER. 1 VE 0 HARICI DEĞER GİRİLİRSE FİLTREYİ YOK SAYAR 
	 * @param FKSUBE 0 İSE UYGULANMAZ
	 * @param FKIL 0 İSE UYGULANMAZ
	 * @param FKKONU 0 İSE UYGULANMAZ
	 * @param IDS STRING 1,2,3,9,8 SEKLİNDE ID LERİ SORGUYA SOKAR
	 * @param WHEREIN IDS PARAMETRESINDE GIRILEN ID LERİ IN YADA NOT IN OLACAĞINI BELİRLER. 1 IN 2 NOT IN 0 YOKSAY
	 * @param TERM ADI KISMINDA LİKE İLE ARAMA YAPAR. BOŞ BIRAKILIRSA UYGULANMAZ
	 * @return
	 */
	public List<Map<String, Object>> getHavuzlar(String IDS,Integer ISACTIVE,Integer FKSUBE,Integer FKIL,Integer FKKONU,Integer WHEREIN,String TERM,Integer skip,Integer take);
	public List<vTotal> countGetHavuzlar(String IDS,Integer ISACTIVE,Integer FKSUBE,Integer FKIL,Integer FKKONU,Integer WHEREIN,String TERM);
	public List<Map<String, Object>> getAramaKanali(String IDS,String TERM,Integer ISACTIVE,Integer fk_parent,Integer WHEREIN,Integer skip,Integer take);
	public List<vTotal> countGetAramaKanali(String IDS,String TERM,Integer ISACTIVE,Integer fk_parent,Integer WHEREIN);
	public List<Map<String, Object>> getEgitimDurumlari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take);
	public List<vTotal> countGetEgitimDurumlari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN);
	public List<Map<String, Object>> getGuvenlikSorulari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take);
	public List<vTotal> countGetGuvenlikSorulari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN);
	public List<Map<String, Object>> getIller(String IDS,String TERM,Integer ISACTIVE,Integer fk_ulke,Integer WHEREIN,Integer skip,Integer take);
	public List<vTotal> countGetIller(String IDS,String TERM,Integer ISACTIVE,Integer fk_ulke,Integer WHEREIN);
	public List<Map<String, Object>> getIlceler(String IDS,String TERM,Integer ISACTIVE,Integer fk_il,Integer WHEREIN,Integer skip,Integer take);
	public List<vTotal> countGetIlceler(String IDS,String TERM,Integer ISACTIVE,Integer fk_il,Integer WHEREIN);
	public List<Map<String, Object>> getUlkeler(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take);
	public List<vTotal> countGetUlkeler(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN);
	public List<Map<String, Object>> getDahili(Integer id,String EXT,Integer WHEREIN,Integer fk_lokasyon,Integer skip,Integer take);
	public List<vTotal> countGetDahili(String EXT,Integer WHEREIN,Integer fk_lokasyon);
	public List<Map<String, Object>> getKkoBaslik(Integer id , Integer isActive,Integer skip,Integer take);
	public List<vTotal> countGetKkoBaslik(Integer id, Integer isActive);
	public List<Map<String, Object>> getKkoSoru(Integer id , Integer isActive,Integer fk_parent,Integer skip,Integer take);
	public List<vTotal> countGetKkoSoru(Integer id, Integer isActive,Integer fk_parent);
	public List<Map<String, Object>> getPeriyodik(Integer id ,Integer skip,Integer take);
	public List<vTotal> countGetPeriyodik(Integer id);
	public vJSONRESULT delPeriyodik(Integer id);
	
	public List<Map<String, Object>> getLokasyonlar();
	public List<vTotal> countGetLokasyonlar();


	public List<vYETKIGRUPLARI> getYetkiGruplari(Integer ID,Integer isAdmin,Integer tip,Integer level,Integer userId);
	public List<vYETKIGRUPLARI> getYetkiGruplariUser(Integer userId);
	public List<vYETKIALANLARI> getYetkiAlanlari(Integer ID,Integer isRoot,Integer yetkiGrubuID,Integer whereInType,String IDS,Integer isActive);
	
	public vJSONRESULT yetkiGrubuDetayGuncelle(Integer fkYetki,Integer fkRol); 
	public vJSONRESULT yetkiGrubuDetayTemizle(Integer fkRol); 
	public vJSONRESULT yetkiGrubuGuncelle(String adi,Integer isActive,Integer isHidden,Integer id); 
	
	public vJSONRESULT kullaniciKaydet(String adi,String email,String password,String username,String gsm,String ext,String tckimlik,String emsicil,String ksicil,Integer email_bildirim,Integer IsActive,Integer fkIslemYapan,Integer fk_rol,Integer fk_lokasyon,String fkEkleyen,Integer fk_kurum);
		public vJSONRESULT kullaniciGuncelle(Integer id,String adi,String email,String password,String username,String gsm,String ext,String tckimlik,String emsicil,String ksicil,Integer email_bildirim,Integer IsActive,Integer fkIslemYapan,String face,String twit,Integer fk_rol, Integer fk_lokasyon,Integer isUpdated,Integer fk_kurum);

	
	public vKULLANICILAR getUserByID(Integer ID); 
	
	/**
	 * SADECE 1 KAYIT DÖNDÜRÜR PARAMETRELERDEN SADECE 1 TANESİ KULLANILIR.
	 * @param ID
	 * @param userName
	 * @param email
	 * @param tcKimlik
	 * @return
	 */
	public vKULLANICILAR findUser(Integer ID,String userName,String email,String tcKimlik); 
	
	public List<vHAVUZLAR> getUserHavuz(Integer userId,Integer whereIN,String IDS,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu);
	public vJSONRESULT userHavuzGuncelle(Integer userID,Integer fkIslemYapan,String havuzlar); 

	public List<vYETKIALANLARI> getUserYetkiAlanlariDetay(Integer userId);
	public vJSONRESULT yetkiGrubuDetayGuncelleUser(Integer userID,Integer fkIslemYapan,String yetkiler,Integer fkRol,Integer fkLokasyon,String yetkiRol);
	public  Map<String,Object> getUserYetkiFromJSON(HttpSession session);
	public  ModelAndView getUserYetkiBildirimModelView(ModelAndView model, HttpSession session);
	public List<Map<String, Object>> getAktifPasifList(Integer tip);
	public vJSONRESULT tnmUpdate(Integer fldCount,String tbl,List<String> cols,List<String> vals,Integer Id,String ayrac);
	public vJSONRESULT tnmInsert(Integer fldCount,String tbl,List<String> cols,List<String> vals,String ayrac);
	
	public List<Map<String,Object>> getRolLowerLevel(Integer RolId);
}

package tr.com.saphira.alo170kurum.WEB;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@EnableWebSecurity
@RequestMapping(value="website/icerik")
public class IcerikController {
	
	@Autowired
	IcerikService service;
	
	@RequestMapping(value="/list", method= RequestMethod.GET)
	public ModelAndView website(HttpSession httpSession) {
		ModelAndView model = new ModelAndView("WEB/lstIcerik");
		model.addObject("listTask", service.getIcerik(0));
		
		return model;
	}
	@RequestMapping(value="/form/{id}", method= RequestMethod.GET)
	public ModelAndView websiteform(@PathVariable("id") Integer id, HttpSession httpSession) {
		ModelAndView model = new ModelAndView("WEB/frmIcerik");
		
		vICERIK vIcerik;
		if (id == 0) {
			vIcerik = new vICERIK();
			vIcerik.setId(0);
			vIcerik.setBaslik("");
			vIcerik.setKisa_aciklama("");
			vIcerik.setIcerik("");
			vIcerik.setResim("");
			vIcerik.setUrl("");
			
		}else {
			vIcerik = service.Find(id);	
		}
		
		model.addObject("webForm", vIcerik);
		
		return model;
	}
	
	
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public ModelAndView save(Integer id, String baslik, String kisa_aciklama, String icerik, String resim, String url) {
		
		service.setIcerik(id, baslik, kisa_aciklama, icerik, resim, url);
		
		
		return new ModelAndView("redirect:/website/icerik/list");
	}

}

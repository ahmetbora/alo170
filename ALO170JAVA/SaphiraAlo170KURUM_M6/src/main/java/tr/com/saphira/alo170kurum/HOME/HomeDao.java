package tr.com.saphira.alo170kurum.HOME;

import java.util.List;


import tr.com.saphira.alo170kurum.model.Lookup;

public interface HomeDao {
	public List<Lookup> SifrelenmemisTaskAciklamaList();
	public void TaskAciklamaSifrele(int id, String aciklama);
	public void TaskDetaySifrele(int TaskID);
	public void AllDetayUpdate();
	public DashBoard getDashboardData();
	public DashBoard getDataOzet(DashBoard d);
}

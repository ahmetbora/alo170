package tr.com.saphira.alo170kurum.HOME;

public class DashBoard {
	private STATS bugun;
	private int bildirim;
	private int vatandas;
	private int detay;
	private int acik;
	public DashBoard() {
		super();
		
	}
	public STATS getBugun() {
		return bugun;
	}
	public void setBugun(STATS bugun) {
		this.bugun = bugun;
	}
	public int getBildirim() {
		return bildirim;
	}
	public void setBildirim(int bildirim) {
		this.bildirim = bildirim;
	}
	public int getVatandas() {
		return vatandas;
	}
	public void setVatandas(int vatandas) {
		this.vatandas = vatandas;
	}
	public int getAcik() {
		return acik;
	}
	public void setAcik(int acik) {
		this.acik = acik;
	}
	public int getDetay() {
		return detay;
	}
	public void setDetay(int detay) {
		this.detay = detay;
	}
	
}

package tr.com.saphira.alo170kurum.RAPOR;

public class SimpleBean {
    private int x = 1;
    private int y = 2;

    public Integer getx() {
		return x;
	}
	public void setx(Integer x) {
		this.x = x;
	}

    public Integer gety() {
		return y;
	}
	public void sety(Integer y) {
		this.y = y;
	}
}

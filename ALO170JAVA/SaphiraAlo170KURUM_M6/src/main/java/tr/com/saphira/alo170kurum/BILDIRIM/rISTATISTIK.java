package tr.com.saphira.alo170kurum.BILDIRIM;

public class rISTATISTIK {
	
	private String il;
	private String adi;
	private String kapanan;
	private String guncelleme_talebi;
	private String birime_atandi;     // ustlenilen
	private String inceleniyor;
	private String inceleniyor_eksure;
	private String islem_yapilmamis;  // ustlenilmeyen
	private String reddedildi;
	private String toplam;
	private String zamaninda_kapatilmayan;
	private String zamaninda_kapanan;
	public rISTATISTIK() {
		super();
	
	}
	
	
	public String getIl() {
		return il;
	}
	public void setIl(String il) {
		this.il = il;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getKapanan() {
		return kapanan;
	}
	public void setKapanan(String kapanan) {
		this.kapanan = kapanan;
	}
	public String getGuncelleme_talebi() {
		return guncelleme_talebi;
	}
	public void setGuncelleme_talebi(String guncelleme_talebi) {
		this.guncelleme_talebi = guncelleme_talebi;
	}
	public String getBirime_atandi() {
		return birime_atandi;
	}
	public void setBirime_atandi(String birime_atandi) {
		this.birime_atandi = birime_atandi;
	}
	public String getInceleniyor() {
		return inceleniyor;
	}
	public void setInceleniyor(String inceleniyor) {
		this.inceleniyor = inceleniyor;
	}
	public String getInceleniyor_eksure() {
		return inceleniyor_eksure;
	}
	public void setInceleniyor_eksure(String inceleniyor_eksure) {
		this.inceleniyor_eksure = inceleniyor_eksure;
	}
	public String getIslem_yapilmamis() {
		return islem_yapilmamis;
	}
	public void setIslem_yapilmamis(String islem_yapilmamis) {
		this.islem_yapilmamis = islem_yapilmamis;
	}
	public String getReddedildi() {
		return reddedildi;
	}
	public void setReddedildi(String reddedildi) {
		this.reddedildi = reddedildi;
	}
	public String getToplam() {
		return toplam;
	}
	public void setToplam(String toplam) {
		this.toplam = toplam;
	}
	public String getZamaninda_kapatilmayan() {
		return zamaninda_kapatilmayan;
	}
	public void setZamaninda_kapatilmayan(String zamaninda_kapatilmayan) {
		this.zamaninda_kapatilmayan = zamaninda_kapatilmayan;
	}
	public String getZamaninda_kapanan() {
		return zamaninda_kapanan;
	}
	public void setZamaninda_kapanan(String zamaninda_kapanan) {
		this.zamaninda_kapanan = zamaninda_kapanan;
	}
	
	
	
}

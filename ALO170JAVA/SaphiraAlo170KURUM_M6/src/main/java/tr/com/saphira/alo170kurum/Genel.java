package tr.com.saphira.alo170kurum;

import java.io.IOException;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tr.com.saphira.alo170kurum.BILDIRIM.BildirimDao;
import tr.com.saphira.alo170kurum.BILDIRIM.vJSONRESULT;



@Component
public class Genel {

	
	
	private static BildirimDao dao;
	
	 @Autowired
	  private BildirimDao dao0;

	 
	 @PostConstruct     
	  private void initStaticDao () {
	     dao = this.dao0;
	  }
	 
	
	private static final String key = "yf6hms.4shan*-02";
	private static final String initVector = "hsjs638qjcbd573d";

	public static String encrypt(String value) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());
			return Base64.encodeBase64String(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public static String decrypt(String encrypted) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

			return new String(original);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
	
	
	public static void TCSifrele() {
		dao.TCSifrele();
	}
	
	
	public static void checkSessionRedirect(HttpSession httpSession, HttpServletResponse response) {
		try {
			if(httpSession.getAttribute("User_ID")==null || httpSession.getAttribute("User_ID")=="") {
				try {
					response.sendRedirect("/login");
				} catch (IOException e) {
			
					e.printStackTrace();
				}
			}
		}catch(Exception ex) {
			try {
				response.sendRedirect("/login");
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		}		
	}

	public static vJSONRESULT checkSessionJson(HttpSession httpSession, HttpServletResponse response) {
		vJSONRESULT res=new vJSONRESULT();
		res.setSonuc("98");
		res.setMesaj("Oturum Durumu Normal.");
		try {
			if(httpSession.getAttribute("User_ID")==null || httpSession.getAttribute("User_ID")=="") {
				res.setSonuc("99");
				res.setMesaj("Oturum Süresi Doldu. Tekrar Giriş Yapmanız Gerekmektedir.");
				return res;
			}
		}catch(Exception ex) {
			res.setSonuc("99");
			res.setMesaj("Oturum Süresi Doldu. Tekrar Giriş Yapmanız Gerekmektedir.");
			return res;
		}
		
		return res;		
	}



	private static final String[] IP_HEADER_CANDIDATES = { 
		"X-Forwarded-For",
		"Proxy-Client-IP",
		"WL-Proxy-Client-IP",
		"HTTP_X_FORWARDED_FOR",
		"HTTP_X_FORWARDED",
		"HTTP_X_CLUSTER_CLIENT_IP",
		"HTTP_CLIENT_IP",
		"HTTP_FORWARDED_FOR",
		"HTTP_FORWARDED",
		"HTTP_VIA",
		"REMOTE_ADDR" };
	
	public static String getClientIpAddress(HttpServletRequest request) {
		for (String header : IP_HEADER_CANDIDATES) {
			String ip = request.getHeader(header);
			if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
				return ip;
			}
		}
		return request.getRemoteAddr();
	}

	public static String tarihSaat(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
		LocalDateTime now = LocalDateTime.now();  
		return dtf.format(now);
	}
	
	public static void setLastUrl(HttpServletResponse response,String url){
		Cookie cookie = new Cookie("lastUrl", url);
		response.addCookie(cookie);
	}

	public static Period dateDiff(String d1,String d2){	
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		LocalDate dates = LocalDate.parse(d1, formatter);
		LocalDate sdate = LocalDate.of(dates.getYear(), dates.getMonth(), dates.getDayOfMonth());

		LocalDate datef = LocalDate.parse(d2, formatter);
		LocalDate fdate = LocalDate.of(datef.getYear(), datef.getMonth(), datef.getDayOfMonth());

		Period diff = Period.between(sdate, fdate);
		return diff;
	}

	public static String formatPhone(String number){
		String phoneNumber = number.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2 $3");
		//System.out.println(phoneNumber);
		return phoneNumber;
	}
	
    public static String generateRandomPassword(int len, int randNumOrigin, int randNumBound)
    {
        SecureRandom random = new SecureRandom();
        return random.ints(randNumOrigin, randNumBound + 1)
                .filter(i -> Character.isAlphabetic(i) || Character.isDigit(i))
                .limit(len)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString();
    }

	public static String regexTCNO(){
		return "^[1-9]{1}[0-9]{9}[02468]{1}$";
	}

	public static String replaceSpace(String txt){
		txt=txt.replaceAll("\\s+","");
		return txt;
	}
}

package tr.com.saphira.alo170kurum.BILDIRIM;

public class vTASKDETAYISLEM {
	private Integer id;
	private Integer fk_gorev;
	private Integer fk_agent;
	private String  tarih;
	private String  aciklama;
	private Integer fk_detay_tip;
	private Integer sistem;
	private String  ipadres;

	
	
	
	public vTASKDETAYISLEM() {
		super();
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getfk_gorev() {
		return fk_gorev;
	}
	public void setfk_gorev(Integer fk_gorev) {
		this.fk_gorev = fk_gorev;
	}
	public Integer getfk_agent() {
		return fk_agent;
	}
	public void setfk_agent(Integer fk_agent) {
		this.fk_agent=fk_agent;
		
	}
	public String getTarih() {
		return tarih;
	}
	public void setTarih(String tarih) {
		this.tarih = tarih;
	}
	public String getaciklama() {
		return aciklama;
	}
	public void setaciklama(String aciklama) {
		this.aciklama=aciklama;
	}
	public Integer getfk_detay_tip() {
		return fk_detay_tip;
	}
	public void setfk_detay_tip(Integer fk_detay_tip) {
		this.fk_detay_tip=fk_detay_tip;
	}
	public Integer getsistem() {
		return sistem;
	}
	public void setsistem(Integer sistem) {
		this.sistem=sistem;
	}
	public String getipadres() {
		return ipadres;
	}
	public void setipadres(String ipadres) {
		this.ipadres=ipadres;
	}
	
}

package tr.com.saphira.alo170kurum.AYARLAR;

public class vYETKIGRUPLARI {

	private Integer id;
	private String adi;
	private Integer isActive;
	private Integer isHidden;
	private Integer fk_rol;
	private Integer fk_user;
	public vYETKIGRUPLARI() {
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	
	public Integer getisActive() {
		return isActive;
	}
	public void setisActive(Integer isActive) {
		this.isActive = isActive;
	}
	public Integer getisHidden() {
		return isHidden;
	}
	public void setisHidden(Integer isHidden) {
		this.isHidden = isHidden;
	}
	public Integer getfk_user() {
		return fk_user;
	}
	public void setfk_user(Integer fk_user) {
		this.fk_user = fk_user;
	}
	public Integer getfk_rol() {
		return fk_rol;
	}
	public void setfk_rol(Integer fk_rol) {
		this.fk_rol = fk_rol;
	}
}

package tr.com.saphira.alo170SSS;

public class vSSSALTGRUP {
	private Integer id;
	private Integer fk_parent;
	private String 	adi;
	private String 	lng;
	
	public vSSSALTGRUP() {
		super();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getfk_parent() {
		return fk_parent;
	}
	public void setfk_parent(Integer fk_parent) {
		this.fk_parent = fk_parent;
	}
	
	public String getbaslik() {
		return adi;
	}
	public void setbaslik(String baslik) {
		this.adi = baslik;
	}
	
	public String getlng() {
		return lng;
	}
	public void setlng(String lng) {
		this.lng = lng;
	}
	
}

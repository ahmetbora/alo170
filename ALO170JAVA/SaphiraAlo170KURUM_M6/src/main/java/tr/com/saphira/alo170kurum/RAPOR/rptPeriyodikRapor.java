package tr.com.saphira.alo170kurum.RAPOR;

public class rptPeriyodikRapor {

    // GENEL ÇAĞRI İSTATİSTİKLERİ
    private Integer id;
    private String toplam_gelen;
    private String toplam_cevaplanan;
    private String toplam_kayip;
    private String ORT_GOR_SURE;
    private String TOPL_SURE;
    private String ORT_BEKL_SURE;
    private String toplam_bildirim;
    private String ilk_kontak;

    // başvurularda ilk 10 konu
    private String konu;
    private Integer say;

    private Integer smssay;
    private Integer emailsay;
    private Integer sure;
    private Integer dissay;



    public rptPeriyodikRapor() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String gettoplam_gelen(){
        return toplam_gelen;
    }
    public void settoplam_gelen(String toplam_gelen){
        this.toplam_gelen=toplam_gelen;
    }

    public String gettoplam_cevaplanan(){
        return toplam_cevaplanan;
    }
    public void settoplam_cevaplanan(String toplam_cevaplanan){
        this.toplam_cevaplanan=toplam_cevaplanan;
    }

    public String gettoplam_kayip(){
        return toplam_kayip;
    }
    public void settoplam_kayip(String toplam_kayip){
        this.toplam_kayip=toplam_kayip;
    }

    public String getORT_GOR_SURE(){
        return ORT_GOR_SURE;
    }
    public void setORT_GOR_SURE(String ORT_GOR_SURE){
        this.ORT_GOR_SURE=ORT_GOR_SURE;
    }

    public String getTOPL_SURE(){
        return TOPL_SURE;
    }
    public void setTOPL_SURE(String TOPL_SURE){
        this.TOPL_SURE=TOPL_SURE;
    }

    public String getORT_BEKL_SURE(){
        return ORT_BEKL_SURE;
    }
    public void setORT_BEKL_SURE(String ORT_BEKL_SURE){
        this.ORT_BEKL_SURE=ORT_BEKL_SURE;
    }

    public String gettoplam_bildirim(){
        return toplam_bildirim;
    }
    public void settoplam_bildirim(String toplam_bildirim){
        this.toplam_bildirim=toplam_bildirim;
    }

    public String getilk_kontak(){
        return ilk_kontak;
    }
    public void setilk_kontak(String ilk_kontak){
        this.ilk_kontak=ilk_kontak;
    }


    public String getkonu() {
		return konu;
	}
	public void setkonu(String konu) {
		this.konu = konu;
	}

    public Integer getsay() {
		return say;
	}
	public void setsay(Integer say) {
		this.say = say;
	}

    public Integer getsmssay() {
		return smssay;
	}
	public void setsmssay(Integer smssay) {
		this.smssay = smssay;
	}

    public Integer getemailsay() {
		return emailsay;
	}
	public void setemailsay(Integer emailsay) {
		this.emailsay = emailsay;
	}

    public Integer getsure() {
		return sure;
	}
	public void setsure(Integer sure) {
		this.sure = sure;
	}

    public Integer getdissay() {
		return dissay;
	}
	public void setdissay(Integer dissay) {
		this.dissay = dissay;
	}

}


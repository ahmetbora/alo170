package tr.com.saphira.alo170kurum.BILDIRIM;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.KART.vKART;
import tr.com.saphira.alo170kurum.SCRIPT.vSCRIPT;
import tr.com.saphira.alo170kurum.SSS.vSSS;
import tr.com.saphira.alo170kurum.model.vTotal;


@Service
public class BildirimServiceImpl implements BildirimService {

	@Autowired
	BildirimDao dao;
	
	public void setDao(BildirimDao dao) {
		this.dao = dao;
	}

	@Override
	public List<vBILDIRIM> getKullaniciHavuzBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		return dao.getKullaniciHavuzBildirimListesi( skip, take, userId, userYetki);
	}
	
	@Override
	public List<vTotal> countKullaniciHavuzBildirimListesi(Integer userId) {
		
		return dao.countKullaniciHavuzBildirimListesi(userId);
	}

	@Override
	public List<vBILDIRIM> getUzerimdekiBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		return dao.getUzerimdekiBildirimListesi(skip,take,userId,userYetki);
	}

	@Override
	public List<vTotal> countUzerimdekiBildirimListesi(Integer userId,Map<String,Object> userYetki) {
		return dao.countUzerimdekiBildirimListesi(userId,userYetki);
	}
	

	@Override
	public List<vBILDIRIM> SonuclandirdigimBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		return dao.SonuclandirdigimBildirimListesi(skip,take,userId,userYetki);
	}

	@Override
	public List<vTotal> countSonuclandirdigimBildirimListesi(Integer userId,Map<String,Object> userYetki) {
		return dao.countSonuclandirdigimBildirimListesi(userId,userYetki);
	}
	

	@Override
	public List<vBILDIRIM> ActigimBildirimListesi(Integer KullaniciID,Map<String,Object> userYetki) {
		return dao.ActigimBildirimListesi(KullaniciID, userYetki);
	}


	@Override
	public List<vBILDIRIM> getYenidenAcilanBildirimListesi(Integer skip,Integer take,Integer userLokıasyon,Map<String,Object> userYetki) {
		return dao.getYenidenAcilanBildirimListesi( skip, take, userLokıasyon, userYetki);
	}
	
	@Override
	public List<vTotal> countYenidenAcilanBildirimListesi(Integer userLokıasyon) {
		return dao.countYenidenAcilanBildirimListesi(userLokıasyon);
	}

	@Override
	public List<vBILDIRIM> getOlusturdugumBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		return dao.getOlusturdugumBildirimListesi( skip, take, userId, userYetki);
	}
	
	@Override
	public List<vTotal> countOlusturdugumBildirimListesi(Integer userId) {
		return dao.countOlusturdugumBildirimListesi(userId);
	}

	@Override
	public List<vBILDIRIM> getVtAciklamaBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		return dao.getVtAciklamaBildirimListesi( skip, take, userId, userYetki);
	}
	
	@Override
	public List<vTotal> countVtAciklamaBildirimListesi(Integer userId) {
		return dao.countVtAciklamaBildirimListesi(userId);
	}

	@Override
	public List<vBILDIRIM> getWebSitesiBildirimListesi(Integer skip,Integer take,String userIds,Map<String,Object> userYetki) {
		return dao.getWebSitesiBildirimListesi( skip, take, userIds, userYetki);
	}
	
	@Override
	public List<vTotal> countWebSitesiBildirimListesi(String userIds) {
		return dao.countWebSitesiBildirimListesi(userIds);
	}

	@Override
	public List<vBILDIRIM> getMobilBildirimListesi(Integer skip,Integer take,Map<String,Object> userYetki) {
		return dao.getMobilBildirimListesi( skip, take,userYetki);
	}
	
	@Override
	public List<vTotal> countMobilBildirimListesi() {
		return dao.countMobilBildirimListesi();
	}



	@Override
	public vKART getKart(Integer KartID,Map<String,Object> userYetki) {
		
		return dao.getKart(KartID,userYetki);
	}

	@Override
	public vKART getKartByTc(String TC) {
		
		return dao.getKartByTc(TC);
	}
	
	@Override
	public vBILDIRIM getTask(Integer TaskID,Map<String,Object> userYetki) {
		
		return dao.getTask(TaskID,userYetki);
	}

	@Override
	public List<vBILDIRIM> getBildirimAra(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki,Integer Statu,String Adi,String SoyAdi,String Tel,String Tc,Integer BildirimNo) {
		
		return dao.getBildirimAra(skip,take,userId,userYetki,Statu,Adi,SoyAdi,Tel,Tc,BildirimNo);
	}
	@Override
	public List<vTotal> countBildirimAra(Integer userId,Map<String,Object> userYetki,Integer Statu,String Adi,String SoyAdi,String Tel,String Tc,Integer BildirimNo) {
		return dao.countBildirimAra(userId,userYetki,Statu,Adi,SoyAdi,Tel,Tc,BildirimNo);
	}


	@Override
	public List<vBILDIRIM> BildirimDetayRaporu(Integer AGENT, Integer IL, Integer ILCE, Integer STATU,Map<String,Object> userYetki) {
		
		return dao.BildirimDetayRaporu(AGENT, IL, ILCE, STATU,userYetki);
	}

	@Override
	public List<vBILDIRIM> BildirimSureRaporu(String ilkTAR, String sonTAR, Integer HAVUZ) {
		
		return dao.BildirimSureRaporu(ilkTAR, sonTAR, HAVUZ);
	}

	@Override
	public List<vBILDIRIM> KurumYetkilisiSureRaporu(String ilkTAR, String sonTAR, Integer TIP) {
		
		return dao.KurumYetkilisiSureRaporu(ilkTAR, sonTAR, TIP);
	}

	@Override
	public List<vBILDIRIM> GuncellenenBildirimListesi(Integer skip,Integer take,Integer KullaniciID,Map<String,Object> userYetki) {
		
		return dao.GuncellenenBildirimListesi(skip,take,KullaniciID,userYetki);
	}

	@Override
	public List<vTotal> countGuncellenenBildirimListesi(Integer userId,Map<String,Object> userYetki) {
		return dao.countGuncellenenBildirimListesi(userId,userYetki);
	}
	
	@Override
	public List<vBILDIRIMDETAY> getBildirimDetay(Integer TaskID,Integer fkHavuz) {
		
		return dao.getBildirimDetay(TaskID,fkHavuz);
	}

	@Override
	public List<vBILDIRIMDETAY> getBildirimDetayGuncelleme(Integer TaskID) {
		
		return dao.getBildirimDetayGuncelleme(TaskID);
	}



	@Override
	public void TCSifrele() {
		dao.TCSifrele();
		
	}

	@Override
	public vADET adetUzerimdekiBild(Integer AGENT) {
		
		return dao.adetUzerimdekiBild(AGENT);
	}

	@Override
	public List<vBILDIRIM> exportExcel(Integer AGENT, String ADI, String SOYADI, String TC, String TEL, Integer TaskID,
			Integer STATU,Map<String,Object> userYetki) {
		
		return dao.exportExcel(AGENT, ADI, SOYADI, TC, TEL, TaskID, STATU, userYetki);
	}


	@Override
	public List<vIVRTC> getIvrTc(String CALLID) {
		
		return dao.getIvrTc(CALLID);
	}


	@Override
	public List<vBILDIRIM> getBildirimGecmisi(Integer kartId,Map<String,Object> userYetki) {
		return dao.getBildirimGecmisi(kartId, userYetki);
	}

	@Override
	public List<vBILDIRIM> bildirimKaydet(
			    // bildirim bilgileri
				String aciklama,Integer tip_level1,Integer tip_level2,Integer tip_level3,String arayan_numara,Integer fk_aramakanali,Integer fk_aramasebebi,Integer fk_kart,
				String uid,Integer fk_proje,Integer fk_sonuckod,Integer fk_lokasyon,Integer isOnayBekle,Integer fk_kaydi_acan,String referans,Integer fk_gorev_statu,
				Integer fk_kapatan_agent,
				// vatandaş bilgileri
				String adres,String adi,String soyadi,String tckimlik,String tel1,String gsm,Integer fk_egitimdurumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,
				String sskno,String email,Integer fk_cinsiyet,String fax,Integer fk_gsoru,String gsoru_cevap,
				// firma bilgileri
				String f_adi,String f_adres,String f_tel,Integer f_fk_il,Integer f_fk_ilce,String nace,String scs,
			
				// kart ve firma update kontrol
				Integer kart_update,Integer firma_bilgi,	
				Map<String,Object> userYetki
				) {
		return 	dao.bildirimKaydet(
			// bildirim bilgileri
			aciklama,tip_level1,tip_level2,tip_level3,arayan_numara,fk_aramakanali,fk_aramasebebi,fk_kart,
			uid,fk_proje,fk_sonuckod,fk_lokasyon,isOnayBekle,fk_kaydi_acan,referans,fk_gorev_statu,
			fk_kapatan_agent,
			// vatandaş bilgileri
			adres,adi,soyadi,tckimlik,tel1,gsm,fk_egitimdurumu,fk_ulke,fk_il,fk_ilce,
			sskno,email,fk_cinsiyet,fax,fk_gsoru,gsoru_cevap,
			// firma bilgileri
			f_adi,f_adres,f_tel,f_fk_il,f_fk_ilce,nace,scs,
		
			// kart ve firma update kontrol
			kart_update,firma_bilgi,
			userYetki
			);
	}	
/*
	@Override
	public void bildirimKaydet(
		    // bildirim bilgileri
		    String aciklama,Integer tip_level1,Integer tip_level2,Integer tip_level3,String arayan_numara,Integer fk_aramakanali,Integer fk_aramasebebi,Integer fk_kart,
			String uid,Integer fk_proje,Integer fk_sonuckod,Integer fk_lokasyon,Integer isOnayBekle,Integer fk_kaydi_acan,String referans,Integer fk_gorev_statu,
			Integer fk_kapatan_agent,
			// vatandaş bilgileri
			String adres,String adi,String soyadi,String tckimlik,String tel1,String gsm,Integer fk_egitimdurumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,
			String sskno,String email,Integer fk_cinsiyet,String fax,Integer fk_gsoru,String gsoru_cevap,
			// firma bilgileri
			String f_adi,String f_adres,String f_tel,Integer f_fk_il,Integer f_fk_ilce,String nace,String scs,
		
			// kart ve firma update kontrol
			Integer kart_update,Integer firma_bilgi
			) {
		
		dao.bildirimKaydet(
			    // bildirim bilgileri
			    aciklama,tip_level1,tip_level2,tip_level3,arayan_numara,fk_aramakanali,fk_aramasebebi,fk_kart,
				uid,fk_proje,fk_sonuckod,fk_lokasyon,isOnayBekle,fk_kaydi_acan,referans,fk_gorev_statu,
				fk_kapatan_agent,
				// vatandaş bilgileri
				adres,adi,soyadi,tckimlik,tel1,gsm,fk_egitimdurumu,fk_ulke,fk_il,fk_ilce,
				sskno,email,fk_cinsiyet,fax,fk_gsoru,gsoru_cevap,
				// firma bilgileri
				f_adi,f_adres,f_tel,f_fk_il,f_fk_ilce,nace,scs,
			
				// kart ve firma update kontrol
				kart_update,firma_bilgi
				);
	}
	*/
	/*@Override
	public void karaListeEkle(String numara,String neden,Integer fk_agent_id) {
		dao.karaListeEkle(numara,neden,fk_agent_id);
	}
*/
	
	@Override
	public List<vSSS> getBilgiBankasiListe(String term,Integer skip,Integer take) {
		return dao.getBilgiBankasiListe(term,skip,take);
	}
	
	
	@Override
	public List<vTotal> countBilgiBankasiListe(String term) {
		
		return dao.countBilgiBankasiListe(term);
	}
	
	@Override
	public vSSS getSssById(Integer ID) {
		
		return dao.getSssById(ID);
	}
	
	
	
	@Override
	public List<vSCRIPT> getBildirimScriptListe(String term,Integer skip,Integer take) {
		return dao.getBildirimScriptListe(term,skip,take);
	}
	
	
	@Override
	public List<vTotal> countBildirimScriptListe(String term) {
		
		return dao.countBildirimScriptListe(term);
	}
	
	@Override
	public List<vKULLANICILAR> getVtMesajKullaniciListe(String term,Integer UserId) {
		return dao.getVtMesajKullaniciListe(term,UserId);
	}
	
	@Override
	public List<vVTMESAJ> getVtMesajListe(String tip,Integer skip,Integer take,Integer userId) {
		return dao.getVtMesajListe(tip,skip,take,userId);
	}
	@Override
	public List<vTotal> countVtMesajListe(String term,Integer userId) {
		
		return dao.countVtMesajListe(term,userId);
	}
	
	@Override
	public vVTMESAJ getVtMesajById(Integer ID) {
		
		return dao.getVtMesajById(ID);
	}
	
	
	@Override
	public vJSONRESULT vtMesajGonder(String mesaj,Integer alici,Integer gonderen,Integer parent,Integer fkroot) {
		
		return dao.vtMesajGonder(mesaj,alici,gonderen,parent,fkroot);
	}
	
	@Override
	public List<vBILDIRIM> getBildirimListeNoHavuz(Integer skip,Integer take,Map<String,Object> userYetki) {
		return dao.getBildirimListeNoHavuz(skip,take, userYetki);
	}
	@Override
	public List<vTotal> countBildirimListeNoHavuz() {
		
		return dao.countBildirimListeNoHavuz();
	}
	
/*	@Override
	public vJSONRESULT uzerimeAl(String ids) {
		
		return dao.uzerimeAl(ids);
	}*/
	
	@Override
	public vJSONRESULT changeTaskUser(Integer id,Integer fkGorevStatu,Integer userId) {
		
		return dao.changeTaskUser(id,fkGorevStatu,userId);
	}
	
	@Override
	public vJSONRESULT addTaskDetay(Integer sistem,Integer id,Integer userId,String aciklama,String ipAdres,Integer fk_detay_tip) {
		
		return dao.addTaskDetay(sistem,id,userId,aciklama,ipAdres,fk_detay_tip);
	}	

	@Override
	public vJSONRESULT addChangeUserLog(Integer svUserId,Integer userId,Integer login,Integer bildirimId,Integer fkIslem) {
		
		return dao.addChangeUserLog(svUserId,userId,login,bildirimId,fkIslem);
	}	
	
	@Override
	public vTASKDETAYISLEM openDetayIslem(Integer Id,Integer fkGorev,Integer Sistem) {
		
		return dao.openDetayIslem(Id,fkGorev,Sistem);
	}	
	
	@Override
	public vJSONRESULT addCrmIslemSureleri(Integer fkGorev,Integer fkAgent,Integer Sistem,Integer fkDetayIslem,Integer Kurum,Integer Kurum1,Integer Kurum2,Integer fkSube,Integer fkHavuz,Integer fkIl,String Tarih) {
		
		return dao.addCrmIslemSureleri(fkGorev,fkAgent,Sistem,fkDetayIslem,Kurum,Kurum1,Kurum2,fkSube,fkHavuz,fkIl,Tarih);
	}	


	@Override
	public vJSONRESULT vatandasBilgisiKapataC(Integer Id, Integer isOnay) {
		
		return dao.vatandasBilgisiKapataC(Id, isOnay);
	}

	@Override
	public vJSONRESULT kurumAltKurumGuncelle(Integer id,Integer t1,Integer t2,Integer t3,Integer fk_aramakanali, Integer fk_aramasebebi) {
		
		return dao.kurumAltKurumGuncelle(id,t1,t2,t3,fk_aramakanali,fk_aramasebebi);
	}

	@Override
	public vJSONRESULT havuzaYonlendir(Integer id,Integer fk_havuz,Integer fkAgent,String aciklama) {
		
		return dao.havuzaYonlendir(id,fk_havuz,fkAgent,aciklama);
	}
	@Override
	public vJSONRESULT taskDBUP(Integer id) {
		
		return dao.taskDBUP(id);
	}
	@Override
	public vTASKHAVUZKULLANICI getTaskHavuzKullanici(Integer userId,Integer whereIN,String IDS,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu) {
		
		return dao.getTaskHavuzKullanici(userId,whereIN,IDS,fkHavuz,fkSube,fkIl,fkKonu);
	}
	@Override
	public vJSONRESULT taskHavuzDetayEkle(Integer fkAgent,Integer fkGorev,Integer detayIslemId,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu) {
		
		return dao.taskHavuzDetayEkle(fkAgent,fkGorev,detayIslemId,fkHavuz,fkSube,fkIl,fkKonu);
	}
	@Override
	public vJSONRESULT kullaniciyaYonlendir(Integer id,Integer fk_havuz,Integer fk_user, Integer tip_level4 ){
		
		return dao.kullaniciyaYonlendir(id,fk_havuz,fk_user, tip_level4);
	}
	@Override
	public vJSONRESULT taskChangeStatu(Integer id,Integer statu) {
		
		return dao.taskChangeStatu(id,statu);
	}
	@Override
	public vJSONRESULT bildirimKapat(Integer id,Integer fk_agent, Integer fk_taskb_kapanma_statu,String kurumYetki,String aciklama,Map<String,Object> userYetki) {
		
		return dao.bildirimKapat(id,fk_agent, fk_taskb_kapanma_statu,kurumYetki,aciklama,userYetki);
	}
	@Override
	public vJSONRESULT yenidenAc(Integer id, Integer userLokasyon,Map<String,Object> userYetki) {
		return dao.yenidenAc(id, userLokasyon,userYetki);
	}
	@Override
	public vJSONRESULT addSmsListe(Integer fk_gorev, String gsm, String mesaj) {
		return dao.addSmsListe(fk_gorev, gsm, mesaj);
	}

	@Override
	public Map<String, Object> getTaskDBDetay(Integer sistem, Integer fkGorev, Integer fkAgent,Integer limit,Integer id) {
		return dao.getTaskDBDetay(sistem, fkGorev, fkAgent,limit,id);
	}

	
}

package tr.com.saphira.alo170kurum.LOGIN;

public class User {
	  private String username;
	  private String password;
	  private String[] roles;
	  private String ukey;

	  public User(String username, String password, String... roles) {
	    this.username = username;
	    this.password = password;
	    this.roles = roles;
	  }
	  
	public String getUkey() {
		return ukey;
	}

	public void setUkey(String ukey) {
		this.ukey = ukey;
	}

	public User() {
		super();
	
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	  // Getter and Setter methods
	  
	  
	}
package tr.com.saphira.alo170kurum.BILDIRIM;

// import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Date;
// import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

// import com.fasterxml.jackson.core.JsonParseException;
// import com.fasterxml.jackson.databind.JsonMappingException;
// import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.AYARLAR.AyarlarService;
import tr.com.saphira.alo170kurum.AYARLAR.vALTKURUMLAR;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKANALI;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKONUSU;
import tr.com.saphira.alo170kurum.AYARLAR.vHAVUZLAR;
import tr.com.saphira.alo170kurum.AYARLAR.vILCELER;
import tr.com.saphira.alo170kurum.AYARLAR.vILLER;
import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.AYARLAR.vKURUMLAR;
import tr.com.saphira.alo170kurum.AYARLAR.vOZELNUMARALAR;
import tr.com.saphira.alo170kurum.AYARLAR.vSUBELER;
import tr.com.saphira.alo170kurum.AYARLAR.vULKELER;
import tr.com.saphira.alo170kurum.KART.vKART;
import tr.com.saphira.alo170kurum.LOOKUP.LOOKUPService;
import tr.com.saphira.alo170kurum.SCRIPT.vSCRIPT;
import tr.com.saphira.alo170kurum.SSS.vSSS;
import tr.com.saphira.alo170kurum.model.Lookup;
import tr.com.saphira.alo170kurum.model.vTotal;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import org.springframework.security.core.Authentication;
@SuppressWarnings("unused")
@Controller
@EnableWebSecurity
@RequestMapping(value="/bildirim")
public class BildirimController {

	@Autowired
	BildirimService service;
	
	@Autowired
	AyarlarService ayarlarService;

	@Autowired
	LOOKUPService lookupService;
	
	@Autowired
    private JavaMailSender javaMailSender;

	@Autowired 
	private HttpServletRequest request;


	
	@RequestMapping(value="", method= RequestMethod.GET)
	public ModelAndView home(HttpSession session) {
		ModelAndView model = new ModelAndView("HOME/home");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		return model;
	}
	
	/*********************** yeni bildirim formu **********************************************/
	
	@RequestMapping(value= {"/yeni/{cid}/{uid}/{recName}/{que}","/yeni"}, method= RequestMethod.GET)
	public ModelAndView yeni(@PathVariable(value = "cid",required=false) String cid,
			@PathVariable(value = "uid", required = false) String uid, 
			@PathVariable(value = "recName", required = false) String recName, 
			@PathVariable(value="que", required = false) String que,
			HttpSession session) {

		ModelAndView model = new ModelAndView("BILDIRIM/frmYeniBildirim");
		List<vULKELER> ulkeler = lookupService.getUlkeler(0);
		List<Lookup> cinsiyet = lookupService.getCinsiyet();
		List<Lookup> egitimdurumu=lookupService.getEgitimDurumu(0);
		List<Lookup> geridonuskanali=lookupService.getGeriDonusKanali();
		List<Lookup> guvenliksorusu=lookupService.getGuvenlikSorusu(0);
		List<Lookup> aramakanalitipi=lookupService.getAramaKanaliTipi();
		List<vARAMAKANALI> aramasebebi = lookupService.getAramaKanaliByTip(0);	
		List<vKURUMLAR> kurum = lookupService.getKurumlar(0,null,0,10);	
		List<vILLER> iller = lookupService.getIllerByUlke(0,190,null,null); 
		List<Lookup> sektorler=lookupService.getSektorler();
		
		
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.default.min.css");
    
        //css.add("/assets/saphira/kendo/styles/kendo.common-fiori.min.css");
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
    
        css.add("/assets/vendors/custom/alertify/alertify.css");
        css.add("/assets/css/pages/wizard/wizard-3.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/countid.js");
        js.add("/assets/saphira/jquery.blockUI.js");

		js.add("/assets/vendors/custom/alertify/alertify.js");
		
        js.add("/assets/js/pages/wizard/wizard-3.js");
 
        model.addObject("js",js);
        
		model.addObject("ulkeler", ulkeler);
		model.addObject("iller", iller);
		model.addObject("cinsiyet", cinsiyet);
		model.addObject("egitimdurumu", egitimdurumu);
		model.addObject("geridonuskanali", geridonuskanali);
		model.addObject("guvenliksorusu", guvenliksorusu);
		model.addObject("aramakanalitipi", aramakanalitipi);
		model.addObject("aramasebebi", aramasebebi);
		model.addObject("kurum", kurum);
		model.addObject("sektorler", sektorler);
		
		model.addObject("ext",session.getAttribute("ext"));
		
		model.addObject("userID",session.getAttribute("User_ID"));
		
		model.addObject("agentSonBildirimNo",session.getAttribute("agentSonBildirimNo"));
		/*************
		 * agent nesnesi
		 * son çağrı ve login status bilgileri.
		 * burdaki bilgiler pbx_agent_phones tablosundan çekilecek
		 * 
		 */
		String 	agentCallID = "0";
		Integer agentPhoneStatus=0;
		Integer agentPbxLoginStatus=0;
		String 	agentfromQue="0";
		String	agentArayanNo = "s";
		Integer userFkLokasyon=(Integer) session.getAttribute("User_fkLokasyon");
		String  userLokasyon=(String) session.getAttribute("User_LOKASYON");
		
		Integer userFkRol=(Integer) session.getAttribute("User_fkROL");
		String	fromQue=null;
		if(agentArayanNo!="") {
			agentArayanNo.replace("(","");
			agentArayanNo.replace(")","");
			agentArayanNo.replace(" ","");
			agentArayanNo.replace(" ","");
		}
		

		
		String arayanNumara=cid;
		Integer cti = 0;
		Integer wsShow = 0;
		if(arayanNumara==""){
			cti=0;
		}else{
			cti=1;
		}
		
		if(uid==agentCallID){
		    wsShow=1;
		}else{
		    wsShow=0;
		}
	
		model.addObject("agentCallID",agentCallID);
		model.addObject("agentPhoneStatus",agentPhoneStatus);
		model.addObject("agentPbxLoginStatus",agentPbxLoginStatus);
		model.addObject("agentfromQue",agentfromQue);
		model.addObject("agentArayanNo",agentArayanNo);
		model.addObject("userFkLokasyon",userFkLokasyon);
		model.addObject("userLokasyon",userLokasyon);
		model.addObject("arayanNumara",arayanNumara);
		model.addObject("fromQue",fromQue);
		model.addObject("cti",cti);
		model.addObject("wsShow",wsShow);
		model.addObject("userFkRol",userFkRol);
		
		
		
		/*********************
		 * test aramaları için tanımlanan numaralar kontrolü
		 */
		List<vOZELNUMARALAR> ozelNumaralar = lookupService.getOzelNumaralar(agentArayanNo);
		int counter=ozelNumaralar.size();
		
		Boolean ozelNumraKontrol=false;
		if(counter>0) {
			ozelNumraKontrol=true;
		}
		model.addObject("ozelNumraKontrol",ozelNumraKontrol);
		
		
		if(uid!="") {
			List<vIVRTC> ivrtc=service.getIvrTc(uid);
			model.addObject("ivrtc",ivrtc);
		}

		model.addObject("ipAdres",request.getRemoteHost());
		
		Boolean webServisGoster=false;
		if(agentPhoneStatus!='1' && agentPbxLoginStatus!='1' && wsShow!='1') {
			webServisGoster=true;
		}
		try {
			if(userFkRol==4 || userFkRol==2 || userFkRol==11 || userFkRol==9999) {
				webServisGoster=true;
			}
		} catch(Exception ex) {
			
		}
		model.addObject("webServisGoster",webServisGoster);
		
		Boolean kuyrukBilgisiGoster=false;
		try {
		if(userFkRol!=4 || userFkRol!=11 || userFkRol!=9999) {
			kuyrukBilgisiGoster=true;
		}
		} catch(Exception ex) {
			
		}		
		model.addObject("kuyrukBilgisiGoster",kuyrukBilgisiGoster);
		
		// bilgibankası konular
		List<Lookup> bilgiBankasiKonular=lookupService.getBilgiBankasiKonular();
		model.addObject("bilgiBankasiKonular",bilgiBankasiKonular); 

	
        model.addObject("yetki","");
		
		return model;
	}
	
	/*****
	 * ulkeye ait sehirler
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/ulkeDetay", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView il(@Valid Integer id) {
		ModelAndView model = new ModelAndView("lookup");
		if(id<1) {
			id=190;
		}
			List<vILLER> l = lookupService.getIllerByUlke(0,id,null,null);
			model.addObject("l", l);
		return model;
	}
	
	/*****
	 * il e ait ilceler
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/ilDetay", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView ilceler(@Valid Integer id) {
		ModelAndView model = new ModelAndView("lookup");
			List<vILCELER> l = lookupService.getIlceler(id);
			model.addObject("l", l);
		return model;
	}
	
	
	 /******
	  * ARAMA KANALI TÜRLERİ ALTINDAKİ ARAMA KANALLARI LİSTESİ
	  * @param ID
	  * @return
	  */
	@RequestMapping(value="/aramaKanaliTurDetay", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView aramakanali(@Valid Integer id) {
		ModelAndView model = new ModelAndView("lookup");
			List<vARAMAKANALI> l = lookupService.getAramaKanaliByTip(id);
			model.addObject("l", l);
		return model;
	}
	
	
	 /******
	  * KURUMLAR ALTINDAKI ALT KURUM LİSTESİ
	  * @param ID
	  * @return
	  */
	@RequestMapping(value="/altKurumlar", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView altkurumlar(@Valid Integer id) {
		ModelAndView model = new ModelAndView("lookup");
			List<vALTKURUMLAR> l = lookupService.getAltKurumlar(id);
			model.addObject("l", l);
		return model;
	}
	
	
	 /******
	  * ALT KURUMLAR ALTINDAKI ARAMA KONUSU LİSTESİ
	  * @param ID
	  * @return
	  */
	@RequestMapping(value="/aramaKonulari", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView aramaKonulari(@Valid Integer id) {
		ModelAndView model = new ModelAndView("lookup");
		if(id>0) {
		List<vARAMAKONUSU> l = lookupService.getAramaKonulari(id);
		model.addObject("l", l);
		}
		
		return model;
	}
	
	@RequestMapping(value="/tcsifrele", method= RequestMethod.GET)
	public ModelAndView tcsifrele() {
		ModelAndView model = new ModelAndView("reload");
		Genel.TCSifrele(); 
		return model;
	}
	
	
	@RequestMapping(value="/yeni/save", method=RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute("form") ModelMap model) {
		
		return new ModelAndView("redirect:/bildirim/yeni"); 
	}
	
	
	
	@RequestMapping(value="/KullaniciHavuzBildirimListesi", method= RequestMethod.GET)
	public ModelAndView KullaniciHavuzBildirimListesi(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstKullaniciHavuzBildirimListesi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
    

    
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	
	@RequestMapping(value="/KullaniciHavuzBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> KullaniciHavuzBildirimListesiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> yetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

	    List<vBILDIRIM>  vBILDIRIM;
		List<vTotal> say;
		//kullanıcı diger havuzlara ait bildirimleri gorme yetkisine sahip ise tüm bildirimleri al
		
		if(yetki.get("bildirim_digerHavuzlaraAitBildirimleriGor")!=null){
			vBILDIRIM =  service.getBildirimListeNoHavuz(skip,take,yetki);
			say = service.countBildirimListeNoHavuz();
		}else{
			//kullanıcı diger havuzlara ait bildirimleri gorme yetkisine sahip değilse kendi havuzundaki bildirimleri al
			vBILDIRIM =  service.getKullaniciHavuzBildirimListesi(skip, take,userId,yetki);
			say = service.countKullaniciHavuzBildirimListesi(userId);
		}

		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",vBILDIRIM); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}
	

	@RequestMapping(value="/YenidenAcilanBildirimListesi", method= RequestMethod.GET)
	public ModelAndView YenidenAcilanBildirimListesi(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstYenidenAcilanBildirimListesi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");

        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	

		
	@RequestMapping(value="/YenidenAcilanBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> YenidenAcilanBildirimListesiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> yetki=ayarlarService.getUserYetkiFromJSON(session);
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	
		Integer userLokasyon=0;
		// kendi havuzu dısındaki bildirimleri gorme yetkisi kontrol
		if(yetki.get("bildirim_digerHavuzlaraAitBildirimleriGor")!=null){
			userLokasyon=(Integer)session.getAttribute("User_fkLokasyon");
		}else{
			userLokasyon=0;
		}
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

	    
		List<vBILDIRIM>  vBILDIRIM =  service.getYenidenAcilanBildirimListesi(skip, take,userLokasyon,yetki);
		List<vTotal> say = service.countYenidenAcilanBildirimListesi(userLokasyon);
		Map<String,Object> maps=new HashMap<>();   
		 maps.put("data",vBILDIRIM); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}
	





	
	@RequestMapping(value="/UzerimdekiBildirimListesi", method= RequestMethod.GET)
	public ModelAndView UzerimdekiBildirimListesi(HttpSession session,Authentication authentication) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstUzerimdekiBildirimler");
	
	
		//model.addObject("listTask", service.UzerimdekiBildirimListesi(userId,userYetki));
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
    

    
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/UzerimdekiBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> UzerimdekiBildirimlerData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

		Integer userId=(Integer) session.getAttribute("User_ID");
		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		List<vBILDIRIM>  vBILDIRIM =  service.getUzerimdekiBildirimListesi(skip,take,userId,userYetki);
		List<vTotal> say = service.countUzerimdekiBildirimListesi(userId,userYetki);
		Map<String,Object> maps=new HashMap<>();   
		maps.put("data",vBILDIRIM); 
		maps.put("total",say.get(0).getTotal());
		return maps;

		
	}
	
	
	@RequestMapping(value="/SonuclandirdigimBildirimListesi", method= RequestMethod.GET)
	public ModelAndView SonuclandirdigimBildirimListesi(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstSonuclandirdigimBildirimler");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);

        Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	
	@RequestMapping(value="/SonuclandirdigimBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  SonuclandirdigimBildirimListesiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    Integer userId = (Integer)session.getAttribute("User_ID");
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		List<vBILDIRIM>  vBILDIRIM =  service.SonuclandirdigimBildirimListesi(skip,take,userId,userYetki);
		List<vTotal> say = service.countSonuclandirdigimBildirimListesi(userId,userYetki);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",vBILDIRIM); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}

	

	@RequestMapping(value="/GuncellenenBildirimListesi", method= RequestMethod.GET)
	public ModelAndView GuncellenenBildirimListesi(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstGuncellenenBildirimler");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
        		Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}

	@RequestMapping(value="/GuncellenenBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  GuncellenenBildirimListesiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    Integer userId = (Integer)session.getAttribute("User_ID");
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		List<vBILDIRIM>  vBILDIRIM =  service.GuncellenenBildirimListesi(skip,take,userId,userYetki);
		List<vTotal> say = service.countGuncellenenBildirimListesi(userId,userYetki);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",vBILDIRIM); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}


	@RequestMapping(value="/OlusturdugumBildirimListesi", method= RequestMethod.GET)
	public ModelAndView OlusturdugumBildirimListesi(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstOlusturdugumBildirimListesi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
        Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		
		return model;
	}

	@RequestMapping(value="/OlusturdugumBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  OlusturdugumBildirimListesiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    Integer userId = (Integer)session.getAttribute("User_ID");
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
   
		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		List<vBILDIRIM>  vBILDIRIM =  service.getOlusturdugumBildirimListesi(skip,take,userId,userYetki);
		List<vTotal> say = service.countOlusturdugumBildirimListesi(userId);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",vBILDIRIM); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}


	@RequestMapping(value="/VtAciklamaBildirimListesi", method= RequestMethod.GET)
	public ModelAndView VtAciklamaBildirimListesi(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstVtAciklamaBildirimListesi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
        		Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}

	@RequestMapping(value="/VtAciklamaBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  VtAciklamaBildirimListesiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    Integer userId = (Integer)session.getAttribute("User_ID");
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
   
		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanıcı uyesi olmadıgı havuzların bildirimlerini gorme yetkisi var ise userId 0 gonderiyoruz. userId 0 dan büyük olursa girilen id ye baglı havuzlarda sorgulama yapar
		if(userYetki.get("bildirim_digerHavuzlaraAitBildirimleriGor")!=null){
			userId=0;
		}else{
			userId=(Integer)session.getAttribute("User_ID");
		}
		
		List<vBILDIRIM>  vBILDIRIM =  service.getVtAciklamaBildirimListesi(skip,take,userId,userYetki);
		List<vTotal> say = service.countVtAciklamaBildirimListesi(userId);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",vBILDIRIM); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}



	@RequestMapping(value="/WebSitesiBildirimListesi", method= RequestMethod.GET)
	public ModelAndView WebSitesiBildirimListesi(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstWebSitesiBildirimListesi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
        Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}

	@RequestMapping(value="/WebSitesiBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  WebSitesiBildirimListesiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    Integer userId = (Integer)session.getAttribute("User_ID");
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
   
		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		userId=(Integer)session.getAttribute("User_ID");
		String userIds="6251,9220,6224,10751";
		List<vBILDIRIM>  vBILDIRIM =  service.getWebSitesiBildirimListesi(skip,take,userIds,userYetki);
		List<vTotal> say = service.countWebSitesiBildirimListesi(userIds);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",vBILDIRIM); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}


	@RequestMapping(value="/MobilBildirimListesi", method= RequestMethod.GET)
	public ModelAndView MobilBildirimListesi(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstMobilBildirimListesi");
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
        Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_bildirimhavuzumobil")==null){
			model.setViewName("BILDIRIM/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}

	@RequestMapping(value="/MobilBildirimListesi", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  MobilBildirimListesiData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    Integer userId = (Integer)session.getAttribute("User_ID");
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		Map<String,Object> maps=new HashMap<>(); 
		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		userId=(Integer)session.getAttribute("User_ID");
		if(userYetki.get("yetki_bildirimhavuzumobil")==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			maps.put("data",yetkiYokMsj);
			maps.put("total",0);
		}else{
			List<vBILDIRIM>  vBILDIRIM =  service.getMobilBildirimListesi(skip,take,userYetki);
			List<vTotal> say = service.countMobilBildirimListesi();
			
			maps.put("data",vBILDIRIM); 
			maps.put("total",say.get(0).getTotal());
		}
		return maps;
	}



	@RequestMapping(value= {"/bildirimdetay/{id}/{code}","/bildirimdetay/{id}"}, method= RequestMethod.GET)
	public ModelAndView bildirimdetay(@PathVariable("id") int id, @PathVariable(value="code",required=false) String code,HttpSession session,HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		vBILDIRIM vbildirim;
		Integer userId = (Integer)session.getAttribute("User_ID");
		
		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Integer DetayGoster=0;
		String yetkiYokMsj=null;

		//SMDR_Log.bildirim_log  tablosune detay log yazılacak  YAPILACAK

		vbildirim = service.getTask(id,userYetki);
		// mevcut kullanıcının, bildirimin baglı oldugu havuzla baglantısı varmı yokmu kontrol ediyoruz
		vTASKHAVUZKULLANICI userHavuz=service.getTaskHavuzKullanici(userId, 10, null, vbildirim.getfkHavuz(), 0, 0, 0);
		// bildirim detay yetkisi kontrol
		if(userHavuz.getId()==0 && userYetki.get("bildirim_digerHavuzlaraAitBildirimleriGor")==null){
			// bildirim kullanıcının atanmıs oldugu havuza baglı deil ve kendi havuzu dısındaki bildirimleri gorme yetkisi yok
			DetayGoster=0;
			yetkiYokMsj="Bu Bildirim Yetkili Olduğunuz Havuzlardan Herhangi Birine Bağlı Değil. <br>Bildirimi Görebilmeniz İçin	Yetkili Olduğunuz Havuzlardan Birine Yönlendirilmiş Olması Gerekmektedir.";
		}else if(userHavuz.getId()==0 && userYetki.get("bildirim_digerHavuzlaraAitBildirimleriGor")!=null){
			// bildirim kullanıcının atanmıs oldugu havuza ait deil. ama kendi havuzu dısındaki bildirimleri gorme yetkisi var.
			if(userYetki.get("bildirim_Detay")!=null){ 
				// kullanıcı bildirimlerin detay sayfalarını görme yetkisine sahip ise
				yetkiYokMsj="BU SAYFAYI GÖRME YETKİNİZ BULUNMAMAKTADIR. YETKİ İŞLEMLERİ İÇİN YÖNETİCİNİZE BAŞVURUNUZ.";
				DetayGoster=1;
			}
		}else if(userHavuz.getId()>0 && userYetki.get("bildirim_Detay")!=null){
			// bildirim kullanıcının atanmış oldugu havuza ait ve bildirimlerin detay sayfalarını gorme yetkisi var
			DetayGoster=1;
		}else{
			DetayGoster=0;
			yetkiYokMsj="BU SAYFAYI GÖRME YETKİNİZ BULUNMAMAKTADIR. YETKİ İŞLEMLERİ İÇİN YÖNETİCİNİZE BAŞVURUNUZ.";
		}
		model.addObject("yetkiYokMsj",yetkiYokMsj);
		if(DetayGoster>0){
			model.setViewName("BILDIRIM/BildirimDetay");
			model.addObject("task", vbildirim);
			model.addObject("kart", service.getKart(vbildirim.getFk_kart(), userYetki));
			model.addObject("detay", service.getBildirimDetay(id,vbildirim.getfkHavuz()));
			model.addObject("detayGuncelleme", service.getBildirimDetayGuncelleme(id));
			model.addObject("userID", userId);
		}else{
			model.setViewName("BILDIRIM/YetkiYok");
		}
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		return model;
	}
	
	
	@RequestMapping(value= {"/kurumAltKurumGuncelle"}, method= RequestMethod.POST)
	public ModelAndView kurumAltKurumGuncelle(Integer id,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		
	    Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// bildirim detay yetkisi kontrol
		if(userYetki.get("bildirim_kurumAltKurumGuncelle")!=null){
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");

		model.addObject("js",js);
					
			List<Lookup> aramakanalitipi=lookupService.getAramaKanaliTipi();
			List<vARAMAKANALI> aramasebebi = lookupService.getAramaKanaliByTip(0);	
			List<vKURUMLAR> kurum = lookupService.getKurumlar(0,null,0,10);	
			model.addObject("aramakanalitipi", aramakanalitipi);
			model.addObject("aramasebebi", aramasebebi);
			model.addObject("kurum", kurum);	
			model.addObject("id",id);	
			model.setViewName("BILDIRIM/detayIslemler/kurumAltKurumGuncelle");

		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			model.setViewName("BILDIRIM/YetkiYokAjax");
		}
		return model;
	}
	


	@RequestMapping(value="/kurumAltKurumKaydet", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> kurumAltKurumKaydet(Integer t1,Integer t2,Integer t3,Integer id,Integer fk_aramakanali,Integer fk_aramasebebi,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>(); 
		
		if(userYetki.get("bildirim_kurumAltKurumGuncelle")!=null){ 
			maps.put("data",t1+"-"+t2+"-"+t3+"-"+id+"-"+fk_aramakanali+"-"+fk_aramasebebi); 
			maps.put("total","1");
			maps.put("response",service.kurumAltKurumGuncelle(id, t1, t2, t3, fk_aramakanali, fk_aramasebebi));
		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			maps.put("message","Bu İşlem İçin Yetkiniz Yok!");
		}
		return maps;
		
	}
	

	@RequestMapping(value="/bildirimsorgulama", method= RequestMethod.GET)
	public ModelAndView bildirimsorgulama(HttpServletRequest req, HttpServletResponse res,HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/BildirimSorgulama");
		List<Lookup> statu = lookupService.getTaskStatu(0);
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
    

    
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
         
		model.addObject("statu", statu);
				Integer userId=(Integer) session.getAttribute("User_ID");
		
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	
	
	@RequestMapping(value="/bildirimsorgulama", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> bildirimsorgulamaData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    Integer Statu=999;
	    String Adi=null;
	    String SoyAdi=null;
	    String Tel=null;
	    String Tc=null;
	    Integer BildirimNo=0;
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    } 
	  
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			 Statu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();			
		} catch (Exception e) {
			Statu=9999;
		}

		try {
			 Adi=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			Adi=null;
		}
		try {
			 SoyAdi=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			SoyAdi=null;
		}
		try {
			 Tel=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			Tel=null;
		}
		try {
			 Tc=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			Tc=null;
		}
		try {
			 BildirimNo=filter.getAsJsonObject().getAsJsonArray("filters").get(5)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			BildirimNo=0;
		}		

	
	    //Map<String,Object> userYetki = null;
		List<vBILDIRIM>  vBILDIRIM =  service.getBildirimAra(skip,take,userId,userYetki,Statu,Adi,SoyAdi,Tel,Tc,BildirimNo);
		List<vTotal> say = service.countBildirimAra(userId,userYetki,Statu,Adi,SoyAdi,Tel,Tc,BildirimNo);
		
		/***********************************
		 * 
		 * XLS EXPORT 20 30 BIN KAYIT TESTI WINDOWS DA 2 SN SÜRÜYOR
		 */
		/*
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Persons");
		sheet.setColumnWidth(0, 6000);
		sheet.setColumnWidth(1, 4000);
		Row header = sheet.createRow(0);
		Cell headerCell = header.createCell(0);
		headerCell.setCellValue("Name");

		headerCell = header.createCell(1);
		headerCell.setCellValue("Age");
		for (int i = 0; i < vBILDIRIM.size()*3000; i++) {
			Row row = sheet.createRow(i+2);
			Cell cell = row.createCell(0);
			cell.setCellValue(vBILDIRIM.get(0).getBasvuruSahibi());
			
			Cell cell1 = row.createCell(1);
			cell1.setCellValue(vBILDIRIM.get(0).getId());
			
			
		}
		File currDir = new File(".");
		String path = currDir.getAbsolutePath();
		String fileLocation = path.substring(0, path.length() - 1) + "temp.xlsx";
		 
		FileOutputStream outputStream = new FileOutputStream(fileLocation);
		workbook.write(outputStream);
		workbook.close();
		*/
		Map<String,Object> maps=new HashMap<>();     
		 maps.put("data",vBILDIRIM); 
		 maps.put("total",say.get(0).getTotal()); 
		return maps;
	}
	

	
	@RequestMapping(value="/export", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView excelexport(Integer AGENT, String adi , String soyadi, String tc, String tel, Integer bildirim_no, Integer statu, HttpServletRequest req, HttpServletResponse res,HttpSession session) throws ParseException{
		ModelAndView model = new ModelAndView(new ExcelExport());
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		List<vBILDIRIM>  vBildirim =  service.exportExcel(AGENT, adi, soyadi, tc, tel, bildirim_no, statu,userYetki);

		
	//	if(typeReport != null && typeReport.equals("xls")) {
			//ModelAndView model1 = new ModelAndView(new ExcelExport(), "userList", vBildirim);
			
	//		return new ModelAndView(new ExcelExport(), "filtre", vBildirim);
	//	}
		
		model.addObject("filtre", vBildirim);
		
		
		return model;
	}
	
	@RequestMapping(value="/bildirimdetayraporu", method= RequestMethod.GET)
	public ModelAndView bildirimdetaysorgulama(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/BildirimDetayRaporu");
	
		List<vILLER> iller = lookupService.getIller(0);
		List<Lookup> statu = lookupService.getTaskStatu(0);
		List<Lookup> aramakanalitipi = lookupService.getAramaKanaliTipi();
		List<Lookup> aramakanali = lookupService.getAramaKanali(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
		List<Lookup> aramasebebi = lookupService.getAramaSebebi(0);	
		

		model.addObject("kurumlar", kurumlar);
		
		model.addObject("iller", iller);
		model.addObject("statu", statu);
		model.addObject("aramakanalitipi", aramakanalitipi);
		model.addObject("aramakanali", aramakanali);
		model.addObject("aramasebebi", aramasebebi);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	
	@RequestMapping(value="/bildirimdetaysorgulama", method= RequestMethod.POST)
	public ModelAndView bildirimdetayraporu(Integer AGENT, Integer iller, Integer ilceler, Integer statu,HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstBildirimRaporDetay");
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		List<vBILDIRIM>  vBildirim =  service.BildirimDetayRaporu(6017, iller, ilceler, statu,userYetki);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		model.addObject("filtre", vBildirim);
			
		return model;
	}
	
	@RequestMapping(value="/bildirimsureraporu", method= RequestMethod.GET)
	public ModelAndView bildirimsuresorgulama(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/HavuzBildirimSureRaporu");
		
		List<Lookup> havuz = lookupService.cmbgetHavuzlar(6017);
		
		model.addObject("havuz", havuz);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	
	@RequestMapping(value="/bildirimsuredetay", method= RequestMethod.POST)
	public ModelAndView bildirimsureraporu(String ilkTAR, String sonTAR, Integer HAVUZ) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstBildirimFiltre");
		
		List<vBILDIRIM>  vBildirim =  service.BildirimSureRaporu(ilkTAR, sonTAR, HAVUZ);
		
		model.addObject("filtre", vBildirim);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	
	@RequestMapping(value="/kurumyetkilisisureraporu", method= RequestMethod.GET)
	public ModelAndView kurumyetkilisisuresorgulama(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/KurumYetkilisiSureRaporu");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	
	@RequestMapping(value="/kurumyetkilisisuredetay", method= RequestMethod.POST)
	public ModelAndView kurumyetkilisisureraporu(String ilkTAR, String sonTAR, Integer TIP) {
		ModelAndView model = new ModelAndView("BILDIRIM/lstBildirimFiltre");
		
		List<vBILDIRIM>  vBildirim =  service.KurumYetkilisiSureRaporu(ilkTAR, sonTAR, TIP);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		model.addObject("filtre", vBildirim);
		
		
		return model;
	}
	
	
	@RequestMapping(value="/konularagorebildirimraporu", method= RequestMethod.GET)
	public ModelAndView konularagorebildirimsorgulama(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/KonularaGoreBildirimRaporu");
		
		List<Lookup> statu = lookupService.getTaskStatu(0);
		List<vKURUMLAR> kurumlar = lookupService.getKurumlar(0,null,0,10);
		
		
		model.addObject("kurumlar", kurumlar);
		model.addObject("statu", statu);
		
		return model;
	}
	
	//filtre detay gelecek
	
	

	
	@RequestMapping(value="/suresigecenbasvurular", method= RequestMethod.GET)
	public ModelAndView suresigecenbasvurular(HttpSession session) {
		ModelAndView model = new ModelAndView("BILDIRIM/SuresiGecenBasvurular");
		
		List<Lookup> havuz = lookupService.cmbgetHavuzlar(6017);
		
		model.addObject("havuz", havuz);
		
		return model;
	}
	
	//filtre detay gelecek
	
	
	@ResponseBody
	@GetMapping(("/adetUzerimdekiBild"))
	public vADET adetUzerimdekiBild(HttpSession session) {
		
		Integer AGENT = (Integer)session.getAttribute("User_ID");
		
		return service.adetUzerimdekiBild(AGENT);

	}
	
	//COMBOLAR
	@RequestMapping(value="/ilceDetay", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView ilce(@Valid Integer id) {
		ModelAndView model = new ModelAndView("lookup");
			List<vILCELER> l = lookupService.getIlceler(id);
			List<Lookup> aramakanali = lookupService.getAramaKanali(id);
			
			model.addObject("l", aramakanali);
			model.addObject("l", l);
		return model;
	}
	
	@RequestMapping(value="/openKart",method=RequestMethod.POST)
	public @ResponseBody vKART add(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		vKART kart = service.getKartByTc(request.getParameter("tckn"));
		return kart;
	}
	
	
	 
	 @ResponseBody
	 @RequestMapping(method = RequestMethod.GET, value = "/excelExport")
	 protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
				HttpServletResponse response) throws Exception {
		 
		 response.setHeader("Content-Disposition", "attachment; filename=\"test.xls\"");
			
			@SuppressWarnings("unchecked")
			List<vBILDIRIM> list = (List<vBILDIRIM>) model.get("filtre");
			
			Sheet sheet = workbook.createSheet("User List");
			
	        Font headerFont = workbook.createFont();
	        headerFont.setBold(true);
	        headerFont.setFontHeightInPoints((short) 14);
	        
			
			//header row
			Row header = sheet.createRow(0);
			header.createCell(0).setCellValue("NO");
			header.createCell(1).setCellValue("VATANDAS");
			header.createCell(2).setCellValue("TARIH");
			header.createCell(3).setCellValue("ISI YAPACAK AGENT");
			header.createCell(4).setCellValue("KONU");
			header.createCell(5).setCellValue("GOREV DURUMU");
			header.createCell(6).setCellValue("SEHIR");
			header.createCell(7).setCellValue("SURE");
			
			int rowNum = 1;
			
			for(vBILDIRIM vbildirim : list) {
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(vbildirim.getId());
				row.createCell(1).setCellValue(vbildirim.getBasvuruSahibi());
				row.createCell(2).setCellValue(vbildirim.getTarih());
				row.createCell(3).setCellValue(vbildirim.getIsiYapacakAgent());
				row.createCell(4).setCellValue(vbildirim.getKonu1());
				row.createCell(5).setCellValue(vbildirim.getStatu());
				row.createCell(6).setCellValue(vbildirim.getIlAdi());
				row.createCell(7).setCellValue(vbildirim.getHavuzSure());
			}
		 
	 }
	 
	 
		@RequestMapping(value="/bildirimGecmisi", method = { RequestMethod.GET, RequestMethod.POST })
		public ModelAndView bildirimara(@Valid Integer id,  HttpServletRequest req, HttpServletResponse res,HttpSession session) throws ParseException{
			ModelAndView model = new ModelAndView("BILDIRIM/lstBildirimGecmisi");
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			List<vBILDIRIM>  vBildirim =  service.getBildirimGecmisi(id,userYetki);
			model.addObject("listTask", vBildirim);
			return model;
		}
		
		
		@RequestMapping(value="/kuyrukBilgisi", method=RequestMethod.POST)
		@ResponseBody
		public ModelAndView kuyrukBilgisi() {
			ModelAndView model = new ModelAndView("BILDIRIM/kuyrukBilgisi");
				String from_que="1";
				/*
				 * kullanılan sql
				 * $sql1="select from_que from pbx_agent_phones where ext=".$_SESSION["ext"]." and pbx_phone_status=1 and fk_pbx_login_status=1";
				 */
				model.addObject("l", from_que);
			return model;
		}		
	

		@RequestMapping(value="/karaListe",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT karaListe(String numara, String neden, Integer fk_agent_id,HttpServletRequest request, HttpServletResponse response) throws Exception {
			vJSONRESULT res =new vJSONRESULT();
			/*
			 * $sql="insert ignore pbx_blacklist set numara='".$numara."',neden='".$aciklama."',fk_agent_id='".$_SESSION["UserID"]."'";
			 * procedure,sevice,da eklenecek
			 */
			//service.karaListeEkle(numara,neden,fk_agent_id);
			res.setSonuc("1");
			res.setMesaj("Numara Kara Listeye Eklendi");
			return res;
		}
		
		
		@RequestMapping(value="/bildirimKaydet",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT bildirimKaydet(String adi,String soyadi,String tckimlik,Integer fk_ulke,Integer fk_il,Integer fk_ilce,
				String sskno,String dt,String email,String tel1,Integer fk_cinsiyet,String gsm,String fax,Integer fk_egitimdurumu,Integer fk_geri_donus_kanali,String adres,Integer fk_gsoru,String gsoru_cevap,String arayan_numara,Integer arama_kanali_tipi,
				Integer fk_aramakanali,Integer fk_aramasebebi,Integer tip_level1,Integer tip_level2,Integer tip_level3,String aciklama,Integer fk_kart,String uid,Integer kod,Integer firma_bilgi,Integer kart_update,Integer isOnayBekle,Integer fk_proje,
				String f_adi,String f_adres,String f_tel,Integer f_fk_il,Integer f_fk_ilce,String nace,String scs,
				HttpServletRequest request, HttpServletResponse response) throws Exception {
			vJSONRESULT res =new vJSONRESULT();
			HttpSession session=request.getSession(false);
			Integer kullanici_id=(Integer) session.getAttribute("User_ID");
			Integer fk_lokasyon=(Integer) session.getAttribute("User_fkLokasyon");
	
			aciklama.toUpperCase();
			Integer fk_sonuckod=2;
			Integer fk_kapatan_agent=0;
			String  referans="task";
			Integer fk_kaydi_acan=kullanici_id;
			Integer fk_gorev_statu=0;
			
			if (fk_gsoru==null) {
				fk_gsoru=0;
			} 
			/******
			 * fk_proje alanı ihbar ve şikayetlerde açılan firma bilgisi kısmında firmanın bağlı oldugu sektör bilgisini içerir
			 */
			if(fk_proje==null) {
				fk_proje=0;
			}
			
			/*****************
			 * bildirim formundaki kaydet kapat butonu ile kayıt yapıldıysa gorev statu ve kapatan agent_bilgisi set edilir.
			 */
			if(kod==2){
				fk_gorev_statu=99;
				fk_kapatan_agent=fk_kaydi_acan;
			}

			//System.out.println("aciklama=" + aciklama + "&" + "tip_level1="+tip_level1+"&"+"tip_level2="+tip_level2 + "&" + "tip_level3=" + tip_level3 + "&" + "arayan_numara="+arayan_numara  + "&" +  "fk_aramakanali=" + fk_aramakanali + "&" + "fk_aramasebebi=" + fk_aramasebebi + "&" + "fk_kart=" + fk_kart + "&" + "uid=" + uid + "&" + "fk_proje=" + fk_proje + "&" + "fk_sonuckod=" + fk_sonuckod + "&" + "fk_lokasyon=" + fk_lokasyon + "&" + "isOnayBekle=" + isOnayBekle + "&" + "fk_kaydi_acan=" + fk_kaydi_acan + "&" + "referans="+referans + "&" +"fk_gorev_statu="+fk_gorev_statu + "&" +"fk_kapatan_agent="+fk_kapatan_agent + "&" +"adres="+adres + "&" +"adi="+adi + "&" +"soyadi="+soyadi + "&" +"tckimlik="+tckimlik + "&" +"tel1="+tel1 + "&" +"gsm="+gsm + "&" +"fk_egitimdurumu="+fk_egitimdurumu + "&" +"fk_ulke="+fk_ulke + "&" +"fk_il="+fk_il + "&" +"fk_ilce="+fk_ilce + "&" +"sskno="+sskno + "&" +"email="+email + "&" +"fk_cinsiyet="+fk_cinsiyet + "&" +"fax="+fax + "&" +"fk_gsoru="+fk_gsoru + "&" +"gsoru_cevap="+gsoru_cevap + "&" +"f_adi="+f_adi + "&" +"f_adres="+f_adres + "&" +"f_tel="+f_tel + "&" +"f_fk_il="+f_fk_il + "&" +"f_fk_ilce="+f_fk_ilce + "&" +"nace="+nace + "&" +"scs="+scs + "&" +"kart_update="+kart_update + "&" +"firma_bilgi="+firma_bilgi);
			
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			List<vBILDIRIM>  vBildirim =  service.bildirimKaydet(
								    // bildirim bilgileri
									aciklama,tip_level1,tip_level2,tip_level3,arayan_numara,fk_aramakanali,fk_aramasebebi,fk_kart,
									uid,fk_proje,fk_sonuckod,fk_lokasyon,isOnayBekle,fk_kaydi_acan,referans,fk_gorev_statu,
									fk_kapatan_agent,
									// vatandaş bilgileri
									adres,adi,soyadi,tckimlik,tel1,gsm,fk_egitimdurumu,fk_ulke,fk_il,fk_ilce,
									sskno,email,fk_cinsiyet,fax,fk_gsoru,gsoru_cevap,
									// firma bilgileri
									f_adi,f_adres,f_tel,f_fk_il,f_fk_ilce,nace,scs,
								
									// kart ve firma update kontrol
									kart_update,firma_bilgi,	
									userYetki
									);
								

									session.setAttribute("agentSonBildirimNo", vBildirim.get(0).getId());		
									Integer sonBildirim=vBildirim.get(0).getId();
									res.setSonBildirim(sonBildirim);
									res.setSonuc("1");
									res.setMesaj("Bildirim Kaydedildi");
									return res;
			
		}
		
		
		
		@RequestMapping(value="/bilgiBankasiListeJSON",method=RequestMethod.POST)
		public @ResponseBody Map<String,Object>  bilgiBankasiListe(HttpServletRequest request, HttpServletResponse response) throws Exception {
			String term="";
			String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		    int take = jsonObject.get("take").getAsInt();
		    int skip = jsonObject.get("skip").getAsInt();   
		    int page = jsonObject.get("page").getAsInt();
		    int pageSize = jsonObject.get("pageSize").getAsInt();

		    if(take<1) {
		    	take=30;
		    }
		    if(skip<1) {
		    	skip=0;
		    }
		    if(page<1) {
		    	page=1;
		    }
		    if(pageSize<1) {
		    	pageSize=30;
		    }

			try {
			JsonParser jsonParser = new JsonParser();
			String filter =jsonParser.parse(querystring)
				    .getAsJsonObject().get("filter")
				    .getAsJsonObject().getAsJsonArray("filters").get(0)
				    .getAsJsonObject().get("value")
				    .getAsString();
					term=filter;	
			}
			catch (Exception ex) {
				 System.out.println("filtre bulunamadı");
			}
			
			List<vSSS>  vSSS =  service.getBilgiBankasiListe(term,skip,take);
			List<vTotal> say = service.countBilgiBankasiListe(term);
			Map<String,Object> maps=new HashMap<>();
			 maps.put("data",vSSS); 
			 maps.put("total",say.get(0).getTotal());
			return maps;
		}
		

		
		@RequestMapping(value="/bilgiBankasiDetay", method=RequestMethod.POST)
		@ResponseBody
		public ModelAndView bilgiBankasiDetay(@Valid Integer sssId) {
			ModelAndView model = new ModelAndView("BILDIRIM/bilgiBankasiDetay");
			sssId =  Integer.parseInt(request.getParameter("sssId"));
			vSSS sss = service.getSssById(sssId);	
			model.addObject("sss", sss);
			return model;
		}
		
		
		
		
		@RequestMapping(value="/bildirimScriptListeJSON",method=RequestMethod.POST)
		public @ResponseBody Map<String,Object> bildirimScriptListe(HttpServletRequest request, HttpServletResponse response) throws Exception {
			String term="";
			String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		    int take = jsonObject.get("take").getAsInt();
		    int skip = jsonObject.get("skip").getAsInt();   
		    int page = jsonObject.get("page").getAsInt();
		    int pageSize = jsonObject.get("pageSize").getAsInt();

		    if(take<1) {
		    	take=30;
		    }
		    if(skip<1) {
		    	skip=0;
		    }
		    if(page<1) {
		    	page=1;
		    }
		    if(pageSize<1) {
		    	pageSize=30;
		    }

			try {
			JsonParser jsonParser = new JsonParser();
			String filter =jsonParser.parse(querystring)
				    .getAsJsonObject().get("filter")
				    .getAsJsonObject().getAsJsonArray("filters").get(0)
				    .getAsJsonObject().get("value")
				    .getAsString();
					term=filter;	
			}
			catch (Exception ex) {
				 //System.out.println("filtre bulunamadı");
			}
			
			List<vSCRIPT>  vSCRIPT =  service.getBildirimScriptListe(term,skip,take);
			List<vTotal> say = service.countBildirimScriptListe(term);
			Map<String,Object> maps=new HashMap<>(); 
			maps.put("data",vSCRIPT); 
			maps.put("total",say.get(0).getTotal());
			return maps;
		}
		
		
		
		@RequestMapping(value="/vtMesajKullaniciListe",method=RequestMethod.POST)
		public @ResponseBody Map<String,Object> vtMesajKullaniciListe(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			String term="";
			Integer AGENT = (Integer)session.getAttribute("User_ID");
			String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
			term = jsonObject.get("term").getAsString();
			
			List<vKULLANICILAR>  vKULLANICILAR =  service.getVtMesajKullaniciListe(term,AGENT);
			Map<String,Object> maps=new HashMap<>(); 
			maps.put("data",vKULLANICILAR); 
			return maps;
		}
		
		
		
		@RequestMapping(value="/vtMesajListeJSON",method=RequestMethod.POST)
		public @ResponseBody Map<String,Object>  vtMesajListe(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
			String tip=null;
			String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		    int take = jsonObject.get("take").getAsInt();
		    int skip = jsonObject.get("skip").getAsInt();   
		    int page = jsonObject.get("page").getAsInt();
		    int pageSize = jsonObject.get("pageSize").getAsInt();
		    Integer userId = (Integer)session.getAttribute("User_ID");

		    if(take<1) {
		    	take=30;
		    }
		    if(skip<1) {
		    	skip=0;
		    }
		    if(page<1) {
		    	page=1;
		    }
		    if(pageSize<1) {
		    	pageSize=30;
		    }

			try {
			JsonParser jsonParser = new JsonParser();
			String filter =jsonParser.parse(querystring)
				    .getAsJsonObject().get("filter")
				    .getAsJsonObject().getAsJsonArray("filters").get(0)
				    .getAsJsonObject().get("value")
				    .getAsString();
			tip=filter;	
			;
			}
			catch (Exception ex) {
				 System.out.println("filtre bulunamadı");
			}
			
			//userId=1009;
			List<vVTMESAJ>  vVTMESAJ =  service.getVtMesajListe(tip,skip,take,userId);
			List<vTotal> say = service.countVtMesajListe(tip,userId);
			Map<String,Object> maps=new HashMap<>();  
			 maps.put("data",vVTMESAJ); 
			 maps.put("total",say.get(0).getTotal());
			return maps;
		}
		
		@RequestMapping(value="/vtMesajDetay", method=RequestMethod.POST)
		@ResponseBody
		public ModelAndView vtMesajDetay(@Valid Integer mesajId,HttpSession session) {
			ModelAndView model = new ModelAndView("BILDIRIM/vtMesajDetay");
			mesajId =  Integer.parseInt(request.getParameter("mesajId"));
			vVTMESAJ mesaj = service.getVtMesajById(mesajId);	
			int gonderen=(int) session.getAttribute("User_ID");
			model.addObject("userId",gonderen);
			model.addObject("mesaj", mesaj);
			return model;
		}
		
		@RequestMapping(value="/vtMesajGonder",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT vtMesajGonder(String mesaj, Integer alici,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			int gonderen=(int) session.getAttribute("User_ID");
			
			vJSONRESULT res = service.vtMesajGonder(mesaj,alici,gonderen,0,0);
			if(res.getSonuc()=="0") {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
			return res;
		}
		
		@RequestMapping(value="/vtMesajCevapla",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT vtMesajCevapla(String mesaj, Integer alici,Integer parentId,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			int gonderen=(int) session.getAttribute("User_ID");
			vVTMESAJ parentMsj=service.getVtMesajById(parentId);
			vJSONRESULT res = service.vtMesajGonder(mesaj,alici,gonderen,parentId,parentMsj.getfk_root());
			if(res.getSonuc()=="0") {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
			return res;
		}
		
		public void yetki() {
			JsonParser parser = new JsonParser();
	        try {
				Object obj = parser.parse(new FileReader(
						"./src/main/java/tr/com/saphira/alo170kurum/YETKI/fileName.json"));
				JsonObject jsonObject = (JsonObject) obj;
				
				System.out.println(jsonObject.get("Name").getAsString());
			} catch (JsonIOException e) {
				
				e.printStackTrace();
			} catch (JsonSyntaxException e) {
				
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				
				e.printStackTrace();
			}
		}
		
		
		
		@RequestMapping(value="/BildirimListesiNoHavuz", method= RequestMethod.GET)
		public ModelAndView BildirimListesiNoHavuz(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
			ModelAndView model = new ModelAndView("BILDIRIM/lstBildirimListesiNoHavuz");
	        // kullanılacak css 
	        List<String> css = new ArrayList<String>(); 
	        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
	        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
	        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
	    

	    
	        css.add("/assets/vendors/custom/alertify/alertify.css");
	        model.addObject("css",css);
	        
	        // kullanılacak js
	        List<String> js=new ArrayList<String>();
	        js.add("/assets/saphira/kendo/js/jszip.min.js");
	        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
	        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
	        js.add("/assets/saphira/custom.js");
	        js.add("/assets/saphira/jquery.blockUI.js");
	        js.add("/assets/vendors/custom/alertify/alertify.js");
	        model.addObject("js",js);
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			ayarlarService.getUserYetkiBildirimModelView(model, session);
			String url=request.getRequestURL().toString();
			String[] urlArr=url.split("/");
			model.addObject("menu",urlArr[urlArr.length-1]);
			return model;
		}
		
		@RequestMapping(value="/BildirimListesiNoHavuz", method= RequestMethod.POST)
		public @ResponseBody Map<String,Object> BildirimListesiNoHavuzData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	
			String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		    int take = jsonObject.get("take").getAsInt();
		    int skip = jsonObject.get("skip").getAsInt();   
		    int page = jsonObject.get("page").getAsInt();
		    int pageSize = jsonObject.get("pageSize").getAsInt();
		 
		    if(take<1) {
		    	take=30;
		    }
		    if(skip<1) {
		    	skip=0;
		    }
		    if(page<1) {
		    	page=1;
		    }
		    if(pageSize<1) {
		    	pageSize=30;
		    }

		    
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			List<vBILDIRIM>  vBILDIRIM =  service.getBildirimListeNoHavuz(skip,take,userYetki);
			List<vTotal> say = service.countBildirimListeNoHavuz();
			
			Map<String,Object> maps=new HashMap<>();   
			 maps.put("data",vBILDIRIM); 
			 maps.put("total",say.get(0).getTotal());
			return maps;
		}
			
		
	
		@RequestMapping(value="/uzerimeAl",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT uzerimeAl(String ids,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			String uzerimeAlYetki=userYetki.get("bildirim_UzerimeAl").toString();
			String uzerimeAlYetkiAll=userYetki.get("bildirim_UzerimeAlAll").toString();
			
			
			if(uzerimeAlYetki !=null || uzerimeAlYetkiAll!=null) {
				
				String[] split = ids.split(",");
				Integer bildirimId=0;
			
				for (int i = 0; i < split.length; i++) {
					bildirimId=Integer.parseInt(split[i]);
					
					if(split[i]!="") {
						
						vBILDIRIM vbildirim = service.getTask(bildirimId,userYetki);
						
						Integer il=	vbildirim.getfkIl();
						Integer Havuz=	vbildirim.getfkHavuz();
						Integer Sube=	vbildirim.getfkSube();
						Integer Kurum=	vbildirim.getTip_level1();
						Integer Kurum1=	vbildirim.getTip_level2();
						Integer Kurum2=	vbildirim.getTip_level3();
						Integer fkAgent= vbildirim.getFk_agent();
						Boolean isControllingUser= false; // supervisor kurum kullanıcısı ile işlem yapıyor ise true
						
						if(fkAgent==0 && vbildirim.getfkGorevStatu()<99 && fkAgent!=userId) {
							String aciklama="Bildirimi üstüne aldı ve okudu [Sistem]";
							
							Integer fkGorevStatu=1;
							Integer sistem=5;
							String ipAdres=Genel.getClientIpAddress(request);
							vJSONRESULT resChangeTaskUser = service.changeTaskUser(bildirimId,fkGorevStatu,userId);
							
							vJSONRESULT resAddTaskDetay=new vJSONRESULT();
							vTASKDETAYISLEM resSonYenidenAcilmaDetayIslem=new vTASKDETAYISLEM();
							vTASKDETAYISLEM resSonUzerimeAlIslem=new vTASKDETAYISLEM();
							if(resChangeTaskUser.getSonuc()=="1") { // bildirim agent atama işlemi başarılı ise
								System.out.println("********************* detay işlem ekle start ************************");
								resAddTaskDetay = service.addTaskDetay(sistem,bildirimId,userId,aciklama,ipAdres,100); // üzerine alma detay işlemi ekle
								System.out.println("********************* detay işlem ekle end ************************");
								if(isControllingUser==true) { // eğer kullanıcı hesabı kontrolü yapılıyor ise
									Integer svUserId=92; //kurum kullanıcısını kontrol eden supervisor id si
									Integer login=0;
									Integer fkIslem=resAddTaskDetay.getsonTaskDetayIslem();
									vJSONRESULT resAddChangeUserLog = service.addChangeUserLog(svUserId,userId,login,bildirimId,fkIslem);
								}
								System.out.println("********************* opendetay işlem 4 start ************************");
								resSonYenidenAcilmaDetayIslem=service.openDetayIslem(0,bildirimId,4); // bildirim en son yeniden acılma işlemi var ise alıyoruz
								System.out.println("********************* opendetay işlem 4 end ************************");
								String yenidenAcilmaTar=resSonYenidenAcilmaDetayIslem.getTarih();
								String Tar=resSonYenidenAcilmaDetayIslem.getTarih();
								Integer Sistem=resSonYenidenAcilmaDetayIslem.getsistem();
								Integer DetayIslemId=resSonYenidenAcilmaDetayIslem.getId();
								
								if(yenidenAcilmaTar==null || yenidenAcilmaTar=="") { // son yeniden acılma işlemi yok ise son uzerime alma işlemini acıyoruz
									System.out.println("********************* 0---"+bildirimId+"---5 opendetay işlem 5 start ************************");
									resSonUzerimeAlIslem = service.openDetayIslem(0,bildirimId,5);
									System.out.println("********************* 0---"+bildirimId+"---5 opendetay işlem 5  end ************************");
									Tar=resSonUzerimeAlIslem.getTarih();
									Sistem=resSonUzerimeAlIslem.getsistem();
									DetayIslemId=resSonUzerimeAlIslem.getId();
								}
						  
								// crm islem süreleri ekleme işlemi
								if(Sube==null) {
									Sube=0;
								}
								System.out.println("********************* işlem süre ekle start ************************");
								vJSONRESULT resCrmIslemSureleri=service.addCrmIslemSureleri(bildirimId, userId, Sistem, DetayIslemId, Kurum, Kurum1, Kurum2, Sube, Havuz, il, Tar);
								System.out.println("********************* işlem süre ekle end ************************");
							}
							
							
							if(resChangeTaskUser.getSonuc()=="1" && resAddTaskDetay.getSonuc()=="1") {
								res.setSonuc("1");
							}else {
								res.setSonuc("0");
							}
						}else {
							res.setSonuc("0");
						}
					}
			
				}
	
			}else{
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bu İşlem İçin Yetkiniz Yok.");
				return res;				
			}

				return res;				


		}
	

		@RequestMapping(value="/vatandasBilgisiKapat",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT vatandasBilgisiKapat(Integer id,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			
			String vatandasBilgisiKapatYetki=userYetki.get("bildirim_VatandasBilgisiAcKapat").toString();
		
			Integer isOnayBekle=1; // burası 1 ise gorev detayında vatandas bilgisi kapalı kalır	
			
			String ipAdres=Genel.getClientIpAddress(request);
			if(vatandasBilgisiKapatYetki==null){		
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bu İşlem İçin Yetkiniz Yok.");
				return res;
			}
			
			res=service.vatandasBilgisiKapataC(id,isOnayBekle);
			if(res.getSonuc()=="0") {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else{
				service.addTaskDetay(0, id, userId, "Talep üzerine vatandaş bilgileri gizlendi.", ipAdres,100);
			}			
			return res;
		}	

		@RequestMapping(value="/vatandasBilgisiAc",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT vatandasBilgisiAc(Integer id,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			String vatandasBilgisiAcYetki=userYetki.get("bildirim_VatandasBilgisiAcKapat").toString();	
			String ipAdres=Genel.getClientIpAddress(request);
			Integer isOnayBekle=0; // burası sıfır ise gorev detayında vatandas bilgisi acık kalır
			if(vatandasBilgisiAcYetki==null){		
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bu İşlem İçin Yetkiniz Yok.");
				return res;
			}
			
			res=service.vatandasBilgisiKapataC(id,isOnayBekle);
			if(res.getSonuc()=="0") {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}else{
				service.addTaskDetay(0, id, userId, "Talep üzerine vatandaş bilgileri açıldı.", ipAdres,100);
			}		
			return res;
		}			




		@RequestMapping(value="/havuzaYonlendir", method= RequestMethod.POST)
		public ModelAndView havuzaYonlendir(Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
			ModelAndView model = new ModelAndView();
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			// üzerine aldığı bildirimleri yonlendi,rme yetkisi *-******************* bu kısıma yonlendirtum yetkişside eklenecek
			// tüm yonlendir ve stanbdart yonlendir bilgisi kontrol edilip varyasyonu yapılacak
			if(userYetki.get("bildirim_HavuzaYonlendir")!=null){
				// kullanılacak css 
				List<String> css = new ArrayList<String>(); 
				css.add("/assets/vendors/custom/alertify/alertify.css");
				model.addObject("css",css);
				
				// kullanılacak js
				List<String> js=new ArrayList<String>();
				js.add("/assets/saphira/jquery.blockUI.js");
				js.add("/assets/vendors/custom/alertify/alertify.js");
				model.addObject("js",js);
				List<vKURUMLAR> kurumlar=lookupService.getKurumlar(0,null,0,10);
				List<vILLER> iller = lookupService.getIllerByUlke(0,190,null,null); 	
				model.addObject("kurumlar", kurumlar);
				model.addObject("iller", iller);	
				model.addObject("id",id);	
				model.setViewName("BILDIRIM/detayIslemler/havuzaYonlendir");		     
			}else{
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				model.setViewName("BILDIRIM/YetkiYokAjax");
			}
			return model;
		}	

		@RequestMapping(value="/havuzaYonlendirKaydet",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT havuzaYonlendirKaydet(Integer id,Integer tip_level1,Integer fk_il,Integer fk_sube,Integer fk_havuz,String aciklama,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			String ipAdres=Genel.getClientIpAddress(request);
			
			if(userYetki.get("bildirim_HavuzaYonlendir")!=null){	
				//******************* havuz bilgisini al ve kontrol et
				
				List<Map<String, Object>> havuz= ayarlarService.getHavuzlar(fk_havuz.toString(),1,0,0,0,0,"",0,1);
				Integer hid=(Integer) havuz.get(0).get("id"); // alttaki hatalı kısım bu sekilde kullanılacak
				if(hid<1){
					response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
					res.setSonuc("0");
					res.setMesaj("Bildirim Yöneldirme İşlemi Başarısız.<br>Havuz Kimliği Bulunamadı. Sorun  Devam Ederse Yöneticinize Başvurun.");
					return res;
				}
				//*******************bildirimi havuza yonlendir 
				res=service.havuzaYonlendir(id, fk_havuz, userId, aciklama);
				if(res.getSonuc()=="0"){
					response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
					res.setSonuc("0");
					res.setMesaj("Bildirim Yöneldirme İşlemi Başarısız. Sorun  Devam Ederse Yöneticinize Başvurun.");
					return res;
				}
			
				//******************* yukarda cekilen havuz bilgisini taskdetaya acıklamasını ekle
				String sistemAciklama=id+" Numaralı Bildirimi <u>" + havuz.get(0).get("adi") +"</u> havuzuna Yönlendirdi. [Sistem]<br>"+ aciklama;
				res=service.addTaskDetay(2,id, userId, sistemAciklama, ipAdres,100);
				Integer lastDetayIslem=res.getsonTaskDetayIslem(); // geri dönen detayişlem id sini alıyoruz
				//*******************task_DBUPD update verisi eklenecek
				//*******************task_DBUPD vt lerin sonradan acıklama eklediği bildirimleri içerir. 
				//*******************vt bildirim ekleyince tabloya insert eder. daha sonra mu yonlendirme yapınca bu tablodaki isActive=1 olarak güncellenir
				res=service.taskDBUP(id);
				//*******************task_DBDetayHavuz a ekleme yapılıyor
				service.taskHavuzDetayEkle(userId, id, lastDetayIslem, fk_havuz, (Integer)havuz.get(0).get("fk_sube"), (Integer)havuz.get(0).get("fk_il"), (Integer)havuz.get(0).get("fk_konu"));
				//******************* baska kullanıcı ekranına girilip işlem yapıldıysa yapılmadıysa kodlanıp change_user_log ye loglanacak

				//*******************task_DBDetay sistem 5 olan task idye denk gelen kaydı ac
				Map<String,Object> taskDbDetay=service.getTaskDBDetay(100, 0, 0,1,lastDetayIslem); 
				vBILDIRIM bildirim=service.getTask(id,userYetki);
				//*******************crm_islem_sureleri ekleme yapılacak
				int sistem=Integer.parseInt(taskDbDetay.get("sistem").toString());
				int taskDbDetayId=Integer.parseInt(taskDbDetay.get("id").toString());
				
				vJSONRESULT resCrmIslemSureleri=service.addCrmIslemSureleri(
					id, 
					userId, 
					sistem,
					taskDbDetayId, 
					bildirim.getTip_level1(), 
					bildirim.getTip_level2(), 
					bildirim.getTip_level3(), 
					bildirim.getfkSube(), 
					bildirim.getfkHavuz(),
					bildirim.getfkIl(),
					taskDbDetay.get("tarih").toString()
					);	
					
					res.setSonuc("1");
					res.setMesaj("Bildirim Havuza Yönlendirildi.");		
			}else{
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bu İşlem İçin Yetkiniz Yok.");				
			}
			return res;
		}


		@RequestMapping(value="/kullaniciyaYonlendir", method= RequestMethod.POST)
		public ModelAndView kullaniciyaYonlendir(Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
			ModelAndView model = new ModelAndView();
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			// kullanılacak css 
			List<String> css = new ArrayList<String>(); 
			css.add("/assets/vendors/custom/alertify/alertify.css");
			model.addObject("css",css);
			// kullanılacak js
			List<String> js=new ArrayList<String>();
			js.add("/assets/saphira/jquery.blockUI.js");
			js.add("/assets/vendors/custom/alertify/alertify.js");
			model.addObject("js",js);
			vBILDIRIM bildirim= service.getTask(id, userYetki);

			if(bildirim.getId()<1){
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				model.addObject("msj","Bildirim Kimliği Bulunamadı.");
				model.setViewName("BILDIRIM/YetkiYokAjax");
				return model;
			}
			if(userYetki.get("bildirim_KullaniciyaYonlendirAll")!=null){
				List<Lookup> kullanicilar= lookupService.getTaskHavuzKullanici(bildirim.getfkHavuz());
				model.addObject("kullanicilar",kullanicilar);
				model.addObject("id",id);	
				model.setViewName("BILDIRIM/detayIslemler/kullaniciyaYonlendir");
			
			}else if(userYetki.get("bildirim_KullaniciyaYonlendir")!=null){
				List<Lookup> kullanicilar= lookupService.getTaskHavuzKullanici(bildirim.getfkHavuz());
				model.addObject("kullanicilar",kullanicilar);
				model.addObject("id",id);	
				model.setViewName("BILDIRIM/detayIslemler/kullaniciyaYonlendir");
	
			}else{
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				model.setViewName("BILDIRIM/YetkiYokAjax");
			}

			return model;
		}	



		@RequestMapping(value="/kullaniciyaYonlendirKaydet",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT kullaniciyaYonlendirKaydet(Integer id,Integer fk_user,String aciklama,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			String ipAdres=Genel.getClientIpAddress(request);
			vBILDIRIM bildirim=service.getTask(id, userYetki);
			Integer il=bildirim.getfkIl();
			Integer havuz=bildirim.getfkHavuz();
			Integer sube=bildirim.getfkSube();
			Integer kurum=bildirim.getTip_level1();
			Integer kurum1=bildirim.getTip_level2();
			Integer kurum2=bildirim.getTip_level3();
			String  havuzAdi=bildirim.getHavuzAdi();
			Integer yonlendirYetki=0;
			Integer fkGorevStatu=1;
			
			// bildirimi kullanıcıya yonlendir
			vJSONRESULT yonlendir=service.changeTaskUser(id, fkGorevStatu, fk_user);
			if(yonlendir.getSonuc()=="0"){
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bildirim Yöneldirme İşlemi Başarısız. Sorun  Devam Ederse Yöneticinize Başvurun.");
				return res;
			}


			// yonlendirme yapılan kullanıcı bilgilerini al
			vKULLANICILAR userr=ayarlarService.getUserByID(fk_user);
			String detayAciklama =id + " Numaralı Bildirimi "+ userr.getAdi()+ " İsimli Kullanıcıya Yönlendirdi. [Sistem]<br>"+aciklama;
			//taskdbdetay bilgilerini ekle
			
			vJSONRESULT detay=service.addTaskDetay(6, id, userId,detayAciklama, ipAdres,100);
			Integer lastDetayIslem=detay.getsonTaskDetayIslem();

			//*******************task_DBDetay sistem 5 yada 6 olan task idye denk gelen kaydı ac
			//yukarda taskdbdetaya eklenen son işlemin sistem değeri zaten 6 olduğu için tekrar detay tablosuna sorgu gondermiyoruz
			Map<String,Object> taskDbDetay=service.getTaskDBDetay(100, 0, 0,1,lastDetayIslem); 
			//*******************crm_islem_sureleri ekleme yapılacak
			int sistem=Integer.parseInt(taskDbDetay.get("sistem").toString());
			int taskDbDetayId=Integer.parseInt(taskDbDetay.get("id").toString());
			// crm işlemsüreleri için ekleme yap
			vJSONRESULT resCrmIslemSureleri=service.addCrmIslemSureleri(
				id, 
				userId, 
				sistem,
				taskDbDetayId, 
				bildirim.getTip_level1(), 
				bildirim.getTip_level2(), 
				bildirim.getTip_level3(), 
				bildirim.getfkSube(), 
				bildirim.getfkHavuz(),
				bildirim.getfkIl(),
				taskDbDetay.get("tarih").toString()
				);	

				res.setSonuc("1");
				res.setMesaj("Bildirim Havuza Yönlendirildi.");					
		
			return res;
		}

		@RequestMapping(value="/getTaskHavuzKullanicilar", method=RequestMethod.POST)
		@ResponseBody
		public ModelAndView subeler(@Valid Integer fk_havuz) {
			ModelAndView model = new ModelAndView("lookup");
				List<Lookup> l = lookupService.getTaskHavuzKullanici(fk_havuz);
				model.addObject("l", l);
			return model;
		}

		@RequestMapping(value="/getSubeler", method=RequestMethod.POST)
		@ResponseBody
		public ModelAndView subeler(@Valid Integer tip_level1,@Valid Integer fk_il) {
				ModelAndView model = new ModelAndView("lookup");
				
				List<Lookup> l = lookupService.getSubeler(0,tip_level1,null,fk_il,null, 2);
				model.addObject("l", l);
			return model;
		}
		@RequestMapping(value="/getHavuzlar", method=RequestMethod.POST)
		@ResponseBody
		public ModelAndView havuzlar(@Valid Integer fk_sube) {
				ModelAndView model = new ModelAndView("lookup");
				List<Lookup> l = lookupService.cmbgetHavuzlarSube(fk_sube);
				model.addObject("l", l);
			return model;
		}

		@RequestMapping(value="/aciklamaEkle", method= RequestMethod.POST)
		public ModelAndView aciklamaEkle(Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
			ModelAndView model = new ModelAndView("BILDIRIM/detayIslemler/aciklamaEkle");
			model.addObject("id",id);
			return model;
		}	
		
		
		@RequestMapping(value="/aciklamaKaydet",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT aciklamaKaydet(Integer id,String aciklama,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			
			String aciklamaEkleYetki=userYetki.get("bildirim_AciklamaEkle").toString();	
			String ipAdres=Genel.getClientIpAddress(request);
			
			if(aciklamaEkleYetki!=null){	
				res=service.addTaskDetay(1, id, userId, aciklama, ipAdres,100);	
			}else{
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bu İşlem İçin Yetkiniz Yok.");				
			}
			
			return res;
		}	
		
		@RequestMapping(value="/guncelle", method= RequestMethod.POST)
		public ModelAndView guncelle(Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
			ModelAndView model = new ModelAndView();
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);

			if(userYetki.get("bildirim_DurumGuncelle")!=null){
				model.setViewName("BILDIRIM/detayIslemler/guncelle");
				List<Lookup> statu = lookupService.getTaskStatu(0);
				model.addObject("statu",statu);
				model.addObject("id",id);
			}else{
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				model.addObject("msj"," BU SAYFAYI GÖRME YETKİNİZ BULUNMAMAKTADIR.<BR>YETKİ İŞLEMLERİ İÇİN YÖNETİCİNİZE BAŞVURUNUZ.");
				model.setViewName("BILDIRIM/YetkiYokAjaxModal");
			}

			return model;
		}	

		@RequestMapping(value="/guncelleKaydet",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT guncelleKaydet(Integer id,String aciklama,Integer fk_gorev_statu,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			vJSONRESULT  resChangeTaskStatu =new vJSONRESULT();
	
			String durumGuncelleYetki=userYetki.get("bildirim_DurumGuncelle").toString();	
			String ipAdres=Genel.getClientIpAddress(request);
			
			if(durumGuncelleYetki!=null){
				if(fk_gorev_statu==99){
				res=kapatKaydet(id, aciklama, 1, request, response, session);
				if(res.getSonuc()!="1"){
					response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
					res.setSonuc("0");
					res.setMesaj("İşlem Sırasında Sorun Oluştu.");
				}
				}else{
					resChangeTaskStatu=service.taskChangeStatu(id,fk_gorev_statu);
					if(resChangeTaskStatu.getSonuc()=="1"){
						List<Lookup> status = lookupService.getTaskStatu(fk_gorev_statu);
						String detayAciklama =id + " Numaralı Bildirimi <strong>"+ status.get(0).getAdi()+ "</strong>  Olarak Güncelledi. [Sistem]<br>"+aciklama;
						res=service.addTaskDetay(0, id, userId, detayAciklama, ipAdres,fk_gorev_statu);	
						Integer lastDetayId=res.getsonTaskDetayIslem();
						vBILDIRIM task=service.getTask(id, userYetki);
						Integer il=task.getfkIl();
						Integer havuz=task.getfkHavuz();
						Integer sube=task.getfkSube();
						Integer kurum=task.getTip_level1();
						Integer kurum1=task.getTip_level2();
						Integer kurum2=task.getTip_level3();
						String  havuzAdi=task.getHavuzAdi();
						vTASKHAVUZKULLANICI taskHavuz=service.getTaskHavuzKullanici(userId, 10,"", task.getfkHavuz(), 0, 0, 0);
						res=service.taskHavuzDetayEkle(userId, id, lastDetayId, task.getfkHavuz(), taskHavuz.getfk_sube(), taskHavuz.getfk_il(), taskHavuz.getfk_konu());
					}else{
						response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
						res.setSonuc("0");
						res.setMesaj("İşlem Sırasında Sorun Oluştu.");	
					}
				}
			}else{
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bu İşlem İçin Yetkiniz Yok.");				
			}
			
			return res;
		}	

		@RequestMapping(value="/kapat", method= RequestMethod.POST)
		public ModelAndView kapat(Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			ModelAndView model = new ModelAndView();
			if(userYetki.get("bildirim_BildirimKapat")!=null){
				model.setViewName("BILDIRIM/detayIslemler/kapat");
				List<Lookup> statu = lookupService.getTaskbKapanmaStatu(0);
				model.addObject("statu",statu);
				model.addObject("id",id);
			}else{
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				model.addObject("msj"," BU SAYFAYI GÖRME YETKİNİZ BULUNMAMAKTADIR.<BR>YETKİ İŞLEMLERİ İÇİN YÖNETİCİNİZE BAŞVURUNUZ.");
				model.setViewName("BILDIRIM/YetkiYokAjaxModal");
			}
			return model;
		}


		void sendEmail(String adi, String tarih, Integer id) {
			SimpleMailMessage msg = new SimpleMailMessage();
			String mailMessage="";
			mailMessage="<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>";
			mailMessage +="Sayin " + adi + "<br>Aile, Çalışma Ve Sosyal Hizmetler İletişim Merkezi ALO 170 e  " + tarih+ " tarihli yapmış olduğunuz başvuru sonuçlanmıştır.<br><br>";
			mailMessage +="<strong>BAŞVURU TAKİP NUMARASI : " + id + "</strong> <br><br> Başvuru sonucunuzun detayı için ÇSGİM ALO 170'i  aramanız gerekmektedir. Teşekkür ederiz";
			msg.setTo("hakanaslantas@hotmail.com");
			msg.setSubject("Alo 170 Başvuru Bildirimi");
			msg.setText(mailMessage);
			javaMailSender.send(msg);
		}
		@RequestMapping(value="/kapatKaydet",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT kapatKaydet(Integer id,String aciklama,Integer fk_taskb_kapanma_statu,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			vJSONRESULT  resKapat =new vJSONRESULT();
			String kapatYetki=userYetki.get("bildirim_BildirimKapat").toString();	
			String ipAdres=Genel.getClientIpAddress(request);
			String kurumYetki=session.getAttribute("User_ROL").toString();
			vBILDIRIM task=service.getTask(id, userYetki);
			Integer Sistem=3;

			if(kapatYetki!=null){	
				resKapat=service.bildirimKapat(id, userId, fk_taskb_kapanma_statu, kurumYetki, aciklama, userYetki);
				System.out.println("bildirimKapat***********************************************************");
				if(resKapat.getSonuc()=="1"){
					res=service.addTaskDetay(Sistem, id, userId, aciklama, ipAdres,100);
					System.out.println("addTaskDetay***********************************************************");
					Integer lastDetayId=res.getsonTaskDetayIslem();
					vTASKDETAYISLEM lastDetayIslem=new vTASKDETAYISLEM();
					lastDetayIslem=service.openDetayIslem(lastDetayId,0,100);
					System.out.println("openDetayIslem***********************************************************");
					String lastDetayIslemTar=lastDetayIslem.getTarih();
					
					if(kurumYetki=="MU"){
						res=service.taskDBUP(id);
					}
					vTASKHAVUZKULLANICI taskHavuz=service.getTaskHavuzKullanici(userId, 10,"", task.getfkHavuz(), 0, 0, 0);
					System.out.println("getTaskHavuzKullanici***********************************************************");
					res=service.taskHavuzDetayEkle(userId, id, lastDetayId, task.getfkHavuz(), taskHavuz.getfk_sube(), taskHavuz.getfk_il(), taskHavuz.getfk_konu());
					System.out.println("taskHavuzDetayEkle***********************************************************");
					res=service.addCrmIslemSureleri(id, userId, Sistem, lastDetayId, task.getTip_level1(), task.getTip_level2(), task.getTip_level3(), task.getfkSube(), task.getfkHavuz(), task.getfkIl(),lastDetayIslemTar);
					System.out.println("addCrmIslemSureleri***********************************************************");
				
					//sms işlemi
					vKART kart= service.getKart(task.getFk_kart(), userYetki);
					String smsMesaj="";
					if(kart.getGsm().length()>7){
						smsMesaj=id+" bildirim numarali basvurunuz sonuclandirilmistir. Aile, Calisma ve Sosyal Hizmetler Iletisim Merkezi ALO 170 i arayarak sonucunuzun detayini ogrenebilirsiniz. Tesekkur ederiz.";
						service.addSmsListe(id, kart.getGsm(), smsMesaj);
					}
			
					// email localde gonderim yapmadı
					//sendEmail(kart.getKartadi()+kart.getKartSoyadi(), task.getTarih(), id); 
				}

			
			}else{
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bu İşlem İçin Yetkiniz Yok.");				
			}
			return res;
		}

		@RequestMapping(value="/yenidenAc", method= RequestMethod.POST)
		public ModelAndView yenidenAc(Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			ModelAndView model = new ModelAndView();
			if(userYetki.get("bildirim_YenidenAc")!=null){
				model.setViewName("BILDIRIM/detayIslemler/yenidenAc");
				model.addObject("id",id);

			}else{
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				model.addObject("msj"," BU SAYFAYI GÖRME YETKİNİZ BULUNMAMAKTADIR.<BR>YETKİ İŞLEMLERİ İÇİN YÖNETİCİNİZE BAŞVURUNUZ.");
				model.setViewName("BILDIRIM/YetkiYokAjaxModal");
			}

			return model;
		}

		@RequestMapping(value="/yenidenAcKaydet",method=RequestMethod.POST)
		public @ResponseBody vJSONRESULT yenidenAcKaydet(Integer id,String aciklama,Integer fk_gorev_statu,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
			Integer userId=(Integer) session.getAttribute("User_ID");
			
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
			vJSONRESULT  res =new vJSONRESULT();
			vJSONRESULT  resDetay =new vJSONRESULT();
			String yedidenAcYetki=userYetki.get("bildirim_YenidenAc").toString();	
			String ipAdres=Genel.getClientIpAddress(request);
			Integer userLokasyon=Integer.parseInt(session.getAttribute("User_fkLokasyon").toString());
			if(userLokasyon==0){
				userLokasyon=1;
			}
			
			aciklama=aciklama.replaceAll("::",":");//replaces all occurrences of "a" to "e"  
			
			if(yedidenAcYetki!=null){	
				res=service.yenidenAc(id, userLokasyon, userYetki);
				String resYenidenAc=res.getSonuc();
				if(resYenidenAc=="1"){
					resDetay=service.addTaskDetay(4, id, userId, aciklama, ipAdres,100);	
				}else{
					response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
					res.setSonuc("0");
					res.setMesaj("Yeniden Açma İşlemi Sırasında Bir Sorun Oluştu. Lütfen Tekrar Deneyiniz.");
					return res;	
				}
				
			}else{
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				res.setMesaj("Bu İşlem İçin Yetkiniz Yok.");				
			}
			return res;
		}
}


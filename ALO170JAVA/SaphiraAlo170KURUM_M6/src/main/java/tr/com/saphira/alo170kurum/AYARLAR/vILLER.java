package tr.com.saphira.alo170kurum.AYARLAR;

public class vILLER {

	private Integer id;
	private String adi;
	private Integer fk_ulke;
	private Integer isActive;

	public vILLER() {
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public Integer getFk_ulke() {
		return fk_ulke;
	}
	public void setFk_ulke(Integer fk_ulke) {
		this.fk_ulke = fk_ulke;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
}

package tr.com.saphira.alo170kurum.LOGIN;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tr.com.saphira.alo170kurum.LOOKUP.LOOKUPService;
import tr.com.saphira.alo170kurum.model.SaphiraUsers;
import tr.com.saphira.alo170kurum.AYARLAR.AyarlarService;

@EnableWebSecurity
@Controller
public class MainController {
	

	
	 @SuppressWarnings("unused")
	@Autowired
	 private LOOKUPService userService;
	 
	 @Autowired
	 private UserService loginService;
	 @Autowired 
	 private AyarlarService ayarlarService;

	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
	public ModelAndView defaultPage(HttpSession httpSession) {

		ModelAndView model = new ModelAndView();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			//System.out.println("**********auth instanceof if");
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
		
			//mUserDetails userDetail = (mUserDetails) auth.getPrincipal();
			if (httpSession.getAttribute("User_ID")==null || httpSession.getAttribute("User_ID")=="") {
			//	System.out.println("**********User_ID if"+userDetail.getUsername());
				SaphiraUsers u = loginService.getLogin(userDetail.getUsername());
				httpSession.setAttribute("User_ID", u.getId());
				httpSession.setAttribute("User_NAME", u.getAdi());
				httpSession.setAttribute("User_isAgent", u.getIsAgent());
				httpSession.setAttribute("User_isAdmin", u.getIsAdmin());
				httpSession.setAttribute("User_ROL", u.getRol());
				httpSession.setAttribute("User_fkROL", u.getFk_rol());
				httpSession.setAttribute("User_LOKASYON", u.getLokasyon());
				httpSession.setAttribute("User_fkLokasyon", u.getFk_lokasyon());
				httpSession.setAttribute("ext",u.getExt());
				httpSession.setAttribute("yetki",u.getyetkiJson());
				httpSession.setAttribute("User_isSysAdmin",u.getSysAdmin());
				httpSession.setAttribute("SVUserID","");
				httpSession.setAttribute("User_Level",u.getRolLevel());

			} else {
				//System.out.println("**********User_ID else");
			}
			ayarlarService.getUserYetkiBildirimModelView(model, httpSession);
			Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(httpSession);
			model.setViewName("home");
		}  else {
			model.setViewName("LOGIN/login");	
		}
		return model;

	}
	
	
	@RequestMapping(value = "/ttl", method = RequestMethod.GET)
	@ResponseBody
	public String ttl(HttpSession httpSession) {
		httpSession.setAttribute("ttl", LocalDate.now() );
		return "ok";
	}

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Login Form - Database Authentication");
		model.addObject("message", "This page is for ROLE_ADMIN only!");
		model.setViewName("admin");

		return model;

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
							  @RequestParam(value = "logout", required = false) String logout) {
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("LOGIN/login");	

		return model;

	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(@RequestParam(value = "error", required = false) String error,
							  @RequestParam(value = "logout", required = false) String logout, HttpSession httpSession) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		
		httpSession.setAttribute("UserID", "");
		httpSession.setAttribute("SysID", "");
		httpSession.setAttribute("UserADI", "");
		httpSession.setAttribute("isADMIN", "");
		httpSession.setAttribute("User_ID","");
		httpSession.setAttribute("User_NAME","");
		httpSession.setAttribute("User_isAgent","");
		httpSession.setAttribute("User_isAdmin","");
		httpSession.setAttribute("User_ROL","");
		httpSession.setAttribute("User_fkROL","");
		httpSession.setAttribute("User_LOKASYON","");
		httpSession.setAttribute("User_fkLokasyon","");
		httpSession.setAttribute("ext","");
		httpSession.setAttribute("yetki","");
		SecurityContextHolder.clearContext();
		// auth = null;
		model.setViewName("LOGIN/login");	

		return model;

	}

	
	
	
	
	 @ResponseBody
	 @RequestMapping(method = RequestMethod.POST, value = "/uploadFile")
	 public String handleFileUpload(@RequestParam("file") MultipartFile file,
	                                RedirectAttributes redirectAttributes) {

		 String ROOT = "/ABORA/JAVA/w/";
		 
	     if (!file.isEmpty()) { 
	         try { 
	        	 String fdir = Paths.get(ROOT, file.getOriginalFilename()).toString();
	             // Java NIO to copy the input stream to a local file
	             Files.copy(file.getInputStream(), Paths.get(ROOT, file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
	             redirectAttributes.addFlashAttribute("message",
	                     "You successfully uploaded " + file.getOriginalFilename() + "!");
	             
	             
	             
	             
	             try {

	                 Workbook workbook =   WorkbookFactory.create(new File(fdir));
	                 
	                 System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
	                 
	                 Sheet sheet = workbook.getSheetAt(0);

	                 // 
	                 DataFormatter dataFormatter = new DataFormatter();
	                 System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
	                 Iterator<Row> rowIterator = sheet.rowIterator();
	                 while (rowIterator.hasNext()) {
	                     Row row = rowIterator.next();

	                     // Now let's iterate over the columns of the current row
	                     //Iterator<Cell> cellIterator = row.cellIterator();

                    	 Cell cell = row.getCell(0);
    	                         String cellValue = dataFormatter.formatCellValue(cell);
    	                         System.out.print(cellValue + "\t");
    	                         
    	                         
	                     System.out.println();
	                 }
	                 
	                 
	                 
	             } catch (FileNotFoundException e) {
	            	 System.out.println(e.getMessage().toString());
	                 e.printStackTrace();
	             } catch (IOException e) {
	            	 System.out.println(e.getMessage().toString());
	                 e.printStackTrace();
	             }catch (InvalidFormatException e) {
	            	 e.printStackTrace();
	             }	             
	             
	             
	         } catch (IOException|RuntimeException e) {
	        	 System.out.println(e.getMessage().toString());
	             redirectAttributes.addFlashAttribute("message", "Failued to upload " + file.getOriginalFilename() + " => " + e.getMessage());
	         }
	     } else {
	         redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " because it was empty");
	     }

	     return "redirect:/";
	 }
	 
	
	 @ResponseBody
	 @RequestMapping(method = RequestMethod.GET, value = "/tes")
		public ModelAndView test() {

			ModelAndView model = new ModelAndView();
			model.setViewName("TEST/upload");
			return model;
		 
	 }
	 

}
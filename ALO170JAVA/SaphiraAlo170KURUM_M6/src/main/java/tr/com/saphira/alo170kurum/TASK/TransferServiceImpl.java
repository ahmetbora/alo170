package tr.com.saphira.alo170kurum.TASK;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.saphira.alo170kurum.BILDIRIM.vADET;

@Service
public class TransferServiceImpl implements TransferService {

	@Autowired
	TransferDao dao;
	
	public void setDao(TransferDao dao) {
		this.dao = dao;
	}

	@Override
	public TRANSFER getTASKDB(Integer ID) {
		
		return dao.getTASKDB(ID);
	}

	@Override
	public void setBILDIRIM(TRANSFER t) {
		this.dao.setBILDIRIM(t);
		
	}

	@Override
	public List<TRANSFERDETAY> getTASKDBDETAY(Integer ID) {

		return dao.getTASKDBDETAY(ID);
	}

	@Override
	public void setBILDIRIMDETAY(TRANSFERDETAY td) {
		this.dao.setBILDIRIMDETAY(td);
		
	}

	@Override
	public List<vADET> getUpdateList() {
		
		return dao.getUpdateList();
	}

	@Override
	public KART getVATANDAS(Integer ID) {

		return dao.getVATANDAS(ID);
	}

	@Override
	public void setVATANDAS(KART k) {
		this.dao.setVATANDAS(k);
		
	}

	@Override
	public vADET adetBildirim() {
		
		return dao.adetBildirim();
	}

	@Override
	public vADET adetBildirimDetay() {
		
		return dao.adetBildirimDetay();
	}

	@Override
	public vADET adetVatandas() {
		
		return dao.adetVatandas();
	}

	@Override
	public List<vADET> getUpdateKartList() {
		
		return dao.getUpdateKartList();
	}

}

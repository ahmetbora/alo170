package tr.com.saphira.alo170kurum.TASK;

import java.util.List;

import tr.com.saphira.alo170kurum.BILDIRIM.vADET;

public interface TransferDao {
	
	public TRANSFER getTASKDB(Integer ID);
	
	public void setBILDIRIM(TRANSFER t);
	
	public List<TRANSFERDETAY> getTASKDBDETAY(Integer ID);
	
	public void setBILDIRIMDETAY(TRANSFERDETAY td);
	
	public List<vADET> getUpdateList();
	
	public List<vADET> getUpdateKartList();
	
	public KART getVATANDAS(Integer ID);
	
	public void setVATANDAS(KART k);
	
	
	// Count
	
	public vADET adetBildirim();
	public vADET adetBildirimDetay();
	public vADET adetVatandas();

}

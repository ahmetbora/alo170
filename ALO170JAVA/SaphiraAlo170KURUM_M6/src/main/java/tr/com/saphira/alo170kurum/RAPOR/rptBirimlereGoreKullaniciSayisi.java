package tr.com.saphira.alo170kurum.RAPOR;

public class rptBirimlereGoreKullaniciSayisi {
    private Integer id;
    private String kurum;
    private String aktif;
    private String pasif;



    public rptBirimlereGoreKullaniciSayisi() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}


    public String getkurum(){
        return kurum;
    }
    public void setkurum(String kurum){
        this.kurum=kurum;
    }

    public String getaktif(){
        return aktif;
    }
    public void setaktif(String aktif){
        this.aktif=aktif;
    }

    public String getpasif(){
        return pasif;
    }
    public void setpasif(String pasif){
        this.pasif=pasif;
    }


}


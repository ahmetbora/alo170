package tr.com.saphira.alo170kurum.AYARLAR;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.jasper.tagplugins.jstl.core.Catch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.PasswordGenerator;
import tr.com.saphira.alo170kurum.BILDIRIM.vJSONRESULT;
import tr.com.saphira.alo170kurum.LOOKUP.LOOKUPService;

import tr.com.saphira.alo170kurum.model.Lookup;
import tr.com.saphira.alo170kurum.model.vTotal;



@Controller
@EnableWebSecurity
@RequestMapping(value="/ayarlar")
@SuppressWarnings("unused")
public class AyarlarController {
	
	@Autowired
	AyarlarService service;

	@Autowired
	LOOKUPService lookupService;
	
	@Autowired 
	private HttpServletRequest request;

	private String DnameCrm="SMDR_1002";
	private String DnamePbx="pbx";

	
	private void checkSessionRedirect(HttpSession session, HttpServletResponse response) {
		try {
			if(session.getAttribute("User_ID")==null || session.getAttribute("User_ID")=="") {
				try {
					response.sendRedirect("/login");
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
		}catch(Exception ex) {
			try {
				response.sendRedirect("/login");
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}		
	}
	
	
	
	@RequestMapping(value="/tanimlar", method= RequestMethod.GET)
	public ModelAndView tanimliste(HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/Tanimlar");
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<vKURUMLAR> kurumlar = service.getKurumlar(0,null,0,0,userYetki);//kontrol et
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);

		model.addObject("kurumlar", kurumlar);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		List<Lookup> lokasyonlar=lookupService.getLokasyon();
		model.addObject("lokasyonlar",lokasyonlar);
		return model;
	}
	
	@RequestMapping(value="/tanimlarKurum", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarKurumData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<vKURUMLAR> kurumlar = service.getKurumlar(0,null,skip,take,userYetki);
		List<vTotal> say = service.countGetKurumlar(0,null);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",kurumlar); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}	
	@RequestMapping(value="/tanimlarKurum/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarKurumEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/KurumlarEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<vKURUMLAR> kurumlar = service.getKurumlar(id,null,0,1,userYetki);//kontrol et
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);
		model.addObject("kurumlar", kurumlar.get(0));
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/tanimlarKurum/Add", method= RequestMethod.POST)
	public ModelAndView tanimlarKurumAdd(HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/KurumlarEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);
	
		model.addObject("islem","Add");
		model.addObject("aktifpasiflist", aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
		
	@RequestMapping(value="/tanimlarAltKurum", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarAltKurumData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_parent=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_parent=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_parent=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
	
		List<vALTKURUMLAR> altKurumlar = service.getAltKurumlar(0,"",fk_parent,skip,take,yetki);
		List<vTotal> say = service.countGetAltKurumlar(0,"",fk_parent);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",altKurumlar); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarAltKurum/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarAltKurumEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/AltKurumlarEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<vKURUMLAR> kurumlar = service.getKurumlar(0,null,0,100,userYetki);
		List<vALTKURUMLAR> altKurumlar = service.getAltKurumlar(id,"",0,0,1,userYetki);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);

		model.addObject("kurumlar", kurumlar);
		model.addObject("altKurumlar", altKurumlar.get(0));
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}


	@RequestMapping(value="/tanimlarAramaKonusu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarAramaKonusuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
	
		List<vARAMAKONUSU> list = service.getAramaKonusu(0,"",fk_konu,skip,take);
		List<vTotal> say = service.countGetAramaKonusu(0,"",fk_konu);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	
	@RequestMapping(value="/tanimlarAramaKonusu/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarAramaKonusuEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/AramaKonusuEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<vKURUMLAR> kurumlar = service.getKurumlar(0,null,0,100,userYetki);
		List<vALTKURUMLAR> altKurumlar = service.getAltKurumlar(0,"",0,0,100,userYetki);
		List<vARAMAKONUSU> aramaKonusu = service.getAramaKonusu(id,"",0,0,1);
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);

		model.addObject("kurumlar", kurumlar);
		model.addObject("altKurumlar", altKurumlar);
		model.addObject("aramakonusu", aramaKonusu.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}



	
	@RequestMapping(value="/tanimlarSube", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarSubeData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
	
		List<Map<String, Object>> list = service.getSubeler(0,"",fk_konu,0,skip,take);
		List<vTotal> say = service.countGetSubeler(0,"",fk_konu);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarSube/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarSubeEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarSubeEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<vKURUMLAR> kurumlar = service.getKurumlar(0,null,0,100,userYetki);
		List<Map<String,Object>> iller = service.getIller("","",1,0,2,0,1000);
		List<Map<String,Object>> sube = service.getSubeler(id,"",0,0,0,1);
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);

		model.addObject("kurumlar", kurumlar);
		model.addObject("iller", iller);
		model.addObject("sube", sube.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}


	@RequestMapping(value="/tanimlarHavuz", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarHavuzData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getHavuzlar("",2,0,0,fk_konu,2,"",skip,take);
		List<vTotal> say = service.countGetHavuzlar("",2,0,0,fk_konu,2,"");
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarHavuz/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarHavuzEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarHavuzEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> havuz = service.getHavuzlar(Integer.toString(id),2,0,0,0,0,"",0,1);
		List<vKURUMLAR> kurumlar = service.getKurumlar(0,null,0,100,userYetki);
		List<Map<String,Object>> iller = service.getIller("","",1,0,2,0,1000);
		Integer fk_il=Integer.parseInt(havuz.get(0).get("fk_il").toString());
		
		List<Map<String,Object>> subeler = service.getSubeler(0,"",0,fk_il,0,1000);
		
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);

		model.addObject("kurumlar", kurumlar);
		model.addObject("iller", iller);
		model.addObject("subeler",subeler);
		model.addObject("havuz", havuz.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}


	@RequestMapping(value="/tanimlarAramaKanali", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarAramaKanaliData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getAramaKanali("","",2,0,2,skip,take);
		List<vTotal> say = service.countGetAramaKanali("","",2,fk_konu,2);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarAramaKanali/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarAramaKanaliEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarAramaKanaliEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		String ids=id.toString();
		List<Map<String,Object>> aramaKanali = service.getAramaKanali(ids,"",2,0,0,0,1);
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);

		model.addObject("aramaKanali", aramaKanali.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}


	@RequestMapping(value="/tanimlarEgitimDurumu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarEgitimDurumuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getEgitimDurumlari("","",2,2,skip,take);
		List<vTotal> say = service.countGetEgitimDurumlari("","",2,2);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarEgitimDurumu/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarEgitimDurumuEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarEgitimDurumuEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		String ids=id.toString();
		List<Map<String,Object>> egitimDurumu = service.getEgitimDurumlari(ids, "",2,0,0,1);
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);

		model.addObject("egitimDurumu", egitimDurumu.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}


	@RequestMapping(value="/tanimlarGuvenlikSorusu", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarGuvenlikSorusuData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getGuvenlikSorulari("","",2,2,skip,take);
		List<vTotal> say = service.countGetGuvenlikSorulari("","",1,2);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarGuvenlikSorusu/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarGuvenlikSorusuEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarGuvenlikSorusuEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		String ids=id.toString();
		List<Map<String,Object>> guvenlikSorusu = service.getGuvenlikSorulari(ids,"",2,0,0,1);
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);

		model.addObject("guvenlikSorusu", guvenlikSorusu.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}


	@RequestMapping(value="/tanimlarIller", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarIllerData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getIller("","",2,0,2,skip,take);
		List<vTotal> say = service.countGetIller("","",2,0,2);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarIller/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarIllerEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarIllerEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> il = service.getIller(id.toString(), "",2,0,0, 0,1);
		List<Map<String,Object>> ulkeler = service.getUlkeler("","",1,2,0,1000);	
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);


		model.addObject("ulkeler",ulkeler);
		model.addObject("il", il.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}


	@RequestMapping(value="/tanimlarIlceler", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarIlcelerData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getIlceler("","",2,0,2,skip,take);
		List<vTotal> say = service.countGetIlceler("","",1,0,2);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarIlceler/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarIlcelerEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarIlcelerEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> iller = service.getIller("", "",2,0,0, 0,1000);
		List<Map<String,Object>> ilce = service.getIlceler(id.toString(), "",2,0,0, 0,1);
		List<Map<String,Object>> ulkeler = service.getUlkeler("","",1,2,0,1000);	
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);


		List<Map<String,Object>> parentIl = service.getIller(ilce.get(0).get("fk_il").toString(), "",2,0,0, 0,1);
		List<Map<String,Object>> parentUlke = service.getUlkeler(parentIl.get(0).get("fk_ulke").toString(), "",2,0,0,1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);


		model.addObject("ulkeler",ulkeler);
		model.addObject("iller",iller);
		model.addObject("ilce", ilce.get(0));
		model.addObject("parentIl", parentIl.get(0));
		model.addObject("parentUlke", parentUlke.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}

	
	@RequestMapping(value="/tanimlarUlkeler", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarUlkelerData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_konu=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_konu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_konu=0;
		}		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getUlkeler("","",2,2,skip,take);
		List<vTotal> say = service.countGetUlkeler("","",1,2);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}

	@RequestMapping(value="/tanimlarUlkeler/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarUlkelerEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarUlkelerEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> ulke = service.getUlkeler(id.toString(),"",2,0,0,1);	
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);

		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);


		model.addObject("ulke",ulke.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}

	@RequestMapping(value="/tanimlarDahili", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarDahiliData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_lokasyon=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_lokasyon=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_lokasyon=0;
		}		
		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getDahili(0,"",2,fk_lokasyon,skip,take);
		List<vTotal> say = service.countGetDahili("",2,fk_lokasyon);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarDahili/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarDahiliEdit(String ext,Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarDahiliEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> dahili = service.getDahili(id,ext,0,2,0,1);	
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);


		model.addObject("dahili",dahili.get(0));
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}


	@RequestMapping(value="/tanimlarKkoBaslik", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarKkoBaslikData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_lokasyon=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_lokasyon=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_lokasyon=0;
		}		
		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getKkoBaslik(0,2,skip,take);
		List<vTotal> say = service.countGetKkoBaslik(0,2);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarKkoBaslik/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarKkoBaslikEdit(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarKkoBaslikEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> kko = service.getKkoBaslik(id, 2, 0,1);	
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);


		model.addObject("kko",kko.get(0));
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}

	@RequestMapping(value="/tanimlarKkoSoru", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarKkoSoruData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_lokasyon=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_lokasyon=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_lokasyon=0;
		}		
		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getKkoSoru(0,2,0,skip,take);
		List<vTotal> say = service.countGetKkoSoru(0,2,0);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarKkoSoru/Edit", method= RequestMethod.POST)
	public ModelAndView tanimlarKkoSorueDİT(Integer id,HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Tanimlar/tanimlarKkoSoruEdit");
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> kko = service.getKkoSoru(id, 2,0,0,1);	
		List<Map<String,Object>> kkoparent = service.getKkoBaslik(0,1,0,500);
		
		List<Map<String,Object>> aktifpasiflist = service.getAktifPasifList(1);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		js.add("/assets/saphira/lstTanimlar.js");
		model.addObject("js",js);


		model.addObject("kko",kko.get(0));
		model.addObject("kkoparent",kkoparent);
		model.addObject("aktifpasiflist",aktifpasiflist);
		service.getUserYetkiBildirimModelView(model, session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		return model;
	}

	@RequestMapping(value="/tanimlarPeriyodik", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object>  tanimlarPeriyodikData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
		Integer fk_lokasyon=0;
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
			fk_lokasyon=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
						.getAsJsonObject().get("value")
						.getAsInt();			
		} catch (Exception e) {
			fk_lokasyon=0;
		}		
		
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		
		List<Map<String,Object>> list = service.getPeriyodik(0,skip,take);
		List<vTotal> say = service.countGetPeriyodik(0);
		Map<String,Object> maps=new HashMap<>(); 
		maps.put("data",list); 
		maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/tanimlarPeriyodik/Del",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT tanimlarPeriyodikDel(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		vJSONRESULT  res =new vJSONRESULT();
		Integer id=Integer.parseInt(request.getParameter("id"));
		res=service.delPeriyodik(id);
		if(res.getSonuc()=="1"){
			res.setMesaj("Tanım GÜncellendi.");
			res.setSonuc("1");
		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			res.setSonuc("0");
		}
		return res;
	}





	@RequestMapping(value="/tnmLokasyonlar", method= RequestMethod.POST)
	public @ResponseBody List<Map<String,Object>>  tnmLokasyonlarData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Map<String,Object> yetki=service.getUserYetkiFromJSON(session);
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<Map<String,Object>> list = service.getLokasyonlar();
		return list;
	}




	@RequestMapping(value="/kullanici/liste", method= RequestMethod.GET)
	public ModelAndView KullaniciListe(HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Kullanicilar/KullaniciListe");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		service.getUserYetkiBildirimModelView(model, session);
		Integer sysAdmin=(Integer)session.getAttribute("User_isAdmin");
		Integer userId=(Integer)session.getAttribute("User_ID");
		if(sysAdmin==null){
			sysAdmin=0;
		}
		
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
		Integer rolLevel=(Integer)session.getAttribute("User_Level");
		List<Lookup> roller=null;
		if(sysAdmin>0){
			roller = lookupService.getRoller(0,sysAdmin,1,rolLevel,0);
		}else{
			roller = lookupService.getRollerUser(userId);
		}
		model.addObject("roller", roller);
		
		
		return model;
	}
	//@SuppressWarnings("unchecked")
	@RequestMapping(value="/kullanici/listeData", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> KullaniciListeData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();
	    String adi="";
	    String email="";
	    String fkRol="0";
	    Integer isActive=2;
	    Integer userId=(Integer)session.getAttribute("User_ID");
	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

		try {
		// datatable filtre parametleleri array olarak al
		JsonElement filter = jsonObject.get("filter");
		filter.getAsJsonObject();
	    JsonObject filtersObj = filter.getAsJsonObject();
	    JsonElement filters = filtersObj.get("filters");
	    JsonArray filtersArr = filters.getAsJsonArray();
	    
	    // dt den alınan post edilmiş filtre alanlarını yukarıdaki array içinden ayıkla
	    JsonElement filterAdi = filtersArr.get(0);
	    JsonObject adiObj=filterAdi.getAsJsonObject();
	    adi=adiObj.get("value").getAsString();
			
		
	    JsonElement filterEmail = filtersArr.get(1);
	    JsonObject emailObj=filterEmail.getAsJsonObject();
	    email=emailObj.get("value").getAsString();
	    
	    JsonElement filterfkRol = filtersArr.get(2);
	    JsonObject fkRolObj=filterfkRol.getAsJsonObject();
	    fkRol=fkRolObj.get("value").getAsString();
	    
	    JsonElement filterisActive = filtersArr.get(3);
	    JsonObject isActiveObj=filterisActive.getAsJsonObject();
	    isActive=isActiveObj.get("value").getAsInt();

		}
		catch(Exception ex) {
			////System.out.println("filtre yok");
		}
		Integer fk_lokasyon=0;	
		Integer sysAdmin=(Integer)session.getAttribute("User_isAdmin");
		if(sysAdmin==null){
			sysAdmin=0;
		}
		List<vKULLANICILAR> vKullanicilar=null;
		List<vTotal> say=null;
		/****
		 * admin kontrolu
		 */
		if(sysAdmin>0){
			//admin ise devam et
			vKullanicilar =  service.KullaniciFiltreListesi(adi, email, fkRol,isActive,fk_lokasyon,skip,take);
			say = service.countKullaniciFiltreListesi(adi, email, fkRol,isActive,fk_lokasyon);
		}else{
			
			List<vYETKIGRUPLARI>  vYETKIGRUPLARI =  service.getYetkiGruplari(0,0,1,0,userId);
			String roller="";
			Integer i=0;
			// formdan gelen rol tümü ise kullanıcıya atanmıs tum yetki gruplarına ait kullanıcıları bul
			if(fkRol.equals("0")){
				for (vYETKIGRUPLARI item : vYETKIGRUPLARI) {
					Integer itemrol=(Integer)item.getfk_rol();
					if (itemrol == null) {
						itemrol=0;
					}
					if(itemrol>0){
						i++;
						if(i==1){
							if(item.getId()>0){
								roller +=item.getId();
							}
						}else{
							if(item.getId()>0){
								roller +=","+item.getId();
							}	
						}
					}
				}
			}else{
				roller=fkRol;
			}
			vKullanicilar =  service.KullaniciFiltreListesi(adi, email, roller.toString(),isActive,fk_lokasyon,skip,take);
			say = service.countKullaniciFiltreListesi(adi, email, roller,isActive,fk_lokasyon);
		}
		
		Map<String,Object> maps=new HashMap<>();  
		maps.put("data",vKullanicilar); 
		maps.put("total",say.get(0).getTotal());

		return maps; 
	}
	
	
	@RequestMapping(value="/kullanici/ekle", method= RequestMethod.GET)
	public ModelAndView kullaniciEkle(HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Kullanicilar/yeniKullanici");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		service.getUserYetkiBildirimModelView(model, session);
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.silver.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        js.add("/assets/demo/default/custom/crud/wizard/wizard.js");
        model.addObject("js",js);

    	UUID uuid = UUID.randomUUID();
    	String uuidStr = uuid.toString();
		Integer rolLevel=(Integer)session.getAttribute("User_Level");
		List<Lookup> roller=null;
		Integer sysAdmin=(Integer)session.getAttribute("User_isAdmin");
		Integer userId=(Integer)session.getAttribute("User_ID");
		if(sysAdmin==null){
			sysAdmin=0;
		}
		if(sysAdmin>0){
			roller = lookupService.getRoller(0,sysAdmin,1,rolLevel,0);
		}else{
			roller = lookupService.getRollerUser(userId);
		}
		model.addObject("roller", roller);
		List<vKURUMLAR> kurumlar = service.getKurumlar(0,"",0,100,userYetki);//kontrol et
		List<Lookup> lokasyon = lookupService.getLokasyon();
		model.addObject("uuid",uuidStr);
		model.addObject("kurumlar", kurumlar);
		model.addObject("lokasyon", lokasyon);
		return model;
	}
	
	@RequestMapping(value="/kullanici/kullaniciKaydet",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT kullaniciKaydet(String adi,String email,String password,String password_t,String username,String gsm,String ext,String tckimlik,String emsicil,String ksicil,Integer email_bildirim,Integer IsActive,Integer fk_rol, Integer fk_lokasyon,Integer fk_kurum,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {

		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("0");
		
		if(adi=="") {
			res.setSonuc("0");
			res.setMesaj("Ad Soyad Giriniz");
			return res;
		}
		if(username=="") {
			res.setSonuc("0");
			res.setMesaj("Kullanıcı Adı Giriniz");	
			return res;
		}

		
		if(password!="") {
			if(!password.equals(password_t)) {
				res.setSonuc("0");
				res.setMesaj("Şifreler Uyuşmuyor");		
				return res;
			}
			
			PasswordGenerator passCheck=new PasswordGenerator();
			if(!passCheck.checkPassword(password)) {
				res.setSonuc("2");
				res.setMesaj("Şifre Kriterlere Uymuyor");		
				return res;
			}
		}
		
	
		
		Integer fkIslemYapan=(Integer) session.getAttribute("User_ID");
		
		vKULLANICILAR User=service.getUserByID(fkIslemYapan);
		String fkEkleyen=User.getAdi();
		try {
			service. kullaniciKaydet(adi, email, password, username, gsm, ext, tckimlik, emsicil, ksicil, email_bildirim, IsActive, fkIslemYapan,fk_rol,fk_lokasyon,fkEkleyen,fk_kurum);
			res.setSonuc("1");
			res.setMesaj("Kullanıcı Güncellendi");	
		}catch(Exception ex) {
			res.setSonuc("0");
			res.setMesaj("Kullanıcı Güncelleme Başarısız");	
			return res;
		}
		
		
		return res;
	}
	
	
	
	//COMBO FİLTRE
	@RequestMapping(value="/AltKurumlarFiltre", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView altkurumfiltre(@Valid Integer id) {
		ModelAndView model = new ModelAndView("AYARLAR/lstFiltreALTKURUM");
		List<vALTKURUMLAR> list = service.getAltKurumlarFiltre(id);

		model.addObject("list", list);
				
		return model;
	}
	
	@RequestMapping(value="/AramaKonusuFiltre", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView aramakonusufiltre(@Valid Integer id) {
		ModelAndView model = new ModelAndView("AYARLAR/lstFiltreARAMAKONUSU");
		List<vARAMAKONUSU> list = service.getAramaKonusuFiltre(id);

		model.addObject("list", list);
				
		return model;
	}
	
	@RequestMapping(value="/SubelerFiltre", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView subelerfiltre(@Valid Integer id) {
		ModelAndView model = new ModelAndView("AYARLAR/lstFiltreSUBELER");
		List<vSUBELER> list = service.getSubelerFiltre(0,id,null,0,null,0);

		model.addObject("list", list);
				
		return model;
	}
	
	@RequestMapping(value="/HavuzlarFiltre", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView havuzlarfiltre(@Valid Integer id) {
		ModelAndView model = new ModelAndView("AYARLAR/lstFiltreHAVUZLAR");

		List<Map<String,Object>> list = service.getHavuzlar(id.toString(),1,0,0,null,2,null,0,1500);
	

		model.addObject("list", list);
				
		return model;
	}

	
	@RequestMapping(value= {"/yetki"}, method= RequestMethod.GET)
	public ModelAndView kullaniciYetki(HttpSession session) {
		ModelAndView model = new ModelAndView("AYARLAR/Yetki/Yetkiler");
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		service.getUserYetkiBildirimModelView(model, session);
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");    
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
       
        
        model.addObject("js",js);

		return model;
	}
	@RequestMapping(value="/yetkiGruplariData", method= RequestMethod.POST)
	public @ResponseBody Map<String, Object> yetkiGruplariData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		//String tip=null;
		Integer tip=1;
		try {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		
		JsonParser parser = new JsonParser();
		JsonElement jsonTree = parser.parse(querystring);
		JsonObject jsonObject = jsonTree.getAsJsonObject();
		JsonElement filter = jsonObject.get("filter");
		filter.getAsJsonObject();
		
	    JsonObject filtersObj = filter.getAsJsonObject();
	    JsonElement filters = filtersObj.get("filters");
	    
	    JsonArray valArr = filters.getAsJsonArray();
	    JsonElement valElem = valArr.get(0);
	    JsonObject tipObj=valElem.getAsJsonObject();
	    tip=tipObj.get("value").getAsInt();
		}catch(Exception e){
			//System.out.println("filtre yok");
		}
		Integer rolLevel=(Integer)session.getAttribute("User_Level");
		List<vYETKIGRUPLARI>  vYETKIGRUPLARI =  service.getYetkiGruplari(0,1,tip,rolLevel,0);
		//Map say = service.countBildirimListeNoHavuz();
		Map<String, Object> maps=new HashMap<String, Object>();  
		 maps.put("data",vYETKIGRUPLARI); 
		 maps.put("total",vYETKIGRUPLARI.size());
		return maps;
	}
	@RequestMapping(value="/yetkiGrubuDetay", method= RequestMethod.POST)
	public ModelAndView yetkiGrubuDetayy(HttpSession session,Integer id) {
		ModelAndView model = new ModelAndView("AYARLAR/Yetki/YetkiGrubuDetay"); 
		List<vYETKIALANLARI>   liste=  service.getYetkiAlanlari(0,0,id,2,null,1);
		Integer rolLevel=(Integer)session.getAttribute("User_Level");
		List<vYETKIGRUPLARI>  yetkiGrubuListe=service.getYetkiGruplari(id,1,1,rolLevel,0);
		vYETKIGRUPLARI yetkiGrubu=yetkiGrubuListe.get(0);
		
		model.addObject("yetkiAlanlari", liste);
		model.addObject("yetkiGrubu", yetkiGrubu);
		model.addObject("fkRol",id);
		return model;
	}
	@RequestMapping(value="/yetkiGrubuDetayUser", method= RequestMethod.POST)
	public ModelAndView yetkiGrubuDetayUser(HttpSession session,Integer id) {
		ModelAndView model = new ModelAndView("AYARLAR/Yetki/YetkiGrubuDetayUser"); 
		List<vYETKIALANLARI>   liste=  service.getYetkiAlanlari(0,0,id,2,null,1);
		Integer rolLevel=(Integer)session.getAttribute("User_Level");
		List<vYETKIGRUPLARI>  yetkiGrubuListe=service.getYetkiGruplari(id,1,1,rolLevel,0);
		vYETKIGRUPLARI yetkiGrubu=yetkiGrubuListe.get(0);
		
		model.addObject("yetkiAlanlari", liste);
		model.addObject("yetkiGrubu", yetkiGrubu);
		model.addObject("fkRol",id);
		return model;
	}
	@RequestMapping(value="/yetkiGuncelle",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT yetkiGuncelle(String adi,Integer isActive,Integer isHidden,Integer fkRol,String chks,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		
		session.getAttribute("User_ID");
		vJSONRESULT  res =new vJSONRESULT();
		vJSONRESULT  resTemizle =new vJSONRESULT();
		vJSONRESULT  resGuncelle =new vJSONRESULT();
		if(adi=="" || adi==null || isActive==null || isHidden==null || fkRol==null || chks=="" || chks==null) {
			res.setSonuc("0");
			res.setMesaj("EKsik Parametre");
			return res;
		}
		resTemizle =service.yetkiGrubuDetayTemizle(fkRol);
		if(resTemizle.getSonuc()=="0") {
			res.setSonuc("0");
			res.setMesaj("Yetki Grubu Detay Temizleme Hatası");
			return res;
		}
		
		resGuncelle=service.yetkiGrubuGuncelle(adi,isActive,isHidden,fkRol);
		if(resGuncelle.getSonuc()=="0") {
			res.setSonuc("0");
			res.setMesaj("Yetki Grubu Güncelleme Hatası");
			return res;
		}
		String[] yetkiler = chks.split(",");
		for (String item : yetkiler) {
			String[] checked=item.split("_");
			Integer yetkiAlani=Integer.parseInt(checked[0]);
			Integer  durum=Integer.parseInt(checked[1]);
			
			if(durum==1) {
				service.yetkiGrubuDetayGuncelle(yetkiAlani,fkRol);
			}
		}
		
		res.setMesaj("Yetki Grubu Ve Detayları GÜncellendi.");
		res.setSonuc("1");
		return res;
	}
	

	@RequestMapping(value="/generateUserPassword",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT generateUserPassword(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		vJSONRESULT ret =new vJSONRESULT(); 
		ret=Genel.checkSessionJson(session, response);
		if(ret.getSonuc()=="99") {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return ret;
		}
		PasswordGenerator pass = new PasswordGenerator();  
		String p=pass.GeneratePassword(12);
		ret.setMesaj(p);
		ret.setSonuc("1");
		return ret;
	}	
	@RequestMapping(value="/checkUserPassword",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT checkUserPassword(String password,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		vJSONRESULT ret =new vJSONRESULT();
		PasswordGenerator pass = new PasswordGenerator();
		Boolean p=pass.checkPassword(password);
		if(p==false) {
			ret.setSonuc("0");
		}else {
			ret.setSonuc("1");
		}

		return ret;
	}	
	@RequestMapping(value="/sehirler",method=RequestMethod.POST)
	public @ResponseBody Map<String, List<vILLER>> sehirler(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		String term="";
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		term = jsonObject.get("term").getAsString();
		List<vILLER> iller = lookupService.getIllerByUlke(0,190,term,null);
		Map<String, List<vILLER>> maps=new HashMap<String, List<vILLER>>();  
		 maps.put("data",iller); 
		return maps;
	}	
	@RequestMapping(value="/kurumlar",method=RequestMethod.POST)
	public @ResponseBody Map<String, List<vKURUMLAR>> kurumlar(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		String term="";
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		term = jsonObject.get("term").getAsString();
		//List<vKURUMLAR> kurumlar =  service.getKurumlar(0,term);
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		List<vKURUMLAR> kurumlar = service.getKurumlar(0,null,0,10,userYetki);//kontrol et
		
		Map<String, List<vKURUMLAR>> maps=new HashMap<String, List<vKURUMLAR>>();  
		 maps.put("data",kurumlar); 
		return maps;
	}
	@RequestMapping(value="/subeler",method=RequestMethod.POST)
	public @ResponseBody Map<String, List<vSUBELER>> subeler(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		String term="";
		Integer fk_il=0;
		Integer tip_level1=0;
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		term = null;
		fk_il = jsonObject.get("fk_il").getAsInt();
		tip_level1 = jsonObject.get("tip_level1").getAsInt();
	    
		List<vSUBELER> subeler =  service.getSubelerFiltre(0,tip_level1,term,fk_il,null,1);
	
		Map<String, List<vSUBELER>> maps=new HashMap<String, List<vSUBELER>>();  
		 maps.put("data",subeler); 
		return maps;
	}
	@RequestMapping(value="/havuzlar",method=RequestMethod.POST)
	public @ResponseBody Map<String, List<Map<String,Object>>> havuzlar(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		Integer fk_sube=0;
		String idsStr="";
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		Integer whereIn=1;
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		
		/*
		 * ids parametresini al sonundaki virgülü sil.
		 */
		try {
			JsonElement filter = jsonObject.get("ids");
			idsStr=filter.getAsString();
			if(idsStr.indexOf(",")<1) {
				idsStr="";
			}
			idsStr=idsStr.substring(0,idsStr.length()-1);
		}catch(Exception ex) {
			idsStr="";
		}
		
		List<Map<String,Object>> havuzlar;
		fk_sube = jsonObject.get("fk_sube").getAsInt();
		havuzlar = service.getHavuzlar(idsStr,1, fk_sube, 0, 0, whereIn, null,0,1500);
		Map<String, List<Map<String,Object>>> maps=new HashMap<String, List<Map<String,Object>>>();  
		 maps.put("data",havuzlar); 
		return maps;
	}	
	@RequestMapping(value="/kullanici/detay/{uid}", method= RequestMethod.GET)
	public ModelAndView kullaniciDetay(@PathVariable(value="uid",required=false) String uid,HttpSession session) {
		ModelAndView model = new ModelAndView();
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		service.getUserYetkiBildirimModelView(model, session);
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.silver.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        js.add("/assets/demo/default/custom/crud/wizard/wizard.js");
        model.addObject("js",js);

        if(uid.length()<36) {
        	model.setViewName("AYARLAR/Kullanicilar/Hata");
        	return model;
        }

        String[] code = uid.split("-");
        Integer id=Integer.parseInt(code[code.length-1]);
        vKULLANICILAR userDetail=service.getUserByID(id);
        String UserName=userDetail.getLuser();
		Integer rolLevel=(Integer)session.getAttribute("User_Level");
		List<Lookup> roller = lookupService.getRoller(0,1,1,rolLevel,0);
		List<Lookup> lokasyon = lookupService.getLokasyon();
        model.addObject("User",userDetail);
        model.addObject("UserName",UserName);
		model.addObject("roller", roller);
		model.addObject("lokasyon", lokasyon);        
        model.setViewName("AYARLAR/Kullanicilar/kullaniciDuzenle");
		return model;
	}	
	@RequestMapping(value="/kullanici/userHavuz",method=RequestMethod.POST)
	public @ResponseBody Map<String, List<vHAVUZLAR>> userHavuz(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		// datatable filtre parametleleri array olarak al
		JsonElement filter = jsonObject.get("filter");
		filter.getAsJsonObject();
	    JsonObject filtersObj = filter.getAsJsonObject();
	    JsonElement filters = filtersObj.get("filters");
	    JsonArray filtersArr = filters.getAsJsonArray();
	    
	    // dt den alınan post edilmiş filtre alanlarını yukarıdaki array içinden ayıkla
	    JsonElement filterAdi = filtersArr.get(0);
	    JsonObject adiObj=filterAdi.getAsJsonObject();
	    Integer fk_user=adiObj.get("value").getAsInt();
		
		List<vHAVUZLAR> havuzlar;
		
		havuzlar = service.getUserHavuz(fk_user, 100, "",0,0,0,0);
		Map<String, List<vHAVUZLAR>> maps=new HashMap<String, List<vHAVUZLAR>>();  
		 maps.put("data",havuzlar); 
		return maps;
	}

	
	@RequestMapping(value="/kullanici/duzenle/{uid}", method= RequestMethod.GET)
	public ModelAndView kullaniciDuzenle(@PathVariable(value="uid",required=false) String uid,HttpSession session) {
		ModelAndView model = new ModelAndView();
		List<String> css = new ArrayList<String>(); 
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		service.getUserYetkiBildirimModelView(model, session);
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.silver.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
	
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);

        if(uid.length()<1) {
        	model.setViewName("AYARLAR/Kullanicilar/Hata");
        	return model;
        }
		Integer sysAdmin=(Integer)session.getAttribute("User_isAdmin");
		Integer userId=(Integer)session.getAttribute("User_ID");
        Integer id=Integer.parseInt(uid);
        vKULLANICILAR userDetail=service.getUserByID(id);
        String UserName=userDetail.getLuser();
        String tcKimlik=userDetail.getTckimlik();
		// düzenlenen kullanıcının yetki grubu düzenleme yapan kullanıcının yoneteceği yetki grupları içinde varmı kontrol ediyoruz
		Integer userRol=(Integer)userDetail.getFkRol().intValue();
		List<vYETKIGRUPLARI> userYetkiGruplari=service.getYetkiGruplariUser(userId);
		Boolean userYetkiGrubuCheck=false;
		for (vYETKIGRUPLARI yetkiGrubuItem : userYetkiGruplari) {
			if(userRol==yetkiGrubuItem.getfk_rol()){
				userYetkiGrubuCheck=true;
			}
		}
		// kullanıcı yetkigrubuna ait yetkiye sahip ise ya da admin ise işleme devam et değilse hata mesajı
		if(userYetkiGrubuCheck==true || sysAdmin>0){
			session.setAttribute("kullaniciDuzenleOldUserName", UserName);
			session.setAttribute("kullaniciDuzenleOldtcKimlik", tcKimlik);
			model.addObject("User",userDetail);
			model.addObject("UserName",UserName);      
			Integer rolLevel=(Integer)session.getAttribute("User_Level");
			List<Lookup> roller=null;

			if(sysAdmin==null){
				sysAdmin=0;
			}
			if(sysAdmin>0){
				roller = lookupService.getRoller(0,sysAdmin,1,rolLevel,0);
			}else{
				roller = lookupService.getRollerUser(userId);
			}

			List<Lookup> lokasyon = lookupService.getLokasyon();
			model.addObject("roller",roller);
			model.addObject("lokasyon", lokasyon);
			List<vKURUMLAR> kurumlar=service.getKurumlar(0, "",0,100,userYetki);
			model.addObject("kurumlar",kurumlar);
			model.setViewName("AYARLAR/Kullanicilar/frmKullaniciDuzenle");
		}else{
			String yetkiYokMsj="Bu işlem İçin Yetkiniz Bulunmamaktadır. Sistem Yöneticinize Başvurun.";
			model.addObject("yetkiYokMsj",yetkiYokMsj);
			model.setViewName("error/YetkiYok");
		}
		return model;
	}
	 
	
	@RequestMapping(value="/kullanici/kullaniciGuncelle",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT kullaniciGuncelle(String adi,String email,String password,String password_t,String username,String gsm,String ext,String tckimlik,String emsicil,String ksicil,Integer email_bildirim,String uuid,Integer IsActive,Integer fk_rol, Integer fk_lokasyon,Integer fk_kurum,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {

		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("0");
		
		if(adi=="") {
			res.setSonuc("0");
			res.setMesaj("Ad Soyad Giriniz");
			return res;
		}
		if(username=="") {
			res.setSonuc("0");
			res.setMesaj("Kullanıcı Adı Giriniz");	
			return res;
		}

		
		if(password!="") {
			if(!password.equals(password_t)) {
				res.setSonuc("0");
				res.setMesaj("Şifreler Uyuşmuyor");		
				return res;
			}
			
			PasswordGenerator passCheck=new PasswordGenerator();
			if(!passCheck.checkPassword(password)) {
				res.setSonuc("2");
				res.setMesaj("Şifre Kriterlere Uymuyor");		
				return res;
			}
		}
		
		if(uuid=="") {
			res.setSonuc("0");
			res.setMesaj("EKsik Parametre");	
			return res;
		}
		
		
		
        String[] code = uuid.split("-");
        Integer id=Integer.parseInt(code[code.length-1]);
        
		Integer fkEkleyen=(Integer) session.getAttribute("User_ID");
		/**
		 * kullanıcı adı değişikliği varmı kontrol et.
		 * değişiklik var ise yeni kullanıcı adı baskasına aitse kontrol et.
		 */
		String oldUserName=session.getAttribute("kullaniciDuzenleOldUserName").toString();
		try {
			vKULLANICILAR checkUserName=service.findUser(0,username,null,null);
			String existUser=checkUserName.getLuser();
			if(!oldUserName.equals(existUser)) {
				res.setSonuc("2");
				res.setMesaj("Kullanıcı Adı Kullanılıyor");	
				return res;
			}
		}catch(Exception ex) {
		}
		
		
		vKULLANICILAR User=service.getUserByID(fkEkleyen);
		User.getAdi();
		try {
			service.kullaniciGuncelle(id,adi, email, password, username, gsm, ext, tckimlik, emsicil, ksicil, email_bildirim, IsActive,fkEkleyen,"","",fk_rol,fk_lokasyon,1,fk_kurum);
			res.setSonuc("1");
			res.setMesaj("Kullanıcı Güncellendi");	
		}catch(Exception ex) {
			res.setSonuc("0");
			res.setMesaj("Kullanıcı Güncelleme Başarısız");	
			return res;
		}
		
		
		return res;
	}
	
	
	
	
	@RequestMapping(value="/kullanici/havuzDuzenle/{uid}", method= RequestMethod.GET)
	public ModelAndView havuzDuzenle(@PathVariable(value="uid",required=false) String uid,HttpSession session) {
		ModelAndView model = new ModelAndView();
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		service.getUserYetkiBildirimModelView(model, session);
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.silver.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        js.add("/assets/demo/default/custom/crud/wizard/wizard.js");
        model.addObject("js",js);

        if(uid.length()<1) {
        	model.setViewName("AYARLAR/Kullanicilar/Hata");
        	return model;
        }

        String[] code = uid.split("-");
        Integer id=Integer.parseInt(code[code.length-1]);
        vKULLANICILAR userDetail=service.getUserByID(id);
		Integer sysAdmin=(Integer)session.getAttribute("User_isAdmin");
		Integer userId=(Integer)session.getAttribute("User_ID");
        String UserName=userDetail.getLuser();
        String tcKimlik=userDetail.getTckimlik();
		// düzenlenen kullanıcının yetki grubu düzenleme yapan kullanıcının yoneteceği yetki grupları içinde varmı kontrol ediyoruz
		Integer userRol=(Integer)userDetail.getFkRol().intValue();
		List<vYETKIGRUPLARI> userYetkiGruplari=service.getYetkiGruplariUser(userId);
		Boolean userYetkiGrubuCheck=false;
		for (vYETKIGRUPLARI yetkiGrubuItem : userYetkiGruplari) {
			if(userRol==yetkiGrubuItem.getfk_rol()){
				userYetkiGrubuCheck=true;
			}
		}
		// kullanıcı yetkigrubuna ait yetkiye sahip ise ya da admin ise işleme devam et değilse hata mesajı
		if(userYetkiGrubuCheck==true || sysAdmin>0){
			model.addObject("userId",id);
			model.addObject("uid",uid);
			model.addObject("User",userDetail);
			model.addObject("UserName",UserName);
			model.setViewName("AYARLAR/Kullanicilar/frmHavuzDuzenle");
		}else{
			String yetkiYokMsj="Bu işlem İçin Yetkiniz Bulunmamaktadır. Sistem Yöneticinize Başvurun.";
			model.addObject("yetkiYokMsj",yetkiYokMsj);
			model.setViewName("error/YetkiYok");
		}
		return model;
	}
	
	
	@RequestMapping(value="/kullanici/userHavuzGuncelle",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT userHavuzGuncelle(String uid,String havuzlar,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("0");
		if(uid=="") {
			res.setSonuc("0");
			res.setMesaj("Eksik Parametre");	
			return res;
		}

        Integer id=Integer.parseInt(uid);   
		Integer fkIslemYapan=(Integer) session.getAttribute("User_ID");	
		res = service.userHavuzGuncelle(id, fkIslemYapan, havuzlar);
		if(res.getSonuc()=="0") {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		return res;
	}
	
	
	
	@RequestMapping(value="/kullanici/yetkiDuzenle/{uid}", method= RequestMethod.GET)
	public ModelAndView yetkiDuzenle(@PathVariable(value="uid",required=false) String uid,HttpSession session,HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		Map<String,Object> userYetki=service.getUserYetkiFromJSON(session);
		service.getUserYetkiBildirimModelView(model, session);
		Genel.checkSessionRedirect(session, response);
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.silver.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        js.add("/assets/demo/default/custom/crud/wizard/wizard.js");
        model.addObject("js",js);

        if(uid.length()<1) {
        	model.setViewName("AYARLAR/Kullanicilar/Hata");
        	return model;
        }

		Integer id=Integer.parseInt(uid);
        vKULLANICILAR userDetail=service.getUserByID(id);
        String UserName=userDetail.getLuser();
		Integer rolLevel=(Integer)session.getAttribute("User_Level");
		List<Lookup> roller=null;
		Integer sysAdmin=(Integer)session.getAttribute("User_isAdmin");
		Integer userId=(Integer)session.getAttribute("User_ID");
		if(sysAdmin==null){
			sysAdmin=0;
		}
		if(sysAdmin>0){
			roller = lookupService.getRoller(0,sysAdmin,1,rolLevel,0);
		}else{
			roller = lookupService.getRollerUser(userId);
		}

        String tcKimlik=userDetail.getTckimlik();
		// düzenlenen kullanıcının yetki grubu düzenleme yapan kullanıcının yoneteceği yetki grupları içinde varmı kontrol ediyoruz
		Integer userRol=(Integer)userDetail.getFkRol().intValue();
		List<vYETKIGRUPLARI> userYetkiGruplari=service.getYetkiGruplariUser(userId);
		Boolean userYetkiGrubuCheck=false;
		for (vYETKIGRUPLARI yetkiGrubuItem : userYetkiGruplari) {
			if(userRol==yetkiGrubuItem.getfk_rol()){
				userYetkiGrubuCheck=true;
			}
		}
		// kullanıcı yetkigrubuna ait yetkiye sahip ise ya da admin ise işleme devam et değilse hata mesajı
		if(userYetkiGrubuCheck==true || sysAdmin>0){		
			List<Lookup> lokasyon = lookupService.getLokasyon();
			model.addObject("roller", roller);
			model.addObject("lokasyon", lokasyon);
			model.addObject("userId",id);
			model.addObject("uid",uid);
			model.addObject("User",userDetail);
			model.addObject("UserName",UserName);

			model.setViewName("AYARLAR/Kullanicilar/frmYetkiDuzenle");
		}else{
			String yetkiYokMsj="Bu işlem İçin Yetkiniz Bulunmamaktadır. Sistem Yöneticinize Başvurun.";
			model.addObject("yetkiYokMsj",yetkiYokMsj);
			model.setViewName("error/YetkiYok");
		}
		return model;
	}
	
	
	@RequestMapping(value="/yetkiGrubuDetayDuzenleUser", method= RequestMethod.POST)
	public ModelAndView yetkiGrubuDetayDuzenleUser(HttpSession session,Integer id,Integer tip) {
		ModelAndView model = new ModelAndView(); 
		
		List<vYETKIALANLARI>   liste;
		List<vYETKIGRUPLARI>  yetkiGrubuListe;
		vYETKIGRUPLARI yetkiGrubu = null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		if(tip>0) {
			// kullanıcının kendisine verilmiş olan yetkileri gosteren yetki sayfası
			liste =  service.getUserYetkiAlanlariDetay(id);
			model.setViewName("AYARLAR/Yetki/YetkiGrubuDetayDuzenleUser");
			Integer rolLevel=(Integer)session.getAttribute("User_Level");
			yetkiGrubuListe=service.getYetkiGruplari(0,0,1,rolLevel,userId);
		}else {
			// burası kullanıcı yetki detayında kullanıcı rolü seçilince yüklenen sayfadır. kullanıcıya rollere atanmıs hazır yetkileri otomatik vermeyi sağlar
			model.setViewName("AYARLAR/Yetki/YetkiGrubuDetayUser");
			liste =  service.getYetkiAlanlari(0,0,id,2,null,1);
			Integer rolLevel=(Integer)session.getAttribute("User_Level");
			yetkiGrubuListe=service.getYetkiGruplari(id,1,1,rolLevel,0);
			yetkiGrubuListe.get(0);
		}
		
		model.addObject("yetkiAlanlari", liste);
		model.addObject("yetkiGrubu", yetkiGrubu);
		model.addObject("yetkiGrubuListe", yetkiGrubuListe);
		model.addObject("fkRol",id);
		return model;
	}
	
	
	
	@RequestMapping(value="/kullanici/yetkiGrubuDetayGuncelleUser",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT yetkiGrubuDetayGuncelleUser(String uid,String yetkiler,Integer fk_rol,Integer fk_lokasyon,String yetkiRol,HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("0");
		if(uid=="") {
			res.setSonuc("0");
			res.setMesaj("Eksik Parametre");	
			return res;
		}

        String code = uid;
        Integer id=Integer.parseInt(code);   
		Integer fkIslemYapan=(Integer) session.getAttribute("User_ID");
		//System.out.println(yetkilerJson);
		res=service.yetkiGrubuDetayGuncelleUser(id, fkIslemYapan, yetkiler, fk_rol, fk_lokasyon,yetkiRol);
		
		if(res.getSonuc()=="0") {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		return res;
	}


	
	@RequestMapping(value="/getAktifPasifList", method= RequestMethod.GET)
	public @ResponseBody List<Map<String,Object>>  getAktifPasifList(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		List<Map<String,Object>> list = service.getAktifPasifList(1);
		return list;
	}

	@RequestMapping(value="/tanimlar/Update",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT tanimlarKurumUpdate(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
		vJSONRESULT  res =new vJSONRESULT();
		List<String> cols=new ArrayList<String>();
		List<String> vals=new ArrayList<String>();
		String splitter=request.getParameter("splitter");
		if(splitter=="" || splitter==null){
			splitter=",";
		}


		String c=request.getParameter("cols").toString();
		String col[]= c.split(splitter);
		Integer colCount=col.length;
		String v=request.getParameter("vals").toString();
		String val[]= v.split(splitter);

		Integer id=Integer.parseInt(request.getParameter("id"));

		// db ismini alıyoruz
		String db=request.getParameter("dName");

		String tbl="";
		//db ismi boş ise yada gelmemişse db yi smdr olarak ayarla
		if(db=="" || db==null){
			db=DnameCrm;
		}
		tbl=db+"."+request.getParameter("tbl");
		for (int i = 0; i < colCount; i++) {
			cols.add(col[i].toString());
			vals.add(val[i].toString());
		}
		res=service.tnmUpdate(colCount,tbl,cols, vals, id,splitter);
		if(res.getSonuc()=="1"){
			res.setMesaj("Tanım GÜncellendi.");
			res.setSonuc("1");
		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			res.setSonuc("0");
		}
		return res;
	}

	@RequestMapping(value="/tanimlar/Insert",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT tanimlarKurumInsert(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {		
		// post edilen datayı key val olarak almak
		//for(String params : Collections.list(request.getParameterNames())) {
			// Whatever you want to do with your map
			// Key : params
			// Value : httpServletRequest.getParameter(params)                
		//	System.out.println(params);

		//}

		vJSONRESULT  res =new vJSONRESULT();
		List<String> cols=new ArrayList<String>();
		List<String> vals=new ArrayList<String>();
		String splitter=request.getParameter("splitter");
		if(splitter=="" || splitter==null){
			splitter=",";
		}


		String c=request.getParameter("cols").toString();
		String col[]= c.split(splitter);
		Integer colCount=col.length;
		String v=request.getParameter("vals").toString();
		String val[]= v.split(splitter);
		// db ismini alıyoruz
		String db=request.getParameter("dName");

		String tbl="";
		//db ismi boş ise yada gelmemişse db yi smdr olarak ayarla
		if(db=="" || db==null){
			db=DnameCrm;
		}else{
			db=DnamePbx;
		}
		tbl=db+"."+request.getParameter("tbl");
		for (int i = 0; i < colCount; i++) {
			cols.add(col[i].toString());
			vals.add(val[i].toString());
		}
		res=service.tnmInsert(colCount,tbl,cols, vals,splitter);
		if(res.getSonuc()=="1"){
			res.setMesaj("Ekleme İşlemi Balşarılı.");
			res.setSonuc("1");
		}else{
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			res.setSonuc("0");
		}
		return res;

	}

	
}


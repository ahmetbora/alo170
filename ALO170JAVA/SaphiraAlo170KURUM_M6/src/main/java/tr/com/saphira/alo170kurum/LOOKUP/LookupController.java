package tr.com.saphira.alo170kurum.LOOKUP;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.AYARLAR.AyarlarService;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKANALI;
import tr.com.saphira.alo170kurum.AYARLAR.vILCELER;
import tr.com.saphira.alo170kurum.AYARLAR.vILLER;
import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.model.Lookup;
@SuppressWarnings("unused")
@Controller
@EnableWebSecurity
@RequestMapping(value="/lookup")
public class LookupController {
    
    @Autowired 
	private HttpServletRequest request;
    
    @Autowired
	LOOKUPService lookupService;

	@Autowired
	AyarlarService ayarlarService;

    @RequestMapping(value="/getSubeler", method=RequestMethod.POST)
    @ResponseBody
    public ModelAndView subeler(Integer id,Integer fk_konu,String term,Integer fk_il, String ids, Integer wherein) {
            ModelAndView model = new ModelAndView("lookup");
            
            List<Lookup> l = lookupService.getSubeler(id, fk_konu, term, fk_il,  ids,  wherein);
            model.addObject("l", l);
        return model;
    }
    @RequestMapping(value="/getHavuzlar", method=RequestMethod.POST)
    @ResponseBody
    public ModelAndView havuzlar(@Valid Integer fk_sube) {
            ModelAndView model = new ModelAndView("lookup");
            List<Lookup> l = lookupService.cmbgetHavuzlarSube(fk_sube);
            model.addObject("l", l);
        return model;
    }


	@RequestMapping(value="/getIlceler", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView ilceler(@Valid Integer fk_il) {
       
		ModelAndView model = new ModelAndView("lookup");
			List<vILCELER> l = lookupService.getIlceler(fk_il);
			model.addObject("l", l);
		return model;
	}

	@RequestMapping(value="/getIller", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView iller(@Valid Integer fk_ulke) {
		ModelAndView model = new ModelAndView("lookup");
			List<vILLER> l = lookupService.getIllerByUlke(0,fk_ulke, null,null);
			model.addObject("l", l);
		return model;
	}
	

	@RequestMapping(value="/getAramaKanali", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView AramaKanali(@Valid Integer arama_kanali_tipi) {
		ModelAndView model = new ModelAndView("lookup");
			List<vARAMAKANALI> l = lookupService.getAramaKanaliByTip(arama_kanali_tipi);
			model.addObject("l", l);
		return model;
	}

	@RequestMapping(value="/getAramaSebebi", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView AramaSebebi() {
		ModelAndView model = new ModelAndView("lookup");
        List<vARAMAKANALI> l = lookupService.getAramaKanaliByTip(0);	
		model.addObject("l", l);
		return model;
	}
	@RequestMapping(value="/getUsers", method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> Users(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		JsonParser jsonParser = new JsonParser();
		String adi=null;
		String rol=null;
		Integer fk_lokasyon=0;
		Integer limit=0;
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		try {
            adi=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			adi="";
		}
		try {
            rol=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			rol="";
		}
		try {
            fk_lokasyon=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_lokasyon=0;
		}
		try {
            limit=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			limit=500;
		}
		// ust duzey yetkisi olmayan kullanıcılarda sadece kendi havuzuna baglı olan kullanıcılar listelenecek ve isactive=1 olacak
		List<Map<String,Object>> l=lookupService.getUsers(adi,"",rol,1, fk_lokasyon);
		Map<String,Object> maps=new HashMap<>();   
		maps.put("data",l); 
		return maps;
	}

	@RequestMapping(value="/getUsersByUserHavuz", method=RequestMethod.POST)
	public @ResponseBody Map<String,Object> getUsersByUserHavuz(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
		JsonParser jsonParser = new JsonParser();
		Integer limit=0;
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");
		Integer userId=(Integer) session.getAttribute("User_ID");
		List<Map<String,Object>> l=lookupService.getUsersByUserHavuz(userId);
		Map<String,Object> maps=new HashMap<>();   
		maps.put("data",l); 
		return maps;				
	}

	@RequestMapping(value="/getSssAltKonu", method=RequestMethod.POST)
	@ResponseBody
	public ModelAndView getSssAltKonu(Integer fk_sss_grup) {
		ModelAndView model = new ModelAndView("lookup");
        List<Lookup> l = lookupService.getSssAltKonu(fk_sss_grup);	
		model.addObject("l", l);
		return model;
	}


	@RequestMapping(value="/getCmbDetayWhere", method= RequestMethod.POST)
	public ModelAndView getCmbDetayWhere(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		String where="";
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    String tbl = jsonObject.get("tbl").getAsString();
		String selectedId=jsonObject.get("selectedId").getAsString();
		JsonParser jsonParser = new JsonParser();
		JsonArray models =jsonParser.parse(querystring)
		.getAsJsonObject().getAsJsonArray("fields");
		for (int i = 0; i < models.size(); i++) {
			String fld=models.get(i).getAsJsonObject().get("keyfiled").getAsString();
			fld=fld.replaceAll("\"","");
			String val=models.get(i).getAsJsonObject().get("keyval").toString();
			val=val.replaceAll("\"","");
			if(fld.length()>0 && val.length()>0){
				where +=" and "+fld+"="+val;
			}
		}
		List<Map<String, Object>> l = lookupService.getCmbDetayWhere(tbl,where);
		ModelAndView model = new ModelAndView("lookup");
		model.addObject("l",l);
		model.addObject("selectedId",selectedId);
		return model;
	}
	
}

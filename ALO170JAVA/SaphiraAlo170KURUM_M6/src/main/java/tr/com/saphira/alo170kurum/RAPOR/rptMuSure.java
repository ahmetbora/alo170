package tr.com.saphira.alo170kurum.RAPOR;

public class rptMuSure {
    private Integer id;
    private String mu;
    private String tarih;
    private String kurumbilgisi;
    private String havuz;
    private String sube;
    private String sure;





    public rptMuSure() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getmu(){
        return mu;
    }
    public void setmu(String mu){
        this.mu=mu;
    }

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String getkurumbilgisi(){
        return kurumbilgisi;
    }
    public void setkurumbilgisi(String kurumbilgisi){
        this.kurumbilgisi=kurumbilgisi;
    }
    public String gethavuz(){
        return havuz;
    }
    public void sethavuz(String havuz){
        this.havuz=havuz;
    }
    public String getsube(){
        return sube;
    }
    public void setsube(String sube){
        this.sube=sube;
    }
    public String getsure(){
        return sure;
    }
    public void setsure(String sure){
        this.sure=sure;
    }



}


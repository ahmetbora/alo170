package tr.com.saphira.alo170kurum.API;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApiServiceImpl implements ApiService{

	@Autowired
	ApiDao dao;
	
	public void setDao(ApiDao dao) {
		this.dao = dao;
	}

	@Override
	public Dashboard bugunAcilan() {
		return dao.bugunAcilan();
	}

}

package tr.com.saphira.alo170kurum.LOOKUP;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.saphira.alo170kurum.AYARLAR.vULKELER;
import tr.com.saphira.alo170kurum.AYARLAR.vALTKURUMLAR;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKANALI;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKONUSU;
import tr.com.saphira.alo170kurum.AYARLAR.vILCELER;
import tr.com.saphira.alo170kurum.AYARLAR.vILLER;
import tr.com.saphira.alo170kurum.AYARLAR.vKURUMLAR;
import tr.com.saphira.alo170kurum.AYARLAR.vOZELNUMARALAR;
import tr.com.saphira.alo170kurum.model.Lookup;

@Service
public class LOOKUPServiceImpl implements LOOKUPService {

	 
	 private LOOKUPDaoImpl Dao;
	 
	 @Autowired
	 public void setUserDao(LOOKUPDaoImpl Dao) {
		this.Dao = Dao;
	}

	public List<Lookup> getAllUser() {
	  return Dao.getServer();
	 }


	@Override
	public List<vULKELER> getUlkeler(Integer ID) {
		
		return Dao.getUlkeler(ID);
	}
	
	@Override
	public List<vILLER> getIller(Integer ID) {
		
		return Dao.getIller(ID);
	}
	
	@Override
	public List<vILLER> getIllerByUlke(Integer ID,Integer FKULKE,String ADI,String IDS) {
		
		return Dao.getIllerByUlke(ID, FKULKE, ADI,IDS);
	}

	@Override
	public List<vILCELER> getIlceler(Integer IL) {
		
		return Dao.getIlceler(IL);
	}

	@Override
	public List<Lookup> getAramaKanaliTipi() {
		
		return Dao.getAramaKanaliTipi();
	}

	@Override
	public List<Lookup> getTaskStatu(Integer id) {
		
		return Dao.getTaskStatu(id);
	}

	@Override
	public List<Lookup> cmbgetHavuzlar(Integer AGENT) {
		
		return Dao.cmbgetHavuzlar(AGENT);
	}
	@Override
	public List<Lookup> cmbgetHavuzlarSube(Integer SUBE) {
		
		return Dao.cmbgetHavuzlarSube(SUBE);
	}

	@Override
	public List<Lookup> getRoller(Integer ID,Integer isSysAdmin,Integer tip,Integer level,Integer userId) {
		
		return Dao.getRoller(ID, isSysAdmin, tip,level,userId);
	}
	public List<Lookup> getRollerUser(Integer userId) {
		return Dao.getRollerUser(userId);
	}

	public List<vULKELER> getUlkeler(int ID) {
		
		return Dao.getUlkeler(ID);
	}

	@Override
	public List<Lookup> getAramaKanali(Integer ID) {
		
		return Dao.getAramaKanali(ID);
	}
	
	
	 /******
	  * ARAMA KANALI TÜRLERİ ALTINDAKİ ARAMA KANALLARI LİSTESİ
	  * @param ID
	  * @return
	  */
	@Override
	public List<vARAMAKANALI> getAramaKanaliByTip(Integer ID) {
		
		return Dao.getAramaKanaliByTip(ID);
	}

	@Override
	public List<Lookup> getEgitimDurumu(Integer ID) {
		
		return Dao.getEgitimDurumu(ID);
	}

	@Override
	public List<Lookup> getGuvenlikSorusu(Integer ID) {
		
		return Dao.getGuvenlikSorusu(ID);
	}



	@Override
	public vILLER getIllerById(Integer ID) {
		
		return Dao.getIllerById(ID);
	}

	@Override
	public void setIller(Integer ID, String ADI, Integer ULKE, Integer ISACTIVE) {
		
		this.Dao.setIller(ID, ADI, ULKE, ISACTIVE);
	}

	@Override
	public vILCELER getIlcelerById(Integer ID) {
		
		return Dao.getIlcelerById(ID);
	}

	@Override
	public void setIlceler(Integer ID, String ADI, Integer IL, Integer ISACTIVE) {
		
		 this.Dao.setIlceler(ID, ADI, IL, ISACTIVE);
	}

	@Override
	public Lookup getAramaKanaliById(Integer ID) {
		
		return Dao.getAramaKanaliById(ID);
	}

	@Override
	public void setAramaKanali(Integer ID, String ADI, Integer ISACTIVE) {
		
		this.Dao.setAramaKanali(ID, ADI, ISACTIVE);
	}

	@Override
	public Lookup getEgitimDurumuById(Integer ID) {
		
		return Dao.getEgitimDurumuById(ID);
	}

	@Override
	public void setEgitimDurumu(Integer ID, String ADI, Integer ISACTIVE) {
		
		this.Dao.setEgitimDurumu(ID, ADI, ISACTIVE);
	}

	@Override
	public Lookup getGuvenlikSorusuById(Integer ID) {
		
		return Dao.getGuvenlikSorusuById(ID);
	}

	@Override
	public void setGuvenlikSorusu(Integer ID, String ADI, Integer ISACTIVE) {
		
		this.Dao.setGuvenlikSorusu(ID, ADI, ISACTIVE);
	}

	@Override
	public List<Lookup> getAramaSebebi(Integer ID) {
		
		return Dao.getAramaSebebi(ID);
	}

	@Override
	public List<vKURUMLAR> getKurumlar(Integer ID,String TERM,Integer skip, Integer take) {
		
		return Dao.getKurumlar(ID, TERM, skip,  take);
	}
	
	@Override
	public List<vALTKURUMLAR> getAltKurumlar(Integer ID) {
		
		return Dao.getAltKurumlar(ID);
	}
	
	@Override
	public List<vARAMAKONUSU> getAramaKonulari(Integer ID) {
		
		return Dao.getAramaKonulari(ID);
	}
	
	
	@Override
	public List<Lookup> getCinsiyet() {
		
		return Dao.getCinsiyet();
	}
	
	@Override
	public List<Lookup> getGeriDonusKanali() {
		
		return Dao.getGeriDonusKanali();
	}
	
	@Override
	public List<Lookup> getSektorler() {
		
		return Dao.getSektorler();
	}
	
	
	@Override
	public List<vOZELNUMARALAR> getOzelNumaralar(String NO) {
		
		return Dao.getOzelNumaralar(NO);
	}
	
	@Override
	public List<Lookup> getBilgiBankasiKonular() {
		
		return Dao.getBilgiBankasiKonular();
	}
	

	@Override
	public List<Lookup> getLokasyon() {
		
		return Dao.getLokasyon();
	}
	@Override
	public Map<String, Object>  getLokasyonById(Integer id) {
		
		return Dao.getLokasyonById(id);
	}

	@Override
	public List<Lookup> getUlkelerById(Integer ID) {
		
		return null;
	}

	@Override
	public void setUlkeler(Integer ID, String ADI, Integer ISACTIVE) {
		
		
	}
	
	
	@Override
	public List<Lookup> getSubeler(Integer id,Integer fk_konu,String term,Integer fk_il, String ids, Integer wherein) {
		
		return Dao.getSubeler(id,fk_konu,term,fk_il, ids, wherein);
	}
	
	@Override
	public List<Lookup> getTaskHavuzKullanici(Integer fk_havuz) {
		
		return Dao.getTaskHavuzKullanici(fk_havuz);
	}
	@Override
	public List<Lookup> getTaskbKapanmaStatu(Integer id){
		
		return Dao.getTaskbKapanmaStatu(id);
	}
	@Override
	public List<Lookup> getSssKonu(){
		
		return Dao.getSssKonu();
	}
	@Override
	public List<Lookup> getSssAltKonu(Integer fk_sss_grup){
		
		return Dao.getSssAltKonu(fk_sss_grup);
	}
	@Override
	public List<Lookup> getVardiyaTipleri(String ID,String Tip){
		
		return Dao.getVardiyaTipleri(ID,Tip);
	}
	@Override
	public List<Map<String, Object>>  getCmbDetayWhere(String table,String where){
		return Dao.getCmbDetayWhere(table,where);
	}
	@Override
	public List<Map<String, Object>>  getUsers(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon) {
		return Dao.getUsers(ADI, EMAIL, ROL, isActive,fk_lokasyon );
	}
	@Override
	public List<Map<String, Object>>  getUsersByUserHavuz(Integer fk_user){
		return Dao.getUsersByUserHavuz(fk_user );
	}
/*	@Override
	public List<Lookup> getSubeler(Integer ID,Integer KONU,String TERM,Integer FKIL,String IDS,Integer WHEREIN) {
		
		return Dao.getSubeler(ID, KONU, TERM, FKIL, IDS, WHEREIN);
	}*/

}

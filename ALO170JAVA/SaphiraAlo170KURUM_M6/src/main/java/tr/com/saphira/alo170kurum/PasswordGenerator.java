package tr.com.saphira.alo170kurum;
import java.security.SecureRandom;
public class PasswordGenerator {
    private SecureRandom random;
    private String sifre;
 
    private String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private String ALPHA = "abcdefghijklmnopqrstuvwxyz";
    private String NUMERIC = "0123456789";
    private String SPECIAL = "!-_?+!@#$%()=}][{/";
    private Integer maxPasswordLength=50;
    

    
    private String karakterleriSec(int len, String dic) {
	    String result = "";
	    for (int i = 0; i < len; i++) {
	        int index = random.nextInt(dic.length());
	        result += dic.charAt(index);
	    }
	   
	    return result;
    }
    
    private String shuffle(String s)
    {
        char c;
        StringBuffer result = new StringBuffer(s);
        int n = result.length();
        while ( n > 1 )
        {
            int pos1 = random.nextInt(n);
            int pos2 = random.nextInt(n);
            c = result.charAt(pos1);
            result.setCharAt(pos1, result.charAt(pos2));
            result.setCharAt(pos2, c);
            n--;
        }
        return result.toString();
    }

	public Boolean checkPassword(String kSifre) {
		int i;
		Boolean alphaCapsVar = false;
		Boolean alphaVar = false;
		Boolean numericVar = false;
		Boolean specialVar = false;
		// Boolean passLengthVar=false;
	
		for (i=0; i < kSifre.length(); i++)
			if (ALPHA_CAPS.indexOf(kSifre.charAt(i)) == -1) {
				alphaCapsVar = false;
			}else {
				alphaCapsVar = true;
				break;				
			}
		for (i=0; i < kSifre.length(); i++)
			if (ALPHA.indexOf(kSifre.charAt(i)) == -1) {
				alphaVar = false;
			}else {
				alphaVar = true;
				break;
			}
		for (i=0; i < kSifre.length(); i++)
			if (NUMERIC.indexOf(kSifre.charAt(i)) == -1) {
				numericVar = false;
			}else {
				numericVar = true;
				break;				
			}
		for (i=0; i < kSifre.length(); i++)
			if (SPECIAL.indexOf(kSifre.charAt(i)) == -1) {
				specialVar = false;
				
			}else {
				specialVar = true;
				break;
			}
		
		if(kSifre.length()<9) {
			// passLengthVar = false;
		}else {
			// passLengthVar = true;
		}
		
		return alphaCapsVar && alphaVar && numericVar && specialVar;
	}

	public String GeneratePassword(int karakterSayisi) {
		sifre = "";
		if(karakterSayisi>maxPasswordLength) {
			sifre="En Fazla 50 Karakter Şifre Belirlenebilir";
			return sifre;
		}
		random = new SecureRandom();
    	
    	sifre += karakterleriSec(random.nextInt(karakterSayisi / 4) + 1, ALPHA_CAPS);
    	sifre += karakterleriSec(1, SPECIAL);
    	sifre += karakterleriSec(random.nextInt(karakterSayisi / 4) + 1, ALPHA);
    	sifre += karakterleriSec(1, SPECIAL);
    	sifre += karakterleriSec((karakterSayisi - sifre.length()), NUMERIC);
    	int shuffleCount=random.nextInt(karakterSayisi);
    	for (int i = 0; i < shuffleCount; i++) {
    		sifre = shuffle(sifre);	
    	}
  	
    	return sifre;
	}
}

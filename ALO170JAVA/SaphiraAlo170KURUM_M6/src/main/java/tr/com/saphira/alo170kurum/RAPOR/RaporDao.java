package tr.com.saphira.alo170kurum.RAPOR;

import java.util.List;
import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.RAPOR.rptKurumSubeIstatistik;
import tr.com.saphira.alo170kurum.KART.vKART;
import tr.com.saphira.alo170kurum.SCRIPT.vSCRIPT;
import tr.com.saphira.alo170kurum.SSS.vSSS;
import tr.com.saphira.alo170kurum.model.vTotal;


@SuppressWarnings("unused")
public interface RaporDao {
    
    public List<rptKurumSubeIstatistik> KurumSubeHavuzIstatistikRaporu(
        String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer fk_havuz,
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
	public List<vTotal> countKurumSubeHavuzIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer fk_havuz,
		Integer userId,
		String yetki
	  );


    
    public List<rptKurumKullaniciIstatistik> KurumKullaniciIstatistikRaporu(
        String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
 	public List<vTotal> countKurumKullaniciIstatistikRaporu(
        String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer userId,
		String yetki
	);   



    public List<rptBildirimDetay> BildirimDetayRaporu(
        String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer tip_level2,
		Integer tip_level3,
		Integer fk_il, 
		Integer fk_ilce, 
		Integer fk_havuz, 
		Integer fk_gorev_statu, 
		Integer arama_kanali_tipi, 
		Integer fk_aramakanali, 
		Integer fk_aramasebebi, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
	public List<vTotal> countBildirimDetayRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer tip_level2,
		Integer tip_level3,
		Integer fk_il, 
		Integer fk_ilce, 
		Integer fk_havuz, 
		Integer fk_gorev_statu, 
		Integer arama_kanali_tipi, 
		Integer fk_aramakanali, 
		Integer fk_aramasebebi, 
		Integer userId,
		String yetki
	);

	public List<rptMuAktivite> MuAktiviteRapor(
		String sdate, 
		String fdate, 
		Integer mu, 
		Integer sistem, 
		Integer skip, 
		Integer take, 
		Integer userId, 
		String yetki
	);
	public List<vTotal> countMuAktiviteRapor(
		String sdate, 
		String fdate, 
		Integer mu, 
		Integer sistem,  
		Integer userId, 
		String yetki
	);

    public List<rptMuSure> MuSureRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
		Integer tip_level1, 
        Integer sistem, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
    );
	public List<vTotal> countMuSureRapor(
		String sdate, 
		String fdate, 
		Integer mu, 
		Integer tip_level1, 
		Integer sistem, 
		Integer userId, 
		String yetki
	);    


        
    public List<rptKySure> KySureRapor(
        String sdate, 
        String fdate, 
        Integer ky, 
        Integer sistem, 
        Integer tip_level1, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
    );
	public  List<vTotal> countKySureRapor(
        String sdate, 
        String fdate, 
        Integer ky, 
        Integer sistem, 
        Integer tip_level1, 
        Integer userId, 
        String yetki
	 );

        
    public List<rptHavuzBildirimSure> HavuzBildirimSureRapor(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        Integer fk_havuz, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
    );
	public  List<vTotal> countHavuzBildirimSureRapor(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        String fk_havuz, 
        Integer userId, 
        String yetki
	 );

        
    public List<rptSektorBazinda> SektorBazindaRapor(
        String sdate, 
        String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
        String yetki
    );
	public  List<vTotal> countSektorBazindaRapor(
        String sdate, 
        String fdate,  
        Integer userId, 
        String yetki
	 );

        
    public List<rptSektorBazindaIhbarSikayet> SektorBazindaIhbarSikayet(
        String sdate, 
		String fdate, 
		Integer fk_sektor, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
	public  List<vTotal> countSektorBazindaIhbarSikayet(
        String sdate, 
		String fdate, 
		Integer fk_sektor, 
		Integer userId,
		String yetki
	 );

        
    public List<rptIlBazinda> IlBazindaRapor(
        String sdate, 
		String fdate, 
		Integer fk_gorev_statu, 
		Integer userId,
		String yetki
    );

        
    public List<rptKonularaGore> KonularaGore(
		String sdate, 
		String fdate, 
		Integer statu,
		Integer tip_level1, 
		Integer tip_level2, 
		Integer tip_level3, 
		Integer userId,
		String yetki
    );

        
    public List<rptHavuzOzet> HavuzOzet(
		Integer skip,
		Integer take,
		Integer userId
    );
	public  List<vTotal> countHavuzOzet(
		Integer userId
	 );
        
    public List<rptSgkBilgilendirme> SgkBilgilendirme(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
	public  List<vTotal> countSgkBilgilendirme(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 );
        
    public List<rptIvrAgent> IvrAgent(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
	public  List<vTotal> IvrAgentCount(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 );
        
    public List<rptTaseronHavuzu> TaseronHavuzu(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
	public  List<vTotal> TaseronHavuzuCount(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 );
        
    public List<rptSesKayit> SesKayit(
		String sdate, 
		String fdate, 
		String tc, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
	public  List<vTotal> SesKayitCount(
		String sdate, 
		String fdate, 
		String tc, 
		Integer userId,
		String yetki
	 );
        
    public List<rptKurumKapanmaIstatistik> KurumKapanmaIstatistik(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
    );

        
    public List<rptBirimlereGoreKullaniciSayisi> BirimlereGoreKullaniciSayisi(
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
	public  List<vTotal> BirimlereGoreKullaniciSayisiCount(
		Integer userId,
		String yetki
	 );


    public List<rptKurumBazindaZamanindaKapanan> KurumBazindaZamanindaKapanan(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    ); 
	public  List<vTotal> KurumBazindaZamanindaKapananCount(
		Integer userId,
		String yetki
	 );



    public List<rptIvrRapor> IvrRapor(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
    ); 

    public List<rptSmsRapor> SmsRapor(
		Integer skip,
		Integer take,
		String tipi,
		Integer userId,
		String yetki
    ); 
	public  List<vTotal> SmsRaporCount(
		String tipi,
		Integer userId,
		String yetki
	 );
	 

    public List<rptPeriyodikRapor> PeriyodikRaporGelenCagri(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    ); 
	public  List<vTotal> PeriyodikRaporGelenCagriCount(
		String sdate,
		String fdate, 
		Integer userId,
		String yetki
	 );


	 public List<rptPeriyodikRapor> PeriyodikRaporIlkOnKonu(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 );



	 
	 public List<rptPeriyodikRapor> PeriyodikRaporDisAramaSms(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 );


    public List<rptRaporLog> RaporLog(
		String sdate, 
		String fdate, 
		Integer fk_user,
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    ); 
	public List<vTotal> countRaporLog(
		String sdate,
		String fdate,
		Integer fk_user,
		Integer userId,
		String yetki
	 );


  /* public List<Map<String,Object>> KurumSubeHavuzIstatistikRaporu(
    String sdate, 
    String fdate, 
    Integer tip_level1, 
    Integer fk_il, 
    Integer fk_sube, 
    Integer fk_havuz,
    Integer skip,
    Integer take,
    Integer userId,
    vUSERYETKI yetki
    );*/
}

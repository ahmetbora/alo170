package tr.com.saphira.alo170kurum.BILDIRIM;

public class vBILDIRIM {
	private int id;
	private int fk_agent;
	private int fk_kart;
	private String tarih;
	private String basvuruSahibi;
	private String isiYapacakAgent;
	private String kaydiAcanAgent;
	private String konu1;
	private String konu2;
	private String konu3;
	private String statu;
	private String ilAdi;
	private String cls;
	private String fulldate;
	private String tip;
	private String saat;
	private String fark;
	private String bugun;
	private String tc;
	private String tel;
	private String ilceAdi;
	private String havuzAdi;
	private Long havuzSure;
	private Integer hCode;
	private String kaydi_acan;
	private String aciklama;
	private String ukey;
	private Integer tip_level1;
	private Integer tip_level2;
	private Integer tip_level3;
	private Integer isEnc;
	private String cryp_aciklama;
	private String lokasyon;
	private String konuFull;
	private String isOnayBekle;
	private Integer fkGorevStatu;
	private Integer fk_statu;
	private Integer fkIl;
	private Integer fkHavuz;
	private Integer fkSube;
	private String aramakanali;
	private String aramakanalituru;
	private String aramasebebi;
	private Integer fk_aramasebebi;
	private Long f_id;
	private Integer fk_proje;
	private String f_adi;
	private String f_adres;
	private String f_tel;
	private String nace;
	private String scs;
	private String f_il;
	private String f_ilce;
	private String subeAdi;



	public vBILDIRIM() {
		super();
	
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFk_agent() {
		return fk_agent;
	}
	public void setFk_agent(int fk_agent) {
		this.fk_agent = fk_agent;
	}
	
	public int getFk_kart() {
		return fk_kart;
	}
	public void setFk_kart(int fk_kart) {
		this.fk_kart = fk_kart;
	}
	public String getTarih() {
		return tarih;
	}
	public void setTarih(String tarih) {
		this.tarih = tarih;
	}
	public String getBasvuruSahibi() {
		return basvuruSahibi;
	}
	public void setBasvuruSahibi(String basvuruSahibi) {
		this.basvuruSahibi = isNull(basvuruSahibi);
	}
	public String getIsiYapacakAgent() {
		return isiYapacakAgent;
	}
	public void setIsiYapacakAgent(String isiYapacakAgent) {
		this.isiYapacakAgent =isNull(isiYapacakAgent);
	}
	public String getKaydiAcanAgent() {
		return kaydiAcanAgent;
	}
	public void setKaydiAcanAgent(String kaydiAcanAgent) {
		this.kaydiAcanAgent = isNull(kaydiAcanAgent);
	}
	public String getKonu1() {
		return konu1;
	}
	public void setKonu1(String konu1) {
		this.konu1 = isNull(konu1);
	}
	public String getKonu2() {
		return konu2;
	}
	public void setKonu2(String konu2) {
		this.konu2 = isNull(konu2);
	}
	public String getKonu3() {
		return konu3;
	}
	public void setKonu3(String konu3) {
		this.konu3 = isNull(konu3);
	}
	public String getStatu() {
		return statu;
	}
	public void setStatu(String statu) {
		this.statu = isNull(statu);
	}
	public String getIlAdi() {
		return ilAdi;
	}
	public void setIlAdi(String ilAdi) {
		if (ilAdi != null && ilAdi.isEmpty()) {
			this.ilAdi = "";	
		} else {
			this.ilAdi = ilAdi;
		}
	}
	public String getCls() {
		return cls;
	}
	public void setCls(String cls) {
		this.cls = isNull(cls);
	}
	public String getFulldate() {
		return fulldate;
	}
	public void setFulldate(String fulldate) {
		this.fulldate = isNull(fulldate);
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = isNull(tip);
	}
	public String getSaat() {
		return saat;
	}
	public void setSaat(String saat) {
		this.saat = isNull(saat);
	}
	public String getFark() {
		return fark;
	}
	public void setFark(String fark) {
		this.fark = isNull(fark);
	}
	public String getBugun() {
		return bugun;
	}
	public void setBugun(String bugun) {
		this.bugun = isNull(bugun);
	}
	public String getTc() {
		return tc;
	}
	public void setTc(String tc) {
		this.tc = isNull(tc);
	}
	
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getIlceAdi() {
		return ilceAdi;
	}
	public void setIlceAdi(String ilceAdi) {
		this.ilceAdi = isNull(ilceAdi);
	}
	public String getHavuzAdi() {
		return havuzAdi;
	}
	public void setHavuzAdi(String havuzAdi) {
		this.havuzAdi = isNull(havuzAdi);
	}
	public Long getHavuzSure() {
		return havuzSure;
	}
	public void setHavuzSure(Long havuzSure) {
		this.havuzSure = havuzSure;
	} 

	
	public Integer gethCode() {
		return hCode;
	}
	public void sethCode() {
		this.hCode = (getTarih() + getId()).hashCode();
	}
	private String isNull(String s) {
		if (s != null && s.isEmpty()) {
			return "";	
		} else {
			return s;
		}
	}
	public String getKaydi_acan() {
		return kaydi_acan;
	}
	public void setKaydi_acan(String kaydi_acan) {
		this.kaydi_acan = kaydi_acan;
	}
	public String getAciklama() {
		return aciklama;
	}
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	public String getUkey() {
		return ukey;
	}
	public void setUkey(String ukey) {
		this.ukey = ukey;
	}
	public Integer getTip_level1() {
		return tip_level1;
	}
	public void setTip_level1(Integer tip_level1) {
		this.tip_level1 = tip_level1;
	}
	public Integer getTip_level2() {
		return tip_level2;
	}
	public void setTip_level2(Integer tip_level2) {
		this.tip_level2 = tip_level2;
	}
	public Integer getTip_level3() {
		return tip_level3;
	}
	public void setTip_level3(Integer tip_level3) {
		this.tip_level3 = tip_level3;
	}
	public Integer getIsEnc() {
		return isEnc;
	}
	public void setIsEnc(Integer isEnc) {
		this.isEnc = isEnc;
	}
	public String getCryp_aciklama() {
		return cryp_aciklama;
	}
	public void setCryp_aciklama(String cryp_aciklama) {
		this.cryp_aciklama = cryp_aciklama;
	}
	
	public String getLokasyon() {
		return lokasyon;
	}
	public void setLokasyon(String lokasyon) {
		this.lokasyon = lokasyon;
	}	
	
	public String getKonuFull() {
		return konuFull;
	}
	public void setKonuFull(String konuFull) {
		this.konuFull = konuFull;
	}
	
	public String getisOnayBekle() {
		return isOnayBekle;
	}
	public void setisOnayBekle(String isOnayBekle) {
		this.isOnayBekle = isOnayBekle;
	}

	public Integer getfkGorevStatu() {
		return fkGorevStatu;
	}
	public void setfkGorevStatu(Integer fkGorevStatu) {
		this.fkGorevStatu = fkGorevStatu;
	}
	public Integer getfkStatu() {
		return fk_statu;
	}
	public void setfkStatu(Integer fk_statu) {
		this.fk_statu = fk_statu;
	}
	
	public Integer getfkIl() {
		return fkIl;
	}
	public void setfkIl(Integer fkIl) {
		this.fkIl = fkIl;
	}
	
	public Integer getfkHavuz() {
		return fkHavuz;
	}
	public void setfkHavuz(Integer fkHavuz) {
		this.fkHavuz = fkHavuz;
	}
	
	public Integer getfkSube() {
		return fkSube;
	}
	public void setfkSube(Integer fkSube) {
		this.fkSube = fkSube;
	}

	public String getaramasebebi() {
		return aramasebebi;
	}
	public void setaramasebebi(String aramasebebi) {
		this.aramasebebi = aramasebebi;
	}

	public Integer getf_aramasebebi() {
		return fk_aramasebebi;
	}
	public void setf_aramasebebi(Integer fk_aramasebebi) {
		this.fk_aramasebebi = fk_aramasebebi;
	}


	public Integer get_fkProje() {
		return fk_proje;
	}
	public void set_fkProje(Integer fk_proje) {
		this.fk_proje = fk_proje;
	}
	
	public String getaramakanalituru() {
		return aramakanalituru;
	}
	public void setaramakanalituru(String aramakanalituru) {
		this.aramakanalituru = aramakanalituru;
	}
	
	public String getaramakanali() {
		return aramakanali;
	}
	public void setaramakanali(String aramakanali) {
		this.aramakanali = aramakanali;
	}

	public Long getf_id() {
		return f_id;
	}
	public void setf_id(Long f_id) {
		this.f_id = f_id;
	}

	public String getf_adi() {
		return f_adi;
	}
	public void setf_adi(String f_adi) {
		this.f_adi = f_adi;
	}

	public String getf_adres() {
		return f_adres;
	}
	public void setf_adres(String f_adres) {
		this.f_adres = f_adres;
	}

	public String getf_tel() {
		return f_tel;
	}
	public void setf_tel(String f_tel) {
		this.f_tel = f_tel;
	}

	public String getnace() {
		return nace;
	}
	public void setnace(String nace) {
		this.nace = nace;
	}

	public String getscs() {
		return scs;
	}
	public void setscs(String scs) {
		this.scs = scs;
	}

	public String getf_il() {
		return f_il;
	}
	public void setf_il(String f_il) {
		this.f_il = f_il;
	}

	public String getf_ilce() {
		return f_ilce;
	}
	public void setf_ilce(String f_ilce) {
		this.f_ilce = f_ilce;
	}
	public String getSubeAdi() {
		return subeAdi;
	}
	public void setSubeAdi(String subeAdi) {
		this.subeAdi = subeAdi;
	}
}

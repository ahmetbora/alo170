package tr.com.saphira.alo170kurum.BILDIRIM;

public class vVTMESAJ {
	private Integer id;
	private String 	mesaj;
	private Integer fk_gonderen;
	private Integer fk_alici;
	private Integer isActive;
	private Integer fk_parent;
	private Integer fk_root;
	private Integer isroot;
	private String	tarih;
	private String 	okunma_tarihi;
	private String	alici;
	private String	gonderen;
	private String	kullanici;
	private Integer isReplied;
	
	public vVTMESAJ() {
		super();
		
	}
	public String getmesaj() {
		return mesaj;
	}
	public void setmesaj(String mesaj) {
		this.mesaj = mesaj;
	}

	public String gettarih() {
		return tarih;
	}
	public void settarih(String tarih) {
		this.tarih = tarih;
	}
	
	public String getokunma_tarihi() {
		return okunma_tarihi;
	}
	public void setokunma_tarihi(String okunma_tarihi) {
		this.okunma_tarihi = okunma_tarihi;
	}
	
	public Integer getid() {
		return id;
	}
	public void setid(Integer id) {
		this.id = id;
	}
	
	public Integer getfk_gonderen() {
		return fk_gonderen;
	}
	public void setfk_gonderen(Integer fk_gonderen) {
		this.fk_gonderen = fk_gonderen;
	}
	
	public Integer getfk_alici() {
		return fk_alici;
	}
	public void setfk_alici(Integer fk_alici) {
		this.fk_alici = fk_alici;
	}
	
	public Integer getisActive() {
		return isActive;
	}
	public void setisActive(Integer isActive) {
		this.isActive = isActive;
	}
	
	public Integer getfk_parent() {
		return fk_parent;
	}
	public void setfk_parent(Integer fk_parent) {
		this.fk_parent = fk_parent;
	}
	
	public Integer getfk_root() {
		return fk_root;
	}
	public void setfk_root(Integer fk_root) {
		this.fk_root = fk_root;
	}
	
	public Integer getisroot() {
		return isroot;
	}
	public void setisroot(Integer isroot) {
		this.isroot= isroot;
	}
	
	public String geAlici() {
		return alici;
	}
	public void setAlici(String alici) {
		this.alici = alici;
	}
	public String getGonderen() {
		return gonderen;
	}
	public void setGonderen(String gonderen) {
		this.gonderen = gonderen;
	}
	public String getKullanici() {
		return kullanici;
	}
	public void setKullanici(String kullanici) {
		this.kullanici = kullanici;
	}
	
	public Integer getIsReplied() {
		return isReplied;
	}
	public void setIsReplied(Integer isReplied) {
		this.isReplied= isReplied;
	}

}

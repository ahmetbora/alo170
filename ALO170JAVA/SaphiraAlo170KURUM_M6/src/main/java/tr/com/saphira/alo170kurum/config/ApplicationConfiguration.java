package tr.com.saphira.alo170kurum.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@EnableWebMvc
@Configuration
public class ApplicationConfiguration implements WebMvcConfigurer {

	@Bean
    public ViewResolver jspViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setPrefix("/WEB-INF/jsp/");
        bean.setSuffix(".jsp");
        return bean;
    }
    
	
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/pdfs/**")
                .addResourceLocations("/WEB-INF/pdfs/");
        
        registry.addResourceHandler("/js/**")
        		.addResourceLocations("/WEB-INF/js/").setCachePeriod(1200);
        
        registry.addResourceHandler("/assets/**")
                .addResourceLocations("/WEB-INF/assets/").setCachePeriod(1200);
        
        registry.addResourceHandler("/tools/**")
        		.addResourceLocations("/WEB-INF/tools/").setCachePeriod(1200);
        
        registry.addResourceHandler("/html/**")
        		.addResourceLocations("/WEB-INF/html/").setCachePeriod(1200);
        
        registry.addResourceHandler("/webjars/**")
        		.addResourceLocations("/webjars/");
    }

}


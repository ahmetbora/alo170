package tr.com.saphira.alo170kurum.AYARLAR;

public class vALTKURUMLAR {

	private Integer id;
	private Integer fk_parent;
	private String adi;
	private String kurumadi;
	private String email;
	private String durum;
	private Integer isActive;

	public vALTKURUMLAR() {
		super();
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public String getKurumadi() {
		return kurumadi;
	}
	public void setKurumadi(String kurumadi) {
		this.kurumadi = kurumadi;
	}
	public Integer getFk_parent() {
		return fk_parent;
	}
	public void setFk_parent(Integer fk_parent) {
		this.fk_parent = fk_parent;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDurum() {
		return durum;
	}
	public void setDurum(String durum) {
		this.durum = durum;
	}
	
	
}

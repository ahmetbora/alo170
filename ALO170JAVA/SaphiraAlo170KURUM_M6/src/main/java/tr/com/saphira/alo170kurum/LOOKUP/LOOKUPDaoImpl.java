package tr.com.saphira.alo170kurum.LOOKUP;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Null;
import javax.websocket.OnClose;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tr.com.saphira.alo170kurum.AYARLAR.vALTKURUMLAR;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKANALI;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKONUSU;
import tr.com.saphira.alo170kurum.AYARLAR.vILCELER;
import tr.com.saphira.alo170kurum.AYARLAR.vILLER;
import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.AYARLAR.vKURUMLAR;
import tr.com.saphira.alo170kurum.AYARLAR.vOZELNUMARALAR;
import tr.com.saphira.alo170kurum.AYARLAR.vULKELER;
import tr.com.saphira.alo170kurum.model.Lookup;

@Transactional
@Repository
public class LOOKUPDaoImpl {
	@Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;
	 
	 
	 
	 public List<Lookup> getServer() {
		  String sql1 = "call spActigimBildirimler (?)";

		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 9220 });
		  for (Map<?, ?> row : rows) {
				
				System.out.println(row.get("basvuru_sahibi"));
			
		  }
		  return null;
		  
	 }
	 

	 
	 public vULKELER getUlkelerById(Integer ID) {
		  String sql1 = "call cmbUlkeler (?)";
		  
		  Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });
		  
		  vULKELER v = new vULKELER();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
		try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
		
	
		  return v;
		  
	 }
	 
	 public List<vULKELER> getUlkeler(Integer ID) {
		  String sql1 = "call cmbUlkeler (?)";
		  List<vULKELER> ret = new ArrayList<vULKELER>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  vULKELER v = new vULKELER();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 public void setUlkeler(Integer ID, String ADI, Integer ISACTIVE) {
		  String sql1 = "call cmbSETUlkeler (?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, ADI, ISACTIVE });
		  
	 }
	 
	 
	 
	 public vILLER getIllerById(Integer ID) {
		  String sql1 = "call cmbIller (?)";
		  
		  Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });
		  
		  vILLER v = new vILLER();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setFk_ulke((Integer)row.get("fk_ulke"));
		} catch (Exception ex) {
			 System.out.println("fk_ulke Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		
	
		  return v;
		  
	 }
	 
	/**
	 * 
	 * @param ID
	 * @param FKULKE
	 * @param ADI
	 * @param IDS virgülle ayrılmış id ler 0 olarak gonderilirse gözardı edilir
	 * @return
	 */
	 public List<vILLER> getIllerByUlke(Integer ID,Integer FKULKE,String ADI,String IDS) {
		  String sql1 = "call cmbIllerByUlke (?,?,?,?)";
		  List<vILLER> ret = new ArrayList<vILLER>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID, FKULKE, ADI,IDS });
		  for (Map<?, ?> row : rows) {
				
			  vILLER v = new vILLER();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setFk_ulke((Integer)row.get("fk_ulke"));
		} catch (Exception ex) {
			 System.out.println("fk_ulke Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	} 
		  return ret;
		  
	 }
	 
	 public List<vILLER> getIller(Integer ID) {
		  String sql1 = "call cmbIller (?)";
		  List<vILLER> ret = new ArrayList<vILLER>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  vILLER v = new vILLER();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setFk_ulke((Integer)row.get("fk_ulke"));
		} catch (Exception ex) {
			 System.out.println("fk_ulke Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 public void setIller(Integer ID, String ADI, Integer ULKE, Integer ISACTIVE) {
		  String sql1 = "call cmbSETIller (?,?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, ADI,ULKE, ISACTIVE });
		  
	 }
	 
	 public vILCELER getIlcelerById(Integer ID) {
		  String sql1 = "call cmbIlcelerById (?)";
		  
		  Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });
		  
		  vILCELER v = new vILCELER();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setFk_il((Integer)row.get("fk_il"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		
	
		  return v;
		  
	 }
	 
	 public List<vILCELER> getIlceler(Integer IL) {
		  String sql1 = "call cmbIlceler (?)";
		  List<vILCELER> ret = new ArrayList<vILCELER>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { IL });
		  for (Map<?, ?> row : rows) {
				
			  vILCELER v = new vILCELER();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setFk_il((Integer)row.get("fk_il"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 public void setIlceler(Integer ID, String ADI, Integer IL, Integer ISACTIVE) {
		  String sql1 = "call cmbSETIlceler (?,?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, ADI,IL, ISACTIVE });
		  
	 }
	 

	 
	

	 
	 public Lookup getAramaKanaliById(Integer ID) {
		  String sql1 = "call cmbAramaKanali (?)";
		  
		  Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });
		  
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		  return v;
	 }
	 
	 public List<Lookup> getAramaKanali(Integer ID) {
		  String sql1 = "call cmbAramaKanali (?)";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 public void setAramaKanali(Integer ID, String ADI, Integer ISACTIVE) {
		  String sql1 = "call cmbSETAramaKanali (?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, ADI, ISACTIVE });
		  
	 }
	 
	 public Lookup getEgitimDurumuById(Integer ID) {
		  String sql1 = "call cmbEgitimDurumu (?)";
		  
		  Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });
		  
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		
	
		  return v;
		  
	 }
	 
	 public List<Lookup> getEgitimDurumu(Integer ID) {
		  String sql1 = "call cmbEgitimDurumu (?)";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 public void setEgitimDurumu(Integer ID, String ADI, Integer ISACTIVE) {
		  String sql1 = "call cmbSETEgitimDurumu (?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, ADI, ISACTIVE });
		  
	 }
	 
	 public Lookup getGuvenlikSorusuById(Integer ID) {
		  String sql1 = "call cmbGuvenlikSorusu (?)";
		  
		  Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });
		  
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		
	
		  return v;
		  
	 }
	 
	 public List<Lookup> getGuvenlikSorusu(Integer ID) {
		  String sql1 = "call cmbGuvenlikSorusu (?)";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 public void setGuvenlikSorusu(Integer ID, String ADI, Integer ISACTIVE) {
		  String sql1 = "call cmbSETGuvenlikSorusu (?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, ADI, ISACTIVE });
		  
	 }
	 public List<Lookup> getAramaSebebi(Integer ID) {
		  String sql1 = "call cmbAramaSebebi (?)";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	  
	 
	 public List<Lookup> getAramaKanaliTipi() {
		  String sql1 = "call cmbAramaKanaliTipi ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 public List<Lookup> getTaskStatu(Integer id) {
		  String sql1 = "call cmbTaskStatu(?)";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { id });
		for (Map<?, ?> row : rows) {
			Lookup v = new Lookup();
			try {		
				v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				System.out.println("adi Hata :" + ex.toString());
			}
				
			ret.add(v);
		}
		  return ret;
		  
	 }
	 
	 public List<Lookup> cmbgetHavuzlar(Integer AGENT) {
		  String sql1 = "call cmbHavuzlar (?) ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { AGENT });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 public List<Lookup> cmbgetHavuzlarSube(Integer SUBE) {
		  String sql1 = "call cmbHavuzlarSube (?) ";

		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { SUBE });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 
	 /******
	  * 
	  * @param ID rol id 
	  * @param isSysAdmin 1 ve 0 değeri alır. mevcut kullanıcı sistem admin ise diğer adminleri gorur
	  * @param tip	1 aktif, 0 pasif, 2 tümü olarak filtreler. 
	  * @return
	  */
	 public List<Lookup> getRoller(Integer ID,Integer isSysAdmin,Integer tip,Integer level,Integer userId) {
		  String sql1 = "call spGetCrmRolListe (?,?,?,?,?) ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID,isSysAdmin,tip,level,userId });
		  for (Map<?, ?> row : rows) {
				
			 Lookup v = new Lookup();
			try {		
					v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				 System.out.println("adi Hata :" + ex.toString());
			}
			try {		
				v.setfk_rol((Integer)row.get("fk_rol"));
			} catch (Exception ex) {
				 System.out.println("fk_rol Hata :" + ex.toString());
			}
				
			ret.add(v);
		  }
		  return ret;
		  
	 }
	 
	 
	 public List<Lookup> getRollerUser(Integer userId) {
		String sql1 = "call spGetCrmRolListeuser (?) ";
		List<Lookup> ret = new ArrayList<Lookup>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {userId });
		for (Map<?, ?> row : rows) {
			  
		   Lookup v = new Lookup();
		  try {		
				  v.setId((Integer)row.get("fk_rol"));
		  } catch (Exception ex) {
			   System.out.println("id Hata :" + ex.toString());
		  }try {		
			  v.setAdi(row.get("adi").toString());
		  } catch (Exception ex) {
			   System.out.println("adi Hata :" + ex.toString());
		  }
		  try {		
			  v.setfk_rol((Integer)row.get("fk_rol"));
		  } catch (Exception ex) {
			   System.out.println("fk_rol Hata :" + ex.toString());
		  }
			  
		  ret.add(v);
		}
		return ret;
		
   }
	 public List<Lookup> getCinsiyet() {
		  String sql1 = "call cmbCinsiyet ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 

	 public List<Lookup> getGeriDonusKanali() {
		  String sql1 = "call cmbGeriDonusKanali ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 
	 /******
	  * ARAMA KANALI TÜRLERİ ALTINDAKİ ARAMA KANALLARI LİSTESİ
	  * @param ID
	  * @return
	  */
	 public List<vARAMAKANALI> getAramaKanaliByTip(Integer ID) {
		  String sql1 = "call cmbAramaKanaliDetay (?)";
		  List<vARAMAKANALI> ret = new ArrayList<vARAMAKANALI>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  vARAMAKANALI v = new vARAMAKANALI();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setFk_parent_int((Integer)row.get("fk_parent_int"));
		} catch (Exception ex) {
			 System.out.println("fk_parent_int Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	} 
		  return ret;
		  
	 }
	 
	 

	 public List<vKURUMLAR> getKurumlar(Integer ID,String TERM,Integer skip, Integer take) {
		  String sql1 = "call tnmKurumlar (?,?,?,?)";
		  List<vKURUMLAR> ret = new ArrayList<vKURUMLAR>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID,TERM, skip,  take });
		  for (Map<?, ?> row : rows) {
			  vKURUMLAR v = new vKURUMLAR();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}try {		
			v.setEmail(row.get("email").toString());
		} catch (Exception ex) {
			 System.out.println("email Hata :" + ex.toString());
		}
			
		ret.add(v);
	} 
		  return ret;
		  
	 }
	 
	 
	 public List<vALTKURUMLAR> getAltKurumlar(Integer ID) {
		  String sql1 = "call cmbAltKurumlar (?)";
		  List<vALTKURUMLAR> ret = new ArrayList<vALTKURUMLAR>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  vALTKURUMLAR v = new vALTKURUMLAR();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}try {		
			v.setEmail(row.get("email").toString());
		} catch (Exception ex) {
			 System.out.println("email Hata :" + ex.toString());
		}
			
		ret.add(v);
	} 
		  return ret;
		  
	 }
	 
	 
	 
	 public List<vARAMAKONUSU> getAramaKonulari(Integer ID) {
		  String sql1 = "call tnmAramaKonusuFiltre (?)";
		  List<vARAMAKONUSU> ret = new ArrayList<vARAMAKONUSU>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  vARAMAKONUSU v = new vARAMAKONUSU();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}
			
		ret.add(v);
	} 
		  return ret;
		  
	 }
	 
	 
	 public List<Lookup> getSektorler() {
		  String sql1 = "call cmbSektor ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 
	 
	 public List<vOZELNUMARALAR> getOzelNumaralar(String NO) {
		  String sql1 = "call ozelNumaralar (?)";
		  List<vOZELNUMARALAR> ret = new ArrayList<vOZELNUMARALAR>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { NO });
		  for (Map<?, ?> row : rows) {
				
			  vOZELNUMARALAR v = new vOZELNUMARALAR();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setNumara(row.get("numara").toString());
		} catch (Exception ex) {
			 System.out.println("numara Hata :" + ex.toString());
		}try {		
			v.setAciklama(row.get("aciklama").toString());
		} catch (Exception ex) {
			 System.out.println("Aciklama Hata :" + ex.toString());
		}
		try {		
			v.setFkEkleyen((Integer)row.get("fk_ekleyen"));
		} catch (Exception ex) {
			 System.out.println("fk_ekleyen Hata :" + ex.toString());
		}
			
		ret.add(v);
	} 
		  return ret;
		  
	 }

	 
	 public List<Lookup> getBilgiBankasiKonular() {
		  String sql1 = "call cmbBilgiBankasiKonular ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows;
		  try {
			  rows = jdbcro.queryForList(sql1);
			  for (Map<?, ?> row : rows) {
					
				Lookup v = new Lookup();
				try {		
						v.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					 System.out.println("id Hata :" + ex.toString());
				}try {		
					v.setAdi(row.get("adi").toString());
				} catch (Exception ex) {
					 System.out.println("adi Hata :" + ex.toString());
				}
					
				ret.add(v);
			  }			
			} catch (Exception e) {
		
			}

		  return ret;
		  
	 }
	 
	 
	 public List<Lookup> getLokasyon() {
		  String sql1 = "call spGetLokasyon() ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		  for (Map<?, ?> row : rows) {
				
			Lookup v = new Lookup();
			try {		
					v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				 System.out.println("adi Hata :" + ex.toString());
			}
				
			ret.add(v);
		  }
		  return ret;
		  
	 }
	 public Map<String, Object> getLokasyonById(Integer id) {
		  String sql1 = "call spGetLokasyonById(?) ";
		  Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { id });
		  return row;
	 }
	 

	 public List<Lookup> getSubeler(Integer id,Integer fk_konu,String term,Integer fk_il, String ids, Integer wherein) {
		String sql1 = "call tnmSubelerFiltre(?,?,?,?,?,?) ";
		List<Lookup> ret = new ArrayList<Lookup>();
			List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {id,fk_konu,term,fk_il, ids, wherein});
			for (Map<?, ?> row : rows) {
				
			Lookup v = new Lookup();
			try {		
					v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				System.out.println("adi Hata :" + ex.toString());
			}
				
			ret.add(v);
			}
		return ret;
		
   }
	 public List<Lookup> getTaskHavuzKullanici(Integer fk_havuz) {
		String sql1 = "call spGetTaskHavuzKullanici(?,?,?,?,?,?,?) ";
		List<Lookup> ret = new ArrayList<Lookup>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {0,10,"",fk_havuz,0,0,0});
		for (Map<?, ?> row : rows) {
			  
		  Lookup v = new Lookup();
		  try {		
				  v.setId((Integer)row.get("userid"));
		  } catch (Exception ex) {
			   System.out.println("userid Hata :" + ex.toString());
		  }try {		
			  v.setAdi(row.get("useradi").toString());
		  } catch (Exception ex) {
			   System.out.println("useradi Hata :" + ex.toString());
		  }
			  
		  ret.add(v);
		}
		return ret;
		
   }
   	public List<Lookup> getTaskbKapanmaStatu(Integer id){
		String sql1 = "call spGetTaskbKapanmaStatu(?) ";
		List<Lookup> ret = new ArrayList<Lookup>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {id});
		for (Map<?, ?> row : rows) {
			  
		  Lookup v = new Lookup();
		  try {		
				  v.setId((Integer)row.get("id"));
		  } catch (Exception ex) {
			   System.out.println("id Hata :" + ex.toString());
		  }try {		
			  v.setAdi(row.get("adi").toString());
		  } catch (Exception ex) {
			   System.out.println("adi Hata :" + ex.toString());
		  }
			  
		  ret.add(v);
		}
		return ret;
		
   }
   	public List<Lookup> getSssKonu(){
		String sql1 = "call spGetSssKonu() ";
		List<Lookup> ret = new ArrayList<Lookup>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		for (Map<?, ?> row : rows) {
			  
		  Lookup v = new Lookup();
		  try {		
				  v.setId((Integer)row.get("id"));
		  } catch (Exception ex) {
			   System.out.println("id Hata :" + ex.toString());
		  }try {		
			  v.setAdi(row.get("adi").toString()+" / "+ row.get("lng").toString());
		  } catch (Exception ex) {
			   System.out.println("adi Hata :" + ex.toString());
		  }
			  
		  ret.add(v);
		}
		return ret;
		
   }
   	public List<Lookup> getSssAltKonu(Integer fk_sss_grup){
		String sql1 = "call spGetSssAltKonu(?)";
		List<Lookup> ret = new ArrayList<Lookup>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {fk_sss_grup});
		for (Map<?, ?> row : rows) {
			  
		  Lookup v = new Lookup();
		  try {		
				  v.setId((Integer)row.get("id"));
		  } catch (Exception ex) {
			   System.out.println("id Hata :" + ex.toString());
		  }try {		
			  v.setAdi(row.get("adi").toString());
		  } catch (Exception ex) {
			   System.out.println("adi Hata :" + ex.toString());
		  }
			  
		  ret.add(v);
		}
		return ret;
		
   }

   public List<Lookup> getVardiyaTipleri(String ID,String Tip){
	String sql1 = "call spGetVardiyaTipleri(?,?)";
	List<Lookup> ret = new ArrayList<Lookup>();
	List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {ID,Tip});
	for (Map<?, ?> row : rows) {
		  
	  Lookup v = new Lookup();
	  try {		
			  v.setId((Integer)row.get("id"));
	  } catch (Exception ex) {
		   System.out.println("id Hata :" + ex.toString());
	  }try {		
		  v.setAdi(row.get("adi").toString());
	  } catch (Exception ex) {
		   System.out.println("adi Hata :" + ex.toString());
	  }
		  
	  ret.add(v);
	}
	return ret;
	
}

	public List<Map<String, Object>>  getCmbDetayWhere(String table,String where){
		String sql1 = "call spGetCmbDetayWhere(?,?)";
	
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {table,where});
		return rows;
	}



	public List<Map<String, Object>>  getUsers(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon) {
		// System.out.println("call appUserList ('"+ADI+"','"+EMAIL+"','"+ROL+"',"+isActive+","+fk_lokasyon+","+skip+","+skip+")");
			String sql1 = "call appUserList (?,?,?,?,?,?,?)";
			List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ADI, EMAIL, ROL, isActive,fk_lokasyon,0,0 });
			List<Map<String,Object>> rowRet=new ArrayList<>();
			for (Map<String,Object> map : rows) {
				Map<String,Object> row=new HashMap<>();   
				row.put("adi",map.get("adi"));
				row.put("id",map.get("id"));
				rowRet.add(row);
			}
			return rowRet;
	}
	public List<Map<String, Object>>  getUsersByUserHavuz(Integer fk_user){
		// System.out.println("call appUserList ('"+ADI+"','"+EMAIL+"','"+ROL+"',"+isActive+","+fk_lokasyon+","+skip+","+skip+")");
			String sql1 = "call cmbGetUsersByUserHavuz (?)";
			List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { fk_user});
			List<Map<String,Object>> rowRet=new ArrayList<>();
			for (Map<String,Object> map : rows) {
				Map<String,Object> row=new HashMap<>();   
				row.put("adi",map.get("adi"));
				row.put("id",map.get("id"));
				rowRet.add(row);
			}
			return rowRet;
	}

	 /*public List<Lookup> getSubeler(Integer ID,Integer KONU,String TERM,Integer FKIL,String IDS,Integer WHEREIN) {
		  String sql1 = "call tnmSubelerFiltre(?,?,?,?,?,?) ";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID, KONU, TERM, FKIL, IDS, WHEREIN });
		  for (Map<?, ?> row : rows) {
				
			Lookup v = new Lookup();
			try {		
					v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				 System.out.println("adi Hata :" + ex.toString());
			}
				
			ret.add(v);
		  }
		  return ret;
		  
	 }*/
	 
	 
	 class UserRowMapper implements RowMapper<Object>{

		  @Override
		  public Lookup mapRow(ResultSet rs, int rowNum) throws SQLException {
			  Lookup lookup = new Lookup();
		   lookup.setId((Integer)rs.getInt("id"));
		   lookup.setAdi(rs.getString("adi"));
		   	System.out.println(rs.getString("adi"));
		   return lookup;
		  }
		  
	}
	 
	 
	
	 

}

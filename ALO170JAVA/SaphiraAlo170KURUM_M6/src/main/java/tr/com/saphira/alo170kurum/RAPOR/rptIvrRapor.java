package tr.com.saphira.alo170kurum.RAPOR;

public class rptIvrRapor {
    private Integer id;
    private String katilmayan;
    private String olumlu;
    private String olumsuz;
    private String s1olumlu;
    private String s2olumlu;
    private String s3olumlu;
    private String s4olumlu;
    private String s5olumlu;
    private String s1olumsuz;
    private String s2olumsuz;
    private String s3olumsuz;
    private String s4olumsuz;
    private String s5olumsuz;



    public rptIvrRapor() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getkatilmayan(){
        return katilmayan;
    }
    public void setkatilmayan(String katilmayan){
        this.katilmayan=katilmayan;
    }

    public String getolumlu(){
        return olumlu;
    }
    public void setolumlu(String olumlu){
        this.olumlu=olumlu;
    }

    public String getolumsuz(){
        return olumsuz;
    }
    public void setolumsuz(String olumsuz){
        this.olumsuz=olumsuz;
    }

    public String gets1olumlu(){
        return s1olumlu;
    }
    public void sets1olumlu(String s1olumlu){
        this.s1olumlu=s1olumlu;
    }

    public String gets2olumlu(){
        return s2olumlu;
    }
    public void sets2olumlu(String s2olumlu){
        this.s2olumlu=s2olumlu;
    }

    public String gets3olumlu(){
        return s3olumlu;
    }
    public void sets3olumlu(String s3olumlu){
        this.s3olumlu=s3olumlu;
    }

    public String gets4olumlu(){
        return s4olumlu;
    }
    public void sets4olumlu(String s4olumlu){
        this.s4olumlu=s4olumlu;
    }

    public String gets5olumlu(){
        return s5olumlu;
    }
    public void sets5olumlu(String s5olumlu){
        this.s5olumlu=s5olumlu;
    }

    public String gets1olumsuz(){
        return s1olumsuz;
    }
    public void sets1olumsuz(String s1olumsuz){
        this.s1olumsuz=s1olumsuz;
    }

    public String gets2olumsuz(){
        return s2olumsuz;
    }
    public void sets2olumsuz(String s2olumsuz){
        this.s2olumsuz=s2olumsuz;
    }

    public String gets3olumsuz(){
        return s3olumsuz;
    }
    public void sets3olumsuz(String s3olumsuz){
        this.s3olumsuz=s3olumsuz;
    }

    public String gets4olumsuz(){
        return s4olumsuz;
    }
    public void sets4olumsuz(String s4olumsuz){
        this.s4olumsuz=s4olumsuz;
    }

    public String gets5olumsuz(){
        return s5olumsuz;
    }
    public void sets5olumsuz(String s5olumsuz){
        this.s5olumsuz=s5olumsuz;
    }


}


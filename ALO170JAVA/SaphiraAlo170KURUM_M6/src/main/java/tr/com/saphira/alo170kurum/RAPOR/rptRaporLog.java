package tr.com.saphira.alo170kurum.RAPOR;

public class rptRaporLog {
    private Integer id;
    private String tarih;
    private String rapor;
    private String kullanici;




    public rptRaporLog() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String getrapor(){
        return rapor;
    }
    public void setrapor(String rapor){
        this.rapor=rapor;
    }

    public String getkullanici(){
        return kullanici;
    }
    public void setkullanici(String kullanici){
        this.kullanici=kullanici;
    }

}


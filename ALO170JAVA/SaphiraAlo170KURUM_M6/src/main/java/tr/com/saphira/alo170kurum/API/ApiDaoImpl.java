package tr.com.saphira.alo170kurum.API;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class ApiDaoImpl implements ApiDao {
	
	 @Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;

	@Override
	public Dashboard bugunAcilan() {
		
		  String sql1 = "call getSTATS ";
		  
		  Map<String, Object>  row = jdbcro.queryForMap(sql1);
		  
		  Dashboard v = new Dashboard();
		try {		
				v.setTask_acilan((Integer)row.get("task_acilan"));
		} catch (Exception ex) {
			 System.out.println("task_acilan Hata :" + ex.toString());
		}try {		
			v.setTask_agent_kapanan((Integer)row.get("task_agent_kapanan"));
		} catch (Exception ex) {
			 System.out.println("task_agent_kapanan Hata :" + ex.toString());
		}try {		
			v.setTask_kapanan((Integer)row.get("task_kapanan"));
		} catch (Exception ex) {
			 System.out.println("task_kapanan Hata :" + ex.toString());
		}try {		
			v.setTask_yeniden_acilan((Integer)row.get("task_yeniden_acilan"));
		} catch (Exception ex) {
			 System.out.println("task_yeniden_acilan Hata :" + ex.toString());
		}try {		
			v.setPbx_gelen((Integer)row.get("pbx_gelen"));
		} catch (Exception ex) {
			 System.out.println("pbx_gelen Hata :" + ex.toString());
		}try {		
			v.setPbx_bekl((Integer)row.get("pbx_bekl"));
		} catch (Exception ex) {
			 System.out.println("pbx_bekl Hata :" + ex.toString());
		}try {		
			v.setPbx_kayip((Integer)row.get("pbx_kayip"));
		} catch (Exception ex) {
			 System.out.println("pbx_kayip Hata :" + ex.toString());
		}try {		
			v.setPbx_gkayip((Integer)row.get("pbx_gkayip"));
		} catch (Exception ex) {
			 System.out.println("pbx_gkayip Hata :" + ex.toString());
		}
	
		  return v;
	}

}

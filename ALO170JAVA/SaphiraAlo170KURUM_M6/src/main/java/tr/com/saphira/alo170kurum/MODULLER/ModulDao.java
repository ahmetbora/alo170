package tr.com.saphira.alo170kurum.MODULLER;

import java.util.List;
import java.util.Map;

import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.BILDIRIM.vJSONRESULT;
import tr.com.saphira.alo170kurum.RAPOR.rptKurumSubeIstatistik;
import tr.com.saphira.alo170kurum.KART.vKART;
import tr.com.saphira.alo170kurum.SCRIPT.vSCRIPT;
import tr.com.saphira.alo170kurum.SSS.vSSS;
import tr.com.saphira.alo170kurum.model.vTotal;


@SuppressWarnings("unused")
public interface ModulDao {
    
	public vJSONRESULT queMudahale(String ext,Integer userId,String uzunluk); 
	public List<Map<String, Object>> AgentHangup(String sdate,String fdate,Integer lokasyon,String tablo);
	public List<Map<String, Object>> AgentHangupDetay(String sdate,String fdate,Integer lokasyon,String tablo,String ext);
	public List<Map<String, Object>> IvrSms(String tc);
	public vJSONRESULT IvrSmsIptal(String tc,Integer userId);
	public List<Map<String, Object>> getsss(Integer id,Integer statu,Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,Integer skip,Integer take);
	public List<vTotal> countsss(Integer statu,Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik );
	public vJSONRESULT sssGuncelle(Integer statu,Integer isWeb, Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,String aciklama,Integer userId,Integer id);
	public vJSONRESULT sssKaydet(Integer statu,Integer isWeb, Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,String aciklama,Integer userId);
	public List<Map<String, Object>> KaraListe(Integer id,Integer isOnay,Integer skip,Integer take);
	public List<vTotal> countKaraListe(Integer isOnay);	
	public vJSONRESULT KaraListeOnay(Integer id,String tarih,Integer userId);
	public vJSONRESULT KaraListeSuresiz(Integer id,Integer userId);
	public vJSONRESULT KaraListeSil(Integer id,Integer userId);
	public List<Map<String, Object>> VipListe(Integer skip,Integer take);
	public List<vTotal> countVipListe();	
	public vJSONRESULT VipListeSil(Integer id);
	public List<Map<String, Object>> MobilUyeListe(Integer statu,String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer skip,Integer take);
	public List<vTotal> countMobilUyeListe(Integer statu,String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce);
	public List<Map<String, Object>> VatandasListe(String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer skip,Integer take,Integer kayit_tipi);
	public List<vTotal> countVatandasListe(String adi,String soaydi,String tc,String ssk,String tel,Integer fk_ulke,Integer fk_il,Integer fk_ilce,Integer kayit_tipi);
	public Map<String, Object> getVatandas(Integer id);
	public vJSONRESULT VatandasGuncelle(Integer userId,Integer id,String adi,String soyadi,String tckimklik,String dt,String tel,String gsm,Integer fk_egitim_durumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,String ssk,String email,Integer fk_cinsiyet,String fax);
	public vJSONRESULT VatandasOnayla(Integer id,Integer statu,String sifre);
	public List<Map<String, Object>> izinTalepSv(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip, Integer skip,Integer take);
	public List<vTotal> countizinTalepSv(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip);
	public List<Map<String, Object>> izinTalepTk(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip, Integer skip,Integer take);
	public List<vTotal> countizinTalepTk(Integer fk_lokasyon, Integer userId, Integer isActive, Integer tip);
	public List<Map<String, Object>> izinTalepVT(Integer userId, Integer isActive, Integer skip,Integer take);
	public List<vTotal> countizinTalepVT(Integer userId, Integer isActive);
	public List<Map<String, Object>> izinTalepAD(Integer userId, Integer skip,Integer take);
	public List<vTotal> countizinTalepAD(Integer userId);
	public vJSONRESULT izinTalepKaydet(String sdate, String ssaat,String fdate, String fsaat, Integer fk_sebep,String aciklama,Integer fk_agent,Integer fk_tipi,Integer fk_agent_,
	Integer fk_lokasyon,Integer admin_onay,Integer islem_admin,Integer tl_onay,Integer islem_tl,Integer sv_onay,Integer islem_sv,Integer isActive,String userFkRol
	);
	public List<Map<String, Object>> izinTalepOnayListe(Integer fk_rol,Integer fk_lokasyon,Integer skip,Integer take);
	public List<vTotal> countizinTalepOnayListe(Integer fk_rol,Integer fk_lokasyon);
	public vJSONRESULT izinTalepOnay(Integer userId,Integer id,Integer fk_rol,Integer onay);
	public List<Map<String, Object>> ScriptKonulari(Integer skip,Integer take);
	public List<vTotal> countScriptKonulari();
	public vJSONRESULT ScriptKonuUpdate(Integer id,String adi);
	public vJSONRESULT ScriptKonuInsert(String adi);
	public List<Map<String, Object>> ScriptListe(Integer skip,Integer take);
	public List<vTotal> countScriptListe();
	public Map<String, Object> ScriptDetay(Integer id);
	public vJSONRESULT ScriptKaydet(String fk_parent,String fk_aramasebebi,String tip_level1,String tip_level2,String tip_level3,String icerik,String aciklama,String id);
}
package tr.com.saphira.alo170kurum.RAPOR;

public class rptBildirimDetay {
	private int id;
    private String vatandas;
    private String tarih;
    private String agent;
    private String konu;
    private String il;
    private String statu;
    private String sure;
    private Integer fk_agent;
    private Integer fk_gorev_statu;
    private String statuClass;

    public rptBildirimDetay() {
		super();
	}

    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

    public String getVatandas(){
        return vatandas;
    }
    public void setVatandas(String vatandas){
        this.vatandas=vatandas;
    }

    public String getTarih(){
        return tarih;
    }
    public void setTarih(String tarih){
        this.tarih=tarih;
    }

    public String getAgent(){
        return agent;
    }
    public void setAgent(String agent){
        this.agent=agent;
    }

    public String getKonu(){
        return konu;
    }
    public void setKonu(String konu){
        this.konu=konu;
    }

    public String getIl(){
        return il;
    }
    public void setIl(String il){
        this.il=il;
    }

    public String getStatu(){
        return statu;
    }
    public void setStatu(String statu){
        this.statu=statu;
    }

    public String getSure(){
        return sure;
    }
    public void setSure(String sure){
        this.sure=sure;
    }

    public Integer getfk_agent(){
        return fk_agent;
    }
    public void setfk_agent(Integer fk_agent){
        this.fk_agent=fk_agent;
    }

    public Integer getfk_gorev_statu(){
        return fk_gorev_statu;
    }
    public void setfk_gorev_statu(Integer fk_gorev_statu){
        this.fk_gorev_statu=fk_gorev_statu;
    }

    public String getstatuClass(){
        return statuClass;
    }
    public void setstatuClass(String statuClass){
        this.statuClass=statuClass;
    }
}


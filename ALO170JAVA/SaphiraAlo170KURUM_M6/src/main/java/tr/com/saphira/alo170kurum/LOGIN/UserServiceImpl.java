package tr.com.saphira.alo170kurum.LOGIN;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.saphira.alo170kurum.model.SaphiraUsers;



@Service
public class UserServiceImpl implements UserService{

	UserDao userDao;
	
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@Override
	public List<SaphiraUsers> listAllusers() {
		
		return userDao.listAllusers();
	}

	@Override
	public void addUser(SaphiraUsers saphirausers) {
		
		
	}

	@Override
	public void updateUser(SaphiraUsers saphirausers) {
		
		
	}

	@Override
	public void deleteUser(int id) {
		
		
	}

	@Override
	public SaphiraUsers findUserById(int id) {
		
		return null;
	}

	@Override
	public User findUserByUserName(String username) {
		
		return userDao.findUserByUserName(username);
	}

	@Override
	public SaphiraUsers getLogin(String KEY) {
		return userDao.getLogin(KEY);
	}

	
}
package tr.com.saphira.alo170kurum.RAPOR;

public class rptMuAktivite {
    private Integer id;
    private String mu;
    private String tarih;
    private String islem;
    private String statu;
    private String aciklama;
    private String detayIslem;



    public rptMuAktivite() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getmu(){
        return mu;
    }
    public void setmu(String mu){
        this.mu=mu;
    }

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String getislem(){
        return islem;
    }
    public void setislem(String islem){
        this.islem=islem;
    }

    public String getstatu(){
        return statu;
    }
    public void setstatu(String statu){
        this.statu=statu;
    }

    public String getaciklama(){
        return aciklama;
    }
    public void setaciklama(String aciklama){
        this.aciklama=aciklama;
    }
    public String getdetayIslem(){
        return detayIslem;
    }
    public void setdetayIslem(String detayIslem){
        this.detayIslem=detayIslem;
    }


}


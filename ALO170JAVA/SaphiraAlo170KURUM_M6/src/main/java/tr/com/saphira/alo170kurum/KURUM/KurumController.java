package tr.com.saphira.alo170kurum.KURUM;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.BILDIRIM.BildirimService;

@Controller
@RequestMapping(value="/kurum")
@EnableWebSecurity()
public class KurumController {
	
	
	@Autowired
	BildirimService service;

	
	@RequestMapping(value="", method= RequestMethod.GET)
	public ModelAndView kurumhome(Integer ID, Integer CODE, HttpSession httpSession) {
		ModelAndView model = new ModelAndView();
		model.setViewName("home");
		return model;
	}
	
	@RequestMapping(value="bildirimdetay/{id}/{code}", method= RequestMethod.GET)
	public ModelAndView bildirimdetay(Integer ID, Integer CODE, HttpSession httpSession) {
		ModelAndView model = new ModelAndView("BILDIRIM/BildirimDetay");
		model.addObject("kart", service.getKart(56542, null));
		return model;
	}

}

package tr.com.saphira.alo170kurum.HOME;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.model.Lookup;

@Transactional
@Repository
public class HomeDaoImpl implements HomeDao {
	@Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;
	 
	 
	@Override
	public List<Lookup> SifrelenmemisTaskAciklamaList() {
		String sql1 = "call tempTaskSifresizAciklama ()";
		  List<Lookup> ret = new ArrayList<Lookup>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString().trim());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;
	}

	@Override
	public void TaskAciklamaSifrele(int id, String aciklama) {
		jdbcrw.update("call tempTaskAciklamaSifrele (?,?)", new Object[] {id, aciklama});	
		TaskDetaySifrele(id);
	}

	@Override
	public void TaskDetaySifrele(int TaskID) {
		
		String sql1 = "call tempTaskDetaySifresizAciklama (?)";
		 
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {TaskID });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
				try {		
						v.setId((Integer)row.get("id"));
						
						
					} catch (Exception ex) {
						 System.out.println("id Hata :" + ex.toString());
					}
				try {		
						v.setAdi( Genel.encrypt(row.get("adi").toString().trim()));
					} catch (Exception ex) {
						 System.out.println("adi Hata :" + ex.toString());
					}
				 
				System.out.println("---- " + v.getId());
				System.out.println(row.get("adi").toString());
				System.out.println("---- ");
				System.out.println(v.getAdi());
				System.out.println("============");
				System.out.println( Genel.decrypt( v.getAdi()));
				
				System.out.println("");
				System.out.println("");
				System.out.println("");
				System.out.println("");
				System.out.println("");
				
				
				jdbcrw.update("call tempTaskDetayAciklamaSifrele (?,?)", new Object[] {v.getId(), v.getAdi()});
				
		  }
		
	}

	@Override
	public void AllDetayUpdate() {
		String sql1 = "call tempAllDetaySifresiz()";
	
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { });
		  for (Map<?, ?> row : rows) {
				
			  Lookup v = new Lookup();
		try {		
				v.setId((Integer)row.get("id"));
				System.out.println(v.getId());
				
				TaskDetaySifrele(v.getId());
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}
			
		 
	}
		
	}

	@Override
	public DashBoard getDashboardData() {
		
		DashBoard d = new DashBoard();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
		   LocalDateTime now = LocalDateTime.now();  
		   System.out.println(); 
		   
		d.setBugun(getStat(dtf.format(now)));
		return d;
	}
	
	
	private STATS getStat(String tarih) {
		STATS s = new STATS();
		String sql1 = "call HOME_getStatsByDate (?)";
		  Map<String, Object>  rows = jdbcro.queryForMap(sql1,new Object[] { tarih });
		  s.setTarih((Date)rows.get("tarih"));
		  s.setAcilan((Integer)rows.get("task_acilan"));
		  s.setAgent_kapanan((Integer)rows.get("task_agent_kapanan"));
		  s.setTask_kapanan((Integer)rows.get("task_kapanan"));
		  s.setC0((Integer)rows.get("c0"));
		  s.setC1((Integer)rows.get("c1"));
		  s.setC2((Integer)rows.get("c2"));
		  s.setC3((Integer)rows.get("c3"));
		  s.setC4((Integer)rows.get("c4"));
		  s.setC5((Integer)rows.get("c5"));
		  s.setC6((Integer)rows.get("c6"));
		  s.setC7((Integer)rows.get("c7"));
		  s.setC8((Integer)rows.get("c8"));
		  s.setC9((Integer)rows.get("c9"));
		  s.setC10((Integer)rows.get("c10"));
		  s.setC11((Integer)rows.get("c11"));
		  s.setC12((Integer)rows.get("c12"));
		  s.setC13((Integer)rows.get("c13"));
		  s.setC14((Integer)rows.get("c14"));
		  s.setC15((Integer)rows.get("c15"));
		  s.setC16((Integer)rows.get("c16"));
		  s.setC17((Integer)rows.get("c17"));
		  s.setC18((Integer)rows.get("c18"));
		  s.setC19((Integer)rows.get("c19"));
		  s.setC20((Integer)rows.get("c20"));
		  s.setC21((Integer)rows.get("c21"));
		  s.setC22((Integer)rows.get("c22"));
		  s.setC23((Integer)rows.get("c23"));
		  
		  s.setO0((Integer)rows.get("o0"));
		  s.setO1((Integer)rows.get("o1"));
		  s.setO2((Integer)rows.get("o2"));
		  s.setO3((Integer)rows.get("o3"));
		  s.setO4((Integer)rows.get("o4"));
		  s.setO5((Integer)rows.get("o5"));
		  s.setO6((Integer)rows.get("o6"));
		  s.setO7((Integer)rows.get("o7"));
		  s.setO8((Integer)rows.get("o8"));
		  s.setO9((Integer)rows.get("o9"));
		  s.setO10((Integer)rows.get("o10"));
		  s.setO11((Integer)rows.get("o11"));
		  s.setO12((Integer)rows.get("o12"));
		  s.setO13((Integer)rows.get("o13"));
		  s.setO14((Integer)rows.get("o14"));
		  s.setO15((Integer)rows.get("o15"));
		  s.setO16((Integer)rows.get("o16"));
		  s.setO17((Integer)rows.get("o17"));
		  s.setO18((Integer)rows.get("o18"));
		  s.setO19((Integer)rows.get("o19"));
		  s.setO20((Integer)rows.get("o20"));
		  s.setO21((Integer)rows.get("o21"));
		  s.setO22((Integer)rows.get("o22"));
		  s.setO23((Integer)rows.get("o23"));	
		  
		  System.out.println("Acilan : " + s.getAcilan());
		  
		return s;
	}

	@Override
	public DashBoard getDataOzet(DashBoard d) {
		String sql1 = "call HOME_getDBOzet ()";
		  Map<String, Object>  rows = jdbcro.queryForMap(sql1);
		 d.setBildirim((Integer)rows.get("Bildirim"));
		 d.setAcik((Integer)rows.get("acik"));
		 d.setVatandas((Integer)rows.get("Kart"));
		 d.setDetay((Integer)rows.get("Detay"));
		return d;
	}

}

package tr.com.saphira.alo170kurum.HOME;

import java.util.Date;

public class STATS {
	private Date tarih;
	private int acilan;
	private int agent_kapanan;
	private int task_kapanan;
	private int task_yeniden_acilan;
	private int pbx_gelen;
	private int pbx_kayip;
	private int o0;
	private int o1;
	private int o2;
	private int o3;
	private int o4;
	private int o5;
	private int o6;
	private int o7;
	private int o8;
	private int o9;
	private int o10;
	private int o11;
	private int o12;
	private int o13;
	private int o14;
	private int o15;
	private int o16;
	private int o17;
	private int o18;
	private int o19;
	private int o20;
	private int o21;
	private int o22;
	private int o23;

	private int c0;
	private int c1;
	private int c2;
	private int c3;
	private int c4;
	private int c5;
	private int c6;
	private int c7;
	private int c8;
	private int c9;
	private int c10;
	private int c11;
	private int c12;
	private int c13;
	private int c14;
	private int c15;
	private int c16;
	private int c17;
	private int c18;
	private int c19;
	private int c20;
	private int c21;
	private int c22;
	private int c23;
	public STATS() {
		super();
		
	}
	public Date getTarih() {
		return tarih;
	}
	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}
	public int getAcilan() {
		return acilan;
	}
	public void setAcilan(int acilan) {
		this.acilan = acilan;
	}
	public int getAgent_kapanan() {
		return agent_kapanan;
	}
	public void setAgent_kapanan(int agent_kapanan) {
		this.agent_kapanan = agent_kapanan;
	}
	public int getTask_kapanan() {
		return task_kapanan;
	}
	public void setTask_kapanan(int task_kapanan) {
		this.task_kapanan = task_kapanan;
	}
	public int getTask_yeniden_acilan() {
		return task_yeniden_acilan;
	}
	public void setTask_yeniden_acilan(int task_yeniden_acilan) {
		this.task_yeniden_acilan = task_yeniden_acilan;
	}
	public int getPbx_gelen() {
		return pbx_gelen;
	}
	public void setPbx_gelen(int pbx_gelen) {
		this.pbx_gelen = pbx_gelen;
	}
	public int getPbx_kayip() {
		return pbx_kayip;
	}
	public void setPbx_kayip(int pbx_kayip) {
		this.pbx_kayip = pbx_kayip;
	}
	public int getO0() {
		return o0;
	}
	public void setO0(int o0) {
		this.o0 = o0;
	}
	public int getO1() {
		return o1;
	}
	public void setO1(int o1) {
		this.o1 = o1;
	}
	public int getO2() {
		return o2;
	}
	public void setO2(int o2) {
		this.o2 = o2;
	}
	public int getO3() {
		return o3;
	}
	public void setO3(int o3) {
		this.o3 = o3;
	}
	public int getO4() {
		return o4;
	}
	public void setO4(int o4) {
		this.o4 = o4;
	}
	public int getO5() {
		return o5;
	}
	public void setO5(int o5) {
		this.o5 = o5;
	}
	public int getO6() {
		return o6;
	}
	public void setO6(int o6) {
		this.o6 = o6;
	}
	public int getO7() {
		return o7;
	}
	public void setO7(int o7) {
		this.o7 = o7;
	}
	public int getO8() {
		return o8;
	}
	public void setO8(int o8) {
		this.o8 = o8;
	}
	public int getO9() {
		return o9;
	}
	public void setO9(int o9) {
		this.o9 = o9;
	}
	public int getO10() {
		return o10;
	}
	public void setO10(int o10) {
		this.o10 = o10;
	}
	public int getO11() {
		return o11;
	}
	public void setO11(int o11) {
		this.o11 = o11;
	}
	public int getO12() {
		return o12;
	}
	public void setO12(int o12) {
		this.o12 = o12;
	}
	public int getO13() {
		return o13;
	}
	public void setO13(int o13) {
		this.o13 = o13;
	}
	public int getO14() {
		return o14;
	}
	public void setO14(int o14) {
		this.o14 = o14;
	}
	public int getO15() {
		return o15;
	}
	public void setO15(int o15) {
		this.o15 = o15;
	}
	public int getO16() {
		return o16;
	}
	public void setO16(int o16) {
		this.o16 = o16;
	}
	public int getO17() {
		return o17;
	}
	public void setO17(int o17) {
		this.o17 = o17;
	}
	public int getO18() {
		return o18;
	}
	public void setO18(int o18) {
		this.o18 = o18;
	}
	public int getO19() {
		return o19;
	}
	public void setO19(int o19) {
		this.o19 = o19;
	}
	public int getO20() {
		return o20;
	}
	public void setO20(int o20) {
		this.o20 = o20;
	}
	public int getO21() {
		return o21;
	}
	public void setO21(int o21) {
		this.o21 = o21;
	}
	public int getO22() {
		return o22;
	}
	public void setO22(int o22) {
		this.o22 = o22;
	}
	public int getO23() {
		return o23;
	}
	public void setO23(int o23) {
		this.o23 = o23;
	}
	public int getC0() {
		return c0;
	}
	public void setC0(int c0) {
		this.c0 = c0;
	}
	public int getC1() {
		return c1;
	}
	public void setC1(int c1) {
		this.c1 = c1;
	}
	public int getC2() {
		return c2;
	}
	public void setC2(int c2) {
		this.c2 = c2;
	}
	public int getC3() {
		return c3;
	}
	public void setC3(int c3) {
		this.c3 = c3;
	}
	public int getC4() {
		return c4;
	}
	public void setC4(int c4) {
		this.c4 = c4;
	}
	public int getC5() {
		return c5;
	}
	public void setC5(int c5) {
		this.c5 = c5;
	}
	public int getC6() {
		return c6;
	}
	public void setC6(int c6) {
		this.c6 = c6;
	}
	public int getC7() {
		return c7;
	}
	public void setC7(int c7) {
		this.c7 = c7;
	}
	public int getC8() {
		return c8;
	}
	public void setC8(int c8) {
		this.c8 = c8;
	}
	public int getC9() {
		return c9;
	}
	public void setC9(int c9) {
		this.c9 = c9;
	}
	public int getC10() {
		return c10;
	}
	public void setC10(int c10) {
		this.c10 = c10;
	}
	public int getC11() {
		return c11;
	}
	public void setC11(int c11) {
		this.c11 = c11;
	}
	public int getC12() {
		return c12;
	}
	public void setC12(int c12) {
		this.c12 = c12;
	}
	public int getC13() {
		return c13;
	}
	public void setC13(int c13) {
		this.c13 = c13;
	}
	public int getC14() {
		return c14;
	}
	public void setC14(int c14) {
		this.c14 = c14;
	}
	public int getC15() {
		return c15;
	}
	public void setC15(int c15) {
		this.c15 = c15;
	}
	public int getC16() {
		return c16;
	}
	public void setC16(int c16) {
		this.c16 = c16;
	}
	public int getC17() {
		return c17;
	}
	public void setC17(int c17) {
		this.c17 = c17;
	}
	public int getC18() {
		return c18;
	}
	public void setC18(int c18) {
		this.c18 = c18;
	}
	public int getC19() {
		return c19;
	}
	public void setC19(int c19) {
		this.c19 = c19;
	}
	public int getC20() {
		return c20;
	}
	public void setC20(int c20) {
		this.c20 = c20;
	}
	public int getC21() {
		return c21;
	}
	public void setC21(int c21) {
		this.c21 = c21;
	}
	public int getC22() {
		return c22;
	}
	public void setC22(int c22) {
		this.c22 = c22;
	}
	public int getC23() {
		return c23;
	}
	public void setC23(int c23) {
		this.c23 = c23;
	}
	
	
	
}

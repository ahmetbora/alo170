package tr.com.saphira.alo170kurum.TASK;

import tr.com.saphira.alo170kurum.Genel;

public class TRANSFER {
	
	private int id;
	private int fk_gorev_statu;
	private int fk_part;
	private int fk_parent;
	private String aciklama;
	// private String cryp_aciklama;
	private String MU_aciklama;
	private String KU_aciklama;
	private String GD_aciklama;
	private int fk_kaydi_acan;
	private String tarih;
	private int fk_kapatan_agent;
	private String kapanma_tarihi;
	private int fk_kart;
	private int fk_statu;
	private int fk_havuz;
	private int fk_agent;
	private int fk_tip;
	private int fk_geri_donus_kanali;
	private int level;
	private int tip_level1;
	private int tip_level2;
	private int tip_level3;
	private int tip_level4;
	private int tip_level5;
	private String arayan_numara;
	private String callid;
	private String ukey;
	private String adres;
	private int kayit_tipi;
	private int fk_proje;
	private int fk_proje_konu;
	private String referans;
	private String okunma_tarihi;
	private int fk_egitim_durumu;
	private int fk_oncelik;
	private String isOnayBekle;
	private String tar;
	private int yil;
	private int ay;
	private int gun;
	private int saat;
	private int fk_taskb_kapanma_statu;
	private int fk_aramakanali;
	private int fk_aramasebebi;
	private int fk_sonuckod;
	private String only_tarih;
	private int vcc;
	private int internet;
	private int phone;
	private int fk_lokasyon;
	private String yeniden_acilma_tar;
	private String bitis_tar;
	private String ya_tar;
	private int isReopen;
	private int transfer_edilmis;
	private String ses_dosyasi_yolu;
	private int isAgentTamamladi;
	private String havuz_transfer_tarihi;
	private int isEnc;
	// private String cryp_MU_aciklama;
	// private String cryp_KU_aciklama;
	// private String cryp_GD_aciklama;
	public TRANSFER() {
		super();
	
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFk_gorev_statu() {
		return fk_gorev_statu;
	}
	public void setFk_gorev_statu(int fk_gorev_statu) {
		this.fk_gorev_statu = fk_gorev_statu;
	}
	public int getFk_part() {
		return fk_part;
	}
	public void setFk_part(int fk_part) {
		this.fk_part = fk_part;
	}
	public int getFk_parent() {
		return fk_parent;
	}
	public void setFk_parent(int fk_parent) {
		this.fk_parent = fk_parent;
	}
	public String getAciklama() {
		return aciklama;
	}
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	public String getCryp_aciklama() {
		
		
		
		
		return Genel.encrypt(getAciklama());
	}
	/*public void setCryp_aciklama(String cryp_aciklama) {
		this.cryp_aciklama = cryp_aciklama;
	}*/
	public String getMU_aciklama() {
		return MU_aciklama;
	}
	public void setMU_aciklama(String mU_aciklama) {
		MU_aciklama = mU_aciklama;
	}
	public String getKU_aciklama() {
		return KU_aciklama;
	}
	public void setKU_aciklama(String kU_aciklama) {
		KU_aciklama = kU_aciklama;
	}
	public String getGD_aciklama() {
		return GD_aciklama;
	}
	public void setGD_aciklama(String gD_aciklama) {
		GD_aciklama = gD_aciklama;
	}
	public int getFk_kaydi_acan() {
		return fk_kaydi_acan;
	}
	public void setFk_kaydi_acan(int fk_kaydi_acan) {
		this.fk_kaydi_acan = fk_kaydi_acan;
	}
	public String getTarih() {
		return tarih;
	}
	public void setTarih(String tarih) {
		this.tarih = tarih;
	}
	public int getFk_kapatan_agent() {
		return fk_kapatan_agent;
	}
	public void setFk_kapatan_agent(int fk_kapatan_agent) {
		this.fk_kapatan_agent = fk_kapatan_agent;
	}
	public String getKapanma_tarihi() {
		return kapanma_tarihi;
	}
	public void setKapanma_tarihi(String kapanma_tarihi) {
		this.kapanma_tarihi = kapanma_tarihi;
	}
	public int getFk_kart() {
		return fk_kart;
	}
	public void setFk_kart(int fk_kart) {
		this.fk_kart = fk_kart;
	}
	public int getFk_statu() {
		return fk_statu;
	}
	public void setFk_statu(int fk_statu) {
		this.fk_statu = fk_statu;
	}
	public int getFk_havuz() {
		return fk_havuz;
	}
	public void setFk_havuz(int fk_havuz) {
		this.fk_havuz = fk_havuz;
	}
	public int getFk_agent() {
		return fk_agent;
	}
	public void setFk_agent(int fk_agent) {
		this.fk_agent = fk_agent;
	}
	public int getFk_tip() {
		return fk_tip;
	}
	public void setFk_tip(int fk_tip) {
		this.fk_tip = fk_tip;
	}
	public int getFk_geri_donus_kanali() {
		return fk_geri_donus_kanali;
	}
	public void setFk_geri_donus_kanali(int fk_geri_donus_kanali) {
		this.fk_geri_donus_kanali = fk_geri_donus_kanali;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getTip_level1() {
		return tip_level1;
	}
	public void setTip_level1(int tip_level1) {
		this.tip_level1 = tip_level1;
	}
	public int getTip_level2() {
		return tip_level2;
	}
	public void setTip_level2(int tip_level2) {
		this.tip_level2 = tip_level2;
	}
	public int getTip_level3() {
		return tip_level3;
	}
	public void setTip_level3(int tip_level3) {
		this.tip_level3 = tip_level3;
	}
	public int getTip_level4() {
		return tip_level4;
	}
	public void setTip_level4(int tip_level4) {
		this.tip_level4 = tip_level4;
	}
	public int getTip_level5() {
		return tip_level5;
	}
	public void setTip_level5(int tip_level5) {
		this.tip_level5 = tip_level5;
	}
	public String getArayan_numara() {
		return arayan_numara;
	}
	public void setArayan_numara(String arayan_numara) {
		this.arayan_numara = arayan_numara;
	}
	public String getCallid() {
		return callid;
	}
	public void setCallid(String callid) {
		this.callid = callid;
	}
	public String getUkey() {
		return ukey;
	}
	public void setUkey(String ukey) {
		this.ukey = ukey;
	}
	public String getAdres() {
		return adres;
	}
	public void setAdres(String adres) {
		this.adres = adres;
	}
	public int getKayit_tipi() {
		return kayit_tipi;
	}
	public void setKayit_tipi(int kayit_tipi) {
		this.kayit_tipi = kayit_tipi;
	}
	public int getFk_proje() {
		return fk_proje;
	}
	public void setFk_proje(int fk_proje) {
		this.fk_proje = fk_proje;
	}
	public int getFk_proje_konu() {
		return fk_proje_konu;
	}
	public void setFk_proje_konu(int fk_proje_konu) {
		this.fk_proje_konu = fk_proje_konu;
	}
	public String getReferans() {
		return referans;
	}
	public void setReferans(String referans) {
		this.referans = referans;
	}
	public String getOkunma_tarihi() {
		return okunma_tarihi;
	}
	public void setOkunma_tarihi(String okunma_tarihi) {
		this.okunma_tarihi = okunma_tarihi;
	}
	public int getFk_egitim_durumu() {
		return fk_egitim_durumu;
	}
	public void setFk_egitim_durumu(int fk_egitim_durumu) {
		this.fk_egitim_durumu = fk_egitim_durumu;
	}
	public int getFk_oncelik() {
		return fk_oncelik;
	}
	public void setFk_oncelik(int fk_oncelik) {
		this.fk_oncelik = fk_oncelik;
	}
	public String getIsOnayBekle() {
		return isOnayBekle;
	}
	public void setIsOnayBekle(String isOnayBekle) {
		this.isOnayBekle = isOnayBekle;
	}
	public String getTar() {
		return tar;
	}
	public void setTar(String tar) {
		this.tar = tar;
	}
	public int getYil() {
		return yil;
	}
	public void setYil(int yil) {
		this.yil = yil;
	}
	public int getAy() {
		return ay;
	}
	public void setAy(int ay) {
		this.ay = ay;
	}
	public int getGun() {
		return gun;
	}
	public void setGun(int gun) {
		this.gun = gun;
	}
	public int getSaat() {
		return saat;
	}
	public void setSaat(int saat) {
		this.saat = saat;
	}
	public int getFk_taskb_kapanma_statu() {
		return fk_taskb_kapanma_statu;
	}
	public void setFk_taskb_kapanma_statu(int fk_taskb_kapanma_statu) {
		this.fk_taskb_kapanma_statu = fk_taskb_kapanma_statu;
	}
	public int getFk_aramakanali() {
		return fk_aramakanali;
	}
	public void setFk_aramakanali(int fk_aramakanali) {
		this.fk_aramakanali = fk_aramakanali;
	}
	public int getFk_aramasebebi() {
		return fk_aramasebebi;
	}
	public void setFk_aramasebebi(int fk_aramasebebi) {
		this.fk_aramasebebi = fk_aramasebebi;
	}
	public int getFk_sonuckod() {
		return fk_sonuckod;
	}
	public void setFk_sonuckod(int fk_sonuckod) {
		this.fk_sonuckod = fk_sonuckod;
	}
	public String getOnly_tarih() {
		return only_tarih;
	}
	public void setOnly_tarih(String only_tarih) {
		this.only_tarih = only_tarih;
	}
	public int getVcc() {
		return vcc;
	}
	public void setVcc(int vcc) {
		this.vcc = vcc;
	}
	public int getInternet() {
		return internet;
	}
	public void setInternet(int internet) {
		this.internet = internet;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public int getFk_lokasyon() {
		return fk_lokasyon;
	}
	public void setFk_lokasyon(int fk_lokasyon) {
		this.fk_lokasyon = fk_lokasyon;
	}
	public String getYeniden_acilma_tar() {
		return yeniden_acilma_tar;
	}
	public void setYeniden_acilma_tar(String yeniden_acilma_tar) {
		this.yeniden_acilma_tar = yeniden_acilma_tar;
	}
	public String getBitis_tar() {
		return bitis_tar;
	}
	public void setBitis_tar(String bitis_tar) {
		this.bitis_tar = bitis_tar;
	}
	public String getYa_tar() {
		return ya_tar;
	}
	public void setYa_tar(String ya_tar) {
		this.ya_tar = ya_tar;
	}
	public int getIsReopen() {
		return isReopen;
	}
	public void setIsReopen(int isReopen) {
		this.isReopen = isReopen;
	}
	public int getTransfer_edilmis() {
		return transfer_edilmis;
	}
	public void setTransfer_edilmis(int transfer_edilmis) {
		this.transfer_edilmis = transfer_edilmis;
	}
	public String getSes_dosyasi_yolu() {
		return ses_dosyasi_yolu;
	}
	public void setSes_dosyasi_yolu(String ses_dosyasi_yolu) {
		this.ses_dosyasi_yolu = ses_dosyasi_yolu;
	}
	public int getIsAgentTamamladi() {
		return isAgentTamamladi;
	}
	public void setIsAgentTamamladi(int isAgentTamamladi) {
		this.isAgentTamamladi = isAgentTamamladi;
	}
	public String getHavuz_transfer_tarihi() {
		return havuz_transfer_tarihi;
	}
	public void setHavuz_transfer_tarihi(String havuz_transfer_tarihi) {
		this.havuz_transfer_tarihi = havuz_transfer_tarihi;
	}
	public int getIsEnc() {
		return isEnc;
	}
	public void setIsEnc(int isEnc) {
		this.isEnc = isEnc;
	}
	public String getCryp_MU_aciklama() {
		return Genel.encrypt(getMU_aciklama());
	}
	public void setCryp_MU_aciklama(String cryp_MU_aciklama) {
		// this.cryp_MU_aciklama = cryp_MU_aciklama;
	}
	public String getCryp_KU_aciklama() {
		return Genel.encrypt(getKU_aciklama());
	}
	public void setCryp_KU_aciklama(String cryp_KU_aciklama) {
		// this.cryp_KU_aciklama = cryp_KU_aciklama;
	}
	public String getCryp_GD_aciklama() {
		return Genel.encrypt(getGD_aciklama());
	}
	public void setCryp_GD_aciklama(String cryp_GD_aciklama) {
		// this.cryp_GD_aciklama = cryp_GD_aciklama;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

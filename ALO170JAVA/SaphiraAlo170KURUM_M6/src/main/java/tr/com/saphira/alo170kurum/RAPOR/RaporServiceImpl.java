package tr.com.saphira.alo170kurum.RAPOR;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.saphira.alo170kurum.model.vTotal;


@Service
public class RaporServiceImpl implements RaporService {
    @Autowired
	RaporDao dao;

    @Override
	public List<rptKurumSubeIstatistik> KurumSubeHavuzIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer fk_havuz,
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	)
	{	
		
		return dao.KurumSubeHavuzIstatistikRaporu(sdate, fdate, tip_level1, fk_il, fk_sube, fk_havuz, skip, take, userId, yetki);
	}

	@Override
	public List<vTotal> countKurumSubeHavuzIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer fk_havuz,
		Integer userId,
		String yetki
	) {
		
		return dao.countKurumSubeHavuzIstatistikRaporu(sdate, fdate, tip_level1, fk_il, fk_sube, fk_havuz, userId, yetki);
	}


    @Override
	public List<rptKurumKullaniciIstatistik> KurumKullaniciIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		
		return dao.KurumKullaniciIstatistikRaporu(sdate, fdate, tip_level1, fk_il, fk_sube, skip, take, userId, yetki);
	}
	@Override
	public List<vTotal> countKurumKullaniciIstatistikRaporu(
		String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer fk_il, 
		Integer fk_sube, 
		Integer userId,
		String yetki
	) {
		
		return dao.countKurumKullaniciIstatistikRaporu(sdate, fdate, tip_level1, fk_il, fk_sube, userId, yetki);
	}



    @Override
	public List<rptBildirimDetay> BildirimDetayRaporu(
        String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer tip_level2,
		Integer tip_level3,
		Integer fk_il, 
		Integer fk_ilce, 
		Integer fk_havuz, 
		Integer fk_gorev_statu, 
		Integer arama_kanali_tipi, 
		Integer fk_aramakanali, 
		Integer fk_aramasebebi, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		
		return dao.BildirimDetayRaporu(
		sdate, 
		fdate, 
		tip_level1, 
		tip_level2,
		tip_level3,
		fk_il, 
		fk_ilce, 
		fk_havuz, 
		fk_gorev_statu, 
		arama_kanali_tipi, 
		fk_aramakanali, 
		fk_aramasebebi, 
		skip,
		take,
		userId,
		yetki
		);
	}
	@Override
	public List<vTotal> countBildirimDetayRaporu(
        String sdate, 
		String fdate, 
		Integer tip_level1, 
		Integer tip_level2,
		Integer tip_level3,
		Integer fk_il, 
		Integer fk_ilce, 
		Integer fk_havuz, 
		Integer fk_gorev_statu, 
		Integer arama_kanali_tipi, 
		Integer fk_aramakanali, 
		Integer fk_aramasebebi, 
		Integer userId,
		String yetki
	) {
		
		return dao.countBildirimDetayRaporu(
			sdate, 
			fdate, 
			tip_level1, 
			tip_level2,
			tip_level3,
			fk_il, 
			fk_ilce, 
			fk_havuz, 
			fk_gorev_statu, 
			arama_kanali_tipi, 
			fk_aramakanali, 
			fk_aramasebebi, 
			userId,
			yetki
		);
	}

    @Override
	public List<rptMuAktivite> MuAktiviteRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
        Integer sistem, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
	) {
		
		return dao.MuAktiviteRapor(
		sdate, 
		fdate, 
		mu, 
        sistem,  
		skip,
		take,
		userId,
		yetki
		);
	}
    @Override
	public List<vTotal> countMuAktiviteRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
        Integer sistem, 
        Integer userId, 
        String yetki
	) {
		
		return dao.countMuAktiviteRapor(
		sdate, 
		fdate, 
		mu, 
        sistem,  
		userId,
		yetki
		);
	}



	@Override
	public List<rptMuSure> MuSureRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
		Integer tip_level1, 
        Integer sistem, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
	) {
		return dao.MuSureRapor(
		sdate, 
		fdate, 
		mu, 
		tip_level1, 
        sistem,  
		skip,
		take,
		userId,
		yetki
		);
	}
    @Override
	public List<vTotal> countMuSureRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
		Integer tip_level1, 
        Integer sistem, 
        Integer userId, 
        String yetki
	) {
		
		return dao.countMuSureRapor(
			sdate, 
			fdate, 
			mu, 
			tip_level1, 
			sistem,  
			userId,
			yetki
		);
	}



    @Override
	public List<rptKySure> KySureRapor(
        String sdate, 
        String fdate, 
        Integer ky, 
        Integer sistem, 
        Integer tip_level1, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
	) {
		
		return dao.KySureRapor(
		sdate, 
		fdate, 
		ky, 
        sistem,  
        tip_level1,  
		skip,
		take,
		userId,
		yetki
		);
	}
	public  List<vTotal> countKySureRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
        Integer sistem, 
        Integer tip_level1, 
        Integer userId, 
        String yetki
	 ){
		
		return dao.countKySureRapor(
			sdate, 
			fdate, 
			mu, 
			sistem, 
			tip_level1, 
			userId, 
			yetki
		);
	}


	
    @Override
	public List<rptHavuzBildirimSure> HavuzBildirimSureRapor(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        Integer fk_havuz, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
	) {
		
		return dao.HavuzBildirimSureRapor(
			 sdate, 
			 fdate, 
			 tip_level1, 
			 fk_il, 
			 fk_sube, 
			 fk_havuz, 
			 skip, 
			 take, 
			 userId, 
			 yetki
		);
	}
	public  List<vTotal> countHavuzBildirimSureRapor(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        String fk_havuz, 
        Integer userId, 
        String yetki
	 ) {
		return dao.countHavuzBildirimSureRapor(
			sdate, 
			fdate, 
			tip_level1, 
			fk_il, 
			fk_sube, 
			fk_havuz, 
			userId, 
			yetki
	   );
	 } 


	
    @Override
	public List<rptSektorBazinda> SektorBazindaRapor(
        String sdate, 
        String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
        String yetki
	) {
		return dao.SektorBazindaRapor(
			 sdate, 
			 fdate, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> countSektorBazindaRapor(
        String sdate, 
        String fdate,  
        Integer userId, 
        String yetki
	 ) {
		return dao.countSektorBazindaRapor(
			sdate, 
			fdate, 
			userId,
			yetki
	   );
	 } 

	
    @Override
	public List<rptSektorBazindaIhbarSikayet> SektorBazindaIhbarSikayet(
        String sdate, 
		String fdate, 
		Integer fk_sektor, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.SektorBazindaIhbarSikayet(
			 sdate, 
			 fdate, 
			 fk_sektor, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> countSektorBazindaIhbarSikayet(
        String sdate, 
		String fdate, 
		Integer fk_sektor, 
		Integer userId,
		String yetki
	 ) {
		return dao.countSektorBazindaIhbarSikayet(
			 sdate, 
			 fdate, 
			 fk_sektor, 
			 userId,
			 yetki
		);
	 }

	
    @Override
	public List<rptIlBazinda> IlBazindaRapor(
        String sdate, 
		String fdate, 
		Integer fk_gorev_statu, 
		Integer userId,
		String yetki
	) {
		return dao.IlBazindaRapor(
			 sdate, 
			 fdate, 
			 fk_gorev_statu, 
			 userId,
			 yetki
		);
	}
	

	
    @Override
    public List<rptKonularaGore> KonularaGore(
        String sdate, 
		String fdate, 
		Integer statu,
		Integer tip_level1, 
		Integer tip_level2, 
		Integer tip_level3,  
		Integer userId,
		String yetki
    ){
		return dao.KonularaGore(
			 sdate, 
			 fdate, 
			 statu,
			 tip_level1, 
			 tip_level2, 
			 tip_level3, 
			 userId,
			 yetki
		);
	}


	
    @Override
	public List<rptHavuzOzet> HavuzOzet(
		Integer skip,
		Integer take,
		Integer userId
	) {
		return dao.HavuzOzet( 
			 skip,
			 take,
			 userId
		);
	}
	public  List<vTotal> countHavuzOzet(
		Integer userId
	 ) {
		return dao.countHavuzOzet( 
			 userId
		);
	 }

	
    @Override
	public List<rptSgkBilgilendirme> SgkBilgilendirme(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.SgkBilgilendirme(
			 sdate, 
			 fdate, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> countSgkBilgilendirme(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		return dao.countSgkBilgilendirme(
			sdate, 
			fdate, 
			userId,
			yetki
	   );
	 }


	
    @Override
	public List<rptIvrAgent> IvrAgent(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.IvrAgent(
			 sdate, 
			 fdate, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> IvrAgentCount(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		return dao.IvrAgentCount(
			sdate, 
			fdate, 
			userId,
			yetki
	   );
	 }

	
    @Override
	public List<rptTaseronHavuzu> TaseronHavuzu(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.TaseronHavuzu(
			 sdate, 
			 fdate, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> TaseronHavuzuCount(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		return dao.TaseronHavuzuCount(
			 sdate, 
			 fdate, 
			 userId,
			 yetki
		);
	 }  

	
    @Override
	public List<rptSesKayit> SesKayit(
		String sdate, 
		String fdate, 
		String tc, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.SesKayit(
			 sdate, 
			 fdate, 
			 tc, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> SesKayitCount(
		String sdate, 
		String fdate, 
		String tc, 
		Integer userId,
		String yetki
	 ) {
		return dao.SesKayitCount(
			 sdate, 
			 fdate, 
			 tc, 
			 userId,
			 yetki
		);
	}

	
    @Override
	public List<rptKurumKapanmaIstatistik> KurumKapanmaIstatistik(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	) {
		return dao.KurumKapanmaIstatistik(
			 sdate, 
			 fdate, 
			 userId,
			 yetki
		);
	}

	
    @Override
	public List<rptBirimlereGoreKullaniciSayisi> BirimlereGoreKullaniciSayisi(
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.BirimlereGoreKullaniciSayisi(
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> BirimlereGoreKullaniciSayisiCount(
		Integer userId,
		String yetki
	 ) {
		return dao.BirimlereGoreKullaniciSayisiCount(
			 userId,
			 yetki
		);
	 }  


	
    @Override
	public List<rptKurumBazindaZamanindaKapanan> KurumBazindaZamanindaKapanan(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.KurumBazindaZamanindaKapanan(
			 sdate, 
			 fdate, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> KurumBazindaZamanindaKapananCount(
		Integer userId,
		String yetki
	 ) {
		return dao.KurumBazindaZamanindaKapananCount(
			 userId,
			 yetki
		);
	 } 
	 
	 


	
    @Override
	public List<rptIvrRapor> IvrRapor(
        String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	) {
		return dao.IvrRapor(
			 sdate, 
			 fdate, 
			 userId,
			 yetki
		);
	}	



	
    @Override
	public List<rptSmsRapor> SmsRapor(
		Integer skip,
		Integer take,
		String tipi,
		Integer userId,
		String yetki
	) {
		return dao.SmsRapor(
			 skip,
			 take,
			 tipi,
			 userId,
			 yetki
		);
	}	
	public  List<vTotal> SmsRaporCount(
		String tipi,
		Integer userId,
		String yetki
	 ) {
		return dao.SmsRaporCount(
			 tipi,
			 userId,
			 yetki
		);
	}


    @Override
	public List<rptPeriyodikRapor> PeriyodikRaporGelenCagri(
		String sdate, 
		String fdate, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.PeriyodikRaporGelenCagri(
			 sdate, 
			 fdate, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}
	public  List<vTotal> PeriyodikRaporGelenCagriCount(
		String sdate,
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		return dao.PeriyodikRaporGelenCagriCount(
			 sdate, 
			 fdate, 
			 userId,
			 yetki
		);
	 } 	
	 
	 

	 @Override
	 public List<rptPeriyodikRapor> PeriyodikRaporIlkOnKonu(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
		
		return dao.PeriyodikRaporIlkOnKonu(
			 sdate, 
			 fdate, 
			 userId,
			 yetki
		);
	 }


	 @Override
	 public List<rptPeriyodikRapor> PeriyodikRaporDisAramaSms(
		String sdate, 
		String fdate, 
		Integer userId,
		String yetki
	 ) {
				
		return dao.PeriyodikRaporDisAramaSms(
			 sdate, 
			 fdate, 
			 userId,
			 yetki
		);

	 }


    @Override
	public List<rptRaporLog> RaporLog(
		String sdate, 
		String fdate, 
		Integer fk_user,
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
	) {
		return dao.RaporLog(
			 sdate, 
			 fdate, 
			 fk_user, 
			 skip,
			 take,
			 userId,
			 yetki
		);
	}	
	public List<vTotal> countRaporLog(
		String sdate,
		String fdate,
		Integer fk_user,
		Integer userId,
		String yetki
	 )	{
		return dao.countRaporLog(
			 sdate, 
			 fdate, 
			 fk_user, 
			 userId,
			 yetki
		);
	}
	/*@Override
	public List<Map<String,Object>> KurumSubeHavuzIstatistikRaporu(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        Integer fk_havuz,
        Integer skip,
        Integer take,
        Integer userId,
        vUSERYETKI yetki
        ){
			return dao.KurumSubeHavuzIstatistikRaporu(sdate,fdate,tip_level1,fk_il,fk_sube,fk_havuz,skip,take,userId,yetki);
		}
*/
	
}

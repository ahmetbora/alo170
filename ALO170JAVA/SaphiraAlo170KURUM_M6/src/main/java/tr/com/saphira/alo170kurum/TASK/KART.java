package tr.com.saphira.alo170kurum.TASK;

public class KART {
	
	private int id;
	private int kayit_tipi;
	private String ukey;
	private String adi;
	private String soyadi;
	private String adi2;
	private String adi3;
	private String tckimlik;
	private String adres;
	private int fk_ulke;
	private int fk_il;
	private int fk_ilce;
	private String tel1;
	private String tel2;
	private String gsm;
	private String fax;
	private String email;
	private int fk_agent;
	private int fk_kaydi_yapan;
	private String kayit_tarihi;
	private String sifre;
	private String dt;
	private String sskno;
	private int fk_cinsiyet;
	private int fk_egitimdurumu;
	private int fk_gsoru;
	private String gsoru_cevap;
	private int isVip;
	private String tmp_soru;
	private String tmp_cevap;
	
	public KART() {
		super();
	
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getKayit_tipi() {
		return kayit_tipi;
	}
	public void setKayit_tipi(int kayit_tipi) {
		this.kayit_tipi = kayit_tipi;
	}
	public String getUkey() {
		return ukey;
	}
	public void setUkey(String ukey) {
		this.ukey = ukey;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getSoyadi() {
		return soyadi;
	}
	public void setSoyadi(String soyadi) {
		this.soyadi = soyadi;
	}
	public String getAdi2() {
		return adi2;
	}
	public void setAdi2(String adi2) {
		this.adi2 = adi2;
	}
	public String getAdi3() {
		return adi3;
	}
	public void setAdi3(String adi3) {
		this.adi3 = adi3;
	}
	public String getTckimlik() {
		return tckimlik;
	}
	public void setTckimlik(String tckimlik) {
		this.tckimlik = tckimlik;
	}
	public String getAdres() {
		return adres;
	}
	public void setAdres(String adres) {
		this.adres = adres;
	}
	public int getFk_ulke() {
		return fk_ulke;
	}
	public void setFk_ulke(int fk_ulke) {
		this.fk_ulke = fk_ulke;
	}
	public int getFk_il() {
		return fk_il;
	}
	public void setFk_il(int fk_il) {
		this.fk_il = fk_il;
	}
	public int getFk_ilce() {
		return fk_ilce;
	}
	public void setFk_ilce(int fk_ilce) {
		this.fk_ilce = fk_ilce;
	}
	public String getTel1() {
		return tel1;
	}
	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}
	public String getTel2() {
		return tel2;
	}
	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}
	public String getGsm() {
		return gsm;
	}
	public void setGsm(String gsm) {
		this.gsm = gsm;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getFk_agent() {
		return fk_agent;
	}
	public void setFk_agent(int fk_agent) {
		this.fk_agent = fk_agent;
	}
	public int getFk_kaydi_yapan() {
		return fk_kaydi_yapan;
	}
	public void setFk_kaydi_yapan(int fk_kaydi_yapan) {
		this.fk_kaydi_yapan = fk_kaydi_yapan;
	}
	public String getKayit_tarihi() {
		return kayit_tarihi;
	}
	public void setKayit_tarihi(String kayit_tarihi) {
		this.kayit_tarihi = kayit_tarihi;
	}
	public String getSifre() {
		return sifre;
	}
	public void setSifre(String sifre) {
		this.sifre = sifre;
	}
	public String getDt() {
		return dt;
	}
	public void setDt(String dt) {
		this.dt = dt;
	}
	public String getSskno() {
		return sskno;
	}
	public void setSskno(String sskno) {
		this.sskno = sskno;
	}
	public int getFk_cinsiyet() {
		return fk_cinsiyet;
	}
	public void setFk_cinsiyet(int fk_cinsiyet) {
		this.fk_cinsiyet = fk_cinsiyet;
	}
	public int getFk_egitimdurumu() {
		return fk_egitimdurumu;
	}
	public void setFk_egitimdurumu(int fk_egitimdurumu) {
		this.fk_egitimdurumu = fk_egitimdurumu;
	}
	public int getFk_gsoru() {
		return fk_gsoru;
	}
	public void setFk_gsoru(int fk_gsoru) {
		this.fk_gsoru = fk_gsoru;
	}
	public String getGsoru_cevap() {
		return gsoru_cevap;
	}
	public void setGsoru_cevap(String gsoru_cevap) {
		this.gsoru_cevap = gsoru_cevap;
	}
	public int getIsVip() {
		return isVip;
	}
	public void setIsVip(int isVip) {
		this.isVip = isVip;
	}
	public String getTmp_soru() {
		return tmp_soru;
	}
	public void setTmp_soru(String tmp_soru) {
		this.tmp_soru = tmp_soru;
	}
	public String getTmp_cevap() {
		return tmp_cevap;
	}
	public void setTmp_cevap(String tmp_cevap) {
		this.tmp_cevap = tmp_cevap;
	}
	
	
	
	
	

}

package tr.com.saphira.alo170kurum.RAPOR;

public class rptSektorBazindaIhbarSikayet {
    private Integer id;
    private String kart;
    private String sektor;
    private String tarih;
    private String agent;
    private String konu;
    private String statu;
    private String il;
    private String sure;


    public rptSektorBazindaIhbarSikayet() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getkart(){
        return kart;
    }
    public void setkart(String kart){
        this.kart=kart;
    }

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String getagent(){
        return agent;
    }
    public void setagent(String agent){
        this.agent=agent;
    }

    public String getkonu(){
        return konu;
    }
    public void setkonu(String konu){
        this.konu=konu;
    }

    public String getstatu(){
        return statu;
    }
    public void setstatu(String statu){
        this.statu=statu;
    }

    public String getil(){
        return il;
    }
    public void setil(String il){
        this.il=il;
    }

    public String getsure(){
        return sure;
    }
    public void setsure(String sure){
        this.sure=sure;
    }

    public String getsektor(){
        return sektor;
    }
    public void setsektor(String sektor){
        this.sektor=sektor;
    }


}


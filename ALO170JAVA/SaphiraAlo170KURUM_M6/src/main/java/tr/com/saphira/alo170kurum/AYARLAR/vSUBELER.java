package tr.com.saphira.alo170kurum.AYARLAR;

public class vSUBELER {

	private Integer id;
	private String adi;
	private String kurum;
	private String il;
	private Integer havuz_sayisi;
	private Integer isActive;
	private Integer fk_konu;
	private Integer fk_il;
	private String durum;
	public vSUBELER() {
		super();
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getKurum() {
		return kurum;
	}
	public void setKurum(String kurum) {
		this.kurum = kurum;
	}
	public String getIl() {
		return il;
	}
	public void setIl(String il) {
		this.il = il;
	}
	public Integer getHavuz_sayisi() {
		return havuz_sayisi;
	}
	public void setHavuz_sayisi(Integer havuz_sayisi) {
		this.havuz_sayisi = havuz_sayisi;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public Integer getFk_konu() {
		return fk_konu;
	}
	public void setFk_konu(Integer fk_konu) {
		this.fk_konu = fk_konu;
	}
	public Integer getFk_il() {
		return fk_il;
	}
	public void setFk_il(Integer fk_il) {
		this.fk_il = fk_il;
	}
	public String getDurum() {
		return durum;
	}
	public void setDurum(String durum) {
		this.durum = durum;
	}	
}

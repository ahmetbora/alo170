package tr.com.saphira.alo170kurum.KART;

public class vKART {
	private int kartid;
	private String ukey;
	private String kartadi;
	private String soyadi;
	private String tckimlik;
	private String tel1;
	private String tel2;
	private String gsm;
	private String email;
	private String fax;
	private String dogumtarihi;
	private String ulkeadi;
	private String iladi;
	private String ilceadi;
	private String edurum;
	private String cinsiyet;
	private String gsoruadi;
	private Integer fk_cinsiyet;
	private Integer fk_egitimdurumu;
	private Integer fk_il;
	private Integer fk_ilce;
	private String sskno;
	
	public vKART() {
		super();
	
	}
	public int getKartid() {
		return kartid;
	}
	public void setKartid(int kartid) {
		this.kartid = kartid;
	}
	public String getUkey() {
		return ukey;
	}
	public void setUkey(String ukey) {
		this.ukey = ukey;
	}
	public String getKartadi() {
		return kartadi;
	}
	public void setKartadi(String kartadi) {
		this.kartadi = kartadi;
	}
	public String getKartSoyadi() {
		return soyadi;
	}
	public void setKartSoyadi(String soyadi) {
		this.soyadi = soyadi;
	}
	public String getTckimlik() {
		return tckimlik;
	}
	public void setTckimlik(String tckimlik) {
		this.tckimlik = tckimlik;
	}
	public String getTel1() {
		return tel1;
	}
	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}
	public String getTel2() {
		return tel2;
	}
	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}
	public String getGsm() {
		return gsm;
	}
	public void setGsm(String gsm) {
		this.gsm = gsm;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getDogumtarihi() {
		return dogumtarihi;
	}
	public void setDogumtarihi(String dogumtarihi) {
		this.dogumtarihi = dogumtarihi;
	}
	public String getUlkeadi() {
		return ulkeadi;
	}
	public void setUlkeadi(String ulkeadi) {
		this.ulkeadi = ulkeadi;
	}
	public String getIladi() {
		return iladi;
	}
	public void setIladi(String iladi) {
		this.iladi = iladi;
	}
	public String getIlceadi() {
		return ilceadi;
	}
	public void setIlceadi(String ilceadi) {
		this.ilceadi = ilceadi;
	}
	public String getEdurum() {
		return edurum;
	}
	public void setEdurum(String edurum) {
		this.edurum = edurum;
	}
	public String getCinsiyet() {
		return cinsiyet;
	}
	public void setCinsiyet(String cinsiyet) {
		this.cinsiyet = cinsiyet;
	}
	public String getGsoruadi() {
		return gsoruadi;
	}
	public void setGsoruadi(String gsoruadi) {
		this.gsoruadi = gsoruadi;
	}
	
	public Integer getFkCinsiyet() {
		return fk_cinsiyet;
	}
	public void setFkCinsiyet(Integer fk_cinsiyet) {
		this.fk_cinsiyet = fk_cinsiyet;
	}
	
	public Integer getFkEgitimDurumu() {
		return fk_egitimdurumu;
	}
	public void setFkEgitimDurumu(Integer fk_egitimdurumu) {
		this.fk_egitimdurumu = fk_egitimdurumu;
	}
	public Integer getFkIl() {
		return fk_il;
	}
	public void setFkIl(Integer fk_il) {
		this.fk_il = fk_il;
	}	
	public Integer getFkIlce() {
		return fk_ilce;
	}
	public void setFkIlce(Integer fk_ilce) {
		this.fk_ilce = fk_ilce;
	}	
	public String getSskNo() {
		return sskno;
	}
	public void setSskNo(String sskno) {
		this.sskno = sskno;
	}		
	
	
}

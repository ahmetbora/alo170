package tr.com.saphira.alo170kurum.LOOKUP;

import java.util.List;
import java.util.Map;

import tr.com.saphira.alo170kurum.AYARLAR.vALTKURUMLAR;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKANALI;
import tr.com.saphira.alo170kurum.AYARLAR.vARAMAKONUSU;
import tr.com.saphira.alo170kurum.AYARLAR.vILCELER;
import tr.com.saphira.alo170kurum.AYARLAR.vILLER;
import tr.com.saphira.alo170kurum.AYARLAR.vKURUMLAR;

import tr.com.saphira.alo170kurum.AYARLAR.vULKELER;
import tr.com.saphira.alo170kurum.model.Lookup;


public interface LOKKUPDao {

	public vULKELER getUlkelerById(Integer ID);
	public List<vILLER> getUlkeler(Integer ID);
	public void setUlkeler(Integer ID, String ADI, Integer ISACTIVE);
	
	public vILLER getIllerById(Integer ID);
	public List<vILLER> getIller(Integer ID);
	public List<vILLER> getIllerByUlke(Integer ID,Integer FKULKE,String ADI,String IDS);
	public void setIller(Integer ID, String ADI,Integer ULKE, Integer ISACTIVE);
	
	public vILCELER getIlcelerById(Integer ID);
	public List<vILCELER> getIlceler(Integer IL);
	public void setIlceler(Integer ID, String ADI, Integer IL, Integer ISACTIVE);
	

	
	public Lookup getAramaKanaliById(Integer ID);
	public List<Lookup> getAramaKanali(Integer ID);
	public void setAramaKanali(Integer ID, String ADI, Integer ISACTIVE);
	
	public Lookup getEgitimDurumuById(Integer ID);
	public List<Lookup> getEgitimDurumu(Integer ID);
	public void setEgitimDurumu(Integer ID, String ADI, Integer ISACTIVE);
	
	public Lookup getGuvenlikSorusuById(Integer ID);
	public List<Lookup> getGuvenlikSorusu(Integer ID);
	public void setGuvenlikSorusu(Integer ID, String ADI, Integer ISACTIVE);
	
	
	
	public List<vARAMAKANALI> getAramaKanaliByTip(Integer ID);
	
	public List<vKURUMLAR> getKurumlar(Integer ID,String TERM,Integer skip, Integer take);
	public List<vALTKURUMLAR> getAltKurumlar(Integer ID);
	public List<vARAMAKONUSU> getAramaKonulari(Integer ID);
	
	
	public List<Lookup> getAramaSebebi(Integer ID);
	public List<Lookup> getAramaKanaliTipi();
	public List<Lookup> getTaskStatu(Integer id);
	public List<Lookup> cmbgetHavuzlar(Integer AGENT);
	public List<Lookup> cmbgetHavuzlarSube(Integer SUBE);
	public List<Lookup> getRoller(Integer ID,Integer isSysAdmin,Integer tip,Integer level,Integer userId);
	public List<Lookup> getRollerUser(Integer userId);
	public List<Lookup> getCinsiyet();
	public List<Lookup> getGeriDonusKanali();
	public List<Lookup> getBilgiBankasiKonular();
	
	public List<Lookup> getLokasyon();
	public Map<String, Object>  getLokasyonById(Integer id);
	
	// public List<Lookup> getSubeler(Integer ID,Integer KONU,String TERM,Integer FKIL,String IDS,Integer WHEREIN);
	public List<Lookup> getSubeler(Integer id,Integer fk_konu,String term,Integer fk_il, String ids, Integer wherein);
	public List<Lookup> gettaskHavuzKullanici(Integer fk_havuz);
	public List<Lookup> getTaskbKapanmaStatu(Integer id);
	public List<Lookup> getSssKonu();
	public List<Lookup> getSssAltKonu(Integer fk_sss_grup);
	public List<Lookup> getVardiyaTipleri(String ID,String Tip);
	public List<Map<String, Object>>  getCmbDetayWhere(String table,String where);
	public List<Map<String, Object>>  getUsers(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon);
	public List<Map<String, Object>>  getUsersByUserHavuz(Integer fk_user);
}

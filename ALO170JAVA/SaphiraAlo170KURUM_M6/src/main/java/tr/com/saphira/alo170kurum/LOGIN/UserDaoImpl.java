package tr.com.saphira.alo170kurum.LOGIN;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import tr.com.saphira.alo170kurum.model.SaphiraUsers;



@Repository
public class UserDaoImpl implements UserDao {
	

	 @Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
    
    
    
    

	@Override
	public List<SaphiraUsers> listAllusers() {
		//String sql = "Select * from saphira_users";
		List<SaphiraUsers> list =  new ArrayList<SaphiraUsers>(); // namedParameterJdbcTemplate.query(sql, getSqlParameterByModel(null),new SaphiraUsersMapper());
		return list;
	}

	
	 
	@Override
	public void addUser(SaphiraUsers saphirausers) {
		
		
	}

	@Override
	public void updateUser(SaphiraUsers saphirausers) {
		
		
	}

	@Override
	public void deleteUser(int id) {
		
		
	}

	@Override
	public SaphiraUsers findUserById(int id) {
		
		return null;
	}

	@Override
	public User findUserByUserName(String username) {
		//System.out.println("findUserByUserNamegeldi");
		  String sql1 = "call spLOGINGetUser (?)";
		  Map<String, Object>  rows = jdbcro.queryForMap(sql1,new Object[] { username });
		  
		  User user = new User();
		  user.setPassword(rows.get("password").toString());
		  user.setUsername(username);
		  user.setRoles(new String[] {"ADMIN"});
		  System.out.println("****************findUserByUserName*************"+user.getUsername());
		  
		/*
		User user = namedParameterJdbcTemplate.queryForObject(sql, askSqlParameterByModel(username, null , null), new ComboMapper());
		if(user == null) {
			System.out.println("user nulll");	
		}
		System.out.println(user.getUsername());
		?*/


		return user;
		
	}


	@Override
	public SaphiraUsers getLogin(String KEY) {
		String sql1 = "call spLogin (?)";
		  Map<String, Object>  rows = jdbcro.queryForMap(sql1,new Object[] { KEY });
		  SaphiraUsers u = new SaphiraUsers();
		  if (rows != null) {
			  //System.out.println("****************getLogin*************"+rows.get("id"));
			  u.setAdi(rows.get("adi").toString());
			  u.setId((Integer)rows.get("id"));
			  u.setExt(rows.get("ext").toString());
			  u.setFk_lokasyon(((Long)rows.get("fk_lokasyon")).intValue());
			  u.setLokasyon(rows.get("lokasyon").toString());
			  u.setUserchange(((Long)rows.get("user_change")).intValue());
			  u.setFk_rol(rows.get("fk_rol").toString());
			  u.setIsWallBoard(((Long)rows.get("isWallBoard")).intValue());
			  u.setSysAdmin(((Long)rows.get("SysAdmin")).intValue());
			  u.setIsAdmin(((Long)rows.get("isAdmin")).intValue());
			  u.setIsAgent(((Long)rows.get("isAgent")).intValue());
			  u.setIsSU(((Long)rows.get("isSU")).intValue());
			  u.setRol(rows.get("rol").toString());
			  u.setyetkiJson(rows.get("yetkiJson").toString());
			  u.setRolLevel((Integer)rows.get("rolLevel"));
		  }else {
			  //System.out.println("****************getLogin*************rows boş");
		  }
		  
		  return u;
		  
		 
	}

}
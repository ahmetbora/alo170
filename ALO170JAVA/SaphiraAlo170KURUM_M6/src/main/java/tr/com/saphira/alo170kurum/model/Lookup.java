package tr.com.saphira.alo170kurum.model;

public class Lookup {
	private Integer id;
	private String adi;
	private Integer isActive;
	private Integer fk_rol;
	public Lookup() {	
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public Integer getfk_rol() {
		return fk_rol;
	}
	public void setfk_rol(Integer fk_rol) {
		this.fk_rol = fk_rol;
	}
	
	
	
}

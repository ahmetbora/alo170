package tr.com.saphira.alo170kurum.BILDIRIM;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.document.AbstractXlsView;

public class ExcelExport extends AbstractXlsView {
	
	@Autowired
	BildirimService service;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
			response.setHeader("Content-Disposition", "attachment; filename=\"test.xls\"");
			
			@SuppressWarnings("unchecked")
			List<vBILDIRIM> list = (List<vBILDIRIM>) model.get("filtre");
			
			Sheet sheet = workbook.createSheet("User List");
			
	        Font headerFont = workbook.createFont();
	        headerFont.setBold(true);
	        headerFont.setFontHeightInPoints((short) 14);
	        
			
			//header row
			Row header = sheet.createRow(0);
			header.createCell(0).setCellValue("NO");
			header.createCell(1).setCellValue("VATANDAS");
			header.createCell(2).setCellValue("TARIH");
			header.createCell(3).setCellValue("ISI YAPACAK AGENT");
			header.createCell(4).setCellValue("KONU");
			header.createCell(5).setCellValue("GOREV DURUMU");
			header.createCell(6).setCellValue("SEHIR");
			header.createCell(7).setCellValue("SURE");
			
			int rowNum = 1;
			
			for(vBILDIRIM vbildirim : list) {
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(vbildirim.getId());
				row.createCell(1).setCellValue(vbildirim.getBasvuruSahibi());
				row.createCell(2).setCellValue(vbildirim.getTarih());
				row.createCell(3).setCellValue(vbildirim.getIsiYapacakAgent());
				row.createCell(4).setCellValue(vbildirim.getKonu1());
				row.createCell(5).setCellValue(vbildirim.getStatu());
				row.createCell(6).setCellValue(vbildirim.getIlAdi());
				row.createCell(7).setCellValue(vbildirim.getHavuzSure());
			}
			
	}

}

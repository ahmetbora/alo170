package tr.com.saphira.alo170kurum.RAPOR;

public class rptSmsRapor {
    private Integer id;
    private String numara_email;
    private String bildirim_no;
    private String statu;
 




    public rptSmsRapor() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getnumara_email(){
        return numara_email;
    }
    public void setnumara_email(String numara_email){
        this.numara_email=numara_email;
    }

    public String gebildirim_no(){
        return bildirim_no;
    }
    public void setbildirim_no(String bildirim_no){
        this.bildirim_no=bildirim_no;
    }

    public String getstatu(){
        return statu;
    }
    public void setstatu(String statu){
        this.statu=statu;
    }

}


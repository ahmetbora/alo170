package tr.com.saphira.alo170kurum.RAPOR;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;

public class UserExcelExporter {
    private List<vKULLANICILAR> liste;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    public UserExcelExporter(List<vKULLANICILAR> liste) {
        this.liste = liste;
        workbook = new XSSFWorkbook();
    }
 
    private void writeHeaderLine() {
        sheet = workbook.createSheet("Users");
         
        Row row = sheet.createRow(0);
         
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);
         
        createCell(row, 0, "User ID", style);      
        createCell(row, 1, "E-mail", style);       
        createCell(row, 2, "Full Name", style);    
        createCell(row, 3, "Roles", style);
        createCell(row, 4, "Enabled", style);
         
    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;
 
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);
                 
        for (vKULLANICILAR item : liste) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
            String lokasyon=null;
            String adi=null;
            String rol=null;
            try {
                adi=item.getAdi().toString();
                adi=adi.replaceAll("ı","i");
                adi=adi.replaceAll("ş","s");
                adi=adi.replaceAll("ğ","g");
                adi=adi.replaceAll("İ","I");
                adi=adi.replaceAll("Ş","S");
                adi=adi.replaceAll("Ğ","G");
            } catch (Exception e) {
                adi="";    
            }
            try {
                lokasyon=item.getLokasyon().toString();
                lokasyon=lokasyon.replaceAll("ı","i");
                lokasyon=lokasyon.replaceAll("ş","s");
                lokasyon=lokasyon.replaceAll("ğ","g");
                lokasyon=lokasyon.replaceAll("İ","I");
                lokasyon=lokasyon.replaceAll("Ş","S");
                lokasyon=lokasyon.replaceAll("Ğ","G");   
            } catch (Exception e) {
                lokasyon="";
            }

            try {
                rol=item.getRol().toString();
                rol=rol.replaceAll("ı","i");
                rol=rol.replaceAll("ş","s");
                rol=rol.replaceAll("ğ","g");
                rol=rol.replaceAll("İ","I");
                rol=rol.replaceAll("Ş","S");
                rol=rol.replaceAll("Ğ","G");    
            } catch (Exception e) {
               rol="";
            }

            String aciklama="AD SOYAD: MUSTAFA	ARIKAN TCKN: 56632157308            KURUM:  BORNOVA NACİ ŞAHİN SOSYAL GÜVENLİK MERKEZİ            KONU:  	ARGESU KİMYA MÜH. GERİ DÖN. ENERJİ SAN.ZİRAİ ÜRÜN.PLS.LTD.ŞT FİRMASINDAN 11. VE 12. AYDAKİ PRİMLERİM ELDEN VERİLMİŞTİR ANCAK HİZMET DÖKÜMÜMDE GÖRÜNMEMEKTEDİR. NE ZAMAN GÖRÜNTÜLENİR? KURUM İLE 1 AY ÖNCE İLE GÖRÜŞME SAĞLADIM; ANCAK İLGİNENEN GÖREVLİNİN DOĞUM İZNİNE AYRILDIĞI SÖYLENMİŞTİR. KONU HAKKINDA İVEDİLİKLE BİLGİ ALMAK İSTİYORUM.";
         
            aciklama=aciklama.replaceAll("ı","i");
            aciklama=aciklama.replaceAll("ş","s");
            aciklama=aciklama.replaceAll("ğ","g");
            aciklama=aciklama.replaceAll("İ","I");
            aciklama=aciklama.replaceAll("Ş","S");
            aciklama=aciklama.replaceAll("Ğ","G");   
             
            createCell(row, columnCount++, item.getId(), style);
            createCell(row, columnCount++, adi, style);
            createCell(row, columnCount++, item.getEmail().toString(), style);
            createCell(row, columnCount++, lokasyon, style);
            createCell(row, columnCount++, rol, style);
            createCell(row, columnCount++, aciklama, style);
             
        }
    }
     
    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();
         
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
         
        outputStream.close();
         
    }


    public void deneme(HttpServletResponse response) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Java Books");
        int rowCount = 0;
        for (vKULLANICILAR item : liste) {
            String aciklama="AD SOYAD: MUSTAFA	ARIKAN TCKN: 56632157308            KURUM:  BORNOVA NACİ ŞAHİN SOSYAL GÜVENLİK MERKEZİ            KONU:  	ARGESU KİMYA MÜH. GERİ DÖN. ENERJİ SAN.ZİRAİ ÜRÜN.PLS.LTD.ŞT FİRMASINDAN 11. VE 12. AYDAKİ PRİMLERİM ELDEN VERİLMİŞTİR ANCAK HİZMET DÖKÜMÜMDE GÖRÜNMEMEKTEDİR. NE ZAMAN GÖRÜNTÜLENİR? KURUM İLE 1 AY ÖNCE İLE GÖRÜŞME SAĞLADIM; ANCAK İLGİNENEN GÖREVLİNİN DOĞUM İZNİNE AYRILDIĞI SÖYLENMİŞTİR. KONU HAKKINDA İVEDİLİKLE BİLGİ ALMAK İSTİYORUM.";
         
            aciklama=aciklama.replaceAll("ı","i");
            aciklama=aciklama.replaceAll("ş","s");
            aciklama=aciklama.replaceAll("ğ","g");
            aciklama=aciklama.replaceAll("İ","I");
            aciklama=aciklama.replaceAll("Ş","S");
            aciklama=aciklama.replaceAll("Ğ","G");             
            Row row = sheet.createRow(++rowCount);
            int columnCount = 0;
            Cell cell = row.createCell(++columnCount);
            cell.setCellValue((String) item.getAdi().toString());
            Cell cell_email = row.createCell(++columnCount);
            cell_email.setCellValue((String) item.getEmail().toString());
            Cell cell_aciklama = row.createCell(++columnCount);
            cell_aciklama.setCellValue((String) aciklama);
        }
         
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
         
        outputStream.close();


    }



}
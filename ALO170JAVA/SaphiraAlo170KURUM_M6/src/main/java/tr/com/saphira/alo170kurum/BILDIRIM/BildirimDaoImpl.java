package tr.com.saphira.alo170kurum.BILDIRIM;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.KART.vKART;
import tr.com.saphira.alo170kurum.SCRIPT.vSCRIPT;
import tr.com.saphira.alo170kurum.SSS.vSSS;
import tr.com.saphira.alo170kurum.model.vTotal;

@Transactional
@Repository
public class BildirimDaoImpl implements BildirimDao {
	
	 @Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;
	 
	 

	@Override
	public List<vBILDIRIM> getKullaniciHavuzBildirimListesi(Integer skip, Integer take, Integer userId,Map<String,Object> userYetki) {
		String sql1 = "call spKulHavuzBildList (?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take,userId });
		return getList(rows, userYetki);
	}
	@Override
	public  List<vTotal> countKullaniciHavuzBildirimListesi(Integer userId) {
		String sql1 = "call spKulHavuzBildListCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId });
		return getCountList(rows);
	}	
	
 
	@Override
	public List<vBILDIRIM> getUzerimdekiBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		String sql1 = "call spUzerimdekiBildirimler (?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take,userId });
		  
		return getList(rows,userYetki);
	}

	@Override
	public List<vTotal> countUzerimdekiBildirimListesi(Integer userId,Map<String,Object> userYetki) {
		String sql1 = "call spUzerimdekiBildirimlerCount (?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId });
		  
		return getCountList(rows);
	}
	
	
	/******
	 * sorguda g.fk_gorev_statu<>5  olarak kullanılmış. burda kapanmış ve sonradan tekrar acılmış olan işlem goren bildirimlerde gorunuyor. fk_kapatan_agent 
	 * kısmı kullanıcıyla uyuşuyor ise statu 5 haric listeleniyor. mantık hatası olup olmadığı kontrol edilecek.
	 */
	@Override
	public List<vBILDIRIM> SonuclandirdigimBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		String sql1 = "call spSonuclandirdigimBild (?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take,userId });
		  
		return getList(rows, userYetki);
	}
	
	@Override
	public List<vTotal> countSonuclandirdigimBildirimListesi(Integer userId,Map<String,Object> userYetki) {
		String sql1 = "call spSonuclandirdigimBildCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId });
		return getCountList(rows);
	}

	
	@Override
	public List<vBILDIRIM> ActigimBildirimListesi(Integer KullaniciID,Map<String,Object> userYetki) {
		  String sql1 = "call spActigimBildirimler (?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { KullaniciID });
		  
		return getList(rows, userYetki);
	}
	
	@Override
	public List<vBILDIRIM> GuncellenenBildirimListesi(Integer skip,Integer take,Integer KullaniciID,Map<String,Object> userYetki) {
		  String sql1 = "call spGuncellenenBild (?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take,KullaniciID });
		  
		return getList(rows, userYetki);
	}
	
	@Override
	public List<vTotal> countGuncellenenBildirimListesi(Integer userId,Map<String,Object> userYetki) {
		String sql1 = "call spGuncellenenBildCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId });
		return getCountList(rows);
	}


	@Override
	public List<vBILDIRIM> getYenidenAcilanBildirimListesi(Integer skip,Integer take,Integer userLokıasyon,Map<String,Object> userYetki) {
		String sql1 = "call spYenidenAcilanBildList (?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take,userLokıasyon });
		return getList(rows, userYetki);
	}
	@Override
	public List<vTotal> countYenidenAcilanBildirimListesi(Integer userLokıasyon) {
		String sql1 = "call spYenidenAcilanBildListCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userLokıasyon });
		return getCountList(rows);
	}

	@Override
	public List<vBILDIRIM> getOlusturdugumBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		String sql1 = "call spOlusturdugumBildList (?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take,userId });
		return getList(rows, userYetki);
	}
	@Override
	public List<vTotal> countOlusturdugumBildirimListesi(Integer userId) {
		String sql1 = "call spOlusturdugumBildListCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId });
		return getCountList(rows);
	}

	@Override
	public List<vBILDIRIM> getVtAciklamaBildirimListesi(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki) {
		String sql1 = "call spVtAciklamaBildList (?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take,userId });
		  
		return getList(rows, userYetki);
	}
	@Override
	public List<vTotal> countVtAciklamaBildirimListesi(Integer userId) {
		String sql1 = "call spVtAciklamaBildListCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId });
		return getCountList(rows);
	}

	@Override
	public List<vBILDIRIM> getWebSitesiBildirimListesi(Integer skip,Integer take,String userIds,Map<String,Object> userYetki) {
		String sql1 = "call spBildListWebSitesi (?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take,userIds });
		  
		return getList(rows, userYetki);
	}
	@Override
	public List<vTotal> countWebSitesiBildirimListesi(String userIds) {
		String sql1 = "call spBildListWebSitesiCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userIds });
		return getCountList(rows);
	}

	@Override
	public List<vBILDIRIM> getMobilBildirimListesi(Integer skip,Integer take,Map<String,Object> userYetki) {
		String sql1 = "call spBildListMobil (?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take });
		return getList(rows, userYetki);
	}
	@Override
	public List<vTotal> countMobilBildirimListesi() {
		String sql1 = "call spBildListMobilCount ()";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		return getCountList(rows);
	}


		/***
	 * listelerde toplam kayıt sayısı için liste olusturur vTotal class içine aktarır
	 * @param rows
	 * @return
	 */
	private List<vTotal> getCountList(List<Map<String, Object>> rows) {
		List<vTotal> ret = new ArrayList<vTotal>();
		for (Map<?, ?> row : rows) {
			vTotal res = new vTotal();
			try {		
				res.setTotal((Long)row.get("total"));
			} catch (Exception ex) {
					System.out.println("total Hata :" + ex.toString());
			}
			ret.add(res);
		}
		return ret;
	}


	private List<vBILDIRIM> getList(List<Map<String, Object>> rows,Map<String,Object> userYetki) {
		//System.out.println("getList" + rows);
		List<vBILDIRIM> ret = new ArrayList<vBILDIRIM>();

		for (Map<?, ?> row : rows) {
			//System.out.println(row.get("tarih") + "---");
			vBILDIRIM v = new vBILDIRIM();
			try {		
					v.setId((Integer)row.get("gorevid"));
			} catch (Exception ex) {
				 System.out.println("gorevid Hata :" + ex.toString());
			 }
			try {
				if(userYetki.get("bildirim_VatandasBilgisiGoster").equals("true")) {
					String onay=(String)row.get("isOnayBekle").toString();
					if(onay.equals("0")) {
						v.setBasvuruSahibi(row.get("basvuru_sahibi").toString());
					}else {
						v.setBasvuruSahibi("***** *****");
					}
				}else {
					v.setBasvuruSahibi("***** *****");
				}
			} catch (Exception ex) {
				 System.out.println("basvuru_sahibi Hata :" + ex.toString());
			 }
			try {
					v.setBugun(row.get("bugun").toString());
			} catch (Exception ex) {
				 System.out.println("bugun Hata :" + ex.toString());
			 }
			try {
					v.setCls(row.get("ncls").toString());
			} catch (Exception ex) {
				 System.out.println("ncls Hata :" + ex.toString());
			 }
			try {
					v.setFark(row.get("fark").toString());
			} catch (Exception ex) {
				 System.out.println("fark Hata :" + ex.toString());
			 }
			try {
					v.setIlAdi(row.get("iladi").toString());
			} catch (Exception ex) {
				 System.out.println("iladi Hata :" + ex.toString());
			 }
			try {
					v.setIlceAdi(row.get("ilceadi").toString());
					
			} catch (Exception ex) {
				 System.out.println("ilceadi Hata :" + ex.toString());
			 }	
				try {
				v.setFk_agent((Integer)row.get("fk_agent"));
				} catch (Exception ex) {
					 System.out.println("fk_agent Hata :" + ex.toString());
				 }
				try {
				v.setFulldate(row.get("fulldate").toString());
				} catch (Exception ex) {
					 System.out.println("fulldate Hata :" + ex.toString());
				 }

				try {
				v.setHavuzAdi(row.get("havuzadi").toString());
				} catch (Exception ex) {
					 System.out.println("havuzadi Hata :" + ex.toString());
				 }
				try {
				v.setHavuzSure(((Long)row.get("havuzsure")));

				} catch (Exception ex) {
					 System.out.println("havuzsure Hata :" + ex.toString());
				 }
				 
				
				try {
				v.setIsiYapacakAgent(row.get("yapacak_agent").toString());
				} catch (Exception ex) { System.out.println("yapacak_agent Hata :" + ex.toString());}
				try {
				v.setKaydiAcanAgent(row.get("acan_agent").toString());
				} catch (Exception ex) { System.out.println("acan_agent Hata :" + ex.toString());}
				try {
				v.setKonu1(row.get("konu1").toString());
				} catch (Exception ex) { System.out.println("konu1 Hata :" + ex.toString());}
				try {
				v.setKonu2(row.get("konu2").toString());
				} catch (Exception ex) { System.out.println("konu2 Hata :" + ex.toString());}
				try {
				v.setKonu3(row.get("konu3").toString());
				} catch (Exception ex) { System.out.println("konu3 Hata :" + ex.toString());}
				try {
				v.setSaat(row.get("saat").toString());
				} catch (Exception ex) { System.out.println("saat Hata :" + ex.toString());}
				try {
				v.setTarih(row.get("tarih").toString());
				} catch (Exception ex) { System.out.println("tarih Hata :" + ex.toString());}
				try {
				v.setStatu(row.get("statu").toString());
				} catch (Exception ex) { System.out.println("statu Hata :" + ex.toString());}
				try {
					if(userYetki.get("bildirim_VatandasBilgisiGoster").equals("true")) {
						v.setTc(row.get("tc").toString());
					}else {
						v.setTc("*****");
					}
				} catch (Exception ex) { System.out.println("tc Hata :" + ex.toString());}
				try {
					v.setTip(row.get("fk_tip").toString());
				} catch (Exception ex) { System.out.println("tip Hata :" + ex.toString());}

				try {
					v.setTip_level1((Integer)row.get("tip_level1"));
				} catch (Exception ex) { System.out.println("tip_level1 Hata :" + ex.toString());
				}try {
					v.setTip_level2((Integer)row.get("tip_level2"));
				} catch (Exception ex) { System.out.println("tip_level2 Hata :" + ex.toString());
				}try {
					v.setTip_level3((Integer)row.get("tip_level3"));
				} catch (Exception ex) { System.out.println("tip_level3 Hata :" + ex.toString());
				
				}
				try {
					v.setLokasyon((String)row.get("lokasyon"));
				} catch (Exception ex) { 
					System.out.println("lokasyon Hata :" + ex.toString());
				}
				try {
					v.setKonuFull((String)row.get("konu"));
				} catch (Exception ex) { 
					System.out.println("konuFull Hata :" + ex.toString());
				}				
				try {
					v.setisOnayBekle((String)row.get("isOnayBekle"));
				} catch (Exception ex) { 
					System.out.println("isOnayBekle Hata :" + ex.toString());
				}		
				try {
					v.setfkGorevStatu((Integer)row.get("fk_gorev_statu"));
				} catch (Exception ex) { 
					System.out.println("fk_gorev_statu Hata :" + ex.toString());
				}		
				try {
					v.setHavuzSure((Long)row.get("havuzsure"));
				} catch (Exception ex) { 
					System.out.println("sethavuzSure Hata :" + ex.toString());
				}					
				v.sethCode();
				ret.add(v);
		}
		//System.out.println(ret);
		return ret;
	}




	@Override
	public vKART getKart(Integer KartID,Map<String,Object> userYetki) {
		String sql1 = "call spGetKart (?)";
		  Map<String, Object>  rows = jdbcro.queryForMap(sql1,new Object[] { KartID });
		  vKART k = new vKART();
		 // if(userYetki.getbildirim_VatandasBilgisiGoster()=="true") {
			  k.setCinsiyet(rows.get("cinsiyet").toString());
			  k.setKartid((Integer)rows.get("kartid"));
			  k.setKartadi(rows.get("kartadi").toString());
			  k.setTckimlik(rows.get("tckimlik").toString());
			  k.setTel1(rows.get("tel1").toString());
			  k.setTel2(rows.get("tel2").toString());
			  k.setGsm(rows.get("gsm").toString());
			  k.setEmail(rows.get("email").toString());
			  k.setFax(rows.get("fax").toString());
			  k.setDogumtarihi(rows.get("dogumtarihi").toString());
			  k.setCinsiyet(rows.get("cinsiyet").toString());
			  k.setGsoruadi(rows.get("gsoruadi").toString());
			  k.setUlkeadi(rows.get("ulkeadi").toString());
			  k.setIladi(rows.get("iladi").toString());
			  k.setIlceadi(rows.get("ilceadi").toString());
			  k.setSskNo(rows.get("sskno").toString());
			  k.setEdurum(rows.get("edurum").toString());
			
		/*  }else {
			  String encVal="*****";
			  k.setCinsiyet(encVal);
			  k.setKartid((Integer)rows.get("kartid"));
			  k.setKartadi(encVal);
			  k.setTckimlik(encVal);
			  k.setTel1(encVal);
			  k.setTel2(encVal);
			  k.setGsm(encVal);
			  k.setEmail(encVal);
			  k.setFax(encVal);
			  k.setDogumtarihi(encVal);
			  k.setCinsiyet(encVal);
			  k.setGsoruadi(encVal);
			  k.setUlkeadi(encVal);
			  k.setIladi(encVal);
			  k.setIlceadi(encVal);		
			  k.setSskNo(encVal);
			  k.setEdurum(encVal);
		  }*/
		  
		return k;
	}


	
	@Override
	 public vKART getKartByTc(String TC) {
		String sql1 = "call spGetKartByTc (?)";
		vKART k = new vKART();
		try {		
			Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { TC });

			try {		
				k.setKartid((Integer)row.get("kartid"));
			} catch (Exception ex) {
				 System.out.println("kartid Hata :" + ex.toString());
			}
			try {		
				k.setKartadi((String)row.get("kartadi"));
			} catch (Exception ex) {
				 System.out.println("kartadi Hata :" + ex.toString());
			}
			try {		
				k.setKartSoyadi((String)row.get("kartsoyadi"));
			} catch (Exception ex) {
				 System.out.println("kartsoyadi Hata :" + ex.toString());
			}
			try {		
				k.setTckimlik((String)row.get("tckimlik"));
			} catch (Exception ex) {
				 System.out.println("tckimlik Hata :" + ex.toString());
			}
			try {		
				k.setEmail((String)row.get("email"));
			} catch (Exception ex) {
				 System.out.println("kartemail Hata :" + ex.toString());
			}
			
			try {		
				k.setTel1((String)row.get("tel1"));
			} catch (Exception ex) {
				 System.out.println("tel1 Hata :" + ex.toString());
			}
			try {		
				k.setGsm((String)row.get("gsm"));
			} catch (Exception ex) {
				 System.out.println("gsm Hata :" + ex.toString());
			}
			try {		
				k.setFkCinsiyet((Integer)row.get("fk_cinsiyet"));
			} catch (Exception ex) {
				 System.out.println("fk_cinsiyet Hata :" + ex.toString());
			}
			try {		
				k.setFax((String)row.get("fax"));
			} catch (Exception ex) {
				 System.out.println("fax Hata :" + ex.toString());
			}
			try {		
				k.setFkEgitimDurumu((Integer)row.get("fk_egitimdurumu"));
			} catch (Exception ex) {
				 System.out.println("fk_egitimdurumu Hata :" + ex.toString());
			}
			try {		
				k.setFkIl((Integer)row.get("fk_il"));
			} catch (Exception ex) {
				 System.out.println("fk_il Hata :" + ex.toString());
			}
			try {		
				k.setFkIlce((Integer)row.get("fk_ilce"));
			} catch (Exception ex) {
				 System.out.println("fk_ilce Hata :" + ex.toString());
			}
			try {		
				k.setSskNo((String)row.get("sskno"));
			} catch (Exception ex) {
				 System.out.println("sskno Hata :" + ex.toString());
			}		
			
		} catch (Exception ex) {
			 System.out.println(" Hata :" + ex.toString());
		} 
		return k;
		  
		
		
		

	 }


	private vBILDIRIM getTaskRow(Map<String, Object> row,Map<String,Object> userYetki) {
		vBILDIRIM v = new vBILDIRIM();
		
		try {		
			v.setId((Integer)row.get("gorevid"));
		} catch (Exception ex) {
			 System.out.println("gorevid Hata :" + ex.toString());
		 }

		try {
				v.setBugun(row.get("bugun").toString());
		} catch (Exception ex) {
			 System.out.println("bugun Hata :" + ex.toString());
		 }
		try {
			v.setCls(row.get("oncelik_class").toString());
		} catch (Exception ex) {
			 System.out.println("oncelik_class Hata :" + ex.toString());
		 }
		try {
				v.setFark(row.get("fark").toString());
		} catch (Exception ex) {
			 System.out.println("fark Hata :" + ex.toString());
		 }
	
		try {
			v.setFk_agent((Integer)row.get("fk_agent"));
		} catch (Exception ex) {
			 System.out.println("getTaskRow - fk_agent Hata :" + ex.toString());
		}
		try {
			v.setFk_kart(((Long)row.get("fk_kart")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_kart Hata :" + ex.toString());
		}

		try {
			v.setHavuzAdi(row.get("havuzadi").toString());
		} catch (Exception ex) {
			 System.out.println("havuzadi Hata :" + ex.toString());
		 }
		try {
			v.setSubeAdi(row.get("subeadi").toString());
		} catch (Exception ex) {
			 System.out.println("subeadi Hata :" + ex.toString());
		 }

		try {
			v.setfkHavuz(((int)row.get("fk_havuz")));
		} catch (Exception ex) {
			 System.out.println("fk_havuz Hata :" + ex.toString());
		}

		try {
			v.setIsiYapacakAgent(row.get("yapacak_agent").toString());
		} catch (Exception ex) { System.out.println("yapacak_agent Hata :" + ex.toString());}
		try {
			
			v.setKaydiAcanAgent(row.get("kaydi_acan").toString());
		} catch (Exception ex) { System.out.println("getTaskRow - acan_agent Hata :" + ex.toString());}
		
		try {
			v.setKonu1(row.get("konu1").toString());
		} catch (Exception ex) { System.out.println("konu1 Hata :" + ex.toString());}
		try {
			v.setKonu2(row.get("konu2").toString());
		} catch (Exception ex) { System.out.println("konu2 Hata :" + ex.toString());}
		try {
			v.setKonu3(row.get("konu3").toString());
		} catch (Exception ex) { System.out.println("konu3 Hata :" + ex.toString());}
		try {
			v.setSaat(row.get("saat").toString());
		} catch (Exception ex) { System.out.println("saat Hata :" + ex.toString());}
		try {
			v.setTarih(row.get("tarih").toString());
		} catch (Exception ex) { System.out.println("tarih Hata :" + ex.toString());}
		try {
			v.setStatu(row.get("statu").toString());
		} catch (Exception ex) { System.out.println("statu Hata :" + ex.toString());}
		try {
			v.setCls(row.get("statu_class").toString());
		} catch (Exception ex) { System.out.println("statu_class Hata :" + ex.toString());}
		try {
			v.setAciklama(row.get("aciklama").toString());
		} catch (Exception ex) { System.out.println("Aciklama Hata :" + ex.toString());}
		try {
			v.setTip_level1((Integer)row.get("tip_level1"));
		} catch (Exception ex) { System.out.println("tip_level1 Hata :" + ex.toString());
		}try {
			v.setTip_level2((Integer)row.get("tip_level2"));
		} catch (Exception ex) { System.out.println("tip_level2 Hata :" + ex.toString());
		}try {
			v.setTip_level3((Integer)row.get("tip_level3"));
		} catch (Exception ex) { System.out.println("tip_level3 Hata :" + ex.toString());
		}try {
			v.setfkStatu((Integer)row.get("fk_statu"));
		} catch (Exception ex) { System.out.println("fk_statu Hata :" + ex.toString());

		}try {
			Genel.encrypt(row.get("aciklama").toString());
			v.setAciklama(row.get("aciklama").toString());
		}catch(Exception ex) {
			
		}

		try {
			v.setfkIl((int)(row.get("fk_il")));
		}catch
			(Exception ex) { System.out.println("fk_il Hata :" + ex.toString());
		}
		try {
			v.setfkHavuz((int)(row.get("fk_havuz")));
		}catch
			(Exception ex) { System.out.println("fk_havuz Hata :" + ex.toString());
		}
		try {
			v.setfkSube((int)(row.get("subeid")));
		}catch
			(Exception ex) { System.out.println("getTaskRow - fk_sube Hata :" + ex.toString());
			
		}	
		try {
			v.setisOnayBekle((String)row.get("isOnayBekle"));
		} catch (Exception ex) { 
			System.out.println("isOnayBekle Hata :" + ex.toString());
		}		
		
		try {
			v.setTip_level1((int)(row.get("tip_level1")));
		}catch
			(Exception ex) { System.out.println("tip_level1 Hata :" + ex.toString());
		}
		try {
			v.setTip_level2((int)(row.get("tip_level2")));
		}catch
			(Exception ex) { System.out.println("tip_level2 Hata :" + ex.toString());
		}
		try {
			v.setTip_level3((int)(row.get("tip_level3")));
		}catch
			(Exception ex) { System.out.println("tip_level3 Hata :" + ex.toString());
		}
		try {
			v.setFk_agent((int)(row.get("fk_agent")));
		}catch
			(Exception ex) { System.out.println("fk_agent Hata :" + ex.toString());
		}	

		try {
			
			v.setaramakanali((String)(row.get("arama_kanali")));
		}catch
			(Exception ex) { System.out.println("aramakanali Hata :" + ex.toString());
		}	
		
		try {
			v.setaramakanalituru((String)(row.get("arama_kanali_turu")));
		}catch
			(Exception ex) { System.out.println("aramakanalituru Hata :" + ex.toString());
		}	
		
		try {
			v.setaramasebebi((String)(row.get("asebep")));
		}catch
			(Exception ex) { System.out.println("aramasebebi Hata :" + ex.toString());
		}		

		try {
			v.setf_aramasebebi((Integer)row.get("fk_aramasebebi"));
		}catch
			(Exception ex) { System.out.println("fk_aramasebebi Hata :" + ex.toString());
		}

		try {
			v.set_fkProje((Integer)row.get("fk_proje"));
		}catch
			(Exception ex) { System.out.println("fk_proje Hata :" + ex.toString());
		}

		try {
			v.setfkGorevStatu((Integer)(row.get("fk_gorev_statu")));
		}catch
			(Exception ex) { System.out.println("fk_gorev_statu Hata :" + ex.toString());
		}			
		
		try {
			v.setf_id((Long)(row.get("f_id")));
			
		}catch
			(Exception ex) { System.out.println("f_id Hata :" + ex.toString());
		}		

		try {
			v.setf_adi((String)(row.get("f_adi")));
		}catch
			(Exception ex) { System.out.println("f_adi Hata :" + ex.toString());
		}	
		
		try {
			v.setf_adres((String)(row.get("f_adres")));
		}catch
			(Exception ex) { System.out.println("f_adres Hata :" + ex.toString());
		}	

		try {
			v.setf_tel((String)(row.get("f_tel")));
		}catch
			(Exception ex) { System.out.println("f_tel Hata :" + ex.toString());
		}	

		try {
			v.setnace((String)(row.get("nace")));
		}catch
			(Exception ex) { System.out.println("nace Hata :" + ex.toString());
		}	

		try {
			v.setscs((String)(row.get("scs")));
		}catch
			(Exception ex) { System.out.println("scs Hata :" + ex.toString());
		}	

		try {
			v.setf_il((String)(row.get("f_il")));
		}catch
			(Exception ex) { System.out.println("f_il Hata :" + ex.toString());
		}	

		try {
			v.setf_ilce((String)(row.get("f_ilce")));
		}catch
			(Exception ex) { System.out.println("f_ilce Hata :" + ex.toString());
		}	

			v.sethCode();
		
		return v;
	}
	@Override
	public vBILDIRIM getTask(Integer TaskID,Map<String,Object> userYetki) {
		String sql = "call spGetTask (?)";
		Map<String, Object> row = jdbcro.queryForMap(sql,new Object[] {TaskID});
		return getTaskRow(row,userYetki);
	}
	
	@Override
	public List<vBILDIRIM> getBildirimAra(Integer skip,Integer take,Integer userId,Map<String,Object> userYetki,Integer Statu,String Adi,String SoyAdi,String Tel,String Tc,Integer BildirimNo) {
		String sql = "call spBildirimAra (?,?,?,?,?,?,?,?)";
		List<Map<String, Object>> list = jdbcro.queryForList(sql,new Object[] {skip,take,Adi,SoyAdi,Tel,Tc,BildirimNo,Statu });
		
		return getList(list, userYetki);
	}

	@Override
	public List<vTotal> countBildirimAra(Integer userId,Map<String,Object> userYetki,Integer Statu,String Adi,String SoyAdi,String Tel,String Tc,Integer BildirimNo) {
		String sql1 = "call spBildirimAraCount (?,?,?,?,?,?)";
		// System.out.println("call spBildirimAraCount ("+Adi+","+SoyAdi+","+Tc+","+Tel+","+BildirimNo+","+Statu+")");
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { Adi,SoyAdi,Tel,Tc,BildirimNo,Statu });
		return getCountList(rows);
	}


	@Override
	public List<vBILDIRIM> BildirimDetayRaporu(Integer AGENT, Integer IL, Integer ILCE, Integer STATU,Map<String,Object> userYetki) {
		  String sql1 = "call rptBildirimDetay (?,?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { AGENT, IL, ILCE, STATU });
		  
		return getList(rows, userYetki);
	}

	@Override
	public List<vBILDIRIM> BildirimSureRaporu(String ilkTAR, String sonTAR, Integer HAVUZ) {
	
		return null;
	}

	@Override
	public List<vBILDIRIM> KurumYetkilisiSureRaporu(String ilkTAR, String sonTAR, Integer TIP) {
		
		return null;
	}

	@Override
	public List<vBILDIRIMDETAY> getBildirimDetay(Integer TaskID,Integer fkHavuz) {
		  String sql1 = "call spGetTaskDetay (?,?)";
		  if(fkHavuz<1){
			  fkHavuz=0;
		  }
		  List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { TaskID,fkHavuz });
		  
		  List<vBILDIRIMDETAY> ret = new ArrayList<vBILDIRIMDETAY>();
		  
			for (Map<String, Object> row : list ) {
				
				vBILDIRIMDETAY a = new vBILDIRIMDETAY();	
			
			try {		
				a.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		 }
			try {		
				a.setAciklama(row.get("aciklama").toString());
		} catch (Exception ex) {
			 System.out.println("aciklama Hata :" + ex.toString());
		 }
			try {		
				a.setAgent(row.get("agent").toString());
		} catch (Exception ex) {
			 System.out.println("agent Hata :" + ex.toString());
		 }
			try {		
				a.setExt(row.get("ext").toString());
		} catch (Exception ex) {
			 System.out.println("ext Hata :" + ex.toString());
		 }
			try {		
				a.setLokasyon(row.get("lokasyon").toString());
		} catch (Exception ex) {
			 System.out.println("location Hata :" + ex.toString());
		 }
			try {		
				a.setRol_adi(row.get("rol_adi").toString());
		} catch (Exception ex) {
			 System.out.println("role_adi Hata :" + ex.toString());
		 }
			try {		
				 a.setTarih(row.get("tarih").toString());
		} catch (Exception ex) {
			 System.out.println("tarih Hata :" + ex.toString());
		 }
			try {		
				a.setSaat(row.get("saat").toString());
		} catch (Exception ex) {
			 System.out.println("saat Hata :" + ex.toString());
		 }
			try {		
				a.setCls(row.get("cls").toString());
		} catch (Exception ex) {
			 System.out.println("cls Hata :" + ex.toString());
		 }
			try {		
				a.setFark(row.get("fark").toString());
		} catch (Exception ex) {
			 System.out.println("fark Hata :" + ex.toString());
		 }
			try {		
				a.setIslemtipi(row.get("islemtipi").toString());
		} catch (Exception ex) {
			 System.out.println("islemtipi Hata :" + ex.toString());
		 }try {
				a.setIsEnc((Integer)row.get("isEnc"));
				
				if(a.getIsEnc().equals(1)) {
					a.setAciklama(Genel.decrypt(row.get("cryp_aciklama").toString()));
					a.setCryp_aciklama(row.get("aciklama").toString());
				}
			} catch (Exception ex) { System.out.println("isEnc Hata :" + ex.toString());
			}try {
				a.setCryp_aciklama(Genel.decrypt(row.get("cryp_aciklama").toString()));
			}catch
				(Exception ex) { System.out.println("cryp_aciklama Hata :" + ex.toString());
			}
			
		ret.add(a);
		
	  }
		return ret;
		
	}



	@Override
	public List<vBILDIRIMDETAY> getBildirimDetayGuncelleme(Integer TaskID) {
		  String sql1 = "call spGetTaskDetayGuncelleme (?)";
		  List<Map<String, Object>> list = jdbcro.queryForList(sql1,new Object[] { TaskID });
		  
		  List<vBILDIRIMDETAY> ret = new ArrayList<vBILDIRIMDETAY>();
		  
			for (Map<String, Object> row : list ) {
				
				vBILDIRIMDETAY a = new vBILDIRIMDETAY();	
			
			try {		
				a.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		 }
			try {		
				a.setAciklama(row.get("aciklama").toString());
		} catch (Exception ex) {
			 System.out.println("aciklama Hata :" + ex.toString());
		 }
			try {		
				a.setAgent(row.get("agent").toString());
		} catch (Exception ex) {
			 System.out.println("agent Hata :" + ex.toString());
		 }
			try {		
				a.setExt(row.get("ext").toString());
		} catch (Exception ex) {
			 System.out.println("ext Hata :" + ex.toString());
		 }
			try {		
				a.setLokasyon(row.get("lokasyon").toString());
		} catch (Exception ex) {
			 System.out.println("location Hata :" + ex.toString());
		 }
			try {		
				a.setRol_adi(row.get("rol_adi").toString());
		} catch (Exception ex) {
			 System.out.println("role_adi Hata :" + ex.toString());
		 }
			try {		
				 a.setTarih(row.get("tarih").toString());
		} catch (Exception ex) {
			 System.out.println("tarih Hata :" + ex.toString());
		 }
			try {		
				a.setSaat(row.get("saat").toString());
		} catch (Exception ex) {
			 System.out.println("saat Hata :" + ex.toString());
		 }
			try {		
				a.setCls(row.get("cls").toString());
		} catch (Exception ex) {
			 System.out.println("cls Hata :" + ex.toString());
		 }
			try {		
				a.setFark(row.get("fark").toString());
		} catch (Exception ex) {
			 System.out.println("fark Hata :" + ex.toString());
		 }
			try {		
				a.setIslemtipi(row.get("islemtipi").toString());
		} catch (Exception ex) {
			 System.out.println("islemtipi Hata :" + ex.toString());
		 }try {
				a.setIsEnc((Integer)row.get("isEnc"));
				
				if(a.getIsEnc().equals(1)) {
					a.setAciklama(Genel.decrypt(row.get("cryp_aciklama").toString()));
					a.setCryp_aciklama(row.get("aciklama").toString());
				}
			} catch (Exception ex) { System.out.println("isEnc Hata :" + ex.toString());
			}try {
				a.setCryp_aciklama(Genel.decrypt(row.get("cryp_aciklama").toString()));
			}catch
				(Exception ex) { System.out.println("cryp_aciklama Hata :" + ex.toString());
			}
			
		ret.add(a);
		
	  }
		return ret;
		
	}




	@Override
	public void TCSifrele() {
		String sql1 = "call tempGetTCList ()";
		  List<Map<String, Object>> list = jdbcro.queryForList(sql1);
		  
		  for (Map<String, Object> row : list ) {
			  System.out.println(row.get("id").toString() + " " + row.get("tckimlik").toString());
			  
			  jdbcrw.update("call tempSetTCList(?,?)", new Object[] { (Integer)row.get("id"), Genel.encrypt(row.get("tckimlik").toString()) });
		  }
		  
		  sql1 = "select id, username, password from alo170data.saphira_users where lpass='-' limit 1000";
		  List<Map<String, Object>> listUser = jdbcro.queryForList(sql1);
		  
		  for (Map<String, Object> row : listUser ) {
			  System.out.println(row.get("id").toString() + " " + row.get("username").toString());
			  
			  
			  jdbcro.update("update alo170data.saphira_users set lpass=?, luser=? where id =?", new Object[] { Genel.encrypt(row.get("password").toString()), Genel.encrypt(row.get("username").toString()), (Integer)row.get("id") });
			  //jdbcrw.update("call tempSetTCList(?,?)", new Object[] { (Integer)row.get("id"), Genel.encrypt(row.get("tckimlik").toString()) });
		  }
		  
	}

	@Override
	public vADET adetUzerimdekiBild(Integer AGENT) {
		String sql1 = "call adetUzerimdekiBildirimler (?)";
		Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { AGENT });
		
		vADET v = new vADET();
		try {		
			v.setId(((Long)row.get("id")).intValue());
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}
		return v;
	}

	@Override
	public List<vBILDIRIM> exportExcel(Integer AGENT, String ADI, String SOYADI, String TC, String TEL, Integer TaskID,
			Integer STATU,Map<String,Object> userYetki) {
		String sql = "call spBildirimAra (?,?,?,?,?,?,?)";
		List<Map<String, Object>> list = jdbcro.queryForList(sql,new Object[] {AGENT,  ADI, SOYADI, TC, TEL, TaskID, STATU });
		
		for (Map<String, Object> row : list ) {
			vBILDIRIM d = new vBILDIRIM();
			
			
		try {		
			d.setId(((Long)row.get("id")).intValue());
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}
		try {		
				d.setBasvuruSahibi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		 }
			try {		
				d.setBasvuruSahibi(row.get("soyadi").toString());
		} catch (Exception ex) {
			 System.out.println("soyadi Hata :" + ex.toString());
		 }
			try {		
				d.setTarih(row.get("tarih").toString());
		} catch (Exception ex) {
			 System.out.println("tarih Hata :" + ex.toString());
		 }
			try {		
				d.setIsiYapacakAgent(row.get("isiyapacakagent").toString());
		} catch (Exception ex) {
			 System.out.println("isiyapacakagent Hata :" + ex.toString());
		 }
			try {		
				d.setKonu1(row.get("konu1").toString());
		} catch (Exception ex) {
			 System.out.println("konu1 Hata :" + ex.toString());
		 }
			try {		
				d.setKonu2(row.get("konu2").toString());
		} catch (Exception ex) {
			 System.out.println("konu2 Hata :" + ex.toString());
		 }
			try {		
				d.setKonu3(row.get("konu3").toString());
		} catch (Exception ex) {
			 System.out.println("konu3 Hata :" + ex.toString());
		 }
			try {		
				d.setStatu(row.get("statu").toString());
		} catch (Exception ex) {
			 System.out.println("statu Hata :" + ex.toString());
		 }
			try {		
				d.setIlAdi(row.get("iladi").toString());
		} catch (Exception ex) {
			 System.out.println("iladi Hata :" + ex.toString());
		 }
			try {		
				d.setTc(row.get("tc").toString());
		} catch (Exception ex) {
			 System.out.println("tc Hata :" + ex.toString());
		 }
			try {		
				d.setTel(row.get("tel").toString());
		} catch (Exception ex) {
			 System.out.println("tel Hata :" + ex.toString());
		 }
			try {		
				d.setFk_kart((Integer)row.get("bildirim_no"));
		} catch (Exception ex) {
			 System.out.println("bildirim_no Hata :" + ex.toString());
		 }
			try {		
				d.setHavuzSure((Long)row.get("havuzsure"));
		} catch (Exception ex) {
			 System.out.println("havuzsure Hata :" + ex.toString());
		 }
			
		d.sethCode();
		
		}
	  
		return getList(list,userYetki);
		
	}
	
	
	
	 public List<vIVRTC> getIvrTc(String CALLID) {
		  String sql1 = "call ivrTC (?)";
		  List<vIVRTC> ret = new ArrayList<vIVRTC>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { CALLID });
		  for (Map<?, ?> row : rows) {
				
			  vIVRTC v = new vIVRTC();
		try {		
				v.setCallid((String)row.get("callid"));
		} catch (Exception ex) {
			 System.out.println("callid Hata :" + ex.toString());
		}try {		
			v.setTc(row.get("tc").toString());
		} catch (Exception ex) {
			 System.out.println("tc Hata :" + ex.toString());
		}try {		
			v.setQue(row.get("que").toString());
		} catch (Exception ex) {
			 System.out.println("que Hata :" + ex.toString());
		}
		try {		
			v.setTarih((String)row.get("tarih"));
		} catch (Exception ex) {
			 System.out.println("tarih Hata :" + ex.toString());
		}
		try {		
			v.setMenu((String)row.get("menu"));
		} catch (Exception ex) {
			 System.out.println("menu Hata :" + ex.toString());
		}		
		
		ret.add(v);
	} 
		  return ret;
		  
	 }
	 
	 
		@Override
		public List<vBILDIRIM> getBildirimGecmisi(Integer kartId,Map<String,Object> userYetki) {
			String sql1 = "call spKartBildirimGecmisi (?)";
			  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { kartId });
			  
			return getList(rows, userYetki);
		}
		
		@Override
		public List<vBILDIRIM> bildirimKaydet(
			
						    // bildirim bilgileri
							String aciklama,Integer tip_level1,Integer tip_level2,Integer tip_level3,String arayan_numara,Integer fk_aramakanali,Integer fk_aramasebebi,Integer fk_kart,
							String uid,Integer fk_proje,Integer fk_sonuckod,Integer fk_lokasyon,Integer isOnayBekle,Integer fk_kaydi_acan,String referans,Integer fk_gorev_statu,
							Integer fk_kapatan_agent,
							// vatandaş bilgileri
							String adres,String adi,String soyadi,String tckimlik,String tel1,String gsm,Integer fk_egitimdurumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,
							String sskno,String email,Integer fk_cinsiyet,String fax,Integer fk_gsoru,String gsoru_cevap,
							// firma bilgileri
							String f_adi,String f_adres,String f_tel,Integer f_fk_il,Integer f_fk_ilce,String nace,String scs,
						
							// kart ve firma update kontrol
							Integer kart_update,Integer firma_bilgi,
							Map<String,Object> userYetki
							) {
						
							String sql1 = "call bildirimKaydet (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			  				List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { 
								aciklama,tip_level1,tip_level2,tip_level3,arayan_numara,fk_aramakanali,fk_aramasebebi,fk_kart,
								uid,fk_proje,fk_sonuckod,fk_lokasyon,isOnayBekle,fk_kaydi_acan,referans,fk_gorev_statu,fk_kapatan_agent,
								
								// vatandaş
								 adres,adi,soyadi,tckimlik,tel1,gsm,fk_egitimdurumu,fk_ulke,fk_il,fk_ilce,
								 sskno,email,fk_cinsiyet,fax,fk_gsoru,gsoru_cevap,
								 // firma
								 f_adi,f_adres,f_tel,f_fk_il,f_fk_ilce,nace,scs,
								 
								 // kart ve firma update kontrol
								 kart_update,firma_bilgi
								
							   });
			  
							return getList(rows, userYetki);
		}


		
			public void karaListeEkle(String numara,String neden,Integer fk_agent_id) {
				String sql1 = "call karaListeEkle (?,?,?)";
				jdbcro.update(sql1,new Object[] { 
					numara,neden,fk_agent_id	
				});
			}
	
			
			
			@Override
			public List<vTotal> countBilgiBankasiListe(String term) {
				String sql1 = "call spSSSCount (?)";
				List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { term });
				return getCountList(rows);
			}
			
			@Override
			public List<vSSS> getBilgiBankasiListe(String term,Integer skip,Integer take) {
				  String sql1 = "call spSSS (?,?,?)";
				  List<vSSS> ret = new ArrayList<vSSS>();
				  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { term,skip,take});
				  for (Map<?, ?> row : rows) {
					  
					  vSSS v = new vSSS();				

						try {		
							v.setId((Integer)row.get("id"));
						} catch (Exception ex) {
							 System.out.println("id Hata :" + ex.toString());
						}	
						try {		
							v.setfk_sss_grup((Integer)row.get("fk_sss_grup"));
						} catch (Exception ex) {
							 System.out.println("fk_sss_grup Hata :" + ex.toString());
						}	
						try {		
							v.setfk_sss_altgrup((Integer)row.get("fk_sss_altgrup"));
						} catch (Exception ex) {
							 System.out.println("fk_sss_altgrup Hata :" + ex.toString());
						}	
						try {		
							v.setfk_agent((Integer)row.get("fk_agent"));
						} catch (Exception ex) {
							 System.out.println("fk_agent Hata :" + ex.toString());
						}	
						try {		
							v.setisWeb((Integer)row.get("isWeb"));
						} catch (Exception ex) {
							 System.out.println("isWeb Hata :" + ex.toString());
						}	
						try {		
							v.setwww_show((Integer)row.get("www_show"));
						} catch (Exception ex) {
							 System.out.println("www_show Hata :" + ex.toString());
						}	
						try {		
							v.setIsActive((Integer)row.get("isActive"));
						} catch (Exception ex) {
							 System.out.println("isActive Hata :" + ex.toString());
						}	
		
						try {		
							v.setbaslik((String)row.get("baslik"));
						} catch (Exception ex) {
							 System.out.println("baslik Hata :" + ex.toString());
						}
						try {		
							v.setaciklama((String)row.get("aciklama"));
						} catch (Exception ex) {
							 System.out.println("aciklama Hata :" + ex.toString());
						}
						try {		
							v.setkisa_aciklama((String)row.get("kisa_aciklama"));
						} catch (Exception ex) {
							 System.out.println("kisa_aciklama Hata :" + ex.toString());
						}	
						try {		
							v.setlng((String)row.get("lng"));
						} catch (Exception ex) {
							 System.out.println("lng Hata :" + ex.toString());
						}							
					  ret.add(v);
				  } 
				  

				  return ret;
			}
			
			
			@Override
			 public vSSS getSssById(Integer ID) {
				String sql1 = "call spGetSSSById (?)";
				vSSS k = new vSSS();
				try {		
					Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });

					try {		
						k.setId((Integer)row.get("sssid"));
					} catch (Exception ex) {
						 System.out.println("sssid Hata :" + ex.toString());
					}
					try {		
						k.setbaslik((String)row.get("baslik"));
					} catch (Exception ex) {
						 System.out.println("baslik Hata :" + ex.toString());
					}					
					try {		
						k.setaciklama((String)row.get("aciklama"));
					} catch (Exception ex) {
						 System.out.println("aciklama Hata :" + ex.toString());
					}							
					
				} catch (Exception ex) {
					 System.out.println(" Hata :" + ex.toString());
				} 
				return k;
			 }
			
			
			
			
			
			@Override
			public List<vTotal> countBildirimScriptListe(String term) {
				String sql1 = "call spBildirimScriptCount (?)";
				List<Map<String, Object>>   rows = jdbcro.queryForList(sql1,new Object[] { term });
				return getCountList(rows);
			}
			
			@Override
			public List<vSCRIPT> getBildirimScriptListe(String term,Integer skip,Integer take) {
				  String sql1 = "call spBildirimScript (?,?,?)";
				  List<vSCRIPT> ret = new ArrayList<vSCRIPT>();
				  List<Map<String, Object>>  rows;
				  rows = jdbcro.queryForList(sql1,new Object[] { term,skip,take});
				  try {
					  
					  for (Map<?, ?> row : rows) {
						
						  vSCRIPT v = new vSCRIPT();				

							try {		
								v.setId((Integer)row.get("id"));
							} catch (Exception ex) {
								 System.out.println("id Hata :" + ex.toString());
							}	
							try {		
								v.setFkParent((Integer)row.get("fk_parent"));
							} catch (Exception ex) {
								 System.out.println("fk_parent Hata :" + ex.toString());
							}							
							try {		
								v.setFkAramaSebebi((Integer)row.get("fk_aramasebebi"));
							} catch (Exception ex) {
								 System.out.println("fk_aramasebebi Hata :" + ex.toString());
							}	
							try {		
								v.setTipLevel1((Integer)row.get("tip_level1"));
							} catch (Exception ex) {
								 System.out.println("tip_level1 Hata :" + ex.toString());
							}	
							try {		
								v.setTipLevel2((Integer)row.get("tip_level2"));
							} catch (Exception ex) {
								 System.out.println("tip_level2 Hata :" + ex.toString());
							}	
							try {		
								v.setTipLevel3((Integer)row.get("tip_level1"));
							} catch (Exception ex) {
								 System.out.println("tip_level1 Hata :" + ex.toString());
							}	
							try {		
								v.setisActive((Integer)row.get("isActive"));
							} catch (Exception ex) {
								 System.out.println("isActive Hata :" + ex.toString());
							}		
							try {		
								v.setScript((String)row.get("script"));
							} catch (Exception ex) {
								 System.out.println("script Hata :" + ex.toString());
							}	
							try {		
								v.setAciklama((String)row.get("aciklama"));
							} catch (Exception ex) {
								 System.out.println("aciklama Hata :" + ex.toString());
							}							
							try {		
								v.setSkAdi((String)row.get("skadi"));
							} catch (Exception ex) {
								 System.out.println("skadi Hata :" + ex.toString());
							}
							try {		
								v.setKnAdi((String)row.get("knadi"));
							} catch (Exception ex) {
								 System.out.println("knadi Hata :" + ex.toString());
							}
							try {		
								v.setKonu((String)row.get("konu"));
							} catch (Exception ex) {
								 System.out.println("konu Hata :" + ex.toString());
							}
							try {		
								v.setAltKonu((String)row.get("alt_konu"));
							} catch (Exception ex) {
								 System.out.println("alt_konu Hata :" + ex.toString());
							}
							try {		
								v.setAltKonuDetay((String)row.get("alt_konu_detay"));
							} catch (Exception ex) {
								 System.out.println("alt_konu_detay Hata :" + ex.toString());
							}						
						  ret.add(v);
					  } 					
					} catch (Exception e) {
						
					}

				  

				  return ret;
			}
			
			
			@Override
			public List<vKULLANICILAR> getVtMesajKullaniciListe(String term,Integer UserId) {
				  String sql1 = "call spVtMesajKullaniciListe (?,?)";
				  List<vKULLANICILAR> ret = new ArrayList<vKULLANICILAR>();
				  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { term,UserId });
				  for (Map<?, ?> row : rows) {
					  
					  vKULLANICILAR v = new vKULLANICILAR();				

						try {		
							v.setId((Integer)row.get("id"));
						} catch (Exception ex) {
							 System.out.println("id Hata :" + ex.toString());
						}	
						try {		
							v.setAdi((String)row.get("adi"));
						} catch (Exception ex) {
							 System.out.println("adi Hata :" + ex.toString());
						}							
												
					  ret.add(v);
				  } 
				  

				  return ret;
			}
			
			
			
			@Override
			public List<vVTMESAJ> getVtMesajListe(String tip,Integer skip,Integer take,Integer userId) {
				
				  String sql1 = "call spVtMesajListe (?,?,?,?)";
				  List<vVTMESAJ> ret = new ArrayList<vVTMESAJ>();
				  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { tip,skip,take,userId });
				  for (Map<?, ?> row : rows) {
					  
					  vVTMESAJ v = new vVTMESAJ();				

						try {		
							v.setid((Integer)row.get("id"));
						} catch (Exception ex) {
							 System.out.println("id Hata :" + ex.toString());
						}	
						try {		
							v.setmesaj((String)row.get("mesaj"));
						} catch (Exception ex) {
							 System.out.println("mesaj Hata :" + ex.toString());
						}	
						try {		
							v.setAlici((String)row.get("alici"));
						} catch (Exception ex) {
							 System.out.println("alici Hata :" + ex.toString());
						}	
						try {		
							v.setGonderen((String)row.get("gonderen"));
						} catch (Exception ex) {
							 System.out.println("gonderen Hata :" + ex.toString());
						}	
						try {		
						    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						    new Date();
							v.settarih((String)dateFormat.format(row.get("tarih")));
							
						} catch (Exception ex) {
							 System.out.println("tarih Hata :" + ex.toString());
						}		
						try {		
							v.setKullanici((String)row.get("kullanici"));
						} catch (Exception ex) {
							 System.out.println("kullanici Hata :" + ex.toString());
						}	
						try {		
							v.setfk_gonderen((Integer)row.get("fk_gonderen"));
						} catch (Exception ex) {
							 System.out.println("fk_gonderen Hata :" + ex.toString());
						}						
						try {		
							v.setfk_alici((Integer)row.get("fk_alici"));
						} catch (Exception ex) {
							 System.out.println("fk_alici Hata :" + ex.toString());
						}	
						try {		
							v.setfk_parent((Integer)row.get("fk_parent"));
						} catch (Exception ex) {
							 System.out.println("fk_parent Hata :" + ex.toString());
						}	
						try {		
							v.setisroot((Integer)row.get("isroot"));
						} catch (Exception ex) {
							 System.out.println("isroot Hata :" + ex.toString());
						}	
						try {		
							v.setfk_root((Integer)row.get("fk_root"));
						} catch (Exception ex) {
							 System.out.println("fk_root Hata :" + ex.toString());
						}	
						try {		
							v.setisActive((Integer)row.get("isActive"));
						} catch (Exception ex) {
							 System.out.println("isActive Hata :" + ex.toString());
						}	
						try {		
							v.setIsReplied((Integer)row.get("isReplied"));
						} catch (Exception ex) {
							 System.out.println("isReplied Hata :" + ex.toString());
						}							
					  ret.add(v);
				  } 
				  

				  return ret;
			}
			
			@Override
			public List<vTotal> countVtMesajListe(String tip,Integer userId) {
				String sql1 = "call spVtMesajListeCount (?,?)";
				List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { tip,userId });
				return getCountList(rows);
			}
			
			
			
			@Override
			 public vVTMESAJ getVtMesajById(Integer ID) {
				String sql1 = "call spGetVtMesajById (?)";
				vVTMESAJ k = new vVTMESAJ();
				try {		
					Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });

					try {		
						k.setid((Integer)row.get("id"));
					} catch (Exception ex) {
						 System.out.println("id Hata :" + ex.toString());
					}
					try {		
						k.setGonderen((String)row.get("gonderen"));
					} catch (Exception ex) {
						 System.out.println("gonderen Hata :" + ex.toString());
					}					
					try {		
						k.setAlici((String)row.get("alici"));
					} catch (Exception ex) {
						 System.out.println("alici Hata :" + ex.toString());
					}							
					try {		
						k.setmesaj((String)row.get("mesaj"));
					} catch (Exception ex) {
						 System.out.println("mesaj Hata :" + ex.toString());
					}		
					try {		
					    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						k.settarih((String)dateFormat.format(row.get("tarih")));
						
					} catch (Exception ex) {
						 System.out.println("tarih Hata :" + ex.toString());
					}		
					
					try {		
						k.setfk_gonderen((Integer)row.get("fk_gonderen"));
					} catch (Exception ex) {
						 System.out.println("fk_gonderen Hata :" + ex.toString());
					}						
					try {		
						k.setfk_alici((Integer)row.get("fk_alici"));
					} catch (Exception ex) {
						 System.out.println("fk_alici Hata :" + ex.toString());
					}	
					try {		
						k.setfk_parent((Integer)row.get("fk_parent"));
					} catch (Exception ex) {
						 System.out.println("fk_parent Hata :" + ex.toString());
					}	
					try {		
						k.setisroot((Integer)row.get("isroot"));
					} catch (Exception ex) {
						 System.out.println("isroot Hata :" + ex.toString());
					}	
					try {		
						k.setfk_root((Integer)row.get("fk_root"));
					} catch (Exception ex) {
						 System.out.println("fk_root Hata :" + ex.toString());
					}	
					try {		
						k.setisActive((Integer)row.get("isActive"));
					} catch (Exception ex) {
						 System.out.println("isActive Hata :" + ex.toString());
					}	
					try {		
						k.setIsReplied((Integer)row.get("isReplied"));
					} catch (Exception ex) {
						 System.out.println("isReplied Hata :" + ex.toString());
					}					
				} catch (Exception ex) {
					 System.out.println(" Hata :" + ex.toString());
				} 
				
				return k;
			 }
			
			
			@Override
			public vJSONRESULT vtMesajGonder(String mesaj,Integer alici,Integer gonderen,Integer parent,Integer fkroot) {
				String sql1 = "call vtMesajGonder (?,?,?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						mesaj,alici,gonderen,parent,fkroot	
					});
					res.setSonuc("1");
					res.setMesaj("Mesaj Gönderildi");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("Mesaj Gönderilemedi");
				}
				return res;
				
			}

			@Override
			public List<vBILDIRIM> getBildirimListeNoHavuz(Integer skip, Integer take, Map<String,Object> userYetki) {
				String sql1 = "call spBildListNoHavuz (?,?)";
				  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { skip,take});
				return getList(rows, userYetki);
			}

			@Override
			public List<vTotal> countBildirimListeNoHavuz() {
				String sql1 = "call spBildListNoHavuzCount ()";
				List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
				return getCountList(rows);
			}
			

			
			
			@Override
			public vJSONRESULT changeTaskUser(Integer id,Integer fkGorevStatu,Integer userId) {
			
				String sql1 = "call spTaskagentChangeUser (?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("0");
				try {
					jdbcro.update(sql1,new Object[] { 
						id,fkGorevStatu,userId
					});
					res.setSonuc("1");
					res.setMesaj("İşlem Başarılı");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("Hata");
				}
				return res;
				
			}
			
			
			@Override
			public vJSONRESULT addTaskDetay(Integer sistem,Integer id,Integer userId,String aciklama,String ipAdres,Integer fk_detay_tip) {
				
				String sql1 = "call spAddTaskDetay (?,?,?,?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { sistem,id,userId,aciklama,ipAdres,fk_detay_tip });
					try {		
						res.setsonTaskDetayIslem((Integer)row.get("id"));
					} catch (Exception ex) {
						 System.out.println("id Hata :" + ex.toString());
					}
					try {		
						res.setsonTaskDetayIslemTipi((Integer)row.get("sistem"));
					} catch (Exception ex) {
						 System.out.println("sistem Hata :" + ex.toString());
					}					
					
					res.setSonuc("1");
					res.setMesaj("İşlem Başarılı");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("Hata");
				}
				return res;
				
			}
			
			@Override
			public vJSONRESULT addChangeUserLog(Integer svUserId,Integer userId,Integer login,Integer bildirimId,Integer fkIslem) {
				String sql1 = "call spAddChangeUserLog (?,?,?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
							svUserId,userId,login,bildirimId,fkIslem
					});
					res.setSonuc("1");
					res.setMesaj("İşlem Başarılı");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("Hata");
				}
				return res;
				
			}		
			
			
			
			
			@Override
			public vTASKDETAYISLEM openDetayIslem(Integer Id,Integer fkGorev,Integer Sistem) {
				
				String sql1 = "call spOpenDetayIslem (?,?,?)";
				vTASKDETAYISLEM res =new vTASKDETAYISLEM();
			
				try {
					Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { Id,fkGorev,Sistem });
					
					try {		
						res.setId((Integer)row.get("id"));
					} catch (Exception ex) {
						 System.out.println("id Hata :" + ex.toString());
					}
					try {		
						res.setfk_gorev((Integer)row.get("fk_gorev"));
					} catch (Exception ex) {
						 System.out.println("fk_gorev Hata :" + ex.toString());
					}					

					try {		
						res.setfk_agent((Integer)row.get("fk_agent"));
					} catch (Exception ex) {
						 System.out.println("fk_agent Hata :" + ex.toString());
					}	
					try {		
						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						new Date();
						res.setTarih((String)dateFormat.format(row.get("tarih")));
					
					} catch (Exception ex) {
						 System.out.println("tarih Hata :" + ex.toString());
					}	
					try {		
						res.setaciklama((String)row.get("aciklama"));
					} catch (Exception ex) {
						 System.out.println("aciklama Hata :" + ex.toString());
					}	
					try {		
						res.setfk_detay_tip((Integer)row.get("fk_detay_tip"));
					} catch (Exception ex) {
						 System.out.println("fk_detay_tip Hata :" + ex.toString());
					}	
					try {		
						res.setsistem((Integer)row.get("sistem"));
					
					} catch (Exception ex) {
						 System.out.println("sistem Hata :" + ex.toString());
					}	
					try {		
						res.setipadres((String)row.get("ipadres"));
					} catch (Exception ex) {
						 System.out.println("ipadres Hata :" + ex.toString());
					}						
				
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
				
				}
				return res;
			}
	
			
			
			@Override
			public vJSONRESULT addCrmIslemSureleri(Integer fkGorev,Integer fkAgent,Integer Sistem,Integer fkDetayIslem,Integer Kurum,Integer Kurum1,Integer Kurum2,Integer fkSube,Integer fkHavuz,Integer fkIl,String Tarih) {
				
				//System.out.println(fkGorev+","+fkAgent+","+Sistem+","+fkDetayIslem+","+Kurum+","+Kurum1+","+Kurum2+","+fkSube+","+fkHavuz+","+fkIl+","+Tarih);
				
				String sql1 = "call spAddCrmIslemSureleri (?,?,?,?,?,?,?,?,?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				try {
					jdbcro.update(sql1,new Object[] { 
							fkGorev,fkAgent,Sistem,fkDetayIslem,Kurum,Kurum1,Kurum2,fkSube,fkHavuz,fkIl,Tarih
						});
					res.setSonuc("1");
					res.setMesaj("İşlem Başarılı");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("Hata");
				}
				return res;
				
			}
			
			
			/**
			 * 
			 * @param Id
			 * @param UserId
			 * @param yetki
			 * @param isOnay 0 ise bilgiler acık 1 ise bilgiler gizli
			 * @return
			 */
			@Override
			public vJSONRESULT vatandasBilgisiKapataC(Integer Id, Integer isOnay) {
				String sql1 = "call vatandasBilgisiAcKapat (?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						Id,isOnay	
					});
					res.setSonuc("1");
					res.setMesaj("Vatandaş Bilgisi Kapatıldı.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
				
			}

			@Override
			public vJSONRESULT kurumAltKurumGuncelle(Integer id,Integer t1,Integer t2,Integer t3,Integer fk_aramakanali, Integer fk_aramasebebi) {
				String sql1 = "call kurumAltKurumGuncelle (?,?,?,?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						id,t1,t2,t3,fk_aramakanali,fk_aramasebebi
					});
					res.setSonuc("1");
					res.setMesaj("Kurum / Alt Kurum Bilgileri Güncellendi.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
				
			}		

			@Override
			public vJSONRESULT havuzaYonlendir(Integer id, Integer fk_havuz,Integer fkAgent,String aciklama) {
				String sql1 = "call spHavuzaYonlendir (?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						id,fk_havuz
					});
					res.setSonuc("1");
					res.setMesaj("Bildirim Havuza Yönlendirildi.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
				
			}	
			
			/**
			 * 
			 * @param id
			 * @param fk_havuz
			 * @param fk_user
			 * @param tip_level4 bu tiplevel4 eski sistemden kalan field. eski sistemde bildirimi havuzun dısındaki kullanıcıya atama vardı. eski kullanıcının havuz bilgisini içeriyor. yeni sistemde yok.
			 * @return
			 */
			@Override
			public vJSONRESULT kullaniciyaYonlendir(Integer id,Integer fk_havuz,Integer fk_user, Integer tip_level4 ) {
			
				String sql1 = "call sptaskKullaniciyaYonlendir (?,?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("0");
				try {
					jdbcro.update(sql1,new Object[] { 
						id,fk_havuz,fk_user,tip_level4
					});
					res.setSonuc("1");
					res.setMesaj("İşlem Başarılı");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("Hata");
				}
				return res;
				
			}

			@Override
			public vJSONRESULT taskDBUP(Integer id) {
				
				String sql1 = "call spTaskDBUP (?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						id
					});
					res.setSonuc("1");
					res.setMesaj("Bildirim taskDBUP Güncellendi.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
				
			}	




			/**
			 *  kullanıcıya ait havuzlar, il e ait havuzlar, subeye ait havuzlar, konu lara ait havuzlar datalarını cekmek için kullanılabilir.
			 * sadece bir kullanıcıya atanmış olması gereklidir kullanıcılara atanmamış havuzlar burada yer almaz
			 * @param userId int 0 girilirse yoksayar
			 * @param whereIN int 0 girilirse where in kullanımı aktif olur ids parametresinide  1,2,3 string olarak girilmesi gerekli
			 * @param IDS String  1,2,3 string olarak girilmesi gerekli. bu kısmın calısması için wherein 0 olarak gonderilmeli
			 * @param fkHavuz int 0 girilirse yoksayar
			 * @param fkSube int 0 girilirse yoksayar
			 * @param fkIl int 0 girilirse yoksayar
			 * @param fkKonu int 0 girilirse yoksayar
			 * @return
			 * geriye liste döner
			 * parametreler 0 girilirse dikkate alınmaz. parametreler sadece filtreleme içindir
			 * IDS parametresi text formatında olmak zorunda 1,2,3 seklinde id no lar gonderilmeli
			 */
			@Override
			public vTASKHAVUZKULLANICI getTaskHavuzKullanici(Integer userId,Integer whereIN,String IDS,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu) {
				
				String sql1 = "call spGetTaskHavuzKullanici (?,?,?,?,?,?,?)";
				vTASKHAVUZKULLANICI res =new vTASKHAVUZKULLANICI();
				
				try {
					Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { userId,whereIN,IDS,fkHavuz,fkSube,fkIl,fkKonu});
						try {		
							res.setId((Integer)row.get("id"));
						} catch (Exception ex) {
							System.out.println("id Hata :" + ex.toString());
						}				
						try {		
							res.setfk_user((Integer)row.get("fk_user"));
						} catch (Exception ex) {
							System.out.println("fk_user Hata :" + ex.toString());
						}				
						try {		
							res.setfk_havuz((Integer)row.get("fk_havuz"));
						} catch (Exception ex) {
							System.out.println("fk_havuz Hata :" + ex.toString());
						}				
						try {		
							res.setfk_sube((Integer)row.get("fk_sube"));
						} catch (Exception ex) {
							System.out.println("fk_sube Hata :" + ex.toString());
						}				
						try {		
							res.setfk_il((Integer)row.get("fk_il"));
						} catch (Exception ex) {
							System.out.println("fk_il Hata :" + ex.toString());
						}				
						try {		
							res.setfk_konu((Integer)row.get("fk_konu"));
						} catch (Exception ex) {
							System.out.println("fk_konu Hata :" + ex.toString());
						}				
						try {		
							res.setisActive((Integer)row.get("isActive"));
						} catch (Exception ex) {
							System.out.println("isActive Hata :" + ex.toString());
						}				
				} catch(Exception ex) {
					res.setId(0);
					System.out.println(" Hata :" + ex.toString());
				}
				return res;
			}


			@Override
			public vJSONRESULT taskHavuzDetayEkle(Integer fkAgent,Integer fkGorev,Integer detayIslemId,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu) {
				
				String sql1 = "call spTaskHavuzDetayEkle (?,?,?,?,?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						fkAgent,fkGorev,detayIslemId,fkHavuz,fkSube,fkIl,fkKonu
					});
					res.setSonuc("1");
					res.setMesaj("Bildirim task_DBDetayHavuz Eklendi.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
				
			}

			/***
			 * 
			 * @param skip
			 * @param take
			 * @param userId
			 * @param userYetki
			 * @return
			 * sadece taskDB_Detay tablosundan gerektigi zaman data cekmek için kullanıyoruz
			 */
			@Override
			public Map<String, Object> getTaskDBDetay(Integer sistem, Integer fkGorev, Integer fkAgent,Integer limit,Integer id) {
				String sql1 = "call spGetTaskDBDetay (?,?,?,?,?)";
					Map<String, Object> map = null;
				  	map = jdbcro.queryForMap(sql1,new Object[] { sistem, fkGorev, fkAgent,limit,id });
				return map;
			}

			@Override
			public vJSONRESULT bildirimKapat(Integer id, Integer fk_agent, Integer fk_taskb_kapanma_statu,String kurumYetki,String aciklama,Map<String,Object> userYetki) {
				String sql1 = "call spBildirimKapat (?,?,?,?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				vBILDIRIM task=this.getTask(id, userYetki);
				Integer fk_statu=task.getfkStatu();
				
				try {
					jdbcro.update(sql1,new Object[] { 
						id,fk_statu,fk_agent, fk_taskb_kapanma_statu,kurumYetki,aciklama
					});
					
					res.setSonuc("1");
					res.setMesaj("Bildirim Kapatma İşlemi Başarılı.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
			}


			@Override
			public vJSONRESULT taskChangeStatu(Integer id,Integer statu) {
				
				String sql1 = "call spTaskChangeStatu (?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						id,statu
					});
					res.setSonuc("1");
					res.setMesaj("Bildirim Durumu Guncellendi.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
				
			}


			@Override
			public vJSONRESULT yenidenAc(Integer id, Integer userLokasyon,Map<String,Object> userYetki) {
				String sql1 = "call spBildirimYenidenAc (?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						id, userLokasyon
					});
					
					res.setSonuc("1");
					res.setMesaj("Bildirim Kapatma İşlemi Başarılı.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
			}

			@Override
			public vJSONRESULT addSmsListe(Integer fk_gorev, String gsm, String mesaj) {
				String sql1 = "call spAddSmsListe (?,?,?)";
				vJSONRESULT res =new vJSONRESULT();
				res.setSonuc("1");
				try {
					jdbcro.update(sql1,new Object[] { 
						fk_gorev, gsm, mesaj
					});
					
					res.setSonuc("1");
					res.setMesaj("Numara SMS Listesine Eklendi.");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("İşlem sırasında bir sorun oluştu.");
				}
				return res;
			}

}

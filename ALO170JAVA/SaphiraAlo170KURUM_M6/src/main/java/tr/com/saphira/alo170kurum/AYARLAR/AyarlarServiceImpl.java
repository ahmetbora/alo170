package tr.com.saphira.alo170kurum.AYARLAR;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.BILDIRIM.vJSONRESULT;
import tr.com.saphira.alo170kurum.model.vTotal;


@Service
public class AyarlarServiceImpl implements AyarlarService{

	@Autowired
	AyarlarDao Dao;


	@Override
	public List<vKULLANICILAR> KullaniciFiltreListesi(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon,Integer skip,Integer take) {
		
		return Dao.KullaniciFiltreListesi(ADI,EMAIL,ROL,isActive,fk_lokasyon,skip,take);
	}
	@Override
	public List<vTotal> countKullaniciFiltreListesi(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon) {
		
		return Dao.countKullaniciFiltreListesi(ADI,EMAIL,ROL,isActive,fk_lokasyon);
	}

	
	@Override
	public List<vKURUMLAR> getKurumlar(Integer ID,String TERM,Integer skip,Integer take,Map<String,Object> userYetki) {
		
		return Dao.getKurumlar(ID,TERM,skip,take,userYetki);
	}
	@Override
	public List<vTotal> countGetKurumlar(Integer ID,String TERM) {
		return Dao.countGetKurumlar(ID,TERM);
	}

	@Override
	public List<vALTKURUMLAR> getAltKurumlar(Integer ID,String TERM,Integer fk_parent,Integer skip,Integer take,Map<String,Object> userYetki) {
		
		return Dao.getAltKurumlar(ID,TERM,fk_parent,skip,take,userYetki);
	}
	@Override
	public List<vTotal> countGetAltKurumlar(Integer ID,String TERM,Integer fk_parent) {
		
		return Dao.countGetAltKurumlar(ID,TERM,fk_parent);
	}


	@Override
	public	List<Map<String, Object>> getSubeler(Integer ID,String TERM,Integer fk_konu,Integer fk_il,Integer skip,Integer take) {
		return Dao.getSubeler(ID,TERM,fk_konu,fk_il,skip,take);
	}
	public List<vTotal> countGetSubeler(Integer ID,String TERM,Integer fk_konu) {
		return Dao.countGetSubeler(ID,TERM,fk_konu);
	}

	@Override
	public List<vARAMAKONUSU> getAramaKonusu(Integer ID,String TERM,Integer fk_konu,Integer skip,Integer take) {
		
		return Dao.getAramaKonusu(ID,TERM,fk_konu,skip,take);
	}
	@Override
	public List<vTotal> countGetAramaKonusu(Integer ID,String TERM,Integer fk_konu) {
		
		return Dao.countGetAramaKonusu(ID,TERM,fk_konu);
	}

	@Override
	public void setKurumlar(Integer ID, String ADI,String EMAIL, Integer ISACTIVE) {
		
		this.Dao.setKurumlar(ID,ADI,EMAIL,ISACTIVE);
	}


	@Override
	public void setAltKurumlar(Integer ID, String ADI, Integer PARENT,String EMAIL, Integer ISACTIVE) {
		
		this.Dao.setAltKurumlar(ID,ADI,PARENT,EMAIL,ISACTIVE);
	}

	@Override
	public void setAramaKonusu(Integer ID, String ADI, Integer KONU,Integer ALTKONU,String EMAIL, Integer ISACTIVE) {
		
		this.Dao.setAramaKonusu(ID, ADI, KONU, ALTKONU,EMAIL, ISACTIVE);
	}
	@Override
	public void setSubeler(Integer ID, String ADI, Integer KONU, Integer IL, Integer ISACTIVE) {
		
		this.Dao.setSubeler(ID, ADI, KONU, IL, ISACTIVE);
	}
	@Override
	public void setHavuzlar(Integer ID, String ADI, Integer KONU, Integer IL, Integer SUBE,String EMAIL, Integer ISACTIVE) {
		
		this.Dao.setHavuzlar(ID, ADI, KONU, IL, SUBE, EMAIL, ISACTIVE);
	}



	@Override
	public List<vALTKURUMLAR> getAltKurumlarFiltre(Integer KONU) {
		
		return Dao.getAltKurumlarFiltre(KONU);
	}



	@Override
	public List<vARAMAKONUSU> getAramaKonusuFiltre(Integer KONU) {
		
		return Dao.getAramaKonusuFiltre(KONU);
	}



	/**
	 * getSubelerFiltre
	 * @param ID 		sube id si 0 girilirse kullanılmaz. bir id girilirse o id ye ait subeyi tek kayıt olarka getirir.
	 * @param KONU		konu id si 0 olursa kullanılmaz
	 * @param TERM		sube adında aranacak boş bırakılırsa kullanılmaz
	 * @param FKIL		il id si 0 olursa kullanılmaz
	 * @param IDS		where in yada not için kullanılacak sube id leri. boş bırakılırsa kullanılmaz
	 * @param WHEREIN	where in mi yoksa not in mi olacağını belirler. 1 in 2 not in 0 olursa wherein kullanılmaz
	 * @return
	 */
	@Override
	public List<vSUBELER> getSubelerFiltre(Integer ID,Integer KONU,String TERM,Integer FKIL,String IDS,Integer WHEREIN) {
		
		return Dao.getSubelerFiltre(ID, KONU, TERM, FKIL, IDS, WHEREIN);
	}


	/**
	 * 
	 * @param ID
	 * @param ISACTIVE 1,0 YADA BAŞKA INT DEĞER. 1 VE 0 HARICI DEĞER GİRİLİRSE FİLTREYİ YOK SAYAR 
	 * @param FKSUBE 0 İSE UYGULANMAZ
	 * @param FKIL 0 İSE UYGULANMAZ
	 * @param FKKONU 0 İSE UYGULANMAZ
	 * @param IDS STRING 1,2,3,9,8 SEKLİNDE ID LERİ SORGUYA SOKAR
	 * @param WHEREIN IDS PARAMETRESINDE GIRILEN ID LERİ IN YADA NOT IN OLACAĞINI BELİRLER. 1 IN 2 NOT IN 0 YOKSAY
	 * @param TERM ADI KISMINDA LİKE İLE ARAMA YAPAR. BOŞ BIRAKILIRSA UYGULANMAZ
	 * @return
	 */
	@Override
	public List<Map<String, Object>> getHavuzlar(String IDS,Integer ISACTIVE,Integer FKSUBE,Integer FKIL,Integer FKKONU,Integer WHEREIN,String TERM,Integer skip,Integer take) {
		
		return Dao.getHavuzlar(IDS,ISACTIVE, FKSUBE, FKIL, FKKONU, WHEREIN, TERM,skip,take);
	}
	@Override
	public List<vTotal> countGetHavuzlar(String IDS,Integer ISACTIVE,Integer FKSUBE,Integer FKIL,Integer FKKONU,Integer WHEREIN,String TERM) {
		
		return Dao.countGetHavuzlar(IDS, ISACTIVE, FKSUBE, FKIL, FKKONU, WHEREIN, TERM);
	}

	@Override
	public List<Map<String, Object>> getAramaKanali(String IDS,String TERM,Integer ISACTIVE,Integer fk_parent,Integer WHEREIN,Integer skip,Integer take) {
		return Dao.getAramaKanali(IDS,TERM,ISACTIVE,fk_parent,WHEREIN,skip,take);
	}
	@Override
	public List<vTotal> countGetAramaKanali(String IDS,String TERM,Integer ISACTIVE,Integer fk_parent,Integer WHEREIN) {
		return Dao.countGetAramaKanali(IDS,TERM,ISACTIVE,fk_parent,WHEREIN);
	}
	public List<Map<String, Object>> getEgitimDurumlari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take) {
		return Dao.getEgitimDurumlari(IDS,TERM,ISACTIVE,WHEREIN,skip,take);
	}
	public List<vTotal> countGetEgitimDurumlari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN) {
		return Dao.countGetEgitimDurumlari(IDS,TERM,ISACTIVE,WHEREIN);
	}
	public List<Map<String, Object>> getGuvenlikSorulari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take) {
		return Dao.getGuvenlikSorulari(IDS,TERM,ISACTIVE,WHEREIN,skip,take);
	}
	public List<vTotal> countGetGuvenlikSorulari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN) {
		return Dao.countGetGuvenlikSorulari(IDS,TERM,ISACTIVE,WHEREIN);
	}
	public List<Map<String, Object>> getIller(String IDS,String TERM,Integer ISACTIVE,Integer fk_ulke,Integer WHEREIN,Integer skip,Integer take) {
		return Dao.getIller(IDS,TERM,ISACTIVE,fk_ulke,WHEREIN,skip,take);
	}
	public List<vTotal> countGetIller(String IDS,String TERM,Integer ISACTIVE,Integer fk_ulke,Integer WHEREIN) {
		return Dao.countGetIller(IDS,TERM,ISACTIVE,fk_ulke,WHEREIN);
	}
	public List<Map<String, Object>> getIlceler(String IDS,String TERM,Integer ISACTIVE,Integer fk_il,Integer WHEREIN,Integer skip,Integer take) {
		return Dao.getIlceler(IDS,TERM,ISACTIVE,fk_il,WHEREIN,skip,take);
	}
	public List<vTotal> countGetIlceler(String IDS,String TERM,Integer ISACTIVE,Integer fk_il,Integer WHEREIN) {
		return Dao.countGetIlceler(IDS,TERM,ISACTIVE,fk_il,WHEREIN);
	}
	public List<Map<String, Object>> getUlkeler(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take) {
		return Dao.getUlkeler(IDS,TERM,ISACTIVE,WHEREIN,skip,take);
	}
	public List<vTotal> countGetUlkeler(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN) {
		return Dao.countGetUlkeler(IDS,TERM,ISACTIVE,WHEREIN);
	}
	public List<Map<String, Object>> getDahili(Integer id,String EXT,Integer WHEREIN,Integer fk_lokasyon,Integer skip,Integer take) {
		return Dao.getDahili(id,EXT,WHEREIN,fk_lokasyon,skip,take);
	}
	public List<vTotal> countGetDahili(String EXT,Integer WHEREIN,Integer fk_lokasyon) {
		return Dao.countGetDahili(EXT,WHEREIN,fk_lokasyon);
	}
	public List<Map<String, Object>> getKkoBaslik(Integer id , Integer isActive,Integer skip,Integer take) {
		return Dao.getKkoBaslik(id,isActive,skip,take);
	}
	public List<vTotal> countGetKkoBaslik(Integer id, Integer isActive) {
		return Dao.countGetKkoBaslik(id,isActive);
	}
	public List<Map<String, Object>> getKkoSoru(Integer id , Integer isActive,Integer fk_parent,Integer skip,Integer take) {
		return Dao.getKkoSoru(id,isActive,fk_parent,skip,take);
	}
	public List<vTotal> countGetKkoSoru(Integer id, Integer isActive,Integer fk_parent) {
		return Dao.countGetKkoSoru(id,isActive,fk_parent);
	}
	public List<Map<String, Object>> getPeriyodik(Integer id ,Integer skip,Integer take) {
		return Dao.getPeriyodik(id,skip,take);
	}
	public List<vTotal> countGetPeriyodik(Integer id) {
		return Dao.countGetPeriyodik(id);
	}
	public vJSONRESULT delPeriyodik(Integer id) {
		return Dao.delPeriyodik(id);
	}


	public List<Map<String, Object>> getLokasyonlar() {
		return Dao.getLokasyonlar();
	}
	public List<vTotal> countGetLokasyonlar() {
		return Dao.countGetLokasyonlar();
	}

	@Override
	public List<vYETKIGRUPLARI> getYetkiGruplari(Integer ID,Integer isAdmin,Integer tip,Integer level,Integer userId) {
		
		return Dao.getYetkiGruplari(ID,isAdmin,tip,level,userId);
	}
	@Override
	public List<vYETKIGRUPLARI> getYetkiGruplariUser(Integer userId) {
		
		return Dao.getYetkiGruplariUser(userId);
	}
	
	@Override
	public List<vYETKIALANLARI> getYetkiAlanlari(Integer ID,Integer isRoot,Integer yetkiGrubuID,Integer whereInType,String IDS,Integer isActive) {
		
		return Dao.getYetkiAlanlari(ID,isRoot,yetkiGrubuID,whereInType,IDS,isActive);
	}
	
	@Override
	public vJSONRESULT yetkiGrubuDetayGuncelle(Integer fkYetki,Integer fkRol) {
		return this.Dao.yetkiGrubuDetayGuncelle(fkYetki,fkRol);
	}
	
	
	@Override
	public vJSONRESULT yetkiGrubuDetayTemizle(Integer fkRol) {
		return this.Dao.yetkiGrubuDetayTemizle(fkRol);
	}
	
	@Override
	public vJSONRESULT yetkiGrubuGuncelle(String adi,Integer isActive,Integer isHidden,Integer id) {
		return this.Dao.yetkiGrubuGuncelle(adi,isActive,isHidden,id);
	}

	@Override
	public vJSONRESULT kullaniciKaydet(String adi,String email,String password,String username,String gsm,String ext,String tckimlik,String emsicil,String ksicil,Integer email_bildirim,Integer IsActive,Integer fkIslemYapan,Integer fk_rol,Integer fk_lokasyon,String fkEkleyen,Integer fk_kurum) {
		return this.Dao.kullaniciKaydet(adi, email, password, username, gsm, ext, tckimlik, emsicil, ksicil, email_bildirim, IsActive, fkIslemYapan,fk_rol,fk_lokasyon,fkEkleyen,fk_kurum);
	}
	
	@Override
	public vJSONRESULT kullaniciGuncelle(Integer id,String adi,String email,String password,String username,String gsm,String ext,String tckimlik,String emsicil,String ksicil,Integer email_bildirim,Integer IsActive,Integer fkIslemYapan,String face,String twit,Integer fk_rol, Integer fk_lokasyon,Integer isUpdated,Integer fk_kurum) {
		return this.Dao.kullaniciGuncelle(id,adi, email, password,username, gsm, ext, tckimlik, emsicil, ksicil, email_bildirim, IsActive,fkIslemYapan,face,twit,fk_rol, fk_lokasyon,isUpdated,fk_kurum);
	}

	@Override
	public vKULLANICILAR getUserByID(Integer ID) {
		return this.Dao.getUserByID(ID);
	}
	
	/**
	 * SADECE 1 KAYIT DÖNDÜRÜR PARAMETRELERDEN SADECE 1 TANESİ KULLANILIR.
	 * @param ID
	 * @param userName
	 * @param email
	 * @param tcKimlik
	 * @return
	 */
	@Override
	public vKULLANICILAR findUser(Integer ID,String userName,String email,String tcKimlik) {
		return this.Dao.findUser(ID, userName, email, tcKimlik);
	}

	@Override
	public List<vHAVUZLAR> getUserHavuz(Integer userId,Integer whereIN,String IDS,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu) {
		
		return Dao.getUserHavuz( userId, whereIN, IDS, fkHavuz, fkSube, fkIl, fkKonu);
	}
	
	@Override
	public vJSONRESULT userHavuzGuncelle(Integer userID,Integer fkIslemYapan,String havuzlar) {
		return this.Dao.userHavuzGuncelle(userID, fkIslemYapan,havuzlar);
	}


	@Override
	public List<vYETKIALANLARI> getUserYetkiAlanlariDetay(Integer userId) {
		
		return Dao.getUserYetkiAlanlariDetay(userId);
	}

	@Override
	public vJSONRESULT yetkiGrubuDetayGuncelleUser(Integer userID,Integer fkIslemYapan,String yetkiler,Integer fkRol,Integer fkLokasyon,String yetkiRol) {
		return this.Dao.yetkiGrubuDetayGuncelleUser( userID, fkIslemYapan, yetkiler, fkRol, fkLokasyon,yetkiRol);
	}
	@Override
	public  Map<String,Object> getUserYetkiFromJSON(HttpSession session){
		return this.Dao.getUserYetkiFromJSON(session);
	}

	@Override
	public  ModelAndView getUserYetkiBildirimModelView(ModelAndView model, HttpSession session) {
		return this.Dao.getUserYetkiBildirimModelView(model,session);
	}
	@Override
	public List<Map<String, Object>> getAktifPasifList(Integer tip) {
		return Dao.getAktifPasifList(tip);
	}
	@Override
	public vJSONRESULT tnmUpdate(Integer fldCount,String tbl,List<String> cols,List<String> vals,Integer Id,String ayrac) {
		return Dao.tnmUpdate(fldCount,tbl,cols,vals,Id,ayrac);
	}
	@Override
	public vJSONRESULT tnmInsert(Integer fldCount,String tbl,List<String> cols,List<String> vals,String ayrac) {
		return Dao.tnmInsert(fldCount,tbl,cols,vals,ayrac);
	}

	@Override
	public List<Map<String,Object>> getRolLowerLevel(Integer RolId) {
		return Dao.getRolLowerLevel(RolId);
	}

}

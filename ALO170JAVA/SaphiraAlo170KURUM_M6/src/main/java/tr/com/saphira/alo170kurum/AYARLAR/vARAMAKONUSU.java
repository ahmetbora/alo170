package tr.com.saphira.alo170kurum.AYARLAR;

public class vARAMAKONUSU {

	private Integer id;
	private String adi;
	private String kurumadi;
	private Integer isActive;
	private String konuadi;
	private Integer fk_parent;
	private Integer fk_konu;
	private String email;
	private String durum;
	public vARAMAKONUSU() {
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getKurumadi() {
		return kurumadi;
	}
	public void setKurumadi(String kurumadi) {
		this.kurumadi = kurumadi;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public String getKonuadi() {
		return konuadi;
	}
	public void setKonuadi(String konuadi) {
		this.konuadi = konuadi;
	}
	public Integer getFk_parent() {
		return fk_parent;
	}
	public void setFk_parent(Integer fk_parent) {
		this.fk_parent = fk_parent;
	}
	public Integer getFk_konu() {
		return fk_konu;
	}
	public void setFk_konu(Integer fk_konu) {
		this.fk_konu = fk_konu;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDurum() {
		return durum;
	}
	public void setDurum(String durum) {
		this.durum = durum;
	}
	
	
}

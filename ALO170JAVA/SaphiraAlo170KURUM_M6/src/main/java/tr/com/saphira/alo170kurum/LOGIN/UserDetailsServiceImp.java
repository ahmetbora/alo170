package tr.com.saphira.alo170kurum.LOGIN;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import tr.com.saphira.alo170kurum.Genel;


public class UserDetailsServiceImp implements UserDetailsService {

	@Autowired
	UserService userService;
	
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		System.out.println("******---**loadUserByUsername*");
		
		
		username = Genel.encrypt(username);
		
		User user = findUserbyUername(username);

			mUserDetails d = new mUserDetails();
			d.setUserid(1);


		
	    UserBuilder builder = null;
	    if (user != null) {
	    	String r[] = {"ADMIN"};
	      builder = org.springframework.security.core.userdetails.User.withUsername(username);
	      builder.password(new BCryptPasswordEncoder().encode( Genel.decrypt(user.getPassword())));
	      builder.roles(r);
	      
	      
	      
	      
	    } else {
	      throw new UsernameNotFoundException("User not found.");
	    }

	    return builder.build();
	}
	
	
	private User findUserbyUername(String username) {
			
		
	    return userService.findUserByUserName(username);
	  }

}
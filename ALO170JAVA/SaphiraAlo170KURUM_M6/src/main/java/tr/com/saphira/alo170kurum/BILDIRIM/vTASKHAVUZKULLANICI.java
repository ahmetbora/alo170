package tr.com.saphira.alo170kurum.BILDIRIM;

public class vTASKHAVUZKULLANICI {
	private Integer id;
	private Integer fk_user;
	private Integer fk_havuz;
	private Integer fk_sube;
	private Integer fk_il;
	private Integer fk_konu;
	private Integer isActive;


	
	
	
	public vTASKHAVUZKULLANICI() {
		super();
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}	
	
	
	public Integer getfk_user() {
		return fk_user;
	}
	public void setfk_user(Integer fk_user) {
		this.fk_user = fk_user;
	}	
	
	public Integer getfk_havuz() {
		return fk_havuz;
	}
	public void setfk_havuz(Integer fk_havuz) {
		this.fk_havuz = fk_havuz;
	}	
	
	public Integer getfk_sube() {
		return fk_sube;
	}
	public void setfk_sube(Integer fk_sube) {
		this.fk_sube = fk_sube;
	}	
	
	public Integer getfk_il() {
		return fk_il;
	}
	public void setfk_il(Integer fk_il) {
		this.fk_il = fk_il;
	}	
	
	public Integer getfk_konu() {
		return fk_konu;
	}
	public void setfk_konu(Integer fk_konu) {
		this.fk_konu = fk_konu;
	}	
	
	public Integer getisActive() {
		return isActive;
	}
	public void setisActive(Integer isActive) {
		this.isActive = isActive;
	}	
	

}

package tr.com.saphira.alo170kurum.RAPOR;

public class rptIvrAgent {
    private Integer id;
    private String tarih;
    private String lokasyon;
    private String dahili;
    private String adi;
    private String arayan_no;
    private String kriter_1;
    private String kriter_2;
    private String kriter_3;
    private String kriter_4;
    private String kriter_5;


    public rptIvrAgent() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String getlokasyon(){
        return lokasyon;
    }
    public void setlokasyon(String lokasyon){
        this.lokasyon=lokasyon;
    }

    public String getdahili(){
        return dahili;
    }
    public void setdahili(String dahili){
        this.dahili=dahili;
    }

    public String getadi(){
        return adi;
    }
    public void setadi(String adi){
        this.adi=adi;
    }

    public String getarayan_no(){
        return arayan_no;
    }
    public void setarayan_no(String arayan_no){
        this.arayan_no=arayan_no;
    }

    public String getkriter_1(){
        return kriter_1;
    }
    public void setkriter_1(String kriter_1){
        this.kriter_1=kriter_1;
    }

    public String getkriter_2(){
        return kriter_2;
    }
    public void setkriter_2(String kriter_2){
        this.kriter_2=kriter_2;
    }

    public String getkriter_3(){
        return kriter_3;
    }
    public void setkriter_3(String kriter_3){
        this.kriter_3=kriter_3;
    }

    public String getkriter_4(){
        return kriter_4;
    }
    public void setkriter_4(String kriter_4){
        this.kriter_4=kriter_4;
    }

    public String getkriter_5(){
        return kriter_5;
    }
    public void setkriter_5(String kriter_5){
        this.kriter_5=kriter_5;
    }

}


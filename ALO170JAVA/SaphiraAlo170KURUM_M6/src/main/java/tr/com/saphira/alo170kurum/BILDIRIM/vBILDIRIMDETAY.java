package tr.com.saphira.alo170kurum.BILDIRIM;

public class vBILDIRIMDETAY {
	
	private int id;
	private String aciklama;
	private String agent;
	private String ext;
	private String lokasyon;
	private String rol_adi;
	private String tarih;
	private String saat;
	private String cls;
	private String fark;
	private String islemtipi;
	private Integer isEnc;
	private String cryp_aciklama;
	
	public vBILDIRIMDETAY() {
		super();
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAciklama() {
		return aciklama;
	}
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public String getLokasyon() {
		return lokasyon;
	}
	public void setLokasyon(String lokasyon) {
		this.lokasyon = lokasyon;
	}
	public String getRol_adi() {
		return rol_adi;
	}
	public void setRol_adi(String rol_adi) {
		this.rol_adi = rol_adi;
	}
	public String getTarih() {
		return tarih;
	}
	public void setTarih(String tarih) {
		this.tarih = tarih;
	}
	public String getSaat() {
		return saat;
	}
	public void setSaat(String saat) {
		this.saat = saat;
	}
	public String getCls() {
		return cls;
	}
	public void setCls(String cls) {
		this.cls = cls;
	}
	public String getFark() {
		return fark;
	}
	public void setFark(String fark) {
		this.fark = fark;
	}
	public String getIslemtipi() {
		return islemtipi;
	}
	public void setIslemtipi(String islemtipi) {
		this.islemtipi = islemtipi;
	}
	public Integer getIsEnc() {
		return isEnc;
	}
	public void setIsEnc(Integer isEnc) {
		this.isEnc = isEnc;
	}
	public String getCryp_aciklama() {
		return cryp_aciklama;
	}
	public void setCryp_aciklama(String cryp_aciklama) {
		this.cryp_aciklama = cryp_aciklama;
	}

	
	
	
	
	
}

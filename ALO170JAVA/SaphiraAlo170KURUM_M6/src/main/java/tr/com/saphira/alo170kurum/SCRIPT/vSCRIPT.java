package tr.com.saphira.alo170kurum.SCRIPT;

public class vSCRIPT {
	Integer id;
	String script;
	String aciklama;
	Integer isActive;
	Integer fk_parent;
	Integer fk_aramasebebi;
	Integer tip_level1;
	Integer tip_level2;
	Integer tip_level3;
	String skadi;
	String knadi;
	String konu;
	String alt_konu;
	String alt_konu_detay;
	
	public vSCRIPT() {
		super();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	
	public String getAciklama() {
		return aciklama;
	}
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	
	public Integer getisActive() {
		return isActive;
	}
	public void setisActive(Integer isActive) {
		this.id = isActive;
	}
	public Integer getFkParent() {
		return fk_parent;
	}
	public void setFkParent(Integer fk_parent) {
		this.fk_parent = fk_parent;
	}
	public Integer getFkAramaSebebi() {
		return fk_aramasebebi;
	}
	public void setFkAramaSebebi(Integer fk_aramasebebi) {
		this.fk_aramasebebi = fk_aramasebebi;
	}
	public Integer getTipLevel1() {
		return tip_level1;
	}
	public void setTipLevel1(Integer tip_level1) {
		this.tip_level1 = tip_level1;
	}
	public Integer getTipLevel2() {
		return tip_level2;
	}
	public void setTipLevel2(Integer tip_level2) {
		this.tip_level2 = tip_level2;
	}
	public Integer getTipLevel3() {
		return tip_level1;
	}
	public void setTipLevel3(Integer tip_level1) {
		this.tip_level1 = tip_level1;
	}
	
	
	
	public String getSkAdi() {
		return skadi;
	}
	public void setSkAdi(String skadi) {
		this.skadi = skadi;
	}
	public String getKnAdi() {
		return knadi;
	}
	public void setKnAdi(String knadi) {
		this.knadi = knadi;
	}
	public String getKonu() {
		return konu;
	}
	public void setKonu(String konu) {
		this.konu = konu;
	}
	public String getAltKonu() {
		return alt_konu;
	}
	public void setAltKonu(String alt_konu) {
		this.alt_konu = alt_konu;
	}
	public String getAltKonuDetay() {
		return alt_konu_detay;
	}
	public void setAltKonuDetay(String alt_konu_detay) {
		this.alt_konu_detay = alt_konu_detay;
	}
	
}

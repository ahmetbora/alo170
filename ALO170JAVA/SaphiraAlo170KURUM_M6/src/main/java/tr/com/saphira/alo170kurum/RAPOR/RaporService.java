package tr.com.saphira.alo170kurum.RAPOR;
import java.util.List;

import tr.com.saphira.alo170kurum.model.vTotal;

public interface RaporService {
    
    /**
     * 
     * @param sdate
     * @param fdate
     * @param tip_level1
     * @param fk_il
     * @param fk_sube
     * @param fk_havuz
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Kurum / Şube / Havuz İstatistik Raporu
     * @return
     */
    public List<rptKurumSubeIstatistik> KurumSubeHavuzIstatistikRaporu(
      String sdate, 
      String fdate, 
      Integer tip_level1, 
      Integer fk_il, 
      Integer fk_sube, 
      Integer fk_havuz,
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public List<vTotal> countKurumSubeHavuzIstatistikRaporu(
      String sdate, 
      String fdate, 
      Integer tip_level1, 
      Integer fk_il, 
      Integer fk_sube, 
      Integer fk_havuz,
      Integer userId,
      String yetki
    );

    
    /***
     * 
     * @param sdate
     * @param fdate
     * @param tip_level1
     * @param fk_il
     * @param fk_sube
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Kurum Kullanıcı İstatistik Raporu
     * @return
     */
    public List<rptKurumKullaniciIstatistik> KurumKullaniciIstatistikRaporu(
      String sdate, 
      String fdate, 
      Integer tip_level1, 
      Integer fk_il, 
      Integer fk_sube, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public List<vTotal> countKurumKullaniciIstatistikRaporu(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        Integer userId,
        String yetki
    ); 


    /**
     * 
     * @param sdate
     * @param fdate
     * @param tip_level1
     * @param tip_level2
     * @param tip_level3
     * @param fk_il
     * @param fk_ilce
     * @param fk_havuz
     * @param fk_gorev_statu
     * @param arama_kanali_tipi
     * @param fk_aramakanali
     * @param fk_aramasebebi
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Bildirim Detay Raporu
     * @return
     */
    public List<rptBildirimDetay> BildirimDetayRaporu(
    String sdate, 
		String fdate, 
		Integer tip_level1, 
    Integer tip_level2,
    Integer tip_level3,
		Integer fk_il, 
		Integer fk_ilce, 
		Integer fk_havuz, 
		Integer fk_gorev_statu, 
		Integer arama_kanali_tipi, 
		Integer fk_aramakanali, 
		Integer fk_aramasebebi, 
		Integer skip,
		Integer take,
		Integer userId,
		String yetki
    );
    public List<vTotal> countBildirimDetayRaporu(
      String sdate, 
      String fdate, 
      Integer tip_level1, 
      Integer tip_level2,
      Integer tip_level3,
      Integer fk_il, 
      Integer fk_ilce, 
      Integer fk_havuz, 
      Integer fk_gorev_statu, 
      Integer arama_kanali_tipi, 
      Integer fk_aramakanali, 
      Integer fk_aramasebebi, 
      Integer userId,
      String yetki
  ); 

  /***
   * 
   * @param sdate
   * @param fdate
   * @param mu
   * @param sistem
   * @param skip
   * @param take
   * @param userId
   * @param yetki
   * @param CRM Mevzuat Uzmanı Aktivite Raporu
   * @return
   */
  public List<rptMuAktivite> MuAktiviteRapor(
    String sdate, 
    String fdate, 
    Integer mu, 
    Integer sistem, 
    Integer skip, 
    Integer take, 
    Integer userId, 
    String yetki
  );
  public List<vTotal> countMuAktiviteRapor(
    String sdate, 
    String fdate, 
    Integer mu, 
    Integer sistem, 
    Integer userId, 
    String yetki
  );


  /***
   * 
   * @param sdate
   * @param fdate
   * @param mu
   * @param tip_level1
   * @param sistem
   * @param skip
   * @param take
   * @param userId
   * @param yetki
   * @param CRM Mevzuat Uzmanı İşlem Süresi Raporu
   * @return
   */
  public List<rptMuSure> MuSureRapor(
        String sdate, 
        String fdate, 
        Integer mu, 
        Integer tip_level1, 
        Integer sistem, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
  );
  public List<vTotal> countMuSureRapor(
    String sdate, 
    String fdate, 
    Integer mu, 
    Integer tip_level1, 
    Integer sistem, 
    Integer userId, 
    String yetki
  );


  /**
   * 
   * @param sdate
   * @param fdate
   * @param mu
   * @param sistem
   * @param tip_level1
   * @param skip
   * @param take
   * @param userId
   * @param yetki
   * @param CRM Kurum Yetkilisi İşlem Süresi Raporu
   * @return
   */
    public List<rptKySure> KySureRapor(
        String sdate, 
        String fdate, 
        Integer ky, 
        Integer sistem, 
        Integer tip_level1, 
        Integer skip, 
        Integer take, 
        Integer userId, 
        String yetki
    );
    public  List<vTotal> countKySureRapor(
      String sdate, 
      String fdate, 
      Integer ky, 
      Integer sistem, 
      Integer tip_level1, 
      Integer userId, 
      String yetki
 );

    
 /***
  * 
  * @param sdate
  * @param fdate
  * @param tip_level1
  * @param fk_il
  * @param fk_sube
  * @param fk_havuz
  * @param skip
  * @param take
  * @param userId
  * @param yetki
  * @param CRM Havuz Bildirim Süre Raporu
  * @return
  */
    public List<rptHavuzBildirimSure> HavuzBildirimSureRapor(
      String sdate, 
      String fdate, 
      Integer tip_level1, 
      Integer fk_il, 
      Integer fk_sube, 
      Integer fk_havuz, 
      Integer skip, 
      Integer take, 
      Integer userId, 
      String yetki
    );
    public  List<vTotal> countHavuzBildirimSureRapor(
      String sdate, 
      String fdate, 
      Integer tip_level1, 
      Integer fk_il, 
      Integer fk_sube, 
      String fk_havuz, 
      Integer userId, 
      String yetki
    ); 


    /**
     * 
     * @param sdate
     * @param fdate
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Sektör Bazında
     * @return
     */
    public List<rptSektorBazinda> SektorBazindaRapor(
      String sdate, 
      String fdate, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public  List<vTotal> countSektorBazindaRapor(
      String sdate, 
      String fdate,  
      Integer userId, 
      String yetki
    ); 
    
    /**
     * 
     * @param sdate
     * @param fdate
     * @param fk_sektor
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Sektör Bazında İhbar / Şikayet
     * @return
     */    
    public List<rptSektorBazindaIhbarSikayet> SektorBazindaIhbarSikayet(
      String sdate, 
      String fdate, 
      Integer fk_sektor, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public  List<vTotal> countSektorBazindaIhbarSikayet(
      String sdate, 
      String fdate, 
      Integer fk_sektor, 
      Integer userId,
      String yetki
    );
    
    /**
     * 
     * @param sdate
     * @param fdate
     * @param fk_gorev_statu
     * @param userId
     * @param yetki
     * @param CRM İl Bazında
     * @return
     */    
    public List<rptIlBazinda> IlBazindaRapor(
      String sdate, 
      String fdate, 
      Integer fk_gorev_statu, 
      Integer userId,
      String yetki
    );

    
    /**
     * 
     * @param sdate
     * @param fdate
     * @param tip_level1
     * @param tip_level2
     * @param tip_level3
     * @param fk_gorev_statu
     * @param userId
     * @param yetki
     * @param CRM Konulara Göre
     * @return
     */    
    public List<rptKonularaGore> KonularaGore(
      String sdate, 
      String fdate, 
      Integer statu,
      Integer tip_level1, 
      Integer tip_level2, 
      Integer tip_level3, 
      Integer userId,
      String yetki
    );

    
    /**
     * 
     * @param skip
     * @param take
     * @param userId
     * @param CRM Havuz Açık Görevler Özet
     * @return
     */    
    public List<rptHavuzOzet> HavuzOzet(
      Integer skip,
      Integer take,
      Integer userId
    );
    public  List<vTotal> countHavuzOzet(
      Integer userId
     );


     /**
      * 
      * @param sdate
      * @param fdate
      * @param skip
      * @param take
      * @param userId
      * @param yetki
      * @param CRM WebChat Kurum Bilgilendirmeleri
      * @return
      */   
    public List<rptSgkBilgilendirme> SgkBilgilendirme(
      String sdate, 
      String fdate, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public  List<vTotal> countSgkBilgilendirme(
      String sdate, 
      String fdate, 
      Integer userId,
      String yetki
     );
     
     

    /**
     * 
     * @param sdate
     * @param fdate
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Agent Bazlı IVR Raporu
     * @return
     */    
    public List<rptIvrAgent> IvrAgent(
      String sdate, 
      String fdate, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public  List<vTotal> IvrAgentCount(
      String sdate, 
      String fdate, 
      Integer userId,
      String yetki
     );
        
    /**
     * 
     * @param sdate
     * @param fdate
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Taşeron Havuzu Raporu
     * @return
     */    
    public List<rptTaseronHavuzu> TaseronHavuzu(
      String sdate, 
      String fdate, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public  List<vTotal> TaseronHavuzuCount(
      String sdate, 
      String fdate, 
      Integer userId,
      String yetki
     );  
        
    /**
     * 
     * @param sdate
     * @param fdate
     * @param tc
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Ses Kayıt Raporu
     * @return
     */        
    public List<rptSesKayit> SesKayit(
      String sdate, 
      String fdate, 
      String tc, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public  List<vTotal> SesKayitCount(
      String sdate, 
      String fdate, 
      String tc, 
      Integer userId,
      String yetki
     );


        
    /**
     * 
     * @param sdate
     * @param fdate
     * @param userId
     * @param yetki
     * @param CRM Kurumlar Bazında Bildirim İstatistikleri
     * @return
     */    
    public List<rptKurumKapanmaIstatistik> KurumKapanmaIstatistik(
      String sdate, 
      String fdate, 
      Integer userId,
      String yetki
    );
        
    /**
     * 
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Birimlere Göre Kullanıcı Sayıları
     * @return
     */    
    public List<rptBirimlereGoreKullaniciSayisi> BirimlereGoreKullaniciSayisi( 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public  List<vTotal> BirimlereGoreKullaniciSayisiCount(
      Integer userId,
      String yetki
     );


         
    /**
     * @param sdate
     * @param fdate
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Kurumlar Bazında Zamanında kapanan Bildirimler
     * @return
     */     
    public List<rptKurumBazindaZamanindaKapanan> KurumBazindaZamanindaKapanan(
      String sdate, 
      String fdate, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    ); 
    public  List<vTotal> KurumBazindaZamanindaKapananCount(
      Integer userId,
      String yetki
     );
     
     


         
    
    /**
     * 
     * @param sdate
     * @param fdate
     * @param userId
     * @param yetki
     * @param CRM IVR Anket Raporu
     * @return
     */
    public List<rptIvrRapor> IvrRapor(
      String sdate, 
      String fdate, 
      Integer userId,
      String yetki
    ); 
    
    /**
     * 
     * @param skip
     * @param take
     * @param tipi
     * @param userId
     * @param yetki
     * @param CRM Sms / Email Raporu
     * @return
     */
    public List<rptSmsRapor> SmsRapor(
      Integer skip,
      Integer take,
      String tipi,
      Integer userId,
      String yetki
    ); 
    public  List<vTotal> SmsRaporCount(
      String tipi,
      Integer userId,
      String yetki
     );
      
     


    /**
     * 
     * @param sdate
     * @param fdate
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Periyodik Rapor
     * @return
     */    
    public List<rptPeriyodikRapor> PeriyodikRaporGelenCagri(
      String sdate, 
      String fdate, 
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
	 public  List<vTotal> PeriyodikRaporGelenCagriCount(
		  String sdate,
		  String fdate, 
		  Integer userId,
		  String yetki
	  );
    


    public List<rptPeriyodikRapor> PeriyodikRaporIlkOnKonu(
     String sdate, 
     String fdate, 
     Integer userId,
     String yetki
    );
   


    public List<rptPeriyodikRapor> PeriyodikRaporDisAramaSms(
     String sdate, 
     String fdate, 
     Integer userId,
     String yetki
    );

   
    
    /**
     * 
     * @param sdate
     * @param fdate
     * @param fk_user
     * @param skip
     * @param take
     * @param userId
     * @param yetki
     * @param CRM Rapor Logları
     * @return
     */
    public List<rptRaporLog> RaporLog(
      String sdate, 
      String fdate, 
      Integer fk_user,
      Integer skip,
      Integer take,
      Integer userId,
      String yetki
    );
    public List<vTotal> countRaporLog(
      String sdate,
      String fdate,
      Integer fk_user,
      Integer userId,
      String yetki
     );

  /*  public List<Map<String,Object>> KurumSubeHavuzIstatistikRaporu(
        String sdate, 
        String fdate, 
        Integer tip_level1, 
        Integer fk_il, 
        Integer fk_sube, 
        Integer fk_havuz,
        Integer skip,
        Integer take,
        Integer userId,
        vUSERYETKI yetki
        );*/
}

package tr.com.saphira.alo170kurum.RAPOR;

public class rptKurumKullaniciIstatistik {
    private Integer id;
    private String il;
    private String sube;
    private String kapanan;
    private String acik;
    private String user;


    public rptKurumKullaniciIstatistik() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getIl(){
        return il;
    }
    public void setIl(String il){
        this.il=il;
    }

    public String getSube(){
        return sube;
    }
    public void setSube(String sube){
        this.sube=sube;
    }

    public String getKapanan(){
        return kapanan;
    }
    public void setKapanan(String kapanan){
        this.kapanan=kapanan;
    }

    public String getAcik(){
        return acik;
    }
    public void setAcik(String acik){
        this.acik=acik;
    }

    public String getUser(){
        return user;
    }
    public void setUser(String user){
        this.user=user;
    }

}


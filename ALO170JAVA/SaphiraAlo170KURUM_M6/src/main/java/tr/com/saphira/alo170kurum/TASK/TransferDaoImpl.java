package tr.com.saphira.alo170kurum.TASK;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.BILDIRIM.vADET;

@Transactional

@Repository
public class TransferDaoImpl implements TransferDao {
	
	 @Autowired
	 @Qualifier("jdbceski")
	 private JdbcTemplate jdbceski;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;
	 
	 @Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 

	@Override
	public TRANSFER getTASKDB(Integer ID) {
		String sql1 = "call getTaskDB (?)";
		Map<String, Object>  row = jdbceski.queryForMap(sql1,new Object[] { ID });
		
		TRANSFER v = new TRANSFER();
		try {		
			v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setFk_gorev_statu(((Long)row.get("fk_gorev_statu")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_gorev_statu Hata :" + ex.toString());
		}try {		
			v.setFk_parent(((Long)row.get("fk_parent")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_parent Hata :" + ex.toString());
		}try {		
			v.setAciklama(row.get("aciklama").toString());
		} catch (Exception ex) {
			 System.out.println("aciklama Hata :" + ex.toString());
		}try {		
			v.setMU_aciklama(row.get("MU_aciklama").toString());
		} catch (Exception ex) {
			 System.out.println("MU_aciklama Hata :" + ex.toString());
		}try {		
			v.setKU_aciklama(row.get("KU_aciklama").toString());
		} catch (Exception ex) {
			 System.out.println("KU_aciklama Hata :" + ex.toString());
		}try {		
			v.setGD_aciklama(row.get("GD_aciklama").toString());
		} catch (Exception ex) {
			 System.out.println("GD_aciklama Hata :" + ex.toString());
		}try {		
			v.setFk_kaydi_acan(((Long)row.get("fk_kaydi_acan")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_kaydi_acan Hata :" + ex.toString());
		}try {		
			v.setTarih(row.get("tarih").toString());
		} catch (Exception ex) {
			 System.out.println("tarih Hata :" + ex.toString());
		}try {		
			v.setFk_kapatan_agent(((Long)row.get("fk_kapatan_agent")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_kapatan_agent Hata :" + ex.toString());
		}try {		
			v.setKapanma_tarihi(row.get("kapanma_tarihi").toString());
		} catch (Exception ex) {
			 System.out.println("kapanma_tarihi Hata :" + ex.toString());
		}try {		
			v.setFk_kart(((Long)row.get("fk_kart")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_kart Hata :" + ex.toString());
		}try {		
			v.setFk_statu(((Long)row.get("fk_statu")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_statu Hata :" + ex.toString());
		}try {		
			v.setFk_havuz(((Long)row.get("fk_havuz")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_havuz Hata :" + ex.toString());
		}try {		
			v.setFk_agent(((Long)row.get("fk_agent")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_agent Hata :" + ex.toString());
		}try {		
			v.setFk_tip(((Long)row.get("fk_tip")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_tip Hata :" + ex.toString());
		}try {		
			v.setFk_geri_donus_kanali(((Long)row.get("fk_geri_donus_kanali")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_geri_donus_kanali Hata :" + ex.toString());
		}try {		
			v.setLevel(((Long)row.get("level")).intValue());
		} catch (Exception ex) {
			 System.out.println("level Hata :" + ex.toString());
		}try {		
			v.setTip_level1(((Long)row.get("tip_level1")).intValue());
		} catch (Exception ex) {
			 System.out.println("tip_level1 Hata :" + ex.toString());
		}try {		
			v.setTip_level2(((Long)row.get("tip_level2")).intValue());
		} catch (Exception ex) {
			 System.out.println("tip_level2 Hata :" + ex.toString());
		}try {		
			v.setTip_level3(((Long)row.get("tip_level3")).intValue());
		} catch (Exception ex) {
			 System.out.println("tip_level3 Hata :" + ex.toString());
		}try {		
			v.setTip_level4(((Long)row.get("tip_level4")).intValue());
		} catch (Exception ex) {
			 System.out.println("tip_level4 Hata :" + ex.toString());
		}try {		
			v.setTip_level5(((Long)row.get("tip_level5")).intValue());
		} catch (Exception ex) {
			 System.out.println("tip_level5 Hata :" + ex.toString());
		}try {		
			v.setArayan_numara(row.get("arayan_numara").toString());
		} catch (Exception ex) {
			 System.out.println("arayan_numara Hata :" + ex.toString());
		}try {		
			v.setCallid(row.get("callid").toString());
		} catch (Exception ex) {
			 System.out.println("callid Hata :" + ex.toString());
		}try {		
			v.setAdres(row.get("adres").toString());
		} catch (Exception ex) {
			 System.out.println("adres Hata :" + ex.toString());
		}try {		
			v.setKayit_tipi(((Long)row.get("kayit_tipi")).intValue());
		} catch (Exception ex) {
			 System.out.println("kayit_tipi Hata :" + ex.toString());
		}try {		
			v.setFk_proje(((Long)row.get("fk_proje")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_proje Hata :" + ex.toString());
		}try {		
			v.setFk_proje_konu(((Long)row.get("fk_proje_konu")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_proje_konu Hata :" + ex.toString());
		}try {		
			v.setReferans(row.get("referans").toString());
		} catch (Exception ex) {
			 System.out.println("referans Hata :" + ex.toString());
		}try {
			String O_TARIHI = row.get("okunma_tarihi").toString();
			if(O_TARIHI.equals("0000-00-00 00:00:00")) {
				O_TARIHI = "1900-01-01 01:01:01";
			}
			v.setOkunma_tarihi(O_TARIHI);
		} catch (Exception ex) {
			 System.out.println("okunma_tarihi Hata :" + ex.toString());
		}try {		
			v.setFk_egitim_durumu(((Long)row.get("fk_egitim_durumu")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_egitim_durumu Hata :" + ex.toString());
		}try {		
			v.setFk_oncelik(((Long)row.get("fk_oncelik")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_oncelik Hata :" + ex.toString());
		}try {		
			v.setIsOnayBekle(row.get("isOnayBekle").toString());
		} catch (Exception ex) {
			 System.out.println("isOnayBekle Hata :" + ex.toString());
		}try {		
			v.setTar(row.get("tar").toString());
		} catch (Exception ex) {
			 System.out.println("tar Hata :" + ex.toString());
		}try {		
			v.setYil(((Long)row.get("yil")).intValue());
		} catch (Exception ex) {
			 System.out.println("yil Hata :" + ex.toString());
		}try {		
			v.setAy(((Long)row.get("ay")).intValue());
		} catch (Exception ex) {
			 System.out.println("ay Hata :" + ex.toString());
		}try {		
			v.setGun(((Long)row.get("gun")).intValue());
		} catch (Exception ex) {
			 System.out.println("gun Hata :" + ex.toString());
		}try {		
			v.setSaat(((Long)row.get("saat")).intValue());
		} catch (Exception ex) {
			 System.out.println("saat Hata :" + ex.toString());
		}try {		
			v.setFk_taskb_kapanma_statu(((Long)row.get("fk_taskb_kapanma_statu")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_taskb_kapanma_statu Hata :" + ex.toString());
		}try {		
			v.setFk_aramakanali(((Long)row.get("fk_aramakanali")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_aramakanali Hata :" + ex.toString());
		}try {		
			v.setFk_aramasebebi(((Long)row.get("fk_aramasebebi")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_aramasebebi Hata :" + ex.toString());
		}try {		
			v.setFk_sonuckod(((Long)row.get("fk_sonuckod")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_sonuckod Hata :" + ex.toString());
		}try {		
			v.setOnly_tarih(row.get("only_tarih").toString());
		} catch (Exception ex) {
			 System.out.println("only_tarih Hata :" + ex.toString());
		}try {		
			v.setVcc(((Long)row.get("vcc")).intValue());
		} catch (Exception ex) {
			 System.out.println("vcc Hata :" + ex.toString());
		}try {		
			v.setInternet(((Long)row.get("internet")).intValue());
		} catch (Exception ex) {
			 System.out.println("internet Hata :" + ex.toString());
		}try {		
			v.setPhone(((Long)row.get("phone")).intValue());
		} catch (Exception ex) {
			 System.out.println("phone Hata :" + ex.toString());
		}try {		
			v.setFk_lokasyon(((Long)row.get("fk_lokasyon")).intValue());
		} catch (Exception ex) {
			 System.out.println("fk_lokasyon Hata :" + ex.toString());
		}try {		
			v.setYeniden_acilma_tar(row.get("yeniden_acilma_tar").toString());
		} catch (Exception ex) {
			 System.out.println("yeniden_acilma_tar Hata :" + ex.toString());
		}try {		
			v.setBitis_tar(row.get("bitis_tar").toString());
		} catch (Exception ex) {
			 System.out.println("bitis_tar Hata :" + ex.toString());
		}try {		
			v.setYa_tar(row.get("ya_tar").toString());
		} catch (Exception ex) {
			 System.out.println("ya_tar Hata :" + ex.toString());
		}try {		
			v.setIsReopen(((Long)row.get("isReopen")).intValue());
		} catch (Exception ex) {
			 System.out.println("isReopen Hata :" + ex.toString());
		}
		
		return v;
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	@Override
	public void setBILDIRIM(TRANSFER t) {
		try {
		 String sql1 = "call alo170.addTransferTask (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		 jdbcrw.update(sql1,new Object[] { t.getId(),t.getFk_gorev_statu(),t.getFk_parent(),t.getAciklama(),t.getCryp_aciklama(),t.getMU_aciklama(),t.getKU_aciklama(),t.getGD_aciklama(),t.getFk_kaydi_acan(),
				 t.getTarih(),t.getFk_kapatan_agent(),t.getKapanma_tarihi(),t.getFk_kart(),t.getFk_statu(),t.getFk_havuz(),t.getFk_agent(),t.getFk_tip(),t.getFk_geri_donus_kanali(),t.getLevel(),t.getTip_level1(),
				 t.getTip_level2(),t.getTip_level3(),t.getTip_level4(),t.getTip_level5(),t.getArayan_numara(),t.getCallid(),t.getAdres(),t.getKayit_tipi(),t.getFk_proje(),t.getFk_proje_konu(),t.getReferans(),t.getOkunma_tarihi(),
				 t.getFk_egitim_durumu(),t.getFk_oncelik(),t.getIsOnayBekle(),t.getTar(),t.getYil(),t.getAy(),t.getGun(),t.getSaat(),t.getFk_taskb_kapanma_statu(),t.getFk_aramakanali(),t.getFk_aramasebebi(),
				 t.getFk_sonuckod(),t.getOnly_tarih(),t.getVcc(),t.getInternet(),t.getPhone(),t.getFk_lokasyon(),t.getYeniden_acilma_tar(),t.getBitis_tar(),t.getYa_tar(),t.getIsReopen(),t.getCryp_MU_aciklama(),
				 t.getCryp_KU_aciklama(),t.getCryp_GD_aciklama() });
		 
		 List<TRANSFERDETAY> l = getTASKDBDETAY(t.getId());
		 
		 for (TRANSFERDETAY row : l) {
			 
			 setBILDIRIMDETAY(row);
		 }
		 
		 //KART k = getVATANDAS(t.getFk_kart());
		 
		 // setVATANDAS(k);
		 
		 // 14022536
		 
		 
		} catch (Exception ex) {
			System.out.println("HATA ---- " + t.getId());
			System.out.println(ex.getMessage());
		}
		 jdbceski.update("call TransferIdSil (?)", new Object[] { t.getId() });

		 
	}
	

	@Override
	public List<TRANSFERDETAY> getTASKDBDETAY(Integer ID) {
		String sql1 = "call getTaskDBDetay (?)";
		 List<TRANSFERDETAY> ret = new ArrayList<TRANSFERDETAY>();
		  List<Map<String, Object>>  rows = jdbceski.queryForList(sql1,new Object[] { ID });
		  for (Map<?, ?> row : rows) {
				
			  TRANSFERDETAY v = new TRANSFERDETAY();
			try {		
				v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setFk_gorev((Integer)row.get("fk_gorev"));
			} catch (Exception ex) {
				 System.out.println("fk_gorev Hata :" + ex.toString());
			}try {		
				v.setFk_agent((Integer)row.get("fk_agent"));
			} catch (Exception ex) {
				 System.out.println("fk_agent Hata :" + ex.toString());
			}try {		
				v.setTarih(row.get("tarih").toString());
			} catch (Exception ex) {
				 System.out.println("tarih Hata :" + ex.toString());
			}try {		
				v.setAciklama(row.get("aciklama").toString());
			} catch (Exception ex) {
				 System.out.println("aciklama Hata :" + ex.toString());
			}try {		
				v.setFk_detay_tip((Integer)row.get("fk_detay_tip"));
			} catch (Exception ex) {
				 System.out.println("fk_detay_tip Hata :" + ex.toString());
			}try {		
				v.setSistem((Integer)row.get("sistem"));
			} catch (Exception ex) {
				 System.out.println("sistem Hata :" + ex.toString());
			}try {		
				v.setIpadres(row.get("ipadres").toString());
			} catch (Exception ex) {
				 System.out.println("ipadres Hata :" + ex.toString());
			}
			
		ret.add(v);
	}
		  return ret;
	}
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	@Override
	public void setBILDIRIMDETAY(TRANSFERDETAY td) {
		String sql1 = " call alo170.addTransferDetay (?,?,?,?,?,?,?,?,?)";
		
		jdbcrw.update(sql1,new Object[] {td.getId(),td.getFk_gorev(),td.getFk_agent(),td.getTarih(),td.getAciklama(),td.getFk_detay_tip(),td.getSistem(),td.getIpadres(),td.getCryp_aciklama()});
		
		
	}

	@Override
	public List<vADET> getUpdateList() {
		String sql1 = "call TransferTaskList ";
		 List<vADET> ret = new ArrayList<vADET>();
		  List<Map<String, Object>>  rows = jdbceski.queryForList(sql1);
		  
		  for (Map<?, ?> row : rows) {
			  
			  vADET v = new vADET();
			  try {		
					v.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					 System.out.println("id Hata :" + ex.toString());
				}
			  ret.add(v);
		  }
		  
		  return ret;
	}
	
	@Override
	public List<vADET> getUpdateKartList() {
		String sql1 = "call TransferVatandasList ";
		 List<vADET> ret = new ArrayList<vADET>();
		  List<Map<String, Object>>  rows = jdbceski.queryForList(sql1);
		  
		  for (Map<?, ?> row : rows) {
			  
			  vADET v = new vADET();
			  try {		
					v.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					 System.out.println("id Hata :" + ex.toString());
				}
			  ret.add(v);
		  }
		  
		  return ret;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	@Override
	public void setVATANDAS(KART k) {
		
			String sql1 = "call alo170.addVatandas (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			String KT = k.getKayit_tarihi();
		
			
			try {
		
			}catch (Exception ex) {
				KT = "1900-01-01";
			}
			jdbcrw.update(sql1,new Object[] { k.getId(),k.getKayit_tipi(),k.getUkey(),k.getAdi(),k.getSoyadi(),k.getAdi2(),k.getAdi3(),Genel.encrypt(k.getTckimlik()),k.getTckimlik(),k.getAdres(),k.getFk_ulke(),k.getFk_il(),k.getFk_ilce(),k.getTel1(),k.getTel2(),k.getGsm(),k.getFax(),k.getEmail(),k.getFk_agent(),k.getFk_kaydi_yapan(),KT,k.getSifre(),k.getDt(),k.getSskno(),k.getFk_cinsiyet(),k.getFk_egitimdurumu(),k.getFk_gsoru(),k.getGsoru_cevap(),k.getIsVip(),k.getTmp_soru(),k.getTmp_cevap()});
			
			
			
			jdbceski.update("call TransferVatandasIdSil (?)", new Object[] { k.getId() });
	}

	@SuppressWarnings("deprecation")
	@Override
	 @Transactional(readOnly = true)
	public KART getVATANDAS(Integer ID) {
		String sql1 = "call trnKart (?)";
		  Map<String, Object>  row = jdbceski.queryForMap(sql1,new Object[] { ID });
		  KART k = new KART();
		  
		  try {		
				k.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}try {		
				k.setKayit_tipi((Integer)row.get("kayit_tipi"));
			} catch (Exception ex) {
				 System.out.println("kayit_tipi Hata :" + ex.toString());
			}try {		
				k.setUkey(row.get("ukey").toString());
			} catch (Exception ex) {
				 System.out.println("ukey Hata :" + ex.toString());
			}try {		
				k.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				 System.out.println("adi Hata :" + ex.toString());
			}try {		
				k.setSoyadi(row.get("soyadi").toString());
			} catch (Exception ex) {
				 System.out.println("soyadi Hata :" + ex.toString());
			}try {		
				k.setAdi2(row.get("adi2").toString());
			} catch (Exception ex) {
				 System.out.println("adi2 Hata :" + ex.toString());
			}try {		
				k.setAdi3(row.get("adi3").toString());
			} catch (Exception ex) {
				 System.out.println("adi3 Hata :" + ex.toString());
			}try {		
				k.setTckimlik(row.get("tckimlik").toString());
			} catch (Exception ex) {
				 System.out.println("tckimlik Hata :" + ex.toString());
			}try {		
				k.setAdres(row.get("adres").toString());
			} catch (Exception ex) {
				 System.out.println("adres Hata :" + ex.toString());
			}try {		
				k.setFk_ulke(((Long)row.get("fk_ulke")).intValue());
			} catch (Exception ex) {
				 System.out.println("fk_ulke Hata :" + ex.toString());
			}try {		
				k.setFk_il(((Long)row.get("fk_il")).intValue());
			} catch (Exception ex) {
				 System.out.println("fk_il Hata :" + ex.toString());
			}try {		
				k.setFk_ilce(((Long)row.get("fk_ilce")).intValue());
			} catch (Exception ex) {
				 System.out.println("fk_ilce Hata :" + ex.toString());
			}try {		
				k.setTel1(row.get("tel1").toString());
			} catch (Exception ex) {
				 System.out.println("tel1 Hata :" + ex.toString());
			}try {		
				k.setTel2(row.get("tel2").toString());
			} catch (Exception ex) {
				 System.out.println("tel2 Hata :" + ex.toString());
			}try {		
				k.setGsm(row.get("gsm").toString());
			} catch (Exception ex) {
				 System.out.println("gsm Hata :" + ex.toString());
			}try {		
				k.setFax(row.get("fax").toString());
			} catch (Exception ex) {
				 System.out.println("fax Hata :" + ex.toString());
			}try {		
				k.setEmail(row.get("email").toString());
			} catch (Exception ex) {
				 System.out.println("email Hata :" + ex.toString());
			}try {		
				k.setFk_agent(((Long)row.get("fk_agent")).intValue());
			} catch (Exception ex) {
				 System.out.println("fk_agent Hata :" + ex.toString());
			}try {		
				k.setFk_kaydi_yapan(((Long)row.get("fk_kaydi_yapan")).intValue());
			} catch (Exception ex) {
				 System.out.println("fk_kaydi_yapan Hata :" + ex.toString());
			}try {		
				
				String KT = row.get("kayit_tarihi").toString();
				
				try {
				
			
					
					if (KT.equals("0000-00-00")) {
						KT = "1900-01-01";
					}
					 else {
						k.setKayit_tarihi(KT);	
					}
					
				} catch (Exception ex) {	
					System.out.println("Kayıt tarihi : " + ex.getMessage());
					k.setKayit_tarihi("1900-01-01");	
				}
				
			} catch (Exception ex) {
				 System.out.println("kayit_tarihi Hata :" + ex.toString());
			}try {		
				k.setSifre(row.get("sifre_").toString());
			} catch (Exception ex) {
				 System.out.println("sifre Hata :" + ex.toString());
			}try {	
				
				
				String DT = row.get("dt").toString();
				try {
					
					SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");  
					Date date1=formatter1.parse(DT);
					
					if (DT.equals("0000-00-00")) {
						DT = "1900-01-01";
					}
					
					if (date1.getYear() < 1900) {
						k.setDt("1900-01-01");
					} else {
						k.setDt(DT);	
					}
					
				} catch (Exception ex) {	
					System.out.println("tarih : " + ex.getMessage());
					k.setDt("1900-01-01");	
				}
				
				
				
				
				
			} catch (Exception ex) {
				 System.out.println("dt Hata :" + ex.toString());
			}try {		
				k.setSskno(row.get("sskno").toString());
			} catch (Exception ex) {
				 System.out.println("sskno Hata :" + ex.toString());
			}try {		
				k.setFk_cinsiyet(((Long)row.get("fk_cinsiyet")).intValue());
			} catch (Exception ex) {
				 System.out.println("fk_cinsiyet Hata :" + ex.toString());
			}try {		
				k.setFk_egitimdurumu(((Long)row.get("fk_egitimdurumu")).intValue());
			} catch (Exception ex) {
				 System.out.println("fk_egitimdurumu Hata :" + ex.toString());
			}try {		
				k.setFk_gsoru(((Long)row.get("fk_gsoru")).intValue());
			} catch (Exception ex) {
				 System.out.println("fk_gsoru Hata :" + ex.toString());
			}try {		
				k.setGsoru_cevap(row.get("gsoru_cevap").toString());
			} catch (Exception ex) {
				 System.out.println("gsoru_cevap Hata :" + ex.toString());
			}try {		
				k.setIsVip(((Long)row.get("isVip")).intValue());
			} catch (Exception ex) {
				 System.out.println("isVip Hata :" + ex.toString());
			}try {		
				k.setTmp_soru(row.get("tmp_soru").toString());
			} catch (Exception ex) {
				 System.out.println("tmp_soru Hata :" + ex.toString());
			}try {		
				k.setTmp_cevap(row.get("tmp_cevap").toString());
			} catch (Exception ex) {
				 System.out.println("tmp_cevap Hata :" + ex.toString());
			}
		  
		  return k;
		  
	}

	@Override
	 @Transactional(readOnly = true)
	public vADET adetBildirim() {
		String sql1 = "call alo170.adetBildirim";
		  Map<String, Object>  row = jdbcrw.queryForMap(sql1);
		  vADET k = new vADET();
		  
		  try {		
				k.setId(((Long)row.get("id")).intValue());
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}
		  
		return k;
	}

	@Override
	 @Transactional(readOnly = true)
	public vADET adetBildirimDetay() {
		String sql1 = "call alo170.adetBildirimDetay";
		  Map<String, Object>  row = jdbcrw.queryForMap(sql1);
		  vADET k = new vADET();
		  
		  try {		
			  k.setId(((Long)row.get("id")).intValue());
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}
		  
		return k;
	}

	@Override
	 @Transactional(readOnly = true)
	public vADET adetVatandas() {
		String sql1 = "call alo170.adetVatandas";
		  Map<String, Object>  row = jdbcrw.queryForMap(sql1);
		  vADET k = new vADET();
		  
		  try {		
			  k.setId(((Long)row.get("id")).intValue());
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}
		  
		return k;
	}
	
	


	
	
	 
	 

}

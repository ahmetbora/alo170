package tr.com.saphira.alo170SSS;

public class vSSS {
	private Integer id;
	private Integer fk_sss_grup;
	private Integer fk_sss_altgrup;
	private Integer fk_agent;
	private Integer isWeb;
	private Integer www_show;
	private Integer isActive;
	private String 	baslik;
	private String 	aciklama;
	private String 	kisa_aciklama;
	private String 	lng;
	
	
	
	public vSSS() {
		super();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getfk_sss_grup() {
		return fk_sss_grup;
	}
	public void setfk_sss_grup(Integer fk_sss_grup) {
		this.fk_sss_grup = fk_sss_grup;
	}
	public Integer getfk_sss_altgrup() {
		return fk_sss_altgrup;
	}
	public void setfk_sss_altgrup(Integer fk_sss_altgrup) {
		this.fk_sss_altgrup = fk_sss_altgrup;
	}
	
	public Integer getfk_agent() {
		return fk_agent;
	}
	public void setfk_agent(Integer fk_agent) {
		this.fk_agent = fk_agent;
	}
	
	public Integer getisWeb() {
		return isWeb;
	}
	public void setisWeb(Integer isWeb) {
		this.isWeb = isWeb;
	}
	
	public Integer getwww_show() {
		return www_show;
	}
	public void setwww_show(Integer www_show) {
		this.www_show = www_show;
	}
	
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	
	public String getbaslik() {
		return baslik;
	}
	public void setbaslik(String baslik) {
		this.baslik = baslik;
	}
	
	public String getaciklama() {
		return aciklama;
	}
	public void setaciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	
	public String getkisa_aciklama() {
		return kisa_aciklama;
	}
	public void setkisa_aciklama(String kisa_aciklama) {
		this.kisa_aciklama = kisa_aciklama;
	}
	
	public String getlng() {
		return lng;
	}
	public void setlng(String lng) {
		this.lng = lng;
	}
	
}

package tr.com.saphira.alo170kurum.RAPOR;

public class rptKySure {
    private Integer id;
    private String tarih;
    private String mu;
    private String havuz;
    private String kurum;
    private String altkurum;
    private String arama_konusu;
    private String sube;
    private String sure;



    public rptKySure() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getmu(){
        return mu;
    }
    public void setmu(String mu){
        this.mu=mu;
    }

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String gethavuz(){
        return havuz;
    }
    public void sethavuz(String havuz){
        this.havuz=havuz;
    }

    public String getkurum(){
        return kurum;
    }
    public void setkurum(String kurum){
        this.kurum=kurum;
    }

    public String getaltkurum(){
        return altkurum;
    }
    public void setaltkurum(String altkurum){
        this.altkurum=altkurum;
    }

    public String getarama_konusu(){
        return arama_konusu;
    }
    public void setarama_konusu(String arama_konusu){
        this.arama_konusu=arama_konusu;
    }

    public String getsube(){
        return sube;
    }
    public void setsube(String sube){
        this.sube=sube;
    }

    public String getsure(){
        return sure;
    }
    public void setsure(String sure){
        this.sure=sure;
    }


}


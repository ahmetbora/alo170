package tr.com.saphira.alo170kurum.HOME;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.API.ApiDao;
import tr.com.saphira.alo170kurum.model.Lookup;

@Controller
@EnableWebSecurity
@RequestMapping(value="/home")

public class HomeContoller {
	
	@Autowired
	HomeDao dao;
	
	@Autowired
	ApiDao apidao;
	
	
	@RequestMapping(value="", method= RequestMethod.GET)
	public ModelAndView yeniBildirim() {
		ModelAndView model = new ModelAndView("HOME/home");
		DashBoard d =dao.getDashboardData();
		
		model.addObject("home",dao.getDataOzet(d));
		model.addObject("ozet", apidao.bugunAcilan());
		return model;
	}
	
	@ResponseBody
	@RequestMapping(value="/encTaskDBAciklama", method= RequestMethod.GET)
	public String encTaskDBAciklama() {
		List<Lookup> liste = dao.SifrelenmemisTaskAciklamaList();
		for (Lookup l : liste) {			
			dao.TaskAciklamaSifrele(l.getId(), Genel.encrypt(l.getAdi()));
		}
		
		
		return "ok";
	}
	
	
	@ResponseBody
	@RequestMapping(value="/encTaskDBDetay", method= RequestMethod.GET)
	public String encTaskDBDetay() {
		dao.AllDetayUpdate();
		
		return "ok";
	}
	
	

}

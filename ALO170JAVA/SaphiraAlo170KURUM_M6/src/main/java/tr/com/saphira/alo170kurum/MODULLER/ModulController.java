package tr.com.saphira.alo170kurum.MODULLER;


import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.AYARLAR.AyarlarService;
import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
import tr.com.saphira.alo170kurum.AYARLAR.vULKELER;
import tr.com.saphira.alo170kurum.BILDIRIM.vJSONRESULT;
import tr.com.saphira.alo170kurum.LOOKUP.LOOKUPService;
import tr.com.saphira.alo170kurum.model.Lookup;
import tr.com.saphira.alo170kurum.model.vTotal;


@SuppressWarnings("unused")
@Controller
@EnableWebSecurity
@RequestMapping(value="/modul")
public class ModulController {
    
    @Autowired 
	private ModulService service;
    
    @Autowired 
	private AyarlarService ayarlarService;
    
    @Autowired  
	private HttpServletRequest request;
    
    @Autowired
	LOOKUPService lookupService;
	
	@Autowired
    private JavaMailSender javaMailSender;

	

    @RequestMapping(value="", method= RequestMethod.GET)
	public ModelAndView home(HttpSession httpSession) {
		ModelAndView model = new ModelAndView("MODULLER/Home");
		return model;
	}
	@RequestMapping(value="/que-mudahale", method= RequestMethod.GET)
	public ModelAndView quemudahale(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("MODULLER/QueMudahale");
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");

   
        model.addObject("js",js);
		model.addObject("pageTitle","Que Müdahale");
        String url=request.getRequestURL().toString();
        String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/que-mudahale",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT quemudahalesave(String agent,String uzunluk,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		// MOBBING-ANKARA REGISTER
		if (agent == "1327") {
			server = "172.30.10.220";
		}
		if (agent == "2130") {
			server = "172.30.10.220";
		}
		if (agent == "2131") {
			server = "172.30.10.220";
		}
		if (agent == "3209") {
			server = "172.30.10.220";
		}
		if (agent == "4216") {
			server = "172.30.10.220";
		}
		// MOBBING
		
		if(agent.length()==4){
			if(agent.substring(0,1)=="1"){
				server = "172.30.10.252";
			}
			if(agent.substring(0,1)=="2"){
				server = "172.30.10.251";
			}
			if(agent.substring(0,1)=="3"){
				server = "172.30.10.221";
			}
			if(agent.substring(0,1)=="4"){
				server = "172.30.10.225";
			}
			if(agent.substring(0,1)=="5"){
				server = "172.30.10.220";
			}
			if(agent.substring(0,1)=="6"){
				server = "172.30.10.222";
			}
			if(agent.substring(0,1)=="8"){
				server = "172.30.10.224";
			}
			if(agent.substring(0,1)=="9"){
				server = "172.30.10.223";
			}
		}
		if(agent.length()==5){
			if(agent.substring(0,1)=="2"){
				server = "172.30.10.226";
			}
			if(agent.substring(0,1)=="6"){
				server = "172.30.10.227";
			}
		}
	// yetki yoksa sadece basarısız deneme log olacak
	/*	if(@$_SESSION["rol"]!="admin" and $row["fk_lokasyon"]!=@$_SESSION["lokasyon"]) {
		$sql3="insert into SMDR_Log.q3p_log SET agent='".$agent."', islemyapan='".$_SESSION["UserID"]."',status='0'";
		$db->query($sql3);
		echo 1;
	}*/
		if(uzunluk!="0"){
			res=service.queMudahale(agent, userId,uzunluk);
		}else{
			res=service.queMudahale(agent, userId,uzunluk);
			// asterisk den agi ile cıkıs yaptırılacak
		}

	
		return res;
	}



	@RequestMapping(value="/agent-hangup", method= RequestMethod.GET)
	public ModelAndView agenthangup(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Lokasyon Bazlı Çağrı Kapatanlar");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		if(userYetki.get("yetki_agenthangup")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		model.setViewName("MODULLER/AgentHangup");
		List<Lookup> lokasyon = lookupService.getLokasyon();
		model.addObject("lokasyon", lokasyon);
		return model;
	}

	@RequestMapping(value="/agent-hangup-basla", method= RequestMethod.POST)
	public ModelAndView agenthangupbasla(Integer lokasyon,String sdate,String fdate,HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ModelAndView model = new ModelAndView("MODULLER/AgentHangupBasla");
		String tablo=null;
		String serverIp=null;
		if(lokasyon>0){
			Map<String, Object> l = lookupService.getLokasyonById(lokasyon);
			serverIp=l.get("server_ip").toString();
			String[] arr=StringUtils.split(serverIp,".");
			tablo="queue_log_"+arr[arr.length-1];
		}
		Integer diff=Genel.dateDiff(sdate,fdate).getYears();
		List<Map<String, Object>> liste=service.AgentHangup(sdate, fdate, lokasyon, tablo);
		model.addObject("liste",liste);
		return model;
	}
	@RequestMapping(value="/agent-hangup-detay", method= RequestMethod.POST)
	public ModelAndView agenthangupdetay(Integer lokasyon,String sdate,String fdate,String ext,HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ModelAndView model = new ModelAndView("MODULLER/AgentHangupBasla");
		String tablo=null;
		String agent=ext.substring(0,1);
		if(agent=="1"){
			tablo="queue_log_252";
		}
		if(agent=="2"){
			tablo="queue_log_251";
		}
		if(agent=="3"){
			tablo="queue_log_221";
		}
		if(agent=="4"){
			tablo="queue_log_225";
		}
		if(agent=="5"){
			tablo="queue_log_220";
		}
		if(agent=="55"){
			tablo="queue_log_225";
		}
		if(agent=="6"){
			tablo="queue_log_222";
		}
		if(agent=="8"){
			tablo="queue_log_224";
		}
		if(agent=="9"){
			tablo="queue_log_223";
		}
		List<Map<String, Object>> liste=service.AgentHangupDetay(sdate, fdate, lokasyon, tablo,ext);
		model.addObject("liste",liste);
		return model;
	}

	@RequestMapping(value="/ivr-sms", method= RequestMethod.GET)
	public ModelAndView ivrsms(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","IVR SMS DURUMU SORGULAMA");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		if(userYetki.get("yetki_ivrsms")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		model.setViewName("MODULLER/IvrSms");
		List<Lookup> lokasyon = lookupService.getLokasyon();
		model.addObject("lokasyon", lokasyon);
		return model;
	}
	@RequestMapping(value="/ivr-sms-sorgula", method= RequestMethod.POST)
	public ModelAndView ivrsmssorgula(String tc,HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","IVR SMS DURUMU SORGULAMA");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		if(userYetki.get("yetki_ivrsms")==null){
			model.setViewName("MODULLER/YetkiYokAjax");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}
		List<Map<String, Object>> liste=service.IvrSms(tc);
		model.addObject("liste",liste);
		model.setViewName("MODULLER/IvrSmsSorgula");
		return model;
	}


	@RequestMapping(value="/ivr-sms-iptal",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT ivrsmsiptal(String tc,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		res=service.IvrSmsIptal(tc, userId);
		return res;
	}

	

	@RequestMapping(value="/sss", method= RequestMethod.GET)
	public ModelAndView sss(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Sık Sorulan Sorular");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		if(userYetki.get("yetki_bilgibankasi")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		model.setViewName("MODULLER/sss");
		List<Lookup> lokasyon = lookupService.getLokasyon();
		List<Lookup> konu = lookupService.getSssKonu();
		List<Lookup> altkonu = lookupService.getSssAltKonu(0);	
		model.addObject("lokasyon", lokasyon);
		model.addObject("konu", konu);
		model.addObject("altkonu", altkonu);
		return model;
	}


	@RequestMapping(value="/sss", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> sssData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		if(userYetki.get("yetki_bilgibankasi")==null){
			maps.put("data",null); 
			maps.put("total",0);
		  	 return maps;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        Integer statu=0;
        Integer fk_sss_grup=0;
        Integer fk_sss_altgrup=0;
        String baslik=null;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            statu=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			statu=null;
		}

		try {
            fk_sss_grup=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_sss_grup=null;
		}
		try {
            fk_sss_altgrup=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_sss_altgrup=null;
		}
		try {
            baslik=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			baslik=null;
		}

		
        
		List<Map<String,Object>>  liste =  service.getsss(0,statu,fk_sss_grup,fk_sss_altgrup,baslik,skip,take);
		List<vTotal> say = service.countsss(statu,fk_sss_grup,fk_sss_altgrup,baslik);
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}



	
	@RequestMapping(value="/sss-detay/{id}", method= RequestMethod.GET)
	public ModelAndView sssdetay(@PathVariable(value="id") Integer id,HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Sık Sorulan Sorular Detay");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		if(userYetki.get("yetki_bilgibankasi")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		model.setViewName("MODULLER/sssDetay");
		List<Map<String,Object>>  liste =  service.getsss(id,2,0,0,"",0,10);
		List<Lookup> lokasyon = lookupService.getLokasyon();
		List<Lookup> konu = lookupService.getSssKonu();
		List<Lookup> altkonu = lookupService.getSssAltKonu(0);	
		model.addObject("sss",liste.get(0));
		model.addObject("lokasyon", lokasyon);
		model.addObject("konu", konu);
		model.addObject("altkonu", altkonu);
		return model;
	}

	@RequestMapping(value="/sss-guncelle",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT sssguncelle(Integer statu,Integer web, Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,String aciklama,Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);

		 baslik = URLDecoder.decode(baslik, "UTF-8");
		 aciklama = URLDecoder.decode(aciklama, "UTF-8");

		//System.out.println(userYetki);
		if(userYetki.get("yetki_bilgibankasisv")==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			res.setMesaj(yetkiYokMsj);
			res.setSonuc("0");
		}else{
			
			res=service.sssGuncelle(statu,web, fk_sss_grup,fk_sss_altgrup,baslik,aciklama,userId,id);
			res.setMesaj("İşlem Başarılı");
			res.setSonuc("1");
		}
		
		return res;
	}


	@RequestMapping(value="/sss-ekle", method= RequestMethod.GET)
	public ModelAndView sssekle(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Sık Sorulan Sorular Ekle");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		

		if(userYetki.get("yetki_bilgibankasi")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		model.setViewName("MODULLER/sssEkle");

		List<Lookup> lokasyon = lookupService.getLokasyon();
		List<Lookup> konu = lookupService.getSssKonu();
		List<Lookup> altkonu = lookupService.getSssAltKonu(0);	

		model.addObject("lokasyon", lokasyon);
		model.addObject("konu", konu);
		model.addObject("altkonu", altkonu);
		return model;
	}
	@RequestMapping(value="/sss-kaydet",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT ssskaydet(Integer statu,Integer web, Integer fk_sss_grup,Integer fk_sss_altgrup,String baslik,String aciklama, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);

		 baslik = URLDecoder.decode(baslik, "UTF-8");
		 aciklama = URLDecoder.decode(aciklama, "UTF-8");

		//System.out.println(userYetki);
		if(userYetki.get("yetki_bilgibankasisv")==null){
			statu=0;
			web=0;
		}else{
			
			res=service.sssKaydet(statu,web, fk_sss_grup,fk_sss_altgrup,baslik,aciklama,userId);
			res.setMesaj("İşlem Başarılı");
			res.setSonuc("1");
		}
		
		return res;
	}


	
	@RequestMapping(value="/karaliste", method= RequestMethod.GET)
	public ModelAndView karaliste(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Kara Liste");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		if(userYetki.get("yetki_karaliste")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		model.setViewName("MODULLER/KaraListe");
		return model;
	}
	@RequestMapping(value="/karaliste", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> karalisteData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		if(userYetki.get("yetki_karaliste")==null){
			maps.put("data",null); 
			maps.put("total",0);
		  	 return maps;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        Integer isOnay=0;
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            isOnay=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			isOnay=1;
		}

		
        
		List<Map<String,Object>>  liste =  service.KaraListe(0,isOnay,skip,take);
		List<vTotal> say = service.countKaraListe(isOnay);
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}
	
	@RequestMapping(value="/karaliste-detay/{id}", method= RequestMethod.GET)
	public ModelAndView karalistedetay(@PathVariable(value="id") Integer id,HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Kara Liste Detay");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		if(userYetki.get("yetki_karaliste")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		model.setViewName("MODULLER/KaraListeDetay");
		List<Map<String,Object>>  item =  service.KaraListe(id,0,0,0);

		model.addObject("item",item.get(0));
		return model;
	}
	@RequestMapping(value="/karaliste-onay",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT karalisteonay(Integer id,String tarih, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_karaliste")==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			res.setMesaj(yetkiYokMsj);
			res.setSonuc("0");
		}else{
			
			res=service.KaraListeOnay(id,tarih,userId);
			res.setMesaj("İşlem Başarılı");
			res.setSonuc("1");
		}
		
		return res;
	}
	@RequestMapping(value="/karaliste-suresizonay",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT karalistesuresiz(Integer id,String tarih, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_karaliste")==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			res.setMesaj(yetkiYokMsj);
			res.setSonuc("0");
		}else{
			
			res=service.KaraListeSuresiz(id,userId);
			res.setMesaj("İşlem Başarılı");
			res.setSonuc("1");
		}
		
		return res;
	}
	@RequestMapping(value="/karaliste-sil",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT karalistesil(Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_karaliste")==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			res.setMesaj(yetkiYokMsj);
			res.setSonuc("0");
		}else{
			
			res=service.KaraListeSil(id,userId);
			res.setMesaj("İşlem Başarılı");
			res.setSonuc("1");
		}
		
		return res;
	}


	@RequestMapping(value="/vipliste", method= RequestMethod.GET)
	public ModelAndView vipliste(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","VIP Liste");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		if(userYetki.get("yetki_vipliste")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		model.setViewName("MODULLER/VipListe");
		return model;
	}
	@RequestMapping(value="/vipliste", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> viplisteData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		if(userYetki.get("yetki_vipliste")==null){
			maps.put("data",null); 
			maps.put("total",0);
		  	 return maps;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		List<Map<String,Object>>  liste =  service.VipListe(skip,take);
		List<vTotal> say = service.countVipListe();
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}
	
	@RequestMapping(value="/vipliste-sil",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT viplistesil(Integer id, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_vipliste")==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			res.setMesaj(yetkiYokMsj);
			res.setSonuc("0");
		}else{
			
			res=service.VipListeSil(id);
			res.setMesaj("İşlem Başarılı");
			res.setSonuc("1");
		}
		
		return res;
	}

  
	@RequestMapping(value="/mobil", method= RequestMethod.GET)
	public ModelAndView mobil(HttpServletRequest req, HttpServletResponse res,HttpSession session) {
		ModelAndView model = new ModelAndView("MODULLER/Mobil");
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		if(userYetki.get("yetki_vipliste")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}
        // kullanılacak css 
        List<String> css = new ArrayList<String>(); 
        css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
        css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
    

    
        css.add("/assets/vendors/custom/alertify/alertify.css");
        model.addObject("css",css);
        
        // kullanılacak js
        List<String> js=new ArrayList<String>();
        js.add("/assets/saphira/kendo/js/jszip.min.js");
        js.add("/assets/saphira/kendo/js/kendo.all.min.js");
        js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
        js.add("/assets/saphira/custom.js");
        js.add("/assets/saphira/jquery.blockUI.js");
        js.add("/assets/vendors/custom/alertify/alertify.js");
        model.addObject("js",js);
		model.addObject("pageTitle","Mobil Uygulama Kullanıcı Listesi");
		return model;
	}
	@RequestMapping(value="/mobil", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> mobilData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		if(userYetki.get("yetki_mobiluygulama")==null){
			maps.put("data",null); 
			maps.put("total",0);
		  	 return maps;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

		Integer kayit_tipi=0;
        String adi=null;
        String soaydi=null;
        String tc=null;
        String ssk=null;
        String tel=null;
		Integer fk_ulke=0;
		Integer fk_il=0;
		Integer fk_ilce=0;
		
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            adi=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			adi=null;
		}
		try {
            soaydi=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			soaydi=null;
		}
		try {
            tc=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			tc=null;
		}
		try {
            ssk=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			ssk=null;
		}
		try {
            tel=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			tel=null;
		}
		try {
            fk_ulke=filter.getAsJsonObject().getAsJsonArray("filters").get(5)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_ulke=null;
		}
		try {
            fk_il=filter.getAsJsonObject().getAsJsonArray("filters").get(6)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_il=null;
		}
		try {
            fk_ilce=filter.getAsJsonObject().getAsJsonArray("filters").get(7)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_ilce=null;
		}
		try {
            kayit_tipi=filter.getAsJsonObject().getAsJsonArray("filters").get(8)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			kayit_tipi=0;
		}

		
     
		List<Map<String,Object>>  liste =  service.VatandasListe(adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,skip,take,kayit_tipi);
		List<vTotal> say = service.countVatandasListe(adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,kayit_tipi);
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/mobil-onayla",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT mobilOnayla(Integer id,Integer statu,Integer sifre, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);

		if(userYetki.get("yetki_vatandasislemleri")==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			res.setMesaj(yetkiYokMsj);
			res.setSonuc("0");
		}else{
			if(sifre>0){
				int len = 10;
				int randNumOrigin = 48, randNumBound = 122;
				String sifre_=Genel.generateRandomPassword(len, randNumOrigin, randNumBound);
				res=service.VatandasOnayla(id,20,sifre_);
				res.setMesaj("İşlem Başarılı");
				res.setSonuc("1");
			}else{
				res=service.VatandasOnayla(id,statu,null);
				res.setMesaj("İşlem Başarılı");
				res.setSonuc("1");
			}
		}
		
		return res;
	}



	@RequestMapping(value="/vatandas", method= RequestMethod.GET)
	public ModelAndView sorgulama(HttpSession session, HttpServletRequest request) {
	ModelAndView model = new ModelAndView("MODULLER/Vatandas");
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_vatandasislemleri")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Vatandaş Sorgulama");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		List<vULKELER> ulkeler = lookupService.getUlkeler(0);
		model.addObject("ulkeler",ulkeler);
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/vatandas", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> vatandas(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		if(userYetki.get("yetki_mobiluygulama")==null){
			maps.put("data",null); 
			maps.put("total",0);
		  	 return maps;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }

        String adi=null;
        String soaydi=null;
        String tc=null;
        String ssk=null;
        String tel=null;
		Integer fk_ulke=0;
		Integer fk_il=0;
		Integer fk_ilce=0;
		
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            adi=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			adi=null;
		}
		try {
            soaydi=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			soaydi=null;
		}
		try {
            tc=filter.getAsJsonObject().getAsJsonArray("filters").get(2)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			tc=null;
		}
		try {
            ssk=filter.getAsJsonObject().getAsJsonArray("filters").get(3)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			ssk=null;
		}
		try {
            tel=filter.getAsJsonObject().getAsJsonArray("filters").get(4)
					    .getAsJsonObject().get("value")
					    .getAsString();				
		} catch (Exception e) {
			tel=null;
		}
		try {
            fk_ulke=filter.getAsJsonObject().getAsJsonArray("filters").get(5)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_ulke=null;
		}
		try {
            fk_il=filter.getAsJsonObject().getAsJsonArray("filters").get(6)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_il=null;
		}
		try {
            fk_ilce=filter.getAsJsonObject().getAsJsonArray("filters").get(7)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_ilce=null;
		}

		
        
		List<Map<String,Object>>  liste =  service.VatandasListe(adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,skip,take,1);
		List<vTotal> say = service.countVatandasListe(adi,soaydi,tc,ssk,tel,fk_ulke,fk_il,fk_ilce,1);
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

	@RequestMapping(value="/vatandas-duzenle/{id}", method= RequestMethod.GET)
	public ModelAndView sorgulama(@PathVariable(value="id") Integer id,HttpSession session, HttpServletRequest request) {
	ModelAndView model = new ModelAndView("MODULLER/VatandasDuzenle");
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);

		if(userYetki.get("yetki_vatandasislemleri")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Vatandaş Bilgisi Düzenle");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		List<vULKELER> ulkeler = lookupService.getUlkeler(0);
		model.addObject("ulkeler",ulkeler);
		model.addObject("menu",urlArr[urlArr.length-1]);
		Map<String,Object> v=service.getVatandas(id);
		model.addObject("v",v);
		List<Lookup> ed=lookupService.getEgitimDurumu(0);
		model.addObject("ed",ed);
		List<vULKELER> ulke = lookupService.getUlkeler(0);
		model.addObject("ulke",ulke);
		List<Lookup> cinsiyet=lookupService.getCinsiyet();
		model.addObject("cinsiyet",cinsiyet);
		return model;
	}

	@RequestMapping(value="/vatandas-guncelle",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT vatandasGuncelle(String adi,String soyadi,String tckimklik,String dt,String tel,String gsm,
	Integer fk_egitim_durumu,Integer fk_ulke,Integer fk_il,Integer fk_ilce,String ssk,String email,Integer fk_cinsiyet,String fax,Integer id,
	 HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_vatandasislemleri")==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			res.setMesaj(yetkiYokMsj);
			res.setSonuc("0");
		}else{
			
			res=service.VatandasGuncelle(userId,id,adi,soyadi,tckimklik,dt,tel,gsm,fk_egitim_durumu,fk_ulke,fk_il,fk_ilce,ssk,email,fk_cinsiyet,fax);
			res.setMesaj("İşlem Başarılı");
			res.setSonuc("1");
		}
		
		return res;
	}


	@RequestMapping(value="/personel-veri", method= RequestMethod.GET)
	public ModelAndView personelveri(HttpSession session, HttpServletRequest request) {
	ModelAndView model = new ModelAndView("MODULLER/PersonelVeri");
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_vatandasislemleri")==null){
			model.setViewName("MODULLER/YetkiYok");
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			return model;
		}

		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Personel Veri Güncelle");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		List<vULKELER> ulkeler = lookupService.getUlkeler(0);
		model.addObject("ulkeler",ulkeler);
		model.addObject("menu",urlArr[urlArr.length-1]);
		List<Lookup> lokasyon = lookupService.getLokasyon();
		model.addObject("lokasyon", lokasyon);
		return model;
	}
	@RequestMapping(value="/personel-veri", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> personelveriData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		if(userYetki.get("yetki_personelislemleri") ==null){
			maps.put("data",null); 
			maps.put("total",0);
		  	 return maps;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }


		Integer fk_lokasyon=0;
		
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            fk_lokasyon=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			fk_lokasyon=0;
		}

		List<vKULLANICILAR>  vKullanicilar =  ayarlarService.KullaniciFiltreListesi(null, null,null,1,fk_lokasyon,skip,take);
		List<vTotal> say = ayarlarService.countKullaniciFiltreListesi(null, null, null,1,fk_lokasyon);
		 
		 maps.put("data",vKullanicilar); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

	@RequestMapping(value="/personel-veri-update",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT personelveriUpdate(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		if(userYetki.get("yetki_personelislemleri") ==null){
			response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			res.setSonuc("0");
		  	 return res;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonParser jsonParser = new JsonParser();
		JsonArray models =jsonParser.parse(querystring)
		.getAsJsonObject().getAsJsonArray("models");
		for (int i = 0; i < models.size(); i++) {
			String sid=models.get(i).getAsJsonObject().get("id").getAsString();
			Integer id=Integer.valueOf(sid);
			String tckimlik=models.get(i).getAsJsonObject().get("tckimlik").toString();
			tckimlik=tckimlik.replaceAll("\"","");
			if(tckimlik.matches(Genel.regexTCNO())){
				ayarlarService.kullaniciGuncelle(id,null, null, null, null, null, null, tckimlik, null, null, 0, 5, userId, null, null, 0, 0,1,0);
			}else{
				response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				res.setSonuc("0");
				return res;
			}
		}
			
		res.setSonuc("1");
		return res;
	}



	@RequestMapping(value="/izin-taleplerim", method= RequestMethod.GET)
	public ModelAndView izintaleplerim(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		String rol=session.getAttribute("User_ROL").toString();
		switch (rol) {
			case "VT":
			model.setViewName("MODULLER/IzinTaleplerimVT");
			break;
			case "SV":
			model.setViewName("MODULLER/IzinTaleplerimSV");
			break;
			case "TK":
			model.setViewName("MODULLER/IzinTaleplerimTK");
			break;
			case "AD":
			model.setViewName("MODULLER/IzinTaleplerimAD");
			break;
			default:
			model.setViewName("MODULLER/YetkiYok");
			break;
		}
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","İzin Taleplerim");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/izin-taleplerimSV", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> izintaleplerimSVData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }


		Integer f_tip=0;
		Integer isActive=0;
		
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            f_tip=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			f_tip=0;
		}
		try {
            isActive=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			isActive=0;
		}

		Integer fk_lokasyon=(Integer) session.getAttribute("User_fkLokasyon");
		List<Map<String,Object>>  liste = service.izinTalepSv(fk_lokasyon, userId, isActive, f_tip, skip, take); 
		List<vTotal> say = service.countizinTalepSv(fk_lokasyon, userId, isActive, f_tip);
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

	@RequestMapping(value="/izin-taleplerimTK", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> izintaleplerimTKData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }


		Integer f_tip=0;
		Integer isActive=0;
		
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            f_tip=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			f_tip=0;
		}
		try {
            isActive=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			isActive=0;
		}

		Integer fk_lokasyon=(Integer) session.getAttribute("User_fkLokasyon");
		List<Map<String,Object>>  liste = service.izinTalepTk(fk_lokasyon, userId, isActive, f_tip, skip, take); 
		List<vTotal> say = service.countizinTalepTk(fk_lokasyon, userId, isActive, f_tip);
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/izin-taleplerimVT", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> izintaleplerimVTData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  

		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }



		Integer isActive=0;
		
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            isActive=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			isActive=0;
		}

		Integer fk_lokasyon=(Integer) session.getAttribute("User_fkLokasyon");
		List<Map<String,Object>>  liste = service.izinTalepVT(userId, isActive, skip, take); 
		List<vTotal> say = service.countizinTalepVT(userId, isActive);
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}
	@RequestMapping(value="/izin-taleplerimAD", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> izintaleplerimADData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }


		Integer f_tip=0;
		Integer isActive=0;
		
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            f_tip=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			f_tip=0;
		}
		try {
            isActive=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			isActive=0;
		}

		Integer fk_lokasyon=(Integer) session.getAttribute("User_fkLokasyon");
		List<Map<String,Object>>  liste = service.izinTalepAD(userId,skip, take); 
		List<vTotal> say = service.countizinTalepAD(userId);
		 
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

	@RequestMapping(value="/personel-izin-talebi-olustur", method= RequestMethod.GET)
	public ModelAndView personelizintalebiolustur(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		String rol=session.getAttribute("User_ROL").toString();
		model.setViewName("MODULLER/PersonelIzinTalebiOlustur");

		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Personel İzin Talebi Oluştur");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		String sdate;
		LocalDateTime now = LocalDateTime.now();  
		sdate=now.toString();
		String fdate; 
		fdate=now.toString();
		model.addObject("sdate",sdate);
		model.addObject("fdate",fdate);
		model.addObject("userRol",rol);
		List<Lookup> vardiya = lookupService.getVardiyaTipleri(null,"0");	
		model.addObject("vardiya",vardiya);
		model.addObject("userLokasyon",session.getAttribute("User_fkLokasyon").toString());
		return model;
	}


	@RequestMapping(value="/personel-izin-talep-kaydet",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT personelizintalepkaydet(String sdate, String ssaat,String fdate, String fsaat, Integer fk_sebep,String aciklama,Integer fk_agent
	,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		String userRol=session.getAttribute("User_ROL").toString();
		String userFkRol=session.getAttribute("User_fkROL").toString();
		vKULLANICILAR user=ayarlarService.getUserByID(fk_agent);
		Integer rol=(Integer)user.getFkRol().intValue();
		Integer lokasyon=(Integer)user.getFkLokasyon().intValue();
		Integer tipi=0;
		Integer fk_agent_=userId;
	
		if(userRol.equals("AD") ){
			if(rol==1 || rol==2){
				tipi=0;
			}
			if(rol==11){
				tipi=1;
			}
			if(rol==4){
				tipi=2;
			}
		
			service.izinTalepKaydet(sdate, ssaat, fdate, fsaat, fk_sebep, aciklama, fk_agent, tipi, fk_agent_, lokasyon, 1, userId, 0,0,0, 0,1,userFkRol);
		}else if(userRol.equals("SV")){
			tipi=1;
			if(rol==1 || rol==2){
				tipi=0;
			}
			if(rol==11){
				tipi=1;
			}
			lokasyon=(Integer) session.getAttribute("User_fkLokasyon");
			
			service.izinTalepKaydet(sdate, ssaat, fdate, fsaat, fk_sebep, aciklama, fk_agent, tipi, fk_agent_, lokasyon, 0,0, 0,0,1,fk_agent_,0,userFkRol);
		}else if(userRol.equals("TK")){
			tipi=0;
			service.izinTalepKaydet(sdate, ssaat, fdate, fsaat, fk_sebep, aciklama, fk_agent, tipi, fk_agent_, lokasyon, 0,0, 1,fk_agent_,0,0,0,userFkRol);
		}
		
		return res;
	}


	@RequestMapping(value="/izin-talebi-olustur", method= RequestMethod.GET)
	public ModelAndView izintalebiolustur(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		String rol=session.getAttribute("User_ROL").toString();
		model.setViewName("MODULLER/IzinTalebiOlustur");

		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","İzin Talebi Oluştur");
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);

		String sdate;
		LocalDateTime now = LocalDateTime.now();  
		sdate=now.toString();
		String fdate; 
		fdate=now.toString();
		model.addObject("sdate",sdate);
		model.addObject("fdate",fdate);
		model.addObject("userRol",rol);
		model.addObject("userId",userId);
		List<Lookup> vardiya = lookupService.getVardiyaTipleri(null,"0");	
		model.addObject("vardiya",vardiya);
		model.addObject("userLokasyon",session.getAttribute("User_fkLokasyon").toString());
		return model;
	}


	
	@RequestMapping(value="/izin-talep-kaydet",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT izintalepkaydet(String sdate, String ssaat,String fdate, String fsaat, Integer fk_sebep,String aciklama,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		String server=null;
		Integer userId=(Integer) session.getAttribute("User_ID");
		String userRol=session.getAttribute("User_ROL").toString();
		String userFkRol=session.getAttribute("User_fkROL").toString();
		vKULLANICILAR user=ayarlarService.getUserByID(userId);
		Integer rol=(Integer)user.getFkRol().intValue();
		Integer lokasyon=(Integer)user.getFkLokasyon().intValue();
		Integer fk_agent=userId;
		Integer tipi=0;
		Integer tl_onay=0;
		Integer sv_onay=0;
		Integer islem_sv=0;
		Integer islem_tl=0;
		Integer ad_onay=0;
		Integer islem_ad=0;
	
		if(userRol.equals("VT") || userRol.equals("MU") ){
			tipi=0;			
		}else if(userRol.equals("TK")){
			tipi=1;
			tl_onay=1;
			islem_tl=userId;
		}else if(userRol.equals("SV")){
			tipi=2;
			sv_onay=1;
			islem_tl=userId;
		}
		service.izinTalepKaydet(sdate, ssaat, fdate, fsaat, fk_sebep, aciklama, fk_agent, tipi, 0, lokasyon, ad_onay,islem_ad, tl_onay,islem_tl, sv_onay,islem_sv,0,userFkRol);
		return res;
	}


	@RequestMapping(value="/izin-talep-onayla", method= RequestMethod.GET)
	public ModelAndView izintaleponay(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		String rol=session.getAttribute("User_ROL").toString();
		model.setViewName("MODULLER/IzinTalepOnayAD");
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","İzin Talep Onay");
		model.addObject("userRol",rol);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/izin-talep-onayla", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> izintaleponaylaData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Integer userId=(Integer) session.getAttribute("User_ID");
		String rol=session.getAttribute("User_ROL").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		Map<String,Object> maps=new HashMap<>();  
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }


		Integer f_tip=0;
		Integer isActive=0;
		
		JsonParser jsonParser = new JsonParser();
		JsonElement filter =jsonParser.parse(querystring)
			    .getAsJsonObject().get("filter");

		try {
            f_tip=filter.getAsJsonObject().getAsJsonArray("filters").get(0)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			f_tip=0;
		}
		try {
            isActive=filter.getAsJsonObject().getAsJsonArray("filters").get(1)
					    .getAsJsonObject().get("value")
					    .getAsInt();				
		} catch (Exception e) {
			isActive=0;
		}

		Integer fk_lokasyon=(Integer) session.getAttribute("User_fkLokasyon");
		List<Map<String,Object>>  liste=null;
		List<vTotal> say=null;
		Integer fk_rol=Integer.parseInt(session.getAttribute("User_fkROL").toString());
		liste = service.izinTalepOnayListe(fk_rol,fk_lokasyon,skip, take); 
		say = service.countizinTalepOnayListe(fk_rol,fk_lokasyon);		
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

	
		
	@RequestMapping(value="/izin-talep-onay",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT izintaleponayla(Integer id,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		Integer userId=(Integer) session.getAttribute("User_ID");
		Integer fk_rol=Integer.parseInt(session.getAttribute("User_fkROL").toString());
		Integer onay=1;
		service.izinTalepOnay(userId, id, fk_rol, onay);
		return res;
	}
	@RequestMapping(value="/izin-talep-red",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT izintalepred(Integer id,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		Integer userId=(Integer) session.getAttribute("User_ID");
		Integer fk_rol=Integer.parseInt(session.getAttribute("User_fkROL").toString());
		Integer onay=2;
		service.izinTalepOnay(userId, id, fk_rol, onay);
		return res;
	}


	
	@RequestMapping(value="/script-konulari", method= RequestMethod.GET)
	public ModelAndView scriptkonulari(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		String rol=session.getAttribute("User_ROL").toString();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_bildirimscript").equals("true")){
			model.setViewName("MODULLER/ScriptKonulari");
		}else{
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			model.setViewName("MODULLER/YetkiYok");
		}
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Bildirim Script Konuları");
		model.addObject("userRol",rol);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
	
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/script-konulari", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> scriptkonularidata(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Map<String,Object> maps=new HashMap<>();
		Integer userId=(Integer) session.getAttribute("User_ID");
		String rol=session.getAttribute("User_ROL").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(!userYetki.get("yetki_bildirimscript").equals("true")){
			return maps;
		}
		  
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		List<Map<String,Object>>  liste=null;
		List<vTotal> say=null;
		liste = service.ScriptKonulari(skip, take); 
		say = service.countScriptKonulari();		
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

	@RequestMapping(value="/script-konu-update",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT scriptkonuupdate(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(!userYetki.get("yetki_bildirimscript").equals("true")){
			res.setSonuc("0");
			return res;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonParser jsonParser = new JsonParser();
		JsonArray models =jsonParser.parse(querystring)
		.getAsJsonObject().getAsJsonArray("models");
		for (int i = 0; i < models.size(); i++) {
			String sid=models.get(i).getAsJsonObject().get("id").getAsString();
			Integer id=Integer.valueOf(sid);
			String adi=models.get(i).getAsJsonObject().get("adi").toString();
			adi=adi.replaceAll("\"","");
			
			service.ScriptKonuUpdate(id, adi);
			res.setSonuc("1");
		}
		return res;
	}
	@RequestMapping(value="/script-konu-insert",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT scriptkonuinsert(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(!userYetki.get("yetki_bildirimscript").equals("true")){
			res.setSonuc("0");
			return res;
		}
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonParser jsonParser = new JsonParser();
		JsonArray models =jsonParser.parse(querystring)
		.getAsJsonObject().getAsJsonArray("models");
		for (int i = 0; i < models.size(); i++) {
			String adi=models.get(i).getAsJsonObject().get("adi").toString();
			adi=adi.replaceAll("\"","");
			
			service.ScriptKonuInsert(adi);
			res.setSonuc("1");
		}
		return res;
	}

	@RequestMapping(value="/script-liste", method= RequestMethod.GET)
	public ModelAndView scriptliste(HttpSession session, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		String rol=session.getAttribute("User_ROL").toString();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_bildirimscript").equals("true")){
			model.setViewName("MODULLER/ScriptListe");
		}else{
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			model.setViewName("MODULLER/YetkiYok");
		}
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Bildirim Script Listesi");
		model.addObject("userRol",rol);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
	
		model.addObject("menu",urlArr[urlArr.length-1]);
		return model;
	}
	@RequestMapping(value="/script-liste", method= RequestMethod.POST)
	public @ResponseBody Map<String,Object> scriptlisteData(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		Map<String,Object> maps=new HashMap<>();
		Integer userId=(Integer) session.getAttribute("User_ID");
		String rol=session.getAttribute("User_ROL").toString();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(!userYetki.get("yetki_bildirimscript").equals("true")){
			return maps;
		}
		  
		String querystring = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		JsonObject jsonObject = new JsonParser().parse(querystring).getAsJsonObject();
	    int take = jsonObject.get("take").getAsInt();
	    int skip = jsonObject.get("skip").getAsInt();   
	    int page = jsonObject.get("page").getAsInt();
	    int pageSize = jsonObject.get("pageSize").getAsInt();

	    if(take<1) {
	    	take=30;
	    }
	    if(skip<1) {
	    	skip=0;
	    }
	    if(page<1) {
	    	page=1;
	    }
	    if(pageSize<1) {
	    	pageSize=30;
	    }
		List<Map<String,Object>>  liste=null;
		List<vTotal> say=null;
		liste = service.ScriptListe(skip, take);
		say = service.countScriptListe();		
		 maps.put("data",liste); 
		 maps.put("total",say.get(0).getTotal());
		return maps;
	}

	@RequestMapping(value="/script-duzenle/{id}", method= RequestMethod.GET)
	public ModelAndView scriptliste(HttpSession session, HttpServletRequest request,
	@PathVariable(value = "id", required = true) Integer id
	) {
		ModelAndView model = new ModelAndView();
		String rol=session.getAttribute("User_ROL").toString();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_bildirimscript").equals("true")){
			model.setViewName("MODULLER/ScriptDuzenle");
		}else{
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			model.setViewName("MODULLER/YetkiYok");
		}
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Bildirim Script Detay");
		model.addObject("userRol",rol);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		Map<String,Object> item=service.ScriptDetay(id);
		model.addObject("item",item);
		List<Map<String,Object>> konu=service.ScriptKonulari(0,250);
		model.addObject("konu",konu);
		List<Map<String,Object>> fk_aramasebebi=lookupService.getCmbDetayWhere("bild_tnm_aramakanali","and fk_parent='asebep'");
		model.addObject("fk_aramasebebi", fk_aramasebebi);
		List<Map<String,Object>> kurum=lookupService.getCmbDetayWhere("task_konu","");
		model.addObject("kurum", kurum);
		return model;
	}

	@RequestMapping(value="/bildirim-script-kaydet",method=RequestMethod.POST)
	public @ResponseBody vJSONRESULT scriptkaydet(String fk_parent,String fk_aramasebebi,String tip_level1,String tip_level2,String tip_level3,String icerik,String aciklama,String id,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		vJSONRESULT res =new vJSONRESULT();
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(!userYetki.get("yetki_bildirimscript").equals("true")){
			res.setSonuc("0");
			return res;
		}
	
		service.ScriptKaydet(fk_parent, fk_aramasebebi, tip_level1, tip_level2, tip_level3, icerik, aciklama,id);
		return res;
	}

	@RequestMapping(value="/script-ekle", method= RequestMethod.GET)
	public ModelAndView scriptekle(HttpSession session, HttpServletRequest request
	) {
		ModelAndView model = new ModelAndView();
		String rol=session.getAttribute("User_ROL").toString();
		Integer userId=(Integer) session.getAttribute("User_ID");
		ayarlarService.getUserYetkiBildirimModelView(model, session);
		Map<String,Object> userYetki=ayarlarService.getUserYetkiFromJSON(session);
		if(userYetki.get("yetki_bildirimscript").equals("true")){
			model.setViewName("MODULLER/ScriptEkle");
		}else{
			String yetkiYokMsj="Bu İşlem İçin Yetkiniz Bulunmamaktadır.";
			model.addObject("yetkiYokMsj", yetkiYokMsj);
			model.setViewName("MODULLER/YetkiYok");
		}
		// kullanılacak css 
		List<String> css = new ArrayList<String>(); 
		css.add("/assets/saphira/kendo/styles/kendo.common.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.rtl.min.css"); 
		css.add("/assets/saphira/kendo/styles/kendo.uniform.min.css");
		css.add("/assets/vendors/custom/alertify/alertify.css");
		model.addObject("css",css);
		
		// kullanılacak js
		List<String> js=new ArrayList<String>();
		js.add("/assets/saphira/kendo/js/jszip.min.js");
		js.add("/assets/saphira/kendo/js/kendo.all.min.js");
		js.add("/assets/saphira/kendo/js/messages/kendo.messages.tr-TR.min.js");
		js.add("/assets/saphira/kendo/js/cultures/kendo.culture.tr-TR.min.js");
		js.add("/assets/saphira/custom.js");
		js.add("/assets/saphira/jquery.blockUI.js");
		js.add("/assets/vendors/custom/alertify/alertify.js");
		model.addObject("js",js);
		model.addObject("pageTitle","Bildirim Script Ekle");
		model.addObject("userRol",rol);
		String url=request.getRequestURL().toString();
		String[] urlArr=url.split("/");
		model.addObject("menu",urlArr[urlArr.length-1]);
		List<Map<String,Object>> konu=service.ScriptKonulari(0,250);
		model.addObject("konu",konu);
		List<Map<String,Object>> fk_aramasebebi=lookupService.getCmbDetayWhere("bild_tnm_aramakanali","and fk_parent='asebep'");
		model.addObject("fk_aramasebebi", fk_aramasebebi);
		List<Map<String,Object>> kurum=lookupService.getCmbDetayWhere("task_konu","");
		model.addObject("kurum", kurum);
		return model;
	}
}


 
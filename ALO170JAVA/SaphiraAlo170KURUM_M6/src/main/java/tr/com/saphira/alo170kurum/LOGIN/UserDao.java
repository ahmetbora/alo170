package tr.com.saphira.alo170kurum.LOGIN;

import java.util.List;

import tr.com.saphira.alo170kurum.model.SaphiraUsers;


public interface UserDao {
	
 public List<SaphiraUsers> listAllusers();
	 
	 public void addUser(SaphiraUsers saphirausers);
	 
	 public void updateUser(SaphiraUsers saphirausers);
	 
	 public void deleteUser(int id);
	 
	 public SaphiraUsers findUserById(int id);
	 
	 public User findUserByUserName(String username);
	 
	 public SaphiraUsers getLogin(String KEY);
}

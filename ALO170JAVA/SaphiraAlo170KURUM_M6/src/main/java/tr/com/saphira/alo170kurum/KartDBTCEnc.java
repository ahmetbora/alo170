package tr.com.saphira.alo170kurum;

import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;

import tr.com.saphira.alo170kurum.BILDIRIM.BildirimService;

public class KartDBTCEnc extends TimerTask  {
	 
	@Autowired
	@Qualifier("jdbcro")
	private JdbcTemplate jdbcro;
	
	@Autowired
	BildirimService service;
	 
	@Override
	public void run() {
	
		service.TCSifrele();
		  
	}

}

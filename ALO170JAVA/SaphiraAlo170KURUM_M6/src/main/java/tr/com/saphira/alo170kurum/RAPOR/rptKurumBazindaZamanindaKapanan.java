package tr.com.saphira.alo170kurum.RAPOR;

public class rptKurumBazindaZamanindaKapanan {
    private Integer id;
    private String kurum;
    private String birim;
    private String toplam;
    private String zamaninda_kapanan;
 




    public rptKurumBazindaZamanindaKapanan() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getkurum(){
        return kurum;
    }
    public void setkurum(String kurum){
        this.kurum=kurum;
    }

    public String getbirim(){
        return birim;
    }
    public void setbirim(String birim){
        this.birim=birim;
    }

    public String gettoplam(){
        return toplam;
    }
    public void settoplam(String toplam){
        this.toplam=toplam;
    }

    public String getzamaninda_kapanan(){
        return zamaninda_kapanan;
    }
    public void setzamaninda_kapanan(String zamaninda_kapanan){
        this.zamaninda_kapanan=zamaninda_kapanan;
    }

}


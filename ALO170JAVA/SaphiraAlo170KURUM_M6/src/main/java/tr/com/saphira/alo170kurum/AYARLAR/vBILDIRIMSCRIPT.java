package tr.com.saphira.alo170kurum.AYARLAR;

public class vBILDIRIMSCRIPT {
	private Integer id;
	private String script;
	private String aciklama;
	private Integer fk_parent;
	private Integer fk_aramasebebi;
	private Integer isActive;
	private Integer tip_level1;
	private Integer tip_level2;
	private Integer tip_level3;
	
	public vBILDIRIMSCRIPT() {
		super();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getscript() {
		return script;
	}
	public void setscript(String script) {
		this.script = script;
	}
	
	public String getaciklama() {
		return aciklama;
	}
	public void seaciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	
	public Integer getfk_parent() {
		return fk_parent;
	}
	public void setfk_parent(Integer fk_parent) {
		this.fk_parent = fk_parent;
	}
	
	public Integer getfk_aramasebebi() {
		return fk_aramasebebi;
	}
	public void setfk_aramasebebi(Integer fk_aramasebebi) {
		this.fk_aramasebebi = fk_aramasebebi;
	}
	
	public Integer getisActive() {
		return isActive;
	}
	public void setisActive(Integer isActive) {
		this.isActive = isActive;
	}
	
	public Integer gettip_level1() {
		return tip_level1;
	}
	public void settip_level1(Integer tip_level1) {
		this.id = tip_level1;
	}
	
	public Integer gettip_level2() {
		return tip_level2;
	}
	public void settip_level2(Integer tip_level2) {
		this.tip_level2 = tip_level2;
	}
	
	public Integer gettip_level3() {
		return tip_level3;
	}
	public void settip_level3(Integer tip_level3) {
		this.tip_level3 = tip_level3;
	}

}

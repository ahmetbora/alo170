package tr.com.saphira.alo170kurum.RAPOR;

public class rptHavuzBildirimSure {
    private Integer id;
    private String tarih;
    private String statu;
    private String agent;
    private String havuz;
    private String sube;
    private String kurum;
    private String sure;
    private String yon_tarih;



    public rptHavuzBildirimSure() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String getagent(){
        return agent;
    }
    public void setagent(String agent){
        this.agent=agent;
    }

    public String gethavuz(){
        return havuz;
    }
    public void sethavuz(String havuz){
        this.havuz=havuz;
    }

    public String getsube(){
        return sube;
    }
    public void setsube(String sube){
        this.sube=sube;
    }

    public String getkurum(){
        return kurum;
    }
    public void setkurum(String kurum){
        this.kurum=kurum;
    }

    public String getsure(){
        return sure;
    }
    public void setsure(String sure){
        this.sure=sure;
    }

    public String getyon_tarih(){
        return yon_tarih;
    }
    public void setyon_tarih(String yon_tarih){
        this.yon_tarih=yon_tarih;
    }

    public String getstatu(){
        return statu;
    }
    public void setstatu(String statu){
        this.statu=statu;
    }

}


package tr.com.saphira.alo170kurum.API;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableWebSecurity
@RequestMapping(value="/dashboard")
public class APIController {
	
	@Autowired
	ApiDao dao;

	@ResponseBody
	@GetMapping(("/wallboard"))
	public Dashboard bugunAcilan() {
		
		
		
		return dao.bugunAcilan();

	}
	
	
	
}

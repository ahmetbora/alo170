package tr.com.saphira.alo170kurum.RAPOR;

public class rptSesKayit {
    private Integer id;
    private String vatandas;
    private String tarih;
    private String sure;
 




    public rptSesKayit() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getvatandas(){
        return vatandas;
    }
    public void setvatandas(String vatandas){
        this.vatandas=vatandas;
    }

    public String getsure(){
        return sure;
    }
    public void setsure(String sure){
        this.sure=sure;
    }

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

}


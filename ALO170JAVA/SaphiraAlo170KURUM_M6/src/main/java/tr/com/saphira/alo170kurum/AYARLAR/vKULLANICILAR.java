package tr.com.saphira.alo170kurum.AYARLAR;

public class vKULLANICILAR {
	
	private Integer id;
	private String rol;
	private String lokasyon;
	private String luser;
	private String kullaniciAdi;
	private String ext;
	private String password;
	private String password_t;
	private String adi;
	private String tckimlik;
	private Long bilgi_bankasi_editor;
	private Long email_bildirim;
	private Long isActive;
	private String email;
	private String durum;
	private String ukey;
	private String ksicil;
	private String emsicil;
	private String gsm;
	private String twitter;
	private String facebook;
	private Long fk_rol;
	private Long fk_lokasyon;
	private String yetki;
	private String yetkiJson;
	private Integer fk_kurum;
	public vKULLANICILAR() {
		super();
	
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	public String getLuser() {
		return luser;
	}
	public void setLuser(String luser) {
		this.luser = luser;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword_t() {
		return password_t;
	}
	public void setPassword_t(String password_t) {
		this.password_t = password_t;
	}
	public String getAdi() {
		return adi;
	}
	public void setAdi(String adi) {
		this.adi = adi;
	}
	public String getTckimlik() {
		return tckimlik;
	}
	public void setTckimlik(String tckimlik) {
		this.tckimlik = tckimlik;
	}
	public Long getBilgi_bankasi_editor() {
		return bilgi_bankasi_editor;
	}
	public void setBilgi_bankasi_editor(Long bilgi_bankasi_editor) {
		this.bilgi_bankasi_editor = bilgi_bankasi_editor;
	}
	public Long getEmail_bildirim() {
		return email_bildirim;
	}
	public void setEmail_bildirim(Long email_bildirim) {
		this.email_bildirim = email_bildirim;
	}
	public Long getIsActive() {
		return isActive;
	}
	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDurum() {
		return durum;
	}
	public void setDurum(String durum) {
		this.durum = durum;
	}	
	public String getUkey() {
		return ukey;
	}
	public void setUkey(String ukey) {
		this.ukey = ukey;
	}	
	public String getKsicil() {
		return ksicil;
	}
	public void setKsicil(String ksicil) {
		this.ksicil = ksicil;
	}	
	public String getEmsicil() {
		return emsicil;
	}
	public void setEmsicil(String emsicil) {
		this.emsicil = emsicil;
	}		
	public String getGsm() {
		return gsm;
	}
	public void setGsm(String gsm) {
		this.gsm = gsm;
	}	
	public String getTwitter() {
		return twitter;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public String getFacebook() {
		return facebook;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	public Long getFkRol() {
		return fk_rol;
	}
	public void setFkRol(Long fk_rol) {
		this.fk_rol = fk_rol;
	}
	public Long getFkLokasyon() {
		return fk_lokasyon;
	}
	public void setFkLokasyon(Long fk_lokasyon) {
		this.fk_lokasyon = fk_lokasyon;
	}	
	public String getYetki() {
		return yetki;
	}
	public void setYetki(String yetki) {
		this.yetki = yetki;
	}	
	public String getLokasyon() {
		return lokasyon;
	}
	public void setLokasyon(String lokasyon) {
		this.lokasyon = lokasyon;
	}	
	public String getYetkiJson() {
		return yetkiJson;
	}
	public void setYetkiJson(String yetkiJson) {
		this.yetkiJson = yetkiJson;
	}	
	public String getkullaniciAdi() {
		return kullaniciAdi;
	}
	public void setkullaniciAdi(String kullaniciAdi) {
		this.kullaniciAdi = kullaniciAdi;
	}	
	public Integer getfk_kurum() {
		return fk_kurum;
	}
	public void setfk_kurum(Integer fk_kurum) {
		this.fk_kurum = fk_kurum;
	}

}

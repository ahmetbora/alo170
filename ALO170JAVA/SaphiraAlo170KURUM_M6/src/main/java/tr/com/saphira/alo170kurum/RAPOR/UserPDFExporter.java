package tr.com.saphira.alo170kurum.RAPOR;
import java.awt.Color;
import java.io.IOException;
import java.util.List;
 
import javax.servlet.http.HttpServletResponse;
 
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;

import tr.com.saphira.alo170kurum.AYARLAR.vKULLANICILAR;
public class UserPDFExporter {
    private List<vKULLANICILAR> liste;
     
    public UserPDFExporter(List<vKULLANICILAR> liste) {
        this.liste = liste;
    }
 
    private void writeTableHeader(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLUE);
        cell.setPadding(3);
         
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);
         
        cell.setPhrase(new Phrase("ID", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Adı", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("E-mail", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Lokasyon", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("Yetki Grubu", font));
        table.addCell(cell);  

        cell.setPhrase(new Phrase("aciklama", font));
        table.addCell(cell);       
    }
     
    private void writeTableData(PdfPTable table) {
        for (vKULLANICILAR item : liste) {
            String lokasyon=null;
            String adi=null;
            String rol=null;
            try {
                adi=item.getAdi().toString();
                adi=adi.replaceAll("ı","i");
                adi=adi.replaceAll("ş","s");
                adi=adi.replaceAll("ğ","g");
                adi=adi.replaceAll("İ","I");
                adi=adi.replaceAll("Ş","S");
                adi=adi.replaceAll("Ğ","G");
            } catch (Exception e) {
                adi="";    
            }
            try {
                lokasyon=item.getLokasyon().toString();
                lokasyon=lokasyon.replaceAll("ı","i");
                lokasyon=lokasyon.replaceAll("ş","s");
                lokasyon=lokasyon.replaceAll("ğ","g");
                lokasyon=lokasyon.replaceAll("İ","I");
                lokasyon=lokasyon.replaceAll("Ş","S");
                lokasyon=lokasyon.replaceAll("Ğ","G");   
            } catch (Exception e) {
                lokasyon="";
            }

            try {
                rol=item.getRol().toString();
                rol=rol.replaceAll("ı","i");
                rol=rol.replaceAll("ş","s");
                rol=rol.replaceAll("ğ","g");
                rol=rol.replaceAll("İ","I");
                rol=rol.replaceAll("Ş","S");
                rol=rol.replaceAll("Ğ","G");    
            } catch (Exception e) {
               rol="";
            }

            String aciklama="AD SOYAD: MUSTAFA	ARIKAN TCKN: 56632157308            KURUM:  BORNOVA NACİ ŞAHİN SOSYAL GÜVENLİK MERKEZİ            KONU:  	ARGESU KİMYA MÜH. GERİ DÖN. ENERJİ SAN.ZİRAİ ÜRÜN.PLS.LTD.ŞT FİRMASINDAN 11. VE 12. AYDAKİ PRİMLERİM ELDEN VERİLMİŞTİR ANCAK HİZMET DÖKÜMÜMDE GÖRÜNMEMEKTEDİR. NE ZAMAN GÖRÜNTÜLENİR? KURUM İLE 1 AY ÖNCE İLE GÖRÜŞME SAĞLADIM; ANCAK İLGİNENEN GÖREVLİNİN DOĞUM İZNİNE AYRILDIĞI SÖYLENMİŞTİR. KONU HAKKINDA İVEDİLİKLE BİLGİ ALMAK İSTİYORUM.";
         
            aciklama=aciklama.replaceAll("ı","i");
            aciklama=aciklama.replaceAll("ş","s");
            aciklama=aciklama.replaceAll("ğ","g");
            aciklama=aciklama.replaceAll("İ","I");
            aciklama=aciklama.replaceAll("Ş","S");
            aciklama=aciklama.replaceAll("Ğ","G");   

            table.addCell(String.valueOf(item.getId()));
            table.addCell(adi);
            table.addCell(item.getEmail());
            table.addCell(lokasyon);
            table.addCell(rol);
            table.addCell(aciklama);
        }
    }
     
    public void export(HttpServletResponse response) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());
         
        document.open();
       
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        font.setSize(18);
        font.setColor(Color.BLUE);
         String baslik="ı-ü-ç-ş-ö-ğ----İ-Ü-Ç-Ş-Ö-Ğ";
         baslik=baslik.replaceAll("ı","i");
        Paragraph p = new Paragraph(baslik, font);
        p.setAlignment(Paragraph.ALIGN_CENTER);
         
        document.add(p);
         
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100f);
        table.setWidths(new float[] {0.5f, 1f, 1f, 1f, 1f, 3.0f});
        table.setSpacingBefore(10);
         
        writeTableHeader(table);
        writeTableData(table);
         
        document.add(table);
         
        document.close();
         
    }
}
package tr.com.saphira.alo170kurum.API;

public class Dashboard {
	
	private Integer task_acilan;
	private Integer task_agent_kapanan;
	private Integer task_kapanan;
	private Integer task_yeniden_acilan;
	private Integer pbx_gelen;
	private Integer pbx_bekl;
	private Integer pbx_kayip;
	private Integer pbx_gkayip;
	public Dashboard() {
		super();
		
	}
	public Integer getTask_acilan() {
		return task_acilan;
	}
	public void setTask_acilan(Integer task_acilan) {
		this.task_acilan = task_acilan;
	}
	public Integer getTask_agent_kapanan() {
		return task_agent_kapanan;
	}
	public void setTask_agent_kapanan(Integer task_agent_kapanan) {
		this.task_agent_kapanan = task_agent_kapanan;
	}
	public Integer getTask_kapanan() {
		return task_kapanan;
	}
	public void setTask_kapanan(Integer task_kapanan) {
		this.task_kapanan = task_kapanan;
	}
	public Integer getTask_yeniden_acilan() {
		return task_yeniden_acilan;
	}
	public void setTask_yeniden_acilan(Integer task_yeniden_acilan) {
		this.task_yeniden_acilan = task_yeniden_acilan;
	}
	public Integer getPbx_gelen() {
		return pbx_gelen;
	}
	public void setPbx_gelen(Integer pbx_gelen) {
		this.pbx_gelen = pbx_gelen;
	}
	public Integer getPbx_bekl() {
		return pbx_bekl;
	}
	public void setPbx_bekl(Integer pbx_bekl) {
		this.pbx_bekl = pbx_bekl;
	}
	public Integer getPbx_kayip() {
		return pbx_kayip;
	}
	public void setPbx_kayip(Integer pbx_kayip) {
		this.pbx_kayip = pbx_kayip;
	}
	public Integer getPbx_gkayip() {
		return pbx_gkayip;
	}
	public void setPbx_gkayip(Integer pbx_gkayip) {
		this.pbx_gkayip = pbx_gkayip;
	}	

}

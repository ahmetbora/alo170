package tr.com.saphira.alo170kurum.config;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class WebConfig {
 
 @Bean(name = "ro")
 @ConfigurationProperties(prefix = "spring.ro")
 public DataSource dataSource1() {
  return DataSourceBuilder.create().build();
 }

 @Bean(name = "jdbcro")
 public JdbcTemplate jdbcro(@Qualifier("ro") DataSource ds) {
  return new JdbcTemplate(ds);
 }
 
 @Bean(name = "rw")
 @ConfigurationProperties(prefix = "spring.rw")
 public DataSource dataSource2() {
  return  DataSourceBuilder.create().build();
 }

 @Bean(name = "jdbcrw")
 public JdbcTemplate jdbcrw(@Qualifier("rw") DataSource ds) {
  return new JdbcTemplate(ds);
 }
 
 @Bean(name = "eski")
 @ConfigurationProperties(prefix = "spring.eski")
 public DataSource dataSource3() {
  return  DataSourceBuilder.create().build();
 }
 
 @Bean(name = "jdbceski")
 public JdbcTemplate jdbceski(@Qualifier("eski") DataSource ds) {
  return new JdbcTemplate(ds);
 }
 
 @Bean(name = "pbx")
 @ConfigurationProperties(prefix = "spring.pbx")
 public DataSource dataSource4() {
  return  DataSourceBuilder.create().build();
 }

 @Bean(name = "jdbcpbx")
 public JdbcTemplate jdbcpbx(@Qualifier("pbx") DataSource ds) {
  return new JdbcTemplate(ds);
 }
}

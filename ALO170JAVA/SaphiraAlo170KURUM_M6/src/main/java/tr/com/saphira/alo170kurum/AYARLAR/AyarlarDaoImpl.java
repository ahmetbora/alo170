package tr.com.saphira.alo170kurum.AYARLAR;

// import java.io.File;
// import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;

import tr.com.saphira.alo170kurum.Genel;
import tr.com.saphira.alo170kurum.BILDIRIM.vJSONRESULT;
import tr.com.saphira.alo170kurum.model.vTotal;

@SuppressWarnings("unused")
@Transactional
@Repository
public class AyarlarDaoImpl implements AyarlarDao {
	
	 @Autowired
	 @Qualifier("jdbcro")
	 private JdbcTemplate jdbcro;
	 
	 @Autowired
	 @Qualifier("jdbcrw")
	 private JdbcTemplate jdbcrw;

	 @Autowired
	 @Qualifier("jdbcpbx")
	 private JdbcTemplate jdbcpbx;
	 
	 @Autowired
	 AyarlarService service;
	 

	 public List<vKURUMLAR> getKurumlar(Integer ID,String TERM,Integer skip,Integer take,Map<String,Object> userYetki) {
		  String sql1 = "call tnmKurumlar (?,?,?,?)";
		 
		  List<vKURUMLAR> ret = new ArrayList<vKURUMLAR>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID, TERM, skip, take });
		  for (Map<?, ?> row : rows) {
			vKURUMLAR v = new vKURUMLAR();
			try {		
					v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				System.out.println("adi Hata :" + ex.toString());
			}try {		
				v.setEmail(row.get("email").toString());
			} catch (Exception ex) {
				System.out.println("email Hata :" + ex.toString());
			}try {		
				v.setIsActive((Integer)row.get("isActive"));
			} catch (Exception ex) {
				System.out.println("isActive Hata :" + ex.toString());
			}try {		
				v.setDurum(row.get("durum").toString());
			} catch (Exception ex) {
				System.out.println("durum Hata :" + ex.toString());
			}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 @Override
	 public List<vTotal> countGetKurumlar(Integer ID,String TERM) {
		 String sql1 = "call tnmKurumlarCount (?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {  ID, TERM });
		 return getCountList(rows);
	 }

	 
	 public void setKurumlar(Integer ID, String ADI,String EMAIL, Integer ISACTIVE) {
		  String sql1 = "call tnmSETKurumlar (?,?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID,ADI,EMAIL,ISACTIVE });
		  
	 }
	 

	 
	 public List<vALTKURUMLAR> getAltKurumlar(Integer ID,String TERM,Integer fk_parent,Integer skip,Integer take,Map<String,Object> userYetki) {
		  String sql1 = "call tnmAltKurumlar (?,?,?,?,?)";
		
		  List<vALTKURUMLAR> ret = new ArrayList<vALTKURUMLAR>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID, TERM,fk_parent, skip, take  });
		  for (Map<?, ?> row : rows) {
				
			vALTKURUMLAR v = new vALTKURUMLAR();
			try {		
					v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				System.out.println("adi Hata :" + ex.toString());
			}try {		
				v.setEmail(row.get("email").toString());
			} catch (Exception ex) {
				System.out.println("email Hata :" + ex.toString());
			}try {		
				v.setKurumadi(row.get("kurumadi").toString());
			} catch (Exception ex) {
				System.out.println("kurumadi Hata :" + ex.toString());
			}try {		
				v.setIsActive((Integer)row.get("isActive"));
			} catch (Exception ex) {
				System.out.println("isActive Hata :" + ex.toString());
			}try {		
				v.setFk_parent((Integer)row.get("fk_parent"));
			} catch (Exception ex) {
				System.out.println("fk_parent Hata :" + ex.toString());
			}try {		
				v.setDurum(row.get("durum").toString());
			} catch (Exception ex) {
				 System.out.println("durum Hata :" + ex.toString());
			}
				
			ret.add(v);
		}
		  return ret;
		  
	 }
	 @Override
	 public List<vTotal> countGetAltKurumlar(Integer ID,String TERM,Integer fk_parent) {
		 String sql1 = "call tnmAltKurumlarCount (?,?,?)";
		 List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {  ID, TERM,fk_parent });
		 return getCountList(rows);
	 }
	 
	 public void setAltKurumlar(Integer ID, String ADI, Integer PARENT,String EMAIL, Integer ISACTIVE) {
		  String sql1 = "call tnmSETAltKurum (?,?,?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID,ADI,PARENT,EMAIL,ISACTIVE });
		  
	 }
	 
	 public List<vARAMAKONUSU> getAramaKonusu(Integer ID,String TERM,Integer fk_konu,Integer skip,Integer take) {
		  String sql1 = "call tnmAramaKonusu (?,?,?,?,?)";
		  List<vARAMAKONUSU> ret = new ArrayList<vARAMAKONUSU>();
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID,TERM,fk_konu,skip,take });
		  for (Map<?, ?> row : rows) {
				
			  vARAMAKONUSU v = new vARAMAKONUSU();
				try {		
					v.setId((Integer)row.get("id"));
			} catch (Exception ex) {
				 System.out.println("id Hata :" + ex.toString());
			}try {		
				v.setAdi(row.get("adi").toString());
			} catch (Exception ex) {
				 System.out.println("adi Hata :" + ex.toString());
			}try {		
				v.setKurumadi(row.get("kurum").toString());
			} catch (Exception ex) {
				 System.out.println("kurum Hata :" + ex.toString());
			}try {		
				v.setKonuadi(row.get("altkurum").toString());
			} catch (Exception ex) {
				 System.out.println("konu Hata :" + ex.toString());
			}try {		
				v.setFk_parent((Integer)row.get("fk_parent"));
			} catch (Exception ex) {
				 System.out.println("fk_parent Hata :" + ex.toString());
			}try {		
				v.setFk_konu((Integer)row.get("fk_konu"));
			} catch (Exception ex) {
				 System.out.println("fk_konu Hata :" + ex.toString());
			}try {		
				v.setIsActive((Integer)row.get("isActive"));
			} catch (Exception ex) {
				 System.out.println("isActive Hata :" + ex.toString());
			}
			try {		
				v.setDurum(row.get("durum").toString());
			} catch (Exception ex) {
				 System.out.println("durum Hata :" + ex.toString());
			}
			
		ret.add(v);
	}
		  return ret;
		  
	 }
	 public List<vTotal> countGetAramaKonusu(Integer ID,String TERM,Integer fk_konu) {
		String sql1 = "call tnmAramaKonusuCount (?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID,TERM,fk_konu });
		return getCountList(rows);
	 }
	 public void setAramaKonusu(Integer ID, String ADI, Integer KONU,Integer ALTKONU,String EMAIL, Integer ISACTIVE) {
		  String sql1 = "call tnmSETAramaKonusu (?,?,?,?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, ADI, KONU, ALTKONU, EMAIL, ISACTIVE });
		  
	 }
	 public List<Map<String,Object>> getSubeler(Integer ID,String TERM,Integer fk_konu,Integer fk_il,Integer skip,Integer take) {
		  String sql1 = "call tnmSubeler (?,?,?,?,?,?)";
		  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {  ID, TERM, fk_konu,fk_il, skip, take });
		  return rows;
		  /*List<vSUBELER> ret = new ArrayList<vSUBELER>();
		  for (Map<?, ?> row : rows) {
				
			  vSUBELER v = new vSUBELER();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			 System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
			 System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setKurum(row.get("kurum").toString());
		} catch (Exception ex) {
			 System.out.println("kurum Hata :" + ex.toString());
		}try {		
			v.setIl(row.get("il").toString());
		} catch (Exception ex) {
			 System.out.println("il Hata :" + ex.toString());
		}try {		
			v.setHavuz_sayisi(((Long)row.get("havuz_sayisi")).intValue());
		} catch (Exception ex) {
			 System.out.println("havuz_sayisi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			 System.out.println("isActive Hata :" + ex.toString());
		}try {		
			v.setFk_konu((Integer)row.get("fk_konu"));
		} catch (Exception ex) {
			 System.out.println("fk_konu Hata :" + ex.toString());
		}try {		
			v.setFk_il((Integer)row.get("fk_il"));
		} catch (Exception ex) {
			 System.out.println("fk_il Hata :" + ex.toString());
		}
		try {		
			v.setDurum(row.get("durumm").toString());
		} catch (Exception ex) {
			 System.out.println("durum Hata :" + ex.toString());
		}
			
		ret.add(v);
	}
		  return ret;*/
		  
	 }
	 public List<vTotal> countGetSubeler(Integer ID,String TERM,Integer fk_konu) {
		String sql1 = "call tnmSubelerCount (?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID,TERM,fk_konu });
		return getCountList(rows);
	 }

	 public void setSubeler(Integer ID, String ADI, Integer KONU, Integer IL, Integer ISACTIVE) {
		  String sql1 = "call tnmSETSubeler (?,?,?,?,?)";
		  
		   jdbcro.update(sql1,new Object[] { ID, ADI, KONU, IL, ISACTIVE });
		  
	 }
	/**
	 * 
	 * @param ID
	 * @param ISACTIVE 1,0 YADA BAŞKA INT DEĞER. 1 VE 0 HARICI DEĞER GİRİLİRSE FİLTREYİ YOK SAYAR 
	 * @param FKSUBE 0 İSE UYGULANMAZ
	 * @param FKIL 0 İSE UYGULANMAZ
	 * @param FKKONU 0 İSE UYGULANMAZ
	 * @param IDS STRING 1,2,3,9,8 SEKLİNDE ID LERİ SORGUYA SOKAR
	 * @param WHEREIN IDS PARAMETRESINDE GIRILEN ID LERİ IN YADA NOT IN OLACAĞINI BELİRLER. 0 IN, 1 NOT IN, 2 YOKSAY
	 * @param TERM ADI KISMINDA LİKE İLE ARAMA YAPAR. BOŞ BIRAKILIRSA UYGULANMAZ
	 * @return
	 */
	public List<Map<String, Object>> getHavuzlar(String IDS,Integer ISACTIVE,Integer FKSUBE,Integer FKIL,Integer FKKONU,Integer WHEREIN,String TERM,Integer skip,Integer take) {
		String sql1 = "call tnmHavuzlar (?,?,?,?,?,?,?,?,?)";
		
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {IDS,ISACTIVE,FKSUBE,FKIL,FKKONU,WHEREIN,TERM,skip,take });
		return rows;
		
	}
	public List<vTotal> countGetHavuzlar(String IDS,Integer ISACTIVE,Integer FKSUBE,Integer FKIL,Integer FKKONU,Integer WHEREIN,String TERM) {
		String sql1 = "call tnmHavuzlarCount (?,?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { IDS, ISACTIVE, FKSUBE, FKIL, FKKONU, WHEREIN, TERM });
		return getCountList(rows);
	}

		/**
	 * 
	 * @param ID
	 * @param ISACTIVE 1,0 YADA BAŞKA INT DEĞER. 1 VE 0 HARICI DEĞER GİRİLİRSE FİLTREYİ YOK SAYAR 
	 * @param fk_parent 0 İSE UYGULANMAZ
	 * @param IDS STRING 1,2,3,9,8 SEKLİNDE ID LERİ SORGUYA SOKAR
	 * @param WHEREIN IDS PARAMETRESINDE GIRILEN ID LERİ IN YADA NOT IN OLACAĞINI BELİRLER. 0 IN, 1 NOT IN, 2 YOKSAY
	 * @param TERM ADI KISMINDA LİKE İLE ARAMA YAPAR. BOŞ BIRAKILIRSA UYGULANMAZ
	 * @return
	 */
	public List<Map<String, Object>> getAramaKanali(String IDS,String TERM,Integer ISACTIVE,Integer fk_parent,Integer WHEREIN,Integer skip,Integer take) {
		String sql1 = "call tnmAramaKanali (?,?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {IDS,TERM,ISACTIVE,fk_parent,WHEREIN,skip,take });
		return rows;
	}
	public List<vTotal> countGetAramaKanali(String IDS,String TERM,Integer ISACTIVE,Integer fk_parent,Integer WHEREIN) {
		String sql1 = "call tnmAramaKanaliCount (?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { IDS,TERM,ISACTIVE,fk_parent,WHEREIN});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getEgitimDurumlari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take) {
		String sql1 = "call tnmEgitimDurumu (?,?,?,?,?,?)";
		//System.out.println("call tnmEgitimDurumu ('"+IDS+"','"+TERM+"',"+ISACTIVE+","+WHEREIN+","+skip+","+take+")");
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {IDS,TERM,ISACTIVE,WHEREIN,skip,take });
		return rows;
	}
	public List<vTotal> countGetEgitimDurumlari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN) {
		String sql1 = "call tnmEgitimDurumuCount (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { IDS,TERM,ISACTIVE,WHEREIN});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getGuvenlikSorulari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take) {
		String sql1 = "call tnmGuvenlikSorulari (?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {IDS,TERM,ISACTIVE,WHEREIN,skip,take });
		return rows;
	}
	public List<vTotal> countGetGuvenlikSorulari(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN) {
		String sql1 = "call tnmGuvenlikSorulariCount (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { IDS,TERM,ISACTIVE,WHEREIN});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getIller(String IDS,String TERM,Integer ISACTIVE,Integer fk_ulke,Integer WHEREIN,Integer skip,Integer take) {
		String sql1 = "call tnmIller (?,?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {IDS,TERM,ISACTIVE,fk_ulke,WHEREIN,skip,take });
		return rows;
	}
	public List<vTotal> countGetIller(String IDS,String TERM,Integer ISACTIVE,Integer fk_ulke,Integer WHEREIN) {
		String sql1 = "call tnmIllerCount (?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { IDS,TERM,ISACTIVE,fk_ulke,WHEREIN});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getIlceler(String IDS,String TERM,Integer ISACTIVE,Integer fk_il,Integer WHEREIN,Integer skip,Integer take) {
		String sql1 = "call tnmIlceler (?,?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {IDS,TERM,ISACTIVE,fk_il,WHEREIN,skip,take });
		return rows;
	}
	public List<vTotal> countGetIlceler(String IDS,String TERM,Integer ISACTIVE,Integer fk_il,Integer WHEREIN) {
		String sql1 = "call tnmIlcelerCount (?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { IDS,TERM,ISACTIVE,fk_il,WHEREIN});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getUlkeler(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN,Integer skip,Integer take) {
		String sql1 = "call tnmUlkeler (?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {IDS,TERM,ISACTIVE,WHEREIN,skip,take });
		return rows;
	}
	public List<vTotal> countGetUlkeler(String IDS,String TERM,Integer ISACTIVE,Integer WHEREIN) {
		String sql1 = "call tnmUlkelerCount (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { IDS,TERM,ISACTIVE,WHEREIN});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getDahili(Integer id,String EXT,Integer WHEREIN,Integer fk_lokasyon,Integer skip,Integer take) {
		String sql1 = "call tnmDahili (?,?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {id,EXT,WHEREIN,fk_lokasyon,skip,take });
		return rows;
	}
	public List<vTotal> countGetDahili(String EXT,Integer WHEREIN,Integer fk_lokasyon) {
		String sql1 = "call tnmDahiliCount (?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { EXT,WHEREIN,fk_lokasyon});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getKkoBaslik(Integer id , Integer isActive,Integer skip,Integer take) {
		String sql1 = "call tnmKkoBaslik (?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {id,isActive,skip,take });
		return rows;
	}
	public List<vTotal> countGetKkoBaslik(Integer id, Integer isActive) {
		String sql1 = "call tnmKkoBaslikCount (?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { id,isActive});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getKkoSoru(Integer id , Integer isActive,Integer fk_parent,Integer skip,Integer take) {
		String sql1 = "call tnmKkoSorular (?,?,?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {id,isActive,fk_parent,skip,take });
		return rows;
	}
	public List<vTotal> countGetKkoSoru(Integer id, Integer isActive,Integer fk_parent) {
		String sql1 = "call tnmKkoSorularCount (?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { id,isActive,fk_parent});
		return getCountList(rows);
	}
	public List<Map<String, Object>> getPeriyodik(Integer id ,Integer skip,Integer take) {
		String sql1 = "call tnmPeriyodik (?,?,?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {id,skip,take });
		return rows;
	}
	public List<vTotal> countGetPeriyodik(Integer id) {
		String sql1 = "call tnmPeriyodikCount (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { id});
		return getCountList(rows);
	}
	public vJSONRESULT delPeriyodik(Integer id) {
		String sql1 = "call tnmDelPeriyodik (?)";
		vJSONRESULT res =new vJSONRESULT();
		try{
			jdbcro.update(sql1,new Object[] {id});
			res.setSonuc("1");
		}catch(Exception e){
			res.setSonuc("0"); 
			res.setMesaj("Tanım Güncellemesi Sırasında Hata.");
		}
		return res;
	}


	public List<Map<String, Object>> getLokasyonlar() {
		String sql1 = "call spGetLokasyon ()";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		return rows;
	}
	public List<vTotal> countGetLokasyonlar() {
		String sql1 = "call spGetLokasyonCount ()";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1);
		return getCountList(rows);
	}

	public void setHavuzlar(Integer ID, String ADI, Integer KONU, Integer IL, Integer SUBE,String EMAIL, Integer ISACTIVE) {
		String sql1 = "call tnmSETHavuzlar (?,?,?,?,?,?,?)";
		jdbcro.update(sql1,new Object[] { ID, ADI, KONU, IL, SUBE, EMAIL, ISACTIVE });
	}
	
	@Override
	public List<vTotal> countKullaniciFiltreListesi(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon) {
		String sql1 = "call appUserListCount (?,?,?,?,?)";
		List<Map<String, Object>> rows = jdbcro.queryForList(sql1,new Object[] { ADI, EMAIL, ROL, isActive,fk_lokasyon });
		return getCountList(rows);
	}
	
	@Override
	public List<vKULLANICILAR> KullaniciFiltreListesi(String ADI, String EMAIL, String ROL,Integer isActive,Integer fk_lokasyon,Integer skip,Integer take) {
		// System.out.println("call appUserList ('"+ADI+"','"+EMAIL+"','"+ROL+"',"+isActive+","+fk_lokasyon+","+skip+","+skip+")");
			String sql1 = "call appUserList (?,?,?,?,?,?,?)";
			List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ADI, EMAIL, ROL, isActive,fk_lokasyon, skip, take });
			return getUserList(rows);
	}
	

// Combo Filtre

	public List<vALTKURUMLAR> getAltKurumlarFiltre(Integer KONU) {
		String sql1 = "call tnmAltKurumlarFiltre (?)";
		List<vALTKURUMLAR> ret = new ArrayList<vALTKURUMLAR>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { KONU });
		for (Map<?, ?> row : rows) {
			
			vALTKURUMLAR v = new vALTKURUMLAR();
	try {		
			v.setId((Integer)row.get("id"));
	} catch (Exception ex) {
			System.out.println("id Hata :" + ex.toString());
	}try {		
		v.setAdi(row.get("adi").toString());
	} catch (Exception ex) {
			System.out.println("adi Hata :" + ex.toString());
	}try {		
		v.setKurumadi(row.get("kurumadi").toString());
	} catch (Exception ex) {
			System.out.println("kurumadi Hata :" + ex.toString());
	}try {		
		v.setIsActive((Integer)row.get("isActive"));
	} catch (Exception ex) {
			System.out.println("isActive Hata :" + ex.toString());
	}try {		
		v.setFk_parent((Integer)row.get("fk_parent"));
	} catch (Exception ex) {
			System.out.println("fk_parent Hata :" + ex.toString());
	}try {		
		v.setDurum(row.get("durum").toString());
	} catch (Exception ex) {
			System.out.println("durum Hata :" + ex.toString());
	}
		
	ret.add(v);
}
		return ret;
		
	}
	
	public List<vARAMAKONUSU> getAramaKonusuFiltre(Integer KONU) {
		String sql1 = "call tnmAramaKonusuFiltre (?)";
		List<vARAMAKONUSU> ret = new ArrayList<vARAMAKONUSU>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { KONU });
		for (Map<?, ?> row : rows) {
			
			vARAMAKONUSU v = new vARAMAKONUSU();
			try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
				System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("adi").toString());
		} catch (Exception ex) {
				System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setKurumadi(row.get("kurumadi").toString());
		} catch (Exception ex) {
				System.out.println("kurumadi Hata :" + ex.toString());
		}try {		
			v.setKonuadi(row.get("konuadi").toString());
		} catch (Exception ex) {
				System.out.println("konuadi Hata :" + ex.toString());
		}try {		
			v.setFk_parent((Integer)row.get("fk_parent"));
		} catch (Exception ex) {
				System.out.println("fk_parent Hata :" + ex.toString());
		}try {		
			v.setFk_konu((Integer)row.get("fk_konu"));
		} catch (Exception ex) {
				System.out.println("fk_konu Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
				System.out.println("isActive Hata :" + ex.toString());
		}
		
	ret.add(v);
}
		return ret;
		
	}
	
	
	/**
	 * getSubelerFiltre
	 * @param ID 		sube id si 0 girilirse kullanılmaz. bir id girilirse o id ye ait subeyi tek kayıt olarka getirir.
	 * @param KONU		konu id si 0 olursa kullanılmaz
	 * @param TERM		sube adında aranacak boş bırakılırsa kullanılmaz
	 * @param FKIL		il id si 0 olursa kullanılmaz
	 * @param IDS		where in yada not için kullanılacak sube id leri. boş bırakılırsa kullanılmaz
	 * @param WHEREIN	where in mi yoksa not in mi olacağını belirler. 1 in 2 not in 0 olursa wherein kullanılmaz
	 * @return
	 */
	public List<vSUBELER> getSubelerFiltre(Integer ID,Integer KONU,String TERM,Integer FKIL,String IDS,Integer WHEREIN) {
			  String sql1 = "call tnmSubelerFiltre (?,?,?,?,?,?)";
			  List<vSUBELER> ret = new ArrayList<vSUBELER>();
			  List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID, KONU, TERM, FKIL, IDS, WHEREIN });
			  for (Map<?, ?> row : rows) {
					
				  vSUBELER v = new vSUBELER();
				  try {		
						v.setId((Integer)row.get("id"));
				} catch (Exception ex) {
					 System.out.println("id Hata :" + ex.toString());
				}try {		
					v.setAdi(row.get("adi").toString());
				} catch (Exception ex) {
					 System.out.println("adi Hata :" + ex.toString());
				}try {		
					v.setKurum(row.get("kurum").toString());
				} catch (Exception ex) {
					 System.out.println("kurum Hata :" + ex.toString());
				}try {		
					v.setIl(row.get("il").toString());
				} catch (Exception ex) {
					 System.out.println("il Hata :" + ex.toString());
				}try {		
					v.setHavuz_sayisi(((Long)row.get("havuz_sayisi")).intValue());
				} catch (Exception ex) {
					 System.out.println("havuz_sayisi Hata :" + ex.toString());
				}try {		
					v.setIsActive((Integer)row.get("isActive"));
				} catch (Exception ex) {
					 System.out.println("isActive Hata :" + ex.toString());
				}try {		
					v.setFk_konu((Integer)row.get("fk_konu"));
				} catch (Exception ex) {
					 System.out.println("fk_konu Hata :" + ex.toString());
				}try {		
					v.setFk_il((Integer)row.get("fk_il"));
				} catch (Exception ex) {
					 System.out.println("fk_il Hata :" + ex.toString());
				}
				
			ret.add(v);
		}
			  return ret;
			  
		 }
		
	
	
	/***
	 * isAdmin 1 olursa adminleri listeler
	* userId girilirse girilen id nin yonetebileceği yetki gruplarınıda alır
	*/
	public List<vYETKIGRUPLARI> getYetkiGruplari(Integer ID,Integer isAdmin,Integer tip,Integer level,Integer userId) {
		String sql1 = "call spGetCrmRolListe (?,?,?,?,?)";
		
		List<vYETKIGRUPLARI> ret = new ArrayList<vYETKIGRUPLARI>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID,isAdmin,tip,level,userId });
		for (Map<?, ?> row : rows) {
			
			vYETKIGRUPLARI v = new vYETKIGRUPLARI();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
				System.out.println("id Hata :" + ex.toString());
		}
		try {		
			v.setAdi((String)row.get("adi"));
		} catch (Exception ex) {
				System.out.println("adi Hata :" + ex.toString());
		}				
		try {		
			v.setisActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
				System.out.println("isActive" + ex.toString());
		}
		try {		
			v.setisHidden((Integer)row.get("isHidden"));
		} catch (Exception ex) {
				System.out.println("isHidden Hata :" + ex.toString());
		}				
		try {		
			v.setfk_rol((Integer)row.get("fk_rol"));
		} catch (Exception ex) {
			v.setfk_rol(0);
				System.out.println("fk_rol Hata :" + ex.toString());
		}				
		try {		
			v.setfk_user((Integer)row.get("fk_user"));
		} catch (Exception ex) {
				System.out.println("fk_user Hata :" + ex.toString());
		}				
		ret.add(v);
		}
		return ret;
		
	}

	


	public List<vYETKIGRUPLARI> getYetkiGruplariUser(Integer userId) {
	String sql1 = "call spGetCrmRolListeUser (?)";
	
	List<vYETKIGRUPLARI> ret = new ArrayList<vYETKIGRUPLARI>();
	List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId});
	for (Map<?, ?> row : rows) {
			
		vYETKIGRUPLARI v = new vYETKIGRUPLARI();
		try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
			System.out.println("id Hata :" + ex.toString());
		}
		try {		
			v.setAdi((String)row.get("adi"));
		} catch (Exception ex) {
			System.out.println("adi Hata :" + ex.toString());
		}				
		try {		
			v.setisActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
			System.out.println("isActive" + ex.toString());
		}
		try {		
			v.setisHidden((Integer)row.get("isHidden"));
		} catch (Exception ex) {
			System.out.println("isHidden Hata :" + ex.toString());
		}				
		try {		
			v.setfk_rol((Integer)row.get("fk_rol"));
		} catch (Exception ex) {
			v.setfk_rol(0);
			System.out.println("fk_rol Hata :" + ex.toString());
		}				
		try {		
			v.setfk_user((Integer)row.get("fk_user"));
		} catch (Exception ex) {
			System.out.println("fk_user Hata :" + ex.toString());
		}				
		ret.add(v);
	}
	return ret;
	
}
	
	
	public List<vYETKIALANLARI> getYetkiAlanlari(Integer ID,Integer isRoot,Integer yetkiGrubuID,Integer whereInType,String IDS,Integer isActive) {
		String sql1 = "call spGetYetkiAlanlariDetay (?,?,?,?,?,?)";
		IDS +=",0";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { ID,isRoot,yetkiGrubuID,whereInType,IDS,isActive });
		return getYetkiAlanlariList(rows);
		
	} 
	
	
	public List<vYETKIALANLARI> getUserYetkiAlanlariDetay(Integer userId) {
		String sql1 = "call spGetYetkiAlanlariDetayUser (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { userId });
		return getYetkiAlanlariList(rows);
	}
	
	
	@Override
	public vJSONRESULT yetkiGrubuDetayGuncelle(Integer fkYetki,Integer fkRol) {

		String sql1 = "call spYetkiGrubuDetayGuncelle (?,?)";
		vJSONRESULT res =new vJSONRESULT();
		try {
			jdbcro.update(sql1,new Object[] { 
					fkYetki,fkRol
				});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("Hata");
		}
		return res;
		
	}
	
	@Override
	public vJSONRESULT yetkiGrubuDetayTemizle(Integer fkRol) {

		String sql1 = "call spYetkiGrubuDetayTemizle (?)";
		vJSONRESULT res =new vJSONRESULT();
		try {
			jdbcro.update(sql1,new Object[] { 
					fkRol
				});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("Hata");
		}
		return res;
		
	}
	
	@Override
	public vJSONRESULT yetkiGrubuGuncelle(String adi,Integer isActive,Integer isHidden,Integer id) {

		String sql1 = "call spYetkiGrubuGuncelle (?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		try {
			jdbcro.update(sql1,new Object[] { 
					adi,isActive,isHidden,id
				});
			res.setSonuc("1");
			res.setMesaj("İşlem Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("Hata");
		}
		return res;
		
	}
	
	
	@Override
	public vJSONRESULT kullaniciKaydet(String adi,String email,String password,String username,String gsm,String ext,String tckimlik,String emsicil,String ksicil,Integer email_bildirim,Integer IsActive,Integer fkIslemYapan,Integer fk_rol,Integer fk_lokasyon,String fkEkleyen,Integer fk_kurum) {
		String sql1 = "call spKullaniciKaydet (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		try {
			jdbcro.update(sql1,new Object[] { 
				adi, email, password, username, gsm, ext, tckimlik, "", ksicil, email_bildirim, IsActive, fkIslemYapan,fk_rol,fk_lokasyon,fkEkleyen,fk_kurum
				});
			res.setSonuc("1");
			res.setMesaj("Kullanıcı Kaydı Başarılı");
		} catch(Exception ex) {
			System.out.println(" Hata :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("Kullanıcı Kaydı Hata");
		}
		return res;
		
	}

	
	@Override
	public vJSONRESULT kullaniciGuncelle(Integer id,String adi,String email,String password,String username,String gsm,String ext,String tckimlik,String emsicil,String ksicil,Integer email_bildirim,Integer IsActive,Integer fkIslemYapan,String face,String twit,Integer fk_rol, Integer fk_lokasyon,Integer isUpdated,Integer fk_kurum) {
		//System.out.println(id+"---"+adi+"---"+email+"---"+password+"---"+username+"---"+gsm+"---"+ext+"---"+tckimlik+"---"+emsicil+"---"+ksicil+"---"+email_bildirim+"---"+IsActive+"---"+fkIslemYapan+"---"+face+"---"+twit);
		String sql1 = "call spKullaniciGuncelle (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		vJSONRESULT res =new vJSONRESULT();
		vKULLANICILAR islemYapan=this.getUserByID(fkIslemYapan);
		String islemYapan_ =islemYapan.getAdi();
		tckimlik=tckimlik.replaceAll("\"","");
		try {
			// String encPass = null;
					
			if(password!="") {
				// encPass=Genel.encrypt(password);
			}else {
				// encPass="";
			}
			jdbcro.update(sql1,new Object[] { 
					id,adi, email, password, username, gsm, ext, tckimlik, emsicil, ksicil, email_bildirim, IsActive,fkIslemYapan,face,twit,fk_rol, fk_lokasyon,islemYapan.getAdi(),isUpdated,fk_kurum
				});

			//guncellenen bilgileri belirleyip loglamak için kullanıcıyı ac
			vKULLANICILAR user=this.getUserByID(id);
			Integer _adi = 0;
			Integer _email = 0;
			Integer _password = 0;
			Integer _username = 0;
			Integer _gsm = 0;
			Integer _ext = 0;
			Integer _fk_rol = 0;
			Integer _tckimlik = 0;
			Integer _emsicil = 0;
			Integer _ksicil = 0;
			Integer _fk_lokasyon = 0;
			Integer _fk_kurum = 0;

			String alanlar = "";	
			String userAdi=user.getAdi();
			String userEmail=user.getEmail();
			String userPass=user.getPassword();
			String userUsername=user.getLuser();
			String userGsm=user.getGsm();
			String userExt=user.getExt();
			String userTc=user.getTckimlik();
			String userEmSicil=user.getEmsicil();
			String Ksicil=user.getKsicil();
		


			if(userAdi.equals(adi)==false && adi!=null){
				_adi=1;
				alanlar ="Ad Soyad,";
			}
			if(userEmail.equals(email)==false && email!=null){
				_email=1;
				alanlar +="Email,";
			}
			if(password!=null){
				if(password.length()>0){
					if(userPass.equals(password)==false){
						_password=1;
						alanlar +="Şifre,";
					}
				}
			}
			if(userUsername.equals(username)==false && username!=null){
				_username=1;
				alanlar +="Kullanıcı Adı,";
			}
			if(userGsm.equals(gsm)==false && gsm!=null){
				_gsm=1;
				alanlar +="Cep Telefonu,";
			}
			if(userExt.equals(ext)==false && ext!=null){
				_ext=1;
				alanlar +="Dahili Numara,";
			}
			Integer userFkRol=user.getFkRol().intValue();
			
			if(userFkRol.equals(fk_rol)==false && fk_rol>0){
				_fk_rol=1;
				alanlar +="Yetki Rolü,";
			}
			if(userTc.equals(tckimlik)==false && tckimlik!=null){
				_tckimlik=1;
				alanlar +="T.C. Kimlik,";
			}
			if(userEmSicil.equals(emsicil)==false && emsicil!=null){
				_emsicil=1;
				alanlar +="Emekli San. Sic. No,";
			}
			if(Ksicil.equals(ksicil)==false && ksicil!=null){
				_ksicil=1;
				alanlar +="Kurum Scili No,";
			}
			Integer userFkLokasyon=user.getFkLokasyon().intValue();
			if(userFkLokasyon!=fk_lokasyon  && fk_lokasyon>0){
				_fk_lokasyon=1;
				alanlar +="Lokasyon Bilgisi,";
			}
			Integer userFkKurum=user.getfk_kurum().intValue();
			if(userFkKurum!=fk_kurum  && fk_kurum>0){
				_fk_kurum=1;
				alanlar +="Kurum Bilgisi,";
			}
			String aciklama = "DEĞİŞİKLİK YAPILAN BİLGİLER :<BR>"+alanlar;
			String sql2 = "call spAddSaphiraUsersLog (?,?,?)";
			jdbcro.update(sql2,new Object[] { 
				id,fkIslemYapan,aciklama
			});
			res.setSonuc("1");
			res.setMesaj("Kullanıcı Kaydı Başarılı");
							
		} catch(Exception ex) {
			System.out.println(" Kullanıcı Güncelleme Hatası :" + ex.toString());
			res.setSonuc("0");
			res.setMesaj("Kullanıcı Kaydı Hata");
		}
		return res;
		
	}
	
	
	
	@Override
	public vKULLANICILAR getUserByID(Integer ID) {
			String sql1 = "call spgetUser (?)";
			Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID });
			return getUser(row);
	}
	


	public List<vHAVUZLAR> getUserHavuz(Integer userId,Integer whereIN,String IDS,Integer fkHavuz,Integer fkSube,Integer fkIl,Integer fkKonu) {
	String sql1 = "call spGetTaskHavuzKullanici (?,?,?,?,?,?,?)";
	
		List<vHAVUZLAR> ret = new ArrayList<vHAVUZLAR>();
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] {  userId, whereIN, IDS, fkHavuz, fkSube, fkIl, fkKonu });
		for (Map<?, ?> row : rows) {
			
		vHAVUZLAR v = new vHAVUZLAR();
			try {		
				v.setId((Integer)row.get("id"));
		} catch (Exception ex) {
				System.out.println("id Hata :" + ex.toString());
		}try {		
			v.setAdi(row.get("havuz").toString());
		} catch (Exception ex) {
				System.out.println("adi Hata :" + ex.toString());
		}try {		
			v.setIsActive((Integer)row.get("isActive"));
		} catch (Exception ex) {
				System.out.println("isActive Hata :" + ex.toString());
		}try {		
			v.setFk_konu((Integer)row.get("fk_konu"));
		} catch (Exception ex) {
				System.out.println("fk_konu Hata :" + ex.toString());
		}try {		
			v.setFk_sube((Integer)row.get("fk_sube"));
		} catch (Exception ex) {
				System.out.println("fk_sube Hata :" + ex.toString());
		}try {		
			v.setFk_il((Integer)row.get("fk_il"));
		} catch (Exception ex) {
				System.out.println("fk_il Hata :" + ex.toString());
		}try {		
			v.setfkHavuz((Integer)row.get("fk_havuz"));
		} catch (Exception ex) {
				System.out.println("fk_havuz Hata :" + ex.toString());
		}
		
		
		ret.add(v);
		}
		return ret;
		
	}


	/**
	 * tek kullanıcı bilgileri için sabit
	* @param row
	* @return
	*/
	private vKULLANICILAR getUser(Map<String, Object>  row) {
		vKULLANICILAR res = new vKULLANICILAR();
	
		try {		
			res.setId((Integer)row.get("id"));
		} catch (Exception ex) {
				System.out.println("id Hata :" + ex.toString());
		}
		
		try {		
			res.setLuser((String)row.get("username"));
		} catch (Exception ex) {
				System.out.println("username Hata :" + ex.toString());
		}
		try {		
			res.setExt((String)row.get("ext"));
		} catch (Exception ex) {
				System.out.println("ext Hata :" + ex.toString());
		}				
		try {		
			res.setEmail((String)row.get("email"));
		} catch (Exception ex) {
				System.out.println("email Hata :" + ex.toString());
		}	
		try {		
			res.setAdi((String)row.get("adi"));
		} catch (Exception ex) {
				System.out.println("adi Hata :" + ex.toString());
		}					
		try {		
			res.setTckimlik(((String)row.get("tckimlik")));
		} catch (Exception ex) {
				System.out.println("tckimlik Hata :" + ex.toString());
		}
		try {		
			res.setEmsicil(((String)row.get("emsicil")));
		} catch (Exception ex) {
				System.out.println("emsicil Hata :" + ex.toString());
		}		
		try {		
			res.setKsicil(((String)row.get("ksicil")));
		} catch (Exception ex) {
				System.out.println("ksicil Hata :" + ex.toString());
		}		
		try {		
			res.setGsm(((String)row.get("gsm")));
		} catch (Exception ex) {
				System.out.println("gsm Hata :" + ex.toString());
		}	
		
		try {		
			res.setFacebook(((String)row.get("face")));
		} catch (Exception ex) {
				System.out.println("facebook Hata :" + ex.toString());
		}	
		try {		 
			res.setTwitter(((String)row.get("twit")));
		} catch (Exception ex) {
				System.out.println("twitter Hata :" + ex.toString());
		}	
		try {		
			res.setEmail_bildirim(((Number)row.get("email_bildirim")).longValue());
		} catch (Exception ex) {
				System.out.println("email_bildirim Hata :" + ex.toString());
		}		
		try {		
			res.setIsActive(((Number)row.get("isActive")).longValue());
		} catch (Exception ex) {
				System.out.println("isActive Hata :" + ex.toString());
		}	
		try {		
			String a=row.get("fk_rol").toString();
			Long num=Long.valueOf(a);
			res.setFkRol(num);
		} catch (Exception ex) {
				System.out.println("fk_rol Hata :" + ex.toString());
		}	
		try {		
			res.setFkLokasyon(((Number)row.get("fk_lokasyon")).longValue());
		} catch (Exception ex) {
				System.out.println("fk_lokasyon Hata :" + ex.toString());
		}		
		try {		
			res.setYetki(((String)row.get("yetki")));
		} catch (Exception ex) {
				System.out.println("yetki Hata :" + ex.toString());
		}
		try {		
			res.setRol(((String)row.get("rol")));
		} catch (Exception ex) {
				System.out.println("rol Hata :" + ex.toString());
		}	
		try {		
			res.setLokasyon(((String)row.get("lokasyon")));
		} catch (Exception ex) {
				System.out.println("lokasyon Hata :" + ex.toString());
		}
		
		try {		
			res.setDurum(((String)row.get("durum")));
		} catch (Exception ex) {
				System.out.println("lokasyon Hata :" + ex.toString());
		}
		try {		
			res.setUkey(((String)row.get("ukey")));
		} catch (Exception ex) {
				System.out.println("ukey Hata :" + ex.toString());
		}					
		try {		
			res.setYetkiJson(((String)row.get("yetkiJson")));
		} catch (Exception ex) {
				System.out.println("yetkiJson Hata :" + ex.toString());
		}	
		try {		
			res.setfk_kurum((Integer)row.get("fk_kurum"));
		} catch (Exception ex) {
				System.out.println("fk_kurum Hata :" + ex.toString());
		}				
		return res;
	}
	
	
	/***
	 * listelerde toplam kayıt sayısı için liste olusturur vTotal class içine aktarır
	 * @param rows
	 * @return
	 */
	private List<vTotal> getCountList(List<Map<String, Object>> rows) {
		List<vTotal> ret = new ArrayList<vTotal>();
		for (Map<?, ?> row : rows) {
			vTotal res = new vTotal();
			try {		
				res.setTotal((Long)row.get("total"));
			} catch (Exception ex) {
					System.out.println("total Hata :" + ex.toString());
			}
			ret.add(res);
		}
		return ret;
	}

	/**
	 * kullanıcıları listeleme için sabit.
	 * @param rows 
	 * @return
	 */
	private List<vKULLANICILAR> getUserList(List<Map<String, Object>> rows) {
		
		List<vKULLANICILAR> ret = new ArrayList<vKULLANICILAR>();
		
		for (Map<?, ?> row : rows) {
		
			vKULLANICILAR res = new vKULLANICILAR();
			try {		
				res.setId((Integer)row.get("id"));
			} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
			}
			try {		
				res.setExt((String)row.get("ext"));
			} catch (Exception ex) {
					System.out.println("ext Hata :" + ex.toString());
			}	
			try {		
				res.setEmail((String)row.get("email"));
			} catch (Exception ex) {
					System.out.println("email Hata :" + ex.toString());
			}	
			try {		
				res.setAdi((String)row.get("adi"));
			} catch (Exception ex) {
					System.out.println("adi Hata :" + ex.toString());
			}					
			try {		
				res.setTckimlik(((String)row.get("tckimlik")));
			} catch (Exception ex) {
					System.out.println("tckimlik Hata :" + ex.toString());
			}
			try {		
				res.setEmsicil(((String)row.get("emsicil")));
			} catch (Exception ex) {
					System.out.println("emsicil Hata :" + ex.toString());
			}		
			try {		
				res.setKsicil(((String)row.get("ksicil")));
			} catch (Exception ex) {
					System.out.println("ksicil Hata :" + ex.toString());
			}		
			try {		
				res.setGsm(((String)row.get("gsm")));
			} catch (Exception ex) {
					System.out.println("gsm Hata :" + ex.toString());
			}	
			
			try {		
				res.setFacebook(((String)row.get("facebook")));
			} catch (Exception ex) {
					System.out.println("facebook Hata :" + ex.toString());
			}	
			try {		 
				res.setTwitter(((String)row.get("twitter")));
			} catch (Exception ex) {
					System.out.println("twitter Hata :" + ex.toString());
			}	
			try {		
				res.setEmail_bildirim(((Long)row.get("email_bildirim")));
			} catch (Exception ex) {
					System.out.println("email_bildirim Hata :" + ex.toString());
			}		
			try {		
				res.setIsActive(((Long)row.get("isActive")));
			} catch (Exception ex) {
					System.out.println("isActive Hata :" + ex.toString());
			}					
			try {		
				res.setFkRol(((Long)row.get("fk_rol")));
			} catch (Exception ex) {
					System.out.println("fk_rol Hata :" + ex.toString());
			}	
			try {		
				res.setFkLokasyon(((Long)row.get("fk_lokasyon")));
			} catch (Exception ex) {
					System.out.println("fk_lokasyon Hata :" + ex.toString());
			}		
			try {		
				res.setYetki(((String)row.get("yetki")));
			} catch (Exception ex) {
					System.out.println("yetki Hata :" + ex.toString());
			}
			try {		
				res.setRol(((String)row.get("rol")));
			} catch (Exception ex) {
					System.out.println("rol Hata :" + ex.toString());
			}	
			try {		
				res.setLokasyon(((String)row.get("lokasyon")));
			} catch (Exception ex) {
					System.out.println("lokasyon Hata :" + ex.toString());
			}	
			try {		
				res.setDurum(((String)row.get("durum")));
			} catch (Exception ex) {
					System.out.println("lokasyon Hata :" + ex.toString());
			}	
			try {		
				res.setUkey(((String)row.get("ukey")));
			} catch (Exception ex) {
					System.out.println("ukey Hata :" + ex.toString());
			}		
			try {		
				res.setLuser((String)row.get("luser"));
			} catch (Exception ex) {
					System.out.println("luser Hata :" + ex.toString());
			}					
			try {		
				res.setExt((String)row.get("ext"));
			} catch (Exception ex) {
					System.out.println("ext Hata :" + ex.toString());
			}					
			ret.add(res);
	}
		return ret;
	}

		
	
	
	
	private List<vYETKIALANLARI> getYetkiAlanlariList(List<Map<String, Object>> rows) {
		
		List<vYETKIALANLARI> ret = new ArrayList<vYETKIALANLARI>();
		
		for (Map<?, ?> row : rows) {
			vYETKIALANLARI res = new vYETKIALANLARI();
			try {		
				
				res.setId((Integer)row.get("id"));
			} catch (Exception ex) {
					System.out.println("id Hata :" + ex.toString());
			}
			try {		
				res.setAdi((String)row.get("adi"));
			} catch (Exception ex) {
					System.out.println("adi Hata :" + ex.toString());
			}
			
			try {		
				res.setModul((String)row.get("modul"));
			} catch (Exception ex) {
					System.out.println("modul Hata :" + ex.toString());
			}
			try {		
				res.setFkparent((Integer)row.get("fk_parent"));
			} catch (Exception ex) {
					System.out.println("fk_parent" + ex.toString());
			}
			try {		
				res.setFkroot((Integer)row.get("fk_root"));
			} catch (Exception ex) {
					System.out.println("fk_root" + ex.toString());
			}
			try {		
				res.setIsroot((Integer)row.get("is_root"));
			} catch (Exception ex) {
					System.out.println("is_root" + ex.toString());
			}				
			try {		
				res.setAciklama((String)row.get("aciklama"));
			} catch (Exception ex) {
					System.out.println("aciklama Hata :" + ex.toString());
			}
			
			try {		
				res.setRootId((Integer)row.get("rootid"));
			} catch (Exception ex) {
					System.out.println("rootid Hata :" + ex.toString());
			}
			try {		
				res.setRootadi((String)row.get("rootadi"));
			} catch (Exception ex) {
					System.out.println("rootadi Hata :" + ex.toString());
			}
			
			try {		
				res.setRootmodul((String)row.get("rootmodul"));
			} catch (Exception ex) {
					System.out.println("rootmodul Hata :" + ex.toString());
			}
			
			try {		
				res.setRootaciklama((String)row.get("setrootaciklama"));
			} catch (Exception ex) {
					System.out.println("rootaciklama Hata :" + ex.toString());
			}
			
			try {		
				
				res.setFkRol((Long)row.get("fk_rol"));
			} catch (Exception ex) {
					System.out.println("fk_rol Hata :" + ex.toString());
			}
			try {		
				res.setFkYetki((Long)row.get("fk_yetki"));
			} catch (Exception ex) {
					System.out.println("fk_yetki Hata :" + ex.toString());
			}		
			try {		
			res.setUserYetkiId((Integer)row.get("user_yetki_id"));
			
			} catch (Exception ex) {
					System.out.println("user_yetki_id Hata :" + ex.toString());
			}	
			try {		
				res.setUserYetki((Integer)row.get("user_yetki"));
			} catch (Exception ex) {
					System.out.println("user_yetki Hata :" + ex.toString());
			}	
			try {		
				res.setUserYetkiUserId((Integer)row.get("user_id"));
			} catch (Exception ex) {
					System.out.println("user_id Hata :" + ex.toString());
			}							
			try {		
				res.setConnectedItems((String)row.get("connected_items"));
				
			} catch (Exception ex) {
					System.out.println("ConnectedItems Hata :" + ex.toString());
			}							
			ret.add(res);
		}
		return ret;
	}
	
	
	/**
	 * SADECE 1 KAYIT DÖNDÜRÜR PARAMETRELERDEN SADECE 1 TANESİ KULLANILIR.
	 * @param ID
	 * @param userName
	 * @param email
	 * @param tcKimlik
	 * @return
	 */
	@Override
	public vKULLANICILAR findUser(Integer ID,String userName,String email,String tcKimlik) {
			String sql1 = "call spFindUser (?,?,?,?)";
			
			Map<String, Object>  row = jdbcro.queryForMap(sql1,new Object[] { ID, userName, email, tcKimlik });
			return getUser(row);
	}
	
	
	public vJSONRESULT userHavuzGuncelle(Integer userID,Integer fkIslemYapan,String havuzlar) {
		String sql1 = "call spUserHavuzGuncelle (?,?,?,?,?)";
		// mevcut havuzları temizle
		String sql2 = "call spUserHavuzTemizle (?)";
		jdbcro.update(sql2,new Object[] { 
			userID
		});
		vJSONRESULT res =new vJSONRESULT();
		res.setSonuc("0");

		// id si gelen havuzları ekle
		if(havuzlar!=null){
			//dizi halinde gelen idleri böl
			String[] hvz=havuzlar.split(",");
			for (String item : hvz) {
				//id ye denk gelen havuz bilgilerini al
				List<Map<String, Object>> h=this.getHavuzlar(item.toString(),1, 0, 0,0,0,null,0,1);
				Integer fk_havuz=(Integer) h.get(0).get("id");
				Integer fk_sube=(Integer) h.get(0).get("fk_sube");
				Integer fk_il=(Integer) h.get(0).get("fk_il");
				Integer fk_konu=(Integer) h.get(0).get("fk_konu");
				try {
					//bilgileri alınan havuzu kullanıcıya ekle
					jdbcro.update(sql1,new Object[] { 
						userID,fk_havuz,fk_sube,fk_il,fk_konu	
					});
					res.setSonuc("1");
					res.setMesaj("Havuzlar Güncellendi");
				} catch(Exception ex) {
					System.out.println(" Hata :" + ex.toString());
					res.setSonuc("0");
					res.setMesaj("Havuz GÜncelleme Hatası");
				}
			}
		}else{
			res.setSonuc("0");
			res.setMesaj("Havuz GÜncelleme Hatası");	
		}

		return res;
	}
	
	@Override
	public vJSONRESULT yetkiGrubuDetayGuncelleUser(Integer userID,Integer fkIslemYapan,String yetkiler,Integer fkRol,Integer fkLokasyon,String yetkiRol) {
		vJSONRESULT res =new vJSONRESULT();
		String yetkilerJson="";
		String yetkilerJsonDefault="";
		/** eski yetkileri temizle */
		String sqlTemizle = "call spUserYetkiTemizle (?)";
		jdbcro.update(sqlTemizle,new Object[] {userID});
		/**
		 * yetkileri split ile ayırıp döngüye sokup db ye yazıyoruz
		 */
		Integer islemId=0;
		if(yetkiler!=null){
			String[] yetkiListe=yetkiler.split(",");
			for (int i = 0; i < yetkiListe.length; i++) {
				if(yetkiListe[i]!="" && yetkiListe[i]!=null){
					islemId=Integer.parseInt(yetkiListe[i]);
					String sql = "call spUserYetkiEkle (?,?)";
					jdbcro.update(sql,new Object[] {userID,islemId});
				}
			}
			res.setSonuc("1"); 
			res.setMesaj("Yetkiler Güncellendi.");
		}

		/****
		 * 
		 * yetkiRol
		 * yonetilecek yetki gruplarını db ye yaz
		 */
		String[] rolIds = yetkiRol.split(",");
		String sql = "call spClearCrmRolUser (?)";
		jdbcro.update(sql,new Object[] {userID});
		for (String rolId : rolIds) {
			if(rolId!="" && rolId!=null){
				String sqlc = "call spSetCrmRolUser (?,?)";
				jdbcro.update(sqlc,new Object[] {userID,rolId});
			}
			res.setSonuc("1"); 
			res.setMesaj("Yetkiler Güncellendi.");
		}
		/**
		 * yetkileri db den al
		 * yetki json dosyası yaz
		 */

		if(yetkiler!=null && yetkiler!="" && yetkiler.length()>0) {
			List<vYETKIALANLARI>   liste=  service.getYetkiAlanlari(0,0,0,0,yetkiler,1);
			String strYetkilerDefault="{\n";
			String strYetkilerJson="{\n";
			Integer say=liste.size();
			Integer i=0;
			for (vYETKIALANLARI yetki : liste) {
			
				i++;
				if(i<say) {
					strYetkilerJson +=" \""+yetki.getModul()+"\" : \"true\",\n";
					strYetkilerDefault +=" \""+yetki.getModul()+"\" : \"\",\n";
				}else {
					strYetkilerJson +=" \""+yetki.getModul()+"\" : \"true\" \n";
					strYetkilerDefault +=" \""+yetki.getModul()+"\" : \"\" \n";
				}
				
				
			}
			strYetkilerJson +="}";	
			strYetkilerDefault +="}";	
			yetkilerJson=strYetkilerJson;
			yetkilerJsonDefault=strYetkilerDefault;
		}
		String sqlYetki = "call spSaphiraUserYetkiGuncelle (?,?)";
		try{
			jdbcro.update(sqlYetki,new Object[] {userID,yetkilerJson});
		}catch(Exception e){
			res.setSonuc("0"); 
			res.setMesaj("Saphira Users Yetkiler Hata.");
		}

		/***kullanıcı lokasyonu ve rolu değişiklikleri kaydet */
		
		return res;
		
	}
	
	public  Map<String,Object> getUserYetkiFromJSON(HttpSession session){
		ObjectMapper objectMapper = new ObjectMapper();
		Integer userId=(Integer)session.getAttribute("User_ID");
		String yetkiJson=null;
		String icerik = null;
		try{
			vKULLANICILAR user=this.getUserByID(userId);
			yetkiJson=user.getYetkiJson();
		}catch(Exception e){
			yetkiJson=null;
		}
		try{
		icerik = new String(yetkiJson);
		}catch(Exception e){
			icerik="";
		}
	
		TypeReference<HashMap<String,Object>> typeRef 
		= new TypeReference<HashMap<String,Object>>() {};

		HashMap<String, Object> yetki;
		try {
			yetki = objectMapper.readValue(icerik, typeRef);
		} catch (IOException e) {
			e.printStackTrace();
			yetki=new HashMap<>();
		} 
		return yetki;
	}
	public  ModelAndView getUserYetkiBildirimModelView(ModelAndView model, HttpSession session) {
					Map<String,Object> yetki=getUserYetkiFromJSON(session);     
					model.addObject("yetki",yetki);  
					String rol=session.getAttribute("User_ROL").toString();
					switch (rol) {
						case "VT":
							model.addObject("rol","VT");
							break;
						case "TK":
							model.addObject("rol",session.getAttribute("User_ROL"));
							break;
						case "SV":
							model.addObject("rol",session.getAttribute("User_ROL"));
							break;
						case "AD":
							model.addObject("rol",session.getAttribute("User_ROL"));
							break;
						case "MU":
							model.addObject("rol","VT");
							break;
						case "SMU":
							model.addObject("rol","VT");
							break;
						default:
							model.addObject("rol","");
							break;
					}
					
					return model;
				}
	
				
	public List<Map<String, Object>> getAktifPasifList(Integer tip) {
		String sql1 = "call spGetAktifPasifList (?)";
		List<Map<String, Object>>  rows= jdbcro.queryForList(sql1,new Object[] {tip});
		return rows;
	}		
	

	/*****
	 * tanımlar kısmını tek procedure ve yordamla güncellemek için yapıldı
	 * fldcount update edilecek alan sayısı
	 * tbl update olacak tablo adı
	 * cols update edilecek field isimleri
	 * vals update olacak field lere karsılık gelen value değerleri
	 * id update edilecek id
	 */
	public vJSONRESULT tnmUpdate(Integer fldCount,String tbl,List<String> cols,List<String> vals,Integer Id,String ayrac) {
		String col="";
		
		for (int i = 0; i < cols.size(); i++) {
			if(i==0){
				col +=cols.get(i);
			}else{
				col +=ayrac+cols.get(i);	
			}		
		}
		String val="";
		for (int i = 0; i < vals.size(); i++) {
			if(i==0){
				val +=vals.get(i);
			}else{
				val +=ayrac+vals.get(i);	
			}	
		}
	/*	col=col.substring(0,col.length()-1);
		val=val.substring(0,val.length()-1);
		*/
		vJSONRESULT res =new vJSONRESULT();
		String sql1 = "call tnmUpdate (?,?,?,?,?,?)";
		 System.out.println("call tnmUpdate ("+fldCount+",'"+tbl+"','"+col+"','"+val+"',"+Id+","+ayrac+")");
		try{
			jdbcro.update(sql1,new Object[] {fldCount,tbl,col,val,Id,ayrac});
			res.setSonuc("1");
		}catch(Exception e){
			res.setSonuc("0"); 
			res.setMesaj("Tanım Güncellemesi Sırasında Hata.");
		}
		
		return res;
	}

	public vJSONRESULT tnmInsert(Integer fldCount,String tbl,List<String> cols,List<String> vals,String ayrac) {
		String col="";
		
		for (int i = 0; i < cols.size(); i++) {
			if(i==0){
				col +=cols.get(i);
			}else{
				col +=ayrac+cols.get(i);	
			}		
		}
		String val="";
		for (int i = 0; i < vals.size(); i++) {
			if(i==0){
				val +=vals.get(i);
			}else{
				val +=ayrac+vals.get(i);	
			}	
		}
	/*	col=col.substring(0,col.length()-1);
		val=val.substring(0,val.length()-1);
		*/
		vJSONRESULT res =new vJSONRESULT();
		String sql1 = "call tnmInsert (?,?,?,?,?)";
		 System.out.println("call tnmInsert ("+fldCount+",'"+tbl+"','"+col+"','"+val+"','"+ayrac+"')");
		try{
			jdbcro.update(sql1,new Object[] {fldCount,tbl,col,val,ayrac});
			res.setSonuc("1");
		}catch(Exception e){
			res.setSonuc("0"); 
			res.setMesaj(e.getMessage().toString());
		}
		
		return res;
	}



	public List<Map<String,Object>> getRolLowerLevel(Integer RolId) {
		String sql1 = "call spGetRolLowerLevel (?)";
		List<Map<String, Object>>  rows = jdbcro.queryForList(sql1,new Object[] { RolId });
		return  rows;
	}
	

}

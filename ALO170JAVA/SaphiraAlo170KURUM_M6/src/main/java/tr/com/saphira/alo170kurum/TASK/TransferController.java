package tr.com.saphira.alo170kurum.TASK;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import tr.com.saphira.alo170kurum.BILDIRIM.vADET;

@Controller
@EnableWebSecurity
@RequestMapping(value="/task")
public class TransferController {
	
	@Autowired
	TransferService service;
	
	@ResponseBody
	@GetMapping(("/transfer"))
	public TRANSFER taskDB() {
		
		List<vADET> v = service.getUpdateList();
		
		for(vADET row : v) {
				
		TRANSFER t = new TRANSFER();
		
		t = service.getTASKDB(row.getId());
		
		service.setBILDIRIM(t);
		
		
		}
		
		return null;
	}
	
	@ResponseBody
	@GetMapping(("/transferVatandas"))
	public KART vatandasDB() {
		
		List<vADET> v = service.getUpdateKartList();
		
		for(vADET row : v) {
			System.out.println(row.getId());
			KART k = new KART();
			
			k = service.getVATANDAS(row.getId());
			
			service.setVATANDAS(k);
		
		}
		
		return null;
	}
	
	@ResponseBody
	@GetMapping(("/transferTest/{id}"))
	public TRANSFER tDB(@PathVariable("id") int id) {
		
		TRANSFER t = new TRANSFER();
		
		t = service.getTASKDB(id);
		
		service.setBILDIRIM(t);
		
		return null;
	}
	
	@RequestMapping(value="/transferHome", method= RequestMethod.GET)
	public ModelAndView transferhome(HttpSession httpSession) {
		ModelAndView model = new ModelAndView("transferHome");
		
		
		return model;
	}
	
	@ResponseBody
	@GetMapping(("/transferHomeB"))
	public vADET adetBildirim() {
		
		return service.adetBildirim();
	}
	
	@ResponseBody
	@GetMapping(("/transferHomeBD"))
	public vADET adetBildirimDetay() {
		
		return service.adetBildirimDetay();
	}
	
	@ResponseBody
	@GetMapping(("/transferHomeV"))
	public vADET adetVatandas() {
		
		return service.adetVatandas();
	}
	


}

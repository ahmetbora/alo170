package tr.com.saphira.alo170kurum.RAPOR;

public class rptKurumKapanmaIstatistik {
    private Integer id;
    private String kurum;
    private String anlik_kapatilan;
    private String kuruma_yonlendirilen;
 




    public rptKurumKapanmaIstatistik() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getkurum(){
        return kurum;
    }
    public void setkurum(String kurum){
        this.kurum=kurum;
    }

    public String getanlik_kapatilan(){
        return anlik_kapatilan;
    }
    public void setanlik_kapatilan(String anlik_kapatilan){
        this.anlik_kapatilan=anlik_kapatilan;
    }

    public String getkuruma_yonlendirilen(){
        return kuruma_yonlendirilen;
    }
    public void setkuruma_yonlendirilen(String kuruma_yonlendirilen){
        this.kuruma_yonlendirilen=kuruma_yonlendirilen;
    }

}


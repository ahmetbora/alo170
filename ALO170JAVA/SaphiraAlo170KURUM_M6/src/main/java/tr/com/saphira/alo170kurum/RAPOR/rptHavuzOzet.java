package tr.com.saphira.alo170kurum.RAPOR;

public class rptHavuzOzet {
    private Integer id;
    private String havuz;
    private String toplam;
    private String ikigun;
    private String ongun;
    private String biray;
    private String ikiay;

    public rptHavuzOzet() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String getHavuz(){
        return havuz;
    }
    public void setHavuz(String havuz){
        this.havuz=havuz;
    }

    public String gettoplam(){
        return toplam;
    }
    public void settoplam(String toplam){
        this.toplam=toplam;
    }

    public String getikigunz(){
        return ikigun;
    }
    public void setikigun(String ikigun){
        this.ikigun=ikigun;
    }

    public String getongun(){
        return ongun;
    }
    public void setongun(String ongun){
        this.ongun=ongun;
    }

    public String getbiray(){
        return biray;
    }
    public void setbiray(String biray){
        this.biray=biray;
    }

    public String getikiay(){
        return ikiay;
    }
    public void setikiay(String ikiay){
        this.ikiay=ikiay;
    }

}


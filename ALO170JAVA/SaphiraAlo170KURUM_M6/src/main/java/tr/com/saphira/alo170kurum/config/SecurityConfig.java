package tr.com.saphira.alo170kurum.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import tr.com.saphira.alo170kurum.LOGIN.UserDetailsServiceImp;


@Configuration
@EnableWebMvc
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	

	
		@Bean
	  public UserDetailsService userDetailsService() {
	    return new UserDetailsServiceImp();
	  };
	  
	  @Bean
	  public BCryptPasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	  };
	
	
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth ) throws Exception {
	    auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
	}	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
 
		http.authorizeRequests()
			.antMatchers("/home/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/ayarlar/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/kurum/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/bildirim/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/pbx/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/ivr/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/settings/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/rapor/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/modul/**").access("hasRole('ROLE_ADMIN')")
			.and()
				.formLogin().loginPage("/login").failureUrl("/login?error")
					.usernameParameter("email").passwordParameter("password")
			.and()
			.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login");

		 
	}
	
}

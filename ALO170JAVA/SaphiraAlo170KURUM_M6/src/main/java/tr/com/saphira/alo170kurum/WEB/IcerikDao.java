package tr.com.saphira.alo170kurum.WEB;

import java.util.List;

public interface IcerikDao {

	public vICERIK Find(Integer ID);
	public List<vICERIK> getIcerik(Integer ID);
	public void setIcerik(Integer ID, String BASLIK, String KACIKLAMA, String ICERIK, String RESIM, String URL);
}

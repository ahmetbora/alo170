package tr.com.saphira.alo170kurum.RAPOR;

public class rptSgkBilgilendirme {
    private Integer id;
    private String tarih;
    private String adi;
    private String tc;
    private String tel;
    private String email;

    public rptSgkBilgilendirme() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String getadi(){
        return adi;
    }
    public void setadi(String adi){
        this.adi=adi;
    }

    public String gettc(){
        return tc;
    }
    public void settc(String tc){
        this.tc=tc;
    }

    public String gettel(){
        return tel;
    }
    public void settel(String tel){
        this.tel=tel;
    }

    public String getemail(){
        return email;
    }
    public void setemail(String email){
        this.email=email;
    }

}


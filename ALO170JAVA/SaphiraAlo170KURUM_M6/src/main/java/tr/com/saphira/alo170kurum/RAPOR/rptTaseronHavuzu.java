package tr.com.saphira.alo170kurum.RAPOR;

public class rptTaseronHavuzu {
    private Integer id;
    private String tarih;
    private String aramakanali;
    private String bildirim_no;
    private String vatandas;
    private String tc;
    private String aciklama;



    public rptTaseronHavuzu() {
		super();
	}

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

    public String gettarih(){
        return tarih;
    }
    public void settarih(String tarih){
        this.tarih=tarih;
    }

    public String getaramakanali(){
        return aramakanali;
    }
    public void setaramakanali(String aramakanali){
        this.aramakanali=aramakanali;
    }

    public String getbildirim_no(){
        return bildirim_no;
    }
    public void setbildirim_no(String bildirim_no){
        this.bildirim_no=bildirim_no;
    }

    public String getvatandas(){
        return vatandas;
    }
    public void setvatandas(String vatandas){
        this.vatandas=vatandas;
    }

    public String gettc(){
        return tc;
    }
    public void settc(String tc){
        this.tc=tc;
    }

    public String getaciklama(){
        return aciklama;
    }
    public void setaciklama(String aciklama){
        this.aciklama=aciklama;
    }

}


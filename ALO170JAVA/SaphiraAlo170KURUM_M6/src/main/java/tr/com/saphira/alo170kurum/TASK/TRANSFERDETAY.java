package tr.com.saphira.alo170kurum.TASK;

import tr.com.saphira.alo170kurum.Genel;

public class TRANSFERDETAY {
	
	private int id;
	private int fk_gorev;
	private int fk_agent;
	private String tarih;
	private String aciklama;
	private int fk_detay_tip;
	private int sistem;
	private String ipadres;
	// private String cryp_aciklama;
	public TRANSFERDETAY() {
		super();
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFk_gorev() {
		return fk_gorev;
	}
	public void setFk_gorev(int fk_gorev) {
		this.fk_gorev = fk_gorev;
	}
	public int getFk_agent() {
		return fk_agent;
	}
	public void setFk_agent(int fk_agent) {
		this.fk_agent = fk_agent;
	}
	public String getTarih() {
		return tarih;
	}
	public void setTarih(String tarih) {
		this.tarih = tarih;
	}
	public String getAciklama() {
		return aciklama;
	}
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	public int getFk_detay_tip() {
		return fk_detay_tip;
	}
	public void setFk_detay_tip(int fk_detay_tip) {
		this.fk_detay_tip = fk_detay_tip;
	}
	public int getSistem() {
		return sistem;
	}
	public void setSistem(int sistem) {
		this.sistem = sistem;
	}
	public String getIpadres() {
		return ipadres;
	}
	public void setIpadres(String ipadres) {
		this.ipadres = ipadres;
	}
	public String getCryp_aciklama() {
		return Genel.encrypt(getAciklama());
	}
	public void setCryp_aciklama(String cryp_aciklama) {
		// this.cryp_aciklama = cryp_aciklama;
	}
	
	

}

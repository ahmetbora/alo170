package tr.com.saphira.alo170kurum.WEB;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IcerikServiceImpl implements IcerikService {

	@Autowired
	IcerikDao Dao;

	public void setDao(IcerikDao dao) {
		Dao = dao;
	}

	@Override
	public List<vICERIK> getIcerik(Integer ID) {
		
		return Dao.getIcerik(ID);
	}

	@Override
	public vICERIK Find(Integer ID) {
		
		return Dao.Find(ID);
	}

	@Override
	public void setIcerik(Integer ID, String BASLIK, String KACIKLAMA, String ICERIK, String RESIM, String URL) {
		this.Dao.setIcerik(ID, BASLIK, KACIKLAMA, ICERIK, RESIM, URL);
		
	}
	
	
}

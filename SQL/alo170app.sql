/*
 Navicat Premium Data Transfer

 Source Server         : mysql8
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : alo170app

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 29/07/2021 03:23:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_turkish_ci NULL DEFAULT NULL,
  `address` varbinary(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_turkish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Procedure structure for adetUzerimdekiBildirimler
-- ----------------------------
DROP PROCEDURE IF EXISTS `adetUzerimdekiBildirimler`;
delimiter ;;
CREATE PROCEDURE `adetUzerimdekiBildirimler`(IN `_AGENT` INT)
  NO SQL 
select count(*) id from SMDR_1002.task_Open  where  fk_agent = _AGENT
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for appUserList
-- ----------------------------
DROP PROCEDURE IF EXISTS `appUserList`;
delimiter ;;
CREATE PROCEDURE `appUserList`(IN `_ADI` VARCHAR(100), IN `_EPOSTA` VARCHAR(100), IN `_ROL` TEXT,IN _ISACTIVE INT,IN fk_lokasyon INT,IN _SKIP INT,IN _TAKE INT)
BEGIN
SET @q = "SELECT ifnull(r.adi,'') rol, 
u.id, 
ifnull(u.adi,'') adi, 
ifnull(u.username,'') luser, 
ifnull(u.email,'') email, 
ifnull(u.isActive,0) isActive,
if(u.isActive<1,'Pasif','Aktif') as durum, 
ifnull(u.username,'') kullaniciAdi,
u.password pass,
u.ext,
u.tckimlik
FROM SMDR_1002.saphira_users  u
left join SMDR_1002.crm_rol r on r.id = u.fk_rol 
where 1=1 ";
 if (_ADI <> '') THEN
  SET @q = concat(@q, " and u.adi LIKE '", _ADI ,"%'" );
end IF;
 if (_EPOSTA <> '') THEN
  SET @q = concat(@q, " and u.email = '", _EPOSTA ,"'" );
end IF;
if (_ROL <> '' AND _ROL<>'0') THEN
  SET @q = concat(@q, " and u.fk_rol IN(", _ROL ,")" );
end IF;
if (fk_lokasyon> 0) THEN
  SET @q = concat(@q, " and u.fk_lokasyon = ", fk_lokasyon);
end IF;
if (_ISACTIVE < 2) THEN
SET @q = concat(@q, " and u.isActive = ", _ISACTIVE);
END IF;
SET @q=concat(@q," order by u.id asc");
SET @q=concat(@q," limit ",_skip,",",_take);

PREPARE stmt FROM @q;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for appUserListCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `appUserListCount`;
delimiter ;;
CREATE PROCEDURE `appUserListCount`(IN `_ADI` VARCHAR(100), IN `_EPOSTA` VARCHAR(100), IN `_ROL` TEXT,IN _ISACTIVE INT,IN fk_lokasyon INT)
BEGIN

SET @q = "SELECT COUNT(*) total 
FROM SMDR_1002.saphira_users  u
left join SMDR_1002.crm_rol r on r.id = u.fk_rol 
where 1=1 ";
 if (_ADI <> '') THEN
  SET @q = concat(@q, " and u.adi LIKE '", _ADI ,"%'" );
end IF;
 if (_EPOSTA <> '') THEN
  SET @q = concat(@q, " and u.email = '", _EPOSTA ,"'" );
end IF;
if (_ROL <> '' AND _ROL<>'0') THEN
  SET @q = concat(@q, " and u.fk_rol IN(", _ROL ,")" );
end IF;
if (fk_lokasyon> 0) THEN
  SET @q = concat(@q, " and u.fk_lokasyon = ", fk_lokasyon);
end IF;
if (_ISACTIVE < 2) THEN
SET @q = concat(@q, " and u.isActive = ", _ISACTIVE);
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for bildirimKaydet
-- ----------------------------
DROP PROCEDURE IF EXISTS `bildirimKaydet`;
delimiter ;;
CREATE PROCEDURE `bildirimKaydet`(IN _aciklama TEXT, IN _tip_level1 INT, IN _tip_level2 INT, IN _tip_level3 INT, IN _arayan_numara TEXT, IN _fk_aramakanali INT, IN _fk_aramasebebi INT,
IN _fk_kart INT, IN _uid TEXT, IN _fk_proje INT, IN _fk_sonuckod INT, IN _fk_lokasyon INT, IN _isOnayBekle INT, IN _fk_kaydi_acan INT, IN _referans TEXT, IN _fk_gorev_statu INT,
IN _fk_kapatan_agent INT,
IN _adres TEXT, IN _adi TEXT, IN _soyadi TEXT, IN _tckimlik TEXT, IN _tel1 TEXT, IN _gsm TEXT,
IN _fk_egitimdurumu INT, IN _fk_ulke INT, IN _fk_il INT, IN _fk_ilce INT, IN _sskno  TEXT, 
IN _email TEXT, IN _fk_cinsiyet INT, IN _fax TEXT, IN _fk_gsoru INT, IN _gsoru_cevap TEXT,
IN _f_adi TEXT, IN _f_adres TEXT, IN _f_tel TEXT, IN _f_fk_il INT, IN _f_fk_ilce INT, IN _nace TEXT, IN _scs TEXT,
IN _kart_update INT,IN _firma_bilgi INT)
  NO SQL 
BEGIN
DECLARE kartID INT;
DECLARE taskID INT;
DECLARE taskDetayID INT;
DECLARE firmaID INT;

######################################
#### yeni kart oluşturup görev ve firma bilgisi kaydet
######################################
IF _fk_kart=0 THEN
	#
	# 	KARTDB SQL
	#
	SET NAMES UTF8;
	SET @sqlKart="INSERT INTO SMDR_1002.crm_KartDB SET ";
	SET @sqlKart=CONCAT(@sqlKart,"adres='",_adres,"',adi='",_adi,"',soyadi='",_soyadi,"',tckimlik='",_tckimlik,"',tel1='",_tel1,"',gsm='",_gsm,"',fk_egitimdurumu='",_fk_egitimdurumu,"',fk_ulke='",
	_fk_ulke,"',fk_il='",_fk_il,"',fk_ilce='",_fk_ilce,"',sskno='",_sskno,"',email='",_email,"',fk_cinsiyet='",_fk_cinsiyet,"',fax='",_fax,"',fk_gsoru='",_fk_gsoru,"',gsoru_cevap='",_gsoru_cevap,"'");

	#select @sqlKart;
	PREPARE stmt FROM @sqlKart;
	EXECUTE stmt;
	SET kartID= LAST_INSERT_ID();    
    
END IF;


######################################
#### mevcut olan bir kart bilgisi var ise ve kart update isteniyorsa kart bilgisini guncelle
######################################
IF _fk_kart>0 THEN
	IF _kart_update=1 THEN
		#
		# 	KARTDB SQL
		#
		SET NAMES UTF8;
		SET @sqlKartUpdate="UPDATE SMDR_1002.crm_KartDB SET ";
		SET @sqlKartUpdate=CONCAT(@sqlKartUpdate,"adres='",_adres,"',adi='",_adi,"',soyadi='",_soyadi,"',tckimlik='",_tckimlik,"',tel1='",_tel1,"',gsm='",_gsm,"',fk_egitimdurumu='",_fk_egitimdurumu,"',fk_ulke='",
		_fk_ulke,"',fk_il='",_fk_il,"',fk_ilce='",_fk_ilce,"',sskno='",_sskno,"',email='",_email,"',fk_cinsiyet='",_fk_cinsiyet,"',fax='",_fax,"',fk_gsoru='",_fk_gsoru,"',gsoru_cevap='",_gsoru_cevap,"'");
		SET @sqlKartUpdate=CONCAT(@sqlKartUpdate," where id=",_fk_kart);
		#select @sqlKart;
		PREPARE stmt FROM @sqlKartUpdate;
		EXECUTE stmt;
		   
    END IF;
    SET kartID= _fk_kart; 
END IF;

#
# 	TASKDB SQL
#

SET NAMES UTF8;
SET @sqlTask="INSERT INTO SMDR_1002.task_DB SET ";
SET @sqlTask=CONCAT(@sqlTask," aciklama='",_aciklama,"',tip_level1='",_tip_level1,"',tip_level2='",_tip_level2,"',tip_level3='",_tip_level3,"',arayan_numara='",_arayan_numara,"',fk_aramakanali='",
_fk_aramakanali,"',fk_aramasebebi='",_fk_aramasebebi,"',fk_kart='",kartID,"',callid='",_uid,"',fk_proje='",_fk_proje,"',fk_sonuckod='",_fk_sonuckod,"',fk_lokasyon='",_fk_lokasyon,"',isOnayBekle='",
_isOnayBekle,"',fk_kaydi_acan='",_fk_kaydi_acan,"',referans='",_referans,"',fk_gorev_statu='",_fk_gorev_statu,"',yeniden_acilma_tar='",CURRENT_TIMESTAMP(),"'"
);                
#select @sqlTask;
PREPARE stmtTask FROM @sqlTask;
EXECUTE stmtTask;
SET taskID= LAST_INSERT_ID();

#
# 	TASKDBDETAY SQL
#

SET NAMES UTF8;
SET @sqlTaskDetay="INSERT INTO SMDR_1002.task_DBDetay SET ";
SET @sqlTaskDetay=CONCAT(@sqlTaskDetay," fk_gorev='",taskID,"',fk_agent='",_fk_kaydi_acan,"',aciklama='Bildirimi Oluşturdu [Sistem]'");
PREPARE stmtTaskDetay FROM @sqlTaskDetay;
EXECUTE stmtTaskDetay;
SET taskDetayID= LAST_INSERT_ID();

#######################################
# firma bilgisi girişi var ise bilgileri kaydet
#######################################
IF _firma_bilgi=1 THEN
	#
	#	FIRMA BILGILERI
	#
	SET NAMES UTF8;
	SET @sqlFirma="INSERT INTO SMDR_1002.crm_kart_firma SET ";         
	SET @sqlFirma=CONCAT(@sqlFirma," f_adi='",_f_adi,"',f_adres='",_f_adres,"',f_tel='",_f_tel,"',f_fk_il='",_f_fk_il,"',f_fk_ilce='",_f_fk_ilce,"',nace='",_nace,"',scs='",_scs);
	SET @sqlFirma=CONCAT(@sqlFirma, "',fk_kart='" , kartID , "',fk_gorev='" , taskID , "',fk_kaydi_yapan='", _fk_kaydi_acan , "'"); 
	 #select @sqlFirma;
	PREPARE stmtFirma FROM @sqlFirma;
	EXECUTE stmtFirma;
	SET firmaID= LAST_INSERT_ID();     
END IF;

select taskID as gorevid;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbAltKurumlar
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbAltKurumlar`;
delimiter ;;
CREATE PROCEDURE `cmbAltKurumlar`(IN `_ID` INT)
  NO SQL 
BEGIN

if(_ID > 0) THEN
SELECT 
b.id,
ifnull(b.adi,'') adi
,ifnull(b.email,'') email
,b.isActive, tk.adi kurumadi
,b.fk_parent

from SMDR_1002.task_alt_konu b
left join SMDR_1002.task_konu tk on tk.id = b.fk_parent

where b.fk_parent = _ID

order by b.id ;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbAramaKanali
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbAramaKanali`;
delimiter ;;
CREATE PROCEDURE `cmbAramaKanali`(IN `_ID` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN
	SELECT id,
ifnull(b.adi,'') adi
,isActive

from SMDR_1002.bild_tnm_aramakanali b

order by id;
END IF;

if(_ID > 0) THEN
SELECT 
id,
 ifnull(b.adi,'') adi,
isActive

from SMDR_1002.bild_tnm_aramakanali b

where b.id = _ID

order by id limit 1;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbAramaKanaliDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbAramaKanaliDetay`;
delimiter ;;
CREATE PROCEDURE `cmbAramaKanaliDetay`(IN `_fk_parent` INT)
  NO SQL 
select id,adi,fk_parent,isActive,ex_islem,fk_aramatipi from SMDR_1002.bild_tnm_aramakanali
where fk_parent_int = _fk_parent
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbAramaKanaliTipi
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbAramaKanaliTipi`;
delimiter ;;
CREATE PROCEDURE `cmbAramaKanaliTipi`()
  NO SQL 
select * from SMDR_1002.bild_tnm_aramakanali_tipi order by adi
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbAramaSebebi
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbAramaSebebi`;
delimiter ;;
CREATE PROCEDURE `cmbAramaSebebi`(IN `_ID` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN
	SELECT id,
ifnull(b.adi,'') adi

from SMDR_1002.bild_tnm_aramasebebi b

order by id;
END IF;

if(_ID > 0) THEN
SELECT 
id,
 ifnull(b.adi,'') adi

from SMDR_1002.bild_tnm_aramasebebi b

where b.id = _ID

order by id limit 1;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbBilgiBankasiKonular
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbBilgiBankasiKonular`;
delimiter ;;
CREATE PROCEDURE `cmbBilgiBankasiKonular`()
BEGIN
	#Routine body goes here...

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbCinsiyet
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbCinsiyet`;
delimiter ;;
CREATE PROCEDURE `cmbCinsiyet`()
  NO SQL 
BEGIN
SELECT id,
ifnull(b.adi,'') adi
from SMDR_1002.bild_tnm_cinsiyet b
order by id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbEgitimDurumu
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbEgitimDurumu`;
delimiter ;;
CREATE PROCEDURE `cmbEgitimDurumu`(IN `_ID` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN
	SELECT id,
ifnull(b.adi,'') adi
,isActive

from SMDR_1002.bild_tnm_egitim_durumu b

order by adi;
END IF;

if(_ID > 0) THEN
SELECT 
id,
 ifnull(b.adi,'') adi,
isActive

from SMDR_1002.bild_tnm_egitim_durumu b

where b.id = _ID

order by id limit 1;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbGeriDonusKanali
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbGeriDonusKanali`;
delimiter ;;
CREATE PROCEDURE `cmbGeriDonusKanali`()
  NO SQL 
BEGIN
	SELECT id,
ifnull(b.adi,'') adi
from SMDR_1002.task_geridonuskanali b
order by id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbGuvenlikSorusu
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbGuvenlikSorusu`;
delimiter ;;
CREATE PROCEDURE `cmbGuvenlikSorusu`(IN `_ID` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN
	SELECT id,
ifnull(b.adi,'') adi
,isActive

from SMDR_1002.bild_tnm_guvenliksoru b

order by id;
END IF;

if(_ID > 0) THEN
SELECT 
id,
 ifnull(b.adi,'') adi,
isActive

from SMDR_1002.bild_tnm_guvenliksoru b

where b.id = _ID

order by id limit 1;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbHavuzlar
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbHavuzlar`;
delimiter ;;
CREATE PROCEDURE `cmbHavuzlar`(IN `_AGENT` INT)
  NO SQL 
select id,adi from SMDR_1002.task_havuzlar
where id in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = _AGENT) order by adi
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbHavuzlarSube
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbHavuzlarSube`;
delimiter ;;
CREATE PROCEDURE `cmbHavuzlarSube`(IN `_SUBE` INT)
  NO SQL 
BEGIN
SET NAMES UTF8;
	SET @qry="select * from SMDR_1002.task_havuzlar 
	where 1=1 
	";
	SET @qry=CONCAT(@qry," and fk_sube=",_SUBE);
	SET @qry=CONCAT(@qry," AND isActive=1");
	SET @qry=CONCAT(@qry," order by id asc");
  --  select @qry;
PREPARE stmt FROM @qry;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbIlceler
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbIlceler`;
delimiter ;;
CREATE PROCEDURE `cmbIlceler`(IN `_IL` INT)
  NO SQL 
select id,adi,fk_il,isActive from SMDR_1002.bild_tnm_ilce
where fk_il = _IL
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbIlcelerById
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbIlcelerById`;
delimiter ;;
CREATE PROCEDURE `cmbIlcelerById`(IN `_ID` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN
	SELECT id,
ifnull(b.adi,'') adi
,isActive,fk_il

from SMDR_1002.bild_tnm_ilce b

order by adi;
END IF;

if(_ID > 0) THEN
SELECT 
id,
 ifnull(b.adi,'') adi,
isActive,fk_il

from SMDR_1002.bild_tnm_ilce b

where b.id = _ID

order by adi limit 1;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbIller
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbIller`;
delimiter ;;
CREATE PROCEDURE `cmbIller`(IN `_ID` INT)
BEGIN

if(_ID = 0) THEN
	SELECT id,
ifnull(b.adi,'') adi
,isActive,fk_ulke

from SMDR_1002.bild_tnm_il b

order by adi;
END IF;

if(_ID > 0) THEN
SELECT 
id,
 ifnull(b.adi,'') adi,
isActive,fk_ulke

from SMDR_1002.bild_tnm_il b

where b.id = _ID

order by adi limit 1;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbIllerByUlke
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbIllerByUlke`;
delimiter ;;
CREATE PROCEDURE `cmbIllerByUlke`(IN `_ID` INT, IN `_FKULKE` INT, IN `_ADI` VARCHAR(150), IN `_IDS` VARCHAR(21844))
BEGIN

SET @q="SELECT id, ifnull(b.adi,'') adi, isActive, fk_ulke
from SMDR_1002.bild_tnm_il b 
where 1=1 ";
if(_ID > 0) THEN
	SET @q=CONCAT(@q," and b.id=",_ID);
END IF;
if(_FKULKE > 0) THEN
	SET @q=CONCAT(@q," and b.fk_ulke=",_FKULKE);
END IF;
if(LENGTH(_ADI) > 2) THEN
	SET @q=CONCAT(@q," and b.adi like '",_ADI," %'");
END IF;
if(LENGTH(_IDS) > 2) THEN
	SET @q=CONCAT(@q," and b.id in (",_IDS,")");
END IF;
SET @q=CONCAT(@q," order by adi ");

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbRoller
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbRoller`;
delimiter ;;
CREATE PROCEDURE `cmbRoller`()
  NO SQL 
select id, adi,level from SMDR_1002.crm_rol order by adi
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbSektor
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbSektor`;
delimiter ;;
CREATE PROCEDURE `cmbSektor`()
  NO SQL 
BEGIN
SELECT id,
ifnull(b.adi,'') adi
from SMDR_1002.crm_sektor b
order by id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbSETAramaKanali
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbSETAramaKanali`;
delimiter ;;
CREATE PROCEDURE `cmbSETAramaKanali`(IN `_ID` INT, IN `_ADI` VARCHAR(80), IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.bild_tnm_aramakanali 
SET adi = _ADI , isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.bild_tnm_aramakanali
SET adi = _ADI , isActive = _ISACTIVE
where id = _ID 

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbSETEgitimDurumu
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbSETEgitimDurumu`;
delimiter ;;
CREATE PROCEDURE `cmbSETEgitimDurumu`(IN `_ID` INT, IN `_ADI` VARCHAR(80), IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.bild_tnm_egitim_durumu 
SET adi = _ADI , isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.bild_tnm_egitim_durumu 
SET adi = _ADI , isActive = _ISACTIVE
where id = _ID 

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbSETGuvenlikSorusu
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbSETGuvenlikSorusu`;
delimiter ;;
CREATE PROCEDURE `cmbSETGuvenlikSorusu`(IN `_ID` INT, IN `_ADI` VARCHAR(80), IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.bild_tnm_guvenliksoru 
SET adi = _ADI , isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.bild_tnm_guvenliksoru
SET adi = _ADI , isActive = _ISACTIVE
where id = _ID 

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbSETIlceler
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbSETIlceler`;
delimiter ;;
CREATE PROCEDURE `cmbSETIlceler`(IN `_ID` INT, IN `_ADI` VARCHAR(50), IN `_IL` INT, IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.bild_tnm_ilce
SET adi = _ADI , fk_il = _IL, isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.bild_tnm_ilce
SET adi = _ADI , fk_il = _IL, isActive = _ISACTIVE
where id = _ID

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbSETIller
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbSETIller`;
delimiter ;;
CREATE PROCEDURE `cmbSETIller`(IN `_ID` INT, IN `_ADI` VARCHAR(50), IN `_ULKE` INT, IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.bild_tnm_il
SET adi = _ADI , fk_ulke = _ULKE, isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.bild_tnm_il
SET adi = _ADI , fk_ulke = _ULKE, isActive = _ISACTIVE
where id = _ID

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbSETUlkeler
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbSETUlkeler`;
delimiter ;;
CREATE PROCEDURE `cmbSETUlkeler`(IN `_ID` INT, IN `_ADI` VARCHAR(50), IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.bild_tnm_ulke
SET adi = _ADI , isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.bild_tnm_ulke
SET adi = _ADI , isActive = _ISACTIVE
where id = _ID

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbTaskStatu
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbTaskStatu`;
delimiter ;;
CREATE PROCEDURE `cmbTaskStatu`(IN _id INT)
  NO SQL 
BEGIN
	SET @sql="SELECT id,adi from SMDR_1002.task_GorevStatu ";
	IF(_id>0) THEN
		SET @sql=CONCAT(@sql," WHERE id=",_id);
	END IF;
	SET @sql=CONCAT(@sql,"  order by adi");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for cmbUlkeler
-- ----------------------------
DROP PROCEDURE IF EXISTS `cmbUlkeler`;
delimiter ;;
CREATE PROCEDURE `cmbUlkeler`(IN `_ID` INT)
BEGIN

if(_ID = 0) THEN
	SELECT id,
ifnull(b.adi,'') adi
,isActive

from SMDR_1002.bild_tnm_ulke b

order by adi;
END IF;

if(_ID > 0) THEN
SELECT 
id,
 ifnull(b.adi,'') adi,
isActive

from SMDR_1002.bild_tnm_ulke b

where b.id = _ID

order by adi limit 1;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for dashboardHavuzOzet
-- ----------------------------
DROP PROCEDURE IF EXISTS `dashboardHavuzOzet`;
delimiter ;;
CREATE PROCEDURE `dashboardHavuzOzet`()
  NO SQL 
select t.fk_havuz, h.adi, count(*) from SMDR_1002.Bildirim t join SMDR_1002.task_havuzlar h on h.id =t.fk_havuz where fk_gorev_statu < 99 and fk_part < 10 group by fk_havuz, h.adi order by 3 desc
limit 20
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for DATEDIFF_WEEKDAYS
-- ----------------------------
DROP PROCEDURE IF EXISTS `DATEDIFF_WEEKDAYS`;
delimiter ;;
CREATE PROCEDURE `DATEDIFF_WEEKDAYS`(IN `IN_from_date` DATE, IN `IN_to_date` DATE)
BEGIN
DECLARE TOTAL_WEEKDAYS INT DEFAULT 0;
DECLARE TOTAL_DAYS INT DEFAULT 0;
DECLARE FROM_DTE DATE DEFAULT NOW();
DECLARE TO_DTE DATE DEFAULT NOW();
DECLARE CURR_DTE DATE DEFAULT NOW();
IF(IN_from_date = IN_to_date) THEN
    SELECT 0;
END IF;
/*
    Check if FROM_DATE provided is later than 
*/
IF(TO_DAYS(IN_from_date) > TO_DAYS(IN_to_date)) THEN
    SET FROM_DTE = IN_to_date;
    SET TO_DTE = IN_from_date;
    SET CURR_DTE = FROM_DTE;
ELSE
    SET FROM_DTE = IN_from_date;
    SET TO_DTE = IN_to_date;
    SET CURR_DTE = FROM_DTE;
END IF;
SELECT FROM_DTE, TO_DTE, CURR_DTE;
/* Loop through days between two dates, up to and including TO_DTE: */
WHILE(TO_DAYS(CURR_DTE) <= TO_DAYS(TO_DTE)) DO
    /* INCREMENT TOTAL_WEEKDAYS ONLY WHEN DAYOFWEEK IS 2 - 6. DAYS 1 and 7 are Sunday and Saturday (resp.) */
    IF(DAYOFWEEK(CURR_DTE) <> 1 AND DAYOFWEEK(CURR_DTE) <> 7) THEN
        SET TOTAL_WEEKDAYS = TOTAL_WEEKDAYS + 1;
    END IF;
    SET TOTAL_DAYS = TOTAL_DAYS + 1;
    /* Update tracking date */
    SET CURR_DTE = DATE_ADD(CURR_DTE, INTERVAL 1 DAY);
END WHILE;
SELECT TOTAL_DAYS, TOTAL_WEEKDAYS;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getSTATS
-- ----------------------------
DROP PROCEDURE IF EXISTS `getSTATS`;
delimiter ;;
CREATE PROCEDURE `getSTATS`()
  NO SQL 
select * from SMDR_1002.Bildirim_Stats where tarih = CURRENT_DATE
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getWebIcerik
-- ----------------------------
DROP PROCEDURE IF EXISTS `getWebIcerik`;
delimiter ;;
CREATE PROCEDURE `getWebIcerik`(IN `_ID` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN
SELECT *
from SMDR_1002.web_content

order by id;

END IF;

if(_ID > 0) THEN
SELECT *
from SMDR_1002.web_content

where id = _ID

order by id ;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Function structure for getWeekDays
-- ----------------------------
DROP FUNCTION IF EXISTS `getWeekDays`;
delimiter ;;
CREATE FUNCTION `getWeekDays`(`havuza_atanma` DATETIME)
 RETURNS int(10) unsigned
  NO SQL 
BEGIN


SET @RET = DATEDIFF( current_date, havuza_atanma ) - ((FLOOR(DATEDIFF(  current_date, havuza_atanma) / 7) * 2) +
CASE WHEN dayofweek( havuza_atanma) - dayofweek(current_date) IN (1, 2, 3, 4, 5) AND dayofweek( current_date) != 0
THEN 2 ELSE 0 END +
CASE WHEN dayofweek( havuza_atanma) != 0 AND dayofweek( current_date) = 0
THEN 1 ELSE 0 END +
CASE WHEN dayofweek( havuza_atanma) = 0 AND dayofweek( current_date) != 0
THEN 1 ELSE 0 END);


RETURN @RET;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for HOME_getDBOzet
-- ----------------------------
DROP PROCEDURE IF EXISTS `HOME_getDBOzet`;
delimiter ;;
CREATE PROCEDURE `HOME_getDBOzet`()
  NO SQL 
select * from SMDR_1002.Z_DBDurumu where id=1
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for HOME_getHavuzAcilan1Aylik
-- ----------------------------
DROP PROCEDURE IF EXISTS `HOME_getHavuzAcilan1Aylik`;
delimiter ;;
CREATE PROCEDURE `HOME_getHavuzAcilan1Aylik`()
  NO SQL 
select tarih, (task_acilan - task_agent_kapanan) as yeni , task_kapanan, (task_acilan - task_agent_kapanan) - task_kapanan as fark
from SMDR_1002.Bildirim_Stats
where tarih > DATE_ADD(current_date, INTERVAL -1 MONTH)
and gun > 1 and gun < 7
order by tarih
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for HOME_getStatsByDate
-- ----------------------------
DROP PROCEDURE IF EXISTS `HOME_getStatsByDate`;
delimiter ;;
CREATE PROCEDURE `HOME_getStatsByDate`(IN `_TAR` DATE)
  NO SQL 
select * from SMDR_1002.Bildirim_Stats where tarih = _TAR limit 1
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for HOME_Havuz_2Hafta_Ozet
-- ----------------------------
DROP PROCEDURE IF EXISTS `HOME_Havuz_2Hafta_Ozet`;
delimiter ;;
CREATE PROCEDURE `HOME_Havuz_2Hafta_Ozet`()
  NO SQL 
SELECT o.fk_havuz id, h.adi, count(*) adet FROM SMDR_1002.`task_Open` o left join SMDR_1002.task_havuzlar h on h.id = o.fk_havuz WHERE fk_havuz > 0 and havuza_atanma < date_add(current_date, interval -14 DAY) and havuza_atanma > '2000-01-01' group by o.fk_havuz, h.adi order by 3 desc limit 20
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for HOME_Havuz_7Gun_Ozet
-- ----------------------------
DROP PROCEDURE IF EXISTS `HOME_Havuz_7Gun_Ozet`;
delimiter ;;
CREATE PROCEDURE `HOME_Havuz_7Gun_Ozet`()
  NO SQL 
SELECT o.fk_havuz id, h.adi, count(*) adet FROM SMDR_1002.`task_Open` o left join SMDR_1002.task_havuzlar h on h.id = o.fk_havuz WHERE fk_havuz > 0 and havuza_atanma < date_add(current_date, interval -7 DAY) and havuza_atanma > '2000-01-01' group by o.fk_havuz, h.adi order by 3 desc limit 20
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for ivrTC
-- ----------------------------
DROP PROCEDURE IF EXISTS `ivrTC`;
delimiter ;;
CREATE PROCEDURE `ivrTC`(IN `_CID` VARCHAR(100))
  NO SQL 
BEGIN
SELECT 
ifnull(b.callid,'') callid,
ifnull(b.tc,'') tc,
ifnull(b.que,'') que,
ifnull(b.menu,'') menu
from SMDR_1002.ivr_tc b 
where b.callid=_CID COLLATE utf8_turkish_ci
order by b.tarih;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for kurumAltKurumGuncelle
-- ----------------------------
DROP PROCEDURE IF EXISTS `kurumAltKurumGuncelle`;
delimiter ;;
CREATE PROCEDURE `kurumAltKurumGuncelle`(id int,t1 int,t2 int,t3 int,fk_aramasebebi int,fk_aramakanali int)
BEGIN
	#Routine body goes here...
SET NAMES UTF8;

SET @sqlDetayIslem="UPDATE SMDR_1002.task_DB SET ";
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," tip_level1 ='",t1,"'");		
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," ,tip_level2 ='",t2,"'");
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," ,tip_level3 ='",t3,"'");
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," ,fk_aramakanali ='",fk_aramakanali,"'");
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," ,fk_aramasebebi ='",fk_aramasebebi,"'");
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," where id=",id);		
PREPARE stmtDetayIslem FROM @sqlDetayIslem;
EXECUTE stmtDetayIslem;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for mobilLogin
-- ----------------------------
DROP PROCEDURE IF EXISTS `mobilLogin`;
delimiter ;;
CREATE PROCEDURE `mobilLogin`(IN `_TC` VARCHAR(50), IN `_SIFRE` VARCHAR(50))
BEGIN

select
		k.id kartid,
		concat(k.adi , ' ' , k.soyadi) kartadi,
        ifnull(k.tckimlik,'') tckimlik,
        ifnull(k.sifre_,'') sifre,
        ifnull(k.email,'') email,
        ifnull(k.gsm,'') gsm
        
		from SMDR_1002.Vatandas k

		where k.tckimlik = _TC 
        and k.sifre_ = _SIFRE
        
        order by k.id desc
        limit 1;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for mobilSETBasvuru
-- ----------------------------
DROP PROCEDURE IF EXISTS `mobilSETBasvuru`;
delimiter ;;
CREATE PROCEDURE `mobilSETBasvuru`(IN `_ID` INT, IN `_ASEBEBI` INT, IN `_KURUM` INT, IN `_AKANALI` INT, IN `_MESAJ` VARCHAR(500))
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.task_DB 
SET fk_aramasebebi = _ASEBEBI , tip_level1 = _KURUM ,
fk_aramakanali = _AKANALI , aciklama = _MESAJ ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.task_DB
SET fk_aramasebebi = _ASEBEBI , tip_level1 = _KURUM ,
fk_aramakanali = _AKANALI , aciklama = _MESAJ
where id = _ID

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for mobilSETVatandas
-- ----------------------------
DROP PROCEDURE IF EXISTS `mobilSETVatandas`;
delimiter ;;
CREATE PROCEDURE `mobilSETVatandas`(IN `_ID` INT, IN `_ADI` VARCHAR(50), IN `_SOYADI` VARCHAR(50), IN `_TEL` VARCHAR(50), IN `_EMAIL` VARCHAR(100), IN `_TC` VARCHAR(100), IN `_SIFRE` VARCHAR(50))
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.crm_KartDB 
SET adi = _ADI, soyadi = _SOYADI, gsm = _TEL, email = _EMAIL,
tckimlik = _TC , sifre_ = _SIFRE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.crm_KartDB
SET adi = _ADI, soyadi = _SOYADI, gsm = _TEL, email = _EMAIL,
tckimlik = _TC , sifre_ = _SIFRE
where id = _ID

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for ozelNumaralar
-- ----------------------------
DROP PROCEDURE IF EXISTS `ozelNumaralar`;
delimiter ;;
CREATE PROCEDURE `ozelNumaralar`(IN `_NO` VARCHAR(50))
BEGIN

if(LENGTH(_NO)<1) THEN
	SELECT id,
ifnull(b.numara,'') numara,
ifnull(b.aciklama,'') aciklama
,fk_ekleyen

from SMDR_1002.ozel_numaralar b

order by numara;
END IF;

if(LENGTH(_NO) > 0) THEN
SELECT 
id,
 ifnull(b.numara,'') numara,
 ifnull(b.aciklama,'') aciklama,
fk_ekleyen

from SMDR_1002.ozel_numaralar b

where b.numara = _NO

order by numara limit 1;

END IF;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for RaporLogla
-- ----------------------------
DROP PROCEDURE IF EXISTS `RaporLogla`;
delimiter ;;
CREATE PROCEDURE `RaporLogla`(IN rapor TEXT,
IN userId INT)
BEGIN
  SET @q = CONCAT("INSERT IGNORE INTO SMDR_1002.rapor_log set rapor='",rapor,"',fk_user='",userId,"'");
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptBildirimDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptBildirimDetay`;
delimiter ;;
CREATE PROCEDURE `rptBildirimDetay`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _tip_level1 INT,
IN _tip_level2 INT,
IN _tip_level3 INT,
IN _fkIl INT,
IN _fkIlce INT,
IN _fkHavuz INT,
IN _fk_gorev_statu INT,
IN _arama_kanali_tipi INT,
IN _fk_aramakanali INT,
IN _fk_aramasebebi INT,
IN _skip INT,
IN _take INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN
	 
	SET @q = "select
		g.*,g.id gorevid,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		concat(k1.adi,'/',k2.adi,'/',k3.adi) konusu,
		gs.adi statu,
		gs.class statu_class,
		
		if(g.fk_agent>0,su1.adi,'Atanmamış')  yapacak_agent,
		g.tarih fulldate,
		tip.adi tip,
		date_format(g.tarih,'%d.%m.%Y') gtarih,
		date_format(g.tarih,'%H:%i') gsaat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		CONCAT(kart.adi,' ',kart.soyadi) vatandas,
		kart.adi musteri,
		kart.soyadi musteris,
		if(g.isOnayBekle<>1,concat(kart.adi ,' ',kart.soyadi),'***** *****') vatandas,
		kart.tckimlik tc,
		il.adi iladi,
		ilce.adi ilceadi,
		fir.f_adi firma 
		 FROM SMDR_1002.task_DB g 
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	
		left join SMDR_1002.crm_kart_firma fir on fir.fk_kart=kart.id and fir.fk_gorev=g.id 
		where 1=1		 
		";
		
	SET @q=CONCAT(@q," and g.tarih>='",_sdate," 00:00:00' and g.tarih<='",_fdate," 23:59:59' ");
	
	IF(_fkIl>0) THEN
		SET @q=CONCAT(@q," and kart.fk_il=",_fkIl);
	END IF;
	
	IF(_fkIlce>0) THEN
		SET @q=CONCAT(@q," and kart.fk_ilce=",_fkIlce);
	END IF;	
	
	IF(_fkHavuz>0) THEN
		SET @q=CONCAT(@q," and g.fk_havuz>0");
	END IF;	
	
	IF(_fk_gorev_statu>0) THEN
		SET @q=CONCAT(@q," and g.fk_gorev_statu=",_fk_gorev_statu);
	END IF;	
	
	IF(_fk_aramakanali>0) THEN
		SET @q=CONCAT(@q," and g.fk_aramakanali=",_fk_aramakanali);
	END IF;	
	
	IF(_fk_aramasebebi>0) THEN
		SET @q=CONCAT(@q," and g.fk_aramasebebi=",_fk_aramasebebi);
	END IF;		
	IF(_tip_level1>0) THEN
		SET @q=CONCAT(@q," and g.tip_level1=",_tip_level1);
	END IF;

	IF(_tip_level2>0) THEN
		SET @q=CONCAT(@q," and g.tip_level2=",_tip_level2);
	END IF;
	
	IF(_tip_level3>0) THEN
		SET @q=CONCAT(@q," and g.tip_level3=",_tip_level3);
	END IF;
	
	IF(_yetki<>"sv") THEN
		SET @q=CONCAT(@q," and g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user =",_userId,") ");
	END IF;
	

	SET @q=CONCAT(@q," limit ",_skip,",",_take);
   -- select @q;
	 PREPARE stmt FROM @q;
	 EXECUTE stmt;
	 CALL RaporLogla("Bildirim Detay Raporu",_userId);


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptBildirimDetayCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptBildirimDetayCount`;
delimiter ;;
CREATE PROCEDURE `rptBildirimDetayCount`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _tip_level1 INT,
IN _tip_level2 INT,
IN _tip_level3 INT,
IN _fkIl INT,
IN _fkIlce INT,
IN _fkHavuz INT,
IN _fk_gorev_statu INT,
IN _arama_kanali_tipi INT,
IN _fk_aramakanali INT,
IN _fk_aramasebebi INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN
	SET @q = "select COUNT(*) total 
		 FROM SMDR_1002.task_DB g 
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	
		left join SMDR_1002.crm_kart_firma fir on fir.fk_kart=kart.id and fir.fk_gorev=g.id 
		where 1=1		 
		";
		
	SET @q=CONCAT(@q," and g.tarih>='",_sdate," 00:00:00' and g.tarih<='",_fdate," 23:59:59' ");
	
	IF(_fkIl>0) THEN
		SET @q=CONCAT(@q," and kart.fk_il=",_fkIl);
	END IF;
	
	IF(_fkIlce>0) THEN
		SET @q=CONCAT(@q," and kart.fk_ilce=",_fkIlce);
	END IF;	
	
	IF(_fkHavuz>0) THEN
		SET @q=CONCAT(@q," and g.fk_havuz>0");
	END IF;	
	
	IF(_fk_gorev_statu>0) THEN
		SET @q=CONCAT(@q," and g.fk_gorev_statu=",_fk_gorev_statu);
	END IF;	
	
	IF(_fk_aramakanali>0) THEN
		SET @q=CONCAT(@q," and g.fk_aramakanali=",_fk_aramakanali);
	END IF;	
	
	IF(_fk_aramasebebi>0) THEN
		SET @q=CONCAT(@q," and g.fk_aramasebebi=",_fk_aramasebebi);
	END IF;	
	
	IF(_tip_level1>0) THEN
		SET @q=CONCAT(@q," and g.tip_level1=",_tip_level1);
	END IF;

	IF(_tip_level2>0) THEN
		SET @q=CONCAT(@q," and g.tip_level2=",_tip_level2);
	END IF;
	
	IF(_tip_level3>0) THEN
		SET @q=CONCAT(@q," and g.tip_level3=",_tip_level3);
	END IF;
	
	IF(_yetki<>"sv") THEN
		SET @q=CONCAT(@q," and g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user =",_userId,") ");
	END IF;
   select @q;
	 PREPARE stmt FROM @q;
	 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptBirimlereGoreKullaniciSayisi
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptBirimlereGoreKullaniciSayisi`;
delimiter ;;
CREATE PROCEDURE `rptBirimlereGoreKullaniciSayisi`(IN sdate TEXT, 
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select t.id,t.tarih,ak.adi tiplevel3,h.adi havuz from SMDR_1002.task_DB t
left join SMDR_1002.task_alt_konu_detay ak on ak.id=t.tip_level3
left join SMDR_1002.task_havuzlar h on h.id=t.fk_havuz
 where t.fk_havuz>0 and t.fk_agent=0 ";
	SET @q=CONCAT(@q,"  and t.tarih>='",sdate," 00:00:00' and  t.tarih<='",fdate," 23:59:59' ");

	SET @q=CONCAT(@q," limit ",skip,",",take);
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptBirimlereGoreKullaniciSayisiCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptBirimlereGoreKullaniciSayisiCount`;
delimiter ;;
CREATE PROCEDURE `rptBirimlereGoreKullaniciSayisiCount`(IN sdate TEXT, 
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select COUNT(*) total  from SMDR_1002.task_DB t
left join SMDR_1002.task_alt_konu_detay ak on ak.id=t.tip_level3
left join SMDR_1002.task_havuzlar h on h.id=t.fk_havuz
 where t.fk_havuz>0 and t.fk_agent=0 ";
	SET @q=CONCAT(@q,"  and t.tarih>='",sdate," 00:00:00' and  t.tarih<='",fdate," 23:59:59' ");
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptHamData
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptHamData`;
delimiter ;;
CREATE PROCEDURE `rptHamData`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _skip INT,
IN _take INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN
	SET @q="";
	SET @q=CONCAT(@q,"
		select
		g.*,g.tarih gtarih,g.id gorevid,
		su.adi acan_agent,
		su.id userid,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		aase.adi altarama_sebebi,
		aase.fk_parent aseparent,
		akanal.adi arama_kanali,
		gs.adi statu,
		gs.class statu_class,
		
		su1.adi yapacak_agent,
		g.tarih fulldate,
		tip.adi tip,
		date_format(g.tarih,'%d.%m.%Y') tarih,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		kart.adi musteri,
		kart.soyadi musteris,
		kart.tckimlik tc,
		kart.tel1 tel1,
		kart.gsm gsm,
		kart.id kartid,
		kart.dt dt,
		kart.ukey dtr,
		kart.email,
		cins.adi cinsiyet,
		ed.adi egitim_durumu,
		il.adi iladi,
		ilce.adi ilceadi,
		fir.f_adi firma,
		fil.adi firmail 
		from crm_KartDB 
		left join task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join saphira_users su on su.id = g.fk_kaydi_acan
		left join saphira_users su1 on su1.id = g.fk_agent
		
		left join task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN bild_tnm_il il ON il.id = kart.fk_il
		
		LEFT JOIN bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN bild_tnm_aramakanali aase ON aase.id = g.fk_aramasebebi	 
		LEFT JOIN bild_tnm_aramakanali akanal ON akanal.id = g.fk_aramakanali
		left join bild_tnm_egitim_durumu ed on ed.id=kart.fk_egitimdurumu
		left join bild_tnm_cinsiyet cins on cins.id=kart.fk_cinsiyet
		left join crm_kart_firma fir on fir.fk_gorev=g.id
		LEFT JOIN bild_tnm_il fil ON fil.id = fir.f_fk_il
		where 1=1 and fk_kaydi_acan<>92 
		");
		
	

	SET @q=CONCAT(@q," limit ",_skip,",",_take);
	 PREPARE stmt FROM @q;
	 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptHavuzBildirimSure
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptHavuzBildirimSure`;
delimiter ;;
CREATE PROCEDURE `rptHavuzBildirimSure`(IN sdate TEXT, 
IN fdate TEXT,
IN tip_level1 INT,
IN fk_il INT,
IN fk_sube INT,
IN fk_havuz TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select g.id,g.tarih,h.adi havuz,sb.adi sube,kn.adi kurum,ad.adi konu,MAX(d.id),
NOW() suantarihsaat,
d.tarih yonlendirme_tarihi,
u.adi kullanici,
u.fk_rol rol,
u2.adi kurumyetkili,
TIMESTAMPDIFF(HOUR,d.tarih,NOW()) sure
from SMDR_1002.task_DB g
left join SMDR_1002.task_DBDetay d on d.fk_gorev=g.id
left join SMDR_1002.saphira_users u on u.id=d.fk_agent
left join SMDR_1002.saphira_users u2 on u2.id=g.fk_agent
left join SMDR_1002.task_GorevStatu st on st.id=g.fk_gorev_statu
left join SMDR_1002.task_havuzlar h on h.id=g.fk_havuz
left join SMDR_1002.crm_SubeDB sb on sb.id=h.fk_sube
left join SMDR_1002.task_konu kn on kn.id=sb.fk_konu
left join SMDR_1002.task_alt_konu_detay ad on ad.id=g.tip_level3
where g.fk_gorev_statu<90 and g.fk_gorev_statu<>6 and g.fk_gorev_statu<>0 and g.fk_havuz>0
	and d.sistem='2' and u.fk_rol<>'3' ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q," and g.tarih>='",sdate," 00:00:00' and g.tarih<='",fdate," 23:59:59' ");
		
    -- bu kısım üst düzey yetkililer için filtreleme
		IF(yetki="sv") THEN	
			IF(fk_havuz>0) THEN
				SET @q=CONCAT(@q," and g.fk_havuz=",fk_havuz);
			END IF;
			IF(fk_sube>0) THEN
				SET @q=CONCAT(@q," and g.fk_havuz in (select id from SMDR_1002.task_havuzlar where fk_sube=",fk_sube,")");
			END IF;			
			IF(fk_il>0) THEN
				SET @q=CONCAT(@q," and g.fk_havuz in (select id from SMDR_1002.task_havuzlar where fk_il=",fk_il,")");
			END IF;			
			IF(tip_level1>0) THEN
				SET @q=CONCAT(@q," and g.fk_havuz in (select id from SMDR_1002.task_havuzlar where fk_konu=",tip_level1,")");
			END IF;				
		ELSE
			IF(fk_havuz=0) THEN
				SET @q=CONCAT(@q," and g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user =",userId,") and g._fk_havuz>0 ");
			ELSE
				SET @q=CONCAT(@q," and g.fk_havuz=",fk_havuz);
			END IF;
		END IF;
	
      SET @q=CONCAT(@q," limit ",skip,",",take);
		  -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rpthavuzOzet
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpthavuzOzet`;
delimiter ;;
CREATE PROCEDURE `rpthavuzOzet`(IN skip INT,
IN take INT,
IN userId INT)
BEGIN

  SET @q = "select   s.adi sube, h.id, h.adi,
(select count(*) from task_DB where fk_gorev_statu < 99 and fk_havuz = h.id) acik,
(select count(*) from task_DB where fk_gorev_statu < 99 and tarih <= date_add(current_date, INTERVAL -3 day) and fk_havuz = h.id) uyari_3gun,
(select count(*) from task_DB where fk_gorev_statu < 99 and tarih <= date_add(current_date, INTERVAL -10 day) and fk_havuz = h.id) uyari_10gun,
(select count(*) from task_DB where fk_gorev_statu < 99 and tarih <= date_add(current_date, INTERVAL -1 month) and fk_havuz = h.id) uyari_1ay,
(select count(*) from task_DB where fk_gorev_statu < 99 and tarih <= date_add(current_date, INTERVAL -2 month) and fk_havuz = h.id) uyari_2ay
 from task_havuzlar h
left join crm_SubeDB s on s.id = h.fk_sube
 where h.isActive = 1
and (select count(*) from task_DB where fk_gorev_statu < 99 and fk_havuz = h.id) > 0
order by s.adi, h.adi  ";


      SET @q=CONCAT(@q," limit ",skip,",",take);
		  -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rpthavuzOzetCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpthavuzOzetCount`;
delimiter ;;
CREATE PROCEDURE `rpthavuzOzetCount`(IN userId INT)
BEGIN

  SET @q = "select  COUNT(*) total 
(select count(*) from task_DB where fk_gorev_statu < 99 and fk_havuz = h.id) acik,
(select count(*) from task_DB where fk_gorev_statu < 99 and tarih <= date_add(current_date, INTERVAL -3 day) and fk_havuz = h.id) uyari_3gun,
(select count(*) from task_DB where fk_gorev_statu < 99 and tarih <= date_add(current_date, INTERVAL -10 day) and fk_havuz = h.id) uyari_10gun,
(select count(*) from task_DB where fk_gorev_statu < 99 and tarih <= date_add(current_date, INTERVAL -1 month) and fk_havuz = h.id) uyari_1ay,
(select count(*) from task_DB where fk_gorev_statu < 99 and tarih <= date_add(current_date, INTERVAL -2 month) and fk_havuz = h.id) uyari_2ay
 from task_havuzlar h
left join crm_SubeDB s on s.id = h.fk_sube
 where h.isActive = 1
and (select count(*) from task_DB where fk_gorev_statu < 99 and fk_havuz = h.id) > 0
order by s.adi, h.adi  ";
		  -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptIlBazinda
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptIlBazinda`;
delimiter ;;
CREATE PROCEDURE `rptIlBazinda`(IN sdate TEXT, 
IN fdate TEXT,
IN statu INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select il.id, il.adi, count(*) adet from SMDR_1002.task_DB t
left join  SMDR_1002.crm_KartDB k on k.id = t.fk_kart
left join SMDR_1002.bild_tnm_il il on il.id = k.fk_il 
 WHERE 1=1 ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q,"  and t.tarih >= '",sdate," 00:00:00' and t.tarih <= '",fdate," 23:59:59' ");
		IF(statu>0) THEN
			SET @q=CONCAT(@q," and t.fk_gorev_statu=",statu);
		END IF;
    -- bu kısım üst düzey yetkililer için filtreleme
		IF(yetki<>"sv") THEN	
			SET @q=CONCAT(@q," and g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = ",userId,")");
		END IF;
		
		SET @q=CONCAT(@q," group by il.adi ");
		   select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptIvrAgent
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptIvrAgent`;
delimiter ;;
CREATE PROCEDURE `rptIvrAgent`(IN sdate TEXT, 
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "SELECT fn_FormatNumber(e.caller_id) ARAYANNO,
		fn_FormatNumber(a.callerid) ARANANNO,
		e.ext EXT,
		u.adi ADISOYADI,
		l.adi LOKASYON,
		a.tarih TARIH,

		CASE a.s1
	WHEN '0' THEN 'OY VERMEMİŞ'
	WHEN '1' THEN 'OLUMLU'
	WHEN '2' THEN 'OLUMSUZ'
	ELSE 'HATALI TUŞLAMA'
	END as SORU1,

		CASE a.s2
	WHEN '0' THEN 'OY VERMEMİŞ'
	WHEN '1' THEN 'OLUMLU'
	WHEN '2' THEN 'OLUMSUZ'
	ELSE 'HATALI TUŞLAMA'
	END as SORU2,

		CASE a.s3
	WHEN '0' THEN 'OY VERMEMİŞ'
	WHEN '1' THEN 'OLUMLU'
	WHEN '2' THEN 'OLUMSUZ'
	ELSE 'HATALI TUŞLAMA'
	END as SORU3,

		CASE a.s4
	WHEN '0' THEN 'OY VERMEMİŞ'
	WHEN '1' THEN 'OLUMLU'
	WHEN '2' THEN 'OLUMSUZ'
	ELSE 'HATALI TUŞLAMA'
	END as SORU4,

		CASE a.s5
	WHEN '0' THEN 'OY VERMEMİŞ'
	WHEN '1' THEN 'OLUMLU'
	WHEN '2' THEN 'OLUMSUZ'
	ELSE 'HATALI TUŞLAMA'
	END as SORU5


		FROM anket a
LEFT JOIN pbx_que_log e on e.uniqid=a.uniq
LEFT JOIN saphira_users u on u.ext=e.ext
LEFT JOIN crm_Lokasyon l on l.id=u.fk_lokasyon
WHERE 1=1 ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q,"  and a.tarih>='",sdate," 00:00:00' and a.tarih<='",fdate," 23:59:59' ");
		SET @q=CONCAT(@q," limit ",skip,",",take);
		-- select @q;
		PREPARE stmt FROM @q;
		EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptIvrAgentCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptIvrAgentCount`;
delimiter ;;
CREATE PROCEDURE `rptIvrAgentCount`(IN sdate TEXT, 
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "SELECT Count(*) total
FROM anket a
LEFT JOIN pbx_que_log e on e.uniqid=a.uniq
LEFT JOIN saphira_users u on u.ext=e.ext
LEFT JOIN crm_Lokasyon l on l.id=u.fk_lokasyon
WHERE 1=1 ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q,"  and a.tarih>='",sdate," 00:00:00' and a.tarih<='",fdate," 23:59:59' ");
		-- select @q;
		PREPARE stmt FROM @q;
		EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptIvrRapor
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptIvrRapor`;
delimiter ;;
CREATE PROCEDURE `rptIvrRapor`(IN sdate TEXT, 
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select count(*) total,  \n";

	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s1=0 and s2=0 and s3=0 and s4=0 and s5=0 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') katilmayan, \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s1=1 and s2=1 and s3=1 and s4=1 and s5=1 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') olumlu, \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s1=2 and s2=2 and s3=2 and s4=2 and s5=2 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') olumsuz, \n");
	
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s1=1 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s1olumlu,  \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s2=1 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s2olumlu,  \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s3=1 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s3olumlu,  \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s4=1 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s4olumlu,  \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s5=1 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s5olumlu,  \n");

	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s1=2 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s1olumsuz,  \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s2=2 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s2olumsuz,  \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s3=2 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s3olumsuz,  \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s4=2 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s4olumsuz,  \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.anket where s5=2 and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59') s5olumsuz  \n");

	SET @q=CONCAT(@q,"from SMDR_1002.anket where  tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59'  \n");
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKonularaGore
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKonularaGore`;
delimiter ;;
CREATE PROCEDURE `rptKonularaGore`(IN sdate TEXT, 
IN fdate TEXT,
IN statu INT,
IN tip_level1 INT,
IN tip_level2 INT,
IN tip_level3 INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select tip_level1, konu.adi konu,  akonu.adi akonu, dkonu.adi detay, count(*) adet from SMDR_1002.task_DB t
left join SMDR_1002.task_konu konu on konu.id = t.tip_level1
left join SMDR_1002.task_alt_konu akonu on akonu.id = t.tip_level2
left join SMDR_1002.task_alt_konu_detay dkonu on dkonu.id = t.tip_level3 WHERE 1=1 ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q,"  and t.tarih >= '",sdate," 00:00:00' and t.tarih <= ' ",fdate," 23:59:59' ");
		
		IF(statu>0) THEN
			SET @q=CONCAT(@q," and t.fk_gorev_statu=",statu);
		END IF;
		
		IF(tip_level1>0) THEN
			SET @q=CONCAT(@q," and t.tip_level1=",tip_level1);
		END IF;		
		
		IF(tip_level2>0) THEN
			SET @q=CONCAT(@q," and t.tip_level2=",tip_level2);
		END IF;		
		
		IF(tip_level3>0) THEN
			SET @q=CONCAT(@q," and t.tip_level3=",tip_level3);
		END IF;				
    -- bu kısım üst düzey yetkililer için filtreleme
		IF(yetki<>"sv") THEN	
			SET @q=CONCAT(@q,"  and t.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = ",userId,")");
		END IF;

		SET @q=CONCAT(@q," group by tip_level1, tip_level2, tip_level3
order by 5 desc");
		  -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKurumBazindaZamanindaKapanan
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKurumBazindaZamanindaKapanan`;
delimiter ;;
CREATE PROCEDURE `rptKurumBazindaZamanindaKapanan`(IN sdate TEXT, 
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select h.adi, ";
	SET @q=CONCAT(@q,"(select count(*) from  SMDR_1002.task_DB  where tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59' and fk_havuz=h.id) toplam,");
	SET @q=CONCAT(@q,"(select count(*) from  SMDR_1002.task_DB where tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59' and fk_havuz=h.id and fk_gorev_statu=99 and TIMESTAMPDIFF(SECOND, tarih, kapanma_tarihi)<4) kapanmis ");
	SET @q=CONCAT(@q," from SMDR_1002.task_havuzlar h
left join SMDR_1002.crm_SubeDB s on s.id=h.fk_sube
where s.fk_konu=1 ");
	SET @q=CONCAT(@q," limit ",skip,",",take);
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKurumBazindaZamanindaKapananCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKurumBazindaZamanindaKapananCount`;
delimiter ;;
CREATE PROCEDURE `rptKurumBazindaZamanindaKapananCount`(IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select COUNT(*) total ";
	SET @q=CONCAT(@q," from SMDR_1002.task_havuzlar h 
left join SMDR_1002.crm_SubeDB s on s.id=h.fk_sube
where s.fk_konu=1 ");
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKurumKapanmaIstatistik
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKurumKapanmaIstatistik`;
delimiter ;;
CREATE PROCEDURE `rptKurumKapanmaIstatistik`(IN sdate TEXT, 
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select count(*) adet,k.adi konu from SMDR_1002.task_DB g 
left join SMDR_1002.task_konu k on k.id=g.tip_level1
where g.fk_kaydi_acan=g.fk_kapatan_agent and g.fk_gorev_statu=99 ";
	SET @q=CONCAT(@q,"  and g.tarih>='",sdate," 00:00:00' and  g.tarih<='",fdate," 23:59:59' ");
	SET @q=CONCAT(@q," group by k.adi");	
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKurumKullaniciIstatistik
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKurumKullaniciIstatistik`;
delimiter ;;
CREATE PROCEDURE `rptKurumKullaniciIstatistik`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fkKonu INT,
IN _fkIl INT,
IN _fkSube INT,
IN _skip INT,
IN _take INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN
	
-- sube konu havuz ile filtreleme sadece üst düzey yetkilendirmede olacak
-- ust duzey yetkisi olmayan kullanıcılarda userid parametresi gonderilecek
  SET @q = "select u.adi user,u.id,s.adi sube,ifnull(i.adi,'') il, ";
	SET @q=CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=99 and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59' and fk_kapatan_agent=u.id) kapanan,");
	SET @q=CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu>99 and fk_gorev_statu<>6 and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59' and fk_agent=u.id) acik");
	SET @q=CONCAT(@q," from SMDR_1002.saphira_users u left join SMDR_1002.crm_SubeDB s on s.id=u.fk_sube left join SMDR_1002.bild_tnm_il i on i.id=u.fk_il where 1=1 and u.isActive=1 and u.fk_rol>1 and u.fk_rol<>9999 ");       
        -- bu kısım üst düzey yetkililer için filtreleme
  IF(_yetki="sv") THEN	
			IF(_fkSube>0) THEN
				SET @q = CONCAT(@q," and u.fk_sube=",_fkSube);
			END IF;
			IF(_fkIl>0) THEN
				SET @q = CONCAT(@q," and u.fk_il=",_fkIl);
			END IF;              
			IF(_fkKonu>0) THEN
				SET @q = CONCAT(@q," and s.fk_konu=",_fkKonu);
			END IF;             
  ELSE
				SET @q = CONCAT(@q," and u.fk_il=(select fk_il from SMDR_1002.saphira_users where id=",_userId,") and u.isActive=1 ");              
	END IF;
        SET @q=CONCAT(@q," limit ",_skip,",",_take);
		-- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKurumKullaniciIstatistikCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKurumKullaniciIstatistikCount`;
delimiter ;;
CREATE PROCEDURE `rptKurumKullaniciIstatistikCount`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fkKonu INT,
IN _fkIl INT,
IN _fkSube INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN
	
-- sube konu havuz ile filtreleme sadece üst düzey yetkilendirmede olacak
-- ust duzey yetkisi olmayan kullanıcılarda userid parametresi gonderilecek
  SET @q = "select COUNT(*) total from SMDR_1002.saphira_users u left join SMDR_1002.crm_SubeDB s on s.id=u.fk_sube left join SMDR_1002.bild_tnm_il i on i.id=u.fk_il where 1=1 and u.isActive=1 and u.fk_rol>1 and u.fk_rol<>9999 ";
     
        -- bu kısım üst düzey yetkililer için filtreleme
  IF(_yetki="sv") THEN	
			IF(_fkSube>0) THEN
				SET @q = CONCAT(@q," and u.fk_sube=",_fkSube);
			END IF;
			IF(_fkIl>0) THEN
				SET @q = CONCAT(@q," and u.fk_il=",_fkIl);
			END IF;              
			IF(_fkKonu>0) THEN
				SET @q = CONCAT(@q," and s.fk_konu=",_fkKonu);
			END IF;             
  ELSE
				SET @q = CONCAT(@q," and u.fk_il=(select fk_il from SMDR_1002.saphira_users where id=",_userId,") and u.isActive=1 ");        
	END IF;

		 select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKurumSubeHavuzIstatistik
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKurumSubeHavuzIstatistik`;
delimiter ;;
CREATE PROCEDURE `rptKurumSubeHavuzIstatistik`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fkKonu INT,
IN _fkIl INT,
IN _fkSube INT,
IN _fkHavuz INT,
IN _skip INT,
IN _take INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN
	
-- sube konu havuz ile filtreleme sadece üst düzey yetkilendirmede olacak
-- ust duzey yetkisi olmayan kullanıcılarda userid parametresi gonderilecek
    SET @q = "select h.*,h.adi havuzadi,h.id havuzid,s.adi sube,i.adi il,k.adi kurum, ";
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=99 and fk_havuz=h.id and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') kapanan,");
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=7 and fk_havuz=h.id and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') guncelleme_talebi,");
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=1 and fk_havuz=h.id and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') birime_atandi,");
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=2 and fk_havuz=h.id and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') inceleniyor,");
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=3 and fk_havuz=h.id and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') inceleniyor_eksure,");
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=0 and fk_havuz=h.id and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') islem_yapilmamis,");
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=5 and fk_havuz=h.id and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') reddedildi,");
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_havuz=h.id and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') toplam,");

		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=99 and bitis_tar<kapanma_tarihi and fk_havuz=h.id and bitis_tar>'0000-00-00 00:00:00' and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') zamaninda_kapatilmayan,");
		SET @q = CONCAT(@q," (select count(*) from SMDR_1002.task_DB where fk_gorev_statu=99 and bitis_tar>kapanma_tarihi and fk_havuz=h.id and bitis_tar>'0000-00-00 00:00:00' and tarih>='",_sdate," 00:00:00' and tarih<='",_fdate," 23:59:59') zamaninda_kapanan");
		SET @q = CONCAT(@q," from SMDR_1002.task_havuzlar h
left join SMDR_1002.crm_SubeDB s on s.id=h.fk_sube
left join SMDR_1002.bild_tnm_il i on i.id=h.fk_il
left join SMDR_1002.task_konu k on k.id=h.fk_konu
where 1=1 ");        
        -- bu kısım üst düzey yetkililer için filtreleme
        IF(_yetki="sv") THEN	
			IF(_fkSube>0) THEN
				SET @q = CONCAT(@q," and h.fk_sube=",_fkSube);
			END IF;
			IF(_fkHavuz>0) THEN
				SET @q = CONCAT(@q," and h.id=",_fkHavuz);
			END IF;	
			IF(_fkIl>0) THEN
				SET @q = CONCAT(@q," and h.fk_il=",_fkIl);
			END IF;              
			IF(_fkKonu>0) THEN
				SET @q = CONCAT(@q," and h.fk_konu=",_fkKonu);
			END IF;             
            ELSE
				IF(_userId>0) THEN
					SET @q = CONCAT(@q," and h.id in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user =",_userId,")");
				END IF;	
				IF(_fkIl>0) THEN
					SET @q = CONCAT(@q," and h.fk_il=",_fkIl);
				END IF;                
		END IF;
        SET @q=CONCAT(@q," limit ",_skip,",",_take);
		 -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKurumSubeHavuzIstatistikCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKurumSubeHavuzIstatistikCount`;
delimiter ;;
CREATE PROCEDURE `rptKurumSubeHavuzIstatistikCount`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fkKonu INT,
IN _fkIl INT,
IN _fkSube INT,
IN _fkHavuz INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN
	
-- sube konu havuz ile filtreleme sadece üst düzey yetkilendirmede olacak
-- ust duzey yetkisi olmayan kullanıcılarda userid parametresi gonderilecek
    SET @q = "select count(*) total	from SMDR_1002.task_havuzlar h
		left join SMDR_1002.crm_SubeDB s on s.id=h.fk_sube
		left join SMDR_1002.bild_tnm_il i on i.id=h.fk_il
		left join SMDR_1002.task_konu k on k.id=h.fk_konu
		where 1=1 
		";
    
       -- bu kısım üst düzey yetkililer için filtreleme
    IF(_yetki="sv") THEN	
				IF(_fkSube>0) THEN
					SET @q = CONCAT(@q," and h.fk_sube=",_fkSube);
				END IF;
				IF(_fkHavuz>0) THEN
					SET @q = CONCAT(@q," and h.id=",_fkHavuz);
				END IF;	
				IF(_fkIl>0) THEN
					SET @q = CONCAT(@q," and h.fk_il=",_fkIl);
				END IF;              
				IF(_fkKonu>0) THEN
					SET @q = CONCAT(@q," and h.fk_konu=",_fkKonu);
				END IF;             
    ELSE
				IF(_userId>0) THEN
					SET @q = CONCAT(@q," and h.id in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user =",_userId,")");
				END IF;	
				IF(_fkIl>0) THEN
					SET @q = CONCAT(@q," and h.fk_il=",_fkIl);
				END IF;                
		END IF;
        
		 -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKySure
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKySure`;
delimiter ;;
CREATE PROCEDURE `rptKySure`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fk_agent INT,
IN _sistem TEXT,
IN _fk_kurum INT,
IN _skip INT,
IN _take INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN

  SET @q = "select SEC_TO_TIME2(s.sure) sure,s.fk_gorev,u.adi,h.adi havuz,
k.adi kurum,k1.adi altkurum,k2.adi aramakonusu,sb.adi sube,il.adi il,t.tarih btarih
 from SMDR_1002.crm_islem_sureleri s
left join SMDR_1002.saphira_users u on u.id=s.fk_agent
left join SMDR_1002.task_havuzlar h on h.id=s.fk_havuz
left join SMDR_1002.task_konu k on k.id=s.tip_level1
left join SMDR_1002.task_alt_konu k1 on k1.id=s.tip_level2
left join SMDR_1002.task_alt_konu_detay k2 on k2.id=s.tip_level3
left join SMDR_1002.crm_SubeDB sb on sb.id=s.fk_sube
left join SMDR_1002.bild_tnm_il il on il.id=s.fk_il
left join SMDR_1002.task_DB t on t.id=s.fk_gorev
where u.fk_rol=3 ";
		IF(_sdate="" OR _fdate="") THEN	
			SET _sdate=CURRENT_DATE();
			SET _fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q," and s.tarih>='",_sdate," 00:00:00' and s.tarih<='",_fdate," 23:59:59' ");
    IF(_sistem<>'0' AND _sistem<>'sure') THEN
			SET @q=CONCAT(@q," and s.sistem=",_sistem);
		END IF;

		
        -- bu kısım üst düzey yetkililer için filtreleme
  IF(_yetki="sv") THEN	
			IF(_fk_agent>0) THEN
				SET @q=CONCAT(@q," and s.fk_agent=",_fk_agent);
			END IF; 
			IF(_fk_kurum>0) THEN
				SET @q=CONCAT(@q," and s.tip_level1=",_fk_kurum);
			END IF; 	
  ELSE
			SET @q=CONCAT(@q," and s.fk_agent=",_userId);
	END IF;
	
        SET @q=CONCAT(@q," limit ",_skip,",",_take);
		 -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptKySureCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptKySureCount`;
delimiter ;;
CREATE PROCEDURE `rptKySureCount`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fk_agent INT,
IN _sistem TEXT,
IN _fk_kurum INT,
IN _skip INT,
IN _take INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN

  SET @q = "select COUNT(*) total 
 from SMDR_1002.crm_islem_sureleri s
left join SMDR_1002.saphira_users u on u.id=s.fk_agent
left join SMDR_1002.task_havuzlar h on h.id=s.fk_havuz
left join SMDR_1002.task_konu k on k.id=s.tip_level1
left join SMDR_1002.task_alt_konu k1 on k1.id=s.tip_level2
left join SMDR_1002.task_alt_konu_detay k2 on k2.id=s.tip_level3
left join SMDR_1002.crm_SubeDB sb on sb.id=s.fk_sube
left join SMDR_1002.bild_tnm_il il on il.id=s.fk_il
left join SMDR_1002.task_DB t on t.id=s.fk_gorev
where u.fk_rol=3 ";
		IF(_sdate="" OR _fdate="") THEN	
			SET _sdate=CURRENT_DATE();
			SET _fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q," and s.tarih>='",_sdate," 00:00:00' and s.tarih<='",_fdate," 23:59:59' ");
    IF(_sistem<>'0' AND _sistem<>'sure') THEN
			SET @q=CONCAT(@q," and s.sistem=",_sistem);
		END IF;

		
        -- bu kısım üst düzey yetkililer için filtreleme
  IF(_yetki="sv") THEN	
			IF(_fk_agent>0) THEN
				SET @q=CONCAT(@q," and s.fk_agent=",_fk_agent);
			END IF; 
			IF(_fk_kurum>0) THEN
				SET @q=CONCAT(@q," and s.tip_level1=",_fk_kurum);
			END IF; 	
  ELSE
			SET @q=CONCAT(@q," and s.fk_agent=",_userId);
	END IF;
	
        SET @q=CONCAT(@q," limit ",_skip,",",_take);
		 select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptMuAktivite
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptMuAktivite`;
delimiter ;;
CREATE PROCEDURE `rptMuAktivite`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fk_agent INT,
IN _sistem INT,
IN _skip INT,
IN _take INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN

  SET @q = "select
		g.*,
		date_format(g.tarih,'%d.%m.%Y %H:%i') gtarih,
		u.adi agent,
		st.adi statu,
		st.class statu_class,
		di.adi detay_islem
		from SMDR_1002.task_DBDetay g
		left join SMDR_1002.saphira_users u on u.id=g.fk_agent
		left join SMDR_1002.task_DB gg on gg.id=g.fk_gorev
		left join SMDR_1002.task_GorevStatu st on st.id=gg.fk_gorev_statu
		left join SMDR_1002.bild_tnm_islem_tipleri di on di.id=g.sistem 
		where 1=1 and u.fk_rol=2 ";
		SET @q=CONCAT(@q," and g.tarih>='",_sdate," 00:00:00' and g.tarih<='",_fdate," 23:59:59' ");
    IF(_sistem>0) THEN
			SET @q=CONCAT(@q," and g.sistem=",_sistem);
		END IF;

        -- bu kısım üst düzey yetkililer için filtreleme
  IF(_yetki="sv") THEN	
			IF(_fk_agent>0) THEN
				SET @q=CONCAT(@q," and g.fk_agent=",_fk_agent);
			END IF;    
  ELSE
			IF(_fk_agent>0) THEN
				SET @q=CONCAT(@q," and g.fk_agent=",_fk_agent);
			ELSE
				SET @q=CONCAT(@q," and g.fk_agent=",_userId);
			END IF; 
			-- SET @q = CONCAT(@q," and u.fk_il=(select fk_il from SMDR_1002.saphira_users where id=",_userId,") and u.isActive=1 ");              
	END IF;
        SET @q=CONCAT(@q," limit ",_skip,",",_take);
		-- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptMuAktiviteCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptMuAktiviteCount`;
delimiter ;;
CREATE PROCEDURE `rptMuAktiviteCount`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fk_agent INT,
IN _sistem INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN

  SET @q = "select COUNT(*) total 
		from SMDR_1002.task_DBDetay g
		left join SMDR_1002.saphira_users u on u.id=g.fk_agent
		left join SMDR_1002.task_DB gg on gg.id=g.fk_gorev
		left join SMDR_1002.task_GorevStatu st on st.id=gg.fk_gorev_statu
		where 1=1 and u.fk_rol=2 ";
		SET @q=CONCAT(@q," and g.tarih>='",_sdate," 00:00:00' and g.tarih<='",_fdate," 23:59:59' ");
    IF(_sistem>0) THEN
			SET @q=CONCAT(@q," and g.sistem=",_sistem);
		END IF;

        -- bu kısım üst düzey yetkililer için filtreleme
  IF(_yetki="sv") THEN	
			IF(_fk_agent>0) THEN
				SET @q=CONCAT(@q," and g.fk_agent=",fk_agent);
			END IF;    
  ELSE
			IF(_fk_agent>0) THEN
				SET @q=CONCAT(@q," and g.fk_agent=",fk_agent);
			ELSE
				SET @q=CONCAT(@q," and g.fk_agent=",_userId);
			END IF; 
			-- SET @q = CONCAT(@q," and u.fk_il=(select fk_il from SMDR_1002.saphira_users where id=",_userId,") and u.isActive=1 ");              
	END IF;
		-- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptMuSure
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptMuSure`;
delimiter ;;
CREATE PROCEDURE `rptMuSure`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fk_agent INT,
IN _tip_level1 INT,
IN _sistem INT,
IN _skip INT,
IN _take INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN

  SET @q = "select SEC_TO_TIME2(s.sure) sure,
	s.fk_gorev,
	u.adi mu,
	h.adi havuz,
	k.adi kurum,
	k1.adi altkurum,
	k2.adi aramakonusu,
	sb.adi sube,
	il.adi il,
	CONCAT(k.adi,'<strong> >> </strong>',k1.adi,'<strong> >> </strong>',k2.adi) kurumBilgisihtml,
	CONCAT(k.adi,'>>',k1.adi,'>>',k2.adi) kurumBilgisi,
date_format(t.tarih,'%d.%m.%Y %H:%i') btarih
 from SMDR_1002.crm_islem_sureleri s
left join SMDR_1002.saphira_users u on u.id=s.fk_agent
left join SMDR_1002.task_havuzlar h on h.id=s.fk_havuz
left join SMDR_1002.task_konu k on k.id=s.tip_level1
left join SMDR_1002.task_alt_konu k1 on k1.id=s.tip_level2
left join SMDR_1002.task_alt_konu_detay k2 on k2.id=s.tip_level3
left join SMDR_1002.crm_SubeDB sb on sb.id=s.fk_sube
left join SMDR_1002.bild_tnm_il il on il.id=s.fk_il
left join SMDR_1002.task_DB t on t.id=s.fk_gorev
where u.fk_rol>0 ";
		SET @q=CONCAT(@q," and s.tarih>='",_sdate," 00:00:00' and s.tarih<='",_fdate," 23:59:59' ");
    IF(_sistem>0) THEN
			SET @q=CONCAT(@q," and s.sistem=",_sistem);
		END IF;
		IF(_tip_level1>0) THEN
			SET @q=CONCAT(@q," and s.tip_level1=",_tip_level1);
		END IF;
		IF(_fk_agent>0) THEN
			SET @q=CONCAT(@q," and s.fk_agent=",_fk_agent);
		END IF; 	
	SET @q=CONCAT(@q," order by s.tarih desc");	
    SET @q=CONCAT(@q," limit ",_skip,",",_take);
		
		-- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptMuSureCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptMuSureCount`;
delimiter ;;
CREATE PROCEDURE `rptMuSureCount`(IN _sdate TEXT, 
IN _fdate TEXT,
IN _fk_agent INT,
IN _tip_level1 INT,
IN _sistem INT,
IN _userId INT,
IN _yetki TEXT)
BEGIN

  SET @q = " SELECT COUNT(*) total 
 from SMDR_1002.crm_islem_sureleri s
left join SMDR_1002.saphira_users u on u.id=s.fk_agent
left join SMDR_1002.task_havuzlar h on h.id=s.fk_havuz
left join SMDR_1002.task_konu k on k.id=s.tip_level1
left join SMDR_1002.task_alt_konu k1 on k1.id=s.tip_level2
left join SMDR_1002.task_alt_konu_detay k2 on k2.id=s.tip_level3
left join SMDR_1002.crm_SubeDB sb on sb.id=s.fk_sube
left join SMDR_1002.bild_tnm_il il on il.id=s.fk_il
left join SMDR_1002.task_DB t on t.id=s.fk_gorev
where u.fk_rol>0 ";
		SET @q=CONCAT(@q," and s.tarih>='",_sdate," 00:00:00' and s.tarih<='",_fdate," 23:59:59' ");
    IF(_sistem>0) THEN
			SET @q=CONCAT(@q," and s.sistem=",_sistem);
		END IF;
		IF(_tip_level1>0) THEN
			SET @q=CONCAT(@q," and s.tip_level1=",_tip_level1);
		END IF;
		IF(_fk_agent>0) THEN
			SET @q=CONCAT(@q," and s.fk_agent=",_fk_agent);
		END IF; 	
		
		-- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptPeriyodikRapor
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptPeriyodikRapor`;
delimiter ;;
CREATE PROCEDURE `rptPeriyodikRapor`(IN sdate TEXT,
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

	SET @q = "select \n
  	l.que, \n
sum(l.gelen_adet) toplam_gelen, \n
sum(l.cevaplanan_adet) toplam_cevaplanan, \n
sum(l.kayip_adet) toplam_kayip, \n
SEC_TO_TIME2(round(SUM(l.gelen_sure) / sum(l.cevaplanan_adet))) ORT_GOR_SURE, \n
SEC_TO_TIME2(ceil(SUM(l.gelen_sure)/60)) TOPL_SURE, \n
SEC_TO_TIME2(round(SUM(l.bekleme_suresi) / sum(l.gelen_adet))) ORT_BEKL_SURE, \n";

	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.task_DB where tarih >='",sdate," 00:00:00' and tarih <='",fdate," 23:59:59') toplam_bildirim, \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.task_DB where fk_kapatan_agent=fk_kaydi_acan and tarih >='",sdate," 00:00:00' and tarih <='",fdate," 23:59:59') ilk_kontak \n");
	SET @q=CONCAT(@q," FROM SMDR_1002.log_que_gunluk_performans l WHERE 1=1 \n");
	SET @q=CONCAT(@q," and l.tarih >='",sdate,"' and l.tarih <='",fdate,"' \n");
	SET @q=CONCAT(@q," limit ",skip,",",take);
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptPeriyodikRaporDisAramaSms
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptPeriyodikRaporDisAramaSms`;
delimiter ;;
CREATE PROCEDURE `rptPeriyodikRaporDisAramaSms`(IN sdate TEXT,
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

	SET @q = "select count(*) smssay, \n";
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.email_liste where  tarih >='",sdate,"' and tarih <='",fdate,"') emailsay,\n");
	SET @q=CONCAT(@q,"(SELECT SEC_TO_TIME2(round(sum(talk_time))) sure FROM SMDR_1002.pbx_que_log WHERE que='7136' and join_date >='",sdate,"' and join_date <='",fdate,"') sure,\n");
	SET @q=CONCAT(@q,"(SELECT count(*)  FROM SMDR_1002.pbx_que_log WHERE que='7136' and join_date >='",sdate,"' and join_date <='",fdate,"') dissay \n");
	SET @q=CONCAT(@q,"from SMDR_1002.sms_liste where tarih >='",sdate,"' and tarih <='",fdate,"'");
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptPeriyodikRaporGelenCagri
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptPeriyodikRaporGelenCagri`;
delimiter ;;
CREATE PROCEDURE `rptPeriyodikRaporGelenCagri`(IN sdate TEXT,
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

	SET @q = "select \n
  	l.que, \n
sum(l.gelen_adet) toplam_gelen, \n
sum(l.cevaplanan_adet) toplam_cevaplanan, \n
sum(l.kayip_adet) toplam_kayip, \n
SEC_TO_TIME2(round(SUM(l.gelen_sure) / sum(l.cevaplanan_adet))) ORT_GOR_SURE, \n
SEC_TO_TIME2(ceil(SUM(l.gelen_sure)/60)) TOPL_SURE, \n
SEC_TO_TIME2(round(SUM(l.bekleme_suresi) / sum(l.gelen_adet))) ORT_BEKL_SURE, \n";

	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.task_DB where tarih >='",sdate," 00:00:00' and tarih <='",fdate," 23:59:59') toplam_bildirim, \n");
	SET @q=CONCAT(@q,"(select count(*) from SMDR_1002.task_DB where fk_kapatan_agent=fk_kaydi_acan and tarih >='",sdate," 00:00:00' and tarih <='",fdate," 23:59:59') ilk_kontak \n");
	SET @q=CONCAT(@q," FROM SMDR_1002.log_que_gunluk_performans l WHERE 1=1 \n");
	SET @q=CONCAT(@q," and l.tarih >='",sdate,"' and l.tarih <='",fdate,"' \n");
	SET @q=CONCAT(@q," limit ",skip,",",take);
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptPeriyodikRaporGelenCagriCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptPeriyodikRaporGelenCagriCount`;
delimiter ;;
CREATE PROCEDURE `rptPeriyodikRaporGelenCagriCount`(IN sdate TEXT,
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

	SET @q = "select COUNT(*) total ";
	SET @q=CONCAT(@q," FROM SMDR_1002.log_que_gunluk_performans l WHERE 1=1 ");
	SET @q=CONCAT(@q,"l.tarih >='",sdate,"' and l.tarih <='",fdate,"' ");
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptPeriyodikRaporIlkOnKonu
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptPeriyodikRaporIlkOnKonu`;
delimiter ;;
CREATE PROCEDURE `rptPeriyodikRaporIlkOnKonu`(IN sdate TEXT,
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

	SET @q = "select t3.adi konu,count(*) say from SMDR_1002.task_DB g 
left join SMDR_1002.task_alt_konu_detay t3 on t3.id=g.tip_level3 WHERE 1=1 ";
	SET @q=CONCAT(@q," and g.tarih >='",sdate," 00:00:00' and g.tarih <='",fdate," 23:59:59' ");
	SET @q=CONCAT(@q," group by g.tip_level3 order by say desc limit 10");
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptRaporLog
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptRaporLog`;
delimiter ;;
CREATE PROCEDURE `rptRaporLog`(IN sdate TEXT,
IN fdate TEXT,
IN fk_user INT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

	SET @q = "select l.*,u.adi from SMDR_1002.rapor_log l
left join SMDR_1002.saphira_users u on u.id=l.fk_user
where 1=1 and u.fk_rol not in(9999) ";

	IF (fk_user>0) THEN
		SET @q=CONCAT(@q," and fk_user=",fk_user);
	ELSE
		SET @q=CONCAT(@q," and l.tarih>='",sdate," 00:00:00' and l.tarih<='",fdate," 23:59:59' ");
	END IF;
	SET @q=CONCAT(@q," group by u.adi,l.rapor order by id desc ");
	SET @q=CONCAT(@q," limit ",skip,",",take);
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSektorBazinda
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSektorBazinda`;
delimiter ;;
CREATE PROCEDURE `rptSektorBazinda`(IN sdate TEXT, 
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select s.id sid, s.adi sektor, count(*) adet from SMDR_1002.task_DB t
join SMDR_1002.crm_sektor s on s.id = t.fk_proje where 1=1 ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q," and t.tarih >= ''",sdate," 00:00:00' and t.tarih<='",fdate," 23:59:59' ");
		
    -- bu kısım üst düzey yetkililer için filtreleme
		IF(yetki<>"sv") THEN	
			SET @q=CONCAT(@q," and t.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user =",userId,")");
		END IF;
			SET @q=CONCAT(@q," group by s.adi ");
      SET @q=CONCAT(@q," limit ",skip,",",take);
		  -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSektorBazindaCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSektorBazindaCount`;
delimiter ;;
CREATE PROCEDURE `rptSektorBazindaCount`(IN sdate TEXT, 
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select s.id sid, s.adi sektor, count(*) total from SMDR_1002.task_DB t
join SMDR_1002.crm_sektor s on s.id = t.fk_proje where 1=1 ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q," and t.tarih >= ''",sdate," 00:00:00' and t.tarih<='",fdate," 23:59:59' ");
		
    -- bu kısım üst düzey yetkililer için filtreleme
		IF(yetki<>"sv") THEN	
			SET @q=CONCAT(@q," and t.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user =",userId,")");
		END IF;
			SET @q=CONCAT(@q," group by s.adi ");
		  -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSektorBazindaIhbarSikayet
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSektorBazindaIhbarSikayet`;
delimiter ;;
CREATE PROCEDURE `rptSektorBazindaIhbarSikayet`(IN sdate TEXT, 
IN fdate TEXT,
IN fk_proje INT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select
		g.*,g.id gorevid,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		gs.adi statu,
		gs.class statu_class,
		su1.adi yapacak_agent,
		g.tarih fulldate,
		tip.adi tip,
		date_format(g.tarih,'%d.%m.%Y') tarih,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		kart.adi musteri,
		kart.soyadi musteris,
		kart.tckimlik tc,
		il.adi iladi,
		ilce.adi ilceadi,
		prj.adi sektor	
		from smdr_1002.task_DB g
		left join smdr_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join smdr_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join smdr_1002.saphira_users su1 on su1.id = g.fk_agent
		left join smdr_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN smdr_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN smdr_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN smdr_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join smdr_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN smdr_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN smdr_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN smdr_1002.crm_sektor prj ON prj.id = g.fk_proje	 
		where 1=1 and g.fk_proje>0 ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q,"  and g.tarih>='",sdate," 00:00:00' and g.tarih<='",fdate," 23:59:59' ");
		IF(fk_proje>0) THEN
			SET @q=CONCAT(@q," and g.fk_proje=",fk_proje);
		END IF;
    -- bu kısım üst düzey yetkililer için filtreleme
		IF(yetki<>"sv") THEN	
			SET @q=CONCAT(@q," and g.fk_havuz in (select hk.fk_havuz from task_havuz_kullanici hk where hk.fk_user =",userId,")");
		END IF;
      SET @q=CONCAT(@q," limit ",skip,",",take);
		  -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSektorBazindaIhbarSikayetCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSektorBazindaIhbarSikayetCount`;
delimiter ;;
CREATE PROCEDURE `rptSektorBazindaIhbarSikayetCount`(IN sdate TEXT, 
IN fdate TEXT,
IN fk_proje INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select COUNT(*) total
		from smdr_1002.task_DB g
		left join smdr_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join smdr_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join smdr_1002.saphira_users su1 on su1.id = g.fk_agent
		left join smdr_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN smdr_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN smdr_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN smdr_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join smdr_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN smdr_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN smdr_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN smdr_1002.crm_sektor prj ON prj.id = g.fk_proje	 
		where 1=1 and g.fk_proje>0 ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q,"  and g.tarih>='",sdate," 00:00:00' and g.tarih<='",fdate," 23:59:59' ");
		IF(fk_proje>0) THEN
			SET @q=CONCAT(@q," and g.fk_proje=",fk_proje);
		END IF;
    -- bu kısım üst düzey yetkililer için filtreleme
		IF(yetki<>"sv") THEN	
			SET @q=CONCAT(@q," and g.fk_havuz in (select hk.fk_havuz from task_havuz_kullanici hk where hk.fk_user =",userId,")");
		END IF;
		  -- select @q;
		  PREPARE stmt FROM @q;
		  EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSesKayit
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSesKayit`;
delimiter ;;
CREATE PROCEDURE `rptSesKayit`(IN sdate TEXT, 
IN fdate TEXT,
IN tckimlik TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "select g.*,concat(k.adi,' ',k.soyadi) adi,k.tckimlik,l.uniqid,l.join_date,sec_to_time(l.talk_time) sure from SMDR_1002.task_DB g 
left join SMDR_1002.crm_KartDB k on k.id=g.fk_kart
left join SMDR_1002.pbx_que_log l on l.uniqid=g.callid
where 1=1 and l.talk_time>0 and l.uniqid<>'' ";
	SET @q=CONCAT(@q,"  and g.tarih>='",sdate," 00:00:00' and  g.tarih<='",fdate," 23:59:59' ");
	IF(LENGTH(tckimlik)=11) THEN
		SET @q=CONCAT(@q," and k.tckimlik='",tckimlik,"' ");
	END IF;
	
	-- bu kısım üst düzey yetkililer için filtreleme
	IF(yetki<>"sv") THEN	
		SET @q=CONCAT(@q," and g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = ",userId,")");
	END IF;
	
	SET @q=CONCAT(@q," order by id desc ");	
	SET @q=CONCAT(@q," limit ",skip,",",take);
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSesKayitCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSesKayitCount`;
delimiter ;;
CREATE PROCEDURE `rptSesKayitCount`(IN sdate TEXT, 
IN fdate TEXT,
IN tckimlik TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "COUNT(*) total from SMDR_1002.task_DB g 
left join SMDR_1002.crm_KartDB k on k.id=g.fk_kart
left join SMDR_1002.pbx_que_log l on l.uniqid=g.callid
where 1=1 and l.talk_time>0 and l.uniqid<>'' ";
	SET @q=CONCAT(@q,"  and g.tarih>='",sdate," 00:00:00' and  g.tarih<='",fdate," 23:59:59' ");
	IF(LENGTH(tckimlik)=11) THEN
		SET @q=CONCAT(@q," and k.tckimlik='",tckimlik,"' ");
	END IF;
	
	-- bu kısım üst düzey yetkililer için filtreleme
	IF(yetki<>"sv") THEN	
		SET @q=CONCAT(@q," and g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = ",userId,")");
	END IF;
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSgkBilgilendirme
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSgkBilgilendirme`;
delimiter ;;
CREATE PROCEDURE `rptSgkBilgilendirme`(IN sdate TEXT,
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "SELECT CONCAT(adi,' ',soyadi) ADSOYAD, tc TC, gsm CEP, date_format(tarih,'%d.%m.%Y %H:%i:%s') TARIH, versiyon, date_format(guncelleme,'%d.%m.%Y %H:%i:%s') GUNCELLEME, email EMAIL
FROM CHAT.gsm_numaralari WHERE versiyon='0' ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q,"  and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59' ");

		SET @q=CONCAT(@q," limit ",skip,",",take);
		-- select @q;
		PREPARE stmt FROM @q;
		EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSgkBilgilendirmeCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSgkBilgilendirmeCount`;
delimiter ;;
CREATE PROCEDURE `rptSgkBilgilendirmeCount`(IN sdate TEXT,
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "SELECT COUNT(*) total 
FROM CHAT.gsm_numaralari WHERE versiyon='0' ";
		IF(sdate="" OR fdate="") THEN	
			SET sdate=CURRENT_DATE();
			SET fdate=CURRENT_DATE();
		END IF;

		SET @q=CONCAT(@q,"  and tarih>='",sdate," 00:00:00' and tarih<='",fdate," 23:59:59' ");
		-- select @q;
		PREPARE stmt FROM @q;
		EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSmsRapor
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSmsRapor`;
delimiter ;;
CREATE PROCEDURE `rptSmsRapor`(IN skip INT,
IN take INT,
IN tipi TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN
	IF (tipi="sms") THEN
		SET @q = "select * from SMDR_1002.sms_liste		where 1=1";
	ELSE
		SET @q = "select * from SMDR_1002.email_liste		where 1=1";
	END IF;
	SET @q=CONCAT(@q," limit ",skip,",",take);
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptSmsRaporCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSmsRaporCount`;
delimiter ;;
CREATE PROCEDURE `rptSmsRaporCount`(IN tipi TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN
	IF (tipi="sms") THEN
		SET @q = "select COUNT(*) total from SMDR_1002.sms_liste		where 1=1";
	ELSE
		SET @q = "select COUNT(*) total from SMDR_1002.email_liste		where 1=1";
	END IF;
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptTaseronHavuz
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptTaseronHavuz`;
delimiter ;;
CREATE PROCEDURE `rptTaseronHavuz`(IN sdate TEXT, 
IN fdate TEXT,
IN skip INT,
IN take INT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "SELECT t.tarih TARIH, 
case t.internet
when '1' then 'İnternet Sitesi'
else 'Telefon'
end as aramakanali,
t.id BILDIRIMNO, CONCAT(k.adi, ' ',k.soyadi) VATANDAS,k.tckimlik TC, t.aciklama ACIKLAMA FROM SMDR_1002.task_DB t
left JOIN SMDR_1002.crm_KartDB k on k.id=t.fk_kart
WHERE t.fk_havuz='778' ";
	SET @q=CONCAT(@q,"  and t.tarih>='",sdate," 00:00:00' and t.tarih<='",fdate," 23:59:59' ");
	SET @q=CONCAT(@q," limit ",skip,",",take);
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rptTaseronHavuzCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptTaseronHavuzCount`;
delimiter ;;
CREATE PROCEDURE `rptTaseronHavuzCount`(IN sdate TEXT, 
IN fdate TEXT,
IN userId INT,
IN yetki TEXT)
BEGIN

  SET @q = "COUNT(*) total FROM SMDR_1002.task_DB t
left JOIN SMDR_1002.crm_KartDB k on k.id=t.fk_kart
WHERE t.fk_havuz='778' ";
	SET @q=CONCAT(@q,"  and t.tarih>='",sdate," 00:00:00' and t.tarih<='",fdate," 23:59:59' ");
	
	-- select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rp_tHavuzBildirimSure
-- ----------------------------
DROP PROCEDURE IF EXISTS `rp_tHavuzBildirimSure`;
delimiter ;;
CREATE PROCEDURE `rp_tHavuzBildirimSure`(IN `_AGENT` INT, IN `_ILKTAR` DATETIME, IN `_SONTAR` DATETIME)
  NO SQL 
SELECT 

		g.id gorevid,
        g.ukey,g.tarih,
        date_format(g.tarih,'%d.%m.%Y') tarih,
        concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		ifnull(su1.adi,'') yapacak_agent,
        
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		gs.adi statu,
		ifnull(il.adi,'') iladi,
		gs.ncls ,

		g.tarih fulldate,
		tip.adi tip,
		
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		
		kart.tckimlik tc,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(hv.adi,'') havuzadi,
        ifnull(
		TIMESTAMPDIFF(HOUR,(select tarih from SMDR_1002.Bildirim_Detay where fk_gorev=g.id and sistem='2' order by id desc limit 1),NOW()),0) havuzsure,
		g.*
		from  SMDR_1002.Bildirim g		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent	
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.Vatandas kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
		
		where   g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = _AGENT)
 	-- and tarih>=_ILKTAR and tarih<=_SONTAR
		
        order by g.id desc 
        limit 1000
;;
delimiter ;

-- ----------------------------
-- Procedure structure for rp_tKurumSubeHavuz_KY
-- ----------------------------
DROP PROCEDURE IF EXISTS `rp_tKurumSubeHavuz_KY`;
delimiter ;;
CREATE PROCEDURE `rp_tKurumSubeHavuz_KY`(IN `_AGENT` INT, IN `_ILKTAR` DATETIME, IN `_SONTAR` DATETIME)
  NO SQL 
select h.*,s.adi sube,i.adi il,
(select count(*) from SMDR_1002.Bildirim where fk_gorev_statu=99 and fk_part =10 and fk_havuz=h.id and tarih>=_ILKTAR and tarih<=_SONTAR) kapanan,

(select count(*) from SMDR_1002.Bildirim where fk_gorev_statu=7 and fk_part =7 and fk_havuz=h.id and tarih>=_ILKTAR and tarih<=_SONTAR) guncelleme_talebi,

(select count(*) from SMDR_1002.Bildirim where fk_gorev_statu=1 and fk_part =1 and fk_havuz=h.id and tarih>=_ILKTAR and tarih<=_SONTAR) birime_atandi,

(select count(*) from SMDR_1002.Bildirim where fk_gorev_statu=2 and fk_part =2 and fk_havuz=h.id and tarih>=_ILKTAR and tarih<=_SONTAR) inceleniyor,

(select count(*) from SMDR_1002.Bildirim where fk_gorev_statu=3 and fk_part =3 and fk_havuz=h.id and tarih>=_ILKTAR and tarih<=_SONTAR) inceleniyor_eksure,

(select count(*) from SMDR_1002.Bildirim where fk_agent=0 and fk_part =0 and fk_havuz=h.id and tarih>=_ILKTAR and tarih<=_SONTAR) islem_yapilmamis,

(select count(*) from SMDR_1002.Bildirim where fk_gorev_statu=5 and fk_part =5 and fk_havuz=h.id and tarih>=_ILKTAR and tarih<=_SONTAR) reddedildi,

(select count(*) from SMDR_1002.Bildirim where fk_havuz=h.id and tarih>=_SONTAR and tarih<=_ILKTAR) toplam,

(select count(*) from SMDR_1002.Bildirim where fk_gorev_statu=99 and fk_part =10 and bitis_tar<kapanma_tarihi and fk_havuz=h.id and bitis_tar>'0000-00-00 00:00:00' and tarih>=_ILKTAR and tarih<=_SONTAR) zamaninda_kapatilmayan,

(select count(*) from SMDR_1002.Bildirim where fk_gorev_statu=99 and fk_part =10 and bitis_tar>kapanma_tarihi and fk_havuz=h.id and bitis_tar>'0000-00-00 00:00:00' and tarih>=_ILKTAR and tarih<=_SONTAR) zamaninda_kapanan

from SMDR_1002.task_havuzlar h
left join SMDR_1002.crm_SubeDB s on s.id=h.fk_sube
left join SMDR_1002.bild_tnm_il i on i.id=h.fk_il
where 1=1 and h.fk_il=34
and h.id in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = _AGENT)
;;
delimiter ;

-- ----------------------------
-- Function structure for SEC_TO_TIME2
-- ----------------------------
DROP FUNCTION IF EXISTS `SEC_TO_TIME2`;
delimiter ;;
CREATE FUNCTION `SEC_TO_TIME2`(in_seconds INT)
 RETURNS varchar(16) CHARSET utf8
  NO SQL 
BEGIN
  DECLARE v_hour INT;
  DECLARE v_minute INT;
  DECLARE s_hour VARCHAR(10);
  DECLARE s_minute VARCHAR(2);
  DECLARE s_second VARCHAR(2);

  SET v_hour = FLOOR(in_seconds / 60 / 60);
  SET in_seconds = in_seconds - (v_hour * 60 * 60);

  SET v_minute = FLOOR(in_seconds / 60);
  SET in_seconds = in_seconds - (v_minute*60);

  SET s_hour = IF (v_hour < 10,LPAD(v_hour,2,'0'),v_hour);
  SET s_minute = IF (v_minute < 10,LPAD(v_minute,2,'0'),v_minute);
  SET s_second = IF (in_seconds < 10,LPAD(in_seconds,2,'0'),in_seconds);

  RETURN CONCAT(s_hour,':',s_minute,':',s_second);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for setWebIcerik
-- ----------------------------
DROP PROCEDURE IF EXISTS `setWebIcerik`;
delimiter ;;
CREATE PROCEDURE `setWebIcerik`(IN `_ID` INT, IN `_BASLIK` VARCHAR(100), IN `_KACIKLAMA` VARCHAR(500), IN `_ICERIK` TEXT, IN `_RESIM` VARCHAR(100), IN `_URL` VARCHAR(50))
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.web_content
SET baslik = _BASLIK , kisa_aciklama = _KACIKLAMA , icerik = _ICERIK ,
resim = _RESIM , url = _URL ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.web_content
SET baslik = _BASLIK , kisa_aciklama = _KACIKLAMA , icerik = _ICERIK ,
resim = _RESIM , url = _URL 
where id = _ID

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spActigimBildirimler
-- ----------------------------
DROP PROCEDURE IF EXISTS `spActigimBildirimler`;
delimiter ;;
CREATE PROCEDURE `spActigimBildirimler`(IN `AGENT` INT)
BEGIN

SELECT 

		g.id gorevid,
        g.ukey,
        date_format(g.tarih,'%d.%m.%Y') tarih,
        concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		ifnull(su1.adi,'') yapacak_agent,
              
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,		
		gs.adi statu,
		ifnull(il.adi,'') iladi,        
		gs.ncls ,
		g.tarih fulldate,
		tip.adi tip,
		
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		
		kart.tckimlik tc,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(hv.adi,'') havuzadi,
        ifnull(
		TIMESTAMPDIFF(HOUR,(select tarih from SMDR_1002.Bildirim_Detay where fk_gorev=g.id and sistem='2' order by id desc limit 1),NOW()),0) havuzsure,
		g.*
		from  SMDR_1002.Bildirim g		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent	
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.Vatandas kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
		
		where g.kayit_tipi = 1  and g.fk_kaydi_acan= AGENT 
        -- and g.tarih >  date_add(CURRENT_DATE, INTERVAL -30 DAY)
		order by g.id desc 
        limit 150
        ;
		
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spAddCrmIslemSureleri
-- ----------------------------
DROP PROCEDURE IF EXISTS `spAddCrmIslemSureleri`;
delimiter ;;
CREATE PROCEDURE `spAddCrmIslemSureleri`(IN _fk_gorev INT, 
IN _fk_agent INT,
IN _sistem INT, 
IN _fk_detay_islem INT, 
IN _kurum INT, 
IN _kurum1 INT, 
IN _kurum2 INT, 
IN _fk_sube INT, 
IN _fk_havuz INT, 
IN _fk_il INT, 
IN _tarih TEXT)
BEGIN
DECLARE islemSureID INT;
SET NAMES UTF8;
SET @sqlIslemSure="INSERT INTO SMDR_1002.crm_islem_sureleri SET ";
SET @sqlIslemSure=CONCAT(@sqlIslemSure," fk_gorev='" , _fk_gorev , "', fk_agent='" , _fk_agent ,"', sistem='",_sistem,"', fk_detay_islem='",_fk_detay_islem,"', tip_level1='", _kurum,"',");
SET @sqlIslemSure=CONCAT(@sqlIslemSure," tip_level2='", _kurum1, "', tip_level3='", _kurum2, "', fk_sube='", _fk_sube,"', fk_havuz='", _fk_havuz,"', fk_il='", _fk_il,"', tar='",CURDATE(),"',");
SET @sqlIslemSure=CONCAT(@sqlIslemSure," sure='",TIMESTAMPDIFF(SECOND,_tarih,NOW()),"'");
#select @sqlIslemSure;
PREPARE stmtIslemSure FROM @sqlIslemSure;
EXECUTE stmtIslemSure;
SET islemSureID= LAST_INSERT_ID();
select islemSureID as id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spAddSaphiraUsersLog
-- ----------------------------
DROP PROCEDURE IF EXISTS `spAddSaphiraUsersLog`;
delimiter ;;
CREATE PROCEDURE `spAddSaphiraUsersLog`(IN _fk_user INT,IN _fk_islem_yapan INT,IN _aciklama TEXT)
BEGIN
SET NAMES UTF8;
SET @sql="INSERT IGNORE INTO SMDR_1002.saphira_users_log ";
SET @sql=CONCAT(@sql," SET fk_agent='",_fk_user,"',");
SET @sql=CONCAT(@sql," fk_islem_yapan='",_fk_islem_yapan,"',");
SET @sql=CONCAT(@sql," aciklama='",_aciklama,"',");
SET @sql=CONCAT(@sql," islem='1'");
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spAddSmsListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spAddSmsListe`;
delimiter ;;
CREATE PROCEDURE `spAddSmsListe`(IN _fk_gorev INT, IN _gsm TEXT, IN _mesaj TEXT)
BEGIN
SET NAMES UTF8;
SET @sql="INSERT IGNORE INTO SMDR_1002.sms_liste SET ";
SET @sql=CONCAT(@sql," fk_gorev='",_fk_gorev,"',numara='",_gsm,"',mesaj='",_mesaj,"'");
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spAddTaskDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spAddTaskDetay`;
delimiter ;;
CREATE PROCEDURE `spAddTaskDetay`(IN _sistem INT, IN _ID INT, IN _userId INT, IN _aciklama TEXT, IN _ipAdres TEXT,IN _fk_detay_tip INT)
BEGIN
DECLARE taskDetayID INT;
SET NAMES UTF8;
SET @sqlTaskDetay="INSERT INTO SMDR_1002.task_DBDetay SET ";
SET @sqlTaskDetay=CONCAT(@sqlTaskDetay," sistem='",_sistem,"',fk_gorev='",_ID,"',fk_agent='",_userId,"',aciklama='",_aciklama,"'");
IF LENGTH(_ipAdres)>7 THEN
	SET @sqlTaskDetay=CONCAT(@sqlTaskDetay,",ipadres='",_ipAdres,"'");
END IF;
IF(_fk_detay_tip<100) THEN
	SET @sqlTaskDetay=CONCAT(@sqlTaskDetay,",fk_detay_tip='",_fk_detay_tip,"'");
END IF;
PREPARE stmtTaskDetay FROM @sqlTaskDetay;
EXECUTE stmtTaskDetay;
SET taskDetayID= LAST_INSERT_ID();
select taskDetayID as id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spAgentHangup
-- ----------------------------
DROP PROCEDURE IF EXISTS `spAgentHangup`;
delimiter ;;
CREATE PROCEDURE `spAgentHangup`(IN sdate TEXT,IN fdate TEXT, IN lokasyon INT,IN tablo TEXT)
BEGIN
SET NAMES UTF8;
SET @q="SELECT que.*,p.caller_id, p.ext, s.adi, COUNT(*) SAY FROM pbx.";SET @q=CONCAT(@q,tablo," que
			left join SMDR_1002.pbx_que_log p on p.uniqid=que.callid
			left join SMDR_1002.saphira_users s on s.ext=p.ext
			WHERE que.data2<=9 ");
SET @q=CONCAT(@q," and que.event='COMPLETEAGENT' and que.time>='",sdate," 00:00:00' and que.time<='",fdate," 23:59:59' and s.fk_lokasyon='",lokasyon,"'"); 
SET @q=CONCAT(@q," 			GROUP BY p.ext			ORDER by p.ext ASC, que.time ASC");
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spAgentHangupDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spAgentHangupDetay`;
delimiter ;;
CREATE PROCEDURE `spAgentHangupDetay`(IN sdate TEXT,IN fdate TEXT, IN lokasyon INT,IN tablo TEXT,In ext TEXT)
BEGIN
SET NAMES UTF8;
SET @q="SELECT que.*,p.caller_id, p.ext, s.adi, COUNT(*) SAY FROM pbx.";
SET @q=CONCAT(@q,tablo," que
			left join SMDR_1002.pbx_que_log p on p.uniqid=que.callid
			left join SMDR_1002.saphira_users s on s.ext=p.ext
			WHERE que.data2<=6 ");
SET @q=CONCAT(@q," and que.event='COMPLETEAGENT' and que.time>='",sdate," 00:00:00' and que.time<='",fdate," 23:59:59' and s.fk_lokasyon='",lokasyon,"' and p.ext='",ext,"'"); 
SET @q=CONCAT(@q," 	ORDER by p.ext ASC, que.time ASC");
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildirimAra
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildirimAra`;
delimiter ;;
CREATE PROCEDURE `spBildirimAra`(IN _skip INT, 
IN _take INT, 
IN `ADI` VARCHAR(50), 
IN `SOYADI` VARCHAR(50), 
IN `TC` VARCHAR(50), 
IN `TEL` VARCHAR(50), 
IN `TASK` INT, 
IN `STATU` INT)
BEGIN
	
    SET @q = "SELECT g.id gorevid,        
    date_format(g.tarih,'%d.%m.%Y') tarih,        
    concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,ifnull(su1.adi,'') yapacak_agent,
    g.tip_level1,g.tip_level2,        
    g.tip_level3,                
    su.adi acan_agent,k1.adi konu1,k2.adi konu2,k3.adi konu3,
    gs.adi statu,ifnull(il.adi,'') iladi,        
    gs.ncls ,g.tarih fulldate,tip.adi tip,
    date_format(g.tarih,'%H:%i') saat,
    TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
    date_format(current_date,'%d.%m.%Y %H:%i') bugun,
    kart.tckimlik tc,ifnull(il.adi,'') iladi,
    ifnull(ilce.adi,'') ilceadi,ifnull(hv.adi,'') havuzadi,        
    ifnull(TIMESTAMPDIFF(HOUR,(select tarih 
    from SMDR_1002.task_DBDetay
    where fk_gorev=g.id and sistem='2' order by id desc limit 1),
    NOW()),0) havuzsure,g.*from  
    SMDR_1002.task_DB g left 
    join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu 
    left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan 
    left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent 
    left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip 
    LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1 
    LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2 
    LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3 
    left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart 
    LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il 
    LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce 
    left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
    where g.kayit_tipi = 1 ";
    
    if (STATU <> 9999) THEN
    	set @q = concat(@q, ' and fk_gorev_statu = ', STATU);
    end if;

    
    IF (ADI <> '') THEN
    set @q = concat(@q, " and kart.adi = '", ADI, "'" );
    END IF;
    
    IF (ADI <> '') THEN
    set @q = concat(@q, " and kart.soyadi = '", SOYADI , "'");
    END IF;
    
    IF (TC <> '') THEN
        set @q = concat(@q, " and kart.tckimlik = '", TC , "'");
    END IF;
    
    IF (TEL <> '') THEN
        set @q = concat(@q, " and ( kart.gsm = '", TEL , "' OR  kart.tel1 = '", TEL , "' or kart.tel2='" , TEL , "')");
    END IF;
    
    IF (TASK <> 0 ) THEN
        set @q = concat(@q, ' and g.id = ',TASK);
    END IF;
    
    
    set @q = concat(@q, ' order by g.id desc ');
    set @q = concat(@q, ' limit ',_skip,",",_take);
PREPARE stmt FROM @q;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildirimAraCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildirimAraCount`;
delimiter ;;
CREATE PROCEDURE `spBildirimAraCount`(IN `ADI` VARCHAR(50), IN `SOYADI` VARCHAR(50), IN `TC` VARCHAR(50), IN `TEL` VARCHAR(50), IN `TASK` INT, IN `STATU` INT)
BEGIN
	
    SET @q = "SELECT count(*) total from  
    SMDR_1002.task_DB g 
    left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu 
    left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan 
    left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent 
    left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip 
    LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1 
    LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2 
    LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3 
    left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart 
    LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il 
    LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce 
    left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz 
    where g.kayit_tipi = 1 ";
    
    if (STATU <> 9999) THEN
    	set @q = concat(@q, ' and fk_gorev_statu = ', STATU);
    end if;

    
    IF (ADI <> '') THEN
    set @q = concat(@q, " and kart.adi = '", ADI, "'" );
    END IF;
    
    IF (ADI <> '') THEN
    set @q = concat(@q, " and kart.soyadi = '", SOYADI , "'");
    END IF;
    
    IF (TC <> '') THEN
        set @q = concat(@q, " and kart.tckimlik = '", TC , "'");
    END IF;
    
    IF (TEL <> '') THEN
        set @q = concat(@q, " and ( kart.gsm = '", TEL , "' OR  kart.tel1 = '", TEL , "' or kart.tel2='" , TEL , "')");
    END IF;
    
    IF (TASK <> 0 ) THEN
        set @q = concat(@q, ' and g.id = ',TASK);
    END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildirimKapat
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildirimKapat`;
delimiter ;;
CREATE PROCEDURE `spBildirimKapat`(IN _id INT,
IN _fk_statu INT,
IN _fk_agent INT,
IN _fk_taskb_kapanma_statu INT,
IN _kurumYetki TEXT,
IN _aciklama TEXT)
  NO SQL 
BEGIN
 SET @sql="UPDATE SMDR_1002.task_DB SET fk_gorev_statu='99' ";
 SET @sql=CONCAT(@sql," ,fk_kapatan_agent='",_fk_agent,"',kapanma_tarihi='",NOW(),"',fk_taskb_kapanma_statu='",_fk_taskb_kapanma_statu,"'");
 IF(_fk_statu=1) THEN
		SET @sql=CONCAT(@sql," ,fk_statu='2'");
 END IF;
 IF(_kurumYetki="MU") THEN
		SET @sql=CONCAT(@sql," ,MU_aciklama='",_aciklama,"'");
 ELSEIF(_kurumYetki="KU") THEN		
		SET @sql=CONCAT(@sql," ,KU_aciklama='",_aciklama,"'");
 ELSEIF(_kurumYetki="GD") THEN		
		SET @sql=CONCAT(@sql," ,GD_aciklama='",_aciklama,"'");		
 END IF;
 SET @sql=CONCAT(@sql," WHERE id=",_id);
 -- SET @sql=CONCAT(@sql,_id,"-",_fk_statu,"-",_fk_agent,"-",_fk_taskb_kapanma_statu,"-",_kurumYetki,"-",_aciklama);
 PREPARE stmt FROM @sql;
 EXECUTE stmt;
 -- select @sql;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildirimScript
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildirimScript`;
delimiter ;;
CREATE PROCEDURE `spBildirimScript`(IN `_TERM` varchar(175), IN `_SKIP` int, IN `_TAKE` int)
BEGIN

	SET @q="select 
	s.*,
	s.script skAdi,
	kn.adi knadi,
	tkn.adi konu,
	takn.adi alt_konu,
	taknd.adi alt_konu_detay
	from SMDR_1002.bildirim_scriptleri s
	left join SMDR_1002.bildirim_script_konu kn on kn.id=s.fk_parent
	left join SMDR_1002.task_konu tkn on tkn.id=s.tip_level1
	left join SMDR_1002.task_alt_konu takn on takn.id=s.tip_level2
	left join SMDR_1002.task_alt_konu_detay taknd on taknd.id=s.tip_level3
	where 1=1 and s.isActive=1";
	
	IF(LENGTH(_TERM)>2) THEN
		SET @q=CONCAT(@q," and s.script like '",_TERM,"%'");
	END IF;
	
	SET @q=CONCAT(@q," order by s.id");
	
	SET @q=CONCAT(@q," LIMIT ",_SKIP,",",_TAKE);
	#select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildirimScriptCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildirimScriptCount`;
delimiter ;;
CREATE PROCEDURE `spBildirimScriptCount`(IN `_TERM` varchar(175))
BEGIN

	SET @q="select count(*) total from SMDR_1002.bildirim_scriptleri s
	where 1=1 and s.isActive=1";
	
	IF(LENGTH(_TERM)>2) THEN
		SET @q=CONCAT(@q," and s.script like '",_TERM,"%'");
	END IF;

	#select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildirimYenidenAc
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildirimYenidenAc`;
delimiter ;;
CREATE PROCEDURE `spBildirimYenidenAc`(IN _ID INT, IN _fk_lokasyon INT)
BEGIN
SET NAMES UTF8;
SET @sql="UPDATE SMDR_1002.task_DB SET fk_gorev_statu='20', fk_agent='0', fk_kapatan_agent='0',fk_havuz='0', kapanma_tarihi='0000-00-00 00:00:00',";
SET @sql=CONCAT(@sql,"yeniden_acilma_tar=NOW(),ya_tar=NOW(),isReopen=isReopen+1,fk_lokasyon='",_fk_lokasyon,"' WHERE id=",_id);
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildListMobil
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildListMobil`;
delimiter ;;
CREATE PROCEDURE `spBildListMobil`(IN _skip INT, IN _take INT)
BEGIN
		select
		g.*,
        g.id gorevid,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		
		gs.adi statu,
		gs.color ncls,
		
		ifnull(su1.adi,'') yapacak_agent,
        ifnull(h.adi,'') havuzadi,
		g.tarih fulldate,
		tip.adi tip,
        
		date_format(g.tarih,'%d.%m.%Y') tarih,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		TIMESTAMPDIFF(HOUR,g.bitis_tar,NOW()) havuzsure,
		kart.adi musteri,
		kart.soyadi musteris,
        CONCAT(kart.adi," ",kart.soyadi) basvuru_sahibi,
		kart.tckimlik tc,
		il.adi iladi,
		ilce.adi ilceadi,
		lok.adi lokasyon		
		from SMDR_1002.task_DB g
		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon
        left join SMDR_1002.task_havuzlar h on h.id=g.fk_havuz
		where 1=1 
        and g.fk_gorev_statu < 99 and g.fk_gorev_statu <>5    and g.fk_gorev_statu<>98  and g.tarih > '2015-07-16 00:00:00' and g.fk_parent=10 
        order by g.id desc
        limit _skip,_take;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildListMobilCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildListMobilCount`;
delimiter ;;
CREATE PROCEDURE `spBildListMobilCount`()
BEGIN
		select
		COUNT(*) total 
		from SMDR_1002.task_DB g
		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon
        left join SMDR_1002.task_havuzlar h on h.id=g.fk_havuz
		where 1=1 
        and g.fk_gorev_statu < 99 and g.fk_gorev_statu <>5    and g.fk_gorev_statu<>98  and g.tarih > '2015-07-16 00:00:00' and g.fk_parent=10;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildListNoHavuz
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildListNoHavuz`;
delimiter ;;
CREATE PROCEDURE `spBildListNoHavuz`(IN _skip INT, IN _take INT)
BEGIN
		select
		g.*,
        g.id gorevid,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		
		gs.adi statu,
		gs.color ncls,
		
		ifnull(su1.adi,'') yapacak_agent,
        ifnull(h.adi,'') havuzadi,
		g.tarih fulldate,
		tip.adi tip,
        
		date_format(g.tarih,'%d.%m.%Y') tarih,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		TIMESTAMPDIFF(HOUR,g.bitis_tar,NOW()) havuzsure,
		kart.adi musteri,
		kart.soyadi musteris,
        CONCAT(kart.adi," ",kart.soyadi) basvuru_sahibi,
		kart.tckimlik tc,
		il.adi iladi,
		ilce.adi ilceadi,
		lok.adi lokasyon		
		from SMDR_1002.task_DB g
		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon
        left join SMDR_1002.task_havuzlar h on h.id=g.fk_havuz
		where g.kayit_tipi = 1
        and g.fk_gorev_statu < 99 and g.fk_gorev_statu<>5 and g.fk_gorev_statu<>98  #and g.fk_kaydi_acan<>92
        order by g.id desc
        limit _skip,_take;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildListNoHavuzCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildListNoHavuzCount`;
delimiter ;;
CREATE PROCEDURE `spBildListNoHavuzCount`()
BEGIN
		select
		count(*) as total
		from SMDR_1002.task_DB g
		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon
		where g.kayit_tipi = 1
        and g.fk_gorev_statu < 99 and g.fk_gorev_statu<>5 and g.fk_gorev_statu<>98;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildListWebSitesi
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildListWebSitesi`;
delimiter ;;
CREATE PROCEDURE `spBildListWebSitesi`(IN _skip INT, IN _take INT,IN _IDS TEXT)
BEGIN
SET @sql="select
		g.*,
        g.id gorevid,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		
		gs.adi statu,
		gs.color ncls,
		
		ifnull(su1.adi,'') yapacak_agent,
     ifnull(h.adi,'') havuzadi,
		g.tarih fulldate,
		tip.adi tip,
        
		date_format(g.tarih,'%d.%m.%Y') tarih,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		TIMESTAMPDIFF(HOUR,g.bitis_tar,NOW()) havuzsure,
		kart.adi musteri,
		kart.soyadi musteris,
    CONCAT(kart.adi,' ',kart.soyadi) basvuru_sahibi,
		kart.tckimlik tc,
		il.adi iladi,
		ilce.adi ilceadi,
		lok.adi lokasyon		
		from SMDR_1002.task_DB g
		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon
        left join SMDR_1002.task_havuzlar h on h.id=g.fk_havuz
		where g.kayit_tipi = 1
		 and g.fk_gorev_statu < 99 and g.fk_gorev_statu<>5 and g.fk_gorev_statu<>98 and g.fk_gorev_statu < 99 and g.fk_gorev_statu<>5 and g.fk_gorev_statu<>98 and g.fk_parent=0 and g.fk_gorev_statu!=1 ";
			SET @sql=CONCAT(@sql," AND g.fk_kaydi_acan IN (",_IDS,")");
			SET @sql=CONCAT(@sql," order by g.id desc limit ",_skip,",",_take);
			PREPARE stmt FROM @sql;
			EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spBildListWebSitesiCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spBildListWebSitesiCount`;
delimiter ;;
CREATE PROCEDURE `spBildListWebSitesiCount`(IN _IDS TEXT)
BEGIN
		select
		count(*) as total
		from SMDR_1002.task_DB g
		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon
		where g.kayit_tipi = 1
		 and g.fk_gorev_statu < 99 and g.fk_gorev_statu<>5 and g.fk_gorev_statu<>98 and g.fk_gorev_statu < 99 and g.fk_gorev_statu<>5 and g.fk_gorev_statu<>98 and g.fk_parent=0 and g.fk_gorev_statu!=1 and ( g.fk_kaydi_acan IN(_IDS));
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spClearCrmRolUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `spClearCrmRolUser`;
delimiter ;;
CREATE PROCEDURE `spClearCrmRolUser`(IN fk_user INT)
BEGIN
SET NAMES UTF8;
SET @sql=CONCAT("DELETE FROM SMDR_1002.crm_rol_user WHERE fk_user=",fk_user);
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetAktifPasifList
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetAktifPasifList`;
delimiter ;;
CREATE PROCEDURE `spGetAktifPasifList`(IN tip INT)
  NO SQL 
BEGIN

SET @q="SELECT * FROM SMDR_1002.tnm_aktifpasif WHERE 1=1 ";
IF (tip>0) THEN
SET @q=CONCAT(@q," AND tip='",tip,"'");
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetCmbDetayWhere
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetCmbDetayWhere`;
delimiter ;;
CREATE PROCEDURE `spGetCmbDetayWhere`(IN tbl TEXT,IN whr TEXT)
  NO SQL 
BEGIN

SET @q=CONCAT("SELECT * FROM SMDR_1002.",tbl," where 1=1 ",whr);
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetCrmRolListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetCrmRolListe`;
delimiter ;;
CREATE PROCEDURE `spGetCrmRolListe`(IN _ID INT,IN _isSysAdmin INT,IN _isActive INT,IN rolLevel INT,IN userId INT)
BEGIN
SET NAMES UTF8;
SET @sql=CONCAT("SELECT r.* ");

-- userid var ise kullanıcının yonetecegi rolleride sorguya ekle
IF userId >0 THEN
	SET @sql=CONCAT(@sql," ,ur.fk_rol,ur.fk_user  FROM SMDR_1002.crm_rol r ");
	SET @sql=CONCAT(@sql," left join SMDR_1002.crm_rol_user ur on ur.fk_rol=r.id and ur.fk_user=",userId);
ELSE
	SET @sql=CONCAT(@sql,"  FROM SMDR_1002.crm_rol r ");
END IF;

SET @sql=CONCAT(@sql," WHERE 1=1 ");
IF _ID>0 THEN
	SET @sql=CONCAT(@sql," and r.id=",_ID); 
ELSE
	IF _isActive<2 THEN
		SET @sql=CONCAT(@sql," and r.isActive=",_isActive);
    END IF;
END IF;

IF _isSysAdmin=0 THEN
	SET @sql=CONCAT(@sql," and r.id<>9999 ");
END IF;

SET @sql=CONCAT(@sql," order by r.id asc");
 -- select @sql;
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetCrmRolListeUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetCrmRolListeUser`;
delimiter ;;
CREATE PROCEDURE `spGetCrmRolListeUser`(IN userId INT)
BEGIN
SET NAMES UTF8;

select ur.fk_rol,ur.fk_user,r.* from SMDR_1002.crm_rol_user ur left join SMDR_1002.crm_rol r on r.id=ur.fk_rol where ur.fk_user=userId;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetKaraListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetKaraListe`;
delimiter ;;
CREATE PROCEDURE `spGetKaraListe`(IN id INT,
IN isOnay INT,
IN skip INT,
IN take INT)
  NO SQL 
BEGIN
IF(id>0) THEN
		SET @q=CONCAT("select p.*, u.adi ekleyen,o.adi onayveren,
		date_format(p.tarih,'%d.%m.%Y %H:%i') ilktarih,
		date_format(p.sure,'%d.%m.%Y %H:%i')  sontarih,
		if(p.isOnay>0,'Onaylanmış','Onay Bekliyor') durum
from SMDR_1002.pbx_blacklist p
		left join SMDR_1002.saphira_users u on u.id = p.fk_agent_id
		left join SMDR_1002.saphira_users o on o.id = p.fk_onay_veren
		where p.id=",id);
ELSE
SET @q=CONCAT("select p.*, u.adi ekleyen,o.adi onayveren,
		date_format(p.tarih,'%d.%m.%Y %H:%i') ilktarih,
		date_format(p.sure,'%d.%m.%Y %H:%i')  sontarih,
		if(p.isOnay>0,'Onaylanmış','Onay Bekliyor') durum
from SMDR_1002.pbx_blacklist p
		left join SMDR_1002.saphira_users u on u.id = p.fk_agent_id
		left join SMDR_1002.saphira_users o on o.id = p.fk_onay_veren
		where p.isOnay=",isOnay," limit ",skip,",",take);
END IF;


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetKaraListeCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetKaraListeCount`;
delimiter ;;
CREATE PROCEDURE `spGetKaraListeCount`(IN isOnay INT)
  NO SQL 
BEGIN
SET @q=CONCAT("select Count(*) total from SMDR_1002.pbx_blacklist p
		left join SMDR_1002.saphira_users u on u.id = p.fk_agent_id
		left join SMDR_1002.saphira_users o on o.id = p.fk_onay_veren
		where p.isOnay=",isOnay);

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetKart
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetKart`;
delimiter ;;
CREATE PROCEDURE `spGetKart`(IN `KARTID` INT)
  NO SQL 
select
		k.id kartid,
        k.ukey ukey,
		concat(k.adi , ' ' , k.soyadi) kartadi,
        ifnull(k.tckimlik,'') tckimlik, 
        ifnull(k.tel1,'') tel1,
        ifnull(k.tel2,'') tel2,
        ifnull(k.gsm,'') gsm,
        ifnull(k.email,'') email,
        ifnull(k.fax,'') fax,
        ifnull(k.dt,'') dogumtarihi,
		ifnull(ulk.adi,'') ulkeadi,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(ed.adi,'') edurum,
		ifnull(cins.adi,'') cinsiyet,
		ifnull(gsoru.adi,'') gsoruadi,
        ifnull(k.sskno,'') sskno
		from SMDR_1002.crm_KartDB k
		left join SMDR_1002.bild_tnm_ulke ulk on ulk.id=k.fk_ulke
		left join SMDR_1002.bild_tnm_il il on il.id=k.fk_il
		left join SMDR_1002.bild_tnm_ilce ilce on ilce.id=k.fk_ilce 
		left join SMDR_1002.bild_tnm_egitim_durumu ed on ed.id=k.fk_egitimdurumu
		left join SMDR_1002.bild_tnm_cinsiyet cins on cins.id=k.fk_cinsiyet
		left join SMDR_1002.bild_tnm_guvenliksoru gsoru on gsoru.id=k.fk_gsoru
		where 1=1
        and k.id = KARTID
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetKartByTc
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetKartByTc`;
delimiter ;;
CREATE PROCEDURE `spGetKartByTc`(IN `TC` VARCHAR(50))
  NO SQL 
select
		k.fk_il,
        k.fk_ilce,
		ifnull(k.sskno,'') sskno,
		k.fk_cinsiyet,
		k.id kartid,
        k.ukey ukey,
        k.fk_egitimdurumu,
		concat(k.adi , ' ' , k.soyadi) kartadisoyadi,
        ifnull(k.adi,'') kartadi,
        ifnull(k.soyadi,'') kartsoyadi,
        ifnull(k.tckimlik,'') tckimlik, 
        ifnull(k.tel1,'') tel1,
        ifnull(k.tel2,'') tel2,
        ifnull(k.gsm,'') gsm,
        ifnull(k.email,'') email,
        ifnull(k.fax,'') fax,
        ifnull(k.dt,'') dogumtarihi,
		ifnull(ulk.adi,'') ulkeadi,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(ed.adi,'') edurum,
		ifnull(cins.adi,'') cinsiyet,
		ifnull(gsoru.adi,'') gsoruadi
		from SMDR_1002.crm_KartDB k
		left join SMDR_1002.bild_tnm_ulke ulk on ulk.id=k.fk_ulke
		left join SMDR_1002.bild_tnm_il il on il.id=k.fk_il
		left join SMDR_1002.bild_tnm_ilce ilce on ilce.id=k.fk_ilce 
		left join SMDR_1002.bild_tnm_egitim_durumu ed on ed.id=k.fk_egitimdurumu
		left join SMDR_1002.bild_tnm_cinsiyet cins on cins.id=k.fk_cinsiyet
		left join SMDR_1002.bild_tnm_guvenliksoru gsoru on gsoru.id=k.fk_gsoru
		where 1=1
        and k.tckimlik = TC
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetLokasyon
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetLokasyon`;
delimiter ;;
CREATE PROCEDURE `spGetLokasyon`()
BEGIN
	SELECT *,adi as name FROM SMDR_1002.crm_lokasyon;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetLokasyonById
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetLokasyonById`;
delimiter ;;
CREATE PROCEDURE `spGetLokasyonById`(IN _id INT)
BEGIN
	SELECT * FROM SMDR_1002.crm_lokasyon where id=_id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetLokasyonCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetLokasyonCount`;
delimiter ;;
CREATE PROCEDURE `spGetLokasyonCount`()
BEGIN
	SELECT COUNT(*) total FROM SMDR_1002.crm_lokasyon;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetSss
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetSss`;
delimiter ;;
CREATE PROCEDURE `spGetSss`(IN id INT,
IN statu INT,
IN fk_sss_grup INT,
IN fk_sss_altgrup INT,
IN baslik TEXT,
IN skip INT,
IN take INT)
  NO SQL 
BEGIN

SET @q="select sss.*,date_format(sss.tarih,'%d.%m.%Y %H:%i') ssstarih, u.adi ekleyen from SMDR_1002.crm_SSS sss
 left join SMDR_1002.saphira_users u on u.id=sss.fk_agent
 where 1=1 ";
 
IF(id>0) THEN
	SET @q=CONCAT(@q," and sss.id=",id);
ELSE
	IF(statu<2) THEN
		SET @q=CONCAT(@q," and sss.isActive=",statu);
	ELSE	
		SET @q=CONCAT(@q," and sss.isActive=0");
	END IF;
	IF(fk_sss_grup>0) THEN
		SET @q=CONCAT(@q," and sss.fk_sss_grup=",fk_sss_grup);
	END IF;
	IF(fk_sss_altgrup>0) THEN
		SET @q=CONCAT(@q," and sss.fk_sss_altgrup=",fk_sss_altgrup);
	END IF;
	IF(LENGTH(baslik)>2) THEN
		SET @q=CONCAT(@q," and sss.baslik  like '%",baslik,"%'");
	END IF;
	SET @q=CONCAT(@q," limit ",skip,",",take);
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetSssAltKonu
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetSssAltKonu`;
delimiter ;;
CREATE PROCEDURE `spGetSssAltKonu`(IN fk_sss_grup INT)
  NO SQL 
BEGIN

SET @q="select * from SMDR_1002.crm_SSSAltGrup ";
IF(fk_sss_grup>0) THEN
	SET @q=CONCAT(@q," WHERE fk_parent=",fk_sss_grup);
END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetSssCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetSssCount`;
delimiter ;;
CREATE PROCEDURE `spGetSssCount`(IN statu INT,
IN fk_sss_grup INT,
IN fk_sss_altgrup INT,
IN baslik TEXT)
  NO SQL 
BEGIN

SET @q="select COUNT(*) total from SMDR_1002.crm_SSS sss
 left join SMDR_1002.saphira_users u on u.id=sss.fk_agent
 where 1=1 ";
 
IF(statu<2) THEN
	SET @q=CONCAT(@q," and sss.isActive=",statu);
ELSE	
	SET @q=CONCAT(@q," and sss.isActive=0");
END IF;
IF(fk_sss_grup>0) THEN
	SET @q=CONCAT(@q," and sss.fk_sss_grup=",fk_sss_grup);
END IF;
IF(fk_sss_altgrup>0) THEN
	SET @q=CONCAT(@q," and sss.fk_sss_altgrup=",fk_sss_altgrup);
END IF;
IF(LENGTH(baslik)>2) THEN
	SET @q=CONCAT(@q," and sss.baslik  like '%",baslik,"%'");
END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetSssKonu
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetSssKonu`;
delimiter ;;
CREATE PROCEDURE `spGetSssKonu`()
  NO SQL 
BEGIN

SET @q="select * from SMDR_1002.crm_SSSGrup order by lng desc ";
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetTask
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetTask`;
delimiter ;;
CREATE PROCEDURE `spGetTask`(IN `_ID` INT)
  NO SQL 
SELECT 
    tip.adi tipi,
    ifnull(k1.adi,'') konu1,
    ifnull(k2.adi,'') konu2,
    ifnull(k3.adi,'') konu3,
    ifnull(k4.adi,'') konu4,
    ifnull(k5.adi,'') konu5,
    ifnull(su.adi,'') kaydi_acan,
    ifnull(gd.adi,'') geri_donus_kanali,
	ifnull(hv.adi,'') havuzadi,
    ifnull(gs.adi,'') statu,
    ifnull(gs.class,'') statu_class,
    ifnull(su1.adi,'') yapacak_agent,
    ifnull(prj.adi,'') proje,
    ifnull(o.adi,'') oncelik,
    ifnull(o.class,'') oncelik_class,
    date_format(g.tarih,'%d.%m.%Y') tarih,
    date_format(g.tarih,'%H:%i') saat,
    TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
    date_format(current_date,'%d.%m.%Y %H:%i') bugun,
    
    ifnull(ase.adi,'') asebep,
    ifnull(g.aciklama,'') aciklama,
    ifnull(g.MU_aciklama,'') MU_aciklama,
    ifnull(g.KU_aciklama,'') KU_aciklama,
    ifnull(g.GD_aciklama,'') GD_aciklama,
    ifnull(g.fk_kart,0) fk_kart,
    g.id gorevid,
    g.fk_kaydi_acan acan_agent,
    g.*,
    ifnull(akt.adi,'') arama_kanali_turu,
    ifnull(ak.adi,'') arama_kanali,
		ifnull(fir.id,0) f_id,
		ifnull(fir.f_adi,'') f_adi,
		ifnull(fir.f_adres,'') f_adres,
		ifnull(fir.f_tel,'') f_tel,
		ifnull(fir.nace,'') nace,
		ifnull(fir.scs,'') scs,
		ifnull(fil.adi,'') f_il,
		ifnull(filce.adi,'') f_ilce,
        sb.id subeid,
				ifnull(sb.adi,'') subeadi
FROM SMDR_1002.task_DB g
    LEFT JOIN SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
    LEFT JOIN SMDR_1002.task_tipi tip ON tip.id = g.fk_tip
    LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
    LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
    LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
    LEFT JOIN SMDR_1002.task_konu k4 ON k4.id = g.tip_level4
    LEFT JOIN SMDR_1002.task_konu k5 ON k5.id = g.tip_level5
    left join SMDR_1002.task_Oncelik o ON o.id = g.fk_oncelik		    
    LEFT JOIN SMDR_1002.saphira_users su ON su.id = g.fk_kaydi_acan
    left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
    LEFT JOIN SMDR_1002.task_GeriDonusKanali gd on gd.id = g.fk_geri_donus_kanali
    left join SMDR_1002.bild_tnm_aramakanali ase on ase.id=g.fk_aramasebebi
    left join SMDR_1002.bild_tnm_aramakanali ak on ak.id=g.fk_aramakanali
	left join SMDR_1002.crm_ProjeDB prj on prj.id = g.fk_proje
	left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
	left join SMDR_1002.bild_tnm_aramakanali_tipi akt on akt.id=g.fk_aramakanali
	left join SMDR_1002.crm_kart_firma fir on fir.fk_gorev=g.id
	left join SMDR_1002.bild_tnm_il fil on fil.id=fir.f_fk_il
	left join SMDR_1002.bild_tnm_ilce filce on filce.id=fir.f_fk_ilce 
	left join SMDR_1002.crm_SubeDB sb on sb.id=hv.fk_sube
where  g.id= _ID
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetTaskbKapanmaStatu
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetTaskbKapanmaStatu`;
delimiter ;;
CREATE PROCEDURE `spGetTaskbKapanmaStatu`(IN _id INT)
  NO SQL 
BEGIN
	SET @sql="SELECT * FROM SMDR_1002.taskb_kapanma_statu WHERE isActive=1 ";
	IF(_id>0) THEN
	SET @sql=CONCAT(@sql," WHERE id=",_id);
	END IF;
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetTaskDBDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetTaskDBDetay`;
delimiter ;;
CREATE PROCEDURE `spGetTaskDBDetay`(IN sistem INT,
IN fkGorev INT,
IN fkAgent INT,
IN take INT,
IN id INT)
  NO SQL 
BEGIN
 SET @sql="select * from SMDR_1002.task_DBDetay where 1=1 ";
IF(id >0) THEN
	SET @sql=CONCAT(@sql," AND id=",id);
ELSE
	IF(sistem<100) THEN
		SET @sql=CONCAT(@sql," AND sistem=",sistem);
	END IF;
	IF(fkGorev>0) THEN
		SET @sql=CONCAT(@sql," AND fk_gorev=",fkGorev);
	END IF;
	IF(fkAgent>0) THEN
		SET @sql=CONCAT(@sql," AND fk_agent=",fkAgent);
	END IF;
	SET @sql=CONCAT(@sql," ORDER BY id DESC ");
	IF(take>0 AND take <1001) THEN
		SET @sql=CONCAT(@sql," limit ",take);
	ELSE
		SET @sql=CONCAT(@sql," limit 1");
	END IF;
END IF;
-- select @sql;
 PREPARE stmt FROM @sql;
 EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetTaskDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetTaskDetay`;
delimiter ;;
CREATE PROCEDURE `spGetTaskDetay`(IN `_ID` int,IN `_fkHavuz` int)
  NO SQL 
BEGIN
SET NAMES UTF8;

SET @sqlDetayIslem="select 
		d.*,
		su.ext ext,
		l.adi lokasyon,
		su.adi agent,
		su.fk_sube,
		su.fk_rol,
		rol.adi rol_adi,
		h.adi havuz,
		uh.adi userhavuz,
		date_format(d.tarih,'%d.%m.%Y') tarih,
		date_format(d.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(MINUTE,d.tarih,CURRENT_TIMESTAMP) fark,
		ifnull(sistem.adi,'') 	islemtipi,
		ifnull(sistem.class,'') cls,
		d.tarih fulldate
		from  SMDR_1002.task_DBDetay d
		left join SMDR_1002.saphira_users su on su.id = d.fk_agent
		left join SMDR_1002.crm_Lokasyon l on l.id=su.fk_lokasyon
		left join SMDR_1002.crm_rol rol on rol.id=su.fk_rol
		left join SMDR_1002.task_havuz_kullanici th on th.fk_user=su.id
		left join SMDR_1002.bild_tnm_islem_tipleri sistem on sistem.id = d.sistem
		left join SMDR_1002.task_havuzlar uh on uh.id=th.fk_havuz";
		SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," left join SMDR_1002.task_havuzlar h on h.id=",_fkHavuz);
		SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," where fk_gorev=",_ID);
		SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," group by d.tarih  order by d.id desc");
		PREPARE stmtDetayIslem FROM @sqlDetayIslem;
		EXECUTE stmtDetayIslem;		
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetTaskDetayGuncelleme
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetTaskDetayGuncelleme`;
delimiter ;;
CREATE PROCEDURE `spGetTaskDetayGuncelleme`(IN `_ID` int)
BEGIN
	#Routine body goes here...
SET NAMES UTF8;

SET @sqlDetayIslem="select 
		d.*,
		su.adi agent,
		su.fk_sube,
		su.fk_rol,
		rol.adi roladi,
		date_format(d.tarih,'%d.%m.%Y') tarih,
		date_format(d.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(MINUTE,d.tarih,CURRENT_TIMESTAMP) fark,
		d.tarih fulldate
		from SMDR_1002.task_DBDetay_Guncelleme d
		left join SMDR_1002.saphira_users su on su.id = d.fk_agent
		left join SMDR_1002.crm_rol rol on rol.id=su.fk_rol";
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," where d.fk_gorev=",_ID);		
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," order by d.id desc");
		PREPARE stmtDetayIslem FROM @sqlDetayIslem;
		EXECUTE stmtDetayIslem;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetTaskHavuzKullanici
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetTaskHavuzKullanici`;
delimiter ;;
CREATE PROCEDURE `spGetTaskHavuzKullanici`(IN userID INT,
IN whereIN INT,
IN IDS TEXT,
IN fkHavuz INT,
IN fkSube INT,
IN fkIl INT,
IN fkKonu INT)
  NO SQL 
BEGIN
-- whereIn 0 şse where id in seklinde kullanır IDS içine 1,2,3 seklinde idler gonderilmesi gerekli
SET @q="SELECT hk.*,h.adi havuz,s.adi sube,il.adi iladi,k.adi konu,u.adi useradi,u.id userid
 FROM SMDR_1002.task_havuz_kullanici hk 
 LEFT JOIN SMDR_1002.task_havuzlar h on h.id=hk.fk_havuz
 LEFT JOIN SMDR_1002.crm_SubeDB s on s.id=hk.fk_sube
 LEFT JOIN SMDR_1002.bild_tnm_il il on il.id=hk.fk_il
 LEFT JOIN SMDR_1002.task_konu k on k.id=hk.fk_konu
 LEFT JOIN SMDR_1002.saphira_users u on u.id=hk.fk_user
 WHERE 1=1 and u.isActive=1
 ";
IF(userID>0) THEN
	SET @q=CONCAT(@q," AND hk.fk_user=",userID);
END IF;
IF(fkHavuz>0) THEN
	SET @q=CONCAT(@q," AND hk.fk_havuz=",fkHavuz);
END IF;
IF(fkSube>0) THEN
	SET @q=CONCAT(@q," AND hk.fk_sube=",fkSube);
END IF;
IF(fkIl>0) THEN
	SET @q=CONCAT(@q," AND hk.fk_il=",fkIl);
END IF;
IF(fkKonu>0) THEN
	SET @q=CONCAT(@q," AND hk.fk_konu=",fkKonu);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND hk.id IN (",IDS,")");
END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spgetUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `spgetUser`;
delimiter ;;
CREATE PROCEDURE `spgetUser`(IN _ID INT)
BEGIN
SET NAMES UTF8;
SET @sql="SELECT * FROM SMDR_1002.saphira_users  WHERE id= ";
SET @sql=CONCAT(@sql,_ID);
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetVardiyaTipleri
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetVardiyaTipleri`;
delimiter ;;
CREATE PROCEDURE `spGetVardiyaTipleri`(IN id TEXT,IN tip TEXT)
  NO SQL 
BEGIN
SET @q="select * from SMDR_1002.personel_vardiya_tipleri where 1=1 and isActive=1 ";
IF(id<>'') THEN
	SET @q=CONCAT(@q," and id in(",id,")");
END IF;
IF(tip<>'') THEN
	SET @q=CONCAT(@q," and tipi in(",tip,")");
END IF;
SET @q=CONCAT(@q," order by adi asc");
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetVatandas
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetVatandas`;
delimiter ;;
CREATE PROCEDURE `spGetVatandas`(IN id INT)
  NO SQL 
BEGIN

SET @q=CONCAT("select k.id, k.adi,k.soyadi, k.adi2, k.tel1, k.email,k.tckimlik, il.adi sehir,ilce.adi ilceadi,ulke.adi ulkeadi,
k.gsm,k.* 
FROM SMDR_1002.crm_KartDB k 
left join SMDR_1002.bild_tnm_ulke ulke on ulke.id = k.fk_ulke 
left join SMDR_1002.bild_tnm_il il on il.id = k.fk_il 
left join SMDR_1002.bild_tnm_il ilce on ilce.id = k.fk_ilce 
WHERE k.id=",id);
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetVipListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetVipListe`;
delimiter ;;
CREATE PROCEDURE `spGetVipListe`(IN skip INT,
IN take INT)
  NO SQL 
BEGIN

SET @q=CONCAT("select p.*,u.adi agent from SMDR_1002.pbx_white_list p 
left join SMDR_1002.saphira_users u on u.id = p.fk_agent limit ",skip,",",take);
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetVipListeCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetVipListeCount`;
delimiter ;;
CREATE PROCEDURE `spGetVipListeCount`()
  NO SQL 
BEGIN

SET @q=CONCAT("select COUNT(*) total from SMDR_1002.pbx_white_list p 
left join SMDR_1002.saphira_users u on u.id = p.fk_agent");
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetVtMesajById
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetVtMesajById`;
delimiter ;;
CREATE PROCEDURE `spGetVtMesajById`(IN `_ID` int)
BEGIN
 
	SET @q="select 
	m.*,
	a.adi alici,
	g.adi gonderen,
	g.adi kullanici
	from SMDR_1002.vt_mesaj m 
	left join SMDR_1002.saphira_users a on a.id=m.fk_alici
	left join SMDR_1002.saphira_users g on g.id=m.fk_gonderen
	where 1=1 
	";
	SET @q=CONCAT(@q," and m.id=",_ID);
	#select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetYetkiAlanlariDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetYetkiAlanlariDetay`;
delimiter ;;
CREATE PROCEDURE `spGetYetkiAlanlariDetay`(IN _ID INT, IN _isRoot INT, IN _yetkiGrubuID INT, IN _whereInType INT, IN _IDS TEXT, IN _isActive INT)
BEGIN
SET NAMES UTF8;
SET @sql="select y.*,
ifnull(d.fk_yetki,0) fk_yetki,
ifnull(d.fk_rol,0) fk_rol 
from SMDR_1002.crm_yetki_alanlari y ";
	SET @sql=CONCAT(@sql," LEFT JOIN SMDR_1002.crm_yetki_alanlari_detay d on d.fk_yetki=y.id and d.fk_rol=",_yetkiGrubuID);	
	SET @sql=CONCAT(@sql," WHERE 1=1 ");
	IF _ID>0 THEN
		SET @sql=CONCAT(@sql," AND y.id=",_ID);
	ELSE 
  
		IF _whereInType=0 AND _IDS<>'' THEN
			SET @sql=CONCAT(@sql," AND y.id IN (",_IDS,")");
		END IF;
	
		IF _isActive<9 THEN
		SET @sql=CONCAT(@sql," AND y.isActive=",_isActive);
		END IF;

	END IF; 
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGetYetkiAlanlariDetayUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGetYetkiAlanlariDetayUser`;
delimiter ;;
CREATE PROCEDURE `spGetYetkiAlanlariDetayUser`(IN _ID INT)
BEGIN
SELECT y.*,
uy.fk_user,
uy.fk_yetki user_yetki_id
FROM smdr_1002.crm_yetki_alanlari y
left join smdr_1002.crm_yetki_alanlari_user uy on uy.fk_yetki=y.id and uy.fk_user=_ID
WHERE y.isActive=1 
order by y.id asc;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGuncellenenBild
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGuncellenenBild`;
delimiter ;;
CREATE PROCEDURE `spGuncellenenBild`(IN _skip INT,IN _take INT,IN `_AGENT` INT)
  NO SQL 
BEGIN

select  
		g.id gorevid,
        date_format(g.tarih,'%d.%m.%Y') tarih,
        concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		ifnull(su1.adi,'') yapacak_agent,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,		
		gs.adi statu,
		ifnull(il.adi,'') iladi,
		gs.ncls ,		
		g.tarih fulldate,
		tip.adi tip,		
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,		
		kart.tckimlik tc,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(hv.adi,'') havuzadi,
        ifnull(
		TIMESTAMPDIFF(HOUR,(select tarih from SMDR_1002.task_DBDetay 
         	where fk_gorev=g.id and sistem='2' order by id desc limit 1),NOW()),0) havuzsure,
g.*
 from (
select upd.fk_gorev id
from SMDR_1002.task_DBUPD upd		
	JOIN SMDR_1002.task_DB g on g.id=upd.fk_gorev 
        where g.kayit_tipi = 1 and upd.isActive=1 and g.fk_gorev_statu<>99 and g.fk_gorev_statu<>6 and
g.fk_havuz in(select fk_havuz from SMDR_1002.task_havuz_kullanici 
   where isActive=1 and fk_user=_AGENT) 
group by fk_gorev
) t
JOIN SMDR_1002.task_DB g on g.id=t.id 
	left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
	left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
	left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent	
	left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
	LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
	LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
	LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
	left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
	LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
	LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
	left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
	limit _skip,_take;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spGuncellenenBildCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spGuncellenenBildCount`;
delimiter ;;
CREATE PROCEDURE `spGuncellenenBildCount`(IN `_AGENT` INT)
BEGIN

select  
	count(*) as total
 from (
select upd.fk_gorev id
from SMDR_1002.task_DBUPD upd		
	JOIN SMDR_1002.task_DB g on g.id=upd.fk_gorev 
        where g.kayit_tipi = 1 and upd.isActive=1 and g.fk_gorev_statu<>99 and g.fk_gorev_statu<>6 and
g.fk_havuz in(select fk_havuz from SMDR_1002.task_havuz_kullanici 
   where isActive=1 and fk_user=_AGENT) 
group by fk_gorev
) t
JOIN SMDR_1002.task_DB g on g.id=t.id 
	left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
	left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
	left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent	
	left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
	LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
	LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
	LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
	left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
	LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
	LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
	left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spHavuzaYonlendir
-- ----------------------------
DROP PROCEDURE IF EXISTS `spHavuzaYonlendir`;
delimiter ;;
CREATE PROCEDURE `spHavuzaYonlendir`(IN `_ID` INT,IN `_HAVUZ` INT)
BEGIN
IF (_ID >0 AND _HAVUZ >0) THEN
SET @qry="UPDATE SMDR_1002.task_DB SET fk_havuz='";
SET @qry=CONCAT(@qry,_HAVUZ,"',fk_gorev_statu='1',fk_agent='0' where id=",_ID);
PREPARE stmt FROM @qry;
EXECUTE stmt;
END IF;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIvrSmsIptal
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIvrSmsIptal`;
delimiter ;;
CREATE PROCEDURE `spIvrSmsIptal`(IN tckn TEXT,In userId INT)
BEGIN
SET NAMES UTF8;
SET @q="DELETE FROM ivr_izin_yeni WHERE tc='";
		SET @q=CONCAT(@q,tckn,"'");
		PREPARE stmt FROM @q;
		EXECUTE stmt;
SET @q="insert into SMDR_Log.ivr_sms_log SET tc='";
SET @q=CONCAT(@q,tckn,"', islemyapan='",userId,"',status='1'");
		PREPARE stmt FROM @q;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIvrSmsSorgula
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIvrSmsSorgula`;
delimiter ;;
CREATE PROCEDURE `spIvrSmsSorgula`(IN tckn TEXT)
BEGIN
SET NAMES UTF8;
SET @q="SELECT tarih, tc, numara, CASE ws 
		WHEN '0' THEN 'SMS Kaydı Var'
		ELSE 'Web Servise Gönderildi'
		END AS webservis, ws FROM SMDR_1002.ivr_izin_yeni WHERE tc='";
		SET @q=CONCAT(@q,tckn,"'");
		PREPARE stmt FROM @q;
		EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spizinTalepKaydet
-- ----------------------------
DROP PROCEDURE IF EXISTS `spizinTalepKaydet`;
delimiter ;;
CREATE PROCEDURE `spizinTalepKaydet`(IN sdate TEXT,
IN ssaat TEXT,
IN fdate TEXT,
IN fsaat TEXT,
IN fk_sebep INT,
IN aciklama TEXT,
IN fk_agent INT,
IN fk_tipi INT,
IN fk_agent_ INT,
IN fk_lokasyon INT,
IN admin_onay INT,
IN islem_admin INT,
IN tl_onay INT,
IN islem_tl INT,
IN sv_onay INT,
IN islem_sv INT,
IN isActive INT,
IN fk_rol_ INT)
  NO SQL 
BEGIN
DECLARE insertId INT;
SET @q=CONCAT("INSERT INTO SMDR_1002.personel_izin_talep SET sdate='",sdate,"',ssaat='",ssaat,":00',fdate='",fdate,"',fsaat='",fsaat,":00',fk_sebep='",fk_sebep,"',aciklama='",aciklama,"',fk_agent='",fk_agent,"',fk_tipi='",fk_tipi,"',fk_agent_='",fk_agent_,"',fk_lokasyon='",fk_lokasyon,"',tl_onay='",tl_onay,"',islem_tl='",islem_tl,"',sv_onay='",sv_onay,"',islem_sv='",islem_sv,"',admin_onay='",admin_onay,"',islem_admin='",islem_admin,"',isActive='",isActive,"'");
PREPARE stmt FROM @q;
EXECUTE stmt;
SET insertId= LAST_INSERT_ID();
SET @qd=CONCAT("INSERT INTO SMDR_1002.personel_izin_talep_detay SET islem='1', islem_yapan='",fk_agent_,"',fk_talep='",insertId,"',fk_rol='",fk_rol_,"'");
PREPARE stmt FROM @qd;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListe`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListe`(IN userId INT)
BEGIN
SET @q="select i.*,s.adi sebep,u.adi user from SMDR_1002.personel_izin_talep i 
left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep 
left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
where 1=1 ";


SET @q=CONCAT(@q," limit ",_skip,",",_take);
select @q;
-- PREPARE stmt FROM @q;
-- EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListeAD
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListeAD`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListeAD`(IN userId INT,
IN skip INT,
IN take INT)
BEGIN
SET @wtip=CONCAT(" and i.fk_agent_=",userId);
SET @q=CONCAT("select 
 date_format(i.sdate,'%d.%m.%Y') starih, 
 date_format(i.fdate,'%d.%m.%Y') ftarih, 
 date_format(i.ssaat,'%H:%i.%s') ssaat, 
 date_format(i.fdate,'%H.%i.%s') fsaat, 
i.*,s.adi sebep,u.adi user from SMDR_1002.personel_izin_talep i 
				left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
				left join SMDR_1002.saphira_users u on u.id=i.fk_agent
				where 1=1 ",@wtip);
SET @q=CONCAT(@q," limit ",skip,",",take);
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListeADCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListeADCount`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListeADCount`(IN userId INT)
BEGIN
SET @wtip=CONCAT(" and i.fk_agent_=",userId);
SET @q=CONCAT("select COUNT(*) total from SMDR_1002.personel_izin_talep i 
				left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
				left join SMDR_1002.saphira_users u on u.id=i.fk_agent
				where 1=1 ",@wtip);
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListeSV
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListeSV`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListeSV`(IN fk_lokasyon INT,
IN userId INT,
IN isActive INT,
IN tip TEXT,
IN skip INT,
IN take INT)
BEGIN
SET @t="2";
SET @wtip="";
IF(tip="" OR tip="0") THEN
	SET @wtip=CONCAT(" and i.fk_agent_=0 and  i.fk_agent=",userId);
	SET @t=" and i.fk_tipi=2";
END IF;
IF(tip<>"0" AND tip<>"") THEN
	SET @wtip=CONCAT(" and i.fk_agent_=",userId);
	SET @t=" and i.fk_tipi<2";
END IF;
SET @w=CONCAT(@t," and i.isActive=",isActive);
SET @q=CONCAT("select 
 date_format(i.sdate,'%d.%m.%Y') starih, 
 date_format(i.fdate,'%d.%m.%Y') ftarih, 
 date_format(i.ssaat,'%H:%i.%s') ssaat, 
 date_format(i.fdate,'%H.%i.%s') fsaat, 
i.*,s.adi sebep,u.adi user from SMDR_1002.personel_izin_talep i 
left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep 
left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
where 1=1 and i.fk_lokasyon=",fk_lokasyon,@w,@wtip);

SET @q=CONCAT(@q," limit ",skip,",",take);
 -- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListeSVCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListeSVCount`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListeSVCount`(IN fk_lokasyon INT,
IN userId INT,
IN isActive INT,
IN tip TEXT)
BEGIN
SET @t="2";
SET @wtip="";
IF(tip="" OR tip="0") THEN
	SET @wtip=CONCAT(" and i.fk_agent_=0 and  i.fk_agent=",userId);
	SET @t=" and i.fk_tipi=2";
END IF;
IF(tip<>"0" AND tip<>"") THEN
	SET @wtip=CONCAT(" and i.fk_agent_=",userId);
	SET @t=" and i.fk_tipi<2";
END IF;
SET @w=CONCAT(@t," and i.isActive=",isActive);
SET @q=CONCAT("select COUNT(*) total from SMDR_1002.personel_izin_talep i 
left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep 
left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
where 1=1 and i.fk_lokasyon=",fk_lokasyon,@w,@wtip);
 -- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListeTK
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListeTK`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListeTK`(IN fk_lokasyon INT,
IN userId INT,
IN isActive INT,
IN tip TEXT,
IN skip INT,
IN take INT)
BEGIN
SET @t="1";
SET @wtip="";
IF(tip="" OR tip="0") THEN
	SET @wtip=CONCAT(" and i.fk_agent_=0 and  i.fk_agent=",userId);
	SET @t="1";
END IF;
IF(tip<>"0" AND tip<>"") THEN
	SET @wtip=CONCAT(" and i.fk_agent_=",userId);
	SET @t="0";
END IF;
SET @w=CONCAT(" and i.fk_tipi=",@t," and i.isActive=",isActive);
SET @q=CONCAT("select 
 date_format(i.sdate,'%d.%m.%Y') starih, 
 date_format(i.fdate,'%d.%m.%Y') ftarih, 
 date_format(i.ssaat,'%H:%i.%s') ssaat, 
 date_format(i.fdate,'%H.%i.%s') fsaat, 
i.*,s.adi sebep,u.adi user from SMDR_1002.personel_izin_talep i 
left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep 
left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
where 1=1 and i.fk_lokasyon=",fk_lokasyon,@w,@wtip);

SET @q=CONCAT(@q," limit ",skip,",",take);
 -- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListeTKCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListeTKCount`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListeTKCount`(IN fk_lokasyon INT,
IN userId INT,
IN isActive INT,
IN tip TEXT)
BEGIN
SET @t="1";
SET @wtip="";
IF(tip="" OR tip="0") THEN
	SET @wtip=CONCAT(" and i.fk_agent_=0 and  i.fk_agent=",userId);
	SET @t="1";
END IF;
IF(tip<>"0" AND tip<>"") THEN
	SET @wtip=CONCAT(" and i.fk_agent_=",userId);
	SET @t="0";
END IF;
SET @w=CONCAT(" and i.fk_tipi=",@t," and i.isActive=",isActive);
SET @q=CONCAT("select COUNT(*) total from SMDR_1002.personel_izin_talep i 
left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep 
left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
where 1=1 and i.fk_lokasyon=",fk_lokasyon,@w,@wtip);
 -- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListeVT
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListeVT`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListeVT`(IN userId INT,
IN isActive INT,
IN skip INT,
IN take INT)
BEGIN
SET @wtip=CONCAT(" and i.fk_tipi=0 and i.isActive=",isActive);
SET @q=CONCAT("select 
 date_format(i.sdate,'%d.%m.%Y') starih, 
 date_format(i.fdate,'%d.%m.%Y') ftarih, 
 date_format(i.ssaat,'%H:%i.%s') ssaat, 
 date_format(i.fdate,'%H.%i.%s') fsaat,
i.*,s.adi sebep,u.adi user from SMDR_1002.personel_izin_talep i 
				left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
				left join SMDR_1002.saphira_users u on u.id=i.fk_agent
				where 1=1 and i.fk_agent=",userId,@wtip);
SET @q=CONCAT(@q," limit ",skip,",",take);
-- select @q;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepListeVTCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepListeVTCount`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepListeVTCount`(IN userId INT,
IN isActive INT)
BEGIN
SET @wtip=CONCAT(" and i.fk_tipi=0 and i.isActive=",isActive);
SET @q=CONCAT("select COUNT(*) total from SMDR_1002.personel_izin_talep i 
				left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
				left join SMDR_1002.saphira_users u on u.id=i.fk_agent
				where 1=1 and i.fk_agent=",userId,@wtip);
-- select @q;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spizinTalepOnay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spizinTalepOnay`;
delimiter ;;
CREATE PROCEDURE `spizinTalepOnay`(IN userId TEXT,
IN id TEXT,
IN rol TEXT,
IN onay TEXT)
  NO SQL 
BEGIN
IF(rol='9999') THEN
	SET @q=CONCAT("UPDATE SMDR_1002.personel_izin_talep SET isActive='",onay,"',admin_onay='",onay,"',islem_admin='",userId,"' where id=",id);
END IF;
IF(rol='4') THEN
	SET @q=CONCAT("UPDATE SMDR_1002.personel_izin_talep SET isActive='",onay,"',sv_onay='",onay,"',admin_onay='",onay,"',islem_sv='",userId,"' where id=",id);
END IF;
IF(rol='11') THEN
	SET @q=CONCAT("UPDATE SMDR_1002.personel_izin_talep SET  tl_onay='",onay,"',islem_tl='",userId,"' where id=",id);
END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;

SET @qd=CONCAT("INSERT INTO SMDR_1002.personel_izin_talep_detay SET islem='1', islem_yapan='",userId,"',fk_talep='",id,"',fk_rol='",rol,"'");
PREPARE stmt FROM @qd;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepOnayListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepOnayListe`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepOnayListe`(IN fk_rol TEXT,
IN fk_lokasyon TEXT,
IN skip INT,
IN take INT)
BEGIN
IF (fk_rol='9999') THEN
SET @q=CONCAT("select 
 date_format(i.sdate,'%d.%m.%Y') starih, 
 date_format(i.fdate,'%d.%m.%Y') ftarih, 
 date_format(i.ssaat,'%H:%i.%s') ssaat, 
 date_format(i.fdate,'%H.%i.%s') fsaat, 
i.*,s.adi sebep,u.adi user from SMDR_1002.personel_izin_talep i 
				left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
				left join SMDR_1002.saphira_users u on u.id=i.fk_agent
				where 1=1 and i.isActive=0 and i.admin_onay=0 and i.sv_onay=1 ");
END IF;			
IF (fk_rol='11') THEN
SET @q=CONCAT("select 
 date_format(i.sdate,'%d.%m.%Y') starih, 
 date_format(i.fdate,'%d.%m.%Y') ftarih, 
 date_format(i.ssaat,'%H:%i.%s') ssaat, 
 date_format(i.fdate,'%H.%i.%s') fsaat, 
 i.*,s.adi sebep,u.adi user from SMDR_1002.personel_izin_talep i 
 left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
 left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
 where 1=1 and i.fk_lokasyon=",fk_lokasyon,"  and i.fk_tipi=0 and i.isActive=0 and i.tl_onay=0 and i.sv_onay=0 and i.admin_onay=0 and fk_agent_=0");
END IF;	
IF (fk_rol='4') THEN
SET @q=CONCAT("select 
 date_format(i.sdate,'%d.%m.%Y') starih, 
 date_format(i.fdate,'%d.%m.%Y') ftarih, 
 date_format(i.ssaat,'%H:%i.%s') ssaat, 
 date_format(i.fdate,'%H.%i.%s') fsaat, 
 i.*,s.adi sebep,u.adi user from SMDR_1002.personel_izin_talep i 
 left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
 left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
 where 1=1 and i.fk_lokasyon=",fk_lokasyon," and i.fk_tipi in(0,1) and i.isActive=0 and i.tl_onay=1 and i.sv_onay=0 and i.admin_onay=0");
END IF;	
SET @q=CONCAT(@q," limit ",skip,",",take);
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spIzinTalepOnayListeCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spIzinTalepOnayListeCount`;
delimiter ;;
CREATE PROCEDURE `spIzinTalepOnayListeCount`(IN fk_rol TEXT,
IN fk_lokasyon TEXT)
BEGIN
IF (fk_rol='9999') THEN
SET @q=CONCAT("select COUNT(*) total from SMDR_1002.personel_izin_talep i 
				left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
				left join SMDR_1002.saphira_users u on u.id=i.fk_agent
				where 1=1 and i.isActive=0 and i.admin_onay=0 and i.sv_onay=1 ");
END IF;			
IF (fk_rol='11') THEN
SET @q=CONCAT("select COUNT(*) total from SMDR_1002.personel_izin_talep i 
 left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
 left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
 where 1=1 and i.fk_lokasyon=",fk_lokasyon,"  and i.fk_tipi=0 and i.isActive=0 and i.tl_onay=0 and i.sv_onay=0 and i.admin_onay=0 and fk_agent_=0");
END IF;	
IF (fk_rol='4') THEN
SET @q=CONCAT("select COUNT(*) total from SMDR_1002.personel_izin_talep i 
 left join SMDR_1002.personel_vardiya_tipleri s on s.id=i.fk_sebep
 left join SMDR_1002.saphira_users u on u.id=i.fk_agent 
 where 1=1 and i.fk_lokasyon=",fk_lokasyon," and i.fk_tipi in(0,1) and i.isActive=0 and i.tl_onay=1 and i.sv_onay=0 and i.admin_onay=0");
END IF;	
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spKaraListeOnay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spKaraListeOnay`;
delimiter ;;
CREATE PROCEDURE `spKaraListeOnay`(IN id INT,
IN tarih TEXT,
IN userId INT,
IN suresiz INT,
IN del INT)
  NO SQL 
BEGIN
IF(del>0) THEN
	SET @q=CONCAT("delete from SMDR_1002.pbx_blacklist where id=",id);
ELSE
	IF(suresiz>0) THEN
			SET @q=CONCAT("update SMDR_1002.pbx_blacklist set isOnay='1',fk_onay_veren='",userId,"',isSuresiz='1' where id=",id);
	ELSE
			SET @q=CONCAT("update SMDR_1002.pbx_blacklist set isOnay='1',fk_onay_veren='",userId,"',isSuresiz='0', sure='",tarih," 23:59:00' where id=",id);
	END IF;
	
END IF;

-- select @q;
  PREPARE stmt FROM @q;
  EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spKartBildirimGecmisi
-- ----------------------------
DROP PROCEDURE IF EXISTS `spKartBildirimGecmisi`;
delimiter ;;
CREATE PROCEDURE `spKartBildirimGecmisi`(IN `_KARTID` INT)
BEGIN

SELECT 

		g.id gorevid,
    
        date_format(g.tarih,'%d.%m.%Y') tarih,
        concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		ifnull(su1.adi,'') yapacak_agent,
        
        
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		
		gs.adi statu,
		ifnull(il.adi,'') iladi,
        
		gs.ncls ,
		

		g.tarih fulldate,
		tip.adi tip,
		
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		
		kart.tckimlik tc,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(hv.adi,'') havuzadi,
        ifnull(
		TIMESTAMPDIFF(HOUR,(select tarih from SMDR_1002.task_DBDetay where fk_gorev=g.id and sistem='2' order by id desc limit 1),NOW()),0) havuzsure,
		g.*
		from  SMDR_1002.task_DB g		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
		
		where g.fk_kart=_KARTID
		order by g.id desc 
        limit 250
        ;
		
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spKulHavuzBildList
-- ----------------------------
DROP PROCEDURE IF EXISTS `spKulHavuzBildList`;
delimiter ;;
CREATE PROCEDURE `spKulHavuzBildList`(IN _skip INT,IN _take INT,IN _AGENT INT)
BEGIN
SELECT 
		g.id gorevid,
        date_format(g.tarih,'%d.%m.%Y') tarih,
        concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		ifnull(su1.adi,'') yapacak_agent,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		gs.adi statu,
		ifnull(il.adi,'') iladi,
		gs.ncls ,
		g.tarih fulldate,
		tip.adi tip,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		kart.tckimlik tc,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(hv.adi,'') havuzadi,
        ifnull(
		TIMESTAMPDIFF(HOUR,(select tarih from SMDR_1002.task_DBDetay where fk_gorev=g.id and sistem='2' order by id desc limit 1),NOW()),0) havuzsure,
		g.*
		from  SMDR_1002.task_DB g		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
		
		where   g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = _AGENT)
		and g.fk_agent = 0 and g.fk_gorev_statu < 99 and 
        g.fk_gorev_statu<>5 and g.fk_gorev_statu<>98
        and g.fk_kaydi_acan<>92  and kart.tckimlik<>''
        order by g.id desc
        limit _skip,_take;
	
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spKulHavuzBildListCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spKulHavuzBildListCount`;
delimiter ;;
CREATE PROCEDURE `spKulHavuzBildListCount`(IN _AGENT INT)
BEGIN
		SELECT
		count(*) as total
		from SMDR_1002.task_DB g
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon
        left join SMDR_1002.task_havuzlar h on h.id=g.fk_havuz
		where g.kayit_tipi = 1
        and g.fk_havuz in (select hk.fk_havuz from SMDR_1002.task_havuz_kullanici hk where hk.fk_user = _AGENT)
        and g.fk_gorev_statu < 99 and g.fk_gorev_statu<>5 and g.fk_gorev_statu<>98;
        #and g.fk_kaydi_acan<>92
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spKullaniciGuncelle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spKullaniciGuncelle`;
delimiter ;;
CREATE PROCEDURE `spKullaniciGuncelle`(IN _id INT, IN _adi TEXT, IN _email TEXT, IN _pass TEXT, 
IN _username TEXT, IN _gsm TEXT, IN _ext TEXT, IN _tckimlik TEXT, 
IN _emsicil TEXT, IN _ksicil TEXT, IN _email_bildirim INT, IN _IsActive INT , 
IN _fkIslemYapan INT, IN _face TEXT , IN _twit TEXT, IN _fk_rol INT, IN _fk_lokasyon INT,IN _fk_duzenleyen TEXT,IN _isUpdated INT,IN _fk_kurum INT)
BEGIN
SET NAMES UTF8;
SET @sql="UPDATE SMDR_1002.saphira_users SET ";
IF _adi<>'' THEN
	SET @sql=CONCAT(@sql," adi='",_adi,"',");
END IF;
IF _email<>'' THEN
	SET @sql=CONCAT(@sql," email='",_email,"',");
END IF;
IF _pass<>'' THEN
	SET @sql=CONCAT(@sql," password='",_pass,"',");
END IF;
IF _username<>'' THEN
	SET @sql=CONCAT(@sql," username='",_username,"',");
END IF;
IF _gsm<>'' THEN
	SET @sql=CONCAT(@sql," gsm='",_gsm,"',");
END IF;
IF _ext<>'' THEN
	SET @sql=CONCAT(@sql," ext='",_ext,"',");
END IF;
IF _tckimlik<>'' THEN
	SET @sql=CONCAT(@sql," tckimlik='",_tckimlik,"',");
END IF;
IF _emsicil<>'' THEN
	SET @sql=CONCAT(@sql," emsicil='",_emsicil,"',");
END IF;
IF _ksicil<>'' THEN
	SET @sql=CONCAT(@sql," ksicil='",_ksicil,"',");
END IF;
IF _email_bildirim>0 THEN
	SET @sql=CONCAT(@sql," email_bildirim='",_email_bildirim,"',");
END IF;
IF _isActive<5 THEN
	SET @sql=CONCAT(@sql," isActive='",_isActive,"',");
END IF;
IF _fkIslemYapan>0 THEN
	SET @sql=CONCAT(@sql," fk_duzenleyen='",_fkIslemYapan,"',");
END IF;
IF _face<>'' THEN
	SET @sql=CONCAT(@sql," face='",_face,"',");
END IF;
IF _twit<>'' THEN
	SET @sql=CONCAT(@sql," twit='",_twit,"',");
END IF;

IF _fk_lokasyon>0 THEN
	SET @sql=CONCAT(@sql," fk_lokasyon='",_fk_lokasyon,"',");
END IF;


IF _fk_rol>0 THEN
	SET @sql=CONCAT(@sql," fk_rol='",_fk_rol,"', ");
END IF;
IF _isUpdated<2 THEN
	SET @sql=CONCAT(@sql," isUpdated='",_isUpdated,"', ");
END IF;
IF _fk_kurum>0 THEN
	SET @sql=CONCAT(@sql," fk_kurum='",_fk_kurum,"', ");
END IF;
	SET @sql=CONCAT(@sql," fk_duzenleyen='",_fk_duzenleyen,"'");


SET @sql=CONCAT(@sql,"WHERE id=",_id);
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spKullaniciKaydet
-- ----------------------------
DROP PROCEDURE IF EXISTS `spKullaniciKaydet`;
delimiter ;;
CREATE PROCEDURE `spKullaniciKaydet`(IN _adi TEXT, 
IN _email TEXT, 
IN _pass TEXT, 
IN _username TEXT, 
IN _gsm TEXT, 
IN _ext TEXT, 
IN _tckimlik TEXT, 
IN _emsicil TEXT, 
IN _ksicil TEXT, 
IN _email_bildirim INT, 
IN _IsActive INT , 
IN _fk_islem_yapan INT,
IN _fk_rol INT, 
IN _fk_lokasyon INT,
IN _fk_ekleyen TEXT,
IN _fk_kurum INT)
BEGIN
DECLARE insertId INT;
SET NAMES UTF8;
SET @sql="INSERT INTO SMDR_1002.saphira_users SET ";
IF _adi<>'' THEN
	SET @sql=CONCAT(@sql," adi='",_adi,"',");
END IF;
IF _email<>'' THEN
	SET @sql=CONCAT(@sql," email='",_email,"',");
END IF;
IF _pass<>'' THEN
	SET @sql=CONCAT(@sql," password='",_pass,"',");
END IF;
IF _username<>'' THEN
	SET @sql=CONCAT(@sql," username='",_username,"',");
END IF;
IF _gsm<>'' THEN
	SET @sql=CONCAT(@sql," gsm='",_gsm,"',");
END IF;
IF _ext<>'' THEN
	SET @sql=CONCAT(@sql," ext='",_ext,"',");
END IF;
IF _tckimlik<>'' THEN
	SET @sql=CONCAT(@sql," tckimlik='",_tckimlik,"',");
END IF;
IF _ksicil<>'' THEN
	SET @sql=CONCAT(@sql," ksicil='",_ksicil,"',");
END IF;
IF _email_bildirim>0 THEN
	SET @sql=CONCAT(@sql," email_bildirim='",_email_bildirim,"',");
END IF;
IF _isActive<5 THEN
	SET @sql=CONCAT(@sql," isActive='",_isActive,"',");
END IF;

IF _fk_lokasyon>0 THEN
	SET @sql=CONCAT(@sql," fk_lokasyon='",_fk_lokasyon,"',");
END IF;


IF _fk_rol>0 THEN
	SET @sql=CONCAT(@sql," fk_rol='",_fk_rol,"', ");
END IF;

IF _fk_kurum>0 THEN
	SET @sql=CONCAT(@sql," fk_kurum='",_fk_kurum,"', ");
END IF;
SET @sql=CONCAT(@sql," fk_ekleyen='",_fk_ekleyen,"'");

PREPARE stmt FROM @sql;
EXECUTE stmt;
SET insertId= LAST_INSERT_ID();   
	
 SET @q=CONCAT("INSERT IGNORE SMDR_1002.saphira_users_log SET fk_agent='",insertId,"',fk_islem_yapan='",_fk_islem_yapan,"',islem='0',aciklama='KULLANICI OLUŞTURULDU.'");
 PREPARE stmt FROM @q;
 EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Function structure for SPLIT_STRING
-- ----------------------------
DROP FUNCTION IF EXISTS `SPLIT_STRING`;
delimiter ;;
CREATE FUNCTION `SPLIT_STRING`(str VARCHAR(255), delim VARCHAR(12), pos INT)
 RETURNS varchar(255) CHARSET utf8 COLLATE utf8_turkish_ci
  NO SQL 
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(str, delim, pos),
       CHAR_LENGTH(SUBSTRING_INDEX(str, delim, pos-1)) + 1),
       delim, '')
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spLogin
-- ----------------------------
DROP PROCEDURE IF EXISTS `spLogin`;
delimiter ;;
CREATE PROCEDURE `spLogin`(IN `_KEY` varchar(100))
BEGIN
	#Routine body goes here...
select u.id,  u.adi, ifnull(u.ext,'') ext, ifnull(u.SysAdmin,0) SysAdmin, 
ifnull(u.isSU,0) isSU, 
ifnull(u.isAgent, 0) isAgent,
ifnull(u.isWallBoard,0)  isWallBoard,
ifnull(u.isAdmin,0)  isAdmin,
ifnull(u.isCrm, 0) isCrm, 
ifnull(u.sysid, 0) sysid,
ifnull(u.fk_lokasyon,0)  fk_lokasyon,
ifnull(u.fk_rol, 0) fk_rol,
ifnull(u.user_change,0) user_change, 
ifnull(u.fk_lokasyon,0) fk_lokasyon,
ifnull(l.adi,'') lokasyon,
ifnull(r.adi,'') roladi,
ifnull(r.kisa_ad,'') rol,
ifnull(u.yetkiJson,'') yetkiJson
from SMDR_1002.saphira_users u
left join SMDR_1002.crm_Lokasyon l on l.id = u.fk_lokasyon
left join SMDR_1002.crm_rol r on r.id = u.fk_rol
-- where u.id=92;
where u.luser=_KEY;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spLOGINGetUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `spLOGINGetUser`;
delimiter ;;
CREATE PROCEDURE `spLOGINGetUser`(IN `_USER` VARCHAR(50))
Select username, lpass password, '' roles,id from SMDR_1002.saphira_users where luser=_USER
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spOlusturdugumBildList
-- ----------------------------
DROP PROCEDURE IF EXISTS `spOlusturdugumBildList`;
delimiter ;;
CREATE PROCEDURE `spOlusturdugumBildList`(IN _skip INT,IN _take INT,IN _userId INT)
BEGIN
SET NAMES UTF8;
SET @sql="
SELECT 
		g.id gorevid,
        date_format(g.tarih,'%d.%m.%Y') tarih,
        concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		ifnull(su1.adi,'') yapacak_agent,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		gs.adi statu,
		ifnull(il.adi,'') iladi,
		gs.ncls ,
		g.tarih fulldate,
		tip.adi tip,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		kart.tckimlik tc,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(hv.adi,'') havuzadi,
		g.*
		from  SMDR_1002.task_DB g		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
		where 1=1 
		and g.fk_gorev_statu<>5
		and g.tarih > date_add(CURRENT_DATE, INTERVAL -3 DAY) ";
		SET @sql=CONCAT(@sql," and g.fk_kaydi_acan=",_userId);
		SET @sql=CONCAT(@sql," order by g.id desc limit ",_skip,",",_take);
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spOlusturdugumBildListCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spOlusturdugumBildListCount`;
delimiter ;;
CREATE PROCEDURE `spOlusturdugumBildListCount`(IN _userId INT)
BEGIN
SET NAMES UTF8;
SET @sql="
SELECT 
		COUNT(*) total 
		from  SMDR_1002.task_DB g		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
		where 1=1
		and g.fk_gorev_statu<>5
		and g.tarih > date_add(CURRENT_DATE, INTERVAL -20 DAY)";
		SET @sql=CONCAT(@sql," and g.fk_kaydi_acan=",_userId);
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spOpenDetayIslem
-- ----------------------------
DROP PROCEDURE IF EXISTS `spOpenDetayIslem`;
delimiter ;;
CREATE PROCEDURE `spOpenDetayIslem`(IN _ID INT, IN _fk_gorev INT, IN _sistem INT)
BEGIN
SET NAMES UTF8;
SET @sqlDetayIslem="select * from SMDR_1002.task_DBDetay ";

IF _ID>0 THEN
	SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," where id=",_ID);
END IF;

IF _ID=0 THEN
	IF _sistem=0 THEN
		SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," where fk_gorev=",_fk_gorev);
    END IF;
		
    IF _sistem<100 THEN
		SET @sqlDetayIslem=CONCAT(@sqlDetayIslem," where fk_gorev=",_fk_gorev," and sistem=",_sistem);
    END IF;
END IF;
SET @sqlDetayIslem=CONCAT(@sqlDetayIslem,"  order by id desc limit 1");
PREPARE stmtDetayIslem FROM @sqlDetayIslem;
EXECUTE stmtDetayIslem;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spQueMudahale
-- ----------------------------
DROP PROCEDURE IF EXISTS `spQueMudahale`;
delimiter ;;
CREATE PROCEDURE `spQueMudahale`(IN _agent INT, IN _islemyapan INT,IN _uzunluk TEXT)
  NO SQL 
BEGIN
DECLARE insertid INT;

	SET NAMES UTF8;
	IF(_uzunluk<>'0') THEN
			SET @q="UPDATE SMDR_1002.pbx_agent_phones SET fk_pbx_login_status='0', fk_pbx_login_detail_status='0', pbx_phone_status='0' ";
			SET @q=CONCAT(@q,"WHERE ext LIKE '",_agent,"%' and length(ext)='",_uzunluk,"'");
	ELSE
		SET @q="UPDATE SMDR_1002.pbx_agent_phones SET fk_pbx_login_status='0',fk_pbx_login_detail_status='0',pbx_phone_status='0' ";
		SET @q=CONCAT(@q," WHERE ext='",_agent,"'");
	END IF;
	PREPARE stmt FROM @q;
	EXECUTE stmt;
	insert into SMDR_Log.q3p_log SET agent=_agent, islemyapan=_islemyapan,status='1';
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spSaphiraUserYetkiGuncelle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spSaphiraUserYetkiGuncelle`;
delimiter ;;
CREATE PROCEDURE `spSaphiraUserYetkiGuncelle`(IN _id INT, IN _yetkiJson LONGTEXT)
BEGIN
SET NAMES UTF8;
SET @q="UPDATE  SMDR_1002.saphira_users SET ";
SET @q=CONCAT(@q,"  yetkiJson='",_yetkiJson,"' where id=",_id);
PREPARE stmt FROM @q;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spScriptDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spScriptDetay`;
delimiter ;;
CREATE PROCEDURE `spScriptDetay`(IN id INT)
BEGIN
SET NAMES UTF8;
SET @q=CONCAT("select * from SMDR_1002.bildirim_scriptleri WHERE id=",id);
		PREPARE stmt FROM @q;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spScriptKaydet
-- ----------------------------
DROP PROCEDURE IF EXISTS `spScriptKaydet`;
delimiter ;;
CREATE PROCEDURE `spScriptKaydet`(IN fk_parent TEXT,
IN fk_aramasebebi TEXT,
IN tip_level1 TEXT,
IN tip_level2 TEXT,
IN tip_level3 TEXT,
IN icerik TEXT,
IN aciklama TEXT,
IN id TEXT)
BEGIN
IF(id="0") THEN
	SET @q=CONCAT("INSERT INTO SMDR_1002.bildirim_scriptleri SET fk_parent='" , fk_parent , "',fk_aramasebebi='" , fk_aramasebebi , "',tip_level1='" , tip_level1 , "',tip_level2='" , tip_level2 , "',tip_level3='" , tip_level3 , "',script='" , icerik , "',aciklama='" , aciklama , "'");
ELSE
	SET @q=CONCAT("UPDATE SMDR_1002.bildirim_scriptleri SET fk_parent='" , fk_parent , "',fk_aramasebebi='" , fk_aramasebebi , "',tip_level1='" , tip_level1 , "',tip_level2='" , tip_level2, "',tip_level3='" , tip_level3 , "',script='" , icerik , "',aciklama='" , aciklama , "' WHERE id=",id);
END IF;

  -- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spScriptKonuInsert
-- ----------------------------
DROP PROCEDURE IF EXISTS `spScriptKonuInsert`;
delimiter ;;
CREATE PROCEDURE `spScriptKonuInsert`(IN adi TEXT)
BEGIN
SET NAMES UTF8;
SET @q=CONCAT("INSERT INTO SMDR_1002.bildirim_script_konu SET adi='",adi,"'");
		PREPARE stmt FROM @q;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spScriptKonuUpdate
-- ----------------------------
DROP PROCEDURE IF EXISTS `spScriptKonuUpdate`;
delimiter ;;
CREATE PROCEDURE `spScriptKonuUpdate`(IN id INT,IN adi TEXT)
BEGIN
SET NAMES UTF8;
SET @q=CONCAT("UPDATE SMDR_1002.bildirim_script_konu SET adi='",adi,"' WHERE id=",id);
		PREPARE stmt FROM @q;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spScriptListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spScriptListe`;
delimiter ;;
CREATE PROCEDURE `spScriptListe`(IN skip INT,IN take INT)
BEGIN
SET NAMES UTF8;
SET @q=CONCAT("select s.*,sk.adi skadi,kn.adi knadi,t1.adi konu,t2.adi alt_konu,t3.adi alt_konu_detay from SMDR_1002.bildirim_scriptleri s 
				left join SMDR_1002.bildirim_script_konu sk on sk.id=s.fk_parent
				left join SMDR_1002.bild_tnm_aramakanali kn on kn.id=s.fk_aramasebebi
				left join SMDR_1002.task_konu t1 on t1.id=s.tip_level1
				left join SMDR_1002.task_alt_konu t2 on t2.id=s.tip_level2
				left join SMDR_1002.task_alt_konu_detay t3 on t3.id=s.tip_level3
				where s.isActive=1 limit ",skip,",",take);
		PREPARE stmt FROM @q;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spScriptListeCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spScriptListeCount`;
delimiter ;;
CREATE PROCEDURE `spScriptListeCount`()
BEGIN
SET NAMES UTF8;
SET @q=CONCAT("select COUNT(*) total from SMDR_1002.bildirim_scriptleri s 
				left join SMDR_1002.bildirim_script_konu sk on sk.id=s.fk_parent
				left join SMDR_1002.bild_tnm_aramakanali kn on kn.id=s.fk_aramasebebi
				left join SMDR_1002.task_konu t1 on t1.id=s.tip_level1
				left join SMDR_1002.task_alt_konu t2 on t2.id=s.tip_level2
				left join SMDR_1002.task_alt_konu_detay t3 on t3.id=s.tip_level3
				where s.isActive=1");
		PREPARE stmt FROM @q;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spSetCrmRolUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `spSetCrmRolUser`;
delimiter ;;
CREATE PROCEDURE `spSetCrmRolUser`(IN fk_user INT,
IN fk_rol INT)
BEGIN
SET NAMES UTF8;
SET @sql=CONCAT("INSERT INTO SMDR_1002.crm_rol_user set fk_user='",fk_user,"',fk_rol='",fk_rol,"'");
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spSonuclandirdigimBild
-- ----------------------------
DROP PROCEDURE IF EXISTS `spSonuclandirdigimBild`;
delimiter ;;
CREATE PROCEDURE `spSonuclandirdigimBild`(IN _skip INT,IN _take INT,IN `AGENT` INT)
BEGIN

select

		g.*,g.id gorevid,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		gs.adi statu,
		gs.class statu_class,
		su1.adi yapacak_agent,
		g.tarih fulldate,
		tip.adi tip,
		date_format(g.tarih,'%d.%m.%Y') tarih,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		kart.adi musteri,
		kart.soyadi musteris,
		kart.tckimlik tc,
		il.adi iladi,
		ilce.adi ilceadi,
		hv.adi havuzadi,
		TIMESTAMPDIFF(HOUR,(select tarih from SMDR_1002.task_DBDetay where fk_gorev=g.id and sistem='2' order by id desc limit 1),NOW()) havuzsure		
		from SMDR_1002.task_DB g
		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz 
		where 1=1
		 and g.fk_kapatan_agent=AGENT and g.fk_gorev_statu<>5  
		order by id desc 
		limit _skip,_take;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spSonuclandirdigimBildCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spSonuclandirdigimBildCount`;
delimiter ;;
CREATE PROCEDURE `spSonuclandirdigimBildCount`(IN _AGENT INT)
BEGIN

SELECT 
		count(*) as total
		from  SMDR_1002.task_DB g		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
		
		where g.fk_kapatan_agent = _AGENT and g.fk_gorev_statu<>5  
        and g.kapanma_tarihi >  date_add(CURRENT_DATE, INTERVAL -7 DAY);
        
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spSssGuncelle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spSssGuncelle`;
delimiter ;;
CREATE PROCEDURE `spSssGuncelle`(IN statu INT,
IN isWeb INT, 
IN fk_sss_grup INT,
IN fk_sss_altgrup INT,
IN baslik TEXT,
IN aciklama TEXT,
IN userId INT,
IN id INT)
BEGIN
		SET NAMES UTF8;
		SET @q="update SMDR_1002.crm_SSS set ";
		SET @q=CONCAT(@q,"fk_sss_grup='",fk_sss_grup,"',fk_sss_altgrup='",fk_sss_altgrup,"',baslik='",baslik,"',aciklama='",aciklama,"',isActive='",statu,"',isWeb='",isWeb,"',lng=(SELECT lng from SMDR_1002.crm_sssgrup where id=",fk_sss_grup,")"," where id=",id);

		PREPARE stmt FROM @q;
		EXECUTE stmt;
		SET @q=CONCAT("insert into SMDR_1002.crm_SSS_Log set fk_agent='",userId,"',islem='1',aciklama='",aciklama,"',fk_sss='",id,"'");
		PREPARE stmt FROM @q;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spSssKaydet
-- ----------------------------
DROP PROCEDURE IF EXISTS `spSssKaydet`;
delimiter ;;
CREATE PROCEDURE `spSssKaydet`(IN statu INT,
IN isWeb INT, 
IN fk_sss_grup INT,
IN fk_sss_altgrup INT,
IN baslik TEXT,
IN aciklama TEXT,
IN userId INT)
BEGIN
DECLARE insertid INT;
		SET NAMES UTF8;
		SET @q="INSERT INTO SMDR_1002.crm_SSS set ";
		SET @q=CONCAT(@q,"fk_sss_grup='",fk_sss_grup,"',fk_sss_altgrup='",fk_sss_altgrup,"',baslik='",baslik,"',aciklama='",aciklama,"',isActive='",statu,"',isWeb='",isWeb,"',lng=(SELECT lng from SMDR_1002.crm_sssgrup where id=",fk_sss_grup,")"," where id=",id);

		PREPARE stmt FROM @q;
		EXECUTE stmt;
		SET insertid= LAST_INSERT_ID();    
		SET @q=CONCAT("insert into SMDR_1002.crm_SSS_Log set fk_agent='",userId,"',islem='1',aciklama='Yeni SSS Ekleme İşlemi.',fk_sss='",insertid,"'");
		PREPARE stmt FROM @q;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spTaskagentChangeUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `spTaskagentChangeUser`;
delimiter ;;
CREATE PROCEDURE `spTaskagentChangeUser`(IN _ID INT, IN _STATU INT, IN _AGENT INT)
BEGIN
	SET NAMES UTF8;
	SET @sqlTask="UPDATE SMDR_1002.task_DB ";
    SET @sqlTask=CONCAT(@sqlTask," SET fk_agent='",_AGENT,"'");
    SET @sqlTask=CONCAT(@sqlTask," WHERE id=",_ID);
    PREPARE stmtTask FROM @sqlTask;
	EXECUTE stmtTask;
    
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spTaskChangeStatu
-- ----------------------------
DROP PROCEDURE IF EXISTS `spTaskChangeStatu`;
delimiter ;;
CREATE PROCEDURE `spTaskChangeStatu`(IN _id INT,
IN _statu INT)
  NO SQL 
BEGIN
	SET @sql="UPDATE SMDR_1002.task_DB SET ";
	SET @sql=CONCAT(@sql," fk_gorev_statu='",_statu,"'");
	SET @sql=CONCAT(@sql," WHERE id=",_id);
 select @sql;
-- PREPARE stmt FROM @sql;
-- EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spTaskDBUP
-- ----------------------------
DROP PROCEDURE IF EXISTS `spTaskDBUP`;
delimiter ;;
CREATE PROCEDURE `spTaskDBUP`(IN taskID INT)
BEGIN
SET NAMES UTF8;
UPDATE SMDR_1002.task_DBUPD SET isActive='1' WHERE fk_gorev= taskID;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spTaskHavuzDetayEkle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spTaskHavuzDetayEkle`;
delimiter ;;
CREATE PROCEDURE `spTaskHavuzDetayEkle`(IN fkAgent INT,
IN fkGorev INT,
IN detayIslemId INT,
IN fkHavuz INT,
IN fkSube INT,
IN fkIl INT,
IN fkKonu INT)
  NO SQL 
BEGIN
 SET @sql="insert ignore task_DBDetayHavuz ";
 SET @sql=CONCAT(@sql,"set fk_agent='",fkAgent,"',fk_gorev='",fkGorev,"',fk_detay='",detayIslemId,"',fk_havuz='",fkHavuz,"',fk_sube='",fkSube,"',fk_il='",fkIl,"',fk_konu='",fkKonu,"'");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spUserHavuzGuncelle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spUserHavuzGuncelle`;
delimiter ;;
CREATE PROCEDURE `spUserHavuzGuncelle`(IN _userId INT,
IN _fk_havuz INT,
IN _fk_sube INT,
IN _fk_il INT,
IN _fk_konu INT)
BEGIN
SET NAMES UTF8;
INSERT INTO SMDR_1002.task_havuz_kullanici SET 
fk_user=_userId,
fk_havuz=_fk_havuz,
fk_sube=_fk_sube,
fk_il=_fk_il,
fk_konu=_fk_konu,
isActive=1;
	
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spUserHavuzTemizle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spUserHavuzTemizle`;
delimiter ;;
CREATE PROCEDURE `spUserHavuzTemizle`(IN _fk_user INT)
  NO SQL 
BEGIN
DELETE FROM SMDR_1002.task_havuz_kullanici where fk_user=_fk_user;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spUserYetkiEkle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spUserYetkiEkle`;
delimiter ;;
CREATE PROCEDURE `spUserYetkiEkle`(IN _userId INT, IN _yetkiId INT)
BEGIN
	INSERT IGNORE SMDR_1002.crm_yetki_alanlari_user SET fk_user=_userId,fk_yetki=_yetkiId;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spUserYetkiTemizle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spUserYetkiTemizle`;
delimiter ;;
CREATE PROCEDURE `spUserYetkiTemizle`(IN _userId INT)
BEGIN
	DELETE FROM SMDR_1002.crm_yetki_alanlari_user WHERE fk_user=_userId;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spUzerimdekiBildirimler
-- ----------------------------
DROP PROCEDURE IF EXISTS `spUzerimdekiBildirimler`;
delimiter ;;
CREATE PROCEDURE `spUzerimdekiBildirimler`(IN _skip INT,IN _take INT,IN _AGENT INT)
BEGIN

	SELECT 
	g.*,
	g.id gorevid,
	date_format(g.tarih,'%d.%m.%Y') tarih,
	concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
	ifnull(su1.adi,'') yapacak_agent,
		  
	su.adi acan_agent,
	k1.adi konu1,
	k2.adi konu2,
	k3.adi konu3,		
	gs.adi statu,
	ifnull(il.adi,'') iladi,       
	gs.ncls ,
	g.tarih fulldate,
	ifnull(tip.adi,'') tip,
	
	date_format(g.tarih,'%H:%i') saat,
	TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
	date_format(current_date,'%d.%m.%Y %H:%i') bugun,
	
	kart.tckimlik tc,
	ifnull(il.adi,'') iladi,
	ifnull(ilce.adi,'') ilceadi,
	ifnull(hv.adi,'') havuzadi,
	ifnull(
	TIMESTAMPDIFF(HOUR,(select tarih from SMDR_1002.task_DBDetay where fk_gorev=g.id and sistem='2' order by id desc limit 1),NOW()),0) havuzsure,
	g.*
	from  SMDR_1002.task_DB g		
	left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
	left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
	left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
	left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
	LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
	LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
	LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
	left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
	LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
	LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
	left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
	-- where g.kayit_tipi = 1  and g.fk_kaydi_acan= AGENT and g.tarih >  date_add(CURRENT_DATE, INTERVAL -30 DAY)
	where   g.fk_gorev_statu < 99 and g.kayit_tipi=1 and g.fk_agent= _AGENT
	order by g.id desc 
	limit _skip,_take;
		
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spUzerimdekiBildirimlerCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spUzerimdekiBildirimlerCount`;
delimiter ;;
CREATE PROCEDURE `spUzerimdekiBildirimlerCount`(IN _AGENT INT)
BEGIN
	SELECT 
	count(*) as total
	from  SMDR_1002.task_DB g		
	left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
	left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
	left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
	left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
	LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
	LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
	LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
	left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
	LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
	LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
	left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
	-- where g.kayit_tipi = 1  and g.fk_kaydi_acan= AGENT and g.tarih >  date_add(CURRENT_DATE, INTERVAL -30 DAY)
	where   g.fk_gorev_statu < 99 and g.kayit_tipi=1 and g.fk_agent= _AGENT;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVatandasGuncelle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVatandasGuncelle`;
delimiter ;;
CREATE PROCEDURE `spVatandasGuncelle`(IN id INT,
IN fk_agent INT,
IN adi TEXT,
IN soyadi TEXT,
IN tckimlik TEXT,
IN dt TEXT,
IN tel TEXT,
IN gsm TEXT,
IN fk_egitim_durumu INT,
IN fk_ulke INT,
IN fk_il INT,
IN fk_ilce INT,
IN ssk TEXT,
IN email TEXT,
IN fk_cinsiyet INT,
IN fax TEXT)
BEGIN
SET NAMES UTF8;
SET @sql="UPDATE SMDR_1002.crm_KartDB SET ";
SET @sql=CONCAT(@sql," adi='",adi,"',");
SET @sql=CONCAT(@sql," soyadi='",soyadi,"',");
SET @sql=CONCAT(@sql," tckimlik='",tckimlik,"',");
SET @sql=CONCAT(@sql," dt='",dt,"',");
SET @sql=CONCAT(@sql," tel1='",tel,"',");
SET @sql=CONCAT(@sql," gsm='",gsm,"',");
SET @sql=CONCAT(@sql," fk_egitimdurumu='",fk_egitim_durumu,"',");
SET @sql=CONCAT(@sql," fk_ulke='",fk_ulke,"',");
SET @sql=CONCAT(@sql," fk_il='",fk_il,"',");
SET @sql=CONCAT(@sql," fk_ilce='",fk_ilce,"',");
SET @sql=CONCAT(@sql," sskno='",ssk,"',");
SET @sql=CONCAT(@sql," email='",email,"',");
SET @sql=CONCAT(@sql," fk_cinsiyet='",fk_cinsiyet,"',");
SET @sql=CONCAT(@sql," fax='",fax,"'");

SET @sql=CONCAT(@sql," WHERE id=",id);
PREPARE stmt FROM @sql;
EXECUTE stmt;

insert ignore SMDR_Log.kart_update_log set fk_agent=fk_agent,fk_kart=id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVatandasListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVatandasListe`;
delimiter ;;
CREATE PROCEDURE `spVatandasListe`(IN adi TEXT,
IN soyadi TEXT,
IN tc TEXT,
IN ssk TEXT,
IN tel TEXT,
IN tel2 TEXT,
IN fk_ulke INT,
IN fk_il INT,
IN fk_ilce INT,
IN skip INT,
IN take INT,
IN kayit_tipi INT)
  NO SQL 
BEGIN

SET @q=CONCAT("select k.id, k.adi,k.soyadi, k.adi2, k.tel1, k.email,k.tckimlik, il.adi sehir,ilce.adi ilceadi,ulke.adi ulkeadi,
k.gsm,k.kayit_tipi
FROM SMDR_1002.crm_KartDB k 
left join SMDR_1002.bild_tnm_ulke ulke on ulke.id = k.fk_ulke 
left join SMDR_1002.bild_tnm_il il on il.id = k.fk_il 
left join SMDR_1002.bild_tnm_il ilce on ilce.id = k.fk_ilce 
WHERE 1=1 and kayit_tipi=",kayit_tipi);
IF(LENGTH(adi)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",adi,"%'");
END IF;
IF(LENGTH(soyadi)>1) THEN
	SET @q=CONCAT(@q," and k.soyadi like '",soyadi,"%'");
END IF;
IF(LENGTH(tc)=11) THEN
	SET @q=CONCAT(@q," and k.tckimlik='",tc,"'");
END IF;
IF(ssk<>'') THEN
	SET @q=CONCAT(@q," and k.sskno='",ssk,"'");
END IF;
IF(tel<>'') THEN
	SET @q=CONCAT(@q," and k.tel1='",tel,"' or k.gsm='",tel,"' or k.tel1='",tel2,"' or k.gsm='",tel2,"'");
END IF;
IF(fk_ulke>0) THEN
	SET @q=CONCAT(@q," and k.fk_ulke=",fk_ulke);
END IF;
IF(fk_il>0) THEN
	SET @q=CONCAT(@q," and k.fk_il=",fk_il);
END IF;
IF(fk_ilce>0) THEN
	SET @q=CONCAT(@q," and k.fk_ilce=",fk_ilce);
END IF;
SET @q=CONCAT(@q," order by id desc limit ",skip,",",take);
select @q;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVatandasListeCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVatandasListeCount`;
delimiter ;;
CREATE PROCEDURE `spVatandasListeCount`(IN adi TEXT,
IN soyadi TEXT,
IN tc TEXT,
IN ssk TEXT,
IN tel TEXT,
IN tel2 TEXT,
IN fk_ulke INT,
IN fk_il INT,
IN fk_ilce INT,
IN kayit_tipi INT)
  NO SQL 
BEGIN

SET @q=CONCAT("select COUNT(*) total 
FROM SMDR_1002.crm_KartDB k 
left join SMDR_1002.bild_tnm_ulke ulke on ulke.id = k.fk_ulke 
left join SMDR_1002.bild_tnm_il il on il.id = k.fk_il 
left join SMDR_1002.bild_tnm_il ilce on ilce.id = k.fk_ilce 
WHERE 1=1 and kayit_tipi=",kayit_tipi);
IF(LENGTH(adi)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",adi,"%'");
END IF;
IF(LENGTH(soyadi)>1) THEN
	SET @q=CONCAT(@q," and k.soyadi like '",soyadi,"%'");
END IF;
IF(LENGTH(tc)=11) THEN
	SET @q=CONCAT(@q," and k.tckimlik='",tc,"'");
END IF;
IF(ssk<>'') THEN
	SET @q=CONCAT(@q," and k.sskno='",ssk,"'");
END IF;
IF(tel<>'') THEN
	SET @q=CONCAT(@q," and k.tel1='",tel,"' or k.gsm='",tel,"' or k.tel1='",tel2,"' or k.gsm='",tel2,"'");
END IF;
IF(fk_ulke>0) THEN
	SET @q=CONCAT(@q," and k.fk_ulke=",fk_ulke);
END IF;
IF(fk_il>0) THEN
	SET @q=CONCAT(@q," and k.fk_il=",fk_il);
END IF;
IF(fk_ilce>0) THEN
	SET @q=CONCAT(@q," and k.fk_ilce=",fk_ilce);
END IF;


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVatandasOnayla
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVatandasOnayla`;
delimiter ;;
CREATE PROCEDURE `spVatandasOnayla`(IN id INT,
IN kayit_tipi INT,
IN sifre TEXT)
  NO SQL 
BEGIN
SET @q="update SMDR_1002.crm_KartDB ";
IF(LENGTH(sifre)>0) THEN
	SET @q=CONCAT(@q," set sifre_='",sifre,"'");
ELSE	
	SET @q=CONCAT(" set kayit_tipi='",kayit_tipi,"'");
END IF;


SET @q=CONCAT(@q,"  where id=",id);
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVipListeSil
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVipListeSil`;
delimiter ;;
CREATE PROCEDURE `spVipListeSil`(IN id INT)
  NO SQL 
BEGIN

SET @q=CONCAT("delete from SMDR_1002.pbx_white_list where id=",id);
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVtAciklamaBildList
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVtAciklamaBildList`;
delimiter ;;
CREATE PROCEDURE `spVtAciklamaBildList`(IN _skip INT,IN _take INT,IN _userId INT)
BEGIN
SET NAMES UTF8;
SET @sql="
select upd.*,g.*,	
		g.id gorevid,
    date_format(g.tarih,'%d.%m.%Y') tarih,
    concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		u.adi yapacak_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,	
		il.adi iladi,
		ilce.adi ilceadi,
		gs.adi statu,
		gs.class statu_class,
		date_format(g.tarih,'%d.%m.%Y') tarih,
		date_format(g.tarih,'%H:%i') saat,
		g.isOnayBekle isonay,
		g.fk_gorev_statu,		
		
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		lok.adi lokasyon
		from SMDR_1002.task_DBUPD upd		
		LEFT JOIN SMDR_1002.task_DB g on g.id=upd.fk_gorev
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		LEFT JOIN SMDR_1002.saphira_users u on u.id=g.fk_agent
    left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce			LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon  
		where g.kayit_tipi = 1 and upd.isActive=0 ";
		IF(_userId>0) THEN
			SET @sql=CONCAT(@sql,"and g.fk_gorev_statu<>99 and g.fk_gorev_statu<>6 and 
	 g.fk_havuz in(select fk_havuz from SMDR_1002.task_havuz_kullanici where isActive=1 and fk_user=",_userId,") 
GROUP by upd.fk_gorev");
		END IF;
		SET @sql=CONCAT(@sql," limit ",_skip,",",_take);
		-- select @sql;
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVtAciklamaBildListCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVtAciklamaBildListCount`;
delimiter ;;
CREATE PROCEDURE `spVtAciklamaBildListCount`(IN _userId INT)
BEGIN
SET NAMES UTF8;
SET @sql="
select COUNT(*) total
		from SMDR_1002.task_DBUPD upd		
		LEFT JOIN SMDR_1002.task_DB g on g.id=upd.fk_gorev
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		LEFT JOIN SMDR_1002.saphira_users u on u.id=g.fk_agent
        left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	
		LEFT JOIN SMDR_1002.crm_Lokasyon lok on lok.id=g.fk_lokasyon  
		where g.kayit_tipi = 1 and upd.isActive=0 ";
		IF(_userId>0) THEN
			SET @sql=CONCAT(@sql,"and g.fk_gorev_statu<>99 and g.fk_gorev_statu<>6 and 
	 g.fk_havuz in(select fk_havuz from SMDR_1002.task_havuz_kullanici where isActive=1 and fk_user=",_userId,") 
GROUP by upd.fk_gorev");
		END IF;
		-- select @sql;
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVtMesajKullaniciListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVtMesajKullaniciListe`;
delimiter ;;
CREATE PROCEDURE `spVtMesajKullaniciListe`(IN `_TERM` varchar(175), IN `_USERID` int)
BEGIN
		SET @q="select 
	u.*
	from SMDR_1002.saphira_users u 
	where 1=1  and isActive=1
	";
		IF(LENGTH(_TERM)>2) THEN
			SET @q=CONCAT(@q," and u.adi like '",_TERM,"%'");
		ELSE
			SET @q=CONCAT(@q," limit 100");
		END IF;
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVtMesajListe
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVtMesajListe`;
delimiter ;;
CREATE PROCEDURE `spVtMesajListe`(IN `_TIP` varchar(10), IN `_SKIP` int, IN `_TAKE` int, IN `_USERID` int)
BEGIN
  
	SET @q="select 
	m.*,
	a.adi alici,
	g.adi gonderen,
	g.adi kullanici
	from SMDR_1002.vt_mesaj m 
	left join SMDR_1002.saphira_users a on a.id=m.fk_alici
	left join SMDR_1002.saphira_users g on g.id=m.fk_gonderen
	where 1=1 
	";
	IF(_TIP='gelen') THEN
		SET @q=CONCAT(@q," and m.fk_alici=",_USERID);
	END IF;
	IF(_TIP='giden') THEN
		SET @q=CONCAT(@q," and m.fk_gonderen=",_USERID);
	END IF;
	SET @q=CONCAT(@q," order by m.id");
	
	SET @q=CONCAT(@q," LIMIT ",_SKIP,",",_TAKE);
	#select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spVtMesajListeCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spVtMesajListeCount`;
delimiter ;;
CREATE PROCEDURE `spVtMesajListeCount`(IN `_TIP` varchar(10), IN `_USERID` int)
BEGIN
	SET @q="select 
	count(*) total
	from SMDR_1002.vt_mesaj m 
	left join SMDR_1002.saphira_users a on a.id=m.fk_alici
	left join SMDR_1002.saphira_users g on g.id=m.fk_gonderen
	where 1=1 
	";
	IF(_TIP='gelen') THEN
		SET @q=CONCAT(@q," and m.fk_alici=",_USERID);
	END IF;
	IF(_TIP='giden') THEN
		SET @q=CONCAT(@q," and m.fk_gonderen=",_USERID);
	END IF;

	#select @q;
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spWWWHaberDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spWWWHaberDetay`;
delimiter ;;
CREATE PROCEDURE `spWWWHaberDetay`(IN `HID` INT)
  NO SQL 
select id, baslik, kisa_aciklama, resim, detay from SMDR_1002.web_haberler where isweb=1 and lng='tr' and id= HID
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spWWWHaberler
-- ----------------------------
DROP PROCEDURE IF EXISTS `spWWWHaberler`;
delimiter ;;
CREATE PROCEDURE `spWWWHaberler`()
  NO SQL 
select id, baslik, kisa_aciklama, resim from SMDR_1002.web_haberler where isweb=1 and lng='tr' order by yayim_tarihi DESC limit 16
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spWWWVideoDetay
-- ----------------------------
DROP PROCEDURE IF EXISTS `spWWWVideoDetay`;
delimiter ;;
CREATE PROCEDURE `spWWWVideoDetay`(IN `VID` INT)
  NO SQL 
select * from SMDR_1002.web_videolar where id = VID
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spWWWVideolar
-- ----------------------------
DROP PROCEDURE IF EXISTS `spWWWVideolar`;
delimiter ;;
CREATE PROCEDURE `spWWWVideolar`()
  NO SQL 
select * from SMDR_1002.web_videolar where lng='tr' order by id desc
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spYenidenAcilanBildList
-- ----------------------------
DROP PROCEDURE IF EXISTS `spYenidenAcilanBildList`;
delimiter ;;
CREATE PROCEDURE `spYenidenAcilanBildList`(IN _skip INT,IN _take INT,IN _fk_lokasyon INT)
BEGIN
SET NAMES UTF8;
SET @sql="
SELECT 
g.*,
		g.id gorevid,
        date_format(g.tarih,'%d.%m.%Y') tarih,
        concat(kart.adi, ' ' , kart.soyadi) basvuru_sahibi,
		ifnull(su1.adi,'') yapacak_agent,
		su.adi acan_agent,
		k1.adi konu1,
		k2.adi konu2,
		k3.adi konu3,
		gs.adi statu,
		ifnull(il.adi,'') iladi,
		gs.ncls ,
		g.tarih fulldate,
		tip.adi tip,
		date_format(g.tarih,'%H:%i') saat,
		TIMESTAMPDIFF(HOUR,g.tarih,CURRENT_TIMESTAMP) fark,
		date_format(current_date,'%d.%m.%Y %H:%i') bugun,
		kart.tckimlik tc,
		ifnull(il.adi,'') iladi,
		ifnull(ilce.adi,'') ilceadi,
		ifnull(hv.adi,'') havuzadi,
		g.*
		from  SMDR_1002.task_DB g		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz  
		where g.kayit_tipi = 1
		and g.fk_gorev_statu=20 and g.isReopen>0 and g.fk_havuz=0 ";

		IF(_fk_lokasyon>0) THEN
					SET @sql=CONCAT(@sql," and g.fk_lokasyon=",_fk_lokasyon);
		END IF;
		SET @sql=CONCAT(@sql," order by g.id desc limit ",_skip,",",_take);
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spYenidenAcilanBildListCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `spYenidenAcilanBildListCount`;
delimiter ;;
CREATE PROCEDURE `spYenidenAcilanBildListCount`(IN AGENT INT)
BEGIN
SET NAMES UTF8;
		select count(*) total
		from SMDR_1002.task_DB g
		
		left join SMDR_1002.task_GorevStatu gs on gs.id = g.fk_gorev_statu
		left join SMDR_1002.saphira_users su on su.id = g.fk_kaydi_acan
		left join SMDR_1002.saphira_users su1 on su1.id = g.fk_agent
		
		left join SMDR_1002.task_tipi tip on tip.id = g.fk_tip
		LEFT JOIN SMDR_1002.task_konu k1 ON k1.id = g.tip_level1
		LEFT JOIN SMDR_1002.task_alt_konu k2 ON k2.id = g.tip_level2
		LEFT JOIN SMDR_1002.task_alt_konu_detay k3 ON k3.id = g.tip_level3
		left join SMDR_1002.crm_KartDB kart on kart.id = g.fk_kart
		LEFT JOIN SMDR_1002.bild_tnm_il il ON il.id = kart.fk_il
		LEFT JOIN SMDR_1002.bild_tnm_ilce ilce ON ilce.id = kart.fk_ilce	 
		left join SMDR_1002.task_havuzlar hv on hv.id=g.fk_havuz 
		where 1=1
		 and g.fk_kapatan_agent=AGENT and g.fk_gorev_statu<>5;	 
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spYetkiGrubuDetayGuncelle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spYetkiGrubuDetayGuncelle`;
delimiter ;;
CREATE PROCEDURE `spYetkiGrubuDetayGuncelle`(IN _fkYetki INT, IN _fkRol INT)
BEGIN
SET NAMES UTF8;
INSERT INTO SMDR_1002.crm_yetki_alanlari_detay SET fk_rol=_fkRol,fk_yetki=_fkYetki;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spYetkiGrubuDetayTemizle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spYetkiGrubuDetayTemizle`;
delimiter ;;
CREATE PROCEDURE `spYetkiGrubuDetayTemizle`(IN _fkRol INT)
BEGIN
	DELETE FROM SMDR_1002.crm_yetki_alanlari_detay WHERE fk_rol=_fkRol;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for spYetkiGrubuGuncelle
-- ----------------------------
DROP PROCEDURE IF EXISTS `spYetkiGrubuGuncelle`;
delimiter ;;
CREATE PROCEDURE `spYetkiGrubuGuncelle`(IN _adi TEXT,IN _isActive INT,IN _isHidden INT,IN _id INT)
BEGIN
SET NAMES UTF8;
SET @sql="UPDATE SMDR_1002.crm_rol SET ";
SET @sql=CONCAT(@sql," adi='",_adi,"',");
SET @sql=CONCAT(@sql," isActive='",_isActive,"',");
SET @sql=CONCAT(@sql," isHidden='",_isHidden,"'");
SET @sql=CONCAT(@sql," WHERE id=",_id);
PREPARE stmt FROM @sql;
EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tempAllDetaySifresiz
-- ----------------------------
DROP PROCEDURE IF EXISTS `tempAllDetaySifresiz`;
delimiter ;;
CREATE PROCEDURE `tempAllDetaySifresiz`()
  NO SQL 
SELECT fk_gorev id FROM SMDR_1002.`Bildirim_Detay` WHERE `isEnc`<2
group by fk_gorev
order by fk_gorev desc
limit 1000
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tempGetTCList
-- ----------------------------
DROP PROCEDURE IF EXISTS `tempGetTCList`;
delimiter ;;
CREATE PROCEDURE `tempGetTCList`()
  NO SQL 
select id, tckimlik from SMDR_1002.crm_KartDB where tc = '-'
limit 1000
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tempSetTCList
-- ----------------------------
DROP PROCEDURE IF EXISTS `tempSetTCList`;
delimiter ;;
CREATE PROCEDURE `tempSetTCList`(IN `_ID` INT, IN `_TC` VARCHAR(100))
  NO SQL 
update SMDR_1002.crm_KartDB set tc=_TC where id=_ID
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tempTaskAciklamaSifrele
-- ----------------------------
DROP PROCEDURE IF EXISTS `tempTaskAciklamaSifrele`;
delimiter ;;
CREATE PROCEDURE `tempTaskAciklamaSifrele`(IN `_ID` INT, IN `_ACIKLAMA` LONGTEXT)
  NO SQL 
BEGIN
update SMDR_1002.Bildirim set cryp_aciklama=_ACIKLAMA, isEnc=1 where id=_ID;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tempTaskDetayAciklamaSifrele
-- ----------------------------
DROP PROCEDURE IF EXISTS `tempTaskDetayAciklamaSifrele`;
delimiter ;;
CREATE PROCEDURE `tempTaskDetayAciklamaSifrele`(IN `_ID` INT, IN `_ACIKLAMA` LONGTEXT)
  NO SQL 
update SMDR_1002.Bildirim_Detay set cryp_aciklama = _ACIKLAMA, isEnc =2
where id = _ID
;
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tempTaskDetaySifresizAciklama
-- ----------------------------
DROP PROCEDURE IF EXISTS `tempTaskDetaySifresizAciklama`;
delimiter ;;
CREATE PROCEDURE `tempTaskDetaySifresizAciklama`(IN `TASKID` INT)
  NO SQL 
BEGIN
select id, aciklama adi from SMDR_1002.Bildirim_Detay where fk_gorev = TASKID ;
-- and isEnc = 0

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tempTaskSifresizAciklama
-- ----------------------------
DROP PROCEDURE IF EXISTS `tempTaskSifresizAciklama`;
delimiter ;;
CREATE PROCEDURE `tempTaskSifresizAciklama`()
  NO SQL 
BEGIN

-- delete FROM SMDR_1002.`task_DBDetay` WHERE aciklama = 'Bildirimi Oluşturdu [Sistem]' and sistem=0 and fk_detay_tip=0
-- limit 20000;

select id, aciklama adi from SMDR_1002.Bildirim where 
-- id >= 30000000 and id <= 35000000 and 
id=50581109
-- isEnc = 0 
limit 1000;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmAltKurumlar
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmAltKurumlar`;
delimiter ;;
CREATE PROCEDURE `tnmAltKurumlar`(IN `_ID` INT,IN `_TERM` VARCHAR(200),IN _fk_parent INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="	SELECT b.id,
ifnull(b.adi,'') adi
,ifnull(b.email,'') email
,b.isActive, tk.adi kurumadi,
b.fk_parent,
if(b.isActive=1,'Aktif','Pasif') durum

from SMDR_1002.task_alt_konu b
left join SMDR_1002.task_konu tk on tk.id = b.fk_parent WHERE 1=1 ";

if(_ID > 0) THEN
		SET @q=CONCAT(@q," and b.id=",_ID);
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and b.adi like '",_TERM,"%'"); 
END IF;
IF(_fk_parent>0) THEN
	SET @q=CONCAT(@q," and b.fk_parent=",_fk_parent);
END IF;
SET @q=CONCAT(@q," order by id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmAltKurumlarCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmAltKurumlarCount`;
delimiter ;;
CREATE PROCEDURE `tnmAltKurumlarCount`(IN `_ID` INT,IN `_TERM` VARCHAR(200),IN _fk_parent INT)
  NO SQL 
BEGIN

SET @q="	SELECT COUNT(*) total
from SMDR_1002.task_alt_konu b
left join SMDR_1002.task_konu tk on tk.id = b.fk_parent WHERE 1=1 ";

if(_ID > 0) THEN
		SET @q=CONCAT(@q," and b.id=",_ID);
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and b.adi like '",_TERM,"%'"); 
END IF;
IF(_fk_parent>0) THEN
	SET @q=CONCAT(@q," and b.fk_parent=",_fk_parent);
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmAltKurumlarFiltre
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmAltKurumlarFiltre`;
delimiter ;;
CREATE PROCEDURE `tnmAltKurumlarFiltre`(IN `_KONU` INT)
  NO SQL 
BEGIN

if(_KONU = 0) THEN
	SELECT b.id,
ifnull(b.adi,'') adi
,b.isActive, tk.adi kurumadi,
b.fk_parent

from SMDR_1002.task_alt_konu b
left join SMDR_1002.task_konu tk on tk.id = b.fk_parent

order by b.id;

END IF;

if(_KONU > 0) THEN
SELECT 
b.id,
 ifnull(b.adi,'') adi,
b.isActive, tk.adi kurumadi,
b.fk_parent

from SMDR_1002.task_alt_konu b
left join SMDR_1002.task_konu tk on tk.id = b.fk_parent

where b.fk_parent = _KONU

order by b.id ;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmAramaKanali
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmAramaKanali`;
delimiter ;;
CREATE PROCEDURE `tnmAramaKanali`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN _fk_parent INT,IN whereIN INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="SELECT *,if(k.isActive=1,'Aktif','Pasif') durumu FROM SMDR_1002.bild_tnm_aramakanali k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;
if(_fk_parent > 0) THEN
		SET @q=CONCAT(@q," and k.fk_parent=",_fk_parent);
END IF;
SET @q=CONCAT(@q," order by k.id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmAramaKanaliCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmAramaKanaliCount`;
delimiter ;;
CREATE PROCEDURE `tnmAramaKanaliCount`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN _fk_parent INT,IN whereIN INT)
  NO SQL 
BEGIN

SET @q="SELECT COUNT(*) total FROM SMDR_1002.bild_tnm_aramakanali k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;
if(_fk_parent > 0) THEN
		SET @q=CONCAT(@q," and k.fk_parent=",_fk_parent);
END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmAramaKonusu
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmAramaKonusu`;
delimiter ;;
CREATE PROCEDURE `tnmAramaKonusu`(IN `_ID` INT,IN `_TERM` VARCHAR(200),IN _fk_konu INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="select d.fk_parent,d.fk_konu,d.id id, d.adi adi, d.isActive isActive, a.adi altkurum, k.adi kurum,if(d.isActive=1,'Aktif','Pasif') durum from SMDR_1002.task_alt_konu_detay d
					left join SMDR_1002.task_alt_konu a on a.id=d.fk_parent
					left join SMDR_1002.task_konu k on k.id=d.fk_konu 
WHERE 1=1 
";

if(_ID > 0) THEN
		SET @q=CONCAT(@q," and d.id=",_ID);
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and d.adi like '",_TERM,"%'"); 
END IF;
if(_fk_konu > 0) THEN
		SET @q=CONCAT(@q," and d.fk_konu=",_fk_konu);
END IF;
SET @q=CONCAT(@q," order by d.id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmAramaKonusuCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmAramaKonusuCount`;
delimiter ;;
CREATE PROCEDURE `tnmAramaKonusuCount`(IN `_ID` INT,IN `_TERM` VARCHAR(200),IN _fk_konu INT)
  NO SQL 
BEGIN

SET @q="select COUNT(*) total from SMDR_1002.task_alt_konu_detay d
					left join SMDR_1002.task_alt_konu a on a.id=d.fk_parent
					left join SMDR_1002.task_konu k on k.id=d.fk_konu 
WHERE 1=1 
";

if(_ID > 0) THEN
		SET @q=CONCAT(@q," and d.id=",_ID);
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and d.adi like '",_TERM,"%'"); 
END IF;
if(_fk_konu > 0) THEN
		SET @q=CONCAT(@q," and d.fk_konu=",_fk_konu);
END IF;



PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmAramaKonusuFiltre
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmAramaKonusuFiltre`;
delimiter ;;
CREATE PROCEDURE `tnmAramaKonusuFiltre`(IN `_KONU` INT)
  NO SQL 
BEGIN

if(_KONU = 0) THEN
	SELECT b.id,
ifnull(b.adi,'') adi
,b.isActive, tk.adi kurumadi
,tkk.adi konuadi,b.fk_parent,b.fk_konu

from SMDR_1002.task_alt_konu_detay b
left join SMDR_1002.task_konu tk on tk.id = b.fk_parent
left join SMDR_1002.task_alt_konu tkk on tkk.id = b.fk_konu

order by b.id;

END IF;

if(_KONU > 0) THEN
SELECT 
b.id,
 ifnull(b.adi,'') adi,
b.isActive, tk.adi kurumadi
,tkk.adi konuadi,b.fk_parent,b.fk_konu

from SMDR_1002.task_alt_konu_detay b
left join SMDR_1002.task_konu tk on tk.id = b.fk_parent
left join SMDR_1002.task_alt_konu tkk on tkk.id = b.fk_konu

where b.fk_parent = _KONU

order by b.id ;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmDahili
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmDahili`;
delimiter ;;
CREATE PROCEDURE `tnmDahili`(IN ID INT,IN EXT TEXT,IN whereIN INT,IN fk_lokasyon INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="SELECT p.id id, p.ext dahili, p.agent_adi sahibi,p.fk_lokasyon,p.password,l.adi lokasyonAdi  
FROM SMDR_1002.pbx_agent_phones p  left join smdr_1002.crm_lokasyon l on l.id=p.fk_lokasyon
WHERE 1=1 ";

IF(ID>0) THEN
	SET @q=CONCAT(@q," AND p.id=",ID);
	ELSE
	IF(whereIN=0 AND length(EXT)>0) THEN
		SET @q=CONCAT(@q," AND p.EXT IN (",EXT,")");
	END IF;
	IF(fk_lokasyon>0) THEN
		SET @q=CONCAT(@q," and p.fk_lokasyon=",fk_lokasyon);
	END IF;
	SET @q=CONCAT(@q," limit ",_skip,",",_take);

END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmDahiliCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmDahiliCount`;
delimiter ;;
CREATE PROCEDURE `tnmDahiliCount`(IN EXT TEXT,IN fk_lokasyon INT,IN whereIN INT)
  NO SQL 
BEGIN

SET @q="SELECT COUNT(*) total 
FROM SMDR_1002.pbx_agent_phones p 
WHERE 1=1 ";

IF(whereIN=0 AND length(EXT)>0) THEN
	SET @q=CONCAT(@q," AND p.EXT IN (",EXT,")");
END IF;
IF(fk_lokasyon>0) THEN
	SET @q=CONCAT(@q," and p.fk_lokasyon=",fk_lokasyon);
END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmDelPeriyodik
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmDelPeriyodik`;
delimiter ;;
CREATE PROCEDURE `tnmDelPeriyodik`(IN id INT)
  NO SQL 
BEGIN
SET @q="DELETE FROM SMDR_1002.periyodik_mail_list ";
SET @q=CONCAT(@q," WHERE id=",id);
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmEgitimDurumu
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmEgitimDurumu`;
delimiter ;;
CREATE PROCEDURE `tnmEgitimDurumu`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN whereIN INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="SELECT *,if(k.isActive=1,'Aktif','Pasif') durumu FROM SMDR_1002.bild_tnm_egitim_durumu k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

SET @q=CONCAT(@q," order by k.id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmEgitimDurumuCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmEgitimDurumuCount`;
delimiter ;;
CREATE PROCEDURE `tnmEgitimDurumuCount`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN whereIN INT)
  NO SQL 
BEGIN

SET @q="SELECT Count(*) total FROM SMDR_1002.bild_tnm_egitim_durumu k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmGuvenlikSorulari
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmGuvenlikSorulari`;
delimiter ;;
CREATE PROCEDURE `tnmGuvenlikSorulari`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN whereIN INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="SELECT *,if(k.isActive=1,'Aktif','Pasif') durumu FROM SMDR_1002.bild_tnm_guvenliksoru k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

SET @q=CONCAT(@q," order by k.id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmGuvenlikSorulariCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmGuvenlikSorulariCount`;
delimiter ;;
CREATE PROCEDURE `tnmGuvenlikSorulariCount`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN whereIN INT)
  NO SQL 
BEGIN

SET @q="SELECT Count(*) total FROM SMDR_1002.bild_tnm_guvenliksoru k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmHavuzlar
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmHavuzlar`;
delimiter ;;
CREATE PROCEDURE `tnmHavuzlar`(IN IDS TEXT,
IN isActive INT,
IN fkSube INT,
IN fkIl INT,
IN fkKonu INT,
IN whereIN INT,
IN Term TEXT,
IN skip INT,
IN take INT)
  NO SQL 
BEGIN
-- WHEREIN, IDS PARAMETRESINDE GIRILEN ID LERİ IN YADA NOT IN OLACAĞINI BELİRLER. 0 IN, 1 NOT IN, 2 YOKSAY
SET @q="SELECT h.*,s.adi sube,il.adi iladi,k.adi konu,
(select count(*) from SMDR_1002.task_havuz_kullanici t 
left join SMDR_1002.saphira_users u on u.id=t.fk_user 
where fk_havuz=s.id and u.isActive=1) kullanici_sayisi,if(h.isActive=1,'Aktif','Pasif') havuz_durum 

 FROM SMDR_1002.task_havuzlar h 
 LEFT JOIN SMDR_1002.crm_SubeDB s on s.id=h.fk_sube
 LEFT JOIN SMDR_1002.bild_tnm_il il on il.id=h.fk_il
 LEFT JOIN SMDR_1002.task_konu k on k.id=h.fk_konu
 WHERE 1=1 
 ";
 
 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND h.isActive=",isActive);
END IF;

IF(fkSube>0) THEN
	SET @q=CONCAT(@q," AND h.fk_sube=",fkSube);
END IF;

IF(fkIl>0) THEN
	SET @q=CONCAT(@q," AND h.fk_il=",fkIl);
END IF;

IF(fkKonu>0) THEN
	SET @q=CONCAT(@q," AND h.fk_konu=",fkKonu);
END IF;

IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND h.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND h.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(Term)>2) THEN
	SET @q=CONCAT(@q," AND h.adi like '",Term,"%'");
END IF;

SET @q=CONCAT(@q," order by h.id ");
SET @q=CONCAT(@q," limit ",skip,",",take);
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmHavuzlarCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmHavuzlarCount`;
delimiter ;;
CREATE PROCEDURE `tnmHavuzlarCount`(IN IDS TEXT,
IN isActive INT,
IN fkSube INT,
IN fkIl INT,
IN fkKonu INT,
IN whereIN INT,
IN Term TEXT)
  NO SQL 
BEGIN
-- WHEREIN, IDS PARAMETRESINDE GIRILEN ID LERİ IN YADA NOT IN OLACAĞINI BELİRLER. 0 IN, 1 NOT IN, 2 YOKSAY
SET @q="SELECT COUNT(*) total 
 FROM SMDR_1002.task_havuzlar h 
 LEFT JOIN SMDR_1002.crm_SubeDB s on s.id=h.fk_sube
 LEFT JOIN SMDR_1002.bild_tnm_il il on il.id=h.fk_il
 LEFT JOIN SMDR_1002.task_konu k on k.id=h.fk_konu
 WHERE 1=1 
 ";
 
 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND h.isActive=",isActive);
END IF;

IF(fkSube>0) THEN
	SET @q=CONCAT(@q," AND h.fk_sube=",fkSube);
END IF;

IF(fkIl>0) THEN
	SET @q=CONCAT(@q," AND h.fk_il=",fkIl);
END IF;

IF(fkKonu>0) THEN
	SET @q=CONCAT(@q," AND h.fk_konu=",fkKonu);
END IF;

IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND h.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND h.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(Term)>2) THEN
	SET @q=CONCAT(@q," AND h.adi like '",Term,"%'");
END IF;

-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmHavuzlar_sil
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmHavuzlar_sil`;
delimiter ;;
CREATE PROCEDURE `tnmHavuzlar_sil`(IN `_ID` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN
	SELECT s.*,
	k.adi kurum,
	i.adi il,
	sb.adi sube,
	s.fk_sube,
	sb.fk_konu,
	s.fk_il,
(select count(*) from SMDR_1002.task_havuz_kullanici t 
left join SMDR_1002.saphira_users u on u.id=t.fk_user 
where fk_havuz=s.id and u.isActive=1) havuz_sayisi

	from SMDR_1002.task_havuzlar s
left join SMDR_1002.crm_SubeDB sb on sb.id=s.fk_sube
left join SMDR_1002.task_konu k on k.id=sb.fk_konu
left join SMDR_1002.bild_tnm_il i on i.id=s.fk_il
                

order by s.id;

END IF;

if(_ID > 0) THEN

SELECT s.*,
k.adi kurum,
i.adi il,
sb.adi sube,
s.fk_sube,
sb.fk_konu,
s.fk_il,
(select count(*) from SMDR_1002.task_havuz_kullanici t 
left join SMDR_1002.saphira_users u on u.id=t.fk_user 
where fk_havuz=s.id and u.isActive=1) havuz_sayisi

from SMDR_1002.task_havuzlar s
left join SMDR_1002.crm_SubeDB sb on sb.id=s.fk_sube
left join SMDR_1002.task_konu k on k.id=sb.fk_konu
left join SMDR_1002.bild_tnm_il i on i.id=s.fk_il
                

where s.id = _ID

order by s.id ;

END IF;



END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmIlceler
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmIlceler`;
delimiter ;;
CREATE PROCEDURE `tnmIlceler`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN fk_il INT,IN whereIN INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="SELECT *,if(k.isActive=1,'Aktif','Pasif') durumu FROM SMDR_1002.bild_tnm_ilce k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
 IF(fk_il>0) THEN
	SET @q=CONCAT(@q," AND k.fk_il=",fk_il);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

SET @q=CONCAT(@q," order by k.id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmIlcelerCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmIlcelerCount`;
delimiter ;;
CREATE PROCEDURE `tnmIlcelerCount`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN fk_il INT,IN whereIN INT)
  NO SQL 
BEGIN

SET @q="SELECT Count(*) total FROM SMDR_1002.bild_tnm_ilce k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
 IF(fk_il>0) THEN
	SET @q=CONCAT(@q," AND k.fk_il=",fk_il);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmIller
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmIller`;
delimiter ;;
CREATE PROCEDURE `tnmIller`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN fk_ulke INT,IN whereIN INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="SELECT *,if(k.isActive=1,'Aktif','Pasif') durumu FROM SMDR_1002.bild_tnm_il k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
 IF(fk_ulke>0) THEN
	SET @q=CONCAT(@q," AND k.fk_ulke=",fk_ulke);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

SET @q=CONCAT(@q," order by k.id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmIllerCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmIllerCount`;
delimiter ;;
CREATE PROCEDURE `tnmIllerCount`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN fk_ulke INT,IN whereIN INT)
  NO SQL 
BEGIN

SET @q="SELECT Count(*) total FROM SMDR_1002.bild_tnm_il k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
 IF(fk_ulke>0) THEN
	SET @q=CONCAT(@q," AND k.fk_ulke=",fk_ulke);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmInsert
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmInsert`;
delimiter ;;
CREATE PROCEDURE `tnmInsert`(IN fldCount INT,IN tbl TEXT,IN flds TEXT,IN vals TEXT,IN ayrac VARCHAR(50))
  NO SQL 
BEGIN

      DECLARE i INT Default 0 ;
			SET @q=CONCAT("INSERT INTO ",tbl," SET ");
      simple_loop: LOOP
         SET i=i+1;
        -- select a;
         IF i>fldCount THEN
            LEAVE simple_loop;
         END IF;
				 IF i=fldCount THEN
						SET @fld=SPLIT_STRING(flds,ayrac,i);
						SET @val=SPLIT_STRING(vals,ayrac,i);
            SET @q=CONCAT(@q,@fld,"='",@val,"'");
				 ELSE
						 SET @fld=SPLIT_STRING(flds,ayrac,i);
						 SET @val=SPLIT_STRING(vals,ayrac,i);
						 SET @q=CONCAT(@q,@fld,"='",@val,"',");
         END IF;				 
		END LOOP simple_loop;
		PREPARE stmt FROM @q;
		EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmKkoBaslik
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmKkoBaslik`;
delimiter ;;
CREATE PROCEDURE `tnmKkoBaslik`(IN id INT,IN isActive INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN
SET @q="	select *,if(isActive=1,'Aktif','Pasif') durum from pbx.kko_basliklar WHERE 1=1 ";
IF (id>0) THEN
SET @q=CONCAT(@q," AND id=",id);
END IF;
IF(isActive <2) THEN
SET @q=CONCAT(@q," AND isActive=",isActive);
END IF;
SET @q=CONCAT(@q," limit ",_skip,",",_take);

-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmKkoBaslikCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmKkoBaslikCount`;
delimiter ;;
CREATE PROCEDURE `tnmKkoBaslikCount`(IN id INT,IN isActive INT)
  NO SQL 
BEGIN
SET @q="	select COUNT(*) total from pbx.kko_basliklar WHERE 1=1 ";
IF (id>0) THEN
SET @q=CONCAT(@q," AND id=",id);
END IF;
IF(isActive <2) THEN
SET @q=CONCAT(@q," AND isActive=",isActive);
END IF;

 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmKkoSorular
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmKkoSorular`;
delimiter ;;
CREATE PROCEDURE `tnmKkoSorular`(IN id INT,IN isActive INT,IN fk_parent INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN
SET @q="	select s.*,if(s.isActive=1,'Aktif','Pasif') durum,b.adi baslik from pbx.kko_sorular s
LEFT JOIN pbx.kko_basliklar b on b.id=s.fk_parent
WHERE 1=1 ";
IF (id>0) THEN
SET @q=CONCAT(@q," AND s.id=",id);
END IF;
IF (fk_parent>0) THEN
SET @q=CONCAT(@q," AND s.fk_parent=",id);
END IF;
IF(isActive <2) THEN
SET @q=CONCAT(@q," AND s.isActive=",isActive);
END IF;
SET @q=CONCAT(@q," limit ",_skip,",",_take);

-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmKkoSorularCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmKkoSorularCount`;
delimiter ;;
CREATE PROCEDURE `tnmKkoSorularCount`(IN id INT,IN isActive INT,IN fk_parent INT)
  NO SQL 
BEGIN
SET @q="	select COUNT(*) total from pbx.kko_sorular s
LEFT JOIN pbx.kko_basliklar b on b.id=s.fk_parent
WHERE 1=1 ";
IF (id>0) THEN
SET @q=CONCAT(@q," AND s.id=",id);
END IF;
IF (fk_parent>0) THEN
SET @q=CONCAT(@q," AND s.fk_parent=",id);
END IF;
IF(isActive <2) THEN
SET @q=CONCAT(@q," AND s.isActive=",isActive);
END IF;


-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmKurumlar
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmKurumlar`;
delimiter ;;
CREATE PROCEDURE `tnmKurumlar`(IN `_ID` INT,IN `_TERM` VARCHAR(200),IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="	SELECT b.id,	ifnull(b.adi,'') adi	,ifnull(b.email,'') email	,isActive,if(b.isActive=1,'Aktif','Pasif') durum	from SMDR_1002.task_konu b where 1=1 ";

if(_ID > 0) THEN
		SET @q=CONCAT(@q," and b.id=",_ID);
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and b.adi like '",_TERM,"%'"); 
END IF;
SET @q=CONCAT(@q," order by id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmKurumlarCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmKurumlarCount`;
delimiter ;;
CREATE PROCEDURE `tnmKurumlarCount`(IN `_ID` INT,IN `_TERM` VARCHAR(200))
  NO SQL 
BEGIN

SET @q="	SELECT COUNT(*) total	from SMDR_1002.task_konu b where 1=1 ";

if(_ID > 0) THEN
		SET @q=CONCAT(@q," and b.id=",_ID);
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and b.adi like '",_TERM,"%'"); 
END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmPeriyodik
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmPeriyodik`;
delimiter ;;
CREATE PROCEDURE `tnmPeriyodik`(IN id INT,IN skip INT,IN take INT)
  NO SQL 
BEGIN
SET @q="	select * from SMDR_1002.periyodik_mail_list
WHERE 1=1 ";
IF (id>0) THEN
SET @q=CONCAT(@q," AND s.id=",id);
END IF;
SET @q=CONCAT(@q," limit ",skip,",",take);
-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmPeriyodikCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmPeriyodikCount`;
delimiter ;;
CREATE PROCEDURE `tnmPeriyodikCount`(IN id INT)
  NO SQL 
BEGIN
SET @q="	select COUNT(*) total from SMDR_1002.periyodik_mail_list
WHERE 1=1 ";
IF (id>0) THEN
SET @q=CONCAT(@q," AND s.id=",id);
END IF;

-- select @q;
 PREPARE stmt FROM @q;
 EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmSETAltKurum
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmSETAltKurum`;
delimiter ;;
CREATE PROCEDURE `tnmSETAltKurum`(IN `_ID` INT, IN `_ADI` VARCHAR(100), IN `_PARENT` INT, IN `_EMAIL` VARCHAR(2000), IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.task_alt_konu 
SET adi = _ADI , fk_parent = _PARENT, email = _EMAIL, isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.task_alt_konu 
SET adi = _ADI , fk_parent = _PARENT, email = _EMAIL, isActive = _ISACTIVE 
where id = _ID 

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmSETAramaKonusu
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmSETAramaKonusu`;
delimiter ;;
CREATE PROCEDURE `tnmSETAramaKonusu`(IN `_ID` INT, IN `_ADI` VARCHAR(100), IN `_KONU` INT, IN `_ALTKONU` INT, IN `_EMAIL` VARCHAR(2000), IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.task_alt_konu_detay 
SET adi = _ADI , fk_parent = _KONU, fk_konu = _ALTKONU,
email = _EMAIL ,isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.task_alt_konu_detay 
SET adi = _ADI , fk_parent = _KONU, fk_konu = _ALTKONU,
email = _EMAIL, isActive = _ISACTIVE 
where id = _ID

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmSETHavuzlar
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmSETHavuzlar`;
delimiter ;;
CREATE PROCEDURE `tnmSETHavuzlar`(IN `_ID` INT, IN `_ADI` VARCHAR(200), IN `_KONU` INT, IN `_IL` INT, IN `_SUBE` INT, IN `_EMAIL` VARCHAR(2000), IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.task_havuzlar 
SET adi = _ADI , fk_konu = _KONU, fk_il = _IL, fk_sube = _SUBE,
email = _EMAIL, isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.task_havuzlar
SET adi = _ADI , fk_konu = _KONU, fk_il = _IL, fk_sube = _SUBE,
email = _EMAIL, isActive = _ISACTIVE 
where id = _ID

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmSETKurumlar
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmSETKurumlar`;
delimiter ;;
CREATE PROCEDURE `tnmSETKurumlar`(IN `_ID` INT, IN `_ADI` VARCHAR(80), IN `_EMAIL` VARCHAR(2000), IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.task_konu 
SET adi = _ADI , email = _EMAIL, isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.task_konu 
SET adi = _ADI , email = _EMAIL,  isActive = _ISACTIVE 
where id = _ID 

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmSETSubeler
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmSETSubeler`;
delimiter ;;
CREATE PROCEDURE `tnmSETSubeler`(IN `_ID` INT, IN `_ADI` VARCHAR(100), IN `_KONU` INT, IN `_IL` INT, IN `_ISACTIVE` INT)
  NO SQL 
BEGIN

if(_ID = 0) THEN

INSERT INTO SMDR_1002.crm_SubeDB 
SET adi = _ADI , fk_konu = _KONU, fk_il = _IL, isActive = _ISACTIVE ;

End if;


if(_ID > 0) THEN

UPDATE SMDR_1002.crm_SubeDB 
SET adi = _ADI , fk_konu = _KONU, fk_il = _IL, isActive = _ISACTIVE 
where id = _ID 

limit 1;

End if;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmSubeler
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmSubeler`;
delimiter ;;
CREATE PROCEDURE `tnmSubeler`(IN `_ID` INT,IN `_TERM` VARCHAR(200),IN _fk_konu INT,IN fk_il INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="select s.*,k.adi kurum,i.adi il,if(s.isActive=1,'Aktif','Pasif') durumm,
				(select count(*) from SMDR_1002.task_havuzlar where fk_sube=s.id and isActive=1) havuz_sayisi 
				from SMDR_1002.crm_SubeDB s
				left join SMDR_1002.task_konu k on k.id=s.fk_konu
				left join SMDR_1002.bild_tnm_il i on i.id=s.fk_il
			WHERE 1=1	";

if(_ID > 0) THEN
		SET @q=CONCAT(@q," and s.id=",_ID);
END IF;
if(_fk_konu > 0) THEN
		SET @q=CONCAT(@q," and s.fk_konu=",_fk_konu);
END IF;
if(fk_il > 0) THEN
		SET @q=CONCAT(@q," and s.fk_il=",fk_il);
END IF;
IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and s.adi like '",_TERM,"%'"); 
END IF;
SET @q=CONCAT(@q," order by s.id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmSubelerCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmSubelerCount`;
delimiter ;;
CREATE PROCEDURE `tnmSubelerCount`(IN `_ID` INT,IN `_TERM` VARCHAR(200),IN _fk_konu INT)
  NO SQL 
BEGIN

SET @q="select COUNT(*) total 
				from SMDR_1002.crm_SubeDB s
				left join SMDR_1002.task_konu k on k.id=s.fk_konu
				left join SMDR_1002.bild_tnm_il i on i.id=s.fk_il
			WHERE 1=1	";

if(_ID > 0) THEN
		SET @q=CONCAT(@q," and s.id=",_ID);
END IF;
if(_fk_konu > 0) THEN
		SET @q=CONCAT(@q," and s.fk_konu=",_fk_konu);
END IF;
IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and s.adi like '",_TERM,"%'"); 
END IF;

PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmSubelerFiltre
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmSubelerFiltre`;
delimiter ;;
CREATE PROCEDURE `tnmSubelerFiltre`(IN _id INT,
IN _fkKonu INT,
IN _term TEXT, 
IN _fk_il INT,
IN _ids TEXT,
IN _wherein INT)
  NO SQL 
BEGIN
-- wherein 0 ise ids den gelen datayı where in seklinde arar. wherein 1 ise not in 2 ise kapatır
SET NAMES UTF8;
	SET @q="SELECT s.*,k.adi kurum,i.adi il,fk_konu,fk_il,
	(select count(*) from SMDR_1002.task_havuzlar where fk_sube=s.id 
	and isActive=1) havuz_sayisi 
	from SMDR_1002.crm_SubeDB s
	left join SMDR_1002.task_konu k on k.id=s.fk_konu
	left join SMDR_1002.bild_tnm_il i on i.id=s.fk_il 
	where 1=1 
	";
	IF(_id>0) THEN
		SET @q=CONCAT(@q," AND s.id=",_id);
	ELSE
	
		IF(_fkKonu>0) THEN
			SET @q=CONCAT(@q," AND s.fk_konu=",_fkKonu);
		END IF;
		
		IF(LENGTH(_term)>2) THEN
			SET @q=CONCAT(@q," AND s.adi like '",_term,"%'");
		END IF;
		
		IF(_fk_il>0) THEN
			SET @q=CONCAT(@q," AND s.fk_il=",_fk_il);
		END IF;
		
		IF(_wherein=0 AND length(_ids)>0) THEN
			SET @q=CONCAT(@q," AND s.id IN (",_ids,")");
		END IF;
	
	END IF;

	SET @q=CONCAT(@q," order by s.id ");

	PREPARE stmt FROM @q;
	EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmUlkeler
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmUlkeler`;
delimiter ;;
CREATE PROCEDURE `tnmUlkeler`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN whereIN INT,IN _skip INT,IN _take INT)
  NO SQL 
BEGIN

SET @q="SELECT *,if(k.isActive=1,'Aktif','Pasif') durumu FROM SMDR_1002.bild_tnm_ulke k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;

SET @q=CONCAT(@q," order by k.id");
SET @q=CONCAT(@q," limit ",_skip,",",_take);


PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmUlkelerCount
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmUlkelerCount`;
delimiter ;;
CREATE PROCEDURE `tnmUlkelerCount`(IN `IDS` TEXT,IN `_TERM` VARCHAR(200),IN isActive INT,IN whereIN INT)
  NO SQL 
BEGIN

SET @q="SELECT COUNT(*) total FROM SMDR_1002.bild_tnm_ulke k WHERE 1=1 
";

 IF(isActive<2) THEN
	SET @q=CONCAT(@q," AND k.isActive=",isActive);
END IF;
IF(whereIN=0 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id IN (",IDS,")");
END IF;

IF(whereIN=1 AND length(IDS)>0) THEN
	SET @q=CONCAT(@q," AND k.id NOT IN (",IDS,")");
END IF;

IF(LENGTH(_TERM)>2) THEN
	SET @q=CONCAT(@q," and k.adi like '",_TERM,"%'"); 
END IF;
PREPARE stmt FROM @q;
EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for tnmUpdate
-- ----------------------------
DROP PROCEDURE IF EXISTS `tnmUpdate`;
delimiter ;;
CREATE PROCEDURE `tnmUpdate`(IN fldCount INT,IN tbl TEXT,IN flds TEXT,IN vals TEXT,IN id INT,IN ayrac VARCHAR(50))
  NO SQL 
BEGIN

      DECLARE i INT Default 0 ;
			SET @q=CONCAT("UPDATE ",tbl," SET ");
      simple_loop: LOOP
         SET i=i+1;
        -- select a;
         IF i>fldCount THEN
            LEAVE simple_loop;
         END IF;
				 IF i=fldCount THEN
						SET @fld=SPLIT_STRING(flds,ayrac,i);
						SET @val=SPLIT_STRING(vals,ayrac,i);
            SET @q=CONCAT(@q,@fld,"='",@val,"'");
				 ELSE
						 SET @fld=SPLIT_STRING(flds,ayrac,i);
						 SET @val=SPLIT_STRING(vals,ayrac,i);
						 SET @q=CONCAT(@q,@fld,"='",@val,"',");
         END IF;				 
		END LOOP simple_loop;
		SET @q=CONCAT(@q," WHERE id=",id);
	-- select @q;
		PREPARE stmt FROM @q;
		EXECUTE stmt;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for vatandasBilgisiAcKapat
-- ----------------------------
DROP PROCEDURE IF EXISTS `vatandasBilgisiAcKapat`;
delimiter ;;
CREATE PROCEDURE `vatandasBilgisiAcKapat`(IN `taskId` int,IN `isOnayBekle` int)
  NO SQL 
BEGIN
	SET NAMES UTF8;
	/*******
	TASK DB UPDATE
	*******/
	SET @sqlTask="UPDATE SMDR_1002.task_DB SET ";
	SET @sqlTask=CONCAT(@sqlTask," isOnayBekle='",isOnayBekle,"' ");
	SET @sqlTask=CONCAT(@sqlTask," WHERE id=",taskId);
	PREPARE stmt FROM @sqlTask;
	EXECUTE stmt;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for vtMesajGonder
-- ----------------------------
DROP PROCEDURE IF EXISTS `vtMesajGonder`;
delimiter ;;
CREATE PROCEDURE `vtMesajGonder`(IN `_MESAJ` LONGTEXT, IN `_ALICI` int, IN `_GONDEREN` int, IN `_PARENT` int, IN `_FKROOT` int)
BEGIN
SET @q="INSERT INTO SMDR_1002.vt_mesaj SET ";
SET @q=concat(@q," fk_gonderen=",_GONDEREN,",fk_alici=",_ALICI,",mesaj='",_MESAJ,"'");
IF(_PARENT>0) THEN
	SET @q=CONCAT(@q,",isroot=0,fk_root=",_FKROOT);
END IF;

	PREPARE stmt FROM @q;
	EXECUTE stmt;
#select @q;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
